package Bundle::Snapshot_2011_05_03_00;

$VERSION = '0.01';

1;

__END__

=head1 NAME

Bundle::Snapshot_2011_05_03_00 - Snapshot of installation on k81 on Tue May  3 08:04:38 2011

=head1 SYNOPSIS

perl -MCPAN -e 'install Bundle::Snapshot_2011_05_03_00'

=head1 CONTENTS

Data::ObjectDriver::BaseObject undef
Data::ObjectDriver::BaseView undef
Data::ObjectDriver::Driver::BaseCache undef
Data::ObjectDriver::Driver::Cache::Apache undef
Data::ObjectDriver::Driver::Cache::Cache undef
Data::ObjectDriver::Driver::Cache::Memcached undef
Data::ObjectDriver::Driver::Cache::RAM undef
Data::ObjectDriver::Driver::DBD undef
Data::ObjectDriver::Driver::DBD::Oracle undef
Data::ObjectDriver::Driver::DBD::Pg undef
Data::ObjectDriver::Driver::DBD::SQLite undef
Data::ObjectDriver::Driver::DBD::mysql undef
Data::ObjectDriver::Driver::DBI undef
Data::ObjectDriver::Driver::GearmanDBI undef
Data::ObjectDriver::Driver::MultiPartition undef
Data::ObjectDriver::Driver::Multiplexer undef
Data::ObjectDriver::Driver::Partition undef
Data::ObjectDriver::Driver::SimplePartition undef
Data::ObjectDriver::Errors undef
Data::ObjectDriver::Iterator undef
Data::ObjectDriver::Profiler undef
Data::ObjectDriver::ResultSet undef
Data::ObjectDriver::SQL undef
Data::ObjectDriver::SQL::Oracle undef
Data::OpenGraph::Parser undef
POEx::Role::Event 1.102610
Data::Paginator::Types undef
Data::ParseBinary::Data::Cap undef
Data::ParseBinary::Data::Netflow undef
Data::ParseBinary::Executable::ELF32 undef
Data::ParseBinary::Executable::PE32 undef
Data::ParseBinary::FileSystem::MBR undef
Data::ParseBinary::Graphics::BMP undef
Data::ParseBinary::Graphics::EMF undef
Data::ParseBinary::Graphics::PNG undef
Data::ParseBinary::Graphics::WMF undef
Data::Phrasebook::Debug 0.31
Data::Phrasebook::Generic 0.31
Data::Phrasebook::Loader 0.31
Data::Phrasebook::Loader::Base 0.31
Data::Phrasebook::Loader::Text 0.31
Data::Phrasebook::Plain 0.31
Data::Phrasebook::SQL 0.31
Data::Phrasebook::SQL::Query 0.31
Data::Predicate::ClosurePredicate undef
Data::Predicate::Predicates undef
Data::Rand::Obscure::Generator undef
Data::Random::WordList 0.05
Data::Rx::CoreType 0.100110
Data::Rx::CoreType::all 0.100110
Data::Rx::CoreType::any 0.100110
Data::Rx::CoreType::arr 0.100110
Data::Rx::CoreType::bool 0.100110
Data::Rx::CoreType::def 0.100110
Data::Rx::CoreType::fail 0.100110
Data::Rx::CoreType::int 0.100110
Data::Rx::CoreType::map 0.100110
Data::Rx::CoreType::nil 0.100110
Data::Rx::CoreType::num 0.100110
Data::Rx::CoreType::one 0.100110
Data::Rx::CoreType::rec 0.100110
Data::Rx::CoreType::seq 0.100110
Data::Rx::CoreType::str 0.100110
Data::Rx::TypeBundle 0.100110
Data::Rx::Type::Perl::Code 0.004
Data::Rx::Type::Perl::Obj 0.004
Data::Rx::Type::Perl::Ref 0.004
Data::Rx::Type::Rx 0.103520
Data::Rx::TypeBundle::Core 0.100110
Data::Rx::Util 0.100110
Data::Schema::Config 0.133
Data::Schema::Plugin::LoadSchema::Base 0.133
Data::Schema::Plugin::LoadSchema::Hash 0.133
Data::Schema::Plugin::LoadSchema::YAMLFile 0.133
Data::Schema::Schema::Schema 0.133
Data::Schema::Type::All 0.133
Data::Schema::Type::Array 0.133
Data::Schema::Type::Base 0.133
Data::Schema::Type::Bool 0.133
Data::Schema::Type::CIStr 0.133
Data::Schema::Type::Comparable 0.133
Data::Schema::Type::Either 0.133
Data::Schema::Type::Float 0.133
Data::Schema::Type::HasElement 0.133
Data::Schema::Type::Hash 0.133
Data::Schema::Type::Int 0.133
Data::Schema::Type::Num 0.133
Data::Schema::Type::Object 0.133
Data::Schema::Type::Printable 0.133
Data::Schema::Type::Scalar 0.133
Data::Schema::Type::Schema 0.133
Data::Schema::Type::Sortable 0.133
Data::SearchEngine::Item undef
Data::Schema::Type::Str 0.133
Data::Schema::Type::TypeName 0.133
Data::SearchEngine::Paginator undef
Data::SearchEngine::Query undef
Data::SearchEngine::Results undef
Data::SearchEngine::Meta::Attribute::Trait::Digestable undef
Data::SearchEngine::Modifiable undef
Data::SearchEngine::Results::Spellcheck::Suggestion undef
Data::SearchEngine::Results::Faceted undef
Data::SearchEngine::Results::Spellcheck undef
WebService::Solr::Response
Data::SearchEngine::Solr::Results undef
Data::Semantic::Net::IPAddress 1.101760
Data::Semantic::Net::IPAddress::IPv4 1.101760
Data::Semantic::Net::IPAddress::IPv4_TEST 1.101760
Data::Semantic::Net::IPAddress::IPv6 1.101760
Data::Semantic::Net::IPAddress::IPv6_TEST 1.101760
Data::Semantic::RegexpAdapter 1.101620
Data::Serializer::Benchmark::Serialize undef
Data::Serializer::Bencode 0.03
Data::Serializer::Config::General 0.02
Data::Serializer::Convert::Bencode 0.03
Data::Serializer::Convert::Bencode_XS 0.03
Data::Serializer::Cookbook 0.05
Data::Serializer::Data::Denter 0.02
Data::Serializer::Data::Dumper 0.05
Data::Serializer::Data::Taxi 0.02
Data::Serializer::FreezeThaw 0.02
Data::Serializer::JSON 0.03
Data::Serializer::JSON::Syck 0.02
Data::Serializer::PHP::Serialization 0.02
Data::Serializer::Persistent 0.01
Data::Serializer::Raw 0.02
Data::Serializer::Storable 0.03
Data::Serializer::XML::Dumper 0.02
Data::Serializer::XML::Simple 0.03
Data::Serializer::YAML 0.02
Data::Serializer::YAML::Syck 0.02
Data::Session::Base 1.05
Data::Session::CGISession 1.05
Data::Session::Driver 1.05
Data::Session::Driver::BerkeleyDB 1.05
Data::Session::Driver::File 1.05
Data::Session::Driver::Memcached 1.05
Data::Session::Driver::ODBC 1.05
Data::Session::Driver::Oracle 1.05
Data::Session::Driver::Pg 1.05
Data::Session::Driver::SQLite 1.05
Data::Session::Driver::mysql 1.05
Data::Session::ID 1.05
Data::Session::ID::AutoIncrement 1.05
Data::Session::ID::MD5 1.05
Data::Session::ID::SHA1 1.05
Data::Session::ID::SHA256 1.05
Data::Session::ID::SHA512 1.05
Data::Session::ID::Static 1.05
Data::Session::ID::UUID16 1.05
Data::Session::ID::UUID34 1.05
Data::Session::ID::UUID36 1.05
Data::Session::ID::UUID64 1.05
Data::Session::SHA 1.05
Data::Session::Serialize::DataDumper 1.05
Data::Session::Serialize::FreezeThaw 1.05
Data::Session::Serialize::JSON 1.05
Data::Session::Serialize::Storable 1.05
Data::Session::Serialize::YAML 1.05
Data::Shark v2.1
Data::Shark::DBI v2.1
Data::Shark::DIO v2.2
Data::Shark::FIO v2.1
Data::Shark::Util v2.1
Data::Stag::Arr2HTML 0.11
Data::Stag::Base 0.11
Data::Stag::BaseGenerator undef
Data::Stag::BaseHandler 0.11
Data::Stag::ChainHandler 0.11
Data::Stag::DTDWriter 0.11
Data::Stag::GraphHandler 0.11
Data::Stag::HashDB 0.11
Data::Stag::ITextParser 0.11
Data::Stag::ITextWriter 0.11
Data::Stag::IndentParser 0.11
Data::Stag::IndentWriter 0.11
Data::Stag::PerlWriter 0.11
Data::Stag::PodParser 0.11
Data::Stag::SAX2Stag 0.11
Data::Stag::Simple 0.11
Data::Stag::StagDB 0.11
Data::Stag::StagI undef
Data::Stag::StagImpl 0.11
Data::Stag::SxprParser 0.11
Data::Stag::SxprWriter 0.11
Data::Storage::DBI::Mock 1.102720
Data::Storage::DBI::Oracle 1.102720
Data::Stag::Util 0.11
Data::Stag::Writer 0.11
Data::Stag::XMLParser 0.11
Data::Stag::XMLWriter 0.11
Data::Stag::XSLHandler undef
Data::Stag::XSLTHandler undef
Data::Stag::null 0.11
Data::Storage::DBI 1.102720
POE::Declarative::Mixin 0.09
POE::Declare::Meta 0.54
POE::Declare::Meta::Attribute 0.54
POE::Declare::Meta::Event 0.54
POE::Declare::Meta::Internal 0.54
POE::Declare::Meta::Message 0.54
POE::Declare::Meta::Param 0.54
POE::Declare::Meta::Slot 0.54
POE::Declare::Meta::Timeout 0.54
POE::Declare::Object 0.54
POE::Driver 1.311
POE::Session::AttributeBased
Spread
POE::Driver::SpreadClient 1.002
POE::Filter::Block 1.311
POE::Filter::FastCGI 0.16
POE::Filter::Grep 1.311
POE::Filter::HTTPChunk 0.942
POE::Filter::HTTPD 1.311
POE::Filter::HTTPHead 0.942
POE::Filter::IRC 6.61
POE::Filter::IRC::Compat 6.61
POE::Filter::Ident 1.16
POE::Filter::Map 1.311
POE::Filter::Postfix::Base64 0.003
POE::Filter::Postfix::Null 0.003
POE::Filter::Postfix::Plain 0.003
POE::Filter::RecordBlock 1.311
POE::Filter::Reference 1.311
POE::Filter::SpreadClient 1.002
POE::Filter::Syslog 1.20
POE::Filter::TacacsPlus 1.06
POE::Kernel 1.311
POE::Loop 1.311
POE::Loop::IO_Poll 1.311
POE::Loop::PerlSignals 1.311
POE::Loop::Select 1.311
POE::Loop::TkActiveState 1.304
POE::Loop::TkCommon 1.304
POE::NFA 1.311
POE::Pipe 1.311
POE::Pipe::OneWay 1.311
POE::Pipe::TwoWay 1.311
POE::Queue 1.311
POE::Queue::Array 1.311
Data::Validate
POE::Test::Helpers::MooseRole 1.11
POE::Resource 1.311
POE::Resource::Aliases 1.311
POE::Resource::Events 1.311
POE::Resource::Extrefs 1.311
POE::Resource::FileHandles 1.311
POE::Resource::SIDs 1.311
POE::Resource::Sessions 1.311
POE::Resource::Signals 1.311
POE::Resources 1.311
POE::Test::DondeEstan 1.311
POE::Test::Helpers::API 1.11
POE::Wheel 1.311
POE::Wheel::Curses 1.311
POE::Wheel::Run 1.311
POE::Wheel::FollowTail 1.311
POE::Wheel::ListenAccept 1.311
POE::Wheel::ReadLine 1.311
POE::Component::Server::HTTP
POE::XUL::Application 0.0600
POE::XUL::CDATA 0.0600
XUL::Node
POE::XUL::ChangeManager 0.0600
POE::XUL::Constants 0.0600
POE::XUL::Controler 0.0600
POE::XUL::Encode 0.0600
POE::XUL::Event 0.0600
POE::XUL::Javascript undef
POE::XUL::Logging 0.0600
POE::XUL::Node 0.0600
POE::XUL::RDF 0.0600
POE::XUL::Request 0.0600
POE::XUL::Session 0.0600
POE::XUL::State 0.0600
POE::XUL::Style 0.0600
POE::XUL::TWindow 0.0600
POE::XUL::TextNode 0.0600
POE::XUL::Window 0.0600
POE::Session::Multiplex
POE::Session::PlainCall
POEx::URI
POEx::HTTP::Server::Connection undef
POEx::HTTP::Server::Error undef
POEx::HTTP::Server::Request undef
POEx::HTTP::Server::Response undef
POEx::ProxySession::Client 1.102760
POEx::ProxySession::MessageSender 1.102760
POEx::ProxySession::Proxy 1.102760
POEx::ProxySession::Server 1.102760
POEx::ProxySession::Types 1.102760
POEx::PubSub::Event 1.102740
POEx::PubSub::Types 1.102740
POEx::Role::Streaming
POEx::Role::TCPServer
POEx::Types
Plack::Test::Suite
POEx::Role::PSGIServer::ProxyWriter 1.103341
POEx::Role::PSGIServer::Streamer 1.103341
POEx::Role::ProxyEvent 1.102760
POEx::Role::SessionInstantiation::Meta::POEState 1.102610
POEx::Role::SessionInstantiation::Meta::Session::Events 1.102610
POEx::Role::SessionInstantiation::Meta::Session::Implementation 1.102610
POEx::Role::SessionInstantiation::Meta::Session::Magic 1.102610
POEx::Role::SessionInstantiation::Meta::Session::Sugar 1.102610
POEx::Types::PSGIServer 1.103341
POEx::WorkerPool::Error 1.102740
POEx::WorkerPool::Error::EnqueueError 1.102740
POEx::WorkerPool::Error::JobError 1.102740
POEx::WorkerPool::Error::NoAvailableWorkers 1.102740
POEx::WorkerPool::Error::StartError 1.102740
POEx::WorkerPool::Role::Job 1.102740
POEx::WorkerPool::Role::WorkerPool 1.102740
POEx::WorkerPool::Role::WorkerPool::OpenEndedWorker 1.102740
POEx::WorkerPool::Role::WorkerPool::Worker 1.102740
POEx::WorkerPool::Role::WorkerPool::Worker::Guts 1.102740
POEx::WorkerPool::Role::WorkerPool::Worker::GutsLoader 1.102740
POEx::WorkerPool::Types 1.102740
POEx::WorkerPool::Worker 1.102740
POEx::WorkerPool::Worker::Guts 1.102740
POEx::WorkerPool::Worker::GutsLoader 1.102740
POSIX 1.19
POEx::WorkerPool::WorkerEvents 1.102740
POSIX::Wide 7
POSIX::RT::Clock 0.009
POSIX::Wide::ERRNO 7
PPI::Find 1.215
POSIX::Wide::EXTENDED_OS_ERROR 7
PPI::Document::Normalized 1.215
PPI::Element 1.215
PPI::Exception 1.215
PPI::Exception::ParserRejection 1.215
PPI::HTML::Fragment 1.08
PPI::Lexer 1.215
PPI::Normal 1.215
PPI::Normal::Standard 1.215
PPI::Statement 1.215
PPI::Statement::Break 1.215
PPI::Statement::Compound 1.215
PPI::Statement::Data 1.215
PPI::Statement::End 1.215
PPI::Statement::Expression 1.215
PPI::Statement::Given 1.215
PPI::Statement::Include 1.215
PPI::Statement::Include::Perl6 1.215
PPI::Statement::Null 1.215
PPI::Statement::Package 1.215
PPI::Statement::Scheduled 1.215
PPI::Statement::Sub 1.215
PPI::Statement::Unknown 1.215
PPI::Statement::UnmatchedBrace 1.215
PPI::Statement::Variable 1.215
PPI::Statement::When 1.215
PPI::Structure 1.215
PPI::Structure::Block 1.215
PPI::Structure::Condition 1.215
PPI::Structure::Constructor 1.215
PPI::Structure::For 1.215
PPI::Structure::Given 1.215
PPI::Structure::List 1.215
PPI::Structure::Subscript 1.215
PPI::Structure::Unknown 1.215
PPI::Structure::When 1.215
PPI::Token 1.215
PPI::Token::ArrayIndex 1.215
PPI::Token::Attribute 1.215
PPI::Token::BOM 1.215
PPI::Token::Cast 1.215
PPI::Token::Comment 1.215
PPI::Token::DashedWord 1.215
PPI::Token::Data 1.215
PPI::Token::End 1.215
PPI::Token::HereDoc 1.215
PPI::Token::Label 1.215
PPI::Token::Magic 1.215
PPI::Token::Number 1.215
PPI::Token::Number::Binary 1.215
PPI::Token::Number::Exp 1.215
PPI::Token::Number::Float 1.215
PPI::Token::Number::Hex 1.215
PPI::Token::Number::Octal 1.215
PPI::Token::Number::Version 1.215
PPI::Token::Operator 1.215
PPI::Token::Pod 1.215
PPI::Token::Prototype 1.215
PPI::Token::Quote 1.215
PPI::Token::Quote::Double 1.215
PPI::Token::Quote::Interpolate 1.215
PPI::Token::Quote::Literal 1.215
PPI::Token::QuoteLike 1.215
PPI::Token::QuoteLike::Backtick 1.215
PPI::Token::QuoteLike::Command 1.215
PPI::Token::QuoteLike::Readline 1.215
PPI::Token::QuoteLike::Regexp 1.215
PPI::Token::QuoteLike::Words 1.215
PPI::Token::Regexp 1.215
PPI::Token::Regexp::Match 1.215
PPI::Token::Regexp::Substitute 1.215
PPI::Token::Regexp::Transliterate 1.215
PPI::Token::Separator 1.215
PPI::Token::Structure 1.215
PPI::Token::Unknown 1.215
PPI::Token::Word 1.215
PPI::Transform 1.215
PPI::Token::_QuoteEngine 1.215
PPI::Token::_QuoteEngine::Full 1.215
PPI::Token::_QuoteEngine::Simple 1.215
PPI::Tokenizer 1.215
PPI::Transform::UpdateCopyright 1.215
PPI::XS::Tokenizer::Constants 0.01
PPI::XSAccessor 1.215
PPIx::EditorTools::FindUnmatchedBrace 0.11
PPIx::EditorTools::FindVariableDeclaration 0.11
PPIx::EditorTools::IntroduceTemporaryVariable 0.11
PPIx::EditorTools::RenamePackage 0.11
PPIx::EditorTools::RenamePackageFromPath 0.11
PPIx::EditorTools::RenameVariable 0.11
PPIx::EditorTools::ReturnObject 0.11
PPIx::Regexp::Constant 0.020
PPIx::Regexp::Dumper 0.020
PPIx::Regexp::Element 0.020
PPIx::Regexp::Lexer 0.020
PPIx::Regexp::Node 0.020
PPIx::Regexp::Node::Range 0.020
PPIx::Regexp::Structure 0.020
PPIx::Regexp::Structure::Assertion 0.020
PPIx::Regexp::Structure::BranchReset 0.020
PPIx::Regexp::Structure::Capture 0.020
PPIx::Regexp::Structure::CharClass 0.020
PPIx::Regexp::Structure::Code 0.020
PPIx::Regexp::Structure::Main 0.020
PPIx::Regexp::Structure::Modifier 0.020
PPIx::Regexp::Structure::NamedCapture 0.020
PPIx::Regexp::Structure::Quantifier 0.020
PPIx::Regexp::Structure::Regexp 0.020
PPIx::Regexp::Structure::Replacement 0.020
PPIx::Regexp::Structure::Subexpression 0.020
PPIx::Regexp::Structure::Switch 0.020
PPIx::Regexp::Structure::Unknown 0.020
PPIx::Regexp::Support 0.020
PPIx::Regexp::Token 0.020
PPIx::Regexp::Token::Assertion 0.020
PPIx::Regexp::Token::Backreference 0.020
PPIx::Regexp::Token::Backtrack 0.020
PPIx::Regexp::Token::CharClass 0.020
PPIx::Regexp::Token::CharClass::POSIX 0.020
PPIx::Regexp::Token::CharClass::POSIX::Unknown 0.020
PPIx::Regexp::Token::CharClass::Simple 0.020
PPIx::Regexp::Token::Code 0.020
PPIx::Regexp::Token::Comment 0.020
PPIx::Regexp::Token::Condition 0.020
PPIx::Regexp::Token::Control 0.020
PPIx::Regexp::Token::Delimiter 0.020
PPIx::Regexp::Token::Greediness 0.020
PPIx::Regexp::Token::GroupType 0.020
PPIx::Regexp::Token::GroupType::Assertion 0.020
PPIx::Regexp::Token::GroupType::BranchReset 0.020
PPIx::Regexp::Token::GroupType::Code 0.020
PPIx::Regexp::Token::GroupType::Modifier 0.020
PPIx::Regexp::Token::GroupType::NamedCapture 0.020
PPIx::Regexp::Token::GroupType::Subexpression 0.020
PPIx::Regexp::Token::GroupType::Switch 0.020
PPIx::Regexp::Token::Interpolation 0.020
PPIx::Regexp::Token::Literal 0.020
PPIx::Regexp::Token::Modifier 0.020
PPIx::Regexp::Token::Operator 0.020
PPIx::Regexp::Token::Quantifier 0.020
PPIx::Regexp::Token::Recursion 0.020
PPIx::Regexp::Token::Reference 0.020
PPIx::Regexp::Token::Structure 0.020
PPIx::Regexp::Token::Unknown 0.020
PPIx::Regexp::Token::Unmatched 0.020
PPIx::Regexp::Token::Whitespace 0.020
PPIx::Regexp::Tokenizer 0.020
PPIx::Regexp::Util 0.020
PPIx::Utilities::Exception::Bug 1.001000
PPM::Make::Bundle 0.97
PPM::Make::CPAN 0.97
PPM::Make::Config 0.97
PPM::Make::Install 0.97
PPM::Make::Meta 0.97
PPM::Make::RepositorySummary 0.97
PPM::Make::Search 0.97
PPM::Make::Util 0.97
Package::Aspect::Application_Folders undef
Package::Aspect::Customized_Settings undef
Package::Aspect::Customized_Settings::Collection undef
Package::Aspect::Customized_Settings::Default::Fixed_String_Set_1ofN undef
Package::Aspect::Customized_Settings::Default::Fixed_String_Set_MofN undef
Package::Aspect::Customized_Settings::Default::Here undef
Package::Aspect::Customized_Settings::Default::Message undef
Package::Aspect::Customized_Settings::Default::Number undef
Package::Aspect::Customized_Settings::Default::Numeric_Range undef
Package::Aspect::Customized_Settings::Default::RGB undef
Package::Aspect::Customized_Settings::Default::String_plain undef
Package::Aspect::Customized_Settings::Default::Table_Indexed undef
Package::Aspect::Customized_Settings::Default::Table_Simple_Array undef
Package::Aspect::Customized_Settings::Default::Table_Simple_Hash undef
Package::Aspect::Customized_Settings::Default::Time_Duration undef
Package::Aspect::Customized_Settings::Default::Variable_String_Set undef
Package::Aspect::Customized_Settings::Default::Yes_No undef
Package::Aspect::Customized_Settings::Default::_ undef
Package::Aspect::Customized_Settings::Default::_Scalar undef
Package::Aspect::Customized_Settings::Default::_Table undef
Package::Aspect::Customized_Settings::Default::_Tabular undef
Package::Aspect::Customized_Settings::Dot_Def undef
Package::Aspect::Customized_Settings::Recognizer undef
Package::Aspect::DBH undef
Package::Aspect::Localized_Messages undef
Package::Aspect::Localized_Messages::Dot_Msg undef
Package::Aspect::Subroutine_Library 0.01
Package::Butcher::Inflator 0.02
Package::New::Dump 0.05
Package::Pkg::Lexicon undef
Package::Pkg::Loader undef
Package::Reaper 0.103
Package::Stash::PP 0.28
Package::Watchdog::Sub undef
Package::Watchdog::Sub::Forbidden undef
Package::Watchdog::Sub::Watched undef
Package::Watchdog::Tracker undef
Package::Watchdog::Tracker::Forbid undef
Package::Watchdog::Tracker::Watch undef
Package::Watchdog::Util undef
Palm::Raw 1.012
PagSeguro::Base undef
PagSeguro::Cliente undef
PagSeguro::Item undef
Palm 1.012
Palm::Address 1.012
Palm::DateTime 1.012
Palm::PDB 1.012
Palm::Datebook 1.012
Palm::Mail 1.012
Palm::Memo 1.012
Palm::StdAppInfo 1.012
Palm::ToDo 1.012
Palm::ZirePhoto 1.012
Regexp::Grammars::Common::String
Paludis::ResumeState::Serialization::Basic 0.01000409
Paludis::ResumeState::Serialization::Grammar 0.01000409
Paper::Specs::Avery undef
Paper::Specs::Avery::2160_1 undef
Paper::Specs::Avery::2160_2 undef
Paper::Specs::Avery::2162_1 undef
Paper::Specs::Avery::2162_2 undef
Paper::Specs::Avery::2163_1 undef
Paper::Specs::Avery::2163_2 undef
Paper::Specs::Avery::2164_1 undef
Paper::Specs::Avery::2164_2 undef
Paper::Specs::Avery::2180_1 undef
Paper::Specs::Avery::2180_2 undef
Paper::Specs::Avery::2181_1 undef
Paper::Specs::Avery::2181_2 undef
Paper::Specs::Avery::2186_1 undef
Paper::Specs::Avery::2186_2 undef
Paper::Specs::Avery::3251 undef
Paper::Specs::Avery::3256 undef
Paper::Specs::Avery::3259 undef
Paper::Specs::Avery::3260 undef
Paper::Specs::Avery::3263 undef
Paper::Specs::Avery::3265 undef
Paper::Specs::Avery::3266 undef
Paper::Specs::Avery::3268 undef
Paper::Specs::Avery::3269 undef
Paper::Specs::Avery::3277 undef
Paper::Specs::Avery::5066 undef
Paper::Specs::Avery::5095 undef
Paper::Specs::Avery::5096 undef
Paper::Specs::Avery::5160 undef
Paper::Specs::Avery::5161 undef
Paper::Specs::Avery::5162 undef
Paper::Specs::Avery::5163 undef
Paper::Specs::Avery::5164 undef
Paper::Specs::Avery::5165 undef
Paper::Specs::Avery::5166 undef
Paper::Specs::Avery::5167 undef
Paper::Specs::Avery::5168 undef
Paper::Specs::Avery::5196 undef
Paper::Specs::Avery::5198 undef
Paper::Specs::Avery::5199_1 undef
Paper::Specs::Avery::5199_2 undef
Paper::Specs::Avery::5260 undef
Paper::Specs::Avery::5261 undef
Paper::Specs::Avery::5262 undef
Paper::Specs::Avery::5263 undef
Paper::Specs::Avery::5264 undef
Paper::Specs::Avery::5265 undef
Paper::Specs::Avery::5266 undef
Paper::Specs::Avery::5293 undef
Paper::Specs::Avery::5294 undef
Paper::Specs::Avery::5305 undef
Paper::Specs::Avery::5309 undef
Paper::Specs::Avery::5315 undef
Paper::Specs::Avery::5361_1 undef
Paper::Specs::Avery::5361_2 undef
Paper::Specs::Avery::5362_2 undef
Paper::Specs::Avery::5364_1 undef
Paper::Specs::Avery::5364_2 undef
Paper::Specs::Avery::5366 undef
Paper::Specs::Avery::5371 undef
Paper::Specs::Avery::5376 undef
Paper::Specs::Avery::5377 undef
Paper::Specs::Avery::5383 undef
Paper::Specs::Avery::5384 undef
Paper::Specs::Avery::5385 undef
Paper::Specs::Avery::5386 undef
Paper::Specs::Avery::5388 undef
Paper::Specs::Avery::5389 undef
Paper::Specs::Avery::5390 undef
Paper::Specs::Avery::5392 undef
Paper::Specs::Avery::5395 undef
Paper::Specs::Avery::5660 undef
Paper::Specs::Avery::5661 undef
Paper::Specs::Avery::5662 undef
Paper::Specs::Avery::5663 undef
Paper::Specs::Avery::5664 undef
Paper::Specs::Avery::5666 undef
Paper::Specs::Avery::5667 undef
Paper::Specs::Avery::5766 undef
Paper::Specs::Avery::5824 undef
Paper::Specs::Avery::5866 undef
Paper::Specs::Avery::5871 undef
Paper::Specs::Avery::5881 undef
Paper::Specs::Avery::5883 undef
Paper::Specs::Avery::5884 undef
Paper::Specs::Avery::5889 undef
Paper::Specs::Avery::5895 undef
Paper::Specs::Avery::5896 undef
Paper::Specs::Avery::5911 undef
Paper::Specs::Avery::5922 undef
Paper::Specs::Avery::5925_1 undef
Paper::Specs::Avery::5925_2 undef
Paper::Specs::Avery::5925_3 undef
Paper::Specs::Avery::5930 undef
Paper::Specs::Avery::5931_1 undef
Paper::Specs::Avery::5931_2 undef
Paper::Specs::Avery::5960 undef
Paper::Specs::Avery::5961 undef
Paper::Specs::Avery::5963 undef
Paper::Specs::Avery::5966 undef
Paper::Specs::Avery::5970 undef
Paper::Specs::Avery::5971 undef
Paper::Specs::Avery::5972 undef
Paper::Specs::Avery::5975 undef
Paper::Specs::Avery::5979 undef
Paper::Specs::Avery::5980 undef
Paper::Specs::Avery::5997_1 undef
Paper::Specs::Avery::5997_2 undef
Paper::Specs::Avery::5998 undef
Paper::Specs::Avery::6460 undef
Paper::Specs::Avery::6464 undef
Paper::Specs::Avery::6465 undef
Paper::Specs::Avery::6466 undef
Paper::Specs::Avery::6467 undef
Paper::Specs::Avery::6490 undef
Paper::Specs::Avery::6873 undef
Paper::Specs::Avery::6879 undef
Paper::Specs::Avery::74520 undef
Paper::Specs::Avery::74540 undef
Paper::Specs::Avery::74541 undef
Paper::Specs::Avery::74550 undef
Paper::Specs::Avery::74551 undef
Paper::Specs::Avery::74552 undef
Paper::Specs::Avery::74558 undef
Paper::Specs::Avery::74650 undef
Paper::Specs::Avery::8160 undef
Paper::Specs::Avery::8161 undef
Paper::Specs::Avery::8162 undef
Paper::Specs::Avery::8163 undef
Paper::Specs::Avery::8164 undef
Paper::Specs::Avery::8165 undef
Paper::Specs::Avery::8166 undef
Paper::Specs::Avery::8167 undef
Paper::Specs::Avery::8196 undef
Paper::Specs::Avery::8252 undef
Paper::Specs::Avery::8253 undef
Paper::Specs::Avery::8254 undef
Paper::Specs::Avery::8255 undef
Paper::Specs::Avery::8257 undef
Paper::Specs::Avery::8313 undef
Paper::Specs::Avery::8314 undef
Paper::Specs::Avery::8315 undef
Paper::Specs::Avery::8316 undef
Paper::Specs::Avery::8317 undef
Paper::Specs::Avery::8324 undef
Paper::Specs::Avery::8366 undef
Paper::Specs::Avery::8371 undef
Paper::Specs::Avery::8373 undef
Paper::Specs::Avery::8376 undef
Paper::Specs::Avery::8377 undef
Paper::Specs::Avery::8384 undef
Paper::Specs::Avery::8387 undef
Paper::Specs::Avery::8389 undef
Paper::Specs::Avery::8460 undef
Paper::Specs::Avery::8461 undef
Paper::Specs::Avery::8462 undef
Paper::Specs::Avery::8463 undef
Paper::Specs::Avery::8464 undef
Paper::Specs::Avery::8465 undef
Paper::Specs::Avery::8660 undef
Paper::Specs::Avery::8662 undef
Paper::Specs::Avery::8663 undef
Paper::Specs::Avery::8665 undef
Paper::Specs::Avery::8667 undef
Paper::Specs::Avery::8763 undef
Paper::Specs::Avery::8769 undef
Paper::Specs::Avery::8871 undef
Paper::Specs::Avery::8877 undef
Paper::Specs::Avery::8923 undef
Paper::Specs::Avery::8925_1 undef
Paper::Specs::Avery::8925_2 undef
Paper::Specs::Avery::8925_3 undef
Paper::Specs::Avery::8931_1 undef
Paper::Specs::Avery::8931_2 undef
Paper::Specs::Avery::8931_3 undef
Paper::Specs::Avery::8931_4 undef
Paper::Specs::Avery::8931_5 undef
Paper::Specs::base::brand 0.02
Paper::Specs::base::label 0.01
Paper::Specs::base::sheet 0.01
Paper::Specs::photo undef
Paper::Specs::photo::10x12 undef
Paper::Specs::photo::10x15 undef
Paper::Specs::photo::10x8 undef
Paper::Specs::photo::12x10 undef
Paper::Specs::photo::12x8 undef
Paper::Specs::photo::15x10 undef
Paper::Specs::photo::16x20 undef
Paper::Specs::photo::20x16 undef
Paper::Specs::photo::20x30 undef
Paper::Specs::photo::30x20 undef
Paper::Specs::photo::30x40 undef
Paper::Specs::photo::40x30 undef
Paper::Specs::photo::4x6 undef
Paper::Specs::photo::5x7 undef
Paper::Specs::photo::6x4 undef
Paper::Specs::photo::6x8 undef
Paper::Specs::photo::6x9 undef
Paper::Specs::photo::7x5 undef
Paper::Specs::photo::8x10 undef
Paper::Specs::photo::8x12 undef
Paper::Specs::photo::8x6 undef
Paper::Specs::photo::9x6 undef
Paper::Specs::standard undef
Paper::Specs::standard::2a0 undef
Paper::Specs::standard::4a0 undef
Paper::Specs::standard::a undef
Paper::Specs::standard::a0 undef
Paper::Specs::standard::a1 undef
Paper::Specs::standard::a10 undef
Paper::Specs::standard::a2 undef
Paper::Specs::standard::a3 undef
Paper::Specs::standard::a4 undef
Paper::Specs::standard::a5 undef
Paper::Specs::standard::a6 undef
Paper::Specs::standard::a7 undef
Paper::Specs::standard::a8 undef
Paper::Specs::standard::a9 undef
Paper::Specs::standard::b undef
Paper::Specs::standard::b0 undef
Paper::Specs::standard::b0_jis undef
Paper::Specs::standard::b1 undef
Paper::Specs::standard::b10 undef
Paper::Specs::standard::b10_jis undef
Paper::Specs::standard::b1_jis undef
Paper::Specs::standard::b2 undef
Paper::Specs::standard::b2_jis undef
Paper::Specs::standard::b3 undef
Paper::Specs::standard::b3_jis undef
Paper::Specs::standard::b4 undef
Paper::Specs::standard::b4_envelope undef
Paper::Specs::standard::b4_jis undef
Paper::Specs::standard::b5 undef
Paper::Specs::standard::b5_envelope undef
Paper::Specs::standard::b5_jis undef
Paper::Specs::standard::b6 undef
Paper::Specs::standard::b6_envelope undef
Paper::Specs::standard::b6_jis undef
Paper::Specs::standard::b7 undef
Paper::Specs::standard::b7_jis undef
Paper::Specs::standard::b8 undef
Paper::Specs::standard::b8_jis undef
Paper::Specs::standard::b9 undef
Paper::Specs::standard::b9_jis undef
Paper::Specs::standard::c undef
Paper::Specs::standard::c0 undef
Paper::Specs::standard::c1 undef
Paper::Specs::standard::c10 undef
Paper::Specs::standard::c2 undef
Paper::Specs::standard::c3 undef
Paper::Specs::standard::c3_envelope undef
Paper::Specs::standard::c4 undef
Paper::Specs::standard::c4_envelope undef
Paper::Specs::standard::c5 undef
Paper::Specs::standard::c5_envelope undef
Paper::Specs::standard::c6 undef
Paper::Specs::standard::c6_c5_envelope undef
Paper::Specs::standard::c6_envelope undef
Paper::Specs::standard::c7 undef
Paper::Specs::standard::c8 undef
Paper::Specs::standard::c9 undef
Paper::Specs::standard::d undef
Paper::Specs::standard::dl_envelope undef
Paper::Specs::standard::e undef
Paper::Specs::standard::e4_envelope undef
Paper::Specs::standard::id_1 undef
Paper::Specs::standard::id_2 undef
Paper::Specs::standard::legal undef
Paper::Specs::standard::letter undef
Paper::Specs::standard::monarch_envelope undef
Paper::Specs::standard::no10_envelope undef
Paper::Specs::standard::no11_envelope undef
Paper::Specs::standard::no12_envelope undef
Paper::Specs::standard::no14_envelope undef
Paper::Specs::standard::no7_envelope undef
Paper::Specs::standard::no9_envelope undef
Paper::Specs::standard::ra0 undef
Paper::Specs::standard::ra1 undef
Paper::Specs::standard::ra2 undef
Paper::Specs::standard::ra3 undef
Paper::Specs::standard::ra4 undef
Paper::Specs::standard::sra0 undef
Paper::Specs::standard::sra1 undef
Paper::Specs::standard::sra2 undef
Paper::Specs::standard::sra3 undef
Paper::Specs::standard::sra4 undef
Paper::Specs::standard::tabloid undef
Parallel::Forker::Process 1.232
Parallel::Prefork::SpareWorkers undef
Parallel::Prefork::SpareWorkers::Scoreboard undef
Parallel::Scoreboard::PSGI::App undef
Parallel::Scoreboard::PSGI::App::JSON undef
Parallel::SubFork::Task 0.08
Paranoid::Args 0.22
Class::EHierarchy
Paranoid::BerkeleyDB 0.83
Paranoid::Data 0.02
Paranoid::Debug 0.93
Paranoid::Filesystem 0.19
Paranoid::Glob 0.2
Paranoid::Input 0.19
Paranoid::Lockfile 0.62
Paranoid::Log 0.14
Paranoid::Log::Buffer 0.83
Paranoid::Log::Email 0.82
Paranoid::Log::File 0.83
Paranoid::Log::Syslog 0.83
Paranoid::Module 0.81
Paranoid::Network 0.61
Paranoid::Process 1.01
Parse::AFP::BAG undef
Parse::AFP::BBC undef
Parse::AFP::BCA undef
Parse::AFP::BCF undef
Parse::AFP::BCP undef
Parse::AFP::BDA undef
Parse::AFP::BDD undef
Parse::AFP::BDG undef
Parse::AFP::BDI undef
Parse::AFP::BDM undef
Parse::AFP::BDT undef
Parse::AFP::BDX undef
Parse::AFP::BFG undef
Parse::AFP::BFM undef
Parse::AFP::BFN undef
Parse::AFP::BGR undef
Parse::AFP::BII undef
Parse::AFP::BIM undef
Parse::AFP::BMM undef
Parse::AFP::BMO undef
Parse::AFP::BNG undef
Parse::AFP::BOC undef
Parse::AFP::BOG undef
Parse::AFP::BPG undef
Parse::AFP::BPM undef
Parse::AFP::BPS undef
Parse::AFP::BPT undef
Parse::AFP::BR undef
Parse::AFP::BRG undef
Parse::AFP::BSG undef
Parse::AFP::Base undef
Parse::AFP::CAT undef
Parse::AFP::CDD undef
Parse::AFP::CFC undef
Parse::AFP::CFI undef
Parse::AFP::CPC undef
Parse::AFP::CPD undef
Parse::AFP::CPI undef
Parse::AFP::CTC undef
Parse::AFP::DXD undef
Parse::AFP::EAG undef
Parse::AFP::EBC undef
Parse::AFP::ECA undef
Parse::AFP::ECF undef
Parse::AFP::ECP undef
Parse::AFP::EDG undef
Parse::AFP::EDI undef
Parse::AFP::EDM undef
Parse::AFP::EDT undef
Parse::AFP::EDX undef
Parse::AFP::EFG undef
Parse::AFP::EFM undef
Parse::AFP::EFN undef
Parse::AFP::EGR undef
Parse::AFP::EII undef
Parse::AFP::EIM undef
Parse::AFP::EMM undef
Parse::AFP::EMO undef
Parse::AFP::ENG undef
Parse::AFP::EOC undef
Parse::AFP::EOG undef
Parse::AFP::EPG undef
Parse::AFP::EPM undef
Parse::AFP::EPS undef
Parse::AFP::EPT undef
Parse::AFP::ER undef
Parse::AFP::ERG undef
Parse::AFP::ESG undef
Parse::AFP::FGD undef
Parse::AFP::FNC undef
Parse::AFP::FND undef
Parse::AFP::FNG undef
Parse::AFP::FNI undef
Parse::AFP::FNM undef
Parse::AFP::FNN undef
Parse::AFP::FNO undef
Parse::AFP::FNP undef
Parse::AFP::GAD undef
Parse::AFP::GDD undef
Parse::AFP::ICP undef
Parse::AFP::IDD undef
Parse::AFP::IEL undef
Parse::AFP::IID undef
Parse::AFP::IMM undef
Parse::AFP::IOB undef
Parse::AFP::IOC undef
Parse::AFP::IPD undef
Parse::AFP::IPG undef
Parse::AFP::IPO undef
Parse::AFP::IPS undef
Parse::AFP::IRD undef
Parse::AFP::LLE undef
Parse::AFP::LNC undef
Parse::AFP::LND undef
Parse::AFP::MBC undef
Parse::AFP::MCA undef
Parse::AFP::MCC undef
Parse::AFP::MCD undef
Parse::AFP::MCF undef
Parse::AFP::MCF1 undef
Parse::AFP::MCF1::DataGroup undef
Parse::AFP::MCF::DataGroup undef
Parse::AFP::MDD undef
Parse::AFP::MDR undef
Parse::AFP::MFC undef
Parse::AFP::MGO undef
Parse::AFP::MIO undef
Parse::AFP::MMC undef
Parse::AFP::MMO undef
Parse::AFP::MMT undef
Parse::AFP::MPG undef
Parse::AFP::MPO undef
Parse::AFP::MPS undef
Parse::AFP::MSU undef
Parse::AFP::NOP undef
Parse::AFP::OBD undef
Parse::AFP::OBP undef
Parse::AFP::OCD undef
Parse::AFP::PFC undef
Parse::AFP::PGD undef
Parse::AFP::PGP undef
Parse::AFP::PGP1 undef
Parse::AFP::PMC undef
Parse::AFP::PTD undef
Parse::AFP::PTD1 undef
Parse::AFP::PTX undef
Parse::AFP::PTX::AMB undef
Parse::AFP::PTX::AMI undef
Parse::AFP::PTX::BLN undef
Parse::AFP::PTX::BSU undef
Parse::AFP::PTX::ControlSequence undef
Parse::AFP::PTX::DBR undef
Parse::AFP::PTX::DIR undef
Parse::AFP::PTX::ESU undef
Parse::AFP::PTX::NOP undef
Parse::AFP::PTX::RMB undef
Parse::AFP::PTX::RMI undef
Parse::AFP::PTX::RPS undef
Parse::AFP::PTX::SBI undef
Parse::AFP::PTX::SCFL undef
Parse::AFP::PTX::SIA undef
Parse::AFP::PTX::SIM undef
Parse::AFP::PTX::STC undef
Parse::AFP::PTX::STO undef
Parse::AFP::PTX::SVI undef
Parse::AFP::PTX::TRN undef
Parse::AFP::Record undef
Parse::AFP::TLE undef
Parse::AFP::Triplet undef
Parse::AFP::Triplet::AD undef
Parse::AFP::Triplet::AQ undef
Parse::AFP::Triplet::AV undef
Parse::AFP::Triplet::C undef
Parse::AFP::Triplet::CF undef
Parse::AFP::Triplet::CGCSGI undef
Parse::AFP::Triplet::CR undef
Parse::AFP::Triplet::CS undef
Parse::AFP::Triplet::DP undef
Parse::AFP::Triplet::EF undef
Parse::AFP::Triplet::ERLI undef
Parse::AFP::Triplet::ESI undef
Parse::AFP::Triplet::FCGCSGI undef
Parse::AFP::Triplet::FDS undef
Parse::AFP::Triplet::FF undef
Parse::AFP::Triplet::FHSF undef
Parse::AFP::Triplet::FO undef
Parse::AFP::Triplet::FQN undef
Parse::AFP::Triplet::FRMT undef
Parse::AFP::Triplet::II undef
Parse::AFP::Triplet::LDOPM undef
Parse::AFP::Triplet::LDTS undef
Parse::AFP::Triplet::MA undef
Parse::AFP::Triplet::MEC undef
Parse::AFP::Triplet::MF undef
Parse::AFP::Triplet::MIS undef
Parse::AFP::Triplet::MMPN undef
Parse::AFP::Triplet::MO undef
Parse::AFP::Triplet::MOR undef
Parse::AFP::Triplet::OAMU undef
Parse::AFP::Triplet::OAS undef
Parse::AFP::Triplet::OBE undef
Parse::AFP::Triplet::OBO undef
Parse::AFP::Triplet::OCH undef
Parse::AFP::Triplet::OCL undef
Parse::AFP::Triplet::OCO undef
Parse::AFP::Triplet::OFSS undef
Parse::AFP::Triplet::OO undef
Parse::AFP::Triplet::OOI undef
Parse::AFP::Triplet::OSFE undef
Parse::AFP::Triplet::OSFO undef
Parse::AFP::Triplet::PC undef
Parse::AFP::Triplet::POCP undef
Parse::AFP::Triplet::PPI undef
Parse::AFP::Triplet::PSMR undef
Parse::AFP::Triplet::PSRM undef
Parse::AFP::Triplet::PV undef
Parse::AFP::Triplet::RLI undef
Parse::AFP::Triplet::ROI undef
Parse::AFP::Triplet::ROT undef
Parse::AFP::Triplet::RSN undef
Parse::AFP::Triplet::RUA undef
Parse::AFP::Triplet::T1CRMT undef
Parse::Template 3.07
Parse::AFP::Triplet::T2FRMT undef
Parse::AFP::Triplet::TO undef
Parse::AFP::Triplet::TS undef
Parse::AFP::Triplet::UDTS undef
Parse::ALex 2.20
Parse::BACKPAN::Packages 0.40
Parse::BBCode::HTML 0.04
Parse::BBCode::Markdown 0.02
Parse::BBCode::Tag 0.02
Parse::BBCode::XHTML 0.01
Parse::Binary::FixedFormat::Variants 0.03
Parse::CLex 2.20
Parse::CPAN::Authors::Author undef
Parse::CPAN::Packages::Distribution undef
Parse::CPAN::Packages::Package undef
Parse::CPAN::Whois::Author 0.01
Parse::Dia::SQL::Const undef
Parse::Dia::SQL::Logger undef
Parse::Dia::SQL::Output undef
Parse::Dia::SQL::Output::DB2 undef
Parse::Dia::SQL::Output::Informix undef
Parse::Dia::SQL::Output::Ingres undef
Parse::Dia::SQL::Output::MySQL undef
Perl::Critic::DynamicPolicy 0.05
Parse::Dia::SQL::Output::MySQL::InnoDB undef
Parse::Dia::SQL::Output::MySQL::MyISAM undef
Parse::Dia::SQL::Output::Oracle undef
Parse::Dia::SQL::Output::Postgres undef
Parse::Dia::SQL::Output::SQLite3 undef
Parse::Dia::SQL::Output::Sas undef
Parse::Dia::SQL::Output::Sybase undef
Parse::Dia::SQL::Utils undef
Parse::ErrorString::Perl::ErrorItem 0.15
Parse::ErrorString::Perl::StackItem 0.15
Parse::Eyapp::Base undef
Parse::Eyapp::Cleaner undef
Parse::Eyapp::Driver 1.181
Parse::Eyapp::Grammar undef
Parse::Eyapp::Lalr undef
Parse::Eyapp::Node undef
Parse::Eyapp::Options undef
Parse::Eyapp::Output undef
Parse::Eyapp::Parse undef
Parse::Eyapp::Scope undef
Parse::Eyapp::TokenGen undef
Parse::Eyapp::Treeregexp undef
Parse::Eyapp::Unify undef
Parse::Eyapp::YATW undef
Parse::Eyapp::_TreeregexpSupport undef
Parse::FSM::Driver 1.02
Parse::Yapp::Driver 1.05
Parse::FSM::Lexer 1.02
Parse::HTTP::UserAgent::Base::Accessors 0.20
Parse::HTTP::UserAgent::Base::Dumper 0.20
Parse::HTTP::UserAgent::Base::IS 0.20
Parse::Lex 2.20
Parse::HTTP::UserAgent::Base::Parsers 0.20
Parse::HTTP::UserAgent::Constants 0.20
Parse::LexEvent 2.20
Parse::MediaWikiDump::CategoryLinks 1.0.3
Parse::MediaWikiDump::Links 1.0.6
Parse::MediaWikiDump::Pages 1.0.4
Parse::MediaWikiDump::Revisions 1.0.4
Parse::MediaWikiDump::category_link 1.0.3
Parse::MediaWikiDump::link 1.0.3
Parse::MediaWikiDump::page 1.0.4
Parse::Method::Signatures::Param undef
Parse::Method::Signatures::Param::Bindable undef
Parse::Method::Signatures::Param::Positional undef
Parse::Method::Signatures::Param::Unpacked undef
Parse::Method::Signatures::Param::Unpacked::Array undef
Parse::Method::Signatures::Param::Unpacked::Hash undef
Parse::Method::Signatures::ParamCollection undef
Parse::Method::Signatures::Sig undef
Parse::RecDescent::FAQ::Original 6.0.j
Parse::Stallion::CSV 0.5
Parse::Stallion::EBNF 0.7
Parse::Stallion::RD 0.41
Parse::Token 2.20
Parse::Trace 2.20
Parse::Win32Registry::Base undef
Parse::Win32Registry::Entry undef
Parse::Win32Registry::File undef
Parse::Win32Registry::Key undef
Parse::Win32Registry::Value undef
Parse::Win32Registry::Win95::File undef
Parse::Win32Registry::Win95::Key undef
Parse::Win32Registry::Win95::Value undef
Parse::Win32Registry::WinNT::Entry undef
Parse::Win32Registry::WinNT::File undef
Parse::Win32Registry::WinNT::Key undef
Parse::Win32Registry::WinNT::Security undef
Parse::Win32Registry::WinNT::Value undef
Parse::YYLex 2.20
Parse::Yapp::Grammar undef
Parse::Yapp::Lalr undef
Parse::Yapp::Options undef
Parse::Yapp::Output undef
Parse::Yapp::Parse undef
Path::Abstract::Fast undef
Path::Abstract::Underload 0.096
Path::Class::Dir 0.23
Path::Class::Entity 0.23
Path::Class::File 0.23
Path::Dispatcher::Declarative::Builder undef
Path::Dispatcher::Dispatch undef
Path::Dispatcher::Match undef
Path::Dispatcher::Path undef
Path::Dispatcher::Role::Rules undef
Path::Dispatcher::Rule undef
Path::Dispatcher::Rule::Alternation undef
Path::Dispatcher::Rule::Always undef
Path::Dispatcher::Rule::Chain undef
Path::Dispatcher::Rule::CodeRef undef
Path::Dispatcher::Rule::Dispatch undef
Path::Dispatcher::Rule::Empty undef
Path::Dispatcher::Rule::Enum undef
Path::Dispatcher::Rule::Eq undef
Path::Dispatcher::Rule::Intersection undef
Path::Dispatcher::Rule::Metadata undef
Path::Dispatcher::Rule::Regex undef
Path::Dispatcher::Rule::Sequence undef
Path::Dispatcher::Rule::Tokens undef
Path::Dispatcher::Rule::Under undef
Path::Extended::Class undef
Path::Extended::Class::Dir undef
Path::Extended::Class::File undef
Path::Extended::Dir undef
Path::Extended::Entity undef
Path::Extended::File undef
Path::Resolver::CustomConverter 3.100451
Path::Resolver::Resolver::AnyDist 3.100451
Path::Resolver::Resolver::Archive::Tar 3.100451
Path::Resolver::Resolver::DataSection 3.100451
Path::Resolver::Resolver::DistDir 3.100451
Path::Resolver::Resolver::FileSystem 3.100451
Path::Resolver::Resolver::Hash 3.100451
Path::Resolver::Resolver::Mux::Ordered 3.100451
Path::Resolver::Resolver::Mux::Prefix 3.100451
Path::Resolver::Role::Converter 3.100451
Path::Resolver::Role::FileResolver 3.100451
Path::Resolver::Role::Resolver 3.100451
Path::Resolver::SimpleEntity 3.100451
Path::Resolver::Types 3.100451
Path::Router::Route 0.10
Path::Router::Route::Match 0.10
Path::Router::Shell 0.10
Path::Router::Types 0.10
Patterns::ChainOfResponsibility::Application undef
Patterns::ChainOfResponsibility::Broadcast undef
Patterns::ChainOfResponsibility::Filter undef
Patterns::ChainOfResponsibility::Provider undef
Patterns::ChainOfResponsibility::Role::Dispatcher undef
Patterns::ChainOfResponsibility::Role::Handler undef
Peco::Container::Abstract undef
Peco::Container::Autoload undef
Peco::Container::Chained undef
Peco::Container::Clonable undef
Peco::Spec undef
Perl6::Junction::All 1.40000
Perl6::Junction::Any 1.40000
Perl6::Perldoc 0.000_006
Perl6::Junction::Base 1.40000
Perl6::Junction::None 1.40000
Perl6::Junction::One 1.40000
Perl6::Perldoc::Parser 0.0.6
Perl6::Perldoc::To::Text undef
Perl6::Perldoc::To::Xhtml undef
Perl6::Pod::Block undef
Perl6::Pod::Block::code undef
Perl6::Pod::Block::comment undef
Perl6::Pod::Block::format undef
Perl6::Pod::Block::input undef
Perl6::Pod::Block::item undef
Perl6::Pod::Block::nested undef
Perl6::Pod::Block::output undef
Perl6::Pod::Block::para undef
Perl6::Pod::Block::pod undef
Perl6::Pod::Block::table undef
Perl6::Pod::Directive::alias undef
Perl6::Pod::Directive::config undef
Perl6::Pod::Directive::use undef
Perl6::Pod::FormattingCode undef
Perl6::Pod::FormattingCode::A undef
Perl6::Pod::FormattingCode::B undef
Perl6::Pod::FormattingCode::C undef
Perl6::Pod::FormattingCode::D undef
Perl6::Pod::FormattingCode::E undef
Perl6::Pod::FormattingCode::I undef
Perl6::Pod::FormattingCode::K undef
Perl6::Pod::FormattingCode::L undef
Perl6::Pod::FormattingCode::M undef
Perl6::Pod::FormattingCode::N undef
Perl6::Pod::FormattingCode::P undef
Perl6::Pod::FormattingCode::R undef
Perl6::Pod::FormattingCode::S undef
Perl6::Pod::FormattingCode::T undef
Perl6::Pod::FormattingCode::U undef
Perl6::Pod::FormattingCode::X undef
Perl6::Pod::FormattingCode::Z undef
Perl6::Pod
Perl6::Pod::Lib::Image undef
Perl6::Pod::Lib::Include undef
Perl6::Pod::Parser undef
Perl6::Pod::Parser::AddHeadLevels undef
Perl6::Pod::Parser::AddIds undef
Perl6::Pod::Parser::Context undef
Perl6::Pod::Parser::CustomCodes undef
Perl6::Pod::Parser::Doallow undef
Perl6::Pod::Parser::Doformatted undef
Perl6::Pod::Parser::FilterPattern undef
Perl6::Pod::Parser::ListLevels undef
Perl6::Pod::Parser::NOTES undef
Perl6::Pod::Parser::NestedAttr undef
Perl6::Pod::Parser::Pod2Events undef
Perl6::Pod::Parser::Utils undef
Perl6::Pod::Test undef
Perl6::Pod::To undef
Perl6::Pod::To::DocBook undef
Perl6::Pod::To::DocBook::ProcessHeads undef
Perl6::Pod::To::Mem undef
Perl6::Pod::To::XHTML undef
Perl6::Pod::To::XHTML::MakeBody undef
Perl6::Pod::To::XHTML::MakeHead undef
Perl6::Pod::To::XHTML::ProcessHeadings undef
Perl6::Pod::To::XML undef
Perl::APIReference::Generator 0.01
Perl::APIReference::V5_006_000 undef
Perl::APIReference::V5_006_001 undef
Perl::APIReference::V5_006_002 undef
Perl::APIReference::V5_008_000 undef
Perl::APIReference::V5_008_001 undef
Perl::APIReference::V5_008_002 undef
Perl::APIReference::V5_008_003 undef
Perl::APIReference::V5_008_004 undef
Perl::APIReference::V5_008_005 undef
Perl::APIReference::V5_008_006 undef
Perl::APIReference::V5_008_007 undef
Perl::APIReference::V5_008_008 undef
Perl::APIReference::V5_008_009 undef
Perl::APIReference::V5_010_000 undef
Perl::APIReference::V5_010_001 undef
Perl::APIReference::V5_011_000 undef
Perl::APIReference::V5_011_001 undef
Perl::APIReference::V5_011_002 undef
Perl::APIReference::V5_012_000 undef
Perl::Critic::Annotation 1.115
Perl::Critic::Command 1.115
Perl::Critic::Exception 1.115
Perl::Critic::Exception::AggregateConfiguration 1.115
Perl::Critic::Exception::Configuration 1.115
Perl::Critic::Exception::Configuration::Generic 1.115
Perl::Critic::Exception::Configuration::NonExistentPolicy 1.115
Perl::Critic::Exception::Configuration::Option 1.115
Perl::Critic::Exception::Configuration::Option::Global 1.115
Perl::Critic::Exception::Configuration::Option::Global::ExtraParameter 1.115
Perl::Critic::Exception::Configuration::Option::Global::ParameterValue 1.115
Perl::Critic::Exception::Configuration::Option::Policy 1.115
Perl::Critic::Exception::Configuration::Option::Policy::ExtraParameter 1.115
Perl::Critic::Exception::Configuration::Option::Policy::ParameterValue 1.115
Perl::Critic::Exception::Fatal 1.115
Perl::Critic::Exception::Fatal::Generic 1.115
Perl::Critic::Exception::Fatal::Internal 1.115
Perl::Critic::Exception::Fatal::PolicyDefinition 1.115
Perl::Critic::Exception::IO 1.115
Perl::Critic::Exception::Parse 1.115
Perl::Critic::OptionsProcessor 1.115
Perl::Critic::Policy::Bangs::ProhibitCommentedOutCode 1.06
Perl::Critic::Policy::Bangs::ProhibitFlagComments 1.06
Perl::Critic::Policy::Bangs::ProhibitNoPlan 1.06
Perl::Critic::Policy::Bangs::ProhibitNumberedNames 1.06
Perl::Critic::Policy::Bangs::ProhibitRefProtoOrProto 1.06
Perl::Critic::Policy::Bangs::ProhibitUselessRegexModifiers 1.06
Perl::Critic::Policy::Bangs::ProhibitVagueNames 1.06
Perl::Critic::Policy::BuiltinFunctions::ProhibitBooleanGrep 1.115
Perl::Critic::Policy::BuiltinFunctions::ProhibitComplexMappings 1.115
Perl::Critic::Policy::BuiltinFunctions::ProhibitLvalueSubstr 1.115
Perl::Critic::Policy::BuiltinFunctions::ProhibitReverseSortBlock 1.115
Perl::Critic::Policy::BuiltinFunctions::ProhibitSleepViaSelect 1.115
Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval 1.115
Perl::Critic::Policy::BuiltinFunctions::ProhibitStringySplit 1.115
Perl::Critic::Policy::BuiltinFunctions::ProhibitUniversalCan 1.115
Perl::Critic::Policy::BuiltinFunctions::ProhibitUniversalIsa 1.115
Perl::Critic::Policy::BuiltinFunctions::ProhibitVoidGrep 1.115
Perl::Critic::Policy::BuiltinFunctions::ProhibitVoidMap 1.115
Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep 1.115
Perl::Critic::Policy::BuiltinFunctions::RequireBlockMap 1.115
Perl::Critic::Policy::BuiltinFunctions::RequireGlobFunction 1.115
Perl::Critic::Policy::BuiltinFunctions::RequireSimpleSortBlock 1.115
Perl::Critic::Policy::ClassHierarchies::ProhibitAutoloading 1.115
Perl::Critic::Policy::ClassHierarchies::ProhibitExplicitISA 1.115
Perl::Critic::Policy::ClassHierarchies::ProhibitOneArgBless 1.115
Perl::Critic::Policy::CodeLayout::ProhibitHardTabs 1.115
Perl::Critic::Policy::CodeLayout::ProhibitParensWithBuiltins 1.115
Perl::Critic::Policy::CodeLayout::ProhibitQuotedWordLists 1.115
Perl::Critic::Policy::CodeLayout::ProhibitTrailingWhitespace 1.115
Perl::Critic::Policy::CodeLayout::RequireASCII 1.000
Perl::Critic::Policy::CodeLayout::RequireConsistentNewlines 1.115
Perl::Critic::Policy::CodeLayout::RequireFinalSemicolon 57
Perl::Critic::Policy::CodeLayout::RequireTidyCode 1.115
Perl::Critic::Policy::CodeLayout::RequireTrailingCommas 1.115
Perl::Critic::Policy::CodeLayout::RequireUseUTF8 v1.0.3
Perl::Critic::Policy::Compatibility::ConstantLeadingUnderscore 57
Perl::Critic::Policy::Compatibility::ConstantPragmaHash 57
Perl::Critic::Policy::Compatibility::Gtk2Constants 57
Perl::Critic::Policy::Compatibility::PerlMinimumVersionAndWhy 57
Perl::Critic::Policy::Compatibility::PodMinimumVersion 57
Perl::Critic::Policy::Compatibility::ProhibitThreeArgumentOpen 1.001
Perl::Critic::Policy::Compatibility::ProhibitUnixDevNull 57
Perl::Critic::Policy::ControlStructures::ProhibitCStyleForLoops 1.115
Perl::Critic::Policy::ControlStructures::ProhibitCascadingIfElse 1.115
Perl::Critic::Policy::ControlStructures::ProhibitDeepNests 1.115
Perl::Critic::Policy::ControlStructures::ProhibitLabelsWithSpecialBlockNames 1.115
Perl::Critic::Policy::ControlStructures::ProhibitMutatingListFunctions 1.115
Perl::Critic::Policy::ControlStructures::ProhibitNegativeExpressionsInUnlessAndUntilConditions 1.115
Perl::Critic::Policy::ControlStructures::ProhibitPostfixControls 1.115
Perl::Critic::Policy::ControlStructures::ProhibitUnlessBlocks 1.115
Perl::Critic::Policy::ControlStructures::ProhibitUnreachableCode 1.115
Perl::Critic::Policy::ControlStructures::ProhibitUntilBlocks 1.115
Perl::Critic::Policy::Documentation::PodSpelling 1.115
Perl::Critic::Policy::Documentation::ProhibitAdjacentLinks 57
Perl::Critic::Policy::Documentation::ProhibitBadAproposMarkup 57
Perl::Critic::Policy::Documentation::ProhibitVerbatimMarkup 57
Perl::Critic::Policy::Documentation::RequireEndBeforeLastPod 57
Perl::Critic::Policy::Documentation::RequireLinkedURLs 57
Perl::Critic::Policy::Documentation::RequirePODUseEncodingUTF8 v1.0.3
Perl::Critic::Policy::Documentation::RequirePackageMatchesPodName 1.115
Perl::Critic::Policy::Documentation::RequirePodAtEnd 1.115
Perl::Critic::Policy::Documentation::RequirePodLinksIncludeText 1.115
Perl::Critic::Policy::Documentation::RequirePodSections 1.115
Perl::Critic::Policy::Dynamic::ValidateAgainstSymbolTable 0.05
Perl::Critic::Policy::Editor::RequireEmacsFileVariables 1.000
Perl::Critic::Policy::ErrorHandling::RequireCarping 1.115
Perl::Critic::Policy::ErrorHandling::RequireCheckingReturnValueOfEval 1.115
Perl::Critic::Policy::ErrorHandling::RequireUseOfExceptions 1.000
Perl::Critic::Policy::InputOutput::ProhibitBacktickOperators 1.115
Perl::Critic::Policy::InputOutput::ProhibitBarewordFileHandles 1.115
Perl::Critic::Policy::InputOutput::ProhibitExplicitStdin 1.115
Perl::Critic::Policy::InputOutput::ProhibitInteractiveTest 1.115
Perl::Critic::Policy::InputOutput::ProhibitJoinedReadline 1.115
Perl::Critic::Policy::InputOutput::ProhibitOneArgSelect 1.115
Perl::Critic::Policy::InputOutput::ProhibitReadlineInForLoop 1.115
Perl::Critic::Policy::InputOutput::ProhibitTwoArgOpen 1.115
Perl::Critic::Policy::InputOutput::RequireBracedFileHandleWithPrint 1.115
Perl::Critic::Policy::InputOutput::RequireBriefOpen 1.115
Perl::Critic::Policy::InputOutput::RequireCheckedClose 1.115
Perl::Critic::Policy::InputOutput::RequireCheckedOpen 1.115
Perl::Critic::Policy::InputOutput::RequireCheckedSyscalls 1.115
Perl::Critic::Policy::InputOutput::RequireEncodingWithUTF8Layer 1.115
Perl::Critic::Policy::Lax::ProhibitComplexMappings::LinesNotStatements 0.008
Perl::Critic::Policy::Lax::ProhibitEmptyQuotes::ExceptAsFallback 0.008
Perl::Critic::Policy::Lax::ProhibitLeadingZeros::ExceptChmod 0.008
Perl::Critic::Policy::Lax::ProhibitStringyEval::ExceptForRequire 0.008
Perl::Critic::Policy::Lax::RequireEndWithTrueConst 0.008
Perl::Critic::Policy::Lax::RequireExplicitPackage::ExceptForPragmata 0.008
Perl::Critic::Policy::Miscellanea::ProhibitFormats 1.115
Perl::Critic::Policy::Miscellanea::ProhibitTies 1.115
Perl::Critic::Policy::Miscellanea::ProhibitUnrestrictedNoCritic 1.115
Perl::Critic::Policy::Miscellanea::ProhibitUselessNoCritic 1.115
Perl::Critic::Policy::Miscellanea::TextDomainPlaceholders 57
Perl::Critic::Policy::Miscellanea::TextDomainUnused 57
Perl::Critic::Policy::Modules::PerlMinimumVersion 1.000
Perl::Critic::Policy::Modules::ProhibitAutomaticExportation 1.115
Perl::Critic::Policy::Modules::ProhibitConditionalUseStatements 1.115
Perl::Critic::Policy::Modules::ProhibitEvilModules 1.115
Perl::Critic::Policy::Modules::ProhibitExcessMainComplexity 1.115
Perl::Critic::Policy::Modules::ProhibitModuleShebang 57
Perl::Critic::Policy::Modules::ProhibitMultiplePackages 1.115
Perl::Critic::Policy::Modules::ProhibitPOSIXimport 57
Perl::Critic::Policy::Modules::ProhibitUseQuotedVersion 57
Perl::Critic::Policy::Objects::ProhibitIndirectSyntax 1.115
Perl::Critic::Policy::Modules::RequireBarewordIncludes 1.115
Perl::Critic::Policy::Modules::RequireEndWithOne 1.115
Perl::Critic::Policy::Modules::RequireExplicitInclusion 0.03
Perl::Critic::Policy::Modules::RequireExplicitPackage 1.115
Perl::Critic::Policy::Modules::RequireFilenameMatchesPackage 1.115
Perl::Critic::Policy::Modules::RequireNoMatchVarsWithUseEnglish 1.115
Perl::Critic::Policy::Modules::RequirePerlVersion 1.000
Perl::Critic::Policy::Modules::RequireVersionVar 1.115
Perl::Critic::Policy::NamingConventions::Capitalization 1.115
Perl::Critic::Policy::NamingConventions::ProhibitAmbiguousNames 1.115
Perl::Critic::Policy::NamingConventions::ProhibitMixedCaseSubs 1.108
Perl::Critic::Policy::NamingConventions::ProhibitMixedCaseVars 1.108
Perl::Critic::OTRS 0.01
Perl::Critic::Policy::Subroutines::ProhibitAmpersandSigils 1.115
Perl::Critic::Policy::References::ProhibitDoubleSigils 1.115
Perl::Critic::Policy::RegularExpressions::ProhibitCaptureWithoutTest 1.115
Perl::Critic::Policy::RegularExpressions::ProhibitComplexRegexes 1.115
Perl::Critic::Policy::RegularExpressions::ProhibitEnumeratedClasses 1.115
Perl::Critic::Policy::RegularExpressions::ProhibitEscapedMetacharacters 1.115
Perl::Critic::Policy::RegularExpressions::ProhibitFixedStringMatches 1.115
Perl::Critic::Policy::RegularExpressions::ProhibitSingleCharAlternation 1.115
Perl::Critic::Policy::RegularExpressions::ProhibitUnusedCapture 1.115
Perl::Critic::Policy::RegularExpressions::ProhibitUnusualDelimiters 1.115
Perl::Critic::Policy::RegularExpressions::RequireBracesForMultiline 1.115
Perl::Critic::Policy::RegularExpressions::RequireDotMatchAnything 1.115
Perl::Critic::Policy::RegularExpressions::RequireExtendedFormatting 1.115
Perl::Critic::Policy::RegularExpressions::RequireLineBoundaryMatching 1.115
Perl::Critic::Policy::Storable::ProhibitStoreOrFreeze 0.01
Perl::Critic::Policy::Subroutines::ProhibitBuiltinHomonyms 1.115
Perl::Critic::Policy::Subroutines::ProhibitCallsToUndeclaredSubs 0.03
Perl::Critic::Policy::Subroutines::ProhibitCallsToUnexportedSubs 0.03
Perl::Critic::Policy::Subroutines::ProhibitExcessComplexity 1.115
Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef 1.115
Perl::Critic::Policy::Subroutines::ProhibitExportingUndeclaredSubs 0.03
Perl::Critic::Policy::Subroutines::ProhibitManyArgs 1.115
Perl::Critic::Policy::Subroutines::ProhibitNestedSubs 1.115
Perl::Critic::Policy::Subroutines::ProhibitQualifiedSubDeclarations 0.03
Perl::Critic::Policy::Subroutines::ProhibitReturnSort 1.115
Perl::Critic::Policy::Subroutines::ProhibitSubroutinePrototypes 1.115
Perl::Critic::Policy::Subroutines::ProhibitUnusedPrivateSubroutines 1.115
Perl::Critic::Policy::Subroutines::ProtectPrivateSubs 1.115
Perl::Critic::Policy::Subroutines::RequireArgUnpacking 1.115
Perl::Critic::Policy::Subroutines::RequireFinalReturn 1.115
Perl::Critic::Policy::TestingAndDebugging::ProhibitNoStrict 1.115
Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings 1.115
Perl::Critic::Policy::TestingAndDebugging::ProhibitProlongedStrictureOverride 1.115
Perl::Critic::Policy::TestingAndDebugging::RequireTestLabels 1.115
Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict 1.115
Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings 1.115
Perl::Critic::Policy::Tics::ProhibitLongLines 0.006
Perl::Critic::Policy::Tics::ProhibitManyArrows 0.006
Perl::Critic::Policy::Tics::ProhibitUseBase 0.006
Perl::Critic::Policy::ValuesAndExpressions::ConstantBeforeLt 57
Perl::Critic::Policy::ValuesAndExpressions::NotWithCompare 57
Perl::Critic::Policy::ValuesAndExpressions::ProhibitAccessOfPrivateData v1.0.0
Perl::Critic::Policy::ValuesAndExpressions::ProhibitBarewordDoubleColon 57
Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitComplexVersion 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitEmptyCommas 57
Perl::Critic::Policy::ValuesAndExpressions::ProhibitEmptyQuotes 1.115
Perl::Critic::Policy::Variables::ProhibitConditionalDeclarations 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitEscapedCharacters 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitFiletest_f 57
Perl::Critic::Policy::ValuesAndExpressions::ProhibitImplicitNewlines 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitInterpolationOfLiterals 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitLongChainsOfMethodCalls 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitMagicNumbers 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitMismatchedOperators 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitNoisyQuotes 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitNullStatements 57
Perl::Critic::Policy::ValuesAndExpressions::ProhibitQuotesAsQuotelikeOperatorDelimiters 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitSpecialLiteralHeredocTerminator 1.115
Perl::Critic::Policy::ValuesAndExpressions::ProhibitUnknownBackslash 57
Perl::Critic::Policy::ValuesAndExpressions::ProhibitVersionStrings 1.115
Perl::Critic::Policy::ValuesAndExpressions::RequireConstantVersion 1.115
Perl::Critic::Policy::ValuesAndExpressions::RequireInterpolationOfMetachars 1.115
Perl::Critic::Policy::ValuesAndExpressions::RequireNumberSeparators 1.115
Perl::Critic::Policy::ValuesAndExpressions::RequireNumericVersion 57
Perl::Critic::Policy::ValuesAndExpressions::RequireQuotedHeredocTerminator 1.115
Perl::Critic::Policy::ValuesAndExpressions::RequireUpperCaseHeredocTerminator 1.115
Perl::Critic::Policy::ValuesAndExpressions::RestrictLongStrings 1.000
Perl::Critic::Policy::ValuesAndExpressions::UnexpandedSpecialLiteral 57
Perl::Critic::Policy::Variables::ProhibitEvilVariables 1.115
Perl::Critic::Policy::Variables::ProhibitLocalVars 1.115
Perl::Critic::Policy::Variables::ProhibitMatchVars 1.115
Perl::Critic::Policy::Variables::ProhibitPackageVars 1.115
Perl::Critic::Policy::Variables::ProhibitPerl4PackageNames 1.115
Perl::Critic::Policy::Variables::ProhibitPunctuationVars 1.115
Perl::Critic::Policy::Variables::ProhibitReusedNames 1.115
Perl::Critic::Policy::Variables::ProhibitUnusedVariables 1.115
Perl::Critic::Policy::Variables::ProhibitUselessInitialization 0.01
Perl::Critic::Policy::Variables::ProtectPrivateVars 1.115
Perl::Critic::Policy::Variables::RequireInitializationForLocalVars 1.115
Perl::Critic::Policy::Variables::RequireLexicalLoopIterators 1.115
Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars 1.115
Perl::Critic::Policy::Variables::RequireNegativeIndices 1.115
Perl::Critic::PolicyConfig 1.115
Perl::Critic::PolicyListing 1.115
Perl::Critic::PolicyParameter::Behavior 1.115
Perl::Critic::PolicyParameter::Behavior::Boolean 1.115
Perl::Critic::PolicyParameter::Behavior::Enumeration 1.115
Perl::Critic::PolicyParameter::Behavior::Integer 1.115
Perl::Critic::PolicyParameter::Behavior::String 1.115
Perl::Critic::PolicyParameter::Behavior::StringList 1.115
Perl::Critic::ProfilePrototype 1.115
Perl::Critic::Pulp::PodParser 57
Perl::Critic::Pulp::Utils 57
Perl::Critic::Statistics 1.115
Perl::Critic::StricterSubs::Utils 0.03
Perl::Critic::Theme 1.115
Perl::Critic::ThemeListing 1.115
Perl::Critic::Utils::DataConversion 1.115
Perl::Critic::Utils::McCabe 1.115
Perl::Critic::Utils::POD 1.115
Perl::Critic::Utils::POD::ParseInteriorSequence 1.115
Perl::Critic::Utils::PPIRegexp 1.108
Perl::Critic::Utils::Perl 1.115
Statistics::Basic::Mean
Statistics::Basic::Median
Statistics::Basic::StdDev
Perl::Metrics::Simple::Analysis 0.15
Perl::Metrics::Simple::Analysis::File 0.15
Perl::MinimumVersion::Reason 1.28
Perl::PrereqScanner::Scanner 1.002
Perl::PrereqScanner::Scanner::Moose 1.002
Perl::PrereqScanner::Scanner::Perl5 1.002
Perl::PrereqScanner::Scanner::TestMore 1.002
Perl::Tags::ClassDot 2.0.0_15
Perl::Tags::ClassDot::Tag::Property 2.0.0_15
PerlIO 1.06
Regexp::Common::ANSIescape
PerlIO::via::EscStatus::Parser 10
PerlIO::via::EscStatus::ShowAll 10
PerlIO::via::EscStatus::ShowNone 10
PerlIO::encoding 0.12
PerlIO::scalar 0.08
PerlIO::via 0.09
PerlReq::Utils undef
XML::OPML::SimpleGen
Perlanet::Entry undef
Perlanet::Feed undef
Perlanet::Simple undef
Perlanet::Trait::Cache undef
Perlanet::Trait::FeedFile undef
Perlanet::Trait::OPML undef
Perlanet::Trait::Scrubber undef
Perlanet::Trait::TemplateToolkit undef
Perlanet::Trait::Tidy undef
Perlanet::Trait::YAMLConfig undef
Perlbal::AIO undef
Perlbal::BackendHTTP undef
Perlbal::Cache 1.0
Perlbal::ChunkedUploadState undef
Perlbal::ClientHTTP undef
Perlbal::ClientHTTPBase undef
Perlbal::ClientManage undef
Perlbal::ClientProxy undef
Perlbal::CommandContext undef
Perlbal::HTTPHeaders undef
Perlbal::ManageCommand undef
Perlbal::Plugin::AccessControl undef
Perlbal::Plugin::AutoRemoveLeadingDir undef
Perlbal::Plugin::Cgilike undef
Perlbal::Plugin::EchoService undef
Perlbal::Plugin::Extredirector v0.0.2
Perlbal::Plugin::Highpri undef
Perlbal::Plugin::Include undef
Perlbal::Plugin::LazyCDN undef
Perlbal::Plugin::MaxContentLength undef
Perlbal::Plugin::NotModified undef
Perlbal::Plugin::Palimg undef
Perlbal::Plugin::Queues undef
Perlbal::Plugin::Redirect undef
Perlbal::Plugin::Stats undef
Perlbal::Plugin::Vhosts undef
Perlbal::Plugin::Vpaths undef
Perlbal::Pool undef
Perlbal::ReproxyManager undef
Perlbal::Service undef
Perlbal::Socket undef
Perlbal::SocketSSL undef
Perlbal::TCPListener undef
Perlbal::Test undef
Perlbal::Test::WebClient undef
Perlbal::Test::WebServer undef
Perlbal::UploadListener undef
Perlbal::Util undef
Perlito::Expression undef
Perlito::Grammar undef
Perlito::Grammar::Regex undef
Perlito::Precedence undef
Perlito::Test undef
Persevere::Client::Class 0.01
Pg::CLI::Role::Command 0.03
Pg::CLI::Role::Connects 0.07
Pg::CLI::Role::Executable 0.07
Pg::CLI::Role::HasVersion 0.07
Pg::CLI::pg_config 0.07
Pg::CLI::pg_dump 0.03
Pg::CLI::psql 0.03
Pg::DatabaseManager::TestMigrations 0.05
HOP::Lexer
Pg::Explain::From 0.52
Pg::Explain::FromJSON 0.52
Pg::Explain::FromText 0.52
Pg::Explain::FromXML 0.52
Pg::Explain::FromYAML 0.52
Pg::Explain::Node 0.52
Plack::App::CGIBin undef
Plack::App::Cascade undef
Plack::App::FCGIDispatcher undef
Plack::App::PSGIBin undef
Plack::App::Proxy::Test undef
Plack::App::WrapCGI undef
Plack::Client::Backend 0.06
Plack::Client::Backend::http 0.06
Plack::Client::Backend::psgi_local 0.06
Plack::HTTPParser undef
Plack::HTTPParser::PP undef
Plack::Handler undef
Plack::Handler::Apache1 undef
Plack::Handler::Apache2 undef
Plack::Handler::Apache2::Registry undef
Plack::Handler::CGI undef
Plack::Handler::Corona undef
Plack::Handler::FCGI::Async 0.22
Plack::Handler::Feersum undef
Plack::Handler::HTTP::Server::PSGI undef
Plack::Handler::HTTP::Server::Simple undef
Plack::Handler::Net::FastCGI undef
Plack::Handler::Standalone undef
Plack::Handler::Starlet undef
Plack::Handler::Starman undef
Plack::Handler::Twiggy undef
Plack::Loader::Delayed undef
Plack::Loader::Restarter undef
Plack::Loader::Shotgun undef
Plack::Middleware::AccessLog undef
Plack::Middleware::AccessLog::Timed undef
Plack::Middleware::BufferedStreaming undef
Plack::Middleware::Chunked undef
Plack::Middleware::ConditionalGET undef
Plack::Middleware::ContentMD5 undef
Plack::Middleware::Debug::CatalystLog 0.12
Plack::Middleware::Debug::DBITrace 0.12
Plack::Middleware::Debug::Dancer::Logger 0.03
Plack::Middleware::Debug::Dancer::Routes 0.03
Plack::Middleware::Debug::Dancer::Session 0.03
Plack::Middleware::Debug::Dancer::Settings 0.03
Plack::Middleware::Debug::Dancer::Version 0.03
Plack::Middleware::Debug::Environment 0.12
Plack::Middleware::Debug::Memory 0.12
Plack::Middleware::Debug::ModuleVersions 0.12
Plack::Middleware::Debug::Panel undef
Plack::Middleware::Debug::Parameters undef
Plack::Middleware::Debug::PerlConfig 0.12
Plack::Middleware::Debug::Response 0.12
Plack::Middleware::Debug::Session undef
Plack::Middleware::Debug::Timer 0.12
Plack::Middleware::Debug::TrackObjects undef
Plack::Middleware::Dispatch undef
Plack::Middleware::DoormanAuthentication undef
Plack::Middleware::DoormanOpenID undef
Plack::Middleware::DoormanTwitter undef
Plack::Middleware::ErrorDocument undef
Plack::Middleware::Lint undef
Plack::Middleware::Log4perl undef
Plack::Middleware::LogDispatch undef
Plack::Middleware::NullLogger undef
Plack::Middleware::Proxy::AddVia 0.01
Plack::Middleware::Proxy::Connect 0.01
Plack::Middleware::Proxy::LoadBalancer 0.01
Plack::Middleware::Proxy::RewriteLocation undef
Plack::Middleware::RearrangeHeaders undef
Plack::Middleware::Recursive undef
Plack::Middleware::Refresh undef
Plack::Middleware::Reproxy::Callback undef
Plack::Middleware::Reproxy::Furl undef
Plack::Middleware::Runtime undef
Plack::Middleware::Session::Cookie undef
Plack::Middleware::SimpleContentFilter undef
Plack::Middleware::SimpleLogger undef
Plack::Middleware::SocketIO::Base undef
Plack::Middleware::SocketIO::Connection undef
Plack::Middleware::SocketIO::Handle undef
Plack::Middleware::SocketIO::Htmlfile undef
Plack::Middleware::SocketIO::JSONPPolling undef
Plack::Middleware::SocketIO::Resource undef
Plack::Middleware::SocketIO::WebSocket undef
Plack::Middleware::SocketIO::XHRMultipart undef
Plack::Middleware::SocketIO::XHRPolling undef
Plack::Middleware::StackTrace undef
Plack::Middleware::Static undef
Plack::Middleware::XFramework undef
Plack::Middleware::XSendfile undef
Plack::Server::AnyEvent::Writer undef
Plack::Server::Coro 0.1002
Plack::Session 0.14
Plack::Session::State 0.14
Plack::Session::State::Cookie 0.14
Plack::Session::Store 0.14
Plack::Session::Store::DBI 0.10
Plack::Session::Store::File 0.14
Plack::Session::Store::Null 0.14
Plack::TempBuffer undef
Plack::TempBuffer::Auto undef
Plack::TempBuffer::File undef
Plack::TempBuffer::PerlIO undef
Plack::Test::Adopt::Catalyst undef
Plack::Test::MockHTTP undef
Plack::Test::Server undef
Pod::Abstract::Filter 0.20
Pod::Abstract::Filter::add_podcmds 0.20
Pod::Abstract::Filter::clear_podcmds 0.20
Pod::Abstract::Filter::cut 0.20
Pod::Abstract::Filter::find 0.20
Pod::Abstract::Filter::number_sections 0.20
Pod::Abstract::Filter::overlay 0.20
Pod::Abstract::Filter::sort 0.20
Pod::Abstract::Filter::summary 0.20
Pod::Abstract::Filter::uncut 0.20
Pod::Abstract::Filter::unoverlay 0.20
Pod::Abstract::Node 0.20
Pod::Abstract::Parser 0.20
Pod::Abstract::Path 0.20
Pod::Abstract::Serial 0.20
Pod::Abstract::Tree 0.20
Pod::CYOA::Transformer 0.001
Pod::CYOA::XHTML 0.001
Pod::Checker 1.45
Pod::Coverage::ExportOnly undef
Pod::Coverage::Overloader undef
Pod::Elemental::Autoblank 0.102360
Pod::Elemental::Element::Generic::Blank 0.102360
Pod::Elemental::Element::Generic::Command 0.102360
Pod::Elemental::Autochomp 0.102360
Pod::Elemental::Command 0.102360
Pod::Elemental::Element::Generic::Nonpod 0.102360
Pod::Elemental::Element::Generic::Text 0.102360
Pod::Elemental::Element::Pod5::Data 0.102360
Pod::Elemental::Node 0.102360
Pod::Elemental::Element::Pod5::Nonpod 0.102360
Pod::Elemental::Flat 0.102360
Pod::Elemental::Transformer 0.102360
Pod::Elemental::Objectifier 0.102360
Pod::Elemental::Paragraph 0.102360
Pod::Functions 1.04
Pod::Generated::Attributes 0.05
Pod::Html 1.09
Pod::Hyperlink::BounceURL 1.7
Pod::InputObjects 1.31
Pod::L10N::Html 0.07
Pod::Loom::Parser 0.03
Pod::Loom::Role::Extender 0.03
Pod::Loom::Template 0.03
Pod::Loom::Template::Default 0.04
Pod::Loom::Template::Identity 0.03
Pod::Man 2.25
Pod::Manual::Docbook2LaTeX 0.08
Pod::Manual::PodXML2Docbook 0.08
Pod::MinimumVersion::Parser 50
Pod::MinimumVersion::Report 50
Pod::POM::Constants 1.01
Pod::POM::Node 1.05
Pod::POM::Nodes 1.03
Pod::POM::Test 1.01
Pod::POM::View::Text 1.03
Pod::POM::View::Pod 1.03
Pod::POM::Web::Indexer undef
Pod::ParseLink 1.10
Pod::ParseUtils 1.36
Pod::Perldoc::BaseTo undef
Pod::Perldoc::GetOptsOO undef
Pod::Perldoc::ToChecker undef
Pod::Perldoc::ToMan undef
Pod::Perldoc::ToNroff undef
Pod::Perldoc::ToPod undef
Pod::Perldoc::ToRtf undef
Pod::Perldoc::ToText undef
Pod::Perldoc::ToTk undef
Pod::Perldoc::ToXml undef
Pod::PlainText 2.04
Pod::PseudoPod::Checker 0.17
Pod::PseudoPod::DocBook 0.17
Pod::PseudoPod::HTML 0.17
Pod::PseudoPod::Index 0.17
Pod::PseudoPod::Text 0.17
Pod::Select 1.36
Pod::Simple::BlackBox 3.16
Pod::Simple::Checker 3.16
Pod::Simple::Debug 3.16
Pod::Simple::HTML 3.16
Pod::Simple::DumpAsText 3.16
Pod::Simple::DumpAsXML 3.16
Pod::Simple::HTMLBatch 3.16
Pod::Simple::HTMLLegacy 5.01
Pod::Simple::LinkSection 3.16
Pod::Simple::Methody 3.16
Pod::Simple::Progress 3.16
Pod::Simple::PullParser 3.16
Pod::Simple::PullParserEndToken 3.16
Pod::Simple::PullParserStartToken 3.16
Pod::Simple::PullParserTextToken 3.16
Pod::Simple::PullParserToken 3.16
Pod::Simple::RTF 3.16
Pod::Simple::Text 3.16
Pod::Simple::Search 3.16
Pod::Weaver::PluginBundle::DAGOLDEN 0.016
Pod::Simple::SimpleTree 3.16
Pod::Simple::TextContent 3.16
Pod::Simple::TiedOutFH 3.16
Pod::Simple::Transcode 3.16
Pod::Simple::TranscodeDumb 3.16
Pod::Simple::TranscodeSmart 3.16
Pod::Simple::Wiki::Confluence 0.09
Pod::Simple::Wiki::Kwiki 0.08
Pod::Simple::Wiki::Mediawiki 0.08
Pod::Simple::Wiki::Moinmoin 0.08
Pod::Simple::Wiki::Template 0.08
Pod::Simple::Wiki::Textile 0.08
Pod::Simple::Wiki::Tiddlywiki 0.08
Pod::Simple::Wiki::Twiki 0.08
Pod::Simple::Wiki::Usemod 0.08
Pod::Simple::XHTML 3.16
Pod::Simple::XMLOutStream 3.16
Pod::Spell::CommonMistakes::WordList 1.000
Pod::Text::Color 2.06
Pod::Text::Overstrike 2.04
Pod::Text::Termcap 2.06
Pod::Tree::HTML 1.10
Pod::Tree::Node 1.10
Pod::Tree::PerlBin undef
Pod::Tree::PerlDist undef
Pod::Tree::PerlFunc undef
Pod::Tree::PerlLib undef
Pod::Tree::PerlMap undef
Pod::Tree::PerlPod undef
Pod::Tree::PerlTop undef
Pod::Tree::PerlUtil undef
Pod::Tree::Pod undef
Pod::WSDL::AUTOLOAD 0.05
Pod::WSDL::Attr 0.05
Pod::WSDL::Doc 0.05
Pod::WSDL::Fault 0.05
Pod::WSDL::Method 0.05
Pod::WSDL::Param 0.05
Pod::Weaver::Plugin::Transformer 3.101632
Pod::WSDL::Return 0.05
Pod::WSDL::Type 0.05
Pod::Weaver::PluginBundle::CorePrep 3.101632
Pod::WSDL::Utils 0.05
Pod::WSDL::Writer 0.05
Pod::Weaver::Config 3.101632
Pod::Weaver::Config::Finder 3.101632
Pod::Weaver::Plugin::EnsurePod5 3.101632
Pod::Weaver::Plugin::H1Nester 3.101632
Pod::Weaver::Role::Transformer 3.101632
Pod::Weaver::Plugin::TaskWeaver 0.101621
Pod::Weaver::Section::Generic 3.101632
Pod::Weaver::Section::Leftovers 3.101632
Pod::Weaver::Section::Legal 3.101632
Pod::Weaver::Section::Name 3.101632
Pod::Weaver::Section::Region 3.101632
Dist::Zilla::Plugin::AutoMetaResources
Pod::Weaver::PluginBundle::AJGB 1.103270
Dist::Zilla::Plugin::CopyFilesFromRelease
Dist::Zilla::Plugin::Run
Pod::Weaver::Section::Version
Pod::Weaver::PluginBundle::Author::RWSTAUNER 2.001
Pod::Weaver::PluginBundle::Default 3.101632
Dist::Zilla::Plugin::BumpVersionFromGit
Pod::Weaver::PluginBundle::GETTY 0.007
Pod::Weaver::PluginBundle::GopherRepellent 1.002
Dist::Zilla::Role::Git::Remote
Dist::Zilla::Role::Git::Remote::Branch
Dist::Zilla::Role::Git::Remote::Check
Dist::Zilla::Plugin::TravisYML
Pod::Elemental::MakeSelector
Pod::Weaver::Section::AllowOverride
Dist::Zilla::Plugin::AuthorSignatureTest
Pod::Weaver::Section::Bugs
Pod::Weaver::PluginBundle::MSCHOUT 0.20
Pod::Weaver::PluginBundle::RJBS 1.006
Pod::Weaver::Role::Plugin 3.101632
Pod::Weaver::Section::ReplaceAuthors 1.00
Pod::Weaver::Section::ReplaceLegal 1.00
Pod::Weaver::Section::ReplaceName 1.00
Pod::Weaver::Section::ReplaceVersion 1.00
Pod::WikiDoc::Parser 0.18
Pod::Wordlist 1.01
Pod::Wrap::Pretty 0.03
Polycom::Contact 0.05
Portable::CPAN 1.14
Portable::Config 1.14
Portable::HomeDir 1.14
Portable::minicpan 1.14
PostScript::BasicTypesetter 0.04
PostScript::File::Metrics 2.02
PostScript::File::Metrics::Loader 2.02
PostScript::File::Metrics::cp1252::Courier 2.01
PostScript::File::Metrics::cp1252::Courier::Bold 2.01
PostScript::File::Metrics::cp1252::Courier::BoldOblique 2.01
PostScript::File::Metrics::cp1252::Courier::Oblique 2.01
PostScript::File::Metrics::cp1252::Helvetica 2.01
PostScript::File::Metrics::cp1252::Helvetica::Bold 2.01
PostScript::File::Metrics::cp1252::Helvetica::BoldOblique 2.01
PostScript::File::Metrics::cp1252::Helvetica::Oblique 2.01
PostScript::File::Metrics::cp1252::Times::Bold 2.01
PostScript::File::Metrics::cp1252::Times::BoldItalic 2.01
PostScript::File::Metrics::cp1252::Times::Italic 2.01
PostScript::File::Metrics::cp1252::Times::Roman 2.01
PostScript::File::Metrics::iso_8859_1::Courier 2.01
PostScript::File::Metrics::iso_8859_1::Courier::Bold 2.01
PostScript::File::Metrics::iso_8859_1::Courier::BoldOblique 2.01
PostScript::File::Metrics::iso_8859_1::Courier::Oblique 2.01
PostScript::File::Metrics::iso_8859_1::Helvetica 2.01
PostScript::File::Metrics::iso_8859_1::Helvetica::Bold 2.01
PostScript::File::Metrics::iso_8859_1::Helvetica::BoldOblique 2.01
PostScript::File::Metrics::iso_8859_1::Helvetica::Oblique 2.01
PostScript::File::Metrics::iso_8859_1::Times::Bold 2.01
PostScript::File::Metrics::iso_8859_1::Times::BoldItalic 2.01
PostScript::File::Metrics::iso_8859_1::Times::Italic 2.01
PostScript::File::Metrics::iso_8859_1::Times::Roman 2.01
PostScript::File::Metrics::std::Courier 2.01
PostScript::File::Metrics::std::Courier::Bold 2.01
PostScript::File::Metrics::std::Courier::BoldOblique 2.01
PostScript::File::Metrics::std::Courier::Oblique 2.01
PostScript::File::Metrics::std::Helvetica 2.01
PostScript::File::Metrics::std::Helvetica::Bold 2.01
PostScript::File::Metrics::std::Helvetica::BoldOblique 2.01
PostScript::File::Metrics::std::Helvetica::Oblique 2.01
PostScript::File::Metrics::std::Times::Bold 2.01
PostScript::File::Metrics::std::Times::BoldItalic 2.01
PostScript::File::Metrics::std::Times::Italic 2.01
PostScript::File::Metrics::std::Times::Roman 2.01
PostScript::File::Metrics::sym::Symbol 2.01
PostScript::Font::TTtoType42 0.04
PostScript::FontInfo 1.05
PostScript::FontMetrics 1.06
PostScript::ISOLatin1Encoding 1.01
PostScript::ISOLatin9Encoding 1.01
PostScript::MailLabels::BasicData 1.30
PostScript::PrinterFontMetrics 0.08
PostScript::PseudoISO 1.00
PostScript::Report::Builder 0.08
PostScript::Report::Checkbox 0.01
PostScript::Report::Field 0.05
PostScript::Report::FieldTL 0.07
PostScript::Report::Font 0.06
PostScript::Report::HBox 0.01
PostScript::Report::Image 0.06
PostScript::Report::LinkField 0.07
PostScript::Report::Role::Component 0.07
PostScript::Report::Role::Container 0.07
PostScript::Report::Role::Value 0.01
PostScript::Report::Spacer 0.01
PostScript::Report::Types 0.08
PostScript::Report::VBox 0.04
PostScript::Report::Value::Constant 0.01
PostScript::Report::Value::Page 0.01
PostScript::Resources 1.03
PostScript::StandardEncoding 1.01
PostScript::WinANSIEncoding 1.01
PowerDNS::API::Client::Request 0.10
PowerDNS::API::Client::Response 0.10
Prima::Application undef
Prima::Calendar undef
Prima::ColorDialog undef
Prima::ComboBox undef
Prima::Config undef
Prima::Const undef
Prima::DetailedList undef
Prima::DockManager undef
Prima::Docks undef
Prima::Edit undef
Prima::EventHook undef
Prima::FileDialog undef
Prima::FontDialog undef
Prima::FrameSet undef
Prima::Gencls undef
Prima::Header undef
Prima::HelpViewer undef
Prima::Image::AnimateGIF undef
Prima::Image::TransparencyControl undef
Prima::Image::gif undef
Prima::Image::jpeg undef
Prima::Image::png undef
Prima::Image::tiff undef
Prima::ImageDialog undef
Prima::ImageViewer undef
Prima::IniFile undef
Prima::InputLine undef
Prima::IntUtils undef
Prima::KeySelector undef
Prima::Label undef
Prima::Lists undef
Prima::MDI undef
Prima::MsgBox undef
Prima::PS::Drawable undef
Prima::PS::Encodings undef
Prima::PS::Fonts undef
Prima::PS::Printer undef
Prima::PodView undef
Prima::ScrollBar undef
Prima::ScrollWidget undef
Prima::StartupWindow undef
Prima::StdBitmap undef
Prima::TextView undef
Prima::Themes undef
Prima::Utils undef
Prima::VB::CfgMaint undef
Prima::VB::Classes undef
Prima::VB::Config undef
Prima::VB::CoreClasses undef
Prima::VB::VBControls undef
Prima::VB::VBLoader undef
Prima::VB::examples::Widgety undef
Prima::sys::win32::FileDialog undef
Prima::themes::sysimage undef
Printer::EVOLIS::Parallel undef
Proc::Background::Unix 1.10
Proc::Background::Win32 1.10
Proc::Info::Environment::Linux undef
Proc::Killall 1.0
Proc::Killfam 1.0
Proc::Launcher::Manager 0.0.35
Proc::Launcher::Roles::Launchable 0.0.35
Proc::Launcher::Supervisor 0.0.35
Proc::ProcessTable::Process 0.02
Process::Delegatable 0.28
Process::Infinite 0.28
Process::Launcher 0.28
Process::Packageable 0.28
Process::Probe 0.28
Process::Role::Serializable 0.28
Process::Serializable 0.28
Process::Storable 0.28
ProgressMonitor::Stringify::ToEscStatus 10
Project::Easy::Config undef
Project::Easy::Config::File undef
Project::Easy::Config::Format::json undef
Project::Easy::Config::Format::perl undef
Project::Easy::DB undef
Project::Easy::Daemon undef
Project::Easy::Distribution undef
Project::Easy::Helper undef
Project::Euler::Lib::Types 0.20
Project::Euler::Lib::Utils 0.20
RDF::Notation3::RDFCore undef
Project::Euler::Problem::Base 0.20
Project::Euler::Problem::P001 0.20
Project::Euler::Problem::P002 0.20
Project::Euler::Problem::P003 0.20
Project::Euler::Problem::P004 0.20
Project::Euler::Problem::P005 0.20
ProjectBuilder::Log undef
ProjectBuilder::Log::Item undef
ProjectBuilder::Version 0.11.2
Property::Lookup::Base 1.101400
Property::Lookup::File 1.101400
Property::Lookup::Hash 1.101400
Property::Lookup::Local 1.101400
Prophet::App undef
Prophet::CLI undef
Prophet::CLI::CollectionCommand undef
Prophet::CLI::Command::Aliases undef
Prophet::CLI::Command::Clone undef
Prophet::CLI::Command::Config undef
Prophet::CLI::Command::Create undef
Prophet::CLI::Command::Delete undef
Prophet::CLI::Command::Export undef
Prophet::CLI::Command::History undef
Prophet::CLI::Command::Info undef
Prophet::CLI::Command::Init undef
Prophet::CLI::Command::Log undef
Prophet::CLI::Command::Merge undef
Prophet::CLI::Command::Mirror undef
Prophet::CLI::Command::Publish undef
Prophet::CLI::Command::Pull undef
Prophet::CLI::Command::Push undef
Prophet::CLI::Command::Search undef
Prophet::CLI::Command::Server undef
Prophet::CLI::Command::Settings undef
Devel::REPL::InProcess
Prophet::CLI::Command::Shell undef
Prophet::CLI::Command::Show undef
Prophet::CLI::Command::Update undef
Prophet::CLI::Dispatcher undef
Prophet::CLI::Dispatcher::Rule undef
Prophet::CLI::MirrorCommand undef
Prophet::CLI::Parameters undef
Prophet::CLI::ProgressBar undef
Prophet::CLI::PublishCommand undef
Prophet::CLI::RecordCommand undef
Prophet::CLI::TextEditorCommand undef
Prophet::CLIContext undef
Prophet::Change undef
Prophet::ChangeSet undef
Prophet::Collection undef
Prophet::Config undef
Prophet::Conflict undef
Prophet::ConflictingChange undef
Prophet::ConflictingPropChange undef
Prophet::ContentAddressedStore undef
Prophet::DatabaseSetting undef
Prophet::FilesystemReplica undef
Prophet::ForeignReplica undef
Prophet::Meta::Types undef
Prophet::PropChange undef
Prophet::Record undef
Prophet::Replica undef
Prophet::Replica::FS::Backend::File undef
Prophet::Replica::FS::Backend::LWP undef
Prophet::Replica::file undef
Prophet::Replica::http undef
Prophet::Replica::prophet undef
Prophet::Replica::prophet_cache undef
Prophet::Replica::sqlite undef
Prophet::ReplicaExporter undef
Prophet::ReplicaFeedExporter undef
Prophet::Resolver undef
Prophet::Resolver::AlwaysSource undef
Prophet::Resolver::AlwaysTarget undef
Prophet::Resolver::Failed undef
Prophet::Resolver::FromResolutionDB undef
Prophet::Resolver::IdenticalChanges undef
Prophet::Resolver::Prompt undef
Prophet::Server undef
Prophet::Server::Controller undef
Prophet::Server::Dispatcher undef
Prophet::Server::View undef
Prophet::Server::ViewHelpers undef
Prophet::Server::ViewHelpers::Function undef
Prophet::Server::ViewHelpers::HiddenParam undef
Prophet::Server::ViewHelpers::ParamFromFunction undef
Prophet::Server::ViewHelpers::Widget undef
Prophet::Test undef
Prophet::Test::Arena undef
Prophet::Test::Editor undef
Prophet::Test::Participant undef
Prophet::UUIDGenerator undef
Prophet::Util undef
Prophet::Web::Field undef
Prophet::Web::FunctionResult undef
Prophet::Web::Menu undef
Prophet::Web::Result undef
Protocol::IMAP::Client 0.002
Protocol::IMAP::Server 0.002
Protocol::PostgreSQL::Client 0.006
Protocol::PostgreSQL::FieldDescription 0.006
Protocol::PostgreSQL::RowDescription 0.006
Protocol::PostgreSQL::Statement 0.006
Protocol::Redis::Test undef
Protocol::WebSocket::Cookie undef
Protocol::WebSocket::Cookie::Request undef
Protocol::WebSocket::Frame undef
Protocol::WebSocket::Cookie::Response undef
Protocol::WebSocket::Handshake::Client undef
Protocol::WebSocket::Handshake::Server undef
Protocol::WebSocket::Handshake undef
Protocol::WebSocket::Message undef
Protocol::WebSocket::Request undef
Protocol::WebSocket::Response undef
Protocol::WebSocket::Stateful undef
Protocol::WebSocket::URL undef
Protocol::XMLRPC::Dispatcher undef
Protocol::XMLRPC::Method undef
Protocol::XMLRPC::MethodCall undef
Protocol::XMLRPC::MethodResponse undef
Protocol::XMLRPC::Value undef
Protocol::XMLRPC::Value::Array undef
Protocol::XMLRPC::Value::Base64 undef
Protocol::XMLRPC::Value::Boolean undef
Protocol::XMLRPC::Value::DateTime undef
Protocol::XMLRPC::Value::Double undef
Protocol::XMLRPC::Value::Integer undef
Protocol::XMLRPC::Value::String undef
Protocol::XMLRPC::Value::Struct undef
Protocol::XMLRPC::ValueFactory undef
Protocol::XMPP::Base 0.004
Protocol::XMPP::Contact 0.004
Protocol::XMPP::Element::Active 0.004
Protocol::XMPP::Element::Auth 0.004
Protocol::XMPP::Element::Bind 0.004
Protocol::XMPP::Element::Body 0.004
Protocol::XMPP::Element::Challenge 0.004
Protocol::XMPP::Element::Feature 0.004
Protocol::XMPP::Element::Features 0.004
Protocol::XMPP::Element::HTML 0.004
Protocol::XMPP::Element::IQ 0.004
Protocol::XMPP::Element::JID 0.004
Protocol::XMPP::Element::Mechanism 0.004
Protocol::XMPP::Element::Mechanisms 0.004
Protocol::XMPP::Element::Message 0.004
Protocol::XMPP::Element::Nick 0.004
Protocol::XMPP::Element::Presence 0.004
Protocol::XMPP::Element::Proceed 0.004
Protocol::XMPP::Element::Register 0.004
Protocol::XMPP::Element::Response 0.004
Protocol::XMPP::Element::Session 0.004
Protocol::XMPP::Element::StartTLS 0.004
Protocol::XMPP::Element::Subject 0.004
Protocol::XMPP::Element::Success 0.004
Protocol::XMPP::ElementBase 0.004
Protocol::XMPP::Handler 0.004
Protocol::XMPP::IQ::Roster 0.004
Protocol::XMPP::Message 0.004
Protocol::XMPP::Stream 0.004
Protocol::XMPP::TextElement 0.004
Protocol::XMPP::User 0.004
Provision::Unix::DNS 0.24
Provision::Unix::DNS::BIND 0.02
Provision::Unix::DNS::NicTool 0.23
Provision::Unix::DNS::tinydns 0.53
Provision::Unix::User 0.26
Provision::Unix::User::Darwin 0.16
Provision::Unix::User::FreeBSD 0.10
Provision::Unix::User::Linux 0.20
Provision::Unix::Utility 5.29
Provision::Unix::VirtualOS 0.59
Provision::Unix::VirtualOS::FreeBSD::Ezjail 0.11
Provision::Unix::VirtualOS::FreeBSD::Jail 0.07
Provision::Unix::VirtualOS::Linux 0.27
Provision::Unix::VirtualOS::Linux::OpenVZ 0.51
Provision::Unix::VirtualOS::Linux::Virtuozzo 0.13
Provision::Unix::VirtualOS::Linux::Xen 0.70
Provision::Unix::VirtualOS::Xen::Config 0.3
Provision::Unix::Web 0.09
Provision::Unix::Web::Apache 0.09
Provision::Unix::Web::Lighttpd 0.02
Qudo::Driver::DBI::DBD undef
Qudo::Driver::DBI::DBD::SQLite undef
Qudo::Driver::DBI::DBD::mysql undef
Qudo::Driver::Skinny undef
Qudo::Driver::Skinny::Row::ExceptionLog undef
Qudo::Driver::Skinny::Row::Func undef
Qudo::Driver::Skinny::Row::Job undef
Qudo::Driver::Skinny::Row::JobStatus undef
Qudo::Driver::Skinny::Schema undef
Qudo::Hook undef
Qudo::Hook::Scoreboard undef
Qudo::Hook::Serialize::JSON undef
Qudo::Hook::Serialize::MessagePack undef
Qudo::Hook::Serialize::Storable undef
Qudo::Job undef
Qudo::Manager undef
Qudo::Parallel::Manager::Registrar undef
Qudo::Plugin undef
Qudo::Test undef
Qudo::Worker undef
Quiki::Attachments 0.01
Quiki::Formatter undef
Quiki::Meta undef
Quiki::Pages undef
Quiki::Users undef
R::YapRI::Base 0.05
R::YapRI::Block 0.04
R::YapRI::Data::Matrix 0.04
R::YapRI::Graph::Simple 0.04
R::YapRI::Interpreter::Perl 0.04
RDF::AllegroGraph 0.04
RDF::AllegroGraph::Catalog 0.06
RDF::AllegroGraph::Catalog3 0.04
RDF::AllegroGraph::Catalog4 0.04
RDF::AllegroGraph::Repository 0.03
RDF::AllegroGraph::Repository3 0.04
RDF::AllegroGraph::Repository4 0.06
RDF::AllegroGraph::Server 0.04
RDF::AllegroGraph::Server3 0.02
RDF::AllegroGraph::Server4 0.02
RDF::AllegroGraph::Session4 0.03
RDF::AllegroGraph::Transaction4 undef
RDF::AllegroGraph::Utils undef
RDF::Core::Constants undef
RDF::Core::Enumerator undef
RDF::Core::Enumerator::DB_File undef
RDF::Core::Enumerator::Memory undef
RDF::Core::Enumerator::Postgres undef
RDF::Core::Evaluator undef
RDF::Core::Function undef
RDF::Core::Model undef
RDF::Core::Literal undef
RDF::Core::Model::Parser undef
RDF::Core::Model::Serializer undef
RDF::Core::ModelSet undef
RDF::Core::Node undef
RDF::Core::NodeFactory undef
RDF::Core::Parser undef
RDF::Core::Query undef
RDF::Core::Resource undef
RDF::Core::Schema undef
RDF::Core::Serializer undef
RDF::Core::Statement undef
RDF::Core::Storage undef
RDF::Core::Storage::DB_File undef
RDF::Core::Storage::Memory undef
RDF::Core::Storage::Postgres undef
RDF::Crypt::Decrypter 0.001
RDF::Crypt::Encrypter 0.001
RDF::Crypt::PrivateKeyFunction 0.001
RDF::Crypt::PublicKeyFunction 0.001
RDF::Crypt::Signer 0.001
RDF::Crypt::Verifier 0.001
RDF::DOAP::ChangeSets 0.102
RDF::Helper::Constants undef
RDF::Helper::Object undef
RDF::Helper::PerlConvenience undef
RDF::Helper::RDFQuery undef
RDF::Helper::RDFRedland undef
RDF::Helper::RDFRedland::Query undef
RDF::Helper::Statement undef
RDF::Helper::TiedPropertyHash undef
RDF::Notation3::PrefTriples undef
RDF::Notation3::RDFStore undef
RDF::Notation3::ReaderFile undef
RDF::Notation3::ReaderString undef
RDF::Notation3::SAX undef
RDF::Notation3::Template::TReader undef
RDF::Notation3::Template::TTriples undef
RDF::Notation3::Template::TXML undef
RDF::Notation3::Triples undef
RDF::Notation3::XML undef
RDF::Query::Algebra 2.905
RDF::Query::Algebra::Aggregate 2.905
RDF::Query::Algebra::BasicGraphPattern 2.905
RDF::Query::Algebra::Clear 2.905
RDF::Query::Algebra::Construct 2.905
RDF::Query::Algebra::Create 2.905
RDF::Query::Algebra::Distinct 2.905
RDF::Query::Algebra::Extend 2.905
RDF::Query::Algebra::Filter 2.905
RDF::Query::Algebra::GroupGraphPattern 2.905
RDF::Query::Algebra::Limit 2.905
RDF::Query::Algebra::Load 2.905
RDF::Query::Algebra::Minus 2.905
RDF::Query::Algebra::NamedGraph 2.905
RDF::Query::Algebra::Offset 2.905
RDF::Query::Algebra::Optional 2.905
RDF::Query::Algebra::Path 2.905
RDF::Query::Algebra::Project 2.905
RDF::Query::Algebra::Quad 2.905
RDF::Query::Algebra::Sequence 2.905
RDF::Query::Algebra::Service 2.905
RDF::Query::Algebra::Sort 2.905
RDF::Query::Algebra::SubSelect 2.905
RDF::Query::Algebra::TimeGraph 2.905
RDF::Query::Algebra::Triple 2.905
RDF::Query::Algebra::Union 2.905
RDF::Query::Algebra::Update 2.905
RDF::Query::BGPOptimizer 2.905
RDF::Query::Compiler::SQL 2.905
RDF::Query::Error 2.905
RDF::Query::ExecutionContext 2.905
RDF::Query::Expression 2.905
RDF::Query::Expression::Alias 2.905
RDF::Query::Expression::Binary 2.905
RDF::Query::Expression::Function 2.905
RDF::Query::Expression::Nary 2.905
RDF::Query::Expression::Unary 2.905
RDF::Query::Federate 2.905
RDF::Query::Federate::Plan 2.905
RDF::Query::Functions 2.905
RDF::Query::Functions::Geo 2.905
RDF::Query::Functions::Jena 2.905
RDF::Query::Functions::Kasei 2.905
RDF::Query::Functions::SPARQL 2.905
RDF::Query::Functions::Xpath 2.905
RDF::Query::Node 2.905
RDF::Query::Node::Blank 2.905
RDF::Query::Node::Literal 2.905
RDF::Query::Node::Resource 2.905
RDF::Query::Node::Variable 2.905
RDF::Query::Parser 2.905
RDF::Query::Parser::RDQL 2.905
RDF::Query::Parser::SPARQL 2.905
RDF::Query::Parser::SPARQL11 2.905
RDF::Query::Plan 2.905
RDF::Query::Plan::Aggregate 2.905
RDF::Query::Plan::BasicGraphPattern 2.905
RDF::Query::Plan::Clear 2.905
RDF::Query::Plan::ComputedStatement 2.905
RDF::Query::Plan::Constant 2.905
RDF::Query::Plan::Construct 2.905
RDF::Query::Plan::Distinct 2.905
RDF::Query::Plan::Exists 2.902
RDF::Query::Plan::Extend 2.905
RDF::Query::Plan::Filter 2.905
RDF::Query::Plan::Iterator 2.905
RDF::Query::Plan::Join 2.905
RDF::Query::Plan::Join::NestedLoop 2.905
RDF::Query::Plan::Join::PushDownNestedLoop 2.905
RDF::Query::Plan::Limit 2.905
RDF::Query::Plan::Load 2.905
RDF::Query::Plan::Minus 2.905
RDF::Query::Plan::NamedGraph 2.905
RDF::Query::Plan::Not 2.902
RDF::Query::Plan::Offset 2.905
RDF::Query::Plan::Path 2.905
RDF::Query::Plan::Project 2.905
RDF::Query::Plan::Quad 2.905
RDF::Query::Plan::Sequence 2.905
RDF::Query::Plan::Service 2.905
RDF::Query::Plan::Sort 2.905
RDF::Query::Plan::SubSelect 2.905
RDF::Query::Plan::ThresholdUnion 2.905
RDF::Query::Plan::Triple 2.905
RDF::Query::Plan::Union 2.905
RDF::Query::Plan::Update 2.905
RDF::Query::ServiceDescription 2.905
RDF::Query::Temporal 2.905
RDF::Query::Util 2.905
RDF::Query::VariableBindings 2.905
Icon::FamFamFam::Silk
RDF::RDFa::Generator::HTML::Head 0.101
RDF::RDFa::Generator::HTML::Hidden 0.101
RDF::RDFa::Generator::HTML::Pretty 0.101
RDF::RDFa::Generator::HTML::Pretty::Note 0.101
RDF::RDFa::Generator
RDF::RDFa::Linter::Error 0.051
RDF::RDFa::Linter::Service 0.051
RDF::RDFa::Linter::Service::CreativeCommons 0.051
RDF::RDFa::Linter::Service::Facebook 0.051
RDF::RDFa::Linter::Service::Google 0.051
RDF::RDFa::Parser::Config 1.094
RDF::RDFa::Parser::OpenDocumentObjectModel 1.094
RDF::Trine::Model 0.135
RDF::Simple::NS 1.004
RDF::Simple::Parser 1.013
RDF::Simple::Parser::Attribs 1.003
RDF::Trine::Parser 0.135
RDF::Simple::Parser::Element 1.003
RDF::Simple::Parser::Handler 1.016
RDF::Simple::Serialiser 1.012
RDF::Simple::Serializer 1.003
RDF::Trine::Error 0.135
RDF::Trine::Exporter::CSV 0.135
RDF::Trine::Graph 0.135
RDF::Trine::Iterator 0.135
RDF::Trine::Iterator::Bindings 0.135
RDF::Trine::Iterator::Bindings::Materialized 0.135
RDF::Trine::Iterator::Boolean 0.135
RDF::Trine::Serializer 0.135
RDF::Trine::Iterator::Graph 0.135
RDF::Trine::Iterator::Graph::Materialized 0.135
RDF::Trine::Iterator::JSONHandler 0.135
RDF::Trine::Iterator::SAXHandler 0.135
RDF::Trine::Model::Dataset 0.135
RDF::Trine::Model::StatementFilter 0.135
RDF::Trine::Model::Union 0.135
RDF::Trine::Namespace 0.135
RDF::Trine::NamespaceMap 0.135
RDF::Trine::Node 0.135
RDF::Trine::Node::Blank 0.135
RDF::Trine::Node::Formula 0.129
RDF::Trine::Node::Literal 0.135
RDF::Trine::Node::Nil 0.135
RDF::Trine::Node::Resource 0.135
RDF::Trine::Node::Variable 0.135
RDF::Trine::Parser::NQuads 0.135
RDF::Trine::Parser::NTriples 0.135
RDF::Trine::Parser::Notation3 0.129
RDF::Trine::Parser::RDFJSON 0.135
RDF::Trine::Parser::RDFXML 0.135
RDF::Trine::Parser::RDFa 0.135
RDF::Trine::Parser::Redland 0.135
RDF::Trine::Parser::ShorthandRDF 0.129
RDF::Trine::Parser::TriG 0.135
RDF::Trine::Parser::Turtle 0.135
RDF::Trine::Pattern 0.135
RDF::Trine::Serializer::NQuads 0.135
RDF::Trine::Serializer::NTriples 0.135
RDF::Trine::Serializer::NTriples::Canonical 0.135
RDF::Trine::Serializer::Notation3 0.129
RDF::Trine::Serializer::RDFJSON 0.135
RDF::Trine::Serializer::RDFXML 0.135
RDF::Trine::Serializer::Turtle 0.135
RDF::Trine::Statement 0.135
RDF::Trine::Statement::Quad 0.135
RDF::Trine::Store 0.135
RDF::Trine::Store::DBI 0.135
RDF::Trine::Store::DBI::Pg 0.135
RDF::Trine::Store::DBI::SQLite 0.135
RDF::Trine::Store::DBI::mysql 0.135
RDF::Trine::Store::Hexastore 0.135
RDF::Trine::Store::Jena::SDB::Layout2::Hash 0.001
RDF::Trine::Store::Memory 0.135
RDF::Trine::Store::Redland 0.135
RDF::Trine::Store::SPARQL 0.135
RDF::Trine::VariableBindings 0.135
RDF::iCalendar::Entity 0.002
RDF::iCalendar::Line 0.002
RDF::vCard::Entity 0.007
RDF::vCard::Entity::WithXmlSupport 0.007
RDF::vCard::Importer 0.007
RDF::vCard::Line 0.007
REST::Google::Feeds 1.0.8
REST::Google::Search::Blogs 1.0.8
REST::Google::Search::Books 1.0.8
REST::Google::Search::Images 1.0.8
REST::Google::Search::Local 1.0.8
REST::Google::Search::News 1.0.8
REST::Google::Search::Patent 1.0.8
REST::Google::Search::Video 1.0.8
REST::Google::Search::Web 1.0.8
RE_ast undef
RMI::Client undef
RMI::Client::ForkedPipes v0.1
RMI::Client::Tcp v0.1
RMI::Node v0.1
RMI::Proxy::DBI::db undef
RMI::ProxyObject undef
RMI::ProxyReference v0.1
RMI::Server v0.1
RMI::Server::ForkedPipes v0.1
RMI::Server::Tcp v0.1
RPC::PlClient 0.2020
RPC::PlClient::Comm 0.1002
RPC::PlServer 0.2020
RPC::PlServer::Comm 0.1003
RPC::PlServer::Test 0.01
Module::MultiConf
RPC::Serialized::ACL undef
RPC::Serialized::ACL::Group undef
RPC::Serialized::ACL::Group::File undef
RPC::Serialized::ACL::Group::GDBM_File undef
RPC::Serialized::ACL::Operation undef
RPC::Serialized::ACL::Subject undef
RPC::Serialized::ACL::Target undef
RPC::Serialized::AuthzHandler undef
RPC::Serialized::AuthzHandler::ACL undef
RPC::Serialized::Client undef
RPC::Serialized::Client::INET undef
RPC::Serialized::Client::NegKrb5 undef
RPC::Serialized::Client::SSL undef
RPC::Serialized::Client::STDIO undef
RPC::Serialized::Client::UNIX undef
RPC::Serialized::Config undef
RPC::Serialized::Exceptions undef
RPC::Serialized::Handler undef
RPC::Serialized::Handler::Echo undef
RPC::Serialized::Handler::Localtime undef
RPC::Serialized::Handler::Sleep undef
RPC::Serialized::Server undef
RPC::Serialized::Server::NetServer undef
RPC::Serialized::Server::NetServer::SSL undef
RPC::Serialized::Server::NetServer::Single undef
RPC::Serialized::Server::STDIO undef
RPC::Serialized::Server::UCSPI undef
RPC::Serialized::Server::UCSPI::IPC undef
RPC::Serialized::Server::UCSPI::NegKrb5 undef
RPC::Serialized::Server::UCSPI::TCP undef
RPC::XML::Parser 1.22
RPC::XML::Parser::XMLLibXML 1.12
RPC::XML::Parser::XMLParser 1.22
RPC::XML::Procedure 1.21
RPC::XML::Server 1.56
RT::Client::REST::Attachment 0.03
RT::Client::REST::Exception 0.19
RT::Client::REST::Forms 0.02
RT::Client::REST::HTTPClient 0.01
RT::Client::REST::Object 0.09
RT::Client::REST::Object::Exception 0.05
RT::Client::REST::Queue 0.02
RT::Client::REST::SearchResult 0.03
RT::Client::REST::Transaction 0.01
RT::Client::REST::User 0.03
RTPG::Direct 0.92
RTSP::Server::Client undef
RTSP::Server::Client::Connection undef
RTSP::Server::Connection undef
RTSP::Server::Listener undef
RTSP::Server::Logger undef
RTSP::Server::Mount undef
RTSP::Server::Mount::Stream undef
RTSP::Server::RTPListener undef
RTSP::Server::Session undef
RTSP::Server::Source undef
RTSP::Server::Source::Connection undef
File::Binary
Sort::DataTypes
Array::AsObject
RadioMobile::Config 0.01
RadioMobile::Config::LandHeightParser 0.01
RadioMobile::Config::MapFileParser 0.01
RadioMobile::Config::Pictures 0.01
RadioMobile::Config::StyleNetworksProperties 0.01
RadioMobile::Config::StyleNetworksPropertiesParser 0.01
RadioMobile::Cov 0.02
RadioMobile::Header 0.01
RadioMobile::Net 0.01
RadioMobile::NetUnit 0.01
RadioMobile::NetUnknown1Parser 0.01
RadioMobile::Nets 0.01
RadioMobile::NetsUnits 0.01
RadioMobile::System 0.01
RadioMobile::SystemAntennaParser 0.01
RadioMobile::SystemCableLossParser 0.01
RadioMobile::Systems 0.01
RadioMobile::Unit 0.01
RadioMobile::UnitIconParser 0.02
RadioMobile::UnitUnknown1Parser 0.01
RadioMobile::Units 0.01
RadioMobile::UnitsAzimutDirectionParser 0.01
RadioMobile::UnitsElevationParser 0.01
RadioMobile::UnitsHeightParser 0.02
RadioMobile::UnitsSystemParser 0.03
RadioMobile::Utils::Matrix 0.01
Razor2::Client::Config undef
Razor2::Client::Core 1.92
Razor2::Client::Engine undef
Razor2::Client::Version 2.83
Razor2::Engine::VR8 undef
Razor2::Errorhandler undef
Razor2::Logger undef
Razor2::Preproc::Manager undef
Razor2::Preproc::deBase64 undef
Razor2::Preproc::deHTML undef
Razor2::Preproc::deHTML_comment undef
Razor2::Preproc::deHTMLxs 2.18
Razor2::Preproc::deNewline undef
Razor2::Preproc::deQP undef
Razor2::Preproc::enBase64 undef
Razor2::Signature::Ephemeral undef
Razor2::Signature::Whiplash undef
Razor2::String undef
Razor2::Syslog 0.03
Redis::Hash undef
Reflex::Callbacks 0.088
Redis::List undef
Reflex::Acceptor 0.088
Reflex::Base 0.088
Reflex::Callback 0.088
Reflex::Callback::CodeRef 0.088
Reflex::Callback::Method 0.088
Reflex::Callback::Promise 0.088
Reflex::Client 0.088
Reflex::Collection 0.088
Reflex::Connector 0.088
Reflex::Role 0.088
Reflex::Interval 0.088
Reflex::Role::Collectible 0.088
Reflex::PID 0.088
Reflex::POE::Event 0.088
Reflex::POE::Postback 0.088
Reflex::POE::Session 0.088
Reflex::POE::Wheel 0.088
Reflex::Role::Reactive 0.088
Reflex::POE::Wheel::Run 0.088
Reflex::Role::Accepting 0.088
Reflex::Role::Connecting 0.088
Reflex::Role::InStreaming 0.088
Reflex::Role::Interval 0.088
Reflex::Role::OutStreaming 0.088
Reflex::Role::PidCatcher 0.088
Reflex::Role::Readable 0.088
Reflex::Role::Reading 0.088
Reflex::Role::Recving 0.088
Reflex::Role::SigCatcher 0.088
Reflex::Role::Streaming 0.088
Reflex::Role::Timeout 0.088
Reflex::Role::Wakeup 0.088
Reflex::Role::Writable 0.088
Reflex::Role::Writing 0.088
Reflex::Sender 0.088
Reflex::Signal 0.088
Reflex::Stream 0.088
Reflex::Timeout 0.088
Reflex::Trait::EmitsOnChange 0.088
Reflex::Trait::Observed 0.088
Reflex::UdpPeer 0.088
Regexp::Common::URI 2010010201
Reflex::Wakeup 0.088
Reflexive::Role::StreamFiltering 1.103450
Regexp::Common::CC 2010010201
Regexp::Common::SEN 2010010201
Regexp::Common::URI::RFC1035 2010010201
Regexp::Common::URI::RFC2396 2010010201
Regexp::Common::URI::RFC1738 2010010201
Regexp::Common::URI::RFC1808 2010010201
Regexp::Common::URI::RFC2384 2010010201
Regexp::Common::URI::RFC2806 2010010201
Regexp::Common::URI::fax 2010010201
Regexp::Common::URI::file 2010010201
Regexp::Common::URI::ftp 2010010201
Regexp::Common::URI::gopher 2010010201
Regexp::Common::URI::http 2010010201
Regexp::Common::URI::news 2010010201
Regexp::Common::URI::pop 2010010201
Regexp::Common::URI::prospero 2010010201
Regexp::Common::URI::tel 2010010201
Regexp::Common::URI::telnet 2010010201
Regexp::Common::URI::tv 2010010201
Regexp::Common::URI::wais 2010010201
Regexp::Common::_support 2010010201
Regexp::Common::balanced 2010010201
Regexp::Common::comment 2010010201
Regexp::Common::debian 0.2.11
Regexp::Common::number 2010010201
Regexp::Common::delimited 2010010201
Regexp::Common::lingua 2010010201
Regexp::Common::list 2010010201
Regexp::Common::net 2010010201
Regexp::Common::profanity 2010010201
Regexp::Common::whitespace 2010010201
Regexp::Common::zip 2010010201
Regexp::Ethiopic::Amharic 0.05
Regexp::Ethiopic::Geez 0.06
Regexp::Ethiopic::Tigrigna 0.05
DBD::Sys
Role::HasMessage::Errf 0.005
Role::HasPayload::Auto 0.005
Role::HasPayload::Meta::Attribute::Payload 0.005
Role::Identifiable::HasTags 0.005
Rose::Class 0.81
Rose::Class::MakeMethods::Generic 0.854
Rose::Class::MakeMethods::Set 0.81
Rose::DB::Cache 0.755
Rose::DB::Cache::Entry 0.736
Rose::DB::Constants undef
Rose::DB::Generic 0.11
Rose::DB::Informix 0.759
Rose::DB::Object::Helpers 0.784
Rose::DB::MySQL 0.762
Rose::DB::Object::Loader 0.787
Rose::DB::Object::Cached 0.785
Rose::DB::Object::Constants 0.791
Rose::DB::Object::ConventionManager 0.786
Rose::DB::Object::ConventionManager::Null 0.73
Rose::DB::Object::Exception 0.01
Rose::DB::Object::Iterator 0.759
Rose::DB::Object::MakeMethods::BigNum 0.788
Rose::DB::Object::MakeMethods::Date 0.787
Rose::DB::Object::MakeMethods::Generic 0.784
Rose::DB::Object::MakeMethods::Pg 0.771
Rose::DB::Object::MakeMethods::Std 0.011
Rose::DB::Object::MakeMethods::Time 0.771
Rose::DB::Object::Manager 0.790
Rose::DB::Object::Metadata 0.786
Rose::DB::Object::Metadata::Auto 0.786
Rose::DB::Object::Metadata::Auto::Generic 0.1
Rose::DB::Object::Metadata::Auto::Informix 0.784
Rose::DB::Object::Metadata::Auto::MySQL 0.784
Rose::DB::Object::Metadata::Auto::Oracle 0.786
Rose::DB::Object::Metadata::Auto::Pg 0.784
Rose::DB::Object::Metadata::Auto::SQLite 0.784
Rose::DB::Object::Metadata::Column 0.791
Rose::DB::Object::Metadata::Column::Array 0.788
Rose::DB::Object::Metadata::Column::BigInt 0.788
Rose::DB::Object::Metadata::Column::BigSerial 0.711
Rose::DB::Object::Metadata::Column::Bitfield 0.788
Rose::DB::Object::Metadata::Column::Blob 0.781
Rose::DB::Object::Metadata::Column::Boolean 0.788
Rose::DB::Object::Metadata::Column::Character 0.60
Rose::DB::Object::Metadata::Column::Date 0.788
Rose::DB::Object::Metadata::Column::Datetime 0.788
Rose::DB::Object::Metadata::Column::DatetimeYearToFraction 0.788
Rose::DB::Object::Metadata::Column::DatetimeYearToFraction1 0.01
Rose::DB::Object::Metadata::Column::DatetimeYearToFraction2 0.01
Rose::DB::Object::Metadata::Column::DatetimeYearToFraction3 0.01
Rose::DB::Object::Metadata::Column::DatetimeYearToFraction4 0.01
Rose::DB::Object::Metadata::Column::DatetimeYearToFraction5 0.01
Rose::DB::Object::Metadata::Column::DatetimeYearToMinute 0.788
Rose::DB::Object::Metadata::Column::DatetimeYearToMonth 0.788
Rose::DB::Object::Metadata::Column::DatetimeYearToSecond 0.788
Rose::DB::Object::Metadata::Column::Decimal 0.788
Rose::DB::Object::Metadata::Column::DoublePrecision 0.788
Rose::DB::Object::Metadata::Column::Enum 0.55
Rose::DB::Object::Metadata::Column::Epoch 0.788
Rose::DB::Object::Metadata::Column::Epoch::HiRes 0.702
Rose::DB::Object::Metadata::Column::Float 0.788
Rose::DB::Object::Metadata::Column::Integer 0.788
Rose::DB::Object::Metadata::Column::Interval 0.788
Rose::DB::Object::Metadata::Column::Numeric 0.788
Rose::DB::Object::Metadata::Column::Pg::Bytea 0.784
Rose::DB::Object::Metadata::Column::Pg::Chkpass 0.03
Rose::DB::Object::Metadata::Column::Scalar 0.60
Rose::DB::Object::Metadata::Column::Serial 0.70
Rose::DB::Object::Metadata::Column::Set 0.788
Rose::DB::Object::Metadata::Column::Text 0.50
Rose::DB::Object::Metadata::Column::Time 0.788
Rose::DB::Object::Metadata::Column::Timestamp 0.788
Rose::DB::Object::Metadata::Column::TimestampWithTimeZone 0.788
Rose::DB::Object::Metadata::Column::Varchar 0.03
Rose::DB::Object::Metadata::ColumnList 0.02
Rose::DB::Object::Metadata::ForeignKey 0.784
Rose::DB::Object::Metadata::MethodMaker 0.769
Rose::DB::Object::Metadata::Object 0.722
Rose::DB::Object::Metadata::PrimaryKey 0.58
Rose::DB::Object::Metadata::Relationship 0.780
Rose::DB::Object::Metadata::Relationship::ManyToMany 0.784
Rose::DB::Object::Metadata::Relationship::ManyToOne 0.781
Rose::DB::Object::Metadata::Relationship::OneToMany 0.781
Rose::DB::Object::Metadata::Relationship::OneToOne 0.771
Rose::DB::Object::Metadata::UniqueKey 0.782
Rose::DB::Object::Metadata::Util 0.67
Rose::DB::Object::MixIn 0.764
Rose::DB::Object::QueryBuilder 0.789
Rose::DB::Object::Std 0.021
Rose::DB::Object::Std::Cached 0.02
Rose::DB::Object::Std::Metadata 0.02
Rose::DB::Object::Util 0.772
Rose::DB::Oracle 0.762
Rose::DB::Pg 0.786
Rose::DB::Registry 0.728
Rose::DB::Registry::Entry 0.753
Rose::DB::SQLite 0.759
Rose::HTML::Form 0.607
Rose::DateTime::Parser 0.50
Rose::HTML::Anchor 0.606
Rose::HTML::Form::Constants 0.606
Rose::HTML::Form::Field 0.607
Rose::HTML::Form::Field::Checkbox 0.606
Rose::HTML::Form::Field::CheckboxGroup 0.606
Rose::HTML::Form::Field::Collection 0.606
Rose::HTML::Form::Field::Compound 0.611
Rose::HTML::Form::Field::Date 0.606
Rose::HTML::Form::Field::DateTime 0.606
Rose::HTML::Form::Field::DateTime::EndDate 0.606
Rose::HTML::Form::Field::DateTime::Range 0.606
Rose::HTML::Form::Field::Hidden 0.606
Rose::HTML::Form::Field::DateTime::Split 0.606
Rose::HTML::Form::Field::DateTime::Split::MDYHMS 0.550
Rose::HTML::Form::Field::DateTime::Split::MonthDayYear 0.606
Rose::HTML::Form::Field::DateTime::StartDate 0.606
Rose::HTML::Form::Field::Email 0.606
Rose::HTML::Form::Field::File 0.606
Rose::HTML::Form::Field::Group 0.606
Rose::HTML::Form::Field::Group::OnOff 0.606
Rose::HTML::Form::Field::Input 0.607
Rose::HTML::Form::Field::Integer 0.606
Rose::HTML::Form::Field::Numeric 0.606
Rose::HTML::Form::Field::OnOff 0.606
Rose::HTML::Form::Field::OnOff::Checkable 0.606
Rose::HTML::Form::Field::OnOff::Selectable 0.606
Rose::HTML::Form::Field::Option 0.606
Rose::HTML::Form::Field::RadioButton 0.606
Rose::HTML::Form::Field::Option::Container 0.606
Rose::HTML::Form::Field::OptionGroup 0.606
Rose::HTML::Form::Field::Password 0.606
Rose::HTML::Form::Field::PhoneNumber::US 0.606
Rose::HTML::Form::Field::Text 0.606
Rose::HTML::Form::Field::PhoneNumber::US::Split 0.606
Rose::HTML::Form::Field::PopUpMenu 0.606
Rose::HTML::Form::Field::RadioButtonGroup 0.606
Rose::HTML::Form::Field::Reset undef
Rose::HTML::Form::Field::SelectBox 0.606
Rose::HTML::Form::Field::Set 0.606
Rose::HTML::Form::Field::Submit 0.606
Rose::HTML::Form::Field::TextArea 0.606
Rose::HTML::Form::Field::Time 0.606
Rose::HTML::Form::Field::Time::Hours 0.606
Rose::HTML::Form::Field::Time::Minutes 0.606
Rose::HTML::Form::Field::Time::Seconds 0.606
Rose::HTML::Form::Field::Time::Split 0.606
Rose::HTML::Form::Field::Time::Split::HourMinuteSecond 0.606
Rose::HTML::Form::Repeatable 0.555
Rose::HTML::Image 0.606
Rose::HTML::Label 0.606
Rose::HTML::Link 0.606
Rose::HTML::Object 0.606
Rose::HTML::Object::Error 0.606
Rose::HTML::Object::Error::Localized 0.606
Rose::HTML::Object::Errors 0.600
Rose::HTML::Object::Exporter 0.605
Rose::HTML::Object::Localized 0.600
Rose::HTML::Object::MakeMethods::Generic 0.606
Rose::HTML::Object::MakeMethods::Localization 0.606
Rose::HTML::Object::Message 0.606
Rose::HTML::Object::Message::Localized 0.600
Rose::HTML::Object::Message::Localizer 0.606
Rose::HTML::Object::Messages 0.605
Rose::HTML::Object::Repeatable 0.606
Rose::HTML::Object::WithWrapAroundChildren 0.554
Rose::HTML::Script 0.606
Rose::HTML::Text 0.602
Rose::HTML::Util 0.011
Rose::HTMLx::Form::Field::RadioButtonBoolean 0.03
Rose::HTMLx::Form::Related::DBIC 0.22
Rose::HTMLx::Form::Related::DBIC::Metadata 0.22
Rose::HTMLx::Form::Related::Metadata 0.22
Rose::HTMLx::Form::Related::RDBO 0.22
Rose::HTMLx::Form::Related::RDBO::Metadata 0.22
Rose::HTMLx::Form::Related::RelInfo 0.22
Rose::Object::MakeMethods 0.856
Rose::Object::MakeMethods::DateTime 0.81
Rose::Object::MakeMethods::Generic 0.859
Rose::Object::MixIn 0.856
Router::Simple::Declare undef
Router::Simple::Route undef
Router::Simple::SubMapper undef
SDBM_File 1.06
SOAP::Constants 0.712
SOAP::Lite::Deserializer::XMLSchema1999 undef
SOAP::Lite::Deserializer::XMLSchema2001 undef
SOAP::Lite::Deserializer::XMLSchemaSOAP1_1 undef
SOAP::Lite::Deserializer::XMLSchemaSOAP1_2 undef
SOAP::Lite::Packager undef
SOAP::Lite::Utils undef
SOAP::Packager 0.712
SOAP::Test 0.712
SOAP::Transport::HTTP 0.712
SOAP::Transport::IO 0.712
SOAP::Transport::LOCAL 0.712
SOAP::Transport::LOOPBACK undef
SOAP::Transport::MAILTO 0.712
SOAP::Transport::POP3 0.712
SOAP::WSDL::Base 2.00.10
SOAP::WSDL::Binding 2.00.10
SOAP::WSDL::Client 2.00.10
SOAP::WSDL::Definitions 2.00.10
SOAP::WSDL::Deserializer::Hash 2.00.10
SOAP::WSDL::Deserializer::SOM 2.00.10
SOAP::WSDL::Deserializer::XSD 2.00.10
SOAP::WSDL::Expat::Base 2.00.10
SOAP::WSDL::Expat::WSDLParser 2.00.10
SOAP::WSDL::Expat::Message2Hash 2.00.10
SOAP::WSDL::Factory::Generator 2.00.10
SOAP::WSDL::Expat::MessageParser 2.00.10
SOAP::WSDL::Expat::MessageStreamParser 2.00.10
SOAP::WSDL::Factory::Deserializer 2.00.10
SOAP::WSDL::Factory::Serializer 2.00.10
SOAP::WSDL::Factory::Transport 2.00.10
SOAP::WSDL::Generator::Iterator::WSDL11 2.00.10
SOAP::WSDL::Generator::PrefixResolver 2.00.10
SOAP::WSDL::Generator::Template 2.00.10
SOAP::WSDL::Generator::Template::Plugin::XSD 2.00.10
SOAP::WSDL::Generator::Template::XSD 2.00.10
SOAP::WSDL::Generator::Visitor 2.00.10
SOAP::WSDL::Generator::Visitor::Typemap 2.00.10
SOAP::WSDL::Message 2.00.10
SOAP::WSDL::OpMessage 2.00.10
SOAP::WSDL::Operation 2.00.10
SOAP::WSDL::Part 2.00.10
SOAP::WSDL::Port 2.00.10
SOAP::WSDL::PortType 2.00.10
SOAP::WSDL::SOAP::Address 2.00.10
SOAP::WSDL::SOAP::Body 2.00.10
SOAP::WSDL::SOAP::Header 2.00.10
SOAP::WSDL::SOAP::HeaderFault 2.00.10
SOAP::WSDL::SOAP::Operation 2.00.10
SOAP::WSDL::SOAP::Typelib::Fault 2.00.10
SOAP::WSDL::SOAP::Typelib::Fault11 2.00.10
SOAP::WSDL::Serializer::XSD 2.00.10
SOAP::WSDL::Server 2.00.10
SOAP::WSDL::Server::CGI 2.00.10
SOAP::WSDL::Server::Mod_Perl2 2.00.10
SOAP::WSDL::Server::Simple 2.00.10
SOAP::WSDL::Service 2.00.10
SOAP::WSDL::Transport::HTTP 2.00.10
SOAP::WSDL::Transport::Loopback 2.00.10
SOAP::WSDL::Transport::Test 2.00.10
SOAP::WSDL::TypeLookup 2.00.10
SOAP::WSDL::Types 2.00.10
SOAP::WSDL::XSD::Annotation 2.00.10
SOAP::WSDL::XSD::Attribute 2.00.10
SOAP::WSDL::XSD::AttributeGroup 2.00.10
SOAP::WSDL::XSD::Builtin 2.00.10
SOAP::WSDL::XSD::ComplexType 2.00.10
SOAP::WSDL::XSD::Element 2.00.10
SOAP::WSDL::XSD::Enumeration 2.00.10
SOAP::WSDL::XSD::FractionDigits 2.00.10
SOAP::WSDL::XSD::Group 2.00.10
SOAP::WSDL::XSD::Length 2.00.10
SOAP::WSDL::XSD::MaxExclusive 2.00.10
SOAP::WSDL::XSD::MaxInclusive 2.00.10
SOAP::WSDL::XSD::MaxLength 2.00.10
SOAP::WSDL::XSD::MinExclusive 2.00.10
SOAP::WSDL::XSD::MinInclusive 2.00.10
SOAP::WSDL::XSD::MinLength 2.00.10
SOAP::WSDL::XSD::Pattern 2.00.10
SOAP::WSDL::XSD::Schema 2.00.10
SOAP::WSDL::XSD::Schema::Builtin 2.00.10
SOAP::WSDL::XSD::SimpleType 2.00.10
SOAP::WSDL::XSD::TotalDigits 2.00.10
SOAP::WSDL::XSD::Typelib::Attribute 2.00.10
SOAP::WSDL::XSD::Typelib::AttributeSet 2.00.10
SOAP::WSDL::XSD::Typelib::Builtin 2.00.10
SOAP::WSDL::XSD::Typelib::Builtin::anyType 2.00.10
SOAP::WSDL::XSD::Typelib::Builtin::boolean 2.00.10
SOAP::WSDL::XSD::Typelib::Builtin::time 2.00.10
SOAP::WSDL::XSD::Typelib::SimpleType 2.00.10
SOAP::WSDL::XSD::WhiteSpace 2.00.10
SQL::Abstract::Test undef
SQL::Abstract::Tree undef
SQL::Dialects::ANSI 1.33
SQL::Dialects::AnyData 1.33
SQL::Dialects::CSV 1.33
SQL::Dialects::Role 1.33
SQL::Eval 1.33
SQL::Maker::Condition undef
SQL::Maker::Plugin::InsertMulti undef
SQL::Maker::Select undef
SQL::Maker::Select::Oracle undef
SQL::Maker::SelectSet undef
SQL::Maker::Util undef
SQL::Translator::Diff undef
SQL::Parser 1.33
SQL::ReservedWords::DB2 0.7
SQL::ReservedWords::MySQL 0.7
SQL::ReservedWords::ODBC 0.7
SQL::ReservedWords::Oracle 0.7
SQL::ReservedWords::PostgreSQL 0.7
SQL::ReservedWords::SQLServer 0.7
SQL::ReservedWords::SQLite 0.7
SQL::ReservedWords::Sybase 0.7
SQL::Statement::Function 1.33
SQL::Statement::Functions 1.33
SQL::Statement::GetInfo 1.33
SQL::Statement::Operation 1.33
SQL::Statement::Placeholder 1.33
SQL::Statement::RAM 1.33
SQL::Statement::Term 1.33
SQL::Statement::TermFactory 1.33
SQL::Statement::Util 1.33
SQL::Translator::Filter::DefaultExtra 1.59
SQL::Translator::Filter::Globals 1.59
SQL::Translator::Filter::Names 1.59
Report::Generator::Render 0.002
Report::Generator::Render::TT2 0.002
ResourcePool::Command 1.0106
ResourcePool::Command::Exception 1.0106
ResourcePool::Command::Execute 1.0106
ResourcePool::Command::NoFailoverException 1.0106
ResourcePool::Command::SOAP::Lite::Call 1.0103
ResourcePool::Factory 1.0106
ResourcePool::Factory::SOAP::Lite 1.0103
ResourcePool::LoadBalancer 1.0106
ResourcePool::LoadBalancer::FailBack 1.0106
ResourcePool::LoadBalancer::FailOver 1.0106
ResourcePool::LoadBalancer::FallBack 1.0106
ResourcePool::LoadBalancer::LeastUsage 1.0106
ResourcePool::LoadBalancer::RoundRobin 1.0106
ResourcePool::Resource 1.0106
ResourcePool::Singleton 1.0106
Squatting::On::PSGI
Image::Xbm
Rhetoric::Formatters undef
Rhetoric::Helpers undef
Rhetoric::Meta undef
Rhetoric::Storage::CouchDB undef
Rhetoric::Storage::File undef
Rhetoric::Storage::MySQL undef
Rhetoric::Theme::BrownStone 0.01
Rhetoric::Widgets undef
Rinchi::XMLSchema::HFP 0.01
SQL::Translator::Parser 1.60
SQL::Translator::Parser::Access 1.59
SQL::Translator::Parser::DB2 undef
SQL::Translator::Parser::DB2::Grammar undef
SQL::Translator::Parser::DBI 1.59
SQL::Translator::Parser::DBI::DB2 undef
SQL::Translator::Parser::DBI::MySQL 1.59
SQL::Translator::Parser::DBI::Oracle 1.59
SQL::Translator::Parser::DBI::PostgreSQL 1.59
SQL::Translator::Parser::DBI::SQLServer 1.59
SQL::Translator::Parser::DBI::SQLite 1.59
SQL::Translator::Parser::DBI::Sybase 1.59
SQL::Translator::Parser::DBIx::Class 1.10
SQL::Translator::Parser::Excel 1.59
SQL::Translator::Parser::MySQL 1.59
SQL::Translator::Parser::Oracle 1.59
SQL::Translator::Parser::PostgreSQL 1.59
SQL::Translator::Parser::SQLServer 1.59
SQL::Translator::Parser::SQLite 1.59
SQL::Translator::Parser::Storable 1.59
SQL::Translator::Parser::Sybase 1.59
SQL::Translator::Parser::XML 1.59
SQL::Translator::Parser::XML::SQLFairy 1.59
SQL::Translator::Parser::YAML 1.59
SQL::Translator::Parser::xSV 1.59
SQL::Translator::Producer 1.59
SQL::Translator::Producer::ClassDBI 1.59
SQL::Translator::Producer::DB2 1.59
SQL::Translator::Producer::DBIx::Class::File 0.1
SQL::Translator::Producer::DiaUml 1.59
SQL::Translator::Producer::Diagram 1.59
SQL::Translator::Schema::Constants 1.59
SQL::Translator::Producer::Dumper 1.59
SQL::Translator::Producer::GraphViz 1.59
SQL::Translator::Producer::HTML 1.59
SQL::Translator::Producer::Latex 1.59
SQL::Translator::Producer::MySQL 1.59
SQL::Translator::Producer::Oracle 1.59
SQL::Translator::Producer::POD 1.59
SQL::Translator::Utils 1.59
SQL::Translator::Producer::PostgreSQL 1.59
SQL::Translator::Producer::SQLServer 1.59
SQL::Translator::Producer::SQLite 1.59
SQL::Translator::Producer::Storable 1.59
SQL::Translator::Producer::Sybase 1.59
SQL::Translator::Producer::TT::Base 1.59
SQL::Translator::Producer::TT::Table 1.59
SQL::Translator::Producer::TTSchema 1.59
SQL::Translator::Producer::XML 1.59
SQL::Translator::Producer::XML::SQLFairy 1.59
SQL::Translator::Producer::YAML 1.59
SQL::Translator::Schema::Constraint 1.59
SQL::Translator::Schema::Field 1.59
SQL::Translator::Schema::Index 1.59
SQL::Translator::Schema::Object 1.59
SQL::Translator::Schema::Procedure 1.59
SQL::Translator::Schema::Trigger 1.59
SQL::Translator::Schema::View 1.59
STD::Actions undef
STD::Cursor undef
STD::LazyMap undef
SVG::DOM 2.50
SVG::Element 2.50
Rose::DBx::AutoReconnect
Rose::DBx::Cache::Anywhere
IOD::Examples
Config::IOD::Reader
Complete::Common
Text::Levenshtein::Flexible
Complete::Util
Complete::Env
Complete::Path
Complete::File
Complete::Bash
Complete::Getopt::Long
Perinci::Sub::Property::arg::cmdline
Perinci::Examples
Perinci::Sub::Complete
Package::MoreUtil
Progress::Any
Perinci::Access::Base
Package::Util::Lite
Perinci::Access::Perl
LWP::Protocol::http::SocketUnixAlt
Perinci::AccessUtil
Perinci::Access::HTTP::Client
Perinci::Access::Schemeless
Perinci::Access::Simple::Client
Perinci::Access
Pipe::Find
Perinci::CmdLine::Inline
Perinci::CmdLine::Gen
Test::Perinci::CmdLine
Text::Table::Tiny
Data::Check::Structure
Text::Table::Any
Number::Format::BigFloat
Sort::BySpec
Perinci::Result::Format::Lite
Log::ger::Output::Screen
Perinci::CmdLine::Lite
Module::CoreList::More
Perinci::Sub::ArgEntity::filename
Class::GenSource
Data::Sah::Coerce::perl::obj::str_url
Getopt::Long::EvenLess
Getopt::Long::Subcommand
Module::DataPack
Perinci::CmdLine::Base
Perinci::CmdLine::POD
Perinci::Sub::ArgEntity::modulename
Perinci::Sub::ArgEntity::riap_url
Perl::Stripper
Sah::Schema::riap::url
Perinci::CmdLine::Util::Config
Perinci::CmdLine::Help
Text::FormatTable
Text::Table::Org
Text::Table::HTML::DataTables
Text::Table::TSV
Term::Choose
Term::Choose::Util
Term::TablePrint
Text::ANSI::BaseUtil
Text::ANSI::WideUtil
Text::Table::TinyColorWide
Text::Table::HTML
Text::MarkdownTable
Text::Table::ASV
Spreadsheet::GenerateXLSX
Text::Table::XLSX
Text::Table::TinyColor
Text::Table::Paragraph
Text::Table::CSV
Text::WideChar::Util
Text::Table::TinyWide
Text::Table::LTSV
Sort::ByExample
JSON::Tiny::Subclassable
Complete::Tcsh
Data::Section::Seekable::Writer
PERLANCAR::AppUtil::PerlStripper
String::Elide::Parts
Progress::Any::Output::TermProgressBarColor
Complete::Fish
HTTP::Tiny::UNIX
Perinci::Sub::ConvertArgs::Array
Perinci::Access::Lite
Log::ger::Output::Composite
Log::ger::Output::Syslog
PERLANCAR::File::HomeDir
Log::ger::App
Complete::Zsh
Markdown::To::POD
Module::Patch
Perinci::CmdLine::Util
Perinci::CmdLine::Dump
Filesys::Cap
Complete::Module
Proc::Find::Parents
Term::Detect::Software
Data::Unixish::ANSI
Term::App::Role::Attrs
Border::Style::Role
Color::ANSI::Util
Color::Theme::Role::ANSI
Test::RandomResult
Color::RGB::Util
Parse::VarName
Text::ANSITable
Scalar::Util::LooksLikeNumber
Data::Dump::Color
Complete::Riap
Dist::Util
Complete::Dist
Perinci::Sub::XCompletion::riap_url
Perinci::Sub::To::CLIDocData
Number::Format::Metric
Sort::Sub
String::Pad
Syntax::Feature::EachOnArray
Data::Unixish::Apply
Perinci::Sub::ConvertArgs::Argv
Bio::DB::Fasta
Bio::DB::GFF::Util::Rearrange
Bio::Location::Simple
Bio::PrimarySeq
Bio::RangeI
Bio::Root::Root
Bio::Root::Test
Bio::Seq
Bio::SeqFeature::CollectionI
Bio::SeqFeature::Lite
Bio::DB::SeqFeature::Store
Log::ger::Output::File
Log::ger::Layout::Pattern
Rose::DBx::Bouquet::Config
Rose::DBx::Garden
Catalyst::Plugin::Static::Simple::ByClass
Rose::DBx::Garden::Catalyst::Controller
Rose::DBx::Garden::Catalyst::Excel
Rose::DBx::Garden::Catalyst::Form
Rose::DBx::Garden::Catalyst::Form::Metadata
Rose::DBx::Garden::Catalyst::Object
Rose::DBx::Garden::Catalyst::TT
Rose::DBx::Garden::Catalyst::View
Rose::ObjectX::CAF::MethodMaker
SWISH::API
SWISH::API::More
SWISH::API::Stat
SWISH::API::Object
SWISH::Prog
Rose::DBx::Object::Indexed::Indexer 0.008
Rose::DBx::Object::Indexed::Indexer::KSx 0.008
Rose::DBx::Object::Indexed::Indexer::Xapian 0.008
Routes::Tiny::Match undef
Routes::Tiny::Pattern undef
Rule::Engine::Filter undef
Rule::Engine::Rule undef
Rule::Engine::RuleSet undef
Rule::Engine::Session undef
SCGI::Request undef
HTTP::Proxy
Net::CUPS
Parse::EDID
FusionInventory::Agent::Config undef
FusionInventory::Agent::Network undef
FusionInventory::Agent::RPC undef
FusionInventory::Agent::SNMP undef
FusionInventory::Agent::Storage undef
FusionInventory::Agent::Target undef
FusionInventory::Agent::Targets undef
Alien::SDL
SDL
SDLx::Widget::Menu undef
SDLx::Widget::Textbox undef
SIAM::AccessScope undef
SIAM::Attribute undef
SIAM::Contract undef
SIAM::Device undef
SIAM::Driver::Simple undef
SIAM::Object undef
SIAM::Privilege undef
SIAM::ScopeMember undef
SIAM::Service undef
SIAM::ServiceDataElement undef
SIAM::ServiceUnit undef
SIAM::User undef
Module::List::Pluggable
Object::Tiny::XS
SNA::Network
SOAP::Data::Builder::Element
SOAP::Data::Builder
SOAP::ISIWoK::Lite
SOAP::ISIWoK::Sword
SOAP::Transport::HTTP::MockReplay
SOAP::Transport::HTTP::Log4perl
SQL::Bibliosoph::CatalogFile 2.00
SQL::Bibliosoph::Dummy 2.00
SQL::Bibliosoph::Query 2.00
SQL::Bibliosoph::Sims 2.0
SVG::Extension 2.50
SVG::Manual 2.5
SVG::Rasterize::Colors 0.000009
SVG::Rasterize::Exception 0.003006
SVG::Rasterize::Properties 0.000009
SVG::Rasterize::Regexes 0.003005
SVG::Rasterize::Specification 0.003005
SVG::Rasterize::Specification::Animation 0.003005
SVG::Rasterize::Specification::Clip 0.003005
SVG::Rasterize::Specification::ColorProfile 0.003005
SVG::Rasterize::Specification::Conditional 0.003005
SVG::Rasterize::Specification::Cursor 0.003005
SVG::Rasterize::Specification::Description 0.003005
SVG::Rasterize::Specification::Extensibility 0.003005
SVG::Rasterize::Specification::Filter 0.003005
SVG::Rasterize::Specification::FilterPrimitive 0.003005
SVG::Rasterize::Specification::Font 0.003005
SVG::Rasterize::Specification::Gradient 0.003005
SVG::Rasterize::Specification::Hyperlink 0.003005
SVG::Rasterize::Specification::Image 0.003005
SVG::Rasterize::Specification::Marker 0.003005
SVG::Rasterize::Specification::Mask 0.003005
SVG::Rasterize::Specification::Pattern 0.003005
SVG::Rasterize::Specification::Script 0.003005
SVG::Rasterize::Specification::Shape 0.003005
SVG::Rasterize::Specification::Structure 0.003005
SVG::Rasterize::Specification::Style 0.003005
SVG::Rasterize::Specification::Text 0.003005
SVG::Rasterize::Specification::TextContent 0.003005
SVG::Rasterize::Specification::Use 0.003005
SVG::Rasterize::Specification::View 0.003005
SVG::Rasterize::State 0.003006
SVG::Rasterize::State::Text 0.003006
SVG::Rasterize::TextNode 0.003002
SVG::Sparkline::Area 0.35
SVG::Sparkline::Bar 0.35
SVG::Sparkline::Line 0.35
SVG::Sparkline::RangeArea 0.35
SVG::Sparkline::RangeBar 0.35
SVG::Sparkline::Utils 0.35
SVG::Sparkline::Whisker 0.35
SVG::TT::Graph::BarHorizontal undef
SVG::TT::Graph::BarLine undef
SVG::TT::Graph::Line undef
SVG::TT::Graph::Pie undef
SVG::TT::Graph::TimeSeries undef
SVG::XML 2.50
SVN::Access::Group 0.06
SVN::Access::Resource 0.06
SVN::Dump
SVN::Analysis 1.000
SVN::Analysis::Copy 1.000
SVN::Analysis::Dir 1.000
SVN::Class::Dir 0.16
SVN::Class::File 0.16
SVN::Class::Info 0.16
SVN::Class::Repos 0.16
SVN::Dump::Analyzer 1.000
SVN::Dump::Arborist 1.000
SVN::Dump::AuthorExtractor 1.000
SVN::Dump::Change 1.000
SVN::Dump::Change::Copy 1.000
SVN::Dump::Change::Cpdir 1.000
SVN::Dump::Change::Cpfile 1.000
SVN::Dump::Change::Edit 1.000
SVN::Dump::Change::Mkdir 1.000
SVN::Dump::Change::Mkfile 1.000
SVN::Dump::Change::Rename 1.000
SVN::Dump::Change::Rm 1.000
SVN::Dump::Change::Rmdir 1.000
SVN::Dump::Change::Rmfile 1.000
SVN::Dump::Headers undef
SVN::Dump::Property undef
SVN::Dump::Reader undef
SVN::Dump::Record undef
SVN::Dump::Replayer 1.000
SVN::Dump::Replayer::Filesystem 1.000
SVN::Dump::Replayer::Git 1.000
SVN::Dump::Revision 1.000
SVN::Dump::Text undef
SVN::Dump::Walker 1.000
SVN::Look
SVN::Notify
JIRA::REST
SVN::Hooks::AllowLogChange undef
SVN::Hooks::AllowPropChange undef
SVN::Hooks::CheckCapability undef
SVN::Hooks::CheckJira undef
SVN::Hooks::CheckLog undef
SVN::Hooks::CheckMimeTypes undef
SVN::Hooks::CheckProperty undef
SVN::Hooks::CheckStructure undef
SVN::Hooks::DenyChanges undef
SVN::Hooks::DenyFilenames undef
SVN::Hooks::Generic undef
SVN::Hooks::JiraAcceptance undef
SVN::Hooks::Mailer 0.11
SVN::Hooks::Notify undef
SVN::Hooks::UpdateConfFile undef
SVN::Notify::Alternative 1.0
SVN::Notify::Filter::Trac 2.81
SVN::Notify::HTML 2.81
SVN::Notify::HTML::ColorDiff 2.81
SWF::BinStream::Codec::Zlib 0.01
SWF::BinStream::File 0.043
SWISH::Filter::Document 0.15
SWISH::Filter::MIMETypes undef
SWISH::Filters::Base 0.15
SWISH::Filters::Decompress 0.15
SWISH::Filters::Doc2html 0.15
SWISH::Filters::Doc2txt 0.15
SWISH::Filters::ID3toHTML 0.15
SWISH::Filters::IPTC2html 0.15
SWISH::Filters::Pdf2HTML 0.15
SWISH::Filters::XLtoHTML 0.15
SWISH::Filters::pp2html 0.15
SWISH::Filters::ppt2txt 0.15
SWISH::Filters::xls2txt 0.15
SWISH::Prog::Aggregator 0.50
SWISH::Prog::Aggregator::DBI 0.50
SWISH::Prog::Aggregator::FS 0.50
SWISH::Prog::Aggregator::Mail 0.50
SWISH::Prog::Aggregator::MailFS 0.50
SWISH::Prog::Aggregator::Object 0.50
SWISH::Prog::Aggregator::Spider 0.50
SWISH::Prog::Aggregator::Spider::UA undef
SWISH::Prog::Cache 0.50
SWISH::Prog::Class 0.50
SWISH::Prog::Config 0.50
SWISH::Prog::Doc 0.50
SWISH::Prog::Headers 0.50
SWISH::Prog::Indexer 0.50
SWISH::Prog::InvIndex 0.50
SWISH::Prog::InvIndex::Meta 0.50
KinoSearch
Search::Query::Dialect::KSx
SWISH::Prog::KSx::Indexer 0.18
SWISH::Prog::KSx::InvIndex 0.18
SWISH::Prog::KSx::Result 0.18
SWISH::Prog::KSx::Results 0.18
SWISH::Prog::KSx::Searcher 0.18
SWISH::Prog::Native::Indexer 0.50
SWISH::Prog::Native::InvIndex 0.50
SWISH::Prog::Native::Result 0.50
SWISH::Prog::Native::Searcher 0.50
SWISH::Prog::Queue 0.50
SWISH::Prog::Result 0.50
SWISH::Prog::Results 0.50
IO::Socket::IP 0.07
SWISH::Prog::Searcher 0.50
SWISH::Prog::Utils 0.50
Sakai::Nakamura::Authn 0.03
Sakai::Nakamura::AuthnUtil 0.03
Sakai::Nakamura::Content 0.03
Sakai::Nakamura::Group 0.03
Sakai::Nakamura::LDAPSynch 0.03
Sakai::Nakamura::User 0.03
Sakai::Nakamura::UserUtil 0.03
Scaffold::Base 0.01
Scaffold::Cache 0.01
Scaffold::Cache::FastMmap 0.01
Scaffold::Cache::Manager 0.01
Scaffold::Cache::Memcached 0.01
Scaffold::Class undef
Scaffold::Constants undef
Scaffold::Engine 0.01
Scaffold::Handler 0.02
Scaffold::Handler::Default 0.01
Scaffold::Handler::ExtDirect 0.01
Scaffold::Handler::ExtPoll 0.01
Scaffold::Handler::Favicon 0.01
Scaffold::Handler::Robots 0.01
Scaffold::Handler::Static 0.01
Scaffold::Lockmgr 0.01
Scaffold::Lockmgr::KeyedMutex 0.01
Scaffold::Lockmgr::UnixMutex 0.02
Scaffold::Plugins 0.01
Scaffold::Render 0.01
Scaffold::Render::Default 0.01
Scaffold::Render::TT 0.01
Scaffold::Routes 0.01
Scaffold::Server 0.02
Scaffold::Session::Manager 0.01
Scaffold::Session::Store::Cache 0.01
Scaffold::Stash 0.01
Scaffold::Stash::Controller 0.01
Scaffold::Stash::Cookies 0.01
Scaffold::Stash::Manager 0.01
Scaffold::Stash::View 0.01
Scaffold::Uaf::Authenticate 0.03
Scaffold::Uaf::Authorize 0.01
Scaffold::Uaf::AuthorizeFactory undef
Scaffold::Uaf::GrantAllRule undef
Scaffold::Uaf::Login 0.01
Scaffold::Uaf::Logout 0.01
Scaffold::Uaf::Manager 0.02
Scaffold::Uaf::Rule 0.01
Scaffold::Uaf::User 0.01
Scaffold::Utils 0.01
Scalar::Random::PP::OO 0.70
Scalar::Util 1.23_03
Scalar::Util::PP 1.23
MooseX::Workers
Schedule::Pluggable::Config undef
Schedule::Pluggable::EventHandler undef
Schedule::Pluggable::Plugin::DefaultEventHandler undef
Schedule::Pluggable::Plugin::JobsFromData undef
Schedule::Pluggable::Plugin::JobsFromXML undef
Schedule::Pluggable::Plugin::JobsFromXMLTemplate undef
Schedule::Pluggable::Plugin::Trace undef
Schedule::Pluggable::Run undef
Schedule::Pluggable::Status undef
Schema::Kwalify 1.04
Scope::Session::Flyweight 0.01
Scope::Session::Singleton 0.01
String::TT
Array::Unique
Scrappy::Action 0.92111220
Scrappy::Action::Generate 0.92111220
Scrappy::Action::Help 0.92111220
Scrappy::Logger 0.92111220
Scrappy::Plugin 0.92111220
Scrappy::Plugin::RandomProxy 0.92111220
Scrappy::Project 0.92111220
Scrappy::Project::Document 0.92111220
Scrappy::Queue 0.92111220
Scrappy::Scraper 0.92111220
Scrappy::Scraper::Control 0.92111220
Scrappy::Scraper::Parser 0.92111220
Scrappy::Scraper::UserAgent 0.92111220
Scrappy::Session 0.92111220
Script::Toolbox::Util 0.31
Script::Toolbox::Util::Formatter undef
Script::Toolbox::Util::Opt undef
Search::Dict 1.03
Search::GIN::Callbacks 0.08
Search::GIN::Core 0.08
Search::GIN::DelegateToIndexed 0.08
Search::GIN::Driver::Pack 0.08
Search::GIN::Driver::Pack::Delim 0.08
Search::GIN::Driver::Pack::IDs 0.08
Search::GIN::Driver::Pack::Length 0.08
Search::GIN::Driver::Pack::UUID 0.08
Search::GIN::Driver::Pack::Values 0.08
Search::GIN::Driver::TXN 0.08
Search::GIN::Extract::Attributes 0.08
Search::GIN::Extract::Callback 0.08
Search::GIN::Extract::Multiplex 0.08
Search::GIN::Indexable 0.08
Search::GIN::Keys 0.08
Search::GIN::Keys::Deep 0.08
Search::GIN::Keys::Expand 0.08
Search::GIN::Keys::Join 0.08
Search::GIN::Query 0.08
Search::GIN::Query::Attributes 0.08
Search::GIN::Query::Manual 0.08
Search::GIN::Query::Set 0.08
Search::GIN::SelfIDs 0.08
Search::OpenSearch::Engine 0.12
Search::OpenSearch::Facets 0.12
Search::OpenSearch::Response 0.12
Search::OpenSearch::Response::JSON 0.12
Search::OpenSearch::Response::XML 0.12
Search::OpenSearch::Server::Plack 0.05
Search::Query::Clause 0.18
Search::Query::Dialect 0.18
Search::Query::Dialect::KSx::Compiler 0.13
Search::Query::Dialect::KSx::NOTWildcardQuery 0.13
Search::Query::Dialect::KSx::Scorer 0.13
Search::Query::Dialect::KSx::WildcardQuery 0.13
Search::Query::Dialect::Native 0.18
Search::Query::Dialect::SQL 0.18
Search::Query::Dialect::SWISH 0.18
Search::Query::Field 0.18
Search::Query::Field::KSx 0.13
Search::Query::Field::SQL 0.18
Search::Query::Field::SWISH 0.18
Search::Query::Parser 0.18
Search::QueryParser::SQL::Column 0.008
Search::QueryParser::SQL::Query 0.008
Search::Sitemap::Index 2.10
Search::Sitemap::Ping 2.10
Search::Sitemap::Pinger 2.10
Search::Sitemap::Pinger::Ask 2.10
Search::Sitemap::Pinger::Google 2.10
Search::Sitemap::Pinger::Live 2.10
Search::Sitemap::Pinger::Yahoo 2.10
Search::Sitemap::Types 2.10
Search::Sitemap::URL 2.10
Search::Sitemap::URLStore 2.10
Search::Sitemap::URLStore::Memory 2.10
Search::Tools::HeatMap 0.57
Search::Tools::HiLiter 0.57
Search::Tools::Keywords 0.57
Search::Tools::Object 0.57
Search::Tools::Query 0.57
Search::Tools::QueryParser 0.57
Search::Tools::RegEx 0.57
Search::Tools::RegExp 0.57
Search::Tools::RegExp::Keyword 0.57
Search::Tools::RegExp::Keywords 0.57
Search::Tools::Snipper 0.57
Search::Tools::SpellCheck 0.57
Search::Tools::Token 0.57
Search::Tools::TokenList 0.57
Search::Tools::TokenListPP 0.57
Search::Tools::TokenListUtils 0.57
Search::Tools::TokenPP 0.57
Search::Tools::Tokenizer 0.57
Search::Tools::Transliterate 0.57
Search::Tools::UTF8 0.57
Search::Tools::XML 0.57
SelectSaver 1.02
RDF::Notation3
SemanticWeb::OAI::ORE::Agent undef
SemanticWeb::OAI::ORE::Constant undef
SemanticWeb::OAI::ORE::Model undef
SemanticWeb::OAI::ORE::N3 undef
SemanticWeb::OAI::ORE::RDFXML undef
SemanticWeb::OAI::ORE::ReM undef
SemanticWeb::OAI::ORE::TriX undef
Sendmail::PMilter::Context 0.94
Sepia::CPAN undef
Sepia::Debug undef
Sepia::Xref 0.65
Set::Infinite::Arithmetic undef
Set::Infinite::Basic undef
Set::Infinite::_recurrence undef
Set::Object::Weak undef
Set::Scalar::Base undef
Set::Scalar::Null undef
Set::Scalar::Real undef
Set::Scalar::Universe undef
Set::Scalar::Valued undef
Set::Scalar::ValuedUniverse undef
Set::Scalar::Virtual undef
Setup::Dir 0.02
Sys::Filesystem
File::MoreUtil
Sys::Filesystem::MountPoint
File::Trash::FreeDesktop
Perinci::Tx::Manager
Test::Perinci::Tx::Manager
File::Trash::Undoable
Setup::File::Dir 0.05
Setup::Text::Snippet::WithID 0.01
Text::Password::Pronounceable
File::Copy::Undoable
Setup::File
Setup::Unix::Group 0.01
Shell::Perl::Dumper 0.0019
ShiftJIS::Regexp::Class 1.01
ShiftJIS::Regexp::Const 1.01
ShiftJIS::Regexp::Equiv 1.01
ShipIt::Conf undef
ShipIt::ProjectType undef
ShipIt::ProjectType::AutoConf undef
ShipIt::ProjectType::Perl undef
ShipIt::ProjectType::Perl::MakeMaker undef
ShipIt::ProjectType::Perl::ModuleBuild undef
ShipIt::State undef
ShipIt::Step undef
ShipIt::Step::AddToSVNDir undef
ShipIt::Step::ChangeRPMVersion undef
ShipIt::Step::ChangeVersion undef
ShipIt::Step::CheckChangeLog undef
ShipIt::Step::Commit undef
ShipIt::Step::DistTest undef
ShipIt::Step::FindVersion undef
ShipIt::Step::MakeDist undef
ShipIt::Step::Tag undef
ShipIt::Step::UploadCPAN undef
ShipIt::Util undef
ShipIt::VC undef
ShipIt::VC::Git undef
ShipIt::VC::Mercurial undef
ShipIt::VC::SVK undef
ShipIt::VC::SVN undef
Shipwright::Backend undef
Shipwright::Backend::Base undef
Shipwright::Backend::FS undef
Shipwright::Backend::Git undef
Shipwright::Backend::SVK undef
Shipwright::Backend::SVN undef
Shipwright::Base undef
Shipwright::Logger undef
Shipwright::Script undef
Shipwright::Script::Create undef
Shipwright::Script::Defaultbranch undef
Shipwright::Script::Delete undef
Shipwright::Script::Flags undef
Shipwright::Script::Help undef
Shipwright::Script::Import undef
Shipwright::Script::Ktf undef
Shipwright::Script::List undef
Shipwright::Script::Maintain undef
Shipwright::Script::Relocate undef
Shipwright::Script::Rename undef
Shipwright::Script::Requires undef
Shipwright::Script::Update undef
Shipwright::Source undef
Shipwright::Source::Base undef
Shipwright::Source::CPAN undef
Shipwright::Source::Compressed undef
Shipwright::Source::Directory undef
Shipwright::Source::FTP undef
Shipwright::Source::Git undef
Shipwright::Source::HTTP undef
Shipwright::Source::SVK undef
Shipwright::Source::SVN undef
Shipwright::Source::Shipyard undef
Shipwright::Test undef
Shipwright::Util undef
Shipwright::Util::CleanINC undef
Shipwright::Util::PatchModuleBuild undef
Signal::Pending 0.005
Signals::XSIG::Default 0.09
SimpleAPI::Agent 0.02
SimpleDB::Class::Cache 1.0502
SimpleDB::Class::Domain 1.0502
SimpleDB::Class::Exception 1.0502
SimpleDB::Class::Item 1.0502
SimpleDB::Class::ResultSet 1.0502
SimpleDB::Class::Role::Itemized 1.0502
SimpleDB::Class::SQL 1.0502
SimpleDB::Class::Types 1.0502
SimpleDB::Client::Exception 1.0500
Simulation::DiscreteEvent::Event 0.09
Simulation::DiscreteEvent::Generator 0.09
Simulation::DiscreteEvent::NumericState 0.09
Simulation::DiscreteEvent::Recorder 0.09
Simulation::DiscreteEvent::Server 0.09
Simulation::DiscreteEvent::Sink 0.09
Slay::MakerRule 0.06
Sniffer::Connection 0.22
Sniffer::Connection::HTTP 0.22
App::Options
Socialtext::Resting
Socialtext::EditPage 0.04
Socialtext::Resting::DefaultRester 0.02
Socialtext::Resting::Getopt 0.01
Socialtext::Resting::LocalCopy 0.01
Socialtext::Resting::Mock 0.04
Socialtext::Resting::TaggedPages 0.01
Socialtext::Resting::Utils
Socialtext::WikiFixture 0.06
Socialtext::WikiFixture::Null undef
Socialtext::WikiFixture::Selenese 0.02
Socialtext::WikiFixture::TestUtils undef
Socialtext::WikiObject 0.03
Socialtext::WikiObject::Factory 0.01
Socialtext::WikiObject::PreBlock 0.01
Socialtext::WikiObject::TableConfig 0.01
Socialtext::WikiObject::TestPlan 0.03
Socket 1.89
Socialtext::WikiObject::YAML 0.01
Socket::GetAddrInfo::Core 0.21
Socket::GetAddrInfo::Emul 0.21
Socket::GetAddrInfo::Socket6api 0.21
Socket::GetAddrInfo::XS 0.21
Socket::Netlink::Generic 0.03
Software::License::AGPL_3 0.103001
Software::License::Apache_1_1 0.103001
Software::License::Apache_2_0 0.103001
Software::License::Artistic_1_0 0.103001
Software::License::Artistic_2_0 0.103001
Software::License::BSD 0.103001
Software::License::CC0_1_0 0.103001
Software::License::Custom 0.103001
Software::License::FreeBSD 0.103001
Software::License::GFDL_1_2 0.103001
Software::License::GPL_1 0.103001
Software::License::GPL_2 0.103001
Software::License::GPL_3 0.103001
Software::License::LGPL_2_1 0.103001
Software::License::LGPL_3_0 0.103001
Software::License::MIT 0.103001
Software::License::Mozilla_1_0 0.103001
Software::License::Mozilla_1_1 0.103001
Software::License::OpenSSL 0.103001
Software::License::Perl_5 0.103001
Software::License::PostgreSQL 0.103001
Software::License::QPL_1_0 0.103001
Software::License::SSLeay 0.103001
Software::License::Sun 0.103001
Software::License::Zlib 0.103001
Solution::Block 0.0001
Solution::Condition 0.0001
Solution::Context 0.0001
Solution::Document 0.0001
Spreadsheet::WriteExcel::Utility 2.37
Solution::Error 0.0001
Solution::Filter::Standard 0.0001
Solution::Tag 0.0003
Solution::Tag::Assign 0.0003
Solution::Tag::Capture 0.0003
Solution::Tag::Case 0.0001
Solution::Tag::Comment 0.0003
Solution::Tag::Cycle 0.0003
Solution::Tag::For 0.0004
Solution::Tag::If 0.0001
Solution::Tag::Include 0.0003
Solution::Tag::Unless 0.0001
Solution::Template 0.0001
Solution::Utility 0.0003
Solution::Variable 0.0004
Spoon::Base undef
Spoon::CGI undef
Spoon::Command undef
Spoon::Config undef
Spoon::ContentObject undef
Spoon::Cookie undef
Spoon::DataObject undef
Spoon::Formatter undef
Spoon::Headers undef
Spoon::Hooks undef
Spoon::Hub undef
Spoon::IndexList undef
Spoon::Installer undef
Spoon::MetadataObject undef
Spoon::Plugin undef
Spoon::Registry undef
Spoon::Template undef
Spoon::Template::TT2 undef
Spoon::Trace undef
Spoon::Utils undef
Spreadsheet::ParseExcel::Cell 0.59
Spreadsheet::ParseExcel::Dump 0.59
Spreadsheet::ParseExcel::FmtDefault 0.59
Spreadsheet::ParseExcel::FmtJapan 0.59
Spreadsheet::ParseExcel::FmtJapan2 0.59
Spreadsheet::ParseExcel::FmtUnicode 0.59
Spreadsheet::ParseExcel::Font 0.59
Spreadsheet::ParseExcel::Format 0.59
Spreadsheet::ParseExcel::SaveParser 0.59
Spreadsheet::ParseExcel::SaveParser::Workbook 0.59
Spreadsheet::ParseExcel::SaveParser::Worksheet 0.59
Spreadsheet::ParseExcel::Utility 0.59
Spreadsheet::ParseExcel::Workbook 0.59
Spreadsheet::ParseExcel::Worksheet 0.59
Data::Table
Spreadsheet::ConvertAA
Spreadsheet::Perl::Arithmetic 0.02
Spreadsheet::Wright::CSV 0.102
Spreadsheet::Wright::Excel 0.102
Spreadsheet::Wright::HTML 0.102
Spreadsheet::Wright::JSON 0.102
Spreadsheet::Wright::OpenDocument 0.102
Spreadsheet::Wright::OpenDocumentXML 0.102
Spreadsheet::Wright::XHTML 0.102
Spreadsheet::WriteExcel::BIFFwriter 2.37
Spreadsheet::WriteExcel::Big 2.37
Spreadsheet::WriteExcel::Chart 2.37
Spreadsheet::WriteExcel::Chart::Area 2.37
Spreadsheet::WriteExcel::Chart::Bar 2.37
Spreadsheet::WriteExcel::Chart::Column 2.37
Spreadsheet::WriteExcel::Chart::External 2.37
Spreadsheet::WriteExcel::Chart::Line 2.37
Spreadsheet::WriteExcel::Chart::Pie 2.37
Spreadsheet::WriteExcel::Chart::Scatter 2.37
Spreadsheet::WriteExcel::Chart::Stock 2.37
Spreadsheet::WriteExcel::Examples 2.37
Spreadsheet::WriteExcel::Format 2.37
Spreadsheet::WriteExcel::Formula 2.37
Spreadsheet::WriteExcel::OLEwriter 2.37
Spreadsheet::WriteExcel::Properties 2.37
Spreadsheet::WriteExcel::Workbook 2.37
Spreadsheet::WriteExcel::Worksheet 2.37
Spreadsheet::WriteExcelXML::Format 0.13
Spreadsheet::WriteExcelXML::Utility 0.13
Spreadsheet::WriteExcelXML::Workbook 0.13
Spreadsheet::WriteExcelXML::Worksheet 0.13
Spreadsheet::WriteExcelXML::XMLwriter 0.13
Squatting::Controller undef
Squatting::H undef
Squatting::Mapper undef
Squatting::On::CGI undef
Squatting::On::Catalyst undef
Squatting::On::Continuity undef
Squatting::On::MP13 undef
Squatting::On::MP20 undef
Squatting::View undef
Squatting::With::AccessTrace undef
Squatting::With::Coro::Debug undef
Squatting::With::Log undef
Squatting::With::MockRequest undef
Squatting::With::Mount undef
Squatting::With::PerHostConfig undef
Squid::Guard::Request 0.15
Squirrel undef
Squirrel::Role undef
Starlet::Server undef
Starman::Server undef
Stash undef
Statistics::Basic::ComputedVector undef
Statistics::Basic::Correlation undef
Statistics::Basic::Covariance undef
Statistics::Descriptive
Statistics::PointEstimation 1.1
Statistics::Basic::LeastSquareFit undef
Statistics::Basic::Mode undef
Statistics::Basic::Variance undef
Statistics::Basic::Vector undef
Statistics::Basic::_OneVectorBase undef
Statistics::Basic::_TwoVectorBase undef
Statistics::R::Bridge 0.07
Array::Compare
Statistics::Data
Statistics::Lite
String::Numeric
Statistics::Sequences
Statistics::Zed
Statistics::Sequences::Joins 0.051
Statistics::Sequences::Pot 0.051
Statistics::Sequences::Runs 0.051
Statistics::Sequences::Turns 0.02
Statistics::Sequences::Vnomes 0.04
Storable::AMF0 0.95
String::FlexMatch::Test 1.100820
Storable::AMF3 0.95
Storable::AMF::Mapper undef
String::BooleanSimple 0.02
String::Comments::Extract::C undef
String::Comments::Extract::CPP undef
String::Comments::Extract::Java undef
String::Comments::Extract::JavaScript undef
String::Comments::Extract::SlashStar undef
String::Formatter::Cookbook 0.102082
Su::Log undef
Su::Model undef
Su::Process undef
Su::Template undef
Sub::Chain::Named 0.010012
Sub::Exporter::Util 0.983
Sub::Spec::Clause::args 0.13
Sub::Spec::Clause::args_as 0.13
Sub::Spec::Clause::deps 0.13
Sub::Spec::Clause::features 0.13
Sub::Spec::Clause::result 0.13
Sub::Spec::Clause::result_naked 0.13
Sub::Spec::Clause::retry 0.13
Sub::Spec::Clause::statuses 0.13
Sub::Spec::Clause::timeout 0.13
Sub::Spec::Exporter 0.13
Sub::Spec::Runner::State 0.12
Sub::Spec::Utils 0.13
Set::Light
File::Pid
Supervisor::Base 0.06
Supervisor::Class undef
Supervisor::Constants undef
Supervisor::Controller 0.02
Supervisor::Log undef
Supervisor::Process 0.03
Supervisor::ProcessFactory 0.02
Supervisor::RPC::Client 0.02
Supervisor::RPC::Server 0.02
Supervisor::Session 0.02
Supervisor::Utils undef
SweetPea::Application::Builder undef
SweetPea::Application::Config undef
SweetPea::Application::Data undef
SweetPea::Application::Devel undef
SweetPea::Application::Email undef
SweetPea::Application::Json undef
SweetPea::Application::Locale undef
SweetPea::Application::Model 0.001
SweetPea::Application::Orm undef
SweetPea::Application::Plugin::Ajax::Jquery undef
SweetPea::Application::Rbac 0.001
SweetPea::Application::Template undef
SweetPea::Application::Validate undef
SweetPea::Application::View 0.001
SweetPea::Cli::Data undef
SweetPea::Cli::Error undef
SweetPea::Cli::Flash undef
SweetPea::Cli::Help undef
SweetPea::Cli::Make undef
SweetPea::Cli::Mvc undef
Syntax::Feature::Junction 0.002001
SweetPea::Cli::Tool undef
SweetPea::Cli::Util undef
Symbol 1.07
Syntax::Feature::Gather 1.001000
Syntax::Highlight::Engine::Kate::ABC 0.06
Syntax::Highlight::Engine::Kate::AHDL 0.06
Syntax::Highlight::Engine::Kate::ANSI_C89 0.06
Syntax::Highlight::Engine::Kate::ASP 0.06
Syntax::Highlight::Engine::Kate::AVR_Assembler 0.06
Syntax::Highlight::Engine::Kate::AWK 0.06
Syntax::Highlight::Engine::Kate::Ada 0.06
Syntax::Highlight::Engine::Kate::Alerts 0.06
Syntax::Highlight::Engine::Kate::All 0.06
Syntax::Highlight::Engine::Kate::Ansys 0.06
Syntax::Highlight::Engine::Kate::Apache_Configuration 0.06
Syntax::Highlight::Engine::Kate::Asm6502 0.06
Syntax::Highlight::Engine::Kate::Bash 0.06
Syntax::Highlight::Engine::Kate::BibTeX 0.06
Syntax::Highlight::Engine::Kate::C 0.06
Syntax::Highlight::Engine::Kate::CGiS 0.06
Syntax::Highlight::Engine::Kate::CMake 0.06
Syntax::Highlight::Engine::Kate::CSS 0.06
Syntax::Highlight::Engine::Kate::CSS_PHP 0.06
Syntax::Highlight::Engine::Kate::CUE_Sheet 0.06
Syntax::Highlight::Engine::Kate::Cdash 0.06
Syntax::Highlight::Engine::Kate::Cg 0.06
Syntax::Highlight::Engine::Kate::ChangeLog 0.06
Syntax::Highlight::Engine::Kate::Cisco 0.06
Syntax::Highlight::Engine::Kate::Clipper 0.06
Syntax::Highlight::Engine::Kate::ColdFusion 0.06
Syntax::Highlight::Engine::Kate::Common_Lisp 0.06
Syntax::Highlight::Engine::Kate::ComponentminusPascal 0.06
Syntax::Highlight::Engine::Kate::Cplusplus 0.06
Syntax::Highlight::Engine::Kate::D 0.06
Syntax::Highlight::Engine::Kate::De_DE 0.06
Syntax::Highlight::Engine::Kate::Debian_Changelog 0.06
Syntax::Highlight::Engine::Kate::Debian_Control 0.06
Syntax::Highlight::Engine::Kate::Desktop 0.06
Syntax::Highlight::Engine::Kate::Diff 0.06
Syntax::Highlight::Engine::Kate::Doxygen 0.06
Syntax::Highlight::Engine::Kate::E_Language 0.06
Syntax::Highlight::Engine::Kate::Eiffel 0.06
Syntax::Highlight::Engine::Kate::Email 0.06
Syntax::Highlight::Engine::Kate::En_US 0.06
Syntax::Highlight::Engine::Kate::Euphoria 0.06
Syntax::Highlight::Engine::Kate::Ferite 0.06
Syntax::Highlight::Engine::Kate::Fortran 0.06
Syntax::Highlight::Engine::Kate::FourGL 0.06
Syntax::Highlight::Engine::Kate::FourGLminusPER 0.06
Syntax::Highlight::Engine::Kate::FreeBASIC 0.06
Syntax::Highlight::Engine::Kate::GDL 0.06
Syntax::Highlight::Engine::Kate::GLSL 0.06
Syntax::Highlight::Engine::Kate::GNU_Assembler 0.06
Syntax::Highlight::Engine::Kate::GNU_Gettext 0.06
Syntax::Highlight::Engine::Kate::HTML 0.06
Syntax::Highlight::Engine::Kate::Haskell 0.06
Syntax::Highlight::Engine::Kate::IDL 0.06
Syntax::Highlight::Engine::Kate::ILERPG 0.06
Syntax::Highlight::Engine::Kate::INI_Files 0.06
Syntax::Highlight::Engine::Kate::Inform 0.06
Syntax::Highlight::Engine::Kate::Intel_x86_NASM 0.06
Syntax::Highlight::Engine::Kate::JSP 0.06
Syntax::Highlight::Engine::Kate::Java 0.06
Syntax::Highlight::Engine::Kate::JavaScript 0.06
Syntax::Highlight::Engine::Kate::JavaScript_PHP 0.06
Syntax::Highlight::Engine::Kate::Javadoc 0.06
Syntax::Highlight::Engine::Kate::KBasic 0.06
Syntax::Highlight::Engine::Kate::Kate_File_Template 0.06
Syntax::Highlight::Engine::Kate::LDIF 0.06
Syntax::Highlight::Engine::Kate::LPC 0.06
Syntax::Highlight::Engine::Kate::LaTeX 0.06
Syntax::Highlight::Engine::Kate::Lex_Flex 0.06
Syntax::Highlight::Engine::Kate::LilyPond 0.06
Syntax::Highlight::Engine::Kate::Literate_Haskell 0.06
Syntax::Highlight::Engine::Kate::Logtalk 0.06
Syntax::Highlight::Engine::Kate::Lua 0.06
Syntax::Highlight::Engine::Kate::M3U 0.06
Syntax::Highlight::Engine::Kate::MABminusDB 0.06
Syntax::Highlight::Engine::Kate::MIPS_Assembler 0.06
Syntax::Highlight::Engine::Kate::Makefile 0.06
Syntax::Highlight::Engine::Kate::Mason 0.06
Syntax::Highlight::Engine::Kate::Matlab 0.06
Syntax::Highlight::Engine::Kate::Modulaminus2 0.06
Syntax::Highlight::Engine::Kate::Music_Publisher 0.06
Syntax::Highlight::Engine::Kate::Nl 0.06
Syntax::Highlight::Engine::Kate::Objective_Caml 0.06
Syntax::Highlight::Engine::Kate::ObjectiveminusC 0.06
Syntax::Highlight::Engine::Kate::Octave 0.06
Syntax::Highlight::Engine::Kate::PHP_HTML 0.06
Syntax::Highlight::Engine::Kate::PHP_PHP 0.06
Syntax::Highlight::Engine::Kate::POVminusRay 0.06
Syntax::Highlight::Engine::Kate::Pascal 0.06
Syntax::Highlight::Engine::Kate::Perl 0.06
Syntax::Highlight::Engine::Kate::PicAsm 0.06
Syntax::Highlight::Engine::Kate::Pike 0.06
Syntax::Highlight::Engine::Kate::PostScript 0.06
Syntax::Highlight::Engine::Kate::Progress 0.06
Syntax::Highlight::Engine::Kate::Prolog 0.06
Syntax::Highlight::Engine::Kate::PureBasic 0.06
Syntax::Highlight::Engine::Kate::Python 0.06
Syntax::Highlight::Engine::Kate::Quake_Script 0.06
Syntax::Highlight::Engine::Kate::REXX 0.06
Syntax::Highlight::Engine::Kate::RPM_Spec 0.06
Syntax::Highlight::Engine::Kate::RSI_IDL 0.06
Syntax::Highlight::Engine::Kate::R_Script 0.06
Syntax::Highlight::Engine::Kate::RenderMan_RIB 0.06
Syntax::Highlight::Engine::Kate::Ruby 0.06
Syntax::Highlight::Engine::Kate::SGML 0.06
Syntax::Highlight::Engine::Kate::SML 0.06
Syntax::Highlight::Engine::Kate::SQL 0.06
Syntax::Highlight::Engine::Kate::SQL_MySQL 0.06
Syntax::Highlight::Engine::Kate::SQL_PostgreSQL 0.06
Syntax::Highlight::Engine::Kate::Sather 0.06
Syntax::Highlight::Engine::Kate::Scheme 0.06
Syntax::Highlight::Engine::Kate::Scilab 0.06
Syntax::Highlight::Engine::Kate::Sieve 0.06
Syntax::Highlight::Engine::Kate::Spice 0.06
Syntax::Highlight::Engine::Kate::Stata 0.06
Syntax::Highlight::Engine::Kate::TI_Basic 0.06
Syntax::Highlight::Engine::Kate::TaskJuggler 0.06
Syntax::Highlight::Engine::Kate::Tcl_Tk 0.06
Syntax::Highlight::Engine::Kate::Template 0.06
Syntax::Highlight::Engine::Kate::ToolKit 0.06
Syntax::Highlight::Engine::Kate::Txt2tags 0.06
Syntax::Highlight::Engine::Kate::UnrealScript 0.06
Syntax::Highlight::Engine::Kate::VHDL 0.06
Syntax::Highlight::Engine::Kate::VRML 0.06
Syntax::Highlight::Engine::Kate::Velocity 0.06
Syntax::Highlight::Engine::Kate::Verilog 0.06
Syntax::Highlight::Engine::Kate::WINE_Config 0.06
Syntax::Highlight::Engine::Kate::Wikimedia 0.06
Syntax::Highlight::Engine::Kate::XHarbour 0.06
Syntax::Highlight::Engine::Kate::XML 0.06
Syntax::Highlight::Engine::Kate::XMLData 0.06
Syntax::Highlight::Engine::Kate::XML_Debug 0.06
Syntax::Keyword::Junction::All 0.002001
Syntax::Keyword::Junction::Any 0.002001
Syntax::Highlight::Engine::Kate::Xorg_Configuration 0.06
Syntax::Keyword::Junction::None 0.002001
Syntax::Keyword::Junction::One 0.002001
Syntax::Highlight::Engine::Kate::Xslt 0.06
Syntax::Highlight::Engine::Kate::Yacas 0.06
Syntax::Highlight::Engine::Kate::Yacc_Bison 0.06
Syntax::Keyword::Junction::Base 0.002001
Sys::CpuLoadX 0.02
Sys::Filesystem::Aix 1.30
Sys::Filesystem::Cygwin 1.30
Sys::Filesystem::Darwin 1.30
Sys::Filesystem::Dummy 1.30
Sys::Filesystem::Freebsd 1.30
Sys::Filesystem::Hpux 1.30
Sys::Filesystem::Linux 1.30
Sys::Filesystem::Mswin32 1.30
Sys::Filesystem::Netbsd 1.30
Sys::Filesystem::Solaris 1.30
Sys::Filesystem::Unix 1.30
Sys::Hostname 1.11
Sys::Info::Constants 0.73
Sys::Info::Device 0.73
Sys::Info::Device::CPU 0.73
Sys::Info::Driver 0.73
Sys::Info::Driver::Linux::Device 0.78
Sys::Info::Driver::Linux::Device::CPU 0.78
Sys::Info::Driver::Linux::OS 0.78
Sys::Info::Driver::Linux::OS::Distribution 0.78
Sys::Info::Driver::Linux::OS::Distribution::Conf 0.78
Sys::Info::Driver::Unknown::Device 0.78
Sys::Info::Driver::Unknown::Device::CPU 0.78
Sys::Info::Driver::Unknown::Device::CPU::Env 0.78
Sys::Info::Driver::Unknown::OS 0.78
Sys::Info::OS 0.73
Sys::Statistics::Linux::Compilation 0.10
Sys::Statistics::Linux::CpuStats 0.20
Sys::Statistics::Linux::DiskStats 0.24
Sys::Statistics::Linux::DiskUsage 0.14
Sys::Statistics::Linux::FileStats 0.09
Sys::Statistics::Linux::LoadAVG 0.08
Sys::Statistics::Linux::MemStats 0.16
Sys::Statistics::Linux::NetStats 0.20
Sys::Statistics::Linux::PgSwStats 0.18
Sys::Statistics::Linux::ProcStats 0.20
Sys::Statistics::Linux::Processes 0.34
Sys::Statistics::Linux::SockStats 0.09
Sys::Statistics::Linux::SysInfo 0.13
Sys::Trace::Impl::Ktrace undef
Sys::Trace::Impl::Strace undef
Sys::Trace::Impl::Truss undef
Sys::Trace::Results undef
System::Command::Reaper 1.01
SystemC::Coverage 1.336
SystemC::Coverage::Item 1.336
SystemC::Coverage::ItemKey 1.336
SystemC::Netlist 1.336
SystemC::Netlist::AutoCover 1.336
SystemC::Netlist::AutoTrace 1.336
SystemC::Netlist::Cell 1.336
SystemC::Netlist::Class 1.336
SystemC::Netlist::CoverGroup 1.336
SystemC::Netlist::CoverPoint 1.336
SystemC::Netlist::File 1.336
SystemC::Netlist::Method 1.336
SystemC::Netlist::Module 1.336
SystemC::Netlist::Net 1.336
SystemC::Netlist::Pin 1.336
SystemC::Netlist::Port 1.336
SystemC::Parser 1.336
SystemC::Template 1.336
TAP::Base 3.23
TAP::DOM::Config undef
TAP::DOM::Entry undef
TAP::DOM::Summary undef
TAP::Formatter::Base 3.23
TAP::Formatter::Color 3.23
TAP::Formatter::Console 3.23
TAP::Formatter::Console::ParallelSession 3.23
TAP::Formatter::Console::Session 3.23
TAP::Formatter::File 3.23
TAP::Formatter::File::Session 3.23
TAP::Formatter::HTML::Session 0.09
TAP::Formatter::Session 3.23
TAP::Formatter::TextMate::Session 0.1
TAP::Harness 3.23
TAP::Object 3.23
TAP::Parser 3.23
TAP::Parser::Aggregator 3.23
TAP::Parser::Grammar 3.23
TAP::Parser::Iterator 3.23
TAP::Parser::Iterator::Array 3.23
TAP::Parser::Iterator::Process 3.23
TAP::Parser::Iterator::Stream 3.23
TAP::Parser::IteratorFactory 3.23
TAP::Parser::Multiplexer 3.23
TAP::Parser::Result 3.23
TAP::Parser::Result::Bailout 3.23
TAP::Parser::Result::Comment 3.23
TAP::Parser::Result::Plan 3.23
TAP::Parser::Result::Pragma 3.23
TAP::Parser::Result::Test 3.23
TAP::Parser::Result::Unknown 3.23
TAP::Parser::Result::Version 3.23
TAP::Parser::Result::YAML 3.23
TAP::Parser::ResultFactory 3.23
TAP::Parser::Scheduler 3.23
TAP::Parser::Scheduler::Job 3.23
TAP::Parser::Scheduler::Spinner 3.23
TAP::Parser::Source 3.23
TAP::Parser::SourceHandler 3.23
TAP::Parser::SourceHandler::Executable 3.23
TAP::Parser::SourceHandler::File 3.23
TAP::Parser::SourceHandler::Handle 3.23
TAP::Parser::SourceHandler::Perl 3.23
TAP::Parser::SourceHandler::RawTAP 3.23
TAP::Parser::YAMLish::Reader 3.23
TAP::Parser::YAMLish::Writer 3.23
TAP::Spec::BailOut 0.05
TAP::Spec::Body 0.05
TAP::Spec::Comment 0.05
TAP::Spec::Footer 0.05
TAP::Spec::Header 0.05
TAP::Spec::JunkLine 0.05
TAP::Spec::Plan 0.05
TAP::Spec::Plan::Simple 0.05
TAP::Spec::Plan::SkipAll 0.05
TAP::Spec::Plan::Todo 0.05
TAP::Spec::TestResult 0.05
TAP::Spec::TestSet 0.05
TAP::Spec::Version 0.05
Exporter::Cluster
TEI::Lite::Document 0.60
TEI::Lite::Element 0.60
TEI::Lite::Header 0.60
TEI::Lite::Utility 0.60
TM::Analysis 0.91
TM::AsTMa::Fact undef
TM::AsTMa::Fact2 undef
Tangence::Message 0.05
TM::Axes 0.2
TM::Bulk 0.5
Tangence::Object 0.05
TM::CTM::CParser undef
TM::CTM::Parser 0.2
TM::Coverage 0.1
TM::DM 0.04
TM::FAQ 0.8
TM::Graph 0.3
TM::Index 0.5
TM::Index::Characteristics 0.1
TM::Index::Match 0.3
TM::Index::Reified 0.4
TM::Index::Taxonomy 0.1
TM::IndexAble 0.7
TM::LTM::CParser undef
TM::LTM::Parser 0.4
TM::Literal 0.1
TM::MapSphere 0.05
TM::Materialized::AsTMa 0.18
TM::Materialized::CTM 0.1
TM::Materialized::JTM 1.2
TM::Materialized::LTM 0.3
TM::Materialized::MLDBM 0.02
TM::Materialized::MLDBM2 0.02
TM::Materialized::Null undef
TM::Materialized::Stream 0.1
TM::Materialized::XTM 0.02
TM::ObjectAble 0.1
TM::Overview 0.3
TM::PSI 0.18
TM::ResourceAble 0.2
TM::ResourceAble::BDB undef
TM::ResourceAble::MLDBM 0.03
TM::ResourceAble::MemCached 0.02
TM::Serializable 0.13
TM::Serializable::AsTMa 0.6
TM::Serializable::CSV 0.02
TM::Serializable::CTM 0.2
TM::Serializable::Dumper undef
TM::Serializable::JTM 1.2
Cog 0.07
TM::Serializable::LTM 0.3
TM::Serializable::Summary undef
TM::Serializable::XTM 0.03
TM::Synchronizable 0.3
TM::Synchronizable::MLDBM 0.03
TM::Synchronizable::MapSphere 0.02
TM::Synchronizable::Null undef
TM::Tau 1.15
TM::Tau::Federate undef
TM::Tau::Filter 0.4
TM::Tau::Filter::Analyze 0.2
TM::Tree 0.4
TM::Utils 1.04
TM::Utils::TreeWalker undef
TM::Workbench::Plugin undef
Data::Decode 0.00006
TM::Workbench::Plugin::Tau undef
TM::Workbench::Plugin::Test undef
TMDB::Movie undef
TMDB::Person undef
TMDB::Search undef
TMDB::Session undef
TSVRPC::Client 0.01
TSVRPC::Parser undef
TSVRPC::Util undef
TUWF::DB 0.1
TUWF::Misc 0.1
TUWF::Request 0.1
TUWF::Response 0.1
TUWF::XML 0.1
Table::Simple::Column undef
Table::Simple::Output undef
Table::Simple::Output::Theory undef
Pid::File::Flock
Tail::Stat::Plugin undef
Tail::Stat::Plugin::apache undef
Tail::Stat::Plugin::clamd undef
Config::File 1.50
Tail::Stat::Plugin::cvsupd undef
Tail::Stat::Plugin::nginx undef
Tail::Stat::Plugin::spamd undef
Tangence::Constants 0.05
Tangence::Meta::Class 0.05
Tangence::Metacode 0.05
IO::Event::AnyEvent undef
IO::Event::Callback undef
IO::Event::Emulate undef
Locale::Maketext 1.17
IO::Event::Event undef
Locale::Codes 3.16
Devel::GlobalDestruction::XS
Image::Hash
MooX::POE
PDL::VectorValued
PDL::CCS
JSON::SL
Import::Base
DateTime::Moonpig
Pod::Usage::Return
HTML::Lint::Pluggable
Statocles
Test::Map::Tube
Map::Tube::Berlin
Test::SVN::Repo
AnyEvent::Open3::Simple
Try::Catch
AnyEvent::ProcessPool
Coro::Countdown
Coro::ProcessPool
Coro::Multicore
Config::MethodProxy
MooX::BuildArgs
Games::Dukedom
Net::Moip
Module::Build::Prereqs::FromCPANfile
BusyBird::DateTime::Format
RedisDB::Parser
Continuity 1.4
App::gh
Devel::Modlist
App::Changelog2x
Daemon::Control
PPI::Prettify
Pod::S5
Test::AgainstSchema
Text::Textile::Plaintext
Task::BeLike::RJRAY
IPC::LeaderBoard
URI::redis
RedisDB
Catalyst::Model::Redis
Data::EventStream
File::Flock::Tiny
FileCache::Appender
Number::Closest::XS
Simulation::DiscreteEvent
Test::Syntax::Aggregate
DBIx::Deployer
File::ShareDir::Dist
Dist::Zilla::Plugin::Author::Plicease
MooseX::Role::Nameable
Set::Functional
MooseX::Role::Hashable
Sub::IsEqual
MooseX::Role::Defaultable
Keyword::Pluggable
DBIx::Perlish
Math::Shape::Vector
Inline::Struct
Alien::FFI
FFI::Platypus
RDF::Helper::Properties
RDF::TrineX::Compatibility::Attean
RDF::Trine::Serializer::RDFa
Plack::Middleware::CrossOrigin
Plack::Middleware::Expires
RDF::Endpoint
App::perlrdf
RDF::Generator::Void
RDF::LinkedData
DBIx::Class::InflateColumn::Serializer::Sereal
Geo::Calc::XS
Mojolicious::Plugin::NYTProf
Activiti::Rest::Client
Marpa::R3
qbit
QBit::Class
QBit::TimeLog
QBit::TimeLog::XS
String::ProgressBar
Mojolicious::Plugin::RelativeUrlFor
Mojolicious::Plugin::Subdispatch
Mojolicious::Plugin::AssetPack
Mojolicious::Plugin::PODViewer
Contenticious
XML::LibXML::Cache
MediaWiki::CleanupHTML
Rstats
Mustache::Simple
Applify
DBIx::Class::InflateColumn::Authen::Passphrase 0.01
Test::Lives
Object::Properties
goto::file
Test2::Plugin::MemUsage
Test2::Plugin::UUID
Test2::Harness
Tie::Hash::ReadonlyStack
Sub::StrictDecl
JavaScript::Duktape
Sub::Clone
CloudApp::REST 0.02
Code::Class::C 0.05
Code::Explain 0.02
CogWiki 0.02
Daemon::Generic 0.71
Collection 0.47
Collision::2D 0.07
Color::Model::Munsell 0.02
Color::Model::RGB 1.02
Color::Model::Munsell::Util 0.03
Algorithm::Loops
Sidef
IO::Interactive::Tiny
Locales
Web::Detect
Locale::Maketext::Utils
Dancer2::Plugin
Dancer2::Plugin::Locale
Crypt::Mode::CBC
Crypt::Mode::ECB
Color::Spectrum 1.06
DBIx::Class::TopoSort
Data::Walk
DBIx::Class::Sims
Test::Sims
Compare::Directory 1.14
Compress::LZMA::External 0.36
Compress::Raw::Lzma 2.034
Compress::unLZMA 0.04
ConditionSystem 0.02
Config::Ant 0.01
Config::Crontab 1.33
Config::Divide 0.031
Config::Hosts 0.01
Config::INI 0.017
Config::IPFilter 1.00
DateTime::Util::Calc
DateTime::Util::Astro 0.12000
Config::JSON 1.5100
Config::Merge 1.01
Config::Model::OpenSsh 1.216
Config::OpenSSH::Authkey 0.99
Config::Path 0.11
Config::Perl::V 0.12
Config::Properties::Simple 0.14
Config::Tiny 2.14
Config::Tiny::Ordered 1.02
Exporter::NoWork
Config::TinyDNS 1
Config::ZOMG 0.001000
Config::XPath 0.16
Config::YAML::Tiny 1.42.0
Contextual::Return 0.2.1
ControlFreak 1.0.0
Convert::Binary::C 0.76
Convert::Color::Library 0.03
Convert::EastAsianWidth 1.02
Convert::GeekCode 0.62
Convert::Moji 0.02
Convert::Scalar 1.04
Convert::UUlib 1.34
Convert::yEnc 1.04
Coro::Generator 0.3.0
Coro::Mysql 1.02
Crypt::CVS 0.03
Crypt::Camellia 2.02
DBIx::Class::I18NColumns 0.15
DateTime::Format::Duration::XSD 0.01
DBIx::Class::InflateColumn::DateTime::Duration 0.01002
DBIx::Class::Journal 0.900200
DBIx::Class::MooseColumns 0.20
DBIx::Class::Numeric 0.004
DBIx::Class::PassphraseColumn 0.01
DBIx::Class::RandomColumns 0.003000
DBIx::Class::Relationship::Predicate 0.03
DBIx::Class::Result::ExternalAttribute 0.01
DBIx::DBHResolver 0.13
DBIx::Class::Result::ProxyField 0.03
DBIx::Class::ResultSet::Faceter 0.04
MySQL::Backup
DBIx::Class::Storage::DBI::mysql::backup 0.04
DBIx::Class::Tree 0.03003
DBIx::Class::Tree::NestedSet 0.07
DBIx::Class::Validation 0.02005
DBIx::Connect::FromConfig 0.05
DBIx::DBH 0.4
DBIx::DBHResolver::Strategy::RoundRobin 0.02
Digest::HMAC_MD6 0.01
DBIx::DBSchema 0.39
DBIx::DataModel 1.26
DBIx::DoMore 0.01003
DBIx::FileStore 0.13
DBIx::HA 1.1
DBIx::HTML::ClientDB 1.08
DBIx::HTML::LinkedMenus 1.10
DBIx::HTML::PopupRadio 1.16
DBIx::Hash2Table 2.04
DBIx::Log4perl 0.23
DBIx::Lookup::Field 2.101420
DBIx::MSAccess::Convert2Db 1.08
DBIx::Migration 0.07
DBIx::Migration::Classes 0.02
DBIx::MultiDB 0.04
DBIx::NoSQL 0.0017
DBIx::ObjectMapper 0.3013
DBIx::Printf 0.08
DBIx::Printf::Named 0.01
DBIx::QueryByName 0.18
DBIx::QueryLog 0.10
Dist::Zilla::Plugin::CompileTests 1.110930
SQL::Script
DBIx::Report::Excel 0.2a
DBIx::ResultSet 0.14
DBIx::RetryOverDisconnects 0.06
DBIx::RewriteDSN 0.04
Directory::Scratch::Structured 0.04
DBIx::RoboQuery 0.012025
DBIx::SQLEngine 0.93
DBIx::SQLite::Simple 0.34
DBIx::Schema::UpToDate 0.005
DBIx::SearchBuilder 1.54
DBIx::Simple::DataSection 0.02
DBIx::Simple::OO 0.02
DBIx::SimpleGoBetween 1.003
DBIx::SimpleQuery 0.04
DBIx::Skinny::Mixin::DBHResolver 0.04
DBIx::Skinny::Schema::Loader 0.20
DBIx::Sunny 0.06
Data::Dump::Partial 0.03
DBIx::TableLoader 1.000
DBIx::Table2Hash 1.17
DBIx::TableLoader::CSV 1.002
DBIx::TextIndex 0.28
DBIx::Tree 1.94
DBR unknown
DFA::Command 2.01
Pithub
DNS::Oterica 0.100001
DNS::ZoneEdit 1.1
DNS::ZoneParse 1.10
Text::ANSI::Util
Data::Unixish
Data::Format::Pretty::Console 0.07
DOCSIS::ConfigFile 0.6004
Daemon::Easy 0.02
Daemon::Mplayer 0.010
Daiku 0.02
Dancer::Logger::LogHandler 0.01
Dancer::Plugin::Auth::RBAC 1.110720
Dancer::Plugin::Authorize 1.110720
Dancer::Plugin::Cache 0.2.1
Dancer::Plugin::Captcha::SecurityImage 0.10
Dancer::Plugin::DataFu 1.103070
Dancer::Hook
Dancer::Plugin::Facebook 0.001
Dancer::Plugin::Fake::Response 0.0101
Dancer::Plugin::MPD 0.01
Dancer::Plugin::ORMesque 0.0101
Dancer::Plugin::SporeDefinitionControl 0.07
Dancer::Plugin::ValidationClass undef
Dancer::Plugin::WebSocket 0.01
Dancer::Template::Semantic 0.01
Danga::Socket::Callback 0.01200
Danga::Socket::Redis 0.06
Darcs::Inventory 1.5
Dash::Leak 0.06
Data::AMF 0.09
Data::ArrayList 0.01
Data::Babel::Client 0.01_05
Data::Beacon 0.2.5
Data::Bucketeer 0.001
Math::Int2Base
Data::Bvec 1.01
Data::CGIForm 0.5
Data::Capture 0.27
Data::CloudWeights 0.6.122
Data::Collector 0.12
Data::Conveyor 1.103130
Data::ConveyorBelt 0.02
Data::DPath::Validator 0.093411
Data::Decycle 0.02
Data::Deduper 0.02
Data::Default 0.11
Data::Dmap 0.08
Data::Dump::PHP 0.07
Data::DumpXML 1.06
Data::Dumper 2.128
Data::Dumper::Perltidy 0.01
Data::Dumper::Simple 0.11
Data::Encoder 0.03
Data::Entropy 0.007
Data::GUID 0.046
Data::Google::Visualization::DataTable 0.07
Data::Handle 0.01011703
Data::HexDump 0.02
Data::Hive 1.008
Data::ID::Exim 0.007
Data::ID::Maildir 0.003
Data::Localize 0.00020
Data::IPV4::Range::Parse 1.05
Data::Localize::Storage::MongoDB undef
Data::Maker 0.27
Data::Model 0.00007
Data::Money 0.04
Data::Morph 1.110540
Data::OpenGraph 0.01
DataFlow 1.111230
Data::OptList 0.106
Data::PABX::ParseLex 1.04
Data::Page::FlickrLike 2.00
Data::Page::Navigation 0.05
Data::StreamDeserializer 0.06
Set::Window
Data::Page::Viewport 1.06
Data::ParseBinary 0.31
Data::Password::Check 0.08
Data::Password::Entropy 0.06
Data::PowerSet 0.05
Data::PowerSet::Hash 0.02
Data::Rx 0.100110
Data::Predicate 2.1.1
Data::Rx::TypeBundle::Perl 0.004
Data::PrettyPrintObjects 1.00
Data::Range::Compare 1.030
Email::ARF 0.005
Data::ResultSet 1.001
Data::Rx::Tools::ShareDirValidator 0.1.0
Data::Rx::TypeBundle::Rx 0.103520
Data::SearchEngine 0.19
Data::Schema 0.133
Data::SearchEngine::Solr 0.17
Data::Semantic::Net 1.101760
Data::Serializable 0.40.1
Data::Sheet 0.01
Data::Show 0.001_004
Data::StreamSerializer 0.07
Data::TUID 0.0122
Data::Tabular::Dumper 0.08
Data::Tabulate 0.06
Data::Tabulate::Plugin::HTMLTable 0.03
Data::Throttler 0.05
Data::Thunk 0.07
Data::TreeDumper::Renderer::GTK 0.02
Data::TreeValidator 0.03
Data::Typed::Expression 0.003
Data::UNLreport 1.03
Date::ISO8601 0.004
DateTime::TimeZone::SystemV 0.004
DateTime::TimeZone::Tzfile 0.005
Data::UUID 1.217
Data::Validate::Date 0.01
Desktop::Notify 0.03
Data::Validate::Image 0.003
Data::Verifier 0.45
Data::YAML 0.0.6
Data::YUID 0.05
Data::Zipper 0.02
Date::Chinese 1.12
DateTime::Calendar::Japanese::Era 0.08001
Date::Easter 1.14
Date::Extract::Surprise 0.006
Date::FromToday 0.02
Date::HolidayParser 0.41
DateTime::Calendar::Julian 0.04
DateTime::Event::Easter 1.04
Date::Holidays::Abstract 0.05
Date::Holidays::CA 0.03
Date::Holidays::DE 1.5
Date::Holidays::EnglandWales 0.07
Date::Holidays::IND 0.03
Date::Holidays::PAK 0.03
Date::Holidays::PL 1.110050
Date::Holidays::UK::EnglandAndWales 0.03
Date::MSAccess 1.05
DateTime::Astro
DateTime::Event::Chinese 0.05
DateTime::Calendar::Chinese 0.07
Date::MSD 0.003
Date::Pcalc 6.1
Date::Remind::Event 0.04
Date::Say::Czech 0.04
Date::Span 1.125
DateTime::Event::Sunrise 0.0501
DateTime::Calendar::FrenchRevolutionary 0.08
Pod::Loom
Dist::Zilla::Plugin::PodLoom 3.02
DateTime::Calendar::Mayan 0.0601
DateTime::Calendar::Japanese 0.06001
DateTime::Duration::Fuzzy 0.03
DateTime::Event::Lunar 0.06
DateTime::Event::SolarTerm 0.05
DateTime::Format::DBI 0.040
FFmpeg::Command 0.15
DateTime::Format::DateManip 0.04
DateTime::Format::Human::Duration 0.0.1
DateTime::Format::WindowsFileTime 0.02
DateTime::Functions 0.10
DateTime::HiRes 0.01
DateTime::TimeZone::Alias 0.06
DateTime::TimeZone::Olson 0.001
DateTimeX::Format 1.03
Debug::MessageArray 0.11
Devel::Assert 0.02
Declare::Constraints::Simple 0.03
Deep::Encode 0.18
Devel::Autoflush 0.05
Devel::ArgNames 0.03
Devel::CallerStack 0.003
Dist::Zilla::Plugin::Test::NewVersion
Dist::Zilla::PluginBundle::FLORA 0.13
Devel::Pragma 0.54
Devel::ChangePackage 0.07
Devel::CheckApplicationCapabilities 1.0
Runops::Trace
Devel::Dt 0.04
Hash::SafeKeys
Devel::DumpTrace 0.12
Devel::Dumpvar 1.06
Devel::EndStats 0.04
Dist::Zilla::Plugin::MatchManifest 4.00
Devel::Events::Objects 0.05
Devel::Hook 0.005
Devel::Hints 0.21
Devel::InPackage 0.01
Devel::Ladybug 0.413
Devel::OptreeDiff 2.3
Devel::REPL::Plugin::DataPrinter 0.002
Fey::Loader 0.11
Devel::Refactor 0.05
Devel::Required 0.08
Devel::SearchINC 2.103460
Devel::SelfStubber 1.05
Devel::SlowBless 0.03
Devel::Spy 0.07
Devel::Todo::Find 1.012
Devel::Trace 0.10
Devel::Trace::Cwd 0.02
Devel::Trace::Fork 0.11
Devel::Trace::More 0.04
Dist::Zilla::Plugin::ReadmeMarkdownFromPod 0.103510
Devel::TraceMethods 1.00
Devel::TrackObjects 0.4
Devel::TrackSIG 0.01
Devel::VersionDump 0.02
Devel::bt 0.05
Device::CurrentCost 1.110792
Device::Dynamixel 0.027
Device::Gsm 1.56
Device::Inverter::Aurora 0.05
Device::KOBOeReader 0.0101
Device::LaCrosse::WS23xx 0.08
Git::Repository::Command
Git::Repository::Plugin
Git::Repository::Log::Iterator
Software::Release
Software::Release::Change
Dist::Zilla::Plugin::ChangelogFromGit 0.002
Device::MAS345 0.03
Device::MegaSquirt 0.01
Device::TLSPrinter 0.50
Digest::MD5 2.51
File::PathInfo 1.27
Device::USB::PCSensor::HidTEMPer 0.02
Dist::Zilla::Plugin::MakeMaker::Awesome 0.12
Device::WH1091 0.03
Device::XBee::API 0.2
Digest 1.16
Digest::FNV 2.00
Digest::FNV::PurePerl 0.03
Digest::HMAC 1.02
Digest::PBKDF2 0.005
Dist::Zilla::Plugin::KwaliteeTests 1.101420
Digest::SHA 5.61
Dist::Zilla::Plugin::GitHub 0.06
Digest::Tiger 0.02
Digest::XSAdler32 0.01
Dir::ListFilesRecursive 0.02
Dir::Project 3.023
Directory::Queue 1.20
Directory::Transactional 0.09
Dist::Zilla::BeLike::CSJEWELL 0.901
Dist::Zilla::Plugin::DynamicPrereqs
Dist::Zilla::Plugin::ApacheTest 0.01
Dist::Zilla::Plugin::ApocalypseTests 1.001
Dist::Zilla::Plugin::AppendExternalData 0.002
Dist::Zilla::Plugin::MetaProvides::Class 1.12060312
Dist::Zilla::Plugin::AssertOS 0.04
Dist::Zilla::Plugin::AutoVersion::Relative 0.01060309
Dist::Zilla::Plugin::BuildFile undef
Dist::Zilla::Plugin::BuildSelf 0.001
Dist::Zilla::Plugin::CPANChangesTests 1.002
Dist::Zilla::Plugin::Catalyst 0.13
Dist::Zilla::Plugin::CheckChangesTests 1.100900
Dist::Zilla::Plugin::NoTabsTests 0.01
Dist::Zilla::Plugin::Conflicts 0.07
Dist::Zilla::Plugin::OSPrereqs 0.002
Dist::Zilla::Plugin::UnusedVarsTests 1.100860
Dist::Zilla::Plugin::CopyTo 0.11
Dist::Zilla::Plugin::PortabilityTests 1.101420
Dist::Zilla::Plugin::CriticTests 1.102280
Dist::Zilla::Plugin::CustomLicense 1.0.2
Dist::Zilla::Plugin::DistManifestTests 1.101420
Dist::Zilla::Plugin::ReportVersions 1.102460
Dist::Zilla::Plugin::Doppelgaenger 0.002
Dist::Zilla::Plugin::DualBuilders 1.001
Dist::Zilla::Plugin::DualLife 0.01
Dist::Zilla::Plugin::FakeFaker 0.02
Dist::Zilla::Plugin::StaticVersion 0.103521
Dist::Zilla::Plugin::FatPacker 1.100841
Dist::Zilla::PluginBundle::GENEHACK 1.004
Dist::Zilla::Plugin::SynopsisTests 1.101420
Dist::Zilla::Plugin::FileKeywords undef
Dist::Zilla::Plugin::FindDirByRegex 1.102640
Dist::Zilla::Plugin::GatherFromManifest 1.001
Git::DescribeVersion
Dist::Zilla::Plugin::Git::DescribeVersion 1.001011
Dist::Zilla::Plugin::Git::Tag::ForRelease undef
Dist::Zilla::Plugin::GitFmtChanges 0.003
Dist::Zilla::Plugin::GitObtain 0.05
CPAN::Mini::Inject::Remote
Dist::Zilla::Plugin::Inject 0.001
Dist::Zilla::Plugin::JSAN 0.04
Locale::Msgfmt
Dist::Zilla::Plugin::LocaleMsgfmt 1.202
Dist::Zilla::Plugin::MakeMaker::SkipInstall 1.100
Dist::Zilla::Plugin::MetaProvides::FromFile 1.11060208
Dist::Zilla::Plugin::ModuleBuild::XSOrPP 0.02
Dist::Zilla::Role::Tempdir
Dist::Zilla::Plugin::ModuleInstall 0.01054020
Dist::Zilla::Plugin::NoAutomatedTesting 0.02
Dist::Zilla::Plugin::PodLinkTests 1.004000
Dist::Zilla::Plugin::PodSpellingTests 1.103491
Dist::Zilla::Plugin::ProgCriticTests 1.102520
Dist::Zilla::Plugin::PurePerlTests 0.02
Form::Sensible 0.20012
Dist::Zilla::Plugin::ReportPhase undef
Dist::Zilla::Plugin::RequiresExternal 0.1091005
Dist::Zilla::Plugin::Rsync 0.1
Dist::Zilla::Plugin::SanityTests 0.03
Dist::Zilla::Plugin::SpellingCommonMistakesTests 1.001000
Dist::Zilla::Plugin::SubmittingPatches 0.03
Dist::Zilla::Plugin::WSDL 0.102600
Dist::Zilla::PluginBundle::AJGB 1.103270
Dist::Zilla::PluginBundle::AVAR 0.25
Dist::Zilla::Plugin::Test::CheckManifest
Dist::Zilla::PluginBundle::Author::ALEXBIO 0.04
Dist::Zilla::Plugin::Git::CheckFor::Fixups
Dist::Zilla::PluginBundle::Author::DOHERTY 0.016
Forks::Super 0.34
Dist::Zilla::PluginBundle::Author::KENTNL 1.0.7
Dist::Zilla::PluginBundle::Author::KENTNL::Lite 1.0.6
Dist::Zilla::PluginBundle::Author::OLIVER 1.110230
Dist::Zilla::PluginBundle::Author::RWSTAUNER 2.001
Dist::Zilla::PluginBundle::BINGOS 0.06
Dist::Zilla::PluginBundle::CEBJYRE 0.1.1
Dist::Zilla::PluginBundle::CJFIELDS 0.0301
Dist::Zilla::Plugin::RecommendedPrereqs
Dist::Zilla::PluginBundle::DANIELP 1.04
Dist::Zilla::PluginBundle::DOY 0.06
Dist::Zilla::PluginBundle::FAYLAND 0.08
Dist::Zilla::PluginBundle::GETTY 0.007
Dist::Zilla::PluginBundle::GopherRepellent 1.002
Dist::Zilla::Plugin::Encoding
Dist::Zilla::PluginBundle::IDOPEREL 0.4
Dist::Zilla::PluginBundle::JQUELIN undef
Dist::Zilla::PluginBundle::JROCKWAY 1.102911
Dist::Zilla::PluginBundle::MARCEL 1.103490
Dist::Zilla::PluginBundle::MSCHOUT 0.20
Dist::Zilla::Plugin::Git::CheckFor::MergeConflicts
Dist::Zilla::PluginBundle::NIGELM 0.12
Dist::Zilla::PluginBundle::NUFFIN 0.01
Dist::Zilla::PluginBundle::OLIVER 1.103640
Dist::Zilla::Plugin::UpdateGitHub
Dist::Zilla::PluginBundle::PDONELAN 1.201
Dist::Zilla::PluginBundle::PadrePlugin 0.08
Dist::Zilla::PluginBundle::RBO 0.002
Dist::Zilla::Plugin::CheckVersionIncrement
Dist::Zilla::PluginBundle::RTHOMPSON 0.103532
RDF::DOAP::Lite
Dist::Zilla::Plugin::DOAP
Dist::Zilla::Plugin::ChangeStats::Git
Dist::Zilla::Plugin::CoalescePod
Dist::Zilla::Role::File::ChangeNotification
Dist::Zilla::Plugin::CoderwallEndorse
Dist::Zilla::Plugin::ContributorsFile
Dist::Zilla::Plugin::Covenant
Dist::Zilla::Plugin::HelpWanted
Dist::Zilla::Plugin::NextVersion::Semantic
Dist::Zilla::Plugin::PreviousVersion::Changelog
Dist::Zilla::Plugin::Test::PAUSE::Permissions
Dist::Zilla::Role::EncodingProvider
Dist::Zilla::Role::LicenseProvider
Dist::Zilla::Role::NameProvider
Dist::Zilla::Plugin::VerifyPhases
Dist::Zilla::PluginBundle::YANICK 0.2.1
Distribution::Guess::BuildSystem 0.12_01
Document::Transform 1.110400
Document::Writer 0.13
Domain::Register::DomainShare 1.02
Doorman 0.06
Dumpvalue 1.15
DynGig::Automata 0.01
DynGig::Cluster 0.01
E::Mail::Acme 1505
EBook::EPUB 0.5
EBook::Generator 0.01
ELF::Extract::Sections 0.02071411
EV::ADNS 2.2
EV::Loop::Async 1.01
EWS::Client 1.110911
ElasticSearch 0.36
Elive 0.88
Emacs::Rep 0.07
Email::ARF::Hotmail 0.08
Email::AddressParser 0.04
Email::Find 0.10
Email::Folder 0.855
Yahoo::Search::XML
Email::Folder::Exchange 2.0
Email::LocalDelivery 0.217
Email::MIME::CreateHTML 1.030
Email::MIME::Kit::Assembler::Markdown 0.093070
Email::MIME::Kit::Renderer::TT 1.000
Email::MIME::Kit::Validator::Rx 0.102010
Email::MIME::RFC2047 0.91
Email::Outlook::Message 0.910
Email::Public 0.13
Email::Send::SMTP::Gmail 0.22
Email::Send::SMTP::TLS 0.04
Email::Sender::Transport::SMTP::TLS 0.09
Email::Stuff 2.102
IO::Socket::Telnet
Net::Nslookup
Email::Verify::SMTP 0.003
Emailesque undef
Enbugger 2.011
Encode::Base58::BigInt 0.02
Encode::CNMap 0.32
Encode::Detect 1.01
Geo::Postcodes 0.32
Encode::DosHebrew 0.5
Encode::HanExtra 0.23
Encode::IBM 0.11
Encode::JIS2K 0.02
EntityModel::Class 0.007
Encode::Locale 1.02
Encode::JP::Emoji 0.60
Encode::UTF8Mac 0.03
Geography::NationalGrid 1.6
Entities 0.2
EntityModel 0.010
Env 1.02
Env::PS1 0.06
Excel::Template::Plus 0.05
Error::Return 1.110510
Geo::Query::LatLong 0.8011
Eval::Compile 0.10
Event::Notify 0.00004
Eval::Context 0.09
Sub::Exporter::Lexical
Exception::Caught undef
Exception::Class::TCF 0.03
Exception::Handler 1.004
Exporter 5.64_03
Exception::NoException 0.07
Exception::Resumable 0.91
Exception::Simple 0.003
ExtUtils::AutoInstall 0.63
Exception::Sink 3.03
Exim::SpoolMessage 0.03
Exodist::Util 0.007
Exporter::Proxy 0.06
ExtJS::AutoForm::Moose 0.01
ExtUtils::ParseXS 2.2206
ExtUtils::BuildRC 0.001
ExtUtils::CBuilder 0.280202
ExtUtils::Constant 0.23
ExtUtils::Command 1.16
ExtUtils::Install 1.55
ExtUtils::MakeMaker::Dist::Zilla::Develop 0.02
ExtUtils::Manifest 1.58
ExtUtils::Typemap 0.05
ExtUtils::Typemap::Default 0.06
ExtUtils::XSBuilder 0.28
ExtUtils::XSpp::Plugin::Cloning 0.02
FBP 0.26
FBP::Perl 0.32
FCGI::Async 0.22
FCGI::Daemon 0.20110421
FFMPEG::Effects 0.03
FFmpeg::Thumbnail 0.01
Template::Liquid
Alien::FLTK
FLTK 0.532006_007
FLV::Info 0.24
FTN::Database 0.20
FTN::JAM 0.04
FTN::Log 0.06
FTN::SRIF 0.05
FabForce::DBDesigner4 0.306
Fatal::Exception 0.05
FedoraCommons::APIA 0.5
FedoraCommons::APIM 0.6
Feersum 1.200
Fennec::Assert::Wrapper::Differences 0.03
GnuPG::Interface 0.44
Fey::ORM::Mock 0.05
Fey::SQL::Pg 0.005
FeyX::Active 0.03
File::ANVL 0.28
File::CheckTree 4.4
File::Checksum 0.01
File::Comments 0.08
File::Corresponding 0.003
File::Fetch 0.32
Algorithm::Nhash
File::DigestStore 1.005
File::Find::Parallel 0.52
File::Find::Rule::PPI 1.06
File::Find::Rule::VCS 1.08
File::LibMagic 0.96
File::Find::Upwards 1.102030
File::KeePass 0.03
File::Listing 6.02
File::LinkDir 1.01
File::LocalizeNewlines 1.12
File::Locate::Iterator 18
File::Lock::Multi 1.01
File::LsColor 0.170
File::Path 2.08_01
File::MMagic::XS 0.09006
File::Media::Sort 0.044
File::Monitor::Lite 0.652
File::Namaste 0.25
File::PathInfo::Ext 1.30
File::PatternMatch 0.044
File::Policy 1.005
File::RsyBak 0.14
File::SearchPath 0.06
File::Rsync::Mirror::Recent 0.1.0
File::RsyncP 0.70
File::Scan 1.43
File::ShareDir::PAR 0.06
Path::IsDev::Object
Path::FindDev
File::ShareDir::ProjectDistDir 0.1.1
File::SharedNFSLock 0.03
File::Slurp::Shortcuts 0.03
File::Spec::Memoized 1.00
File::Stamped 0.02
File::Sync 0.09
File::Stat 0.01
File::Stat::Ls undef
File::Stream 2.30
XML::Snap
File::TTX 0.03
File::Tabular::Web 0.19
File::Tempdir 0.02
File::TinyLock 1.1
File::Type::WebImages 1.01
File::Trash 1.10
File::TreeBuilder 0.01
File::Unpack 0.39
File::UserConfig 0.06
File::Vctools 0.09
File::Which 1.09
File::Versions 0.01
Filter::Simple 0.85
Filesys::DfPortable 0.85
Filter::CommaEquals 0.01
Filesys::DiskUsage 0.05
Filesys::Type 0.02
Filter::Arguments 0.14
Filter::BoxString 0.04
Filter::HereDocIndent 0.91
Filter::Indent::HereDoc 1.01
Filter::gunzip 4
Finance::Bank::Halifax::Sharedealing 0.03
Finance::Bank::ID::BCA 0.17
HTTP::Headers::Patch::DontUseStorable
Parse::Number::EN
Finance::Bank::ID::Mandiri 0.15
Finance::Bank::JP::Mizuho 0.02
Graphics::Magick
Finance::Bank::LaPoste 7.05
Finance::BankVal::UK 0.04
Finance::Card::Citibank 2.02
Finance::Card::Discover 0.05
Finance::Quote
Finance::Currency::Convert 1.08
Finance::Currency::Convert::ECB 0.1
Finance::FITF 0.30
Finance::FIX 0.01
Finance::InteractiveBrokers::API 0.02
Finance::MtGox 0.01
Finance::NASDAQ::Quote 0.06
Finance::QIF 3.02
Finance::QuoteOptions 0.23
Finance::TW::TAIFEX 0.34
Finance::Wesabe 0.02
Finnigan 0.0203
Firepear::Catechesis 1.002
Flickr::API 1.04
Flickr::Upload 1.32
Flame::Palette 0.02
Flea 0.04
Flickr::API2 2.01
Flickr::Simple2 0.03
Font::AFM 1.20
Font::TTF::OpenTypeLigatures 0.01
Food::ECodes 0.04
Test::Role::TinyCommons::Tree
Class::Accessor::Array
Class::Accessor::Array::Glob
Tree::Object
Forest::Tree::Viewer::Gtk2 undef
Hook::AfterRuntime
Class::Build::Array::Glob
Catalyst::Plugin::DBIC::Schema::Profiler
JSON::Tiny
DateTimeX::Moment
Parse::Taxonomy
Github::Fork::Parent
Form::Factory 0.020
Form::Sensible::Reflector::DBIC 0.349
Form::Sensible::Reflector::MySQL 0.2
Data::RecordStore
Yote
FormValidator::LazyWay 0.19
Formatter::HTML::Textile 1.02
Forward::Routes 0.14
FreeBSD::Ports::INDEXhash 1.2.2
Frost 0.70
Furl::S3 0.01
FusionInventory::Agent 2.1.8_rc1
GD::Barcode 1.15
FusionInventory::Agent::Task::OcsDeploy 1.1.0
GCC::TranslationUnit 1.00
GD::Barcode::Code93 1.4
Geo::Constants 0.06
Geo::Functions 0.07
GD::Graph::Polar 0.16
GRID::Machine 0.113
GD::Thumbnail 1.35
GDS2 3.00
Geo::Ellipsoid
GRID::Cluster 0.04
Games::Affenspiel 0.1.0
Games::ConnectFour 0.02
Games::Dissociate 1
Games::FrozenBubble 2.212
SDL::Mixer
SDL::Mixer::Music
Games::HotPotato 0.110020
Games::NES::ROM 0.07
Games::Pentago 0.01
Games::SGF 0.993
Games::SGF::Go::Rotator 1.2
Dir::Manifest
Games::Solitaire::Verify 0.09
Games::Sudoku::CPSearch 1.00
Games::Tournament::Swiss 0.18
Games::Word 0.05
Games::Word::Wordlist::Enable 2010090401
Games::Word::Wordlist::SGB 2010091501
Gentoo::MetaEbuild::Spec::Base 0.1.3
Ganglia::Gmetric::PP 1.04
Gedcom::Date 0.06
Genezzo 0.72
Geo::Address::Mail 0.04
Genome 0.05
Gentoo::MetaEbuild::Spec::MiniSpec 0.1.0
Geo::Address::Mail::Standardizer 0.02
Geo::Coder::SimpleGeo 0.03
Geo::Coder::PlaceFinder 0.04
Gentoo::MirrorList 1.0.0
Gentoo::Overlay 0.02004319
Gentoo::PerlMod::Version 0.3.0
Gentoo::PerlMod::Version::FixEbuild 0.1.1
Geo::Address::Mail::Standardizer::USPS 0.03
Geo::Address::Mail::Standardizer::USPS::AMS::Results 0.05
Geo::Address::Mail::UK 0.02
Geo::BUFR 1.19
Math::Units
Geo::Calc 0.04
Geo::Cloudmade 0.5
Geo::Distance::XS 0.08
Geo::Coder::Bing::Bulk 0.03
Geo::Coder::Googlev3 0.07
Geo::Coder::OSM 0.02
List::Util::WeightedRoundRobin
Geo::Coder::OpenCage
Geo::Coder::Many 0.23
Geo::Coder::Mappy 0.01
Geo::Coder::Navteq 0.03
Geo::Coder::Ovi 0.02
Geo::Coder::RandMcnally 0.01
Geo::Coder::TomTom 0.03
Geo::Hash 0.02
Gtk2::Ex::CellLayout::Base 5
Geo::Coder::Yahoo 0.50
Geo::Converter::dms2dd 0.02
Gtk2::Ex::DateSpinner 8
Geohash
Geo::Coordinates::Converter::Format::Geohash 0.04
Gtk2::Ex::Dragger 8
Gtk2::Ex::ErrorTextDialog 9
Geo::Coordinates::GMap 0.05
Gtk2::Ex::MenuView 4
Geo::Coordinates::KKJ 0.01
Geo::Ellipsoids 0.16
Geo::ECEF 1.10
Glib::Ex::ConnectProperties 14
Gtk2::Ex::Dashes::MenuItem
Gtk2::Ex::History 7
Gtk2::Ex::ListModelConcat 10
Gtk2::Ex::NoShrink 4
Gtk2::Ex::WidgetBits 37
Gtk2::Ex::NumAxis 4
Gtk2::Ex::Splash 52
Gtk2::Ex::WidgetCursor 15
Gtk2::Ex::TickerView 15
Geo::Gpx 0.26
Geo::Hash::XS 0.00013
Geo::IPfree 1.110450
Geo::KML 0.92
Geo::OSM::StaticMap 0.3
Geo::Parse::OSM 0.40_1
Device::Chip
Geo::Postcode 0.17
Geo::Postcodes::DK 0.32
Geo::Query 0.04
Geo::ShapeFile 2.52
Geo::TigerLine 0.03
Geo::TigerLine::Abbreviations 0.04
Geometry::AffineTransform 1.3
Geo::WebService::Elevation::USGS 0.006
Geography::JapanesePrefectures 0.09
Geography::NationalGrid::TW 0.08
Geometry::Formula 0.02
DBIx::Class::Schema::Loader::DBI::RelPatterns
Log::File::Rolling
Tree::Parser
Devel::PerlySense
Lingua::Sentence
Markdent
Array::Utils
DateTime::Calendar::Hebrew
DateTime::Calendar::Hijri
DateTime::Calendar::Pataphysical
Lingua::YALI
Math::BigInt::Random
Dallycot
Dist::Zilla::Plugin::Prereqs::Soften
Pod::Weaver::Plugin::Ditaa
Template::Plugin::Filter::ANSIColor
IO::Prompter
BalanceOfPower
Redis::CappedCollection
WWW::Alexa::TrafficRank
Set::IntervalTree
Module::Build::Pluggable::CPANfile
Bio::DB::Big
Bio::DB::HTS
Math::Utils
Math::Derivative
Math::Spline
SVG::Graph
Algorithm::Munkres
PostScript::TextBlock
Bio::Root::Version
Bio::DB::USeq
Bio::Perl
BioPerl
Bio::ToolBox
Gzip::RandomAccess 0.91
JSON::Relaxed
Fl
Getopt::Compact::WithCmd 0.13
DBIx::Class::BatchUpdate
Sub::Retry
Test::Mock::Time
Devel::Cover::Report::Codecov
Net::Ping::External
Getopt::FileConfig 1.0001
Graphics::GnuplotIF 1.6
Glib::Ex::ObjectBits 12
Getopt::Flex 1.07
Log::Any::Adapter::Screen
Boxer
namespace::clean::xs
Lock::Server
Test::DBGp
XML::Parser::EasyTree
DBGp::Client
CPAN::Plugin::Sysdeps
URI::XSEscape
Alien::Sodium
Crypt::Sodium
HTTP::Async
Tie::DNS
Dancer2::Core::Route
IPC::Transit
MaxMind::DB::Metadata
Test::Bits
Data::IEEE754
MaxMind::DB::Reader
GeoIP2
Hiredis::Raw
Crypt::JWT
Config::Model::Systemd
Config::Entities
Maven::Agent
Template::Resolver
Footprintless
JavaScript::Packer
HTML::Packer 1.001_001
Yote::Server
Getopt::GUI::Long 0.92
HTML::FormFu 0.08002
Dancer2::Plugin::Minify
Carmel
Text::CharWidth
Text::WrapI18N
Getopt::Modular 0.06
Getopt::Tree 1.10
GitMeta 0.01
Git::Gitalist
Gitalist 0.003001
Glib::EV 2.02
Global::Context 0.001
Gloom 0.15
GnuPG 0.17
Google::AJAX::Library 0.022
Google::GeoCoder::Smart 1.15
Google::SAML::Request 0.04
Google::SAML::Response 0.10
Google::Search 0.027
Google::Voice 0.02
Goto::Cached 0.22
Encode::Detect::CJK
Lingua::Han::Utils
Lingua::Han::PinYin
Grades 0.12
Graph::Centrality::Pagerank 1.05
Graph::Chart 0.63
Hash::PriorityQueue 0.01
Graph::Easy::Marpa 0.51
Compiler::Lexer
List::PriorityQueue
Graph::Fast 0.02
Graph::MaxFlow 0.03
Graphics::ColorObject 0.5.0
Growl::Any 0.08
Sub::Call::Tail
Gtk2::Ex::Builder 0.003001
Gtk2::Ex::Clock 15
Gtk2::Ex::ComboBoxBits 29
Gtk2::Ex::Dashes 2
Gtk2::Ex::Entry::Pango 0.10
CGI::Test
RPC::ExtDirect
CGI::ExtDirect
RPC::ExtDirect::Server
RPC::ExtDirect::Client
Gtk2::Ex::PodViewer 0.18
Gtk2::Ex::TiedListColumn 5
Gtk2::Ex::TreeModelFilter::DragDest 3
Gtk2::Ex::Xor 20
Gzip::BinarySearch 0.9
HDML::LinkExtor 0.02
HTML5::Manifest 0.03
HTML::Barcode 0.08
HTML::HTML5::Microdata::Parser 0.031
HTML::ElementTable
HTML::CalendarMonth 1.25
HTML::CalendarMonthSimple 1.26
RDF::Dumper
HTML::Embedded::Turtle
XML::Saxon::XSLT2
XML::GRDDL
HTML::Data::Parser 0.003
HTML::DateSelector 0.04
HTML::Defang 1.04
HTML::Display 0.39
HTML::EasyForm 0.001
HTML::TagParser 0.16
HTML::EasyTags 1.071
HTML::EditableTable 0.21
HTML::ExtractContent 0.10
HTML::EmbeddedPerl 0.22
HTML::Embellish 0.05
HTML::Entities::Latin2 0.04
HTML::Entities::Numbered 0.04
HTML::Toc 1.12
Sub::Recursive
HTML::Entities::Recursive 0.01
HTML::ExtractMain 0.62
HTML::Feature 3.00011
HTML::FillInForm::Lite 1.09
HTML::Filter::Callbacks 0.05
HTML::FormFu::ExtJS 0.077
HTML::FormFu::Model::DBIC 0.09000
HTML::FormTemplate 2.03
HTML::FormatExternal 19
HTML::FromANSI 2.03
HTML::HTML5::Microdata::ToRDFa 0.030
HTML::Latemp::News 0.1.7
HTML::Latemp::GenMakeHelpers 0.2.0
HTML::Highlighter 0.05
HTML::Hyphenate 0.05
HTML::LinkAdd 0.13
HTML::Macro 1.29
HTML::Mason::PSGIHandler 0.52
HTML::Mason::PlackHandler 0.103070
HTML::Miner 0.05
HTML::MobileJp 0.07
HTML::Parser 3.68
HTML::Tidy 1.54
HTML::Parser::Simple 1.06
Image::Resize 0.5
HTML::QuickTable 1.12
HTML::Quoted 0.03
HTML::Revelation 1.03
Module::Pluggable 3.9
HTML::RewriteAttributes 0.04
HTML::SimpleLinkExtor 1.23
HTML::SimpleParse 0.12
HTML::StickyQuery::DoCoMoGUID 0.03
HTML::StripScripts 1.05
HTML::StripScripts::Parser 1.03
HTML::Summary 0.017
HTML::SummaryBasic 0.2
HTML::Tagset 3.20
HTML::TagCloud::Centred 5
HTML::Template::Pluggable 0.17
HTML::Template::Compiled::Plugin::I18N 1.04
HTML::Template::Default 1.10
HTML::Tree 4.2
HTML::Template::Expr 0.07_01
HTML::Template::JIT 0.05
HTML::Truncate 0.20
HTML::Template::Pro::Extension 0.10
HTML::Tested::JavaScript 0.30
HTML::Transmorgify 0.08
Ham::APRS::FAP 1.17
HTML::TurboForm 0.66
HTML::Widget::Factory 0.082
HTML::Widgets::Index 0.61
HTML::Widgets::NavMenu 1.0600
HTML::YUI3::Menu 1.01
HTTP::AppServer 0.04
HTTP::Body::MultiPart::Extend 0.01
HTTP::Cache::Transparent 1.0
HTTP::Cookies::Mozilla 2.03
HTTP::Cookies::Omniweb 1.13
IO::File::AtomicChange 0.03
Time::Fake
HTTP::Cookies::Opera 0.07
HTTP::DAV 0.43
HTTP::Date 6.00
HTTP::Cookies::iCab 1.131
Build::Daily
HTTP::DAV::Browse 0.05
HTTP::Engine 0.03004
HTTP::LoadGen 0.07
HTTP::MobileAttribute 0.22
Date::Easy
Thread::Queue::Any
IO::Stream 1.0.5
HTTP::OAI 3.24
HTTP::ProxyTest 0.10
HTTP::Response::CGI 1.0
HTTP::Response::Parser 0.03
HTTP::Session 0.43
HTTP::Server::Simple::Recorder 0.03
HTTP::Server::Simple::Static 0.07
HTTP::Session::Store::KyotoTycoon 0.02
HTTP::Tiny 0.012
HTTP::SimpleLinkChecker 1.16
HTTP::WebTest 2.04
HTTPx::Dispatcher 0.08
Hadoop::Streaming 0.110030
Ham::APRS::DeviceID 1.04
Ham::APRS::LastPacket 0.03
Ham::Locator 0.1000
Ham::Reference::Callook 0.02
Ham::Reference::Phonetics 0.02
Ham::Reference::QRZ 0.03
Ham::Reference::Qsignals 0.02
Ham::Reference::Solar 0.03
Hash::ConsistentHash 0.05
Hash::Diff 0.005
Hash::MultiKey 0.06
Hash::NoRef 0.03
Hash::Work 0.03
Hook::Modular 1.101050
Heap::Simple 0.13
Lingua::PT::Words2Nums 1.04
Helios 2.23
POE::Component::Server::SOAP
Hopkins
Hopkins::Plugin::RPC 0.900
Lingua::Translate 0.09
Hostfile::Manager 0.07
I18N::Collate 1.01
I18N::Handle 0.051
I18N::Langinfo::Wide 7
IDNA::Punycode 1.100
IMDB::Film 0.49
IMS::CP::Manifest 0.0.2
IO 1.25_02
Linux::Epoll
IO::Async::Loop::Epoll 0.11
Image::TextMode 0.14
IO::Async::Loop::IO::Async 0.02
IO::Async::Loop::POE 0.04
IO::Ppoll 0.10
IO::Async::Loop::Ppoll 0.08
IO::Compress::Lzop 2.027
IO::Epoll 0.02
IO::File::WithFilename 0.01
IO::Handle::Record 0.14
IO::Interface 1.05
IO::LCDproc 0.037
IO::Multiplex 1.12
IO::Multiplex::Intermediary 0.06
IO::Mux 0.11
IO::Pipeline 0.009002
IO::Socket::Multicast 1.12
IO::Socket::RedisPubSub 0.02
IO::Socket::Socks::Wrapper 0.03
IO::Stream::Proxy::SOCKSv5 1.0.0
IO::Tee 0.64
IO::Tty 1.10
IO::Unread 1.04
IO::Util 1.5
IO::YAML 0.08
IO::Zlib 1.10
IOMux 0.12
IP::Country 2.27
IP::Country::DB_File 2.01
IP::Location 0.01
IP::World 0.37
IPC::Cmd 0.70
IPC::ConcurrencyLimit 0.01
IPC::ConcurrencyLimit::Lock::NFS 0.01
IPC::Exe 1.010
IPC::Fork::Simple 1.45
IPC::Locker 1.488
IPC::MPS 0.11
IPC::Mmap 0.21
IPC::Open3::Utils 0.8
IPC::Pipeline 0.3
IPC::PubSub 0.29
IPC::Run3::Simple 0.002
IPC::Run::Fused 0.01028806
IPC::SafeFork 0.0100
IPC::Signal::Force 0.003
IPC::SysV 2.03
IPTables::Log 0.0005
IRC::Formatting::HTML 0.29
Icon::Theme::List 0.0.0
SOAP::XML::Client::DotNet
SOAP::XML::Client::Generic
SOAP::XML::Client
IhasQuery 0.022
Image::Scale 0.06
Image::Base::GD 9
Image::Base::Imager 5
Image::Compare 0.9
Image::EXIF::DateTime::Parser 1.2
Image::Epeg 0.12
Image::Fixup 0.01002
Image::MetaData::JPEG 0.153
Image::Imlib2 2.03
Image::Index::LaTeX 0.01
Image::Magick::Thumbnail::Fixed 0.04
Image::Math::Constrain 1.02
Image::PNG 0.03
Image::PNG::FileConvert 0.02
Image::PNG::Rewriter 0.9
Image::SVG::Path 0.02
Image::Simple::Gradient 0.05
Image::TextMode::Reader::ANSI::XS 0.07
Image::ThousandWords 0.10
Image::Thumbnail 0.66
Image::VisualConfirmation 0.10007
Image::Xpm 1.12
Imager::ExifOrientation 0.07
Imager::File::GIF 0.78
Imager::File::JPEG 0.78
Imager::File::TIFF 0.78
Imager::Filter::FishEye 0.04
Imager::Font::FT2 0.79
Imager::Graph 0.09
Imager::QRCode 0.033
Lingua::FR::Numbers 0.04
Imager::Screenshot 0.010
Imager::Simple 0.010003
IncomeTax::IND 0.02
Inline::Files 0.64
Inline::Python 0.38
Inline::TT 0.07
Inline::YAML 0.11
Labyrinth::Plugin::Core 5.06
InlineX::C2XS 0.16
Insolation 0.01
InterMine::Item 0.95
InterMine::TypeLibrary 0.9600
Iterator::IO 0.02
Internals::DumpArenas 0.11
Introspector 0.04
Ipernity::API 0.10
Iterator::GroupedRange 0.03
Iterator::ToArray 0.02
JS 0.17
JS::JSON 0.02
JS::Test::Simple 0.29
JS::Test::Base 0.16
JS::jQuery 1.2.6.001
JSAN::Client 0.29
JSAN::Parse::FileDeps 1.00
JSAN::ServerSide 0.06
JSON::DWIW 0.47
JSON::JOM 0.005
JSON::JOM::Plugins::JsonPath 0.001
JSON::JOM::Plugins::JsonT 0.001
JSON::PP::Compat5005 1.10
JSON::PP::Compat5006 1.09
JSON::PPdev 2.27100
JSON::RPC 0.96
JSON::RPC::Dispatcher 0.0505
MooseX::Deprecated
JSON::RPC::LWP 0.006
JSON::RPC::Simple 0.01
JavaScript::Minifier 1.05
JSON::T 0.100
JSON::XS::VersionOneAndTwo 0.31
JSON::YAJL 0.05
JavaScript::Beautifier 0.17
JavaScript::Squish 0.07
JavaScript::Writer 0.3.1
Javascript::Closure 0.07
Javascript::MD5 1.10
Javascript::SHA1 1.07
Jet 0.001
Jifty::DBI 0.68
Jit 0.04_08
Job::Machine 0.17
Jonk 0.10_02
Judy 0.40
Kafka::Client 0.02
Kephra 0.4.3.24
KeyedMutex::Memcached 0.03
Keyword::API 0.0004
Keyword::Boolean 0.001
KinoSearch1 1.01
KinoSearchX::Simple 0.08
KiokuDB::Backend::BDB 0.15
KiokuDB::Cmd 0.03
Linux::usermod 0.69
LEOCHARRE::CLI 1.19
Crypt::Util
KiokuDB::Serializer::Crypt 0.02
Kwargs undef
LWPx::Record::DataSection 0.01
Kwiki 0.39
Kwiki::Attachments 0.21
LEOCHARRE::Basename 1.08
LEOCHARRE::Class2 1.19
String::Prettify
LEOCHARRE::Strings 1.02
LJ::GetCookieSession 0.01
LWP::MediaTypes 6.01
LUGS::Events::Parser 0.05
Log::Syslog::DangaSocket 1.06
LWP::Authen::Wsse 0.05
LWP::Protocol::http10 6.02
LWP::Protocol::socks 1.3
LWP::Simple::Post 0.05
LWP::UserAgent::Anonymous 0.03
LWP::UserAgent::OfflineCache 0.02
LWP::UserAgent::POE 0.03
LWP::UserAgent::WithCache 0.11
LaTeX::Decode 0.03
LaTeX::TikZ 0.02
Labkey::Query 0.06
Labyrinth::Plugin::Articles::Diary 1.01
Labyrinth::Plugin::Event 1.01
Language::Befunge 4.13
Language::Befunge::Vector::XS 1.1.1
Language::Befunge::Storage::Generic::Vec::XS 0.03
Language::Farnsworth 0.7.7
Language::GolfScript 0.04
Language::Homespring::Visualise::GraphViz 0.04
Lazy::Lockfile 1.16
Lemonldap::NG::Portal 1.0.5
Lexical::Import 0.001
Lexical::Types 0.11
Lexical::Var 0.005
Leyland 0.001002
Library::CallNumber::LC 0.10
Lingua::AF::Numbers 1.2
Lingua::EN::Numbers 1.01
Lingua::AR::MacArabic 0.10
Lingua::Alphabet::Phonetic 1.09
Lingua::Alphabet::Phonetic::StarWars 1.02
Lingua::CS::Num2Word
Lingua::DE::Num2Word
Lingua::ES::Numeros
Lingua::EU::Numbers
Lingua::HU::Numbers
Lingua::ID::Nums2Words
Lingua::IT::Numbers
Lingua::NL::Numbers
Lingua::NO::Num2Word
Lingua::PL::Numbers
Lingua::PT::Nums2Words
Lingua::PT::Nums2Ords
Lingua::SV::Numbers
Lingua::TR::Numbers
Lingua::ZH::Numbers
Lingua::JA::Numbers
Lingua::Any::Numbers
Task::Lingua::Any::Numbers
Lingua::Boolean 0.005
Lingua::Concordance 0.04
Lingua::EN::AddressParse 1.16
Lingua::EN::Alphabet::Shaw 0.64
Lingua::EN::Bigram 0.03
Lingua::EN::Contraction
Lingua::EN::Conjugate 0.311
Lingua::EN::Infinitive 1.11
Lingua::EN::Ngram 0.02
Lingua::EN::Titlecase 0.14
Lingua::EN::Titlecase::Simple 0.1
Lingua::FA::MacFarsi 0.10
Lingua::Gram 0.01
Lingua::HE::MacHebrew 0.10
Lingua::Han::CanonicalPinYin 0.04
Test::Approx
Lingua::JA::FindDates 0.011
Lingua::JA::Kana 0.04
Lingua::JA::Moji 0.05
Lingua::JA::Romaji::Valid 0.02
Lingua::Jspell 1.74
Lingua::KO::TypoCorrector 0.02
Lingua::PT::Abbrev 0.07
Lingua::LO::Romanize 0.08
Lingua::PT::Conjugate 1.17
Lingua::PT::Hyphenate 1.05
Lingua::PT::PLNbase 0.24
Lingua::PT::Inflect 0.06
Lingua::PT::Ords2Nums 0.07
Text::RewriteRules
Lingua::PT::PLN 0.17
Lingua::PT::ProperNames 0.09
Lingua::RU::Preposition 0.01
Lingua::Slavic::Numbers 0.03
Lingua::StarDict::Gen 0.09
Lingua::TR::ASCII 0.11
WWW::Google::Translate
Lingua::Translate::Google 0.12
Lingua::Translate::InterTran 0.05
Lingua::Translit 0.19
Lingua::ZH::Wrap 0.03
LinkedList::Single 0.99.9
Linux::DVB 1.01
Linux::DVB::DVBT 2.09
Linux::DVB::DVBT::Advert 0.02
Linux::DVB::DVBT::TS 0.06
Linux::Distribution 0.20
Linux::FD 0.004
Linux::Proc::Net::TCP 0.04
Linux::Smaps 0.09
Linux::Smaps::Tiny 0.06
Linux::SocketFilter 0.04
List::Bisect 0.002
List::Conditional 0.02
List::DoubleLinked 0.003
Mail::SpamAssassin 3.003001_01
List::Enumerator 0.10
List::Gen 0.80
List::MergeSorted::XS 1.05
Test::HexDifferences
Locale::MO::File 0.01
List::PowerSet 0.01
List::Parseable 1.06
List::Tuples 0.04
LocalOverride 1
Locale::BR 0.01
Locale::Country::Multilingual 0.23
Locale::Geocode 1.2
Mail::DKIM 0.39
Locale::Hebrew 1.05
Locale::ID::GuessGender::FromFirstName 0.03
Locale::Maketext::Simple 0.21
Locale::Maketext::Fuzzy 0.10
Locale::Maketext::TieHash::L10N 0.13
Locale::Maketext::TieHash::quant 0.06
Locale::Nationality::en 1.01
Locale::Object undef
Locale::PO::Callback 0.04
Locale::PO::Utils 0.08
MediaWiki::API
Test::Is
MediaWiki::Bot 3.3.1
Locale::US 1.2
Log4Perl::ImportHandle 0.02
Log::Any::Adapter::FileHandle 0.005
Log::Any::Adapter::Syslog 1.2
Encode::MAB2
Log::Dispatch::Dir 0.06
IO::Socket::UNIX::Util
Log::Any::IfLOG
Log::Dispatch::ArrayWithLimits
Log::Dispatch::FileWriteRotate
Log::Any::App 0.31
JSON5
File::Serialize
Crypt::Curve25519
Syntax::Feature::Try
Swim
String::Tokenizer
CQL::Parser
Catmandu::Store::Hash
Catmandu::Solr
Dancer2::Plugin::Feed
Sig::PackageScoped
Digest::SipHash
Perinci::CmdLine::Any
Perinci::Sub::XCompletion::perl_distname
Perinci::Sub::XCompletion::perl_modname
Perinci::Sub::XCompletion::perl_modname_or_prefix
Perinci::Sub::XCompletion::perl_modprefix
Config::IOD
Data::Sah::Coerce::perl::str::str_normalize_perl_modname
Sah::Schema::perl::modname
Version::Util
App::PDRUtils
Config::IOD::Base
Filename::Backup
Sort::Sub::prereq_ala_perlancar
Perl::PrereqScanner::Lite
App::LintPrereqs
Dist::Zilla::Plugin::LintPrereqs
Crypt::ECB
Graph::Maker::Complete
Graph::Maker
Graph::Maker::Hypercube
Graph::Maker::Star
Graph::Maker::Wheel
Graph::Maker::CircularLadder
Graph::Maker::Cycle
Graph::Maker::Hanoi
Net::Yadis
Term::Readline
Term::Readline::Event
IRC::Server::Tree
EekBoek
Catalyst::Authentication::Store::MongoDB
CGI::Session::Driver::memcache
Data::TDMA
Rex::Endpoint::HTTP
Business::PayPal::EWP
Template::Alloy::XS
MooseX::Getopt::Usage::Role::Man
HPC::Runner
HPC::Runner::Scheduler
HPC::Runner::Slurm
POE::Component::Logger
POE::Filter::Stomp
POE::Component::MessageQueue
Business::Payment::SwissESR
Redis::Jet
WebService::SonarQube
PDF::Cropmarks
PerlGSL::DiffEq
FFI::Raw
GCCJIT
DBIx::Class::Factory
Net::Random
Mojolicious::Plugin::Pingen
Method::Slice
Perl::Tidy::Sweet
Perl::Tidy::Sweetened
Alien::ProtoBuf
Alien::uPB
Module::Build::WithXSpp
Google::ProtocolBuffers::Dynamic
Devel::SharedLibs
HPC::Runner::PBS
Test2::Tools::EventDumper
SQL::Composer
Net::TinyERP
PerlGSL::Integration::MultiDim
PerlGSL::Integration::SingleDim
PerlGSL::RootFinding::SingleDim
PerlGSL
Test::Deep::Fuzzy
TOML::Parser
TOML
Perl::MinimumVersion::Fast
Test::MinimumVersion::Fast
Test::Spellunker
Minilla
Statistics::Frequency
Statistics::Shannon
B::CallChecker
Debug::Show
JSON::Decode::Regexp
Otogiri
Otogiri::Plugin
Otogiri::Plugin::BulkInsert
Mojolicious::Plugin::PlainRoutes
Mojolicious::Plugin::JQuery
Log::Dispatch::Binlog 0.02
Log::Dispatch::Config 1.04
MsgPack::RPC
Math::Geometry::Planar::Offset 1.05
Log::Dispatch::Config::Watcher 0.02
Test::BrewBuild::Plugin::TestAgainst
invoker
Net::Frame::Layer::NTP
Log::Dispatch::Configurator::YAML 0.03
Log::Dispatch::Email::EmailSender 0.05
Alien::BWIPP 0.006
Alien::FLTK2 0.08550
Alien::IUP v0.0.22
Alien::InteractiveBrokers 9.6403
Alien::V8 0.02
Alvis::TermTagger 0.7
Antispam::Toolkit 0.08
Antispam::httpBL 0.02
Any::Daemon 0.12
AnyData 0.10
Log::Dispatch::File::Stamped 0.09
Log::Dispatch::FileRotate 1.19
Log::Dispatch::Growl 1.0
Log::Dispatch::Kafka 0.01
Log::Dispatch::MongoDB undef
Log::Dispatch::Screen::Color 0.04
Log::Fast 1.0.3
Text::xSV
Log::Log4perl::Appender::DBIx::Class 0.02
Log::Log4perl::Appender::Gearman 0.29
Log::Log4perl::Appender::RabbitMQ 0.102220
Log::Log4perl::Filter::CallerMatch 1.200
Log::Log4perl::Tiny 1.0.0
Log::Log4perl::Warn::Multiple::EasyInit 0.0.1
Log::Sprintf 0.001001
Log::Stderr 1.00
Log::Structured 0.001001
Log::Syslog::Abstract 1.200
Log::Syslog::Fast 0.55
Log::Tiny 0.9
Log::UDP::Client 0.20.0
Log::UDP::Server 0.40.0
Log::Unrotate 1.25
Love::Match::Calc 0.2
Lua::API 0.04
Lustre::Info 0.02
Lustre::LFS 0.01
Lvalue 0.21
Lyrics::Fetcher 0.5.2
MIME::Base64 3.13
Lyrics::Fetcher::LyricWiki 0.10
MARC::Fast 0.11
MARC::Record 2.0.3
MARC::Utils::MARC2Ini 0.02
MARC::Utils::MARC2MARC_in_JSON 0.03
MIDI::Simple::Drummer 0.0101
MIME::Base16 1.0
MIME::Base2 1.0
Mail::Builder 2.05
MIME::Base85 1.0
MP3::Tag 1.13
MIME::Base91 1.0
MIME::EncWords 1.012
MIME::Lite::TT 0.02
Map::Tube 2.19
MLDBM::Serializer::JSON 0.001
MLDBM::Sync 0.30
MMapDB 0.14
MP3::M3U::Parser 2.30
MP3::Tag::Utils 0.0.2
MP4::Info 1.13
Mac::Errors 1.14
Mac::Finder::DSStore 0.95
Mac::PropertyList::SAX 0.85
Mac::iTunes::Library 1.0
Mail::Alias::Reader 0.03
Mail::Box 2.098
Mail::Builder::Simple 0.15
Mail::Chimp 0.12
Mail::DeliveryStatus::BounceParser 1.527
Mail::ExpandAliases 0.49
Mail::Field::Received 0.26
Mail::GnuPG 0.16
Mail::Header::Generator 0.301
Mail::ListDetector 1.04
Mail::Maildir::Is::A 0.0.0
SSH::Batch::ForNodes
Mail::QmailQueue 0.03
Mail::Send::Loop 0.3
Mail::Sender 0.8.16
Mail::Simple::DKIM::Signer 0.01
Mail::Toaster 5.27
Log::ger
Mail::Transport::Dbx 0.07
Maildir::Reader 0.3
Maplat 0.994
Markup::Unified 0.03
Marpa 0.204000
Marpa::HTML 0.102000
Marpa::XS undef
Module::Setup 0.09
Mason::Plugin::DefaultFilter 0.002
Mason::Plugin::PSGIHandler 0.06
Math::Cephes 0.47
Mason::Plugin::QuoteFilters 0.002
MasonX::Lexer::MSP 0.11
MassSpec::ViewSpectrum 0.07
Math::Polynomial 1.004
Math::Aronson 4
Math::Base36 0.09
Math::Big 1.12
Math::BigInt::FastCalc 0.28
Math::BigRat 0.26
Math::BigInt::Lite 0.14
Math::BigInt::Parts 0.02
Math::Calc::Units 1.07
Math::Clipper 0.01
Math::Complex 1.56
Math::Complex_C 0.03
Math::ContinuedFraction 0.11
Math::ConvexHull 1.04
Math::ConvexHull::MonotoneChain
Math::Counting 0.09
Math::Currency 0.47
Math::EMA 0.03
Math::Evol 1.12
Math::Fractal::Noisemaker 0.105
Math::Function::Roots 0.065
Math::GMP 2.06
Math::GMPn 0.03
Math::Geometry::Planar::GPC 1.04
Math::Geometry::Planar 1.18
Math::Geometry::Planar::GPC::Polygon 0.05
Math::Gradient 0.04
Math::Inequalities::Parser 0.001
Math::Interpolator 0.004
Math::ModInt 0.004
Math::PlanePath 24
Math::Prime::XS 0.23
Math::Geometry::Planar::GPC::PolygonXS
Math::Polygon::Tree 0.041
Math::Polygon
Math::Polynom 0.13
Math::Polynomial::Horner 3
Math::Polynomial::Solve 2.61
Math::Random::MicaliSchnorr 0.02
Math::Random::Xorshift 0.05
Math::Round::Fair 0.03
Math::RungeKutta 1.07
Math::Symbolic 0.606
Math::SymbolicX::FastEvaluator 0.01
Math::VecStat 0.08
Math::WalshTransform 1.17
Mcache 0.02
MeSH::Parser::ASCII 0.03
MealMaster 0.28
Media::Type::Simple 0.02
MediaWiki::Bot::Plugin::Admin 3.2.1
Net::CIDR
MediaWiki::Bot::Plugin::Steward 0.0003
MediaWiki::Bot::Shell 0.002
MediaWiki::DumpFile 0.2.1
MediaWiki::USERINFO 0.04
Medical::DukePTP 0.3
Medical::ICD10 0.03
MemHandle 0.06
Memcached::Client 2.01
Memcached::Server 0.04
Memoize 1.02
Message::Stack 0.19
Memoize::Saves 0.67
Memory::Usage 0.201
Message::Stack::Parser 0.02
MetaStore 0.53
MooseX::Types::Parameterizable 0.05
Metabase::Client::Simple 0.008
Metabase::Fact 0.019
MetasploitExpress::Parser undef
Method::Assert 0.0.1
Method::Lexical 0.23
Method::Workflow 0.204
Mixin::ExtraFields::Param 0.011
Method::Workflow::Case 0.201
Method::Workflow::SPEC 0.201
Microsoft::AdCenter 7.08
Miril 0.008
Mixin::ExtraFields 0.100971
Mixin::ExtraFields::Hive 0.005
Mixin::Linewise 0.003
Music::Tag 0.4103
Mknod 0.02
Mock::Person 0.01
Module::Build::Bundle 0.04
Module::Build::DB 0.10
Module::Build::DistVersion 0.04
Module::Build::JSAN 0.05
Encode::Arabic
Module::Build::JSAN::Installable 0.13
Module::Changes 0.05
Inline::Awk
XHTML::MediaWiki
Tie::Trace
YAML::Logic
Text::Unaccent::PurePerl
WWW::Shopify::Liquid
Module::CheckDeps 0.08
Sort::Maker
autobox::Transform
WebService::PivotalTracker
Machine::Epsilon
Math::BivariateCDF
Math::Gauss::XS
Performance::Probability
Test::SSH
Event::Stats
DBIx::POS::Template
Module::Locate 1.7
Algorithm::ConsistentHash::CHash
Device::I2C
SQL::Template
Devel::NYTProf::Callgrind::TicksDiff
Geo::GeoNames
Swagger2::Markdown
Math::Matrix
Image::SVG::Transform
SVG::Estimate
Catalyst::ResponseHelpers
Net::FTPSSL
Test::LWP::Recorder
Geo::What3Words
Net::SSH::Any
HTML::Show
Chart::Dygraphs
Parallel::Tiny
Module::ConfigureRequires 0.03
Module::Depends 0.15
Module::Faker 0.009
Module::Info::File 0.11
Module::Install::AuthorRequires 0.02
Module::Install::AutoConf 0.001
MooseX::amine 0.3
Module::Install::CheckConflicts 0.02
Params::Check 0.28
Module::Install::Debian 0.030
Module::Install::ExtraTests 0.007
Module::Install::Homepage 0.01
Module::Install::PerlTar 1.001
Module::Install::ProvidesClass 1.000000
Module::Install::RPM 0.01
Module::Install::RTx 0.28
Module::Install::ReadmeMarkdownFromPod 0.03
Module::Install::TemplateInstallPath 0.03
Module::Install::VersionCheck 0.11
Module::Install::XSUtil 0.26
Module::Load 0.18
Module::Loaded 0.06
MooseX::Role::Loggable 0.003
Module::Load::Conditional 0.44
Module::LocalBuild 1.013
Module::LocalLoad 0.172
Module::Pluggable::Ordered 1.5
Module::Metadata::Changes 2.03
Module::OTRS::CoreList 0.02
Module::Overview 0.01
Module::Pluggable::Singleton 0.01
Module::Release 2.05_04
Module::Signature 0.66
Module::Setup::Flavor::JJNAPIORK 0.04
Module::Starter::CSJEWELL 0.200
Module::Starter::Plugin::CGIApp 0.30
Module::Version 0.12
MogileFS::Client 1.14
MogileFS::Utils 2.19
Mojito 0.10
Mojo::JSON::Any 0.990103
MojoX::AIO 0.02
MojoX::Logite 0.01
MojoX::Ping 0.4
MojoX::Redis 0.7
MojoX::Renderer::Alloy 1.110180
MojoX::Renderer::Haml undef
MojoX::Renderer::TT 1.11
MojoX::Renderer::WriteExcel 1.0
MojoX::Renderer::Xslate 0.04
MojoX::Run 0.14
MojoX::Session::Store::Libmemcached 0.17
MojoX::Validator 0.0012
Mojolicious::Command::Package 0.02
Mojolicious::Plugin::BasicAuth 0.04
Mojolicious::Plugin::CSRFDefender 0.0.5
Mojolicious::Plugin::Cache 0.0015
Mojolicious::Plugin::ConsoleLogger 0.02
Mojolicious::Plugin::Database 1.05
Mojolicious::Plugin::Disqus 1.22
Mojolicious::Plugin::DomIdHelper 0.1
Mojolicious::Plugin::GroupedParams 0.02
Mojolicious::Plugin::Mail 0.8
Mojolicious::Plugin::MarkaplRenderer 0.2.0
Mojolicious::Plugin::Mongodb 1.06
Mojolicious::Plugin::ParamsAuth 0.01
Mojolicious::Plugin::Proxy 0.2
Mojolicious::Plugin::Recaptcha 0.2
MooseX::Types::DateTime::MySQL 0.003
Mojolicious::Plugin::Redis 0.03
Monitoring::Livestatus 0.74
MooseX::Types::ISO8601 0.07
Mojolicious::Plugin::ShareHelpers 0.2
Mojolicious::Plugin::SslAuth 0.03
Mojolicious::Plugin::TweetButton 0.0003
Mojolicious::Plugin::Wolowitz 1.0.1
Mojolicious::Plugin::YamlConfig 0.1.1
Mollie::Micropayment 0.04
MongoDBx::Bread::Board::Container undef
MongoDBx::Class 0.7
MongoX 0.05
Mongoose 0.09
Mongrel undef
Monitoring::Availability 0.18
Monitoring::Generator::TestConfig 0.38
Monitoring::Livestatus::Class 0.03
Monotone::AutomateStdio 0.12
Moose::Policy 0.05
MooseX::ABC 0.05
MooseX::APIRole 0.01
MooseX::AlwaysCoerce 0.16
MooseX::Async 0.07
MooseX::Atom 0.02
MooseX::Attribute::Deflator 2.1.4
MooseX::Attribute::Dependent 1.0.1
MooseX::AttributeCloner 0.24
MooseX::AttributeIndexes 1.0.1
MooseX::AttributeInflate 0.03
MooseX::AuthorizedMethods 0.006
MooseX::AutoDestruct 0.006
MooseX::AutoImmute 0.001
MooseX::BatmanBeforeRobin 0.03
MooseX::CascadeClearing 0.04
MooseX::ChainedAccessors 0.02
MooseX::Collect 0.92
MooseX::CompileTime::Traits 1.102570
MooseX::ComposedBehavior 0.003
MooseX::Constructor::AllErrors 0.016
MooseX::Error::Trap 0.021
MooseX::GlobRef 0.0701
MooseX::Has::Options 0.002
MooseX::InsideOut 0.106
MooseX::InstanceTracking 0.06
MooseX::LogDispatch 1.2002
MooseX::MethodAttributes 0.24
MooseX::MultiInitArg 0.01
MooseX::MultiObject 0.01
MooseX::Net::API 0.12
MooseX::Params 0.003
MooseX::Plaggerize 0.06
MooseX::Policy::SemiAffordanceAccessor 0.02
MooseX::Privacy 0.05
MooseX::PrivateSetters 0.03
MooseX::Role::Cmd 0.10
MooseX::Role::DBIC 0.01
MooseX::Role::Listenable undef
MooseX::Role::Pluggable 0.02
MooseX::Role::Restricted 1.03
MooseX::Role::Strict 0.05
MooseX::Role::Timer 0.03
MooseX::SlurpyConstructor 1.2
MooseX::Timestamp 0.07
MooseX::Traits::Attribute::MergeHashRef 1.002
MooseX::TransactionalMethods 0.008
MooseX::Types::Common 0.001002
Net::DBus 0.33.6
MooseX::Types::DBIx::Class 0.04
Net::DNS::SEC 0.16
MooseX::Types::Data::GUID 0.001000
MooseX::Types::IO 0.03
MooseX::Types::Digest 0.03
MooseX::Types::Implements 1.103350
Net::DNS 0.66
MooseX::Types::LWP::UserAgent 0.02
MouseX::App::Cmd 0.06
MooseX::Types::Locale::BR 0.01
MooseX::Types::NetAddr::IP 0.04
MooseX::Types::Tied 0.002
MooseX::UndefTolerant 0.12
MooseX::WithCache 0.01002
MouseX::AttributeHelpers 0.06
MouseX::Types::DateTime 0.02
Mozilla::CA 20110409
Mousse 0.87
Mplayer::NowPlaying 0.030
MsOffice::Word::HTML::Writer 0.07
Muldis::D 0.147000
Muldis::D::Manual 0.009000
Music::Tag::FLAC 0.4101
Music::Tag::File 0.4101
Music::Tag::M4A 0.4101
Music::Tag::MP3 0.4101
Text::Levenshtein
Music::Tag::MusicBrainz 0.4101
Ogg::Vorbis::Header::PurePerl
Music::Tag::OGG 0.4101
MySQL::Easy::PYH 0.01
MySQL::Privilege::Reader 0.01
MySQL::Sandbox 3.0.17
Mysql::PrettyPrinter 0.1
NCBIx::Geo 1.0.0
NETIOM 0.10
NEXT 0.65
NRD::Daemon 0.02
Nagios::Object 46
Nagios::Plugin 0.35
Nagios::Scrape 0.03
Net::APE 0.003
Net::APNS 0.0201
Net::Abuse::Utils 0.11
Net::Flickr::API
MooX::ShortHas
Mu
RDF::Simple
Net::Flickr::RDF 2.2
Net::Akamai
Net::Akismet::Protocol
Amazon::SNS 1.0
Music::Tag::Amazon 0.4101
Net::Amazon 0.59
Net::Amazon::Route53 0.110310
Net::Amazon::AWSSign
Net::AozoraBunko 0.02
Net::Appliance::Session 2.111080
Net::Async::FTP 0.05
Net::Async::FastCGI 0.23
Net::Async::IMAP 0.003
Net::Async::PostgreSQL 0.002
Net::Async::WebSocket 0.03
Net::Async::XMPP 0.002
Net::BGP 0.14
Net::Backtype 0.03
Net::BitTorrent 0.050
Net::BitTorrent::File 1.02
Net::Bot::IRC::Message 0.01
Net::Bot::IRC::NumericCodes 0.05
Net::CSTAv3::Client 0.05
Net::CampaignMonitor 0.04
Net::Canopy::BAM 0.04
PPR
Keyword::Declare
JSON::Color
Data::Format::Pretty::JSON
Data::Format::Pretty::HTML
Data::Format::Pretty
Data::Format::Pretty::PHPSerialization
Data::Format::Pretty::Perl
Data::Dump::Ruby
Data::Format::Pretty::Ruby
YAML::Tiny::Color
Data::Format::Pretty::YAML
Perinci::Result::Format
Perinci::CmdLine::Classic
Pipe::Between::Object
Net::Chaton::API 0.02
Net::Clickatell 0.5
Net::DAV::Server 1.302
methods
Net::Curl::Simple 0.10
Net::DHCPv6::DUID::Parser 1.01
Net::DNS::Check 0.45
Net::DNS::DynDNS 0.96
Net::DNS::Nslookup 0.03
POE::Component::Metabase::Relay::Server 0.18
Net::DNS::ZoneFile::Fast 1.15
SDLx::Controller
POE::Component::Client::RADIUS 1.02
Net::DNS::ZoneParse 0.102
Net::RabbitFoot 1.03
Net::DNSBL::Client 0.200
Net::DRI 0.96_01
Net::Daemon 0.47
Net::Delicious 1.14
Sys::Sendfile
Crypt::RHash
Net::DirectConnect 0.09
Net::DirectConnect::TigerHash 0.06
POE::Filter::JSON
POE::Component::Hailo 0.10
Net::Disqus 1.19
Net::Domain::ExpireDate 1.00
Net::Douban 1.07_2
Net::Dropbox::API 1.4
Net::Dynect::REST 175.
Net::ENUM 0.3
Net::EPP 0.17
Net::FTP::Mock 0.103300
Net::Facebook::API 0.07
Net::FileMaker 0.063
Net::Finger::Server 0.003
Net::Flickr::Backup 3.1
Net::FluidDB 0.30
Net::Fluidinfo 0.40
Net::Frame::Dump 1.09
Net::Frame::Layer::ICMPv6 1.05
Net::Frame::Layer::IPv6 1.03
Net::GPSD3 0.17
Net::Frame::Simple 1.04
Net::GPSD3::POE 0.16
Net::Gandi 0.9
Net::Google::Blogger 0.09
Net::Google::Calendar 1.0
Net::Google::DocumentsList 0.05
Net::Google::FederatedLogin 0.6.0
Net::SSL::ExpireDate 1.10
Net::Google::SafeBrowsing2 0.5
Net::Google::Spreadsheets 0.14
Net::Growl 0.99
Net::HTTP::Spore 0.03
Net::HTTP::Factual 0.110030
Net::IDN::Nameprep 1.100
Net::ILO 0.53
Net::IMAP::Client 0.95
Net::IMAP::Server 1.29
Net::IP 1.25
Net::IMAP::Simple 1.2019
Net::IP::AddrRanges 0.01
Net::INET6Glue 0.5
Net::IP::Flakweb 0.02
Net::IP::Match::Bin 0.06
Net::IP::RangeCompare 4.025
Net::IP::XS 0.06
Net::IPAddress 1.10
Net::SSH2 0.33
Net::IPAddress::Minimal 0.05
POE::Declare 0.54
Net::IPMessenger 0.12
Net::IPTrie 0.7
Net::ISC::DHCPd 0.1001
Net::LDAP::Filter::SQL 0.02
Sys::Group::GIDhelper
Sys::User::UIDhelper
Net::LDAP::posixAccount 0.0.2
Net::LDAP::posixGroup 0.0.1
Net::LDAPxs 1.31
Net::LMTP 0.02
Net::LibResolv 0.02_001
Net::MAC 2.103622
Net::ManageSieve 0.10
Net::Milter 0.09
Net::Mosso::CloudFiles 0.44
Net::MyOpera 0.02
Net::NBName 0.26
Net::NIS::Netgroup 1.1
Net::NSCA::Client 0.008
Net::Nessus::XMLRPC 0.30
Net::OSCAR 1.928
Net::SSH 0.09
Net::OpenSSH::Compat 0.01
Net::OpenSSH::Parallel 0.10
Net::OpenXchange 0.001
Net::OperaLink 0.05
Net::PJLink 1.01
Net::Ping 2.36
Net::POP3::SSLWrapper 0.06
Net::Pachube 1.102900
Net::Patricia 1.19
Net::Pcap::Easy 1.4205
Net::Postage::App 0.02
Net::Posterous 0.8
Net::Printer 1.11
Net::Proxy::Type 0.03
Net::RTorrent 0.11
Net::RabbitMQ::Channel 0.03
Net::RabbitMQ::Simple 0.0007
Net::RackSpace::CloudServers 0.13
Net::Rackspace::Notes 0.0200
Net::Recurly 0.001
Net::Riak 0.15
Net::SAML2 0.14
Net::SFTP::Foreign::Backend::Net_SSH2 0.03
Net::SIP 0.62_6
Net::SMPP 1.18
Net::SMS::160By2 0.01
Net::SMS::MunduSMS 0.021
Net::SMS::MyTMN 0.08
Net::SMS::SMSPilot 0.05
Net::SMS::TxtLocal 0.03
Net::SMS::WAY2SMS 0.05
Net::SNMP::Util 1.04
Net::SNMP::XS 1.2
Net::SSH::AuthorizedKeysFile 0.14
Net::SSH::Expect 1.09
PAR::Repository::Client 0.25
Net::SSLGlue 0.5
Net::STOMP::Client 1.77
Net::SinaWeibo 0.003
Net::Social 0.4
Net::Songkick 0.04
Net::Squid::Auth::Engine 0.04
Net::Squid::Auth::Plugin::SimpleLDAP 0.1.83
Net::StackExchange 0.102740
Net::Syslog 0.04
Net::TacacsPlus 1.09
Net::Todoist 0.04
Net::Tor::Servers 0.03
Net::Trac 0.16
Net::Traceroute 1.13
Net::Trustico 0.01
Net::Twitpic 0.01
Net::Twitter::Diff 0.12
Net::Twitter::Stream 0.25
Net::WhitePages 1.04
Net::WOT 0.02
Net::Whois::RIPE 2.00003
Net::XMPP 1.02_02
Scrappy
Net::Whois::RIS 0.5
Net::Whois::Raw 2.31
Net::Whois::SIDN 0.96
Net::Wigle 0.02
Net::Write 1.05
Net::XIPCloud 0.5
Net::YAR 1.076
Net::YahooMessenger 0.19
Net::fonolo 1.4
Net::isoHunt 0.102770

=head1 CONFIGURATION

Summary of my perl5 (revision 5 version 13 subversion 2) configuration:
  Commit id: e0de7c21b08329275a76393ac4d80aae90ac428e
  Platform:
    osname=linux, osvers=2.6.32-2-amd64, archname=x86_64-linux
    uname='linux k81 2.6.32-2-amd64 #1 smp fri feb 12 00:01:47 utc 2010 x86_64 gnulinux '
    config_args='-Dprefix=/home/src/perl/repoperls/installed-perls/perl/v5.13.2-213-ge0de7c2 -Dinstallusrbinperl=n -Uversiononly -Dusedevel -des -Ui_db'
    hint=recommended, useposix=true, d_sigaction=define
    useithreads=undef, usemultiplicity=undef
    useperlio=define, d_sfio=undef, uselargefiles=define, usesocks=undef
    use64bitint=define, use64bitall=define, uselongdouble=undef
    usemymalloc=n, bincompat5005=undef
  Compiler:
    cc='cc', ccflags ='-fno-strict-aliasing -pipe -fstack-protector -I/usr/local/include -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64',
    optimize='-O2',
    cppflags='-fno-strict-aliasing -pipe -fstack-protector -I/usr/local/include'
    ccversion='', gccversion='4.4.4', gccosandvers=''
    intsize=4, longsize=8, ptrsize=8, doublesize=8, byteorder=12345678
    d_longlong=define, longlongsize=8, d_longdbl=define, longdblsize=16
    ivtype='long', ivsize=8, nvtype='double', nvsize=8, Off_t='off_t', lseeksize=8
    alignbytes=8, prototype=define
  Linker and Libraries:
    ld='cc', ldflags =' -fstack-protector -L/usr/local/lib'
    libpth=/usr/local/lib /lib /usr/lib /lib64 /usr/lib64
    libs=-lnsl -lgdbm -ldb -ldl -lm -lcrypt -lutil -lc -lgdbm_compat
    perllibs=-lnsl -ldl -lm -lcrypt -lutil -lc
    libc=/lib/libc-2.11.2.so, so=so, useshrplib=false, libperl=libperl.a
    gnulibc_version='2.11.2'
  Dynamic Linking:
    dlsrc=dl_dlopen.xs, dlext=so, d_dlsymun=undef, ccdlflags='-Wl,-E'
    cccdlflags='-fPIC', lddlflags='-shared -O2 -L/usr/local/lib -fstack-protector'

=head1 AUTHOR

This Bundle has been generated automatically by the autobundle routine in CPAN.pm.
