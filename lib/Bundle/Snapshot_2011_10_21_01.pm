package Bundle::Snapshot_2011_05_03_00;

$VERSION = '0.01';

1;

__END__

=head1 NAME

Bundle::Snapshot_2011_05_03_00 - Snapshot of installation on k81 on Tue May  3 08:04:38 2011

=head1 SYNOPSIS

perl -MCPAN -e 'install Bundle::Snapshot_2011_05_03_00'

=head1 CONTENTS

ExtUtils::XSpp
Alien::wxWidgets
Wx
Ask
WebService::Cryptsy
GraphViz2
Kelp::Module::JSON::XS
Encode::HP
Test::Valgrind
Perl6::GatherTake
Catmandu
List::Gather
forks
Plack::App::DAIA
Sub::Trigger::Lock
GitHub::Extract
REST::Neo4p
Test::Routine::AutoClear
Beam::Service
Beam::Wire
Carp::Notify
Tangence::Server 0.05
Tangence::Registry 0.05
PerlIO::unicodeeol
Text::MicroTemplate::Extended
Ark
DB_File::Lock
Google::ProtocolBuffers
UnderscoreJS
underscore
Compress::LZ4Frame
Gzip::Faster
Kafka
Queue::Q
Data::Undump
McBain
Time::Concise
Resque
CHI::Memoize
Path::Mapper
MooseX::Getopt::Usage
Business::CompanyDesignator
MooseX::XSAccessor
Tangence::ObjectProxy
CGI::Application::PSGI
Catmandu::RDF
GraphViz::Makefile
GraphViz2::Marpa
Unix::Lsof
XT::Manager
DBIx::Class::FormTools
Class::ArrayObjects
Stream::Aggregate
PerlIO::text
DBIx::Custom
HTML::TagHelper
CHI::Cascade
PAUSE::Packages
B::Tap
Beam::Wire::Moose
Loop::Sustainable
Debuggit
MooX::Types::CLike
Fun
Compress::Raw::Bzip2
Compress::Raw::Zlib
IO::Compress::Base
IO::Uncompress::Base
IO::Compress::Lzf
Session::Token
Plack::Middleware::Profiler::NYTProf
Config::ENV
Math::Decimal64
Linux::Setns
Statistics::LineFit
Redis::Fast
Cache::Redis
File::Tee
Data::BitStream
Google::Data::JSON
Test::MemoryGrowth
Tie::DBIx::Class
I22r::Translate
HTML::Zoom::Parser::HH5P
Seis
Error::Pure::Output::JSON
AnyEvent::HTTP::LWP::UserAgent
W3C::SOAP
Parser::Combinators
returning
MooseX::Validation::Doctypes
XS::TCC
HTML::Grabber
Document::eSign::Docusign
Params::Lazy
Geo::Proj4
Tangence::Server::Context 0.05
Tangence::Stream 0.05
Tapper::CLI::API undef
Tapper::CLI::API::Command::download undef
Tapper::CLI::API::Command::upload undef
Tapper::CLI::DbDeploy undef
Tapper::CLI::DbDeploy::Command::init undef
Tapper::CLI::DbDeploy::Command::makeschemadiffs undef
Tapper::CLI::DbDeploy::Command::saveschema undef
Tapper::CLI::DbDeploy::Command::upgrade undef
Tapper::CLI::Testrun undef
Tapper::CLI::Testrun::Command::delete undef
Tapper::CLI::Testrun::Command::deletehost undef
Tapper::CLI::Testrun::Command::deleteprecondition undef
Tapper::CLI::Testrun::Command::deletequeue undef
Tapper::CLI::Testrun::Command::freehost undef
Tapper::CLI::Testrun::Command::list undef
Tapper::CLI::Testrun::Command::listhost undef
Tapper::CLI::Testrun::Command::listprecondition undef
Tapper::CLI::Testrun::Command::listqueue undef
Tapper::CLI::Testrun::Command::new undef
Tapper::CLI::Testrun::Command::newhost undef
Tapper::CLI::Testrun::Command::newprecondition undef
Tapper::CLI::Testrun::Command::newqueue undef
Tapper::Installer::Base undef
Tapper::CLI::Testrun::Command::newscenario undef
Tapper::CLI::Testrun::Command::newtestplan undef
Tapper::CLI::Testrun::Command::rerun undef
Tapper::CLI::Testrun::Command::show undef
Tapper::CLI::Testrun::Command::updatehost undef
Tapper::CLI::Testrun::Command::updateprecondition undef
Tapper::CLI::Testrun::Command::updatequeue undef
Tapper::Installer::Precondition undef
Tapper::Installer::Precondition::Copyfile undef
Tapper::Installer::Precondition::Exec undef
Tapper::Installer::Precondition::Fstab undef
Tapper::Installer::Precondition::Image undef
Tapper::Installer::Precondition::Kernelbuild undef
Tapper::Installer::Precondition::PRC undef
Tapper::Installer::Precondition::Package undef
Tapper::Installer::Precondition::Rawimage undef
Tapper::Installer::Precondition::Repository undef
Tapper::Installer::Precondition::Simnow undef
Tapper::PRC::Testcontrol undef
Tapper::Reports::API::Daemon undef
Tapper::Reports::Receiver::Daemon undef
Tapper::Reports::Receiver::Util undef
Tapper::Schema::ReportsDB 3.000001
Tapper::Schema::ReportsDB::Result::Report undef
Tapper::Schema::ReportsDB::Result::ReportComment undef
Tapper::Schema::ReportsDB::Result::ReportFile undef
Tapper::Schema::ReportsDB::Result::ReportSection undef
Tapper::Schema::ReportsDB::Result::ReportTopic undef
Tapper::Schema::ReportsDB::Result::ReportgroupArbitrary undef
Tapper::Schema::ReportsDB::Result::ReportgroupTestrun undef
Tapper::Schema::ReportsDB::Result::ReportgroupTestrunStats undef
Tapper::Schema::ReportsDB::Result::Suite undef
Tapper::Schema::ReportsDB::Result::Tap undef
Tapper::Schema::ReportsDB::Result::User undef
Tapper::Schema::ReportsDB::Result::View010TestrunOverviewReports undef
Tapper::Schema::ReportsDB::Result::View020TestrunOverview undef
Tapper::Schema::ReportsDB::ResultSet::ReportgroupTestrun undef
Tapper::Schema::TestrunDB::Result::Host undef
Tapper::Schema::TestrunDB::Result::HostFeature undef
Tapper::Schema::TestrunDB::Result::Message undef
Tapper::Schema::TestrunDB::Result::PrePrecondition undef
Tapper::Schema::TestrunDB::Result::Precondition undef
Tapper::Schema::TestrunDB::Result::Preconditiontype undef
Tapper::Schema::TestrunDB::Result::Queue undef
Tapper::Schema::TestrunDB::Result::QueueHost undef
Tapper::Schema::TestrunDB::Result::Scenario undef
Tapper::Schema::TestrunDB::Result::ScenarioElement undef
Tapper::Schema::TestrunDB::Result::State undef
Tapper::Schema::TestrunDB::Result::TestplanInstance undef
Tapper::Schema::TestrunDB::Result::Testrun undef
Tapper::Schema::TestrunDB::Result::TestrunPrecondition undef
Tapper::Schema::TestrunDB::Result::TestrunRequestedFeature undef
Tapper::Schema::TestrunDB::Result::TestrunRequestedHost undef
Tapper::Schema::TestrunDB::Result::TestrunScheduling undef
Tapper::Schema::TestrunDB::Result::Topic undef
Tapper::Schema::TestrunDB::Result::User undef
Tapper::Schema::TestrunDB::ResultSet::Host undef
Tapper::Schema::TestrunDB::ResultSet::Precondition undef
Tapper::Schema::TestrunDB::ResultSet::Queue undef
Tapper::Schema::TestrunDB::ResultSet::Testrun undef
Tapper::Schema::TestrunDB::ResultSet::TestrunScheduling undef
Tapper::Testplan::Generator undef
Tapper::Testplan::Plugins::Taskjuggler undef
Tapper::Testplan::Reporter undef
Tapper::Testplan::Reporter::Plugins 3.000001
Task::Deprecations::5_12 1.002
Tatsumaki::Application undef
Tatsumaki::Error undef
Tatsumaki::HTTPClient undef
Tatsumaki::Handler undef
Tatsumaki::MessageQueue undef
Tatsumaki::Request undef
Tatsumaki::Response undef
Tatsumaki::Server undef
Tatsumaki::Service undef
Tatsumaki::Template::Micro undef
TeX::AutoTeX 0.9
TeX::AutoTeX::ConcatPDF v0.9
Test::Aggregate::Nested 0.364
TeX::AutoTeX::Config 1.7.2.7
TeX::AutoTeX::Exception 1.7.2.5
TeX::AutoTeX::File 1.36.2.5
TeX::AutoTeX::Fileset 1.8.2.4
TeX::AutoTeX::HyperTeX 1.10.2.6
TeX::AutoTeX::Log 1.10.2.4
TeX::AutoTeX::Mail 1.5.2.3
TeX::AutoTeX::PostScript 1.11.2.4
TeX::AutoTeX::Process 1.14.2.5
TeX::AutoTeX::StampPDF 1.9.2.5
TeX::DVI::Parse 1.01
Text::Lorem
Test::CGI::Multipart 0.0.3
TeX::Hyphen::Pattern::Af_ZA 0.04
TeX::Hyphen::Pattern::Bg 0.00
TeX::Hyphen::Pattern::Ca 0.04
TeX::Hyphen::Pattern::Cs 0.04
TeX::Hyphen::Pattern::Cy 0.04
TeX::Hyphen::Pattern::Da 0.04
TeX::Hyphen::Pattern::De_1901 0.04
TeX::Hyphen::Pattern::De_1996 0.04
TeX::Hyphen::Pattern::De_DE 0.04
TeX::Hyphen::Pattern::De_ch_1901 0.04
TeX::Hyphen::Pattern::El_monoton 0.00
TeX::Hyphen::Pattern::El_polyton 0.00
TeX::Hyphen::Pattern::En_gb 0.04
TeX::Hyphen::Pattern::En_us 0.04
Tree::Simple::Visitor
TeX::Hyphen::Pattern::Es 0.04
TeX::Hyphen::Pattern::Et 0.04
TeX::Hyphen::Pattern::Et_EE 0.04
TeX::Hyphen::Pattern::Eu 0.04
TeX::Hyphen::Pattern::Fi 0.04
TeX::Hyphen::Pattern::Fr 0.04
TeX::Hyphen::Pattern::Fr_FR 0.04
TeX::Hyphen::Pattern::Ga 0.04
TeX::Hyphen::Pattern::Gl 0.04
TeX::Hyphen::Pattern::Grc 0.00
TeX::Hyphen::Pattern::Hr 0.04
TeX::Hyphen::Pattern::Hsb 0.04
TeX::Hyphen::Pattern::Hu 0.04
TeX::Hyphen::Pattern::Hu_HU 0.04
TeX::Hyphen::Pattern::Ia 0.04
TeX::Hyphen::Pattern::Id 0.04
TeX::Hyphen::Pattern::Is 0.00
TeX::Hyphen::Pattern::It 0.04
TeX::Hyphen::Pattern::It_IT 0.04
TeX::Hyphen::Pattern::Kmr 0.04
TeX::Hyphen::Pattern::La 0.04
TeX::Hyphen::Pattern::Lt 0.04
TeX::Hyphen::Pattern::Lt_LT 0.04
TeX::Hyphen::Pattern::Lv 0.04
TeX::Hyphen::Pattern::Mn_cyrl 0.00
TeX::Hyphen::Pattern::Mn_cyrl_x_2a 0.00
TeX::Hyphen::Pattern::Nb 0.04
TeX::Hyphen::Pattern::Nl 0.04
TeX::Hyphen::Pattern::Nl_NL 0.04
TeX::Hyphen::Pattern::Nn 0.04
TeX::Hyphen::Pattern::No 0.04
TeX::Hyphen::Pattern::Pl 0.04
TeX::Hyphen::Pattern::Pl_PL 0.04
TeX::Hyphen::Pattern::Pt 0.04
TeX::Hyphen::Pattern::Pt_BR 0.04
TeX::Hyphen::Pattern::Ro 0.04
TeX::Hyphen::Pattern::Ru 0.00
TeX::Hyphen::Pattern::Ru_RU 0.00
TeX::Hyphen::Pattern::Sa 0.00
TeX::Hyphen::Pattern::Sh 0.04
TeX::Hyphen::Pattern::Sh_cyrl 0.00
TeX::Hyphen::Pattern::Sh_latn 0.04
TeX::Hyphen::Pattern::Sk 0.04
TeX::Hyphen::Pattern::Sl 0.04
TeX::Hyphen::Pattern::Sl_SI 0.04
TeX::Hyphen::Pattern::Sr 0.00
TeX::Hyphen::Pattern::Sr_cyrl 0.00
TeX::Hyphen::Pattern::Sv 0.04
TeX::Hyphen::Pattern::Tr 0.04
TeX::Hyphen::Pattern::Uk 0.00
TeX::Hyphen::Pattern::Zh_latn 0.04
TeX::Hyphen::Pattern::Zu_ZA 0.04
TeX::Hyphen::czech 0.121
TeX::Hyphen::german 0.121
Tee::App 0.14
Tempest::Gd undef
Tempest::Graphicsmagick undef
Tempest::Imagemagick undef
Template::Alloy::Compile undef
Template::Alloy::Context undef
Template::Alloy::Exception undef
Template::Alloy::HTE undef
Template::Alloy::Iterator undef
Template::Alloy::Operator undef
Template::Alloy::Parse undef
Template::Alloy::Play undef
Template::Alloy::Stream undef
Template::Alloy::TT undef
Template::Alloy::Tmpl undef
Template::Alloy::VMethod undef
Template::Alloy::Velocity undef
Template::AutoFilter::Parser 0.110080
Template::Base 2.78
Template::Benchmark::Engine 1.09_01
Template::Benchmark::Engines::HTMLMacro 1.09_01
Template::Benchmark::Engines::HTMLMason 1.09_01
Template::Benchmark::Engines::HTMLTemplate 1.09_01
Template::Benchmark::Engines::HTMLTemplateCompiled 1.09_01
Template::Benchmark::Engines::HTMLTemplateExpr 1.09_01
Template::Benchmark::Engines::HTMLTemplateJIT 1.09_01
Template::Benchmark::Engines::HTMLTemplatePro 1.09_01
Template::Benchmark::Engines::MojoTemplate 1.09_01
Template::Benchmark::Engines::NTSTemplate 1.09_01
Template::Benchmark::Engines::ParseTemplate 1.09_01
Template::Benchmark::Engines::Solution 1.09_01
Template::Benchmark::Engines::TemplateAlloyHT 1.09_01
Template::Benchmark::Engines::TemplateAlloyTT 1.09_01
Template::Benchmark::Engines::TemplateSandbox 1.09_01
Template::Benchmark::Engines::TemplateTiny 1.09_01
Template::Benchmark::Engines::TemplateToolkit 1.09_01
Template::Benchmark::Engines::Tenjin 1.09_01
Template::Benchmark::Engines::TextClearSilver 1.09_01
Template::Benchmark::Engines::TextClevery 1.09_01
Template::Benchmark::Engines::TextMicroMasonHM 1.09_01
Template::Benchmark::Engines::TextMicroMasonTeTe 1.09_01
Template::Benchmark::Engines::TextMicroTemplate 1.09_01
Template::Benchmark::Engines::TextMicroTemplateExtended 1.09_01
Template::Benchmark::Engines::TextTemplate 1.09_01
Template::Benchmark::Engines::TextTemplateSimple 1.09_01
Template::Benchmark::Engines::TextTemplet 1.09_01
Escape::Houdini
Template::Mustache v0.5.1
Template::Benchmark::Engines::TextTmpl 1.09_01
Template::Benchmark::Engines::TextXslate 1.09_01
Template::Config 2.75
Template::Benchmark::Engines::TextXslateTT 1.09_01
Template::Context 2.98
Template::Constants 2.75
Template::Declare::Buffer undef
Template::Declare::TagSet undef
Template::Declare::TagSet::HTML undef
Template::Declare::TagSet::RDF undef
Template::Declare::TagSet::RDF::EM undef
Template::Directive 2.2
Template::Declare::TagSet::XUL undef
Template::Exception 2.7
Template::Document 2.79
Template::Extract::Compile 0.41
Template::Extract::Parser 0.41
Template::Filters 2.87
Template::Extract::Run 0.41
Template::Flute::Config undef
Template::Flute::Container undef
Template::Flute::Form undef
Template::Flute::HTML undef
Template::Flute::HTML::Table undef
Template::Flute::I18N undef
Template::Flute::Increment undef
Template::Flute::Iterator undef
Template::Flute::Iterator::JSON undef
Template::Flute::List undef
Template::Flute::Specification undef
Template::Flute::Specification::Scoped undef
Template::Flute::Specification::XML undef
Template::Flute::Utils undef
Template::Grammar 2.25
Template::Iterator 2.68
Template::Parser 2.89
Template::Plugin 2.7
threads::variable::reap
Template::Plugin::DBI 2.65
Template::Namespace::Constants 1.27
Template::Plugin::Assert 1
Template::Plugin::CGI 2.7
Template::Plugin::Catalyst::View::PDF::Reuse undef
Template::Plugin::Catalyst::View::PDF::Reuse::Barcode undef
Template::Plugin::Datafile 2.72
Template::Plugin::Date 2.78
Template::Plugin::Directory 2.7
Template::Plugin::Dumper 2.7
Template::Plugin::Filter 1.38
Template::Plugin::File 2.71
Template::Plugin::Format 2.7
Template::Plugin::HTML 2.62
Template::Plugin::Image 1.21
Template::Plugin::Iterator 2.68
Template::Plugin::Math 1.16
Template::Plugin::Pod 2.69
Template::Plugin::Procedural 1.17
Template::Plugin::PodGenerated 0.05
Template::Plugin::Scalar 1
Template::Plugin::String 2.4
Template::Plugin::Table 2.71
Template::Plugin::URL 2.74
Template::Plugin::VMethods::VMethodContainer undef
Template::Plugin::View 2.68
Template::Plugin::Wrap 2.68
Template::Plugin::encoding 0.02
Template::Plugins 2.77
Template::Preprocessor::TTML::Base undef
Template::Provider 2.94
Template::Preprocessor::TTML::CmdLineProc undef
Template::Sandbox::Library 1.04_01
Template::Sandbox::NumberFunctions 1.04_01
Template::Sandbox::StringFunctions 1.04_01
Template::Semantic::Document undef
Template::Semantic::Filter undef
Template::Stash 2.91
Template::Service 2.8
Template::Stash::AutoEscaping::Escaped::Base undef
Template::Stash::AutoEscaping::Escaped::HTML undef
Template::Stash::AutoEscaping::Escaped::YourCode undef
Template::Stash::AutoEscaping::RawString undef
Template::Stash::Context 1.63
Template::Stash::ForceUTF8 0.03
Template::Test 2.75
Template::Stash::XS undef
Template::VMethods 2.16
Template::View 2.91
TemplateM::Galore 2.2
TemplateM::GaloreWin32 2.21
TemplateM::Simple 2.2
TemplateM::Util 2.2
Teng::Iterator undef
Teng::Plugin::BulkInsert undef
Teng::Plugin::Count undef
Teng::Plugin::FindOrCreate undef
Teng::Plugin::Pager undef
Teng::Plugin::Pager::MySQLFoundRows undef
Teng::Plugin::Replace undef
Teng::QueryBuilder undef
Teng::Row undef
Teng::Schema undef
Teng::Schema::Declare undef
Teng::Schema::Dumper undef
Teng::Schema::Loader undef
Teng::Schema::Table undef
Tenjin::Context 0.070001
Term::ANSIColor 3.00
Tenjin::Preprocessor 0.070001
Tenjin::Template 0.070001
Tenjin::Util 0.070001
Test::Assertions::TestScript 1.018
Term::ANSIColorx::AutoFilterFH 2.7185
Term::Clui::FileSelect 1.64
Term::Complete 1.402
Term::Completion::Multi 0.90
Term::Completion::Path 0.90
Term::Completion::_POSIX undef
Term::Completion::_readkey undef
Term::Completion::_stty undef
Term::ReadLine 1.07
Term::Completion::_termsize undef
HTTP::Lite
Data::Entropy::Algorithms
Digest::MD4
Crypt::MySQL
Crypt::UnixCrypt_XS
Digest::CRC
Data::Integer
Authen::DecHpwd
Authen::Passphrase::RejectAll
Term::EditorEdit::Edit undef
Term::ExtendedColor::Xresources
Term::ExtendedColor::Xresources::Colorscheme 0.003
Term::HiliteDiff::_impl 0.10
Term::ReadLine::Gnu::XS 1.17
Term::UI::History undef
Test::Able::Cookbook undef
Test::Able::FatalException undef
Test::Able::Helpers undef
Test::Able::Method::Array undef
Test::Able::Object undef
Test::Able::Planner undef
Test::Able::Role undef
Test::Able::Role::Meta::Class undef
Test::Able::Role::Meta::Method undef
Test::Able::Runner::Role::Meta::Class 1.001
Test::Aggregate::Base 0.364
Test::Base::Filter undef
Test::Aggregate::Builder 0.364
Test::Apache::RewriteRules::ClientEnvs undef
URI::Title
Test::Bot::BasicBot::Pluggable 0.91
Test::Bot::BasicBot::Pluggable::Store 0.91
Test::Builder 0.98
Test::Builder::Mock::Class::Role::Meta::Class 0.0203
Test::Builder::Mock::Class::Role::Object 0.0203
Test::Builder::Module 0.98
Test::Builder::Tester 1.22
Test::Builder::Tester::Color 1.22
Test::CompanionClasses::Base 1.101370
Test::CompanionClasses::Engine 1.101370
Test::CGI::Multipart::Gen::Image 0.0.3
Test::CGI::Multipart::Gen::Text 0.0.3
Test::CPAN::Meta::JSON::Version 0.10
Test::CPAN::Meta::Version 0.18
Test::CPAN::Meta::YAML::Version 0.17
Test::Class::Load 0.35
Test::Class::MethodInfo 0.34
Test::Classy::Base undef
Test::Cmd::Common 1.05
Test::CompanionClasses::Engine_TEST 1.101370
Test::Cukes::Feature undef
Test::Cukes::Scenario undef
SQL::ReservedWords
Test::Data::Array 1.22
Test::Data::Function 1.22
Test::Data::Scalar 1.22
Test::Data::Hash 1.22
Test::Database::Driver undef
Test::Database::Driver::CSV undef
Test::Database::Driver::DBM undef
Test::Database::Driver::Pg undef
Test::Database::Driver::SQLite undef
Test::Database::Driver::SQLite2 undef
Test::Database::Driver::mysql undef
Test::Database::Handle undef
Test::Database::Util undef
Test::Deep::All undef
Test::Deep::Any undef
Test::Deep::Array undef
Test::Deep::ArrayEach undef
Test::Deep::ArrayElementsOnly undef
Test::Deep::ArrayLength undef
Test::Deep::ArrayLengthOnly undef
Test::Deep::Blessed undef
Test::Deep::Boolean undef
Test::Deep::Cache undef
Test::Deep::Cache::Simple undef
Test::Deep::Class undef
Test::Deep::Cmp undef
Test::Deep::Code undef
Test::Deep::Hash undef
Test::Deep::HashEach undef
Test::Deep::HashElements undef
Test::Deep::HashKeys undef
Test::Deep::HashKeysOnly undef
Test::Deep::Ignore undef
Test::Deep::Isa undef
Test::Deep::ListMethods undef
Test::Deep::MM undef
Test::Deep::Methods undef
Test::Deep::NoTest undef
Test::Deep::Number undef
Test::Deep::Ref undef
Test::Deep::RefType undef
Test::Deep::Regexp undef
Test::Deep::RegexpMatches undef
Test::Deep::RegexpRef undef
Test::Deep::RegexpRefOnly undef
Test::Deep::RegexpVersion undef
Test::Deep::ScalarRef undef
Test::Deep::ScalarRefOnly undef
Test::Deep::Set undef
Test::Deep::Shallow undef
Test::Deep::Stack undef
Test::Deep::String undef
Test::Dependencies::Heavy undef
Test::Dependencies::Light undef
Test::Dir::Base 1.005
Test::Excel::Template::Plus 0.02
Test::FITesque::Fixture undef
Test::FITesque::Suite undef
Test::FITesque::Test undef
Test::File::Cleaner::State 0.03
Test::File::ShareDir::TempDirObject 0.1.1
Test::Fixture::DBI::Util 0.01
Test::Fixture::DBI::Util::SQLite 0.01
Test::Fixture::DBI::Util::mysql 0.02
Test::Folder 1.007
Test::Git 1.01
Test::Group::NoWarnings 0.01
Test::Group::Plan 0.01
Test::Group::Tester 0.01
Test::HTTP::Syntax undef
Test::Harness::Results 0.01
Test::Inline::Content 2.212
Test::Inline::Content::Default 2.212
Test::Inline::Content::Legacy 2.212
Test::Mini::Unit v1.0.3
Test::Inline::Content::Simple 2.212
Test::Inline::IO::File 2.212
Test::Inline::Script 2.212
Test::Inline::Section 2.212
Test::Inline::Util 2.212
Test::JSON::Meta::Version 0.09
Test::Lazy::Template undef
Test::Lazy::Tester undef
Test::LeakTrace::Script undef
Test::LectroTest::Compat undef
Test::LectroTest::FailureRecorder undef
Test::LectroTest::Generator 0.14
Test::LectroTest::Property undef
Test::LectroTest::TestRunner undef
Test::Magpie::ArgumentMatcher 0.05
Test::Magpie::Inspect 0.05
Test::Magpie::Invocation 0.05
Test::Magpie::Meta::Class 0.05
Test::Magpie::Mock 0.05
Test::Magpie::Role::HasMock 0.05
Test::Magpie::Role::MethodCall 0.05
Test::Magpie::Spy 0.05
Test::Magpie::Stub 0.05
Test::Magpie::Types 0.05
Test::Magpie::Util 0.05
Test::Magpie::When 0.05
Test::Markdent 0.17
Test::Mini::Assertions undef
Test::Mini::Logger undef
Test::Mini::Logger::TAP undef
Test::Mini::Runner undef
Test::Mini::TestCase undef
Test::Mini::Unit::SharedBehavior undef
Test::Mini::Unit::Sugar::Advice undef
Test::Mini::Unit::Sugar::Reuse undef
Test::Mini::Unit::Sugar::Shared undef
Test::Mini::Unit::Sugar::Test undef
Test::Perl::Critic::Policy 1.115
Test::Mini::Unit::Sugar::TestCase undef
Test::Mock::Class::Role::Meta::Class 0.0303
Test::Mock::Class::Role::Object 0.0303
Test::Mock::HTTP::Request 0.01
Test::Mock::LWP::UserAgent 0.01
Test::MockObject::Extends 1.09
Test::Mock::HTTP::Response 0.01
Test::Moose 2.0002
Test::Mojo undef
Test::MooseX::Daemonize 0.12
Module::Extract::Use
Test::Prereq::Build 1.037_02
Test::More 0.98
Test::Most::Exception 0.23
Test::Mouse undef
Test::Nginx::LWP 0.13
Test::Nginx::Socket 0.13
Test::Nginx::Util 0.13
Test::Path::Router 0.10
Test::NoPlan 0.0.6
Test::NoWarnings::Warning 1.02
Test::Object::Test 0.07
Test::Output::Tie 0.14
Test::Override::UserAgent::Scope 0.004
Test::POP3 0.05
Test::Pockito::DefaultMatcher undef
Test::Pockito::Exported undef
Test::Pockito::Moose::Role undef
Test::RDF::Trine::Store undef
IO::Capture::Stdout::Extended
IO::Capture::Stderr::Extended
IO::Capture::Sayfix
IO::Capture::Tellfix
Devel::Comments
Test::Ranger 0.0.4
Test::Ranger::List 0.0.4
Test::Reporter::Transport 1.57
Test::Reporter::Transport::File 1.57
Test::Reporter::Transport::HTTPGateway 1.57
Test::Reporter::Transport::Mail::Send 1.57
Test::Reporter::Transport::Net::SMTP 1.57
Test::Reporter::Transport::Net::SMTP::TLS 1.57
Test::Rest::Commands undef
Test::Rest::Context undef
Test::Routine::Common 0.009
MooseX::Getopt::OptionTypeMap
Test::Routine::Compositor 0.009
Test::Routine::Manual::Demo 0.009
Test::Routine::Runner 0.009
Test::Routine::Util 0.009
Test::Routine::Test 0.009
Test::Run::Core 0.0123
Test::Run::Assert 0.03
Test::Run::Base undef
Test::Run::Base::PlugHelpers undef
Test::Weaken::Gtk2 37
Test::Run::Base::Plugger undef
Test::Run::CmdLine::Trap::ProveApp undef
Test::Run::Base::Struct undef
Test::Run::Class::Hierarchy undef
Test::Run::CmdLine::Drivers::Default undef
Test::Run::CmdLine::Iface undef
Test::Run::CmdLine
Test::Run::CmdLine::Plugin::BreakOnFailure 0.0.1
Test::Run::CmdLine::Prove 0.0120
Test::Run::CmdLine::Prove::App undef
Test::Run::CmdLine::Trap::Prove undef
Test::Run::Core_GplArt undef
Test::Run::Iface undef
Test::Run::Obj 0.0121
Test::Run::Obj::CanonFailedObj undef
Test::Run::Obj::Error undef
Test::Run::Obj::FailedObj undef
Test::Run::Obj::IntOrUnknown undef
Test::Run::Obj::IntOrUnknown::Moose 0.0121
Test::Run::Obj::TestObj undef
Test::Run::Obj::TotObj undef
Test::Run::Output undef
Test::Run::Plugin::BreakOnFailure 0.0.1
Test::Run::Plugin::CmdLine::Output undef
Test::Run::Sprintf::Named::FromAccessors undef
Test::Run::Straps::Base undef
Test::Run::Straps::EventWrapper undef
Test::Run::Straps::StrapsDetailsObj undef
Test::Run::Straps::StrapsTotalsObj undef
Test::Run::Straps_GplArt undef
Test::Run::Trap::Obj undef
Test::SQL::Translator 1.59
Test::SharedFork::Array undef
Test::SharedFork::Scalar undef
Test::SharedFork::Store undef
System::Info
Test::Smoke::BuildCFG 0.009
Test::Smoke::FTPClient 0.011
Test::Smoke::Mailer 0.014
Test::Smoke::Patcher 0.011
Test::Smoke::Policy 0.004
Test::Smoke::Reporter 0.035
Test::Smoke::Smoker 0.045
Crypt::OpenSSL::Guess
Crypt::OpenSSL::Random
Test::Smoke::SourceTree 0.008
Crypt::OpenSSL::Bignum
Crypt::OpenSSL::RSA
Test::Smoke::Syncer 0.027
Test::Pretty
Test::Smoke::Util 0.58
Test::Story::File undef
Test::Story::Fixture undef
Test::Story::Fixture::Selenium undef
Test::Story::Fixture::VMWare 0.01
Test::Story::TestCase undef
Test::Story::Tutorial undef
Test::StructuredObject::CodeStub 0.01000009
Test::StructuredObject::NonTest 0.01000009
Test::StructuredObject::SubTest 0.01000009
Test::StructuredObject::Test 0.01000009
Authen::Passphrase
MooseX::Types::Authen::Passphrase
KiokuX::User
Test::Tolerant 1.701
Test::StructuredObject::TestSuite 0.01000009
Test::Suite 0.032
Test::Sweet::Keyword::Test undef
Test::Sweet::Types undef
Test::Sys::Info::Driver 0.20
Test::TempDir::Factory undef
Test::TempDir::Handle undef
Test::Tester::Capture undef
Test::Trap::Builder 0.2.1
Test::Trap::Builder::PerlIO 0.2.1
Test::Trap::Builder::SystemSafe 0.2.1
Test::Trap::Builder::TempFile 0.2.1
Test::Unit::Assert undef
Test::Unit::Assertion undef
Test::Unit::Assertion::Boolean undef
Test::Unit::Assertion::CodeRef undef
Test::Unit::Assertion::Exception undef
Test::Unit::Assertion::Regexp undef
Test::Unit::Debug undef
Test::Unit::Decorator undef
Test::Unit::Error undef
Test::Unit::Exception undef
Test::Unit::Failure undef
Test::Unit::HarnessUnit undef
Test::Unit::Listener undef
Test::Unit::Loader undef
Test::Unit::Procedural undef
Test::Unit::Result undef
Test::Unit::Runner undef
Test::Unit::Runner::Terminal undef
Test::Unit::Setup undef
Test::Unit::Test undef
Digest::MD5::File
MooseX::Types::DateTime::MoreCoercions
VM::EC2::Instance::Metadata
VM::EC2::Security::CredentialCache
Net::Amazon::S3
Test::Unit::TestCase undef
Plack::Middleware::ProxyMap
Test::Unit::TestRunner undef
Ouch
Test::Unit::TestSuite undef
Test::Unit::TkTestRunner undef
Test::Unit::Tutorial undef
Test::Unit::UnitHarness 1.1502
Test::Unit::Warning undef
Test::Without::Gtk2Things 37
Exporter::Declare::Magic
Devel::Declare::Parser::Fennec
Fennec::Declare
Fennec
Test::Workflow undef
Test::Workflow::Block undef
Test::Workflow::Layer undef
Test::Workflow::Meta undef
Convert::NLS_DATE_FORMAT
DateTime::Format::Oracle
Test::Workflow::Test undef
Test::XML::Assert 0.03
Test::XML::SAX 0.01
Test::XML::Twig 0.01
Test::XML::XPath 0.03
Test::XML::Compare
DBIx::Schema::DSL
IO::Storm
Sereal::Splitter
DestructAssign
Statistics::R::IO
Mojolicious::Plugin::MethodOverride
Memory::Leak::Hunter
Rose::Object
Rose::DateTime
Time::Clock
Bit::Vector::Overload
Rose::DateTime::Util
Rose::DB
Rose::URI
Net::HTTPS::Any
return::thence
Module::Path::More
lib::filter
Package::New
Rose::HTML::Objects
Rose::DBx::TestDB
Rose::HTMLx::Form::Field::Serial
Scope::Context
Rose::HTMLx::Form::Field::PopUpMenuNumeric
Statistics::ChiSquare
Crypt::Random
Rose::HTMLx::Form::Field::Autocomplete
Rose::HTMLx::Form::Field::Boolean
Rose::HTMLx::Form::Related
Finance::BankVal::International::GetABA
Finance::BankVal::International::GetSWIFT
Finance::BankVal::International::IBANValidate
Protocol::IRC::Client
Net::Async::IRC
Net::Async::Webservice::UPS
warnings::anywhere
JQuery::DataTables::Heavy
Rex::JobControl
ByteBeat
Object::Remote
Catalyst::ControllerRole::CatchErrors
RT::Crypt::SMIME
CPAN::Indexer::Mirror
Apache::HeavyCGI
SQL::Smart
Bio::GFF3::LowLevel 0.3
Bio::GFF3::Transform::FromFasta 0.3
Bio::KEGG v0.1.5
Bio::KEGG::genome v0.1.5
Bio::KEGG::ko v0.1.5
Bio::KEGG::pathway v0.1.5
Bio::KEGGI v0.1.5
Bio::KEGGI::genome v0.1.5
Bio::KEGGI::ko v0.1.5
Bio::KEGGI::pathway v0.1.5
Tangram
Bio::MAGETAB::ArrayDesign undef
Bio::MAGETAB::Assay undef
Bio::MAGETAB::BaseClass undef
Bio::MAGETAB::Comment undef
Bio::MAGETAB::CompositeElement undef
Bio::MAGETAB::Contact undef
Bio::MAGETAB::ControlledTerm undef
Bio::MAGETAB::Data undef
Bio::MAGETAB::DataAcquisition undef
Bio::MAGETAB::DataFile undef
Bio::MAGETAB::DataMatrix undef
Bio::MAGETAB::DatabaseEntry undef
Bio::MAGETAB::DesignElement undef
Bio::MAGETAB::Edge undef
Bio::MAGETAB::Event undef
Bio::MAGETAB::Extract undef
Bio::MAGETAB::Factor undef
Bio::MAGETAB::FactorValue undef
Bio::MAGETAB::Feature undef
Bio::MAGETAB::Investigation undef
Bio::MAGETAB::LabeledExtract undef
Bio::MAGETAB::Material undef
Bio::MAGETAB::MatrixColumn undef
Bio::MAGETAB::MatrixRow undef
Bio::MAGETAB::Measurement undef
Bio::MAGETAB::Node undef
Bio::MAGETAB::Normalization undef
Bio::MAGETAB::ParameterValue undef
Bio::MAGETAB::Protocol undef
Bio::MAGETAB::ProtocolApplication undef
Bio::MAGETAB::ProtocolParameter undef
Bio::MAGETAB::Publication undef
Bio::MAGETAB::Reporter undef
Bio::MAGETAB::SDRF undef
Bio::MAGETAB::SDRFRow undef
Bio::MAGETAB::Sample undef
Bio::MAGETAB::Source undef
Bio::MAGETAB::TermSource undef
Bio::MAGETAB::Types undef
Bio::MAGETAB::Util::Builder undef
Bio::MAGETAB::Util::DBLoader undef
Bio::MAGETAB::Util::Persistence undef
Bio::MAGETAB::Util::Reader 1
Bio::MAGETAB::Util::Reader::ADF undef
Bio::MAGETAB::Util::Reader::DataMatrix undef
Bio::MAGETAB::Util::Reader::IDF undef
Bio::MAGETAB::Util::Reader::SDRF undef
Bio::MAGETAB::Util::Reader::Tabfile undef
Bio::MAGETAB::Util::Reader::TagValueFile undef
Bio::MAGETAB::Util::RewriteAE undef
Bio::MAGETAB::Util::Writer undef
Bio::MAGETAB::Util::Writer::ADF undef
Bio::MAGETAB::Util::Writer::GraphViz undef
Bio::MAGETAB::Util::Writer::IDF undef
Bio::MAGETAB::Util::Writer::SDRF undef
Bio::MAGETAB::Util::Writer::Tabfile undef
Bio::NEXUS::AssumptionsBlock 0.76
Bio::NEXUS::Block 0.76
Bio::NEXUS::CharactersBlock 0.76
Bio::NEXUS::CodonsBlock 0.76
Bio::NEXUS::DataBlock 0.76
Bot::BasicBot::Pluggable::Module 0.91
Bio::NEXUS::DistancesBlock 0.76
Bio::NEXUS::Functions 0.76
Bio::NEXUS::HistoryBlock 0.76
Bio::NEXUS::Import 0.2.0
Bio::NEXUS::Matrix 0.76
Bio::NEXUS::NHXCmd 0.76
Bio::NEXUS::Node 0.76
Bio::NEXUS::NotesBlock 0.76
Bio::NEXUS::SetsBlock 0.76
Bio::NEXUS::SpanBlock 0.76
Bio::NEXUS::TaxUnit 0.76
Bio::NEXUS::TaxUnitSet 0.76
Bio::NEXUS::TaxaBlock 0.76
Bio::NEXUS::Tools::GraphicsParams 0.76
Bio::NEXUS::Tools::NexModifier 0.76
Bio::NEXUS::Tools::NexPlotter 0.76
Bio::NEXUS::Tree 0.76
Bio::NEXUS::TreesBlock 0.76
Bio::NEXUS::UnalignedBlock 0.76
Bio::NEXUS::UnknownBlock 0.76
Bio::NEXUS::Util::Exceptions undef
Bio::NEXUS::Util::Logger undef
Bio::NEXUS::WeightSet 0.76
SVG
Math::Random
Math::CDF
Data::TemporaryBag
SWF::File
SWF::BinStream
SWF::Element
SWF::Parser
SWF::Builder
XML::XML2JSON
Bio::Phylo::EvolutionaryModels 0.36_1641
Bio::Phylo::Factory undef
Bio::Phylo::Forest undef
Bio::Phylo::Forest::DrawNode undef
Bio::Phylo::Forest::DrawTree undef
Bio::Phylo::Forest::Node undef
Bio::Phylo::Forest::Tree undef
Bio::Phylo::Generator undef
Bio::Phylo::IO undef
Bio::Phylo::Identifiable undef
Bio::Phylo::Listable undef
Bio::Phylo::Matrices undef
Bio::Phylo::Matrices::Character undef
Bio::Phylo::Matrices::Characters undef
Bio::Phylo::Matrices::Datatype undef
Bio::Phylo::Matrices::Datatype::Continuous undef
Bio::Phylo::Matrices::Datatype::Custom undef
Bio::Phylo::Matrices::Datatype::Dna undef
Bio::Phylo::Matrices::Datatype::Mixed undef
Bio::Phylo::Matrices::Datatype::Protein undef
Bio::Phylo::Matrices::Datatype::Restriction undef
Bio::Phylo::Matrices::Datatype::Rna undef
Bio::Phylo::Matrices::Datatype::Standard undef
Bio::Phylo::Matrices::Datum undef
Bio::Phylo::Matrices::Matrix undef
Bio::Phylo::Matrices::TypeSafeData undef
Bio::Phylo::Mediators::TaxaMediator undef
Bio::Phylo::NeXML::DOM undef
Bio::Phylo::NeXML::DOM::Document undef
Bio::Phylo::NeXML::DOM::Document::Libxml undef
Bio::Phylo::NeXML::DOM::Document::Twig undef
Bio::Phylo::NeXML::DOM::Element undef
Bio::Phylo::NeXML::DOM::Element::Libxml undef
Bio::Phylo::NeXML::DOM::Element::Twig undef
Bio::Phylo::NeXML::Meta undef
Bio::Phylo::NeXML::Meta::XMLLiteral undef
Bio::Phylo::NeXML::Writable undef
Bio::Phylo::Parsers::Abstract undef
Bio::Phylo::Parsers::Fasta undef
Bio::Phylo::Parsers::Json undef
Bio::Phylo::Parsers::Newick undef
Bio::Phylo::Parsers::Nexml undef
Bio::Phylo::Parsers::Nexus undef
Bio::Phylo::Parsers::Phylip undef
Bio::Phylo::Parsers::Phyloxml undef
Bio::Phylo::Parsers::Table undef
Bio::Phylo::Parsers::Taxlist undef
Bio::Phylo::Parsers::Tolweb undef
Bio::Phylo::Project undef
Bio::Phylo::Set undef
Bio::Phylo::Taxa undef
Bio::Phylo::Taxa::TaxaLinker undef
Bio::Phylo::Taxa::Taxon undef
Bio::Phylo::Taxa::TaxonLinker undef
Bio::Phylo::Treedrawer undef
Bio::Phylo::Treedrawer::Abstract undef
Bio::Phylo::Treedrawer::Canvas undef
Bio::Phylo::Treedrawer::Gif undef
Bio::Phylo::Treedrawer::Jpeg undef
Bio::Phylo::Treedrawer::Pdf undef
Bio::Phylo::Treedrawer::Png undef
Bio::Phylo::Treedrawer::Svg undef
Bio::Phylo::Treedrawer::Swf undef
Bio::Phylo::Unparsers::Abstract undef
Bio::Phylo::Unparsers::Mrp undef
Bio::Phylo::Unparsers::Newick undef
Bio::Phylo::Unparsers::Nexml undef
Bio::Phylo::Unparsers::Nexus undef
Bio::Phylo::Unparsers::Pagel undef
Bio::Phylo::Unparsers::Phylip undef
Bio::Phylo::Unparsers::Phyloxml undef
Bio::Phylo::Util::CONSTANT undef
Bio::Phylo::Util::Exceptions undef
Bio::Phylo::Util::IDPool undef
Bio::Phylo::Util::Logger undef
Bio::Phylo::Util::StackTrace undef
Bio::Protease::Role::Specificity::Regex 1.102690
Bio::Protease::Role::WithCache 1.102690
Bio::Protease::Types 1.102690
Bio::ProteaseI 1.102690
Bit::Vector::String 7.1
Block::NamedVar::ForLike undef
Block::NamedVar::MapLike undef
Blog::Spam::API undef
Blog::Spam::Plugin::00blacklist undef
Blog::Spam::Plugin::00whitelist undef
Blog::Spam::Plugin::Sample undef
Blog::Spam::Plugin::badip undef
Blog::Spam::Plugin::bayasian undef
Blog::Spam::Plugin::bayesian undef
Blog::Spam::Plugin::bogusip undef
Blog::Spam::Plugin::drone undef
Blog::Spam::Plugin::dropme undef
Blog::Spam::Plugin::emailtests undef
Blog::Spam::Plugin::hashcash undef
Blog::Spam::Plugin::httpbl undef
Blog::Spam::Plugin::logger undef
Blog::Spam::Plugin::lotsaurls undef
Blog::Spam::Plugin::multilinks undef
Blog::Spam::Plugin::rdns undef
Blog::Spam::Plugin::requiremx undef
Blog::Spam::Plugin::sfs undef
Blog::Spam::Plugin::size undef
Blog::Spam::Plugin::stopwords undef
Blog::Spam::Plugin::strong undef
Blog::Spam::Plugin::surbl undef
Blog::Spam::Plugin::wordcount undef
Blog::Spam::Server 1.0.2
Bot::BasicBot::Pluggable::Module::Auth 0.91
Bot::BasicBot::Pluggable::Module::Base 0.91
Bot::BasicBot::Pluggable::Module::ChanOp 0.91
Bot::BasicBot::Pluggable::Module::DNS 0.91
Bot::BasicBot::Pluggable::Module::Google 0.91
Bot::BasicBot::Pluggable::Module::Infobot 0.91
Bot::BasicBot::Pluggable::Module::Join 0.91
Bot::BasicBot::Pluggable::Module::Karma 0.91
Bot::BasicBot::Pluggable::Module::Loader 0.91
Bot::BasicBot::Pluggable::Module::Seen 0.86
Bot::BasicBot::Pluggable::Module::Title 0.91
Bot::BasicBot::Pluggable::Module::Vars 0.91
Bot::BasicBot::Pluggable::Store 0.91
Bot::BasicBot::Pluggable::Store::DBI 0.91
Bot::Training::Plugin 0.04
Bot::BasicBot::Pluggable::Store::Deep 0.91
Bot::BasicBot::Pluggable::Store::Memory 0.91
Bot::BasicBot::Pluggable::Store::Storable 0.91
Bot::BasicBot::Pluggable::Terminal 0.91
Sah::Schema::posint
Sah::Schema::poseven
Sah::Schema::example::recurse2a
Sah::Schema::example::recurse1
Sah::Schema::example::recurse2b
Sah::Schema::example::has_merge
Package::Pkg
Getopt::Usaginator
MooseX::Types::DateTime::W3C
Carp::Clan::Share
Config::JFDI
DBIx::Class::ResultSet::RecursiveUpdate
HTML::FormHandler::Model::DBIC
Bracket::Controller::Admin undef
Bracket::Controller::Auth undef
Bracket::Controller::Final4 undef
Bracket::Controller::Player undef
Bracket::Controller::Region undef
Bracket::Controller::Root undef
Bracket::Form::Login undef
Bracket::Form::Password::Change undef
Bracket::Form::Password::Reset undef
Bracket::Form::Password::ResetEmail undef
Bracket::Form::Register undef
Bracket::Model::DBIC undef
Bracket::Schema undef
Bracket::Schema::Result::Game undef
Bracket::Schema::Result::Pick undef
Bracket::Schema::Result::Player undef
Bracket::Schema::Result::PlayerRole undef
Bracket::Schema::Result::Region undef
Bracket::Schema::Result::RegionScore undef
Bracket::Schema::Result::Role undef
Bracket::Schema::Result::Session undef
Bracket::Schema::Result::Team undef
Bracket::Schema::Result::Token undef
Bracket::View::TT undef
Brackup::Backup undef
Brackup::BackupStats undef
Brackup::ChunkIterator undef
Brackup::Chunker::Default undef
Brackup::Chunker::MP3 undef
Brackup::CompositeChunk undef
Brackup::Config undef
Brackup::ConfigSection undef
Brackup::Decrypt undef
Brackup::DecryptedFile undef
Brackup::Dict::DBM undef
Brackup::Dict::Null undef
Brackup::Dict::SQLite undef
Brackup::Dict::SQLite2 undef
Brackup::DigestCache undef
Brackup::File undef
Brackup::GPGProcManager undef
Brackup::GPGProcess undef
Brackup::InventoryDatabase undef
Brackup::Metafile undef
Brackup::Mount undef
Brackup::PositionedChunk undef
Brackup::Restore undef
Brackup::Root undef
Brackup::StoredChunk undef
Brackup::Target undef
Brackup::Target::Amazon undef
Brackup::Target::CloudFiles undef
Brackup::Target::Filebased undef
Brackup::Target::Filesystem undef
Brackup::Target::Ftp undef
Brackup::Target::GoogleAppEngine undef
Brackup::Target::Riak undef
Brackup::Target::Sftp undef
Brackup::TargetBackupStatInfo undef
Brackup::Test undef
Brackup::Util undef
MooseX::Params::Validate
Brannigan::Tree 0.8
Bread::Board::BlockInjection 0.18
Bread::Board::ConstructorInjection 0.18
Bread::Board::Container 0.18
Brannigan::Validations 0.8
Bread::Board::Container::Parameterized 0.18
Bread::Board::Declare::BlockInjection 0.06
Bread::Board::Declare::ConstructorInjection 0.06
Bread::Board::Declare::Literal 0.06
Bread::Board::Declare::Meta::Role::Attribute 0.06
Bread::Board::Declare::Meta::Role::Class 0.06
Bread::Board::Declare::Meta::Role::Instance 0.06
Bread::Board::Declare::Role::Object 0.06
Bread::Board::Declare::Role::Service 0.06
Bread::Board::Dependency 0.18
Bread::Board::Dumper 0.18
Bread::Board::LifeCycle 0.18
Bread::Board::LifeCycle::Singleton 0.18
Bread::Board::Literal 0.18
Bread::Board::Service 0.18
Bread::Board::LifeCycle::Singleton::WithParameters 0.18
Bread::Board::Service::Deferred 0.18
Bread::Board::Service::Deferred::Thunk 0.18
Bread::Board::Service::Inferred 0.18
Bread::Board::Service::WithClass 0.18
Bread::Board::Service::WithDependencies 0.18
Bread::Board::Service::WithParameters 0.18
Bread::Board::SetterInjection 0.18
Bread::Board::Types 0.18
Bread::Board::Traversable 0.18
Buffer::Transactional::Buffer 0.02
Buffer::Transactional::Buffer::Array 0.02
Buffer::Transactional::Buffer::File 0.02
Buffer::Transactional::Buffer::Lazy 0.02
Buffer::Transactional::Buffer::String 0.02
Log::Dispatch::DBI
Bundle::Compress::Zlib 2.034
Data::FormValidator::Constraints
Data::FormValidator
FindBin::Real
CGI::Session
Business::AU::Ledger::Database 0.88
Business::AU::Ledger::Database::Base 0.88
Business::AU::Ledger::Database::Payment 0.88
Business::AU::Ledger::Database::Receipt 0.88
Business::AU::Ledger::Util::Config 0.88
Business::AU::Ledger::Util::Create 0.88
Business::AU::Ledger::Util::Validate 0.88
Business::AU::Ledger::View 0.88
Business::AU::Ledger::View::Base 0.88
Business::AU::Ledger::View::Context 0.88
Business::AU::Ledger::View::Payment 0.88
Business::AU::Ledger::View::Receipt 0.88
Business::AU::Ledger::View::Reconciliation 0.88
Data::Session
CGI::Untaint
WWW::Scraper::Wikipedia::ISO3166::Database
Log::Handler::Output::DBI
Business::Cart::Generic::Base 0.80
Business::Cart::Generic::Controller 0.80
Business::Cart::Generic::Controller::Add undef
Business::Cart::Generic::Controller::AutoComplete 0.80
Business::Cart::Generic::Controller::Initialize 0.80
Business::Cart::Generic::Controller::Search 0.80
Business::Cart::Generic::Database 0.80
Business::Cart::Generic::Database::AutoComplete 0.80
Business::Cart::Generic::Database::Base 0.80
Business::Cart::Generic::Database::Create 0.80
Business::Cart::Generic::Database::Export 0.80
Business::Cart::Generic::Database::Import 0.80
Business::Cart::Generic::Database::Loader 0.80
Business::Cart::Generic::Database::Order 0.80
Business::Cart::Generic::Database::Product 0.80
Business::Cart::Generic::Database::Search 0.80
Business::Cart::Generic::Schema undef
Business::Cart::Generic::Schema::Result::Category undef
Business::Cart::Generic::Schema::Result::CategoryDescription undef
Business::Cart::Generic::Schema::Result::Country undef
Business::Cart::Generic::Schema::Result::Currency undef
Business::Cart::Generic::Schema::Result::Customer undef
Business::Cart::Generic::Schema::Result::CustomerStatuse undef
Business::Cart::Generic::Schema::Result::CustomerType undef
Business::Cart::Generic::Schema::Result::EmailAddress undef
Business::Cart::Generic::Schema::Result::EmailAddressType undef
Business::Cart::Generic::Schema::Result::EmailList undef
Business::Cart::Generic::Schema::Result::Gender undef
Business::Cart::Generic::Schema::Result::Language undef
Business::Cart::Generic::Schema::Result::Log undef
Business::Cart::Generic::Schema::Result::Logon undef
Business::Cart::Generic::Schema::Result::Manufacturer undef
Business::Cart::Generic::Schema::Result::ManufacturersInfo undef
Business::Cart::Generic::Schema::Result::Order undef
Business::Cart::Generic::Schema::Result::OrderHistory undef
Business::Cart::Generic::Schema::Result::OrderItem undef
Business::Cart::Generic::Schema::Result::OrderStatuse undef
Business::Cart::Generic::Schema::Result::PaymentMethod undef
Business::Cart::Generic::Schema::Result::PhoneList undef
Business::Cart::Generic::Schema::Result::PhoneNumber undef
Business::Cart::Generic::Schema::Result::PhoneNumberType undef
Business::Cart::Generic::Schema::Result::Product undef
Business::Cart::Generic::Schema::Result::ProductClass undef
Business::Cart::Generic::Schema::Result::ProductColor undef
Business::Cart::Generic::Schema::Result::ProductDescription undef
Business::Cart::Generic::Schema::Result::ProductSize undef
Business::Cart::Generic::Schema::Result::ProductStatuse undef
Business::Cart::Generic::Schema::Result::ProductStyle undef
Business::Cart::Generic::Schema::Result::ProductType undef
Business::Cart::Generic::Schema::Result::ProductsToCategory undef
Business::Cart::Generic::Schema::Result::Session undef
Business::Cart::Generic::Schema::Result::StreetAddress undef
Business::Cart::Generic::Schema::Result::TaxClass undef
Business::Cart::Generic::Schema::Result::TaxRate undef
Business::Cart::Generic::Schema::Result::Title undef
Business::Cart::Generic::Schema::Result::WeightClass undef
Business::Cart::Generic::Schema::Result::WeightClassRule undef
Business::Cart::Generic::Schema::Result::YesNo undef
Business::Cart::Generic::Schema::Result::Zone undef
Business::Cart::Generic::Util::Config 0.80
Business::Cart::Generic::View 0.80
Business::Cart::Generic::View::Add 0.80
Business::Cart::Generic::View::AutoComplete 0.80
Business::Cart::Generic::View::Base 0.80
Business::Cart::Generic::View::Order 0.80
Business::Cart::Generic::View::Product 0.80
Business::Cart::Generic::View::Search 0.80
Business::EDI::CodeList 0.01
Business::EDI::CodeList::AccountTypeCodeQualifier undef
Business::EDI::CodeList::AccountingEntryTypeNameCode undef
Business::EDI::CodeList::AcknowledgementRequest undef
Business::EDI::CodeList::ActionCode undef
Business::EDI::CodeList::ActionCoded undef
Business::EDI::CodeList::AddressFormatCode undef
Business::EDI::CodeList::AddressPurposeCode undef
Business::EDI::CodeList::AddressStatusCode undef
Business::EDI::CodeList::AddressTypeCode undef
Business::EDI::CodeList::AdjustmentReasonDescriptionCode undef
Business::EDI::CodeList::AgreementTypeCodeQualifier undef
Business::EDI::CodeList::AgreementTypeDescriptionCode undef
Business::EDI::CodeList::AlgorithmCodeListIdentifier undef
Business::EDI::CodeList::AlgorithmCoded undef
Business::EDI::CodeList::AlgorithmParameterQualifier undef
Business::EDI::CodeList::AlgorithmParameterValue undef
Business::EDI::CodeList::AllowanceOrChargeCodeQualifier undef
Business::EDI::CodeList::AllowanceOrChargeIdentificationCode undef
Business::EDI::CodeList::AnticollisionSegmentGroupIdentification undef
Business::EDI::CodeList::ApplicabilityCodeQualifier undef
Business::EDI::CodeList::ApplicationPassword undef
Business::EDI::CodeList::ApplicationRecipientIdentification undef
Business::EDI::CodeList::ApplicationReference undef
Business::EDI::CodeList::ApplicationSenderIdentification undef
Business::EDI::CodeList::AssociationAssignedCode undef
Business::EDI::CodeList::AttendanceTypeCodeQualifier undef
Business::EDI::CodeList::AttendeeCategoryDescriptionCode undef
Business::EDI::CodeList::AttributeFunctionCodeQualifier undef
Business::EDI::CodeList::BackOrderArrangementTypeCode undef
Business::EDI::CodeList::BankOperationCode undef
Business::EDI::CodeList::BasisCodeQualifier undef
Business::EDI::CodeList::BusinessFunctionCode undef
Business::EDI::CodeList::BusinessFunctionTypeCodeQualifier undef
Business::EDI::CodeList::CalculationSequenceCode undef
Business::EDI::CodeList::CargoTypeClassificationCode undef
Business::EDI::CodeList::CertaintyDescriptionCode undef
Business::EDI::CodeList::CertificateOriginalCharacterSetRepertoireCoded undef
Business::EDI::CodeList::CertificateReference undef
Business::EDI::CodeList::CertificateSequenceNumber undef
Business::EDI::CodeList::CertificateSyntaxAndVersionCoded undef
Business::EDI::CodeList::ChangeReasonDescriptionCode undef
Business::EDI::CodeList::CharacterEncodingCoded undef
Business::EDI::CodeList::CharacteristicRelevanceCode undef
Business::EDI::CodeList::CharacteristicValueDescriptionCode undef
Business::EDI::CodeList::ChargeCategoryCode undef
Business::EDI::CodeList::ChargePeriodTypeCode undef
Business::EDI::CodeList::ChargeUnitCode undef
Business::EDI::CodeList::ClassTypeCode undef
Business::EDI::CodeList::ClauseCodeQualifier undef
Business::EDI::CodeList::ClinicalInformationTypeCodeQualifier undef
Business::EDI::CodeList::ClinicalInterventionDescriptionCode undef
Business::EDI::CodeList::ClinicalInterventionTypeCodeQualifier undef
Business::EDI::CodeList::CodeListDirectoryVersionNumber undef
Business::EDI::CodeList::CodeListResponsibleAgencyCode undef
Business::EDI::CodeList::CodeSetIndicatorCode undef
Business::EDI::CodeList::CodeValueSourceCode undef
Business::EDI::CodeList::CommonAccessReference undef
Business::EDI::CodeList::CommunicationMeansTypeCode undef
Business::EDI::CodeList::CommunicationMediumTypeCode undef
Business::EDI::CodeList::ComputerEnvironmentDetailsCodeQualifier undef
Business::EDI::CodeList::ConfigurationOperationCode undef
Business::EDI::CodeList::ContactFunctionCode undef
Business::EDI::CodeList::ContractAndCarriageConditionCode undef
Business::EDI::CodeList::ContributionCodeQualifier undef
Business::EDI::CodeList::ContributionTypeDescriptionCode undef
Business::EDI::CodeList::ControlTotalTypeCodeQualifier undef
Business::EDI::CodeList::ControllingAgencyCoded undef
Business::EDI::CodeList::ConveyanceCallPurposeDescriptionCode undef
Business::EDI::CodeList::CreditCoverRequestTypeCode undef
Business::EDI::CodeList::CreditCoverResponseReasonCode undef
Business::EDI::CodeList::CreditCoverResponseTypeCode undef
Business::EDI::CodeList::CryptographicModeOfOperationCoded undef
Business::EDI::CodeList::CurrencyTypeCodeQualifier undef
Business::EDI::CodeList::CurrencyUsageCodeQualifier undef
Business::EDI::CodeList::DamageDetailsCodeQualifier undef
Business::EDI::CodeList::DangerousGoodsRegulationsCode undef
Business::EDI::CodeList::DataElementUsageTypeCode undef
Business::EDI::CodeList::DataFormatDescriptionCode undef
Business::EDI::CodeList::DataRepresentationTypeCode undef
Business::EDI::CodeList::Date undef
Business::EDI::CodeList::DateAndTimeQualifier undef
Business::EDI::CodeList::DateOrTimeOrPeriodFormatCode undef
Business::EDI::CodeList::DateOrTimeOrPeriodFunctionCodeQualifier undef
Business::EDI::CodeList::DefinitionExtentCode undef
Business::EDI::CodeList::DefinitionFunctionCode undef
Business::EDI::CodeList::DeliveryInstructionCode undef
Business::EDI::CodeList::DeliveryOrTransportTermsDescriptionCode undef
Business::EDI::CodeList::DeliveryOrTransportTermsFunctionCode undef
Business::EDI::CodeList::DeliveryPlanCommitmentLevelCode undef
Business::EDI::CodeList::DescriptionFormatCode undef
Business::EDI::CodeList::DesignatedClassCode undef
Business::EDI::CodeList::DespatchPatternCode undef
Business::EDI::CodeList::DespatchPatternTimingCode undef
Business::EDI::CodeList::DiagnosisTypeCode undef
Business::EDI::CodeList::DialogueIdentification undef
Business::EDI::CodeList::DialogueReleaseNumber undef
Business::EDI::CodeList::DialogueVersionNumber undef
Business::EDI::CodeList::DimensionTypeCodeQualifier undef
Business::EDI::CodeList::DischargeTypeDescriptionCode undef
Business::EDI::CodeList::DiscrepancyNatureIdentificationCode undef
Business::EDI::CodeList::DocumentLineActionCode undef
Business::EDI::CodeList::DocumentNameCode undef
Business::EDI::CodeList::DocumentStatusCode undef
Business::EDI::CodeList::DosageAdministrationCodeQualifier undef
Business::EDI::CodeList::DuplicateIndicator undef
Business::EDI::CodeList::DutyOrTaxOrFeeCategoryCode undef
Business::EDI::CodeList::DutyOrTaxOrFeeFunctionCodeQualifier undef
Business::EDI::CodeList::DutyOrTaxOrFeeRateBasisCode undef
Business::EDI::CodeList::DutyOrTaxOrFeeTypeNameCode undef
Business::EDI::CodeList::DutyRegimeTypeCode undef
Business::EDI::CodeList::EditMaskRepresentationCode undef
Business::EDI::CodeList::EmploymentDetailsCodeQualifier undef
Business::EDI::CodeList::EnactingPartyIdentifier undef
Business::EDI::CodeList::EncryptionReferenceNumber undef
Business::EDI::CodeList::EquipmentSizeAndTypeDescriptionCode undef
Business::EDI::CodeList::EquipmentStatusCode undef
Business::EDI::CodeList::EquipmentSupplierCode undef
Business::EDI::CodeList::EquipmentTypeCodeQualifier undef
Business::EDI::CodeList::ErroneousComponentDataElementPosition undef
Business::EDI::CodeList::ErroneousDataElementOccurrence undef
Business::EDI::CodeList::ErroneousDataElementPositionInSegment undef
Business::EDI::CodeList::EventDate undef
Business::EDI::CodeList::EventDetailsCodeQualifier undef
Business::EDI::CodeList::EventTime undef
Business::EDI::CodeList::EventTimeReferenceCode undef
Business::EDI::CodeList::ExcessTransportationReasonCode undef
Business::EDI::CodeList::ExcessTransportationResponsibilityCode undef
Business::EDI::CodeList::ExchangeRateCurrencyMarketIdentifier undef
Business::EDI::CodeList::FacilityTypeDescriptionCode undef
Business::EDI::CodeList::FilterFunctionCoded undef
Business::EDI::CodeList::FinancialTransactionTypeCode undef
Business::EDI::CodeList::FirstAndLastTransfer undef
Business::EDI::CodeList::FormulaSequenceCodeQualifier undef
Business::EDI::CodeList::FormulaSequenceOperandCode undef
Business::EDI::CodeList::FormulaTypeCodeQualifier undef
Business::EDI::CodeList::FreeTextFormatCode undef
Business::EDI::CodeList::FreeTextFunctionCode undef
Business::EDI::CodeList::FrequencyCode undef
Business::EDI::CodeList::FrequencyCodeQualifier undef
Business::EDI::CodeList::FullOrEmptyIndicatorCode undef
Business::EDI::CodeList::GeographicAreaCode undef
Business::EDI::CodeList::GeographicalPositionCodeQualifier undef
Business::EDI::CodeList::GovernmentActionCode undef
Business::EDI::CodeList::GovernmentAgencyIdentificationCode undef
Business::EDI::CodeList::GovernmentInvolvementCode undef
Business::EDI::CodeList::GovernmentProcedureCode undef
Business::EDI::CodeList::GroupControlCount undef
Business::EDI::CodeList::GroupReferenceNumber undef
Business::EDI::CodeList::HandlingInstructionDescriptionCode undef
Business::EDI::CodeList::HaulageArrangementsCode undef
Business::EDI::CodeList::HierarchicalStructureRelationshipCode undef
Business::EDI::CodeList::HierarchyObjectCodeQualifier undef
Business::EDI::CodeList::IdentificationCodeQualifier undef
Business::EDI::CodeList::IndexCodeQualifier undef
Business::EDI::CodeList::IndexRepresentationCode undef
Business::EDI::CodeList::IndexTypeIdentifier undef
Business::EDI::CodeList::IndexingStructureCodeQualifier undef
Business::EDI::CodeList::InformationCategoryCode undef
Business::EDI::CodeList::InformationDetailsCodeQualifier undef
Business::EDI::CodeList::InitiatorControlReference undef
Business::EDI::CodeList::InitiatorReferenceIdentification undef
Business::EDI::CodeList::InstructionDescriptionCode undef
Business::EDI::CodeList::InstructionReceivingPartyIdentifier undef
Business::EDI::CodeList::InstructionTypeCodeQualifier undef
Business::EDI::CodeList::InteractiveMessageReferenceNumber undef
Business::EDI::CodeList::InterchangeAgreementIdentifier undef
Business::EDI::CodeList::InterchangeControlCount undef
Business::EDI::CodeList::InterchangeControlReference undef
Business::EDI::CodeList::InterchangeRecipientIdentification undef
Business::EDI::CodeList::InterchangeRecipientInternalIdentification undef
Business::EDI::CodeList::InterchangeRecipientInternalSubidentification undef
Business::EDI::CodeList::InterchangeSenderIdentification undef
Business::EDI::CodeList::InterchangeSenderInternalIdentification undef
Business::EDI::CodeList::InterchangeSenderInternalSubidentification undef
Business::EDI::CodeList::IntracompanyPaymentIndicatorCode undef
Business::EDI::CodeList::InventoryBalanceMethodCode undef
Business::EDI::CodeList::InventoryMovementDirectionCode undef
Business::EDI::CodeList::InventoryMovementReasonCode undef
Business::EDI::CodeList::InventoryTypeCode undef
Business::EDI::CodeList::ItemAvailabilityCode undef
Business::EDI::CodeList::ItemCharacteristicCode undef
Business::EDI::CodeList::ItemDescriptionCode undef
Business::EDI::CodeList::ItemTypeIdentificationCode undef
Business::EDI::CodeList::KeyManagementFunctionQualifier undef
Business::EDI::CodeList::KeyName undef
Business::EDI::CodeList::LanguageCodeQualifier undef
Business::EDI::CodeList::LanguageCoded undef
Business::EDI::CodeList::LengthOfDataInOctetsOfBits undef
Business::EDI::CodeList::LengthOfObjectInOctetsOfBits undef
Business::EDI::CodeList::LengthTypeCode undef
Business::EDI::CodeList::ListParameter undef
Business::EDI::CodeList::ListParameterQualifier undef
Business::EDI::CodeList::LocationFunctionCodeQualifier undef
Business::EDI::CodeList::MaintenanceOperationCode undef
Business::EDI::CodeList::MaritalStatusDescriptionCode undef
Business::EDI::CodeList::MarkingInstructionsCode undef
Business::EDI::CodeList::MarkingTypeCode undef
Business::EDI::CodeList::MeasuredAttributeCode undef
Business::EDI::CodeList::MeasurementPurposeCodeQualifier undef
Business::EDI::CodeList::MeasurementSignificanceCode undef
Business::EDI::CodeList::MembershipCategoryDescriptionCode undef
Business::EDI::CodeList::MembershipLevelCodeQualifier undef
Business::EDI::CodeList::MembershipTypeCodeQualifier undef
Business::EDI::CodeList::MessageFunctionCode undef
Business::EDI::CodeList::MessageGroupIdentification undef
Business::EDI::CodeList::MessageImplementationGuidelineIdentification undef
Business::EDI::CodeList::MessageImplementationGuidelineReleaseNumber undef
Business::EDI::CodeList::MessageImplementationGuidelineVersionNumber undef
Business::EDI::CodeList::MessageReferenceNumber undef
Business::EDI::CodeList::MessageRelationCoded undef
Business::EDI::CodeList::MessageReleaseNumber undef
Business::EDI::CodeList::MessageSectionCode undef
Business::EDI::CodeList::MessageSubsetIdentification undef
Business::EDI::CodeList::MessageSubsetReleaseNumber undef
Business::EDI::CodeList::MessageSubsetVersionNumber undef
Business::EDI::CodeList::MessageType undef
Business::EDI::CodeList::MessageTypeSubfunctionIdentification undef
Business::EDI::CodeList::MessageVersionNumber undef
Business::EDI::CodeList::ModeOfOperationCodeListIdentifier undef
Business::EDI::CodeList::MonetaryAmountFunctionDescriptionCode undef
Business::EDI::CodeList::MonetaryAmountTypeCodeQualifier undef
Business::EDI::CodeList::MovementTypeDescriptionCode undef
Business::EDI::CodeList::NameComponentTypeCodeQualifier undef
Business::EDI::CodeList::NameComponentUsageCode undef
Business::EDI::CodeList::NameOriginalAlphabetCode undef
Business::EDI::CodeList::NameStatusCode undef
Business::EDI::CodeList::NameTypeCode undef
Business::EDI::CodeList::NationalityCodeQualifier undef
Business::EDI::CodeList::NondiscreteMeasurementNameCode undef
Business::EDI::CodeList::NumberOfPaddingBytes undef
Business::EDI::CodeList::NumberOfSecuritySegments undef
Business::EDI::CodeList::NumberOfSegmentsBeforeObject undef
Business::EDI::CodeList::NumberOfSegmentsInAMessage undef
Business::EDI::CodeList::ObjectIdentificationCodeQualifier undef
Business::EDI::CodeList::ObjectTypeAttribute undef
Business::EDI::CodeList::ObjectTypeAttributeIdentification undef
Business::EDI::CodeList::ObjectTypeCodeQualifier undef
Business::EDI::CodeList::ObjectTypeQualifier undef
Business::EDI::CodeList::OrganisationClassificationCode undef
Business::EDI::CodeList::OrganisationalClassNameCode undef
Business::EDI::CodeList::OriginalCharacterSetEncodingCoded undef
Business::EDI::CodeList::OriginatorTypeCode undef
Business::EDI::CodeList::PackageReferenceNumber undef
Business::EDI::CodeList::PackagingDangerLevelCode undef
Business::EDI::CodeList::PackagingLevelCode undef
Business::EDI::CodeList::PackagingRelatedDescriptionCode undef
Business::EDI::CodeList::PackagingTermsAndConditionsCode undef
Business::EDI::CodeList::PaddingMechanismCodeListIdentifier undef
Business::EDI::CodeList::PaddingMechanismCoded undef
Business::EDI::CodeList::PartyFunctionCodeQualifier undef
Business::EDI::CodeList::PartyNameFormatCode undef
Business::EDI::CodeList::PayerResponsibilityLevelCode undef
Business::EDI::CodeList::PaymentArrangementCode undef
Business::EDI::CodeList::PaymentChannelCode undef
Business::EDI::CodeList::PaymentConditionsCode undef
Business::EDI::CodeList::PaymentGuaranteeMeansCode undef
Business::EDI::CodeList::PaymentMeansCode undef
Business::EDI::CodeList::PaymentTermsDescriptionIdentifier undef
Business::EDI::CodeList::PaymentTermsTypeCodeQualifier undef
Business::EDI::CodeList::PercentageBasisIdentificationCode undef
Business::EDI::CodeList::PercentageTypeCodeQualifier undef
Business::EDI::CodeList::PeriodTypeCode undef
Business::EDI::CodeList::PeriodTypeCodeQualifier undef
Business::EDI::CodeList::PersonCharacteristicCodeQualifier undef
Business::EDI::CodeList::PhysicalOrLogicalStateDescriptionCode undef
Business::EDI::CodeList::PhysicalOrLogicalStateTypeCodeQualifier undef
Business::EDI::CodeList::PriceCodeQualifier undef
Business::EDI::CodeList::PriceMultiplierTypeCodeQualifier undef
Business::EDI::CodeList::PriceSpecificationCode undef
Business::EDI::CodeList::PriceTypeCode undef
Business::EDI::CodeList::PriorityDescriptionCode undef
Business::EDI::CodeList::PriorityTypeCodeQualifier undef
Business::EDI::CodeList::ProcessStageCodeQualifier undef
Business::EDI::CodeList::ProcessTypeDescriptionCode undef
Business::EDI::CodeList::ProcessingIndicatorDescriptionCode undef
Business::EDI::CodeList::ProcessingInformationCodeQualifier undef
Business::EDI::CodeList::ProcessingPriorityCode undef
Business::EDI::CodeList::ProductCharacteristicIdentificationCode undef
Business::EDI::CodeList::ProductDetailsTypeCodeQualifier undef
Business::EDI::CodeList::ProductGroupTypeCode undef
Business::EDI::CodeList::ProductIdentifierCodeQualifier undef
Business::EDI::CodeList::ProvisoCodeQualifier undef
Business::EDI::CodeList::QualificationApplicationAreaCode undef
Business::EDI::CodeList::QualificationTypeCodeQualifier undef
Business::EDI::CodeList::QuantityTypeCodeQualifier undef
Business::EDI::CodeList::RangeTypeCodeQualifier undef
Business::EDI::CodeList::RateOrTariffClassDescriptionCode undef
Business::EDI::CodeList::RatePlanCode undef
Business::EDI::CodeList::RateTypeCodeQualifier undef
Business::EDI::CodeList::RecipientReferencepassword undef
Business::EDI::CodeList::RecipientReferencepasswordQualifier undef
Business::EDI::CodeList::ReferenceCodeQualifier undef
Business::EDI::CodeList::ReferenceIdentificationNumber undef
Business::EDI::CodeList::ReferenceQualifier undef
Business::EDI::CodeList::RelatedCauseCode undef
Business::EDI::CodeList::RelationshipDescriptionCode undef
Business::EDI::CodeList::RelationshipTypeCodeQualifier undef
Business::EDI::CodeList::RemunerationTypeNameCode undef
Business::EDI::CodeList::ReportFunctionCoded undef
Business::EDI::CodeList::ReportReasonCoded undef
Business::EDI::CodeList::RequestedInformationDescriptionCode undef
Business::EDI::CodeList::RequirementDesignatorCode undef
Business::EDI::CodeList::RequirementOrConditionDescriptionIdentifier undef
Business::EDI::CodeList::ReservationIdentifierCodeQualifier undef
Business::EDI::CodeList::ResponderControlReference undef
Business::EDI::CodeList::ResponseTypeCode undef
Business::EDI::CodeList::ResponseTypeCoded undef
Business::EDI::CodeList::ResultNormalcyCode undef
Business::EDI::CodeList::ResultRepresentationCode undef
Business::EDI::CodeList::ResultValueTypeCodeQualifier undef
Business::EDI::CodeList::ReturnablePackageFreightPaymentResponsibilityCode undef
Business::EDI::CodeList::ReturnablePackageLoadContentsCode undef
Business::EDI::CodeList::RevocationReasonCoded undef
Business::EDI::CodeList::RoleOfSecurityProviderCoded undef
Business::EDI::CodeList::SampleDirectionCode undef
Business::EDI::CodeList::SampleLocationDescriptionCode undef
Business::EDI::CodeList::SampleProcessStepCode undef
Business::EDI::CodeList::SampleSelectionMethodCode undef
Business::EDI::CodeList::SampleStateCode undef
Business::EDI::CodeList::ScenarioIdentification undef
Business::EDI::CodeList::ScenarioReleaseNumber undef
Business::EDI::CodeList::ScenarioVersionNumber undef
Business::EDI::CodeList::ScopeOfSecurityApplicationCoded undef
Business::EDI::CodeList::SealConditionCode undef
Business::EDI::CodeList::SealTypeCode undef
Business::EDI::CodeList::SealingPartyNameCode undef
Business::EDI::CodeList::SectionIdentification undef
Business::EDI::CodeList::SectorAreaIdentificationCodeQualifier undef
Business::EDI::CodeList::SecurityErrorCoded undef
Business::EDI::CodeList::SecurityPartyCodeListQualifier undef
Business::EDI::CodeList::SecurityPartyCodeListResponsibleAgencyCoded undef
Business::EDI::CodeList::SecurityPartyIdentification undef
Business::EDI::CodeList::SecurityPartyName undef
Business::EDI::CodeList::SecurityPartyQualifier undef
Business::EDI::CodeList::SecurityReferenceNumber undef
Business::EDI::CodeList::SecuritySegmentPosition undef
Business::EDI::CodeList::SecuritySequenceNumber undef
Business::EDI::CodeList::SecurityServiceCoded undef
Business::EDI::CodeList::SecurityStatusCoded undef
Business::EDI::CodeList::SegmentPositionInMessageBody undef
Business::EDI::CodeList::SenderSequenceNumber undef
Business::EDI::CodeList::SequenceIdentifierSourceCode undef
Business::EDI::CodeList::SequenceOfTransfers undef
Business::EDI::CodeList::ServiceBasisCodeQualifier undef
Business::EDI::CodeList::ServiceCharacterForSignature undef
Business::EDI::CodeList::ServiceCharacterForSignatureQualifier undef
Business::EDI::CodeList::ServiceCodeListDirectoryVersionNumber undef
Business::EDI::CodeList::ServiceRequirementCode undef
Business::EDI::CodeList::ServiceSegmentTagCoded undef
Business::EDI::CodeList::ServiceTypeCode undef
Business::EDI::CodeList::SetTypeCodeQualifier undef
Business::EDI::CodeList::SettlementMeansCode undef
Business::EDI::CodeList::SimpleDataElementCharacterRepresentationCode undef
Business::EDI::CodeList::SizeTypeCodeQualifier undef
Business::EDI::CodeList::SpecialConditionCode undef
Business::EDI::CodeList::SpecialServiceDescriptionCode undef
Business::EDI::CodeList::StatisticTypeCodeQualifier undef
Business::EDI::CodeList::Status undef
Business::EDI::CodeList::StatusCategoryCode undef
Business::EDI::CodeList::StatusCoded undef
Business::EDI::CodeList::StatusDescriptionCode undef
Business::EDI::CodeList::StatusReasonDescriptionCode undef
Business::EDI::CodeList::StructureComponentFunctionCodeQualifier undef
Business::EDI::CodeList::StructureTypeCode undef
Business::EDI::CodeList::SublineIndicatorCode undef
Business::EDI::CodeList::SublineItemPriceChangeOperationCode undef
Business::EDI::CodeList::SubstitutionConditionCode undef
Business::EDI::CodeList::SupportingEvidenceTypeCodeQualifier undef
Business::EDI::CodeList::SurfaceOrLayerCode undef
Business::EDI::CodeList::SyntaxErrorCoded undef
Business::EDI::CodeList::SyntaxIdentifier undef
Business::EDI::CodeList::SyntaxReleaseNumber undef
Business::EDI::CodeList::SyntaxVersionNumber undef
Business::EDI::CodeList::TaxOrDutyOrFeePaymentDueDateCode undef
Business::EDI::CodeList::TemperatureTypeCodeQualifier undef
Business::EDI::CodeList::TermsTimeRelationCode undef
Business::EDI::CodeList::TestAdministrationMethodCode undef
Business::EDI::CodeList::TestIndicator undef
Business::EDI::CodeList::TestMediumCode undef
Business::EDI::CodeList::TextSubjectCodeQualifier undef
Business::EDI::CodeList::Time undef
Business::EDI::CodeList::TimeOffset undef
Business::EDI::CodeList::TradeClassCode undef
Business::EDI::CodeList::TrafficRestrictionCode undef
Business::EDI::CodeList::TrafficRestrictionTypeCodeQualifier undef
Business::EDI::CodeList::TransactionControlReference undef
Business::EDI::CodeList::TransferPositionCoded undef
Business::EDI::CodeList::TransitDirectionIndicatorCode undef
Business::EDI::CodeList::TransportChargesPaymentMethodCode undef
Business::EDI::CodeList::TransportMeansOwnershipIndicatorCode undef
Business::EDI::CodeList::TransportMovementCode undef
Business::EDI::CodeList::TransportServicePriorityCode undef
Business::EDI::CodeList::TransportStageCodeQualifier undef
Business::EDI::CodeList::UnitTypeCodeQualifier undef
Business::EDI::CodeList::UseOfAlgorithmCoded undef
Business::EDI::CodeList::UserAuthorisationLevel undef
Business::EDI::CodeList::ValidationCriteriaCode undef
Business::EDI::CodeList::ValidationValue undef
Business::EDI::CodeList::ValidationValueQualifier undef
Business::EDI::CodeList::ValueDefinitionCodeQualifier undef
Business::EDI::CodeList::ValueListTypeCode undef
Business::EDI::Composite undef
Business::EDI::DataElement 0.01
Business::EDI::Generator 0.01
Business::EDI::Object 0.01
Business::EDI::Segment 0.01
Business::EDI::Segment::BGM 0.02
Business::EDI::Segment::RFF 0.02
Business::EDI::Spec 0.02
Business::EDI::Test 0.02
Business::FraudDetect 0.01
Business::OnlinePayment::HTTPS 0.10
Business::FraudDetect::preCharge 0.02
Business::ID::KTP 0.03
Business::OnlinePayment
Business::OnlinePayment::Iridium::Action undef
Business::OnlinePayment::Iridium::Action::CardDetailsTransaction undef
Business::OnlinePayment::Iridium::Action::CrossReferenceTransaction undef
Business::OnlinePayment::Iridium::Action::GetCardType undef
Business::OnlinePayment::Iridium::Action::GetGatewayEntryPoints undef
Business::OnlinePayment::Iridium::Action::ThreeDSecureAuthentication undef
Log::Scrubber
Business::OnlinePayment::Litle::ErrorCodes 0.01
Business::OnlinePayment::Litle::UpdaterResponse undef
Business::Shipping::Config 400
Business::Shipping::Logging 400
Business::Shipping::Package 400
Business::Shipping::RateRequest 400
Business::Shipping::RateRequest::Offline 400
Business::Shipping::RateRequest::Online 400
Business::Shipping::Shipment 400
Business::Shipping::Shipment::UPS 400
Business::Shipping::Tracking 400
Business::Shipping::UPS_Offline::Package 400
Business::Shipping::UPS_Offline::RateRequest 400
Business::Shipping::UPS_Offline::Shipment 400
Business::Shipping::UPS_Online::Package 400
Business::Shipping::UPS_Online::RateRequest 400
Business::Shipping::UPS_Online::Shipment 400
Business::Shipping::UPS_Online::Tracking 400
Business::Shipping::USPS_Online::Package 400
Business::Shipping::USPS_Online::RateRequest 400
Business::Shipping::USPS_Online::Shipment 400
Business::Shipping::USPS_Online::Tracking 400
Business::Shipping::Util 400
Business::TNTPost::NL::Data 0.11
Business::UPS::Tracking::Commandline undef
Business::UPS::Tracking::Element::Activity undef
Business::UPS::Tracking::Element::Address undef
Business::UPS::Tracking::Element::Code undef
Business::UPS::Tracking::Element::Package undef
Business::UPS::Tracking::Element::ReferenceNumber undef
Business::UPS::Tracking::Element::Weight undef
Business::UPS::Tracking::Exception undef
Business::UPS::Tracking::Meta::Attribute::Trait::Serializable undef
Business::UPS::Tracking::Request undef
Business::UPS::Tracking::Response undef
Business::UPS::Tracking::Role::Base undef
Business::UPS::Tracking::Role::Builder undef
Business::UPS::Tracking::Role::Serialize undef
Business::UPS::Tracking::Shipment undef
Business::UPS::Tracking::Shipment::Freight undef
Business::UPS::Tracking::Shipment::SmallPackage undef
Business::UPS::Tracking::Utils undef
Net::SSL
Business::WebMoney::Exchanger 0.02
Opcodes
ByteLoader 0.07
Text::PDF
CAM::PDF::Content 1.53
CAM::PDF::Decrypt 1.53
CAM::PDF::GS 1.53
CAM::PDF::GS::NoText 1.53
CAM::PDF::Node 1.53
CAM::PDF::PageText 1.53
CAM::PDF::Renderer::Dump 1.53
CAM::PDF::Renderer::Images 1.53
CAM::PDF::Renderer::Text 1.53
CAM::PDF::Renderer::TextFB 1.53
CGI::Apache 1.01
CGI::Application::Bouquet::Rose::Config 1.05
CGI::Application::Dispatch::Regexp 2.13
CGI::Application::Mailform undef
CGI::Application::Plugin::JSON
Test::Image::GD
CGI::Application::Plugin::AJAXUpload 0.0.3
Test::Regression
CGI::Application::Plugin::Session
Graphics::ColorNames
Graphics::ColorNames::WWW
Graphics::ColorNames::HTML
Color::Calc
CGI::Application::Plugin::Authentication::Display undef
CGI::Application::Plugin::Authentication::Display::Basic undef
CGI::Application::Plugin::Authentication::Display::Classic undef
CGI::Application::Plugin::Authentication::Driver undef
Export::Attrs
CGI::Easy::Headers 1.0.0
CGI::Application::Plugin::Authentication::Driver::Authen::Simple undef
CGI::Application::Plugin::Authentication::Driver::DBI undef
CGI::Easy::Util 1.0.0
CGI::Application::Plugin::Authentication::Driver::Dummy undef
CGI::Application::Plugin::Authentication::Driver::Filter::crypt undef
CGI::Application::Plugin::Authentication::Driver::Filter::lc undef
CGI::Application::Plugin::Authentication::Driver::Filter::md5 undef
CGI::Application::Plugin::Authentication::Driver::Filter::sha1 undef
CGI::Application::Plugin::Authentication::Driver::Filter::strip undef
CGI::Application::Plugin::Authentication::Driver::Filter::uc undef
CGI::Application::Plugin::Authentication::Driver::Generic undef
CGI::Application::Plugin::Authentication::Driver::HTPasswd undef
CGI::Application::Plugin::Authentication::Store undef
CGI::Application::Plugin::Authentication::Store::Cookie undef
CGI::Application::Plugin::Authentication::Store::Session undef
CGI::Application::Plugin::AutoRunmode::FileDelegate 0.13
CGI::Application::Plugin::DevPopup::HTTPHeaders 1.06
CGI::Application::Plugin::DevPopup::Log 1.06
Class::Base
IPC::Capture
CGI::Application::Plugin::DevPopup::Timing 1.06
CGI::Application::Dispatch
CGI::Application::Util::Diff::Actions 1.03
CGI::Application::Util::Diff::Config 1.03
CGI::Application::Util::Logger 1.03
CGI::Auth::FOAF_SSL::Agent 1.001
CGI::Capture::TieSTDIN undef
CGI::Carp 3.51
CGI::Easy::Request 1.0.0
CGI::Easy::Session 1.0.0
CGI::FormBuilder::Field 3.0501
CGI::FormBuilder::Field::button 3.0501
CGI::FormBuilder::Field::checkbox 3.0501
CGI::FormBuilder::Field::file 3.0501
CGI::FormBuilder::Field::hidden 3.0501
CGI::FormBuilder::Field::image 3.0501
CGI::FormBuilder::Field::password 3.0501
CGI::FormBuilder::Field::radio 3.0501
CGI::FormBuilder::Field::select 3.0501
CGI::FormBuilder::Field::static 3.0501
CGI::FormBuilder::Field::submit 3.0501
CGI::FormBuilder::Field::text 3.0501
CGI::FormBuilder::Field::textarea 3.0501
CGI::FormBuilder::Messages 3.0501
CGI::FormBuilder::Multi 3.0501
CGI::FormBuilder::Source 3.0501
CGI::FormBuilder::Source::File 3.0501
CGI::FormBuilder::Template 3.0501
CGI::FormBuilder::Template::Builtin 3.0501
CGI::FormBuilder::Template::Div 3.0501
CGI::FormBuilder::Template::Fast 3.0501
CGI::FormBuilder::Template::HTML 3.0501
CGI::FormBuilder::Template::TT2 3.0501
CGI::FormBuilder::Template::Text 3.0501
CGI::FormBuilder::Test 3.0501
CGI::FormBuilder::Util 3.0501
CGI::Parse::PSGI undef
CGI::Pretty 3.46
Class::Prototyped
CGI::Prototype::Hidden undef
CGI::Push 1.05
CGI::Session::Driver::DBI 4.43
CGI::Session::Driver 4.43
CGI::Session::Driver::db_file 4.43
CGI::Session::Driver::file 4.43
CGI::Session::Driver::mysql 4.43
CGI::Session::Driver::postgresql 4.43
CGI::Session::ErrorHandler 4.43
CGI::Session::Driver::sqlite 4.43
CGI::Session::ID::incr 4.43
CGI::Session::ID::md5 4.43
CGI::Session::ID::static 4.43
CGI::Session::Serialize::default 4.43
CGI::Session::Serialize::freezethaw 4.43
CGI::Session::Serialize::storable 4.43
CGI::Session::Test::Default 4.43
CGI::Session::Tutorial 4.43
CGI::Simple::Standard 1.113
CGI::Simple::Util 1.113
CGI::Switch 1.01
CGI::Untaint::hex undef
CGI::Untaint::integer undef
CGI::Untaint::object undef
CGI::Untaint::printable undef
Test::DatabaseRow
CGI::Uploader::Transform::ImageMagick 2.18
CGI::Util 3.53
CGI::WebToolkit::Tutorial undef
CHI::CacheObject 0.46
CHI::Constants 0.46
CHI::Driver 0.46
CHI::Driver::Base::CacheContainer 0.46
CHI::Driver::CacheCache 0.46
CHI::Driver::FastMmap 0.46
CHI::Driver::File 0.46
Mojolicious::Plugin::RenderSteps
Memcached::libmemcached
CHI::Driver::Memcached::Base 0.13
CHI::Driver::Memcached::Fast 0.13
CHI::Driver::Memcached::Test::Driver 0.13
CHI::Driver::Memcached::Test::Driver::Base 0.13
CHI::Driver::Memcached::Test::Driver::Fast 0.13
CHI::Driver::Memcached::Test::Driver::Memcached 0.13
CHI::Driver::Memcached::Test::Driver::libmemcached 0.13
CHI::Driver::Memcached::libmemcached 0.13
CHI::Driver::Memcached::t::CHIDriverTests::Base 0.13
CHI::Driver::Memcached::t::CHIDriverTests::Fast 0.13
CHI::Driver::Memcached::t::CHIDriverTests::Memcached 0.13
CHI::Driver::Memcached::t::CHIDriverTests::libmemcached 0.13
CHI::Driver::Memory 0.46
CHI::Driver::Metacache 0.46
CHI::Driver::Null 0.46
CHI::Driver::RawMemory 0.46
CHI::Driver::Role::HasSubcaches undef
CHI::Driver::Role::IsSizeAware undef
CHI::Driver::Role::IsSubcache undef
CHI::Driver::Role::Universal 0.46
CHI::Serializer::JSON undef
CHI::Serializer::Storable undef
CHI::Stats 0.46
CHI::Test undef
CHI::Test::Class undef
CHI::Test::Driver::NonMoose undef
CHI::Test::Driver::Readonly undef
CHI::Test::Driver::Role::CheckKeyValidity undef
CHI::Test::Driver::Writeonly undef
CHI::Test::Util undef
CHI::Types undef
CHI::Util undef
CHI::t::Bugs undef
CHI::t::Constants undef
CHI::t::Driver undef
CHI::t::Driver::CacheCache undef
CHI::t::Driver::FastMmap undef
CHI::t::Driver::File undef
CHI::t::Driver::File::DepthZero undef
CHI::t::Driver::Memory undef
CHI::t::Driver::NonMoose undef
CHI::t::Driver::Subcache undef
CHI::t::Driver::Subcache::l1_cache undef
CHI::t::Driver::Subcache::mirror_cache undef
CHI::t::GetError undef
CHI::t::Initialize undef
CHI::t::Null undef
CHI::t::RequiredModules undef
CHI::t::Sanity undef
CHI::t::SetError undef
CHI::t::Subcache undef
CHI::t::Subclass undef
CHI::t::Util undef
Log::Dump
Test::Classy
CLI::Dispatch::Command undef
CLI::Dispatch::Help undef
CLI::Framework::Application 0.04
CLI::Framework::Command 0.04
CLI::Framework::Command::Alias 0.01
CLI::Framework::Command::Console 0.01
CLI::Framework::Command::Dump 0.01
CLI::Framework::Command::Help 0.01
CLI::Framework::Command::List 0.01
CLI::Framework::Command::Menu 0.01
CLI::Framework::Command::Meta 0.01
CLI::Framework::Command::Tree 0.01
CLI::Framework::Exceptions 0.02
CNC::Cog::Gcode 0.061
CNC::Cog::Gdcode 0.061
CPAN::Author 5.5001
CPAN::Bundle 5.5
CPAN::CacheMgr 5.5001
CPAN::Changes::Release undef
CPAN::Complete 5.5
CPAN::Debug 5.5001
CPAN::DeferredCode 5.50
CPAN::Distribution 1.9602
CPAN::Distroprefs 6
GD::Text 0.86
CPAN::Distrostatus 5.5
GD::Graph::lines 1.15
CPAN::Exception::RecursiveDependency 5.5
CPAN::Exception::blocked_urllist 1.001
CPAN::Exception::yaml_not_installed 5.5
CPAN::FTP 5.5005
CPAN::FTP::netrc 1.01
CPAN::FindDependencies::Dependency 2.1
CPAN::FindDependencies::MakeMaker 0.3
CPAN::FirstTime 5.5303
CPAN::HTTP::Client 1.9600
CPAN::HTTP::Credentials 1.9600
CPAN::HandleConfig 5.5003
Regexp::SQL::LIKE
Metabase::Test::Archive
CPAN::Index 1.9600
CPAN::InfoObj 5.5
CPAN::Kwalify 5.50
CPAN::LWP::UserAgent 1.9600
CPAN::Meta::Converter 2.110930
CPAN::Meta::Feature 2.110930
CPAN::Meta::History 2.110930
CPAN::Meta::Prereqs 2.110930
CPAN::Meta::Spec 2.110930
CPAN::Meta::Validator 2.110930
CPAN::Mini::Inject::Config 0.27_03
CPAN::Mini::Webserver::Index undef
CPAN::Mini::Webserver::Templates undef
CPAN::Mini::Webserver::Templates::CSS undef
CPAN::Mini::Webserver::Templates::Images undef
CPAN::Mirrors 1.9600
CPAN::Module 5.5001
CPAN::Nox 5.50
CPAN::PackageDetails::Entries 0.25_05
CPAN::PackageDetails::Entry 0.25_05
CPAN::PackageDetails::Header 0.25_05
CPAN::Prompt 5.5
CPAN::Queue 5.5001
CPAN::Recent::Uploads::Retriever 0.02
CPAN::Reporter::API 1.1902
CPAN::Reporter::Config 1.1902
CPAN::Reporter::FAQ 1.1902
CPAN::Reporter::History 1.1902
CPAN::Reporter::PrereqCheck 1.1902
XML::Fast
SimpleDB::Client
Net::Amazon::Config
Metabase::Archive
Metabase::Index
Metabase::Test::Index
Metabase::Archive::S3
CPAN::SQLite::DBI 0.199
CPAN::SQLite::DBI::Index 0.199
CPAN::SQLite::Index 0.199
CPAN::SQLite::Info 0.199
CPAN::SQLite::META 0.199
CPAN::SQLite::Populate 0.199
CPAN::SQLite::Search 0.199
CPAN::SQLite::State 0.199
Metabase::Index::SimpleDB
CPAN::SQLite::Util 0.199
CPAN::Shell 5.5002
CPAN::Tarzip 5.5011
CPAN::Testers::Fact::InstalledModules 1.999001
CPAN::Testers::Fact::LegacyReport 1.999001
CPAN::Testers::Fact::PerlConfig 1.999001
CPAN::Testers::Fact::Prereqs 1.999001
CPAN::Testers::Fact::TestEnvironment 1.999001
CPAN::Testers::Fact::TestOutput 1.999001
CPAN::Testers::Fact::TestSummary 1.999001
CPAN::Testers::Fact::TesterComment 1.999001
DBIx::RunSQL
Metabase::Archive::SQLite
Metabase::Gateway
Metabase::Librarian
CPAN::Testers::Metabase::AWS 1.999001
Metabase::Index::FlatFile
CPAN::Testers::Metabase::Demo 1.999001
GD::Graph::Data 1.22
GD::Graph::Error 1.8
GD::Graph::area 1.17
GD::Graph::axestype 1.45
GD::Graph::bars 1.26
GD::Graph::colour 1.10
GD::Graph::hbars 1.3
GD::Graph::linespoints 1.8
GD::Graph::mixed 1.13
GD::Graph::pie 1.21
GD::Graph::points 1.13
GD::Graph::utils 1.7
CPAN::Testers::Common::DBUtils
CPAN::Testers::WWW::Statistics::Graphs 0.88
CPAN::Testers::WWW::Statistics::Pages 0.88
CPAN::URL 5.5
CPAN::Version 5.5001
CPAN::YACSmoke::Plugin::Recent 0.02
ORLite
ORLite::Mirror
CPAN::YACSmoke::Plugin::SmokeDB 0.01
CPANDB::Distribution 0.14
Package::Constants
Object::Accessor
CPANPLUS::Backend undef
CPANPLUS::Backend::RV undef
CPANPLUS::Config undef
CPANPLUS::Dist::Base undef
CPANPLUS::Dist::Build
CPANPLUS::Config::YACSmoke 0.62
CPANPLUS::Configure undef
CPANPLUS::Configure::Setup undef
CPANPLUS::Dist undef
CPANPLUS::Dist::Autobundle undef
CPANPLUS::Dist::Build::Constants 0.56
CPANPLUS::Dist::Gentoo::Atom 0.11
CPANPLUS::Dist::Gentoo::Guard 0.11
CPANPLUS::Error undef
CPANPLUS::Dist::Gentoo::Maps 0.11
CPANPLUS::Dist::Gentoo::Version 0.11
CPANPLUS::Dist::MM undef
CPANPLUS::Dist::Sample undef
CPANPLUS::Dist::YACSmoke 0.62
CPANPLUS::Internals 0.9010
CPANPLUS::Internals::Constants undef
CPANPLUS::Internals::Constants::Report undef
CPANPLUS::Internals::Extract undef
CPANPLUS::Internals::Fetch undef
CPANPLUS::Internals::Report undef
CPANPLUS::Internals::Search undef
CPANPLUS::Internals::Source undef
CPANPLUS::Internals::Source::CPANIDX::HTTP 0.04
CPANPLUS::Internals::Source::CPANIDX::Tie 0.04
CPANPLUS::Internals::Source::CPANMetaDB::HTTP 0.04
CPANPLUS::Internals::Source::CPANMetaDB::Tie 0.04
CPANPLUS::Internals::Source::Memory undef
CPANPLUS::Internals::Source::SQLite undef
CPANPLUS::Internals::Source::SQLite::Tie undef
CPANPLUS::Internals::Utils undef
CPANPLUS::Internals::Utils::Autoflush undef
CPANPLUS::Module undef
CPANPLUS::Module::Author undef
CPANPLUS::Module::Author::Fake undef
CPANPLUS::Module::Checksums undef
CPANPLUS::Module::Fake undef
CPANPLUS::Module::Signature undef
CPANPLUS::Selfupdate undef
CPANPLUS::Shell undef
CPANPLUS::Shell::Classic 0.0562
CPANPLUS::Shell::Default 0.9010
CPANPLUS::Shell::Default::Plugins::CustomSource undef
CPANPLUS::Shell::Default::Plugins::Remote undef
CPANPLUS::Shell::Default::Plugins::Source undef
CPANPLUS::YACSmoke::IniFiles 0.62
CPANPLUS::YACSmoke::ReAssemble 0.62
CPANPLUS::YACSmoke::SortVers 0.62
CPS::Functional 0.11
CPS::Governor 0.11
CPS::Governor::Deferred 0.11
CPS::Governor::Simple 0.11
CPU::Emulator::Memory::Banked 1.1002
Iterator::Simple
Iterator::Simple::Lookahead
Asm::Preproc
Iterator::Array::Jagged
CPU::Z80::Assembler::AsmTable 2.12
Asm::Z80::Table
CPU::Z80::Assembler::Expr 2.13
CPU::Z80::Assembler::JumpOpcode 2.13
CPU::Z80::Assembler::Lexer 2.12
CPU::Z80::Assembler::List 2.13
CPU::Z80::Assembler::Macro 2.13
CPU::Z80::Assembler::Opcode 2.13
CPU::Z80::Assembler::Parser 2.13
CPU::Z80::Assembler::Preprocessor 2.12
CPU::Z80::Assembler::Program 2.13
CPU::Z80::Assembler::Segment 2.13
CPU::Z80::Assembler
CPU::Z80::Disassembler::Format 0.04
CPU::Z80::Disassembler::Instruction 0.04
CPU::Z80::Disassembler::Label 0.04
CPU::Z80::Disassembler::Labels 0.04
CSS::DOM::Constants 0.14
CPU::Z80::Disassembler::Memory 0.04
CSS::DOM::Interface 0.14
CSS::Adaptor 1.01
CSS::Adaptor::Debug 1.01
CSS::Adaptor::Pretty 1.01
CSS::DOM::Array 0.14
CSS::DOM::Exception 0.14
CSS::DOM::MediaList 0.14
CSS::DOM::Parser 0.14
MouseX::ConfigFromFile
MouseX::SimpleConfig
MouseX::Getopt
MouseX::Types::Path::Class
CSS::DOM::PropertyParser 0.14
CSS::DOM::Rule 0.14
CSS::DOM::Rule::Charset 0.14
CSS::DOM::Rule::FontFace 0.14
CSS::DOM::Style 0.14
CSS::DOM::StyleSheetList 0.14
CSS::DOM::Util 0.14
CSS::DOM::Rule::Import 0.14
CSS::DOM::Rule::Media 0.14
CSS::DOM::Rule::Page 0.14
CSS::DOM::Rule::Style 0.14
CSS::DOM::RuleList 0.14
CSS::DOM::Value 0.14
Encoding::FixLatin
Encoding::FixLatin::XS
CSS::DOM::Value::List 0.14
CSS::DOM::Value::Primitive 0.14
warnings::illegalproto
CSS::Declare undef
CSS::Parse 1.01
CSS::Parse::Compiled 1.01
CSS::Parse::CompiledGrammar undef
CSS::Parse::Heavy 1.01
CSS::Parse::Lite 1.02
CSS::Parse::PRDGrammar 1.01
CSS::Prepare v0.9.2.1
CSS::Prepare::CSSGrammar undef
CSS::Prepare::Plugin::BorderRadius undef
CSS::Prepare::Plugin::Contain undef
CSS::Prepare::Plugin::Opacity undef
CSS::Prepare::Plugin::TestPlugins undef
CSS::Prepare::Property::Background undef
CSS::Prepare::Property::Border undef
CSS::Prepare::Property::BorderRadius undef
CSS::Prepare::Property::Color undef
CSS::Prepare::Property::Effects undef
CSS::Prepare::Property::Expansions undef
CSS::Prepare::Property::Font undef
CSS::Prepare::Property::Formatting undef
CSS::Prepare::Property::Generated undef
CSS::Prepare::Property::Hacks undef
CSS::Prepare::Property::Margin undef
CSS::Prepare::Property::Padding undef
CSS::Prepare::Property::Tables undef
CSS::Prepare::Property::Text undef
CSS::Prepare::Property::UI undef
CSS::Prepare::Property::Values undef
CSS::Prepare::Property::Vendor undef
CSS::Property 1.02
CSS::Selector 1.01
CSS::Style 1.02
CSS::Value 1.03
Chart::Plot
Chart::Plot::Tagged
Chart::Plot::Canvas
CVS::Metrics::Parser 0.18
CVS::Metrics::TaggedChart 0.18
Cache::BaseCacheTester undef
Cache::CacheFactory::Expiry 1.10
Catalyst::Helper 1.28
Cache::CacheFactory::Expiry::Base 1.10
Cache::CacheFactory::Expiry::LastModified 1.10
Cache::CacheFactory::Expiry::Size 1.10
Cache::CacheFactory::Expiry::Time 1.10
Cache::CacheFactory::Object 1.10
Cache::CacheFactory::Storage 1.10
Cache::CacheMetaData undef
Cache::CacheSizer undef
Cache::CacheTester undef
Cache::CacheUtils undef
Cache::Entry 2.04
Cache::FastMemoryBackend undef
Cache::FastMmap::CImpl 1.36
Cache::File 2.04
Cache::File::Entry 2.04
Cache::File::Heap 2.04
Cache::FileBackend undef
Cache::KyotoTycoon::Cursor undef
Cache::Memcached::GetParser undef
Cache::Memory 2.04
Cache::Memory::Entry 2.04
Cache::MemoryBackend undef
Cache::MemoryCache undef
Cache::Null 2.04
Cache::Null::Entry 2.04
Cache::NullCache undef
Cache::Ref::CAR 0.05
Cache::Ref::CAR::Base 0.05
Cache::Ref::CLOCK::Base 0.05
Cache::Ref::FIFO 0.05
Cache::Ref::LRU 0.05
Cache::Ref::GCLOCK 0.05
Cache::Ref::LIFO 0.05
Cache::Ref::Null 0.05
Cache::Ref::Random 0.05
Cache::Ref::Role::API 0.05
Cache::Ref::Role::Index 0.05
Cache::Ref::Role::WithDoublyLinkedList 0.05
Catalyst::Action::Serialize 0.90
Cache::Ref::Util::LRU::API 0.05
Cache::Ref::Util::LRU::Array 0.05
Cache::Ref::Util::LRU::List 0.05
Cache::RemovalStrategy 2.04
Carp 1.16
Cache::SharedMemoryBackend undef
Cache::SharedMemoryCache undef
Cache::SizeAwareCache undef
String::Print
Log::Report::Optional
Log::Report
Taint::Runtime
Cache::SizeAwareCacheTester undef
Cache::SizeAwareFileCache undef
Cache::SizeAwareMemoryCache undef
Cache::SizeAwareSharedMemoryCache undef
Cache::Tester 2.04
IO::Pty::Easy
Carp::Always::Color::HTML 0.04
Carp::Always::Color::Term 0.04
Carp::Source::Always 1.101420
Convert::ASN1
Catalyst::Action::Deserialize 0.90
Catalyst::Action::Deserialize::Data::Serializer 0.90
Catalyst::Action::Deserialize::JSON 0.90
Catalyst::Action::Deserialize::View 0.90
Catalyst::Action::Deserialize::XML::Simple 0.90
Catalyst::Action::Deserialize::YAML 0.90
Catalyst::Action::REST::ForBrowsers 0.90
Catalyst::Action::Role::ACL 0.05
XML::Compile::Tester
XML::Compile
XML::Compile::Cache
XML::Compile::SOAP
Catalyst::Action::SOAP undef
Catalyst::Authentication::User undef
Catalyst::Action::SOAP::DocumentLiteral undef
Catalyst::Action::SOAP::DocumentLiteralWrapped undef
Catalyst::ClassData undef
Catalyst::Component undef
Catalyst::Component::ApplicationAttribute undef
Catalyst::Action::SOAP::HTTPGet undef
Catalyst::Action::SOAP::RPCEncoded undef
Catalyst::Action::SOAP::RPCEndpoint undef
Catalyst::Controller::REST 0.90
Catalyst::Action::SOAP::RPCLiteral undef
Catalyst::Action::Serialize::Data::Serializer 0.90
Catalyst::Action::Serialize::JSON 0.90
Catalyst::Action::Serialize::JSON::XS 0.90
Catalyst::Action::Serialize::JSONP 0.90
Catalyst::Action::Serialize::View 0.90
Catalyst::Action::Serialize::XML::Simple 0.90
Catalyst::Action::Serialize::YAML 0.90
Catalyst::Action::Serialize::YAML::HTML 0.90
Catalyst::Action::SerializeBase 0.90
Catalyst::ActionChain undef
Catalyst::ActionContainer undef
Catalyst::ActionRole::NeedsLogin undef
Catalyst::Authentication::Credential::Remote undef
Catalyst::Authentication::Store::Minimal
Catalyst::Authentication::Credential::RemoteHTTP::UserAgent 0.04
Catalyst::Authentication::Realm undef
Catalyst::Authentication::Realm::Compatibility undef
Catalyst::Authentication::Realm::Progressive undef
Catalyst::Authentication::Realm::SimpleDB undef
Catalyst::Model::DBI
Catalyst::Authentication::Store::DBI::ButMaintained::User undef
Data::Pageset
Catalyst::Authentication::Store::DBIx::Class::User undef
Catalyst::Authentication::Store::Fey::ORM::User 0.001
Socket6
IO::Socket::INET6
Net::LDAP
Net::LDAP::Server
Net::LDAP::SID
Net::LDAP::Server::Test
Catalyst::Authentication::Store::LDAP::Backend 1.012
Catalyst::Authentication::Store::LDAP::User 1.012
KiokuX::Model
Catalyst::Authentication::Store::Model::KiokuDB undef
Catalyst::Authentication::Store::Model::KiokuDB::UserWrapper undef
Catalyst::Authentication::Store::Null undef
Catalyst::Authentication::User::Hash undef
Catalyst::Base undef
Catalyst::Component::ContextClosure undef
Catalyst::Controller::CGIBin 0.030
Catalyst::Controller::Metal undef
Catalyst::Controller::SOAP::DocumentLiteralWrapped undef
Catalyst::Controller::SOAP::RPC undef
Catalyst::DispatchType undef
Catalyst::DispatchType::Chained undef
Catalyst::DispatchType::Default undef
Catalyst::DispatchType::Index undef
Catalyst::DispatchType::Path undef
Catalyst::DispatchType::Regex undef
Catalyst::Dispatcher undef
Catalyst::Engine undef
Catalyst::Engine::Apache2 1.16
Catalyst::Engine::Apache2::MP19 1.16
Catalyst::Engine::Apache2::MP20 1.16
Catalyst::Engine::Apache::MP13 1.16
CGI::Cookie::XS
Cookie::XS
HTTP::HeaderParser::XS
Catalyst::Engine::HTTP::Prefork::Handler undef
Catalyst::Engine::HTTP::Prefork::Restarter undef
Catalyst::Exception::Basic undef
Catalyst::Exception::Detach undef
Catalyst::Exception::Go undef
Catalyst::Exception::Interface undef
Catalyst::Helper::Model::Adaptor undef
JSON::RPC::Legacy::Client
Finance::Bitcoin
Catalyst::Helper::Model::Bitcoin 0.02
CouchDB::Client
Catalyst::Helper::Model::CouchDB 0.01
Catalyst::Helper::Model::DBI 0.28
Catalyst::Helper::Model::DBIC::Schema 0.48
MIME::Base64::URLSafe
Facebook
Catalyst::Helper::Model::Facebook 0.006
Catalyst::Helper::Model::Factory undef
Catalyst::Helper::Model::Factory::PerRequest undef
Catalyst::Helper::Model::LDAP undef
Catalyst::Helper::Model::MongoDB 0.10
Catalyst::Helper::PSGI undef
Catalyst::Helper::View::ByCode 0.13
Catalyst::Helper::View::Clevery 0.002
Catalyst::Helper::View::Email 0.31
Catalyst::Helper::View::Email::Template 0.31
Catalyst::Helper::View::Excel::Template::Plus undef
Catalyst::View
Catalyst::Helper::View::HTML::Mason 0.15
Catalyst::Helper::View::Haml undef
Catalyst::Helper::View::JSON undef
JavaScript::Minifier::XS
Catalyst::Helper::View::JavaScript::Minifier::XS 2.101000
Catalyst::Helper::View::Mason 0.13
Catalyst::Helper::View::Mason2 0.02
Catalyst::Helper::View::PDF::Reuse undef
Catalyst::Helper::View::TT undef
Catalyst::Helper::View::TT::Alloy undef
Catalyst::Helper::View::TTSite undef
Catalyst::Helper::View::Tenjin 0.042
Catalyst::Helper::View::Text::Template undef
Catalyst::Helper::View::Wkhtmltopdf undef
Catalyst::Helper::View::Xslate undef
Catalyst::Log undef
Catalyst::Model::Adaptor::Base undef
Catalyst::Plugin::Session::Store undef
Catalyst::Model::Factory 0.10
Catalyst::Model::Factory::PerRequest 0.10
Catalyst::Model::LDAP::Connection undef
Catalyst::Model::LDAP::Entry undef
Catalyst::Model::LDAP::Search undef
Catalyst::Request undef
Catalyst::Model::REST::Response 0.16
Catalyst::Model::REST::Serializer 0.16
Catalyst::Model::REST::Serializer::JSON undef
Catalyst::Model::REST::Serializer::YAML undef
Catalyst::Model::Role::RunAfterRequest 0.04
Catalyst::Model::XML::Feed::Item undef
HTML::TokeParser::Simple
Catalyst::Plugin::AccessLog::Formatter 1.04
Catalyst::Plugin::Authentication::Credential::Password undef
Catalyst::Plugin::Authentication::Store::Minimal undef
Catalyst::Plugin::Authentication::User undef
Catalyst::Plugin::Authentication::User::Hash undef
Catalyst::Plugin::Bread::Board::Container 0.03
Catalyst::Plugin::Cache::Backend undef
Catalyst::Plugin::Cache::Backend::Memory undef
Catalyst::Plugin::Cache::Choose::KeyRegexes undef
Catalyst::Plugin::Cache::Curried undef
Catalyst::Plugin::Cache::Store::Memory undef
Catalyst::Plugin::Compress::Deflate undef
Catalyst::Plugin::Compress::Gzip undef
FindBin::libs
Catalyst::Plugin::ErrorCatcher::Email 0.0.8.8
Catalyst::Plugin::ErrorCatcher::File 0.0.8.8
Catalyst::Plugin::ErrorCatcher::Plugin::CleanUp::CaughtException 0.0.8.8
Catalyst::Plugin::ErrorCatcher::Plugin::CleanUp::Pg::MissingColumn 0.0.8.8
Locale::Maketext::Lexicon::DBI
Catalyst::Plugin::ErrorCatcher::Plugin::CleanUp::TxnDo 0.0.8.8
Catalyst::Plugin::I18N::DBI 0.2.5
Catalyst::Plugin::Params::Nested::Expander undef
Catalyst::Plugin::Session::State undef
Catalyst::Plugin::Session::Store::DBIC::Delegate undef
Catalyst::Plugin::Session::Test::Store 123
Catalyst::Plugin::Widget::Base undef
Catalyst::Plugin::Widget::ThroughView undef
Catalyst::Plugin::Widget::WithResultSet undef
Catalyst::Request::REST 0.90
Catalyst::Request::REST::ForBrowsers 0.90
Catalyst::Request::Upload undef
Catalyst::Response undef
Catalyst::Restarter undef
Catalyst::Restarter::Forking undef
Catalyst::Restarter::Win32 undef
Catalyst::Script::CGI undef
Catalyst::Script::Create undef
Catalyst::Script::FastCGI undef
Catalyst::Script::Server undef
Catalyst::Script::Test undef
Catalyst::ScriptRole undef
Catalyst::Stats undef
Test::HTML::Form
Catalyst::TraitFor::Model::DBIC::ConfigPerSite undef
Catalyst::TraitFor::Model::DBIC::Schema::Caching undef
Catalyst::TraitFor::Model::DBIC::Schema::Replicated undef
Catalyst::TraitFor::Model::DBIC::Schema::SchemaProxy undef
CatalystX::RoleApplicator
Catalyst::TraitFor::Request::DecodedParams::JSON 0.01
Catalyst::TraitFor::Request::REST 0.90
Catalyst::TraitFor::Request::REST::ForBrowsers 0.90
Catalyst::TraitFor::View::TT::ConfigPerSite undef
Catalyst::View::ByCode::Declare 0.13
MooseX::AttributeHelpers
Catalyst::View::ByCode::Markup::Document 0.11
Catalyst::View::ByCode::Markup::Element 0.11
Catalyst::View::ByCode::Markup::EscapedText 0.11
Catalyst::View::ByCode::Markup::Structured 0.11
Catalyst::View::ByCode::Markup::Tag 0.11
Catalyst::View::ByCode::Renderer 0.13
Catalyst::Plugin::SubRequest
Catalyst::View::Component::SubInclude::ESI 0.07_03
Catalyst::View::Component::SubInclude::HTTP 0.01
Catalyst::View::Component::SubInclude::SSI 0.10
Catalyst::View::Component::SubInclude::SubRequest 0.07_03
Catalyst::View::Component::SubInclude::Visit 0.07_03
Catalyst::View::Email::Template 0.31
Search::QueryParser::SQL
Sort::SQL
Catalyst::Component::ACCEPT_CONTEXT
CatalystX::CRUD::Controller 0.51
CatalystX::CRUD::Iterator 0.51
CatalystX::CRUD::Iterator::File 0.51
CatalystX::CRUD::Model 0.51
CatalystX::CRUD::Model::File 0.51
CatalystX::CRUD::Model::Utils 0.51
CatalystX::CRUD::ModelAdapter undef
CatalystX::CRUD::ModelAdapter::File 0.51
CatalystX::CRUD::Object 0.51
CatalystX::CRUD::Object::File 0.51
Rose::DB::Object
Rose::DBx::Object::MoreHelpers
CatalystX::CRUD
CatalystX::CRUD::Object::RDBO 0.22
CatalystX::CRUD::REST 0.51
CatalystX::CRUD::Results 0.51
CatalystX::CRUD::Test::Controller 0.51
CatalystX::CRUD::Test::Form 0.51
CatalystX::CRUD::Controller::RHTMLO
Catalyst::View::Excel::Template::Plus
CatalystX::CRUD::View::Excel
Template::Plugin::VMethods
Template::Plugin::Handy
Data::Transformer
Search::Query
Search::Tools
CatalystX::CRUD::Controller::REST
DBIx::Class::RDBOHelpers
CatalystX::CRUD::ModelAdapter::DBIC
CatalystX::CRUD::Model::RDBO
CatalystX::CRUD::YUI::Controller 0.025
CatalystX::CRUD::YUI::Excel 0.025
CatalystX::CRUD::YUI::LiveGrid 0.025
CatalystX::CRUD::YUI::Serializer 0.025
CatalystX::CRUD::YUI::TT 0.025
CatalystX::CRUD::YUI::TT::crud 0.025
CatalystX::CRUD::YUI::View 0.025
CatalystX::Controller::SimpleAPI 0.03
CatalystX::Controller::Sugar::ActionPack::Default undef
CatalystX::Controller::Sugar::ActionPack::End undef
CatalystX::Controller::Sugar::ActionPack::Error undef
CatalystX::Controller::Sugar::ActionPack::Merge undef
CatalystX::Controller::Sugar::Plugin undef
CLASS
Data::Pond
CatalystX::Declare::Action::CatchValidationError undef
CatalystX::Declare::Context::AppSetup undef
CatalystX::Declare::Context::StringParsing undef
CatalystX::Declare::Controller::ActionPreparation undef
CatalystX::Declare::Controller::DetermineActionClass undef
CatalystX::Declare::Controller::Meta::TypeConstraintMapping undef
CatalystX::Declare::Controller::QualifyClassNames undef
CatalystX::Declare::DefaultSuperclassing undef
CatalystX::Declare::Dispatching::ChainTypeSensitivity undef
CatalystX::Declare::Keyword::Action undef
CatalystX::Declare::Keyword::Application undef
CatalystX::Declare::Keyword::Component undef
CatalystX::Declare::Keyword::Controller undef
CatalystX::Declare::Keyword::Model undef
CatalystX::Declare::Keyword::Role undef
CatalystX::Declare::Keyword::View undef
CatalystX::Features::Backend 0.20
CatalystX::Features::Feature 0.20
CatalystX::Features::Init 0.20
CatalystX::Features::Lib 0.20
CatalystX::Features::Plugin::ConfigLoader 0.20
CatalystX::Features::Plugin::I18N 0.20
CatalystX::Features::Plugin::Static::Simple 0.20
CatalystX::Features::Role::Backend 0.20
CatalystX::Features::Role::Feature 0.20
CatalystX::Features::View::Mason 0.20
CatalystX::Features::View::TT 0.20
CatalystX::GlobalContext 0.030
CatalystX::NavigationMenu undef
CatalystX::NavigationMenuItem undef
CatalystX::PathContext v0.0.1
CatalystX::Profile::Controller::ControlProfiling 0.01
CatalystX::SimpleLogin::Controller::Login undef
CatalystX::SimpleLogin::Form::Login undef
CatalystX::SimpleLogin::Form::LoginOpenID undef
CatalystX::SimpleLogin::TraitFor::Controller::Login::Logout undef
CatalystX::SimpleLogin::TraitFor::Controller::Login::OpenID undef
CatalystX::SimpleLogin::TraitFor::Controller::Login::RenderAsTTTemplate undef
Test::Deep::Type
MooseX::Storage::Format::JSONpm
MooseX::Storage
Geometry::Primitive
Forest
MooseX::Storage::Deferred
Graphics::Color
Graphics::Primitive::Border undef
Graphics::Primitive::Brush undef
Graphics::Primitive::Canvas undef
Graphics::Primitive::Component undef
CatalystX::SimpleLogin::TraitFor::Controller::Login::WithRedirect undef
Graphics::Primitive::Container undef
Graphics::Primitive::Aligned undef
Graphics::Primitive::Driver undef
Graphics::Primitive::ComponentList undef
Graphics::Primitive::Driver::TextLayout undef
Graphics::Primitive::Font undef
Text::Flow
Graphics::Primitive::Insets undef
Graphics::Primitive::TextBox
Geometry::Primitive::Point
Geometry::Primitive::Rectangle
Graphics::Color::RGB
Graphics::Primitive::Driver::Cairo::TextLayout undef
Graphics::Primitive::Operation::Fill undef
Graphics::Primitive::Operation::Stroke undef
Graphics::Primitive::Oriented undef
Graphics::Primitive::Image undef
Graphics::Primitive::Operation undef
Graphics::Primitive::Paint::Gradient::Linear
Graphics::Primitive::Paint::Gradient::Radial undef
Graphics::Primitive::Paint::Solid undef
Graphics::Primitive::Path undef
Graphics::Primitive::Paint undef
Graphics::Primitive::Paint::Gradient undef
Chart::Bars 2.4.2
Chart::Base 2.4.2
Graphics::Primitive
Layout::Manager::Flow
Color::Scheme
Layout::Manager::Absolute
Layout::Manager::Compass
Layout::Manager::Axis
Layout::Manager::Grid
Layout::Manager::Single
Graphics::Primitive::Driver::Cairo
Geometry::Primitive::Arc
Geometry::Primitive::Circle
Chart::Clicker::Axis undef
Chart::Clicker::Data::DataSet undef
Chart::Clicker::Axis::DateTime undef
Chart::Clicker::Data::Range undef
Chart::Clicker::Data::Series undef
Chart::Clicker::Component undef
Chart::Clicker::Container undef
Chart::Clicker::Context undef
Chart::Clicker::Data::Marker undef
Chart::Clicker::Data::Series::HighLow undef
Chart::Clicker::Data::Series::Size undef
Chart::Clicker::Decoration undef
Chart::Clicker::Decoration::Glass undef
Chart::Clicker::Decoration::Grid undef
Chart::Clicker::Decoration::Legend undef
Chart::Clicker::Drawing::ColorAllocator undef
Chart::Clicker::Decoration::Legend::Tabular undef
Chart::Clicker::Decoration::MarkerOverlay undef
Chart::Clicker::Decoration::OverAxis undef
Chart::Clicker::Decoration::Plot undef
Chart::Clicker::Positioned undef
Chart::Clicker::Renderer undef
Math::VectorReal
Chemistry::File::Formula 0.37
Chart::Clicker::Renderer::Area undef
Chart::Clicker::Renderer::Bar undef
Chart::Clicker::Renderer::Bubble undef
Chart::Clicker::Renderer::CandleStick undef
Chart::Clicker::Renderer::HeatMap undef
Chart::Clicker::Renderer::Line undef
Chart::Clicker::Renderer::Pie undef
Chart::Clicker::Renderer::Point undef
Chart::Clicker::Renderer::PolarArea undef
Chart::Clicker::Renderer::StackedArea undef
Chart::Clicker::Renderer::StackedBar undef
Chart::Composite 2.4.2
Chart::Constants undef
Chart::Direction 2.4.2
Chart::ErrorBars 2.4.2
Chart::Gnuplot::Util undef
Chart::HorizontalBars 2.4.2
Chart::Lines 2.4.2
Chart::LinesPoints 2.4.2
Chart::Mountain 2.4.2
Chart::OFC2::Axis 0.08_02
Chart::OFC2::Bar 0.08_02
Chart::OFC2::BarLineBase 0.08_02
Chart::OFC2::Element 0.08_02
Chart::OFC2::Extremes 0.08_02
Chart::OFC2::HBar 0.08_02
Chart::OFC2::HBarValues 0.08_02
Chart::OFC2::Labels 0.08_02
Chart::OFC2::Line 0.08_02
Chart::OFC2::Pie 0.08_02
Chart::OFC2::PieValues 0.08_02
Chart::OFC2::Scatter 0.08_02
Chart::OFC2::Title 0.08_02
Chart::OFC2::ToolTip 0.08_02
Chart::OFC2::Types undef
Chart::Pareto 2.4.2
Chart::Pie 2.4.2
Chart::Points 2.4.2
Chart::Split 2.4.2
Chart::StackedBars 2.4.2
Number::Format
Chart::Clicker
Chart::Weather::Forecast::Temperature 0.04
Cheat::Meta 0.0.4
Chemistry::Atom 0.37
Chemistry::Bond 0.37
Chemistry::File 0.37
Chemistry::File::Dumper 0.37
Chemistry::Obj 0.37
Chemistry::Ring::Find 0.2
Sys::MemInfo
Chess::FIDE::Player 1.10
Child::IPC::Pipe undef
Data::Container
Error::Hierarchy::Mixin
YAML::Active::Plugin::Array
YAML::Active::Plugin::Hash
Data::Comparable
Getopt::Inherited
Class::Factory::Enhanced
Class::Accessor::FactoryTyped
Class::Value
Class::Accessor::Constructor
String::FlexMatch
Test::Class::GetoptControl
Property::Lookup
Class::Null
Data::Storage
YAML::Active
Class::Scaffold::Accessor 1.102280
Class::Scaffold::App::CommandLine 1.102280
Class::Scaffold::App::Test::YAMLDriven 1.102280
Class::Scaffold::App::Test::Classes 1.102280
Class::Scaffold::Base 1.102280
Error::Hierarchy::Internal
Child::IPC::Socket undef
Class::Scaffold::ConstantImporter 1.102280
Child::Link undef
Class::Scaffold::Delegate::Mixin 1.102280
Class::Scaffold::Environment 1.102280
Child::Link::IPC undef
Class::Scaffold::Exception 1.102280
Class::Scaffold::Exception::Container 1.102280
Class::Scaffold::Exception::Business 1.102280
Child::Link::IPC::Pipe undef
Child::Link::IPC::Pipe::Parent undef
Class::Scaffold::Exception::Util 1.102280
Class::Scaffold::Factory 1.102280
Class::Scaffold::HierarchicalDirty 1.102280
Class::Scaffold::Factory::Type 1.102280
Child::Link::IPC::Pipe::Proc undef
Child::Link::IPC::Socket undef
Class::Scaffold::Log 1.102280
Child::Link::IPC::Socket::Parent undef
Class::Scaffold::Storable 1.102280
Child::Link::IPC::Socket::Proc undef
Class::Scaffold::Test 1.102280
Class::Scaffold::Test::Inherited 1.102280
Class::Scaffold::Test::UtilLoader 1.102280
Class::Scaffold::Util 1.102280
Class::Scaffold::YAML::Active 1.102280
Child::Link::Parent undef
Child::Link::Proc undef
Child::Util undef
Chloro::Error::Field 0.02
Chloro::Error::Form 0.02
Class::Scaffold::YAML::Marshall 1.102280
Class::Scaffold::YAML::Active::Hash 1.102280
Chloro::ErrorMessage 0.02
Chloro::Field 0.02
Chloro::Group 0.02
Chloro::Result::Field 0.02
Chloro::Result::Group 0.02
Chloro::ResultSet 0.02
Chloro::Role::Error 0.02
Chloro::Role::Form 0.02
Chloro::Role::FormComponent 0.02
Chloro::Role::Result 0.02
Chloro::Role::ResultSet 0.02
Chloro::Role::Trait::HasFormComponents 0.02
Class::Value::Boolean 1.100840
Chloro::Trait::Application 0.02
Class::Value::Enum 1.100840
Chloro::Trait::Application::ToClass 0.02
Class::Value::Exception::Base 1.100840
Chloro::Trait::Application::ToRole 0.02
Class::Value::Exception::NotWellFormedValue 1.100840
Chloro::Trait::Class 0.02
Chloro::Trait::Role 0.02
Chloro::Trait::Role::Composite 0.02
Chloro::Types 0.02
Chloro::Types::Internal 0.02
Cindy::Action undef
Cindy::CJSGrammar undef
Cindy::Injection undef
Cindy::Log undef
Cindy::Log::Default undef
Cindy::Profile undef
Cindy::Sheet undef
Cindy::XPathContext undef
Tangence::Client
Net::Async::Tangence::Server
Attribute::Storage
Circle 0.02
Circle::Collection undef
Circle::Command undef
Circle::CommandInvocation undef
Circle::Commandable undef
Circle::Configurable undef
IO::Async::Loop::Glib
Circle::FE::Gtk 0.02
Circle::FE::Gtk::Tab undef
Circle::FE::Gtk::Widget::Box undef
Circle::FE::Gtk::Widget::Entry undef
Circle::FE::Gtk::Widget::Label undef
Circle::FE::Gtk::Widget::Scroller undef
Circle::GlobalRules undef
Circle::Net::IRC undef
Circle::Net::IRC::Channel undef
Circle::Net::IRC::Target undef
Circle::Net::IRC::User undef
Circle::Net::Raw undef
Circle::RootObj undef
Circle::Rule::Chain undef
Circle::Rule::Resultset undef
Circle::Rule::Store undef
Circle::Ruleable undef
Circle::Session::Tabbed undef
Circle::TaggedString undef
Circle::Widget undef
Circle::Widget::Box undef
Circle::Widget::Entry undef
Circle::Widget::Label undef
Circle::Widget::Scroller undef
Circle::WindowItem undef
HTML::Encoding
HTML::DOM::Collection
Citrix::Farm 0.25
Citrix::LaunchMesg 0.25
Citrix::SessOp 0.25
Citrix::SessionSet 0.25
Class::Accessor::Classy v0.9.1
Graph
Class::Accessor::Constructor::Base 1.100880
Class::Accessor::Fast 0.34
Class::Value::String 1.100840
Class::Accessor::Faster 0.34
Class::Value::Test 1.100840
Class::Accessor::Lvalue::Fast undef
Class::Adapter::Builder 1.07
Test::Carp
Class::Action::Step 0.4
Class::Adapter::Clear 1.07
Tie::Hash::MultiValue
Hash::AutoHash
Hash::AutoHash::Args
Class::AutoClass::Root 1
Class::Autouse::Parent 2.00
Class::InsideOut
Business::DK::CVR
PPI::Cache
Perl::Critic::Policy
Perl::Critic::PolicyFactory
Perl::Critic::PolicyParameter
Perl::Critic::TestUtils
Perl::Critic::UserProfile
Perl::Critic::Bangs
Class::Business::DK::CPR 0.01
Class::Business::DK::CVR 0.01
Class::Business::DK::FI 0.01
Class::Component::Attribute undef
Class::Component::Attribute::Hook undef
Class::Component::Attribute::Method undef
Class::Component::Component::Autocall undef
Class::Component::Component::Autocall::Autoload undef
Class::Component::Component::Autocall::InjectMethod undef
Class::Component::Component::Autocall::SingletonMethod undef
Class::Component::Component::AutoloadPlugin undef
Class::Component::Component::DisableDynamicPlugin undef
Class::Component::Component::Moosenize undef
Class::Component::Component::Plaggerize undef
Class::Component::Component::Plaggerize::ConfigLoader undef
Class::Component::Component::Plaggerize::Log undef
Class::Component::Component::Plaggerize::PluginLoader undef
Class::Component::Component::SingletonMethod undef
Class::Component::Plugin undef
DBIx::ContextualFetch
Ima::DBI
UNIVERSAL::moniker
Class::DBI 3.0.17
Class::DBI::Attribute undef
Class::DBI::Cascade::Delete undef
Class::DBI::Cascade::Fail undef
Class::DBI::Cascade::None undef
Class::DBI::ClassGenerator::DBD::SQLite 1.0
Class::DBI::ClassGenerator::DBD::mysql 1.0
Class::DBI::Column undef
Class::DBI::ColumnGrouper undef
Class::DBI::Iterator undef
Ima::DBI::Contextual
Class::DBI::Lite::CacheManager undef
Class::DBI::Lite::CacheManager::InMemory undef
Class::DBI::Lite::CacheManager::Memcached undef
Class::DBI::Lite::ColumnInfo undef
Class::DBI::Lite::Dataset undef
Class::DBI::Lite::EntityMeta undef
Class::DBI::Lite::Fixture undef
Class::DBI::Lite::Iterator undef
Class::DBI::Lite::Pager undef
Class::DBI::Lite::RootMeta undef
Class::DBI::Lite::SQLite undef
Class::DBI::Lite::TableInfo undef
Class::DBI::Lite::mysql undef
Class::DBI::Query undef
Class::DBI::Relationship undef
Class::DBI::Relationship::HasA undef
Class::DBI::Relationship::HasMany undef
Class::DBI::Relationship::MightHave undef
Class::DBI::SQL::Transformer undef
Class::DBI::Search::Basic undef
Class::DBI::Test::SQLite undef
Class::Date::Const undef
Class::Declarative::EventContext 0.01
Class::Declarative::Node 0.03
Class::Declarative::Parser 0.01
Class::Declarative::Semantics 0.01
Class::Declarative::Semantics::Code 0.01
Class::Declarative::Semantics::Data 0.01
Class::Declarative::Semantics::POD 0.02
Class::Declarative::Semantics::Value 0.01
Class::Declarative::Util 0.01
Class::Declare::Dump 0.17
Class::Declare::Hash 0.17
Tk::NoteBook
Class::Dot::Types 1.5.0
Class::Easy::Base undef
POD::Tested
Class::Easy::Import undef
Class::Easy::Log undef
Class::Easy::Log::Tie undef
Class::Easy::Timer undef
Class::Fields::Attribs 0.03
Class::Fields::Fuxor 0.06
Class::Fields::Inherit 0.06
Class::Gomor::Array 1.02
Class::Gomor::Hash 1.02
Class::InsideOut::Manual::About 1.10
Class::InsideOut::Manual::Advanced 1.10
Class::Inspector::Functions 1.25
Scalar::Defer
Class::Lego::Myself 0.003
Class::LoaderTest undef
Class::MOP 2.0002
File::ShareDir::PathClass
Class::MOP::Attribute 2.0002
Class::MOP::Class 2.0002
Class::MOP::Instance 2.0002
Class::MOP::Method 2.0002
Class::MOP::Method::Accessor 2.0002
Class::MOP::Method::Constructor 2.0002
Class::MOP::Method::Generated 2.0002
Class::MOP::Method::Inlined 2.0002
Class::MOP::Method::Meta 2.0002
Class::MOP::Method::Wrapped 2.0002
Class::MOP::Module 2.0002
Class::MOP::Object 2.0002
Class::MOP::Package 2.0002
Data::Flow
Role::HasPayload::Merged
Throwable::X
unmocked
mocked
Net::API::RPX
Perl::Critic::Tics
Perl::Critic::Policy::ValuesAndExpressions::ProhibitLeadingZeros
Perl::Critic::Lax
YAML::Active::Plugin 1.100810
Path::Dispatcher
Path::Dispatcher::Declarative
Class::MakeMethods::Attribute 1.005
Class::MakeMethods::Autoload 1
Class::MakeMethods::Basic 1
Class::MakeMethods::Basic::Array 1
Class::MakeMethods::Basic::Global 1
Class::MakeMethods::Basic::Hash 1
Class::MakeMethods::Composite 1
Class::MakeMethods::Composite::Array 1
Class::MakeMethods::Composite::Global 1
Class::MakeMethods::Composite::Hash 1
Class::MakeMethods::Composite::Inheritable 1
Class::MakeMethods::Composite::Universal 1
Class::MakeMethods::Emulator 1.009
Class::MakeMethods::Emulator::AccessorFast undef
Class::MakeMethods::Emulator::Inheritable undef
Class::MakeMethods::Emulator::MethodMaker 1.03
Class::MakeMethods::Emulator::Singleton undef
Class::MakeMethods::Emulator::Struct undef
Class::MakeMethods::Emulator::accessors 0.02
Class::MakeMethods::Emulator::mcoder 0.05
Class::MakeMethods::Evaled 1
Class::MakeMethods::Evaled::Hash 1
Class::MakeMethods::Standard 1
Class::MakeMethods::Standard::Array 1
Class::MakeMethods::Standard::Global 1
Class::MakeMethods::Standard::Hash 1
Class::MakeMethods::Standard::Inheritable 1
Class::MakeMethods::Standard::Universal 1
Class::MakeMethods::Template 1.008
Class::MakeMethods::Template::Array 1.008
Class::MakeMethods::Template::Class 1.008
Class::MakeMethods::Template::ClassInherit 1.008
Class::MakeMethods::Template::ClassName 1.008
Class::MakeMethods::Template::ClassVar 1.008
Class::MakeMethods::Template::Flyweight 1.008
Class::MakeMethods::Template::Generic 1.008
Class::MakeMethods::Template::Global 1.008
Class::MakeMethods::Template::Hash 1.008
Class::MakeMethods::Template::Inheritable 1.008
Class::MakeMethods::Template::InsideOut 1.008
Class::MakeMethods::Template::PackageVar 1.008
Class::MakeMethods::Template::Ref 1.008
Class::MakeMethods::Template::Scalar 1.008
Class::MakeMethods::Template::Static 1.008
Class::MakeMethods::Template::Struct 1.008
Class::MakeMethods::Template::StructBuiltin 1.008
Class::MakeMethods::Template::Universal 1.008
Class::MakeMethods::Utility::ArraySplicer 1
Class::MakeMethods::Utility::DiskCache 1.008
Class::MakeMethods::Utility::Inheritable 1
Class::MakeMethods::Utility::Ref 1
Class::MakeMethods::Utility::TextBuilder 1.008
Class::Member::Dynamic 1.6
Class::Member::GLOB 1.6
Class::Member::HASH 1.6
Class::Meta::AccessorBuilder 0.63
Class::Meta::AccessorBuilder::Affordance 0.63
Class::Meta::AccessorBuilder::SemiAffordance 0.63
Class::Meta::Attribute 0.63
Class::Meta::Class 0.63
Class::Meta::Constructor 0.63
Class::Meta::Method 0.63
Class::Meta::Type 0.63
Class::Meta::Types::Boolean 0.63
Class::Meta::Types::Numeric 0.63
Class::Meta::Types::Perl 0.63
Class::Meta::Types::String 0.63
Class::MethodMaker::Constants undef
Class::MethodMaker::Engine 2.16
Class::MethodMaker::OptExt undef
Class::MethodMaker::V1Compat undef
Class::MixinFactory::Factory 0.91
Class::MixinFactory::HasAFactory undef
Class::MixinFactory::InsideOutAttr undef
Class::MixinFactory::NEXT undef
Class::Scaffold::App 1.102280
Class::Scaffold::App::Test 1.102280
Class::Scaffold::App::Test::Class 1.102280
Class::Scaffold::Base_TEST 1.102280
Class::Scaffold::BusinessObject 1.102280
Class::Scaffold::Context 1.102280
Class::Scaffold::Environment_TEST 1.102280
Class::Scaffold::Exception::Loader 1.102280
Class::Scaffold::Exception::NoSuchFactoryHandler 1.102280
Class::Scaffold::Introspect 1.102280
Class::Scaffold::LazyString 1.102280
Class::Scaffold::Log_TEST 1.102280
Class::Scaffold::Storable_TEST 1.102280
Class::Scaffold::YAML::Active::Array 1.102280
Class::Scaffold::YAML::Active::Constant 1.102280
Class::Scaffold::YAML::Active::Environment 1.102280
Class::Scaffold::YAML::Active::Exception 1.102280
Class::Scaffold::YAML::Active::ExceptionContainer 1.102280
Class::Scaffold::YAML::Marshall::Concat 1.102280
Class::Scaffold::YAML::Marshall::Constant 1.102280
Class::Scaffold::YAML::Marshall::ExceptionContainer 1.102280
Class::Scaffold::YAML::Marshall::PID 1.102280
Class::Spiffy::mixin undef
Class::Std
Class::Std::Fast 0.0.8
Class::Std::Fast::Storable 0.0.8
Class::Std::Utils 0.0.3
Class::Struct 0.63
Class::Trait::Base 0.31
Class::Trait::Config 0.31
Class::Trait::Reflection 0.31
Class::Value::DefaultNotify 1.100840
Class::Value::Exception 1.100840
Class::Value::Exception::InvalidValue 1.100840
Class::Value::Exception::UnsupportedOperation 1.100840
Class::Value::SemanticAdapter
Data::Semantic
Data::Semantic::Test
Data::Semantic::Net::IPAddress::TestData::IPv6
Data::Semantic::Net::IPAddress::TestData::IPv4
Class::Value::Net::DNSSEC::DS::Alg 1.110250
Class::Value::Net::DNSSEC::DS::Alg_TEST 1.110250
Class::Value::Net::DNSSEC::DS::Digest 1.110250
Class::Value::Net::DNSSEC::DS::DigestType 1.110250
Class::Value::Net::DNSSEC::DS::DigestType_TEST 1.110250
Class::Value::Net::DNSSEC::DS::Digest_TEST 1.110250
Class::Value::Net::DNSSEC::DS::KeyTag 1.110250
Class::Value::Net::DNSSEC::DS::KeyTag_TEST 1.110250
Class::Value::Net::Exception 1.110250
Class::Value::Net::Exception::DNSSEC 1.110250
Class::Value::Net::Exception::DNSSEC::DS::InvalidAlg 1.110250
Class::Value::Net::Exception::DNSSEC::DS::InvalidDigest 1.110250
Class::Value::Net::Exception::DNSSEC::DS::InvalidDigestType 1.110250
Class::Value::Net::Exception::DNSSEC::DS::InvalidKeyTag 1.110250
Class::Value::Net::Exception::InvalidIPAddress 1.110250
Class::Value::Net::Exception::MalformedHostname 1.110250
Class::Value::Net::Exception::MalformedIPAddress 1.110250
Class::Value::Net::Exception::NAPTR 1.110250
Class::Value::Net::Exception::NAPTR::InvalidFlags 1.110250
Class::Value::Net::Exception::NAPTR::InvalidOrder 1.110250
Class::Value::Net::Exception::NAPTR::InvalidPreference 1.110250
Class::Value::Net::Exception::NAPTR::InvalidRegexp 1.110250
Class::Value::Net::Exception::NAPTR::InvalidReplacement 1.110250
Class::Value::Net::Exception::NAPTR::InvalidServices 1.110250
Class::Value::Net::Exception::NAPTR::MalformedFlags 1.110250
Class::Value::Net::Exception::NAPTR::MalformedOrder 1.110250
Class::Value::Net::Exception::NAPTR::MalformedPreference 1.110250
Class::Value::Net::Exception::NAPTR::MalformedRegexp 1.110250
Class::Value::Net::Exception::NAPTR::MalformedReplacement 1.110250
Class::Value::Net::Exception::NAPTR::MalformedServices 1.110250
Class::Value::Net::Hostname 1.110250
Class::Value::Net::IPAddress 1.110250
Class::Value::Net::IPAddress::IPv4 1.110250
Class::Value::Net::IPAddress::IPv4_TEST 1.110250
Class::Value::Net::IPAddress::IPv6 1.110250
Class::Value::Net::IPAddress::IPv6_TEST 1.110250
Class::Value::Net::NAPTR::CharacterString 1.110250
Class::Value::Net::NAPTR::CharacterString_TEST 1.110250
Class::Value::Net::NAPTR::Flags 1.110250
Class::Value::Net::NAPTR::Flags_TEST 1.110250
Class::Value::Net::NAPTR::Order 1.110250
Class::Value::Net::NAPTR::Order_TEST 1.110250
Class::Value::Net::NAPTR::Origin 1.110250
Class::Value::Net::NAPTR::Preference 1.110250
Class::Value::Net::NAPTR::Preference_TEST 1.110250
Class::Value::Net::NAPTR::Regexp 1.110250
Class::Value::Net::NAPTR::Replacement 1.110250
Class::Value::Net::NAPTR::Replacement_TEST 1.110250
Class::Value::Net::NAPTR::Services 1.110250
Class::Value::Net::NAPTR::UInt16 1.110250
Class::Value::Net::NAPTR::UInt16_TEST 1.110250
Class::Value::Net::NAPTR::Wildcard 1.110250
Class::Value::Notify 1.100840
Class::Virtually::Abstract 0.03
Class::XSAccessor::Array 1.11
XML::Catalog
XML::TreeBuilder
ClearPress::authdecor 349
ClearPress::authenticator 388
ClearPress::authenticator::db 388
ClearPress::authenticator::ldap 390
ClearPress::authenticator::passwd 348
ClearPress::authenticator::session 348
ClearPress::controller 389
ClearPress::decorator 399
ClearPress::driver 349
ClearPress::driver::Pg 348
ClearPress::driver::SQLite 320
ClearPress::driver::mysql 375
ClearPress::model 398
ClearPress::util 388
ClearPress::view 388
ClearPress::view::error 384
Clipboard::MacPasteboard undef
Clipboard::Win32 undef
Olson::Abbreviations
MooseX::Types::DateTime::ButMaintained
MooseX::Types::DateTimeX
Clipboard::Xclip undef
CloudApp::REST::Item undef
CloudApp::REST::Item::Archive undef
CloudApp::REST::Item::Audio undef
CloudApp::REST::Item::Bookmark undef
CloudApp::REST::Item::Image undef
CloudApp::REST::Item::Pdf undef
CloudApp::REST::Item::Text undef
CloudApp::REST::Item::Unknown undef
Convert::Base32
Convert::Base32::Crockford
CloudApp::REST::Item::Video undef
Template::Toolkit::Simple
Plack::Middleware::ETag
Plack::Middleware::Cache
JavaScript::V8
JavaScript::V8x::TestMoreish
Jemplate
Cog::App undef
Cog::App::Test undef
Cog::Base undef
Cog::Cog 0.05
Cog::Command undef
Cog::Config undef
Cog::Content undef
Cog::FileBrowser undef
Cog::Maker undef
Cog::Node undef
Cog::Node::Schema undef
Cog::Page undef
Cog::Plugin undef
Cog::Schema undef
Cog::Store undef
Cog::View undef
Cog::WebApp undef
Cog::WebApp::Runner undef
XML::Flow
Flow
Collection::AutoSQL 1.1
Collection::AutoSQLnotUnique 0.01
Collection::Mem 0.01
Collection::Memcached 0.01
Collection::Storable 0.01
Collection::Utl::ActiveRecord 0.01
Collection::Utl::Base 0.01
Collection::Utl::HashUnion 0.01
Collection::Utl::Item 0.02
Collection::Utl::LazyObject 0.01
Collection::Utl::Mirror 0.02
Collision::2D::Collision undef
Collision::2D::Entity undef
Collision::2D::Entity::Circle undef
Collision::2D::Entity::Grid undef
Collision::2D::Entity::Point undef
Collision::2D::Entity::Rect undef
Color::Calc::WWW 1.070
Color::Calc::hex 1.070
Color::Calc::html 1.070
Color::Calc::object 1.070
Color::Calc::pdf 1.070
Color::Calc::tuple 1.070
Color::Library::Color undef
Color::Library::Dictionary undef
Color::Library::Dictionary::HTML undef
Color::Library::Dictionary::IE undef
Color::Library::Dictionary::Mozilla undef
Color::Library::Dictionary::NBS_ISCC undef
Color::Library::Dictionary::NBS_ISCC::A undef
Color::Library::Dictionary::NBS_ISCC::B undef
Color::Library::Dictionary::NBS_ISCC::F undef
Color::Library::Dictionary::NBS_ISCC::H undef
Color::Library::Dictionary::NBS_ISCC::M undef
Color::Library::Dictionary::NBS_ISCC::P undef
Color::Library::Dictionary::NBS_ISCC::R undef
Color::Library::Dictionary::NBS_ISCC::RC undef
Color::Library::Dictionary::NBS_ISCC::S undef
Color::Library::Dictionary::NBS_ISCC::SC undef
Color::Library::Dictionary::NBS_ISCC::TC undef
Color::Library::Dictionary::Netscape undef
Color::Library::Dictionary::SVG undef
Color::Library::Dictionary::Tango undef
Color::Library::Dictionary::VACCC undef
Color::Library::Dictionary::WWW undef
Color::Library::Dictionary::Windows undef
Color::Library::Dictionary::X11 undef
Devel::Mallinfo
Getopt::Complete
Class::AutoloadCAN
Net::HTTPServer
Class::Autouse
Command 0.30
Command::Tree 0.30
Command::V1 0.30
Command::V2 0.30
Compress::Zlib 2.033
Config undef
Config::Any::Base undef
Config::Any::General undef
Config::Any::INI undef
Config::Any::JSON undef
Config::Any::Perl undef
Config::Any::XML undef
Config::Any::YAML undef
Config::AutoConf::Linker undef
Config::Extensions 0.01
Config::Find::Any 0.26
Config::Find::Unix 0.23
Config::Find::Where 0.24
Config::Find::Win2k 0.01
Config::Find::Win2k3 0.01
Config::Find::Win95 0.01
Config::Find::Win98 0.01
Config::Find::WinAny 0.18
Config::Find::WinCE 0.01
Config::Find::WinME 0.01
Config::Find::WinNT 0.01
MouseX::StrictConstructor
Config::Find::WinXP 0.01
Test::File::Contents
Test::Log::Log4perl
Config::Model::Tester
Text::Levenshtein::Damerau
Lchown
Filesys::Statvfs
Unix::Mknod
Fuse
Config::Model::Backend::Any 1.243
Config::General::Extended 2.05
Config::General::Interpolated 2.14
Config::GitLike::Cascaded undef
Config::GitLike::Git undef
Config::Model::Backend::IniFile 1.243
Config::INI::Writer 0.017
Config::IPFilter::Rule 1.00
Config::IPFilter::Types 1.00
Config::Identity::GitHub undef
Config::Identity::PAUSE undef
Coro::Channel 5.372
Coro::Event 5.372
Config::JFDI::Carp undef
Config::JFDI::Source::Loader undef
Config::MVP::Error 2.200001
Config::MVP::Reader::Findable 2.200001
Config::MVP::Reader::Hash 2.200001
Config::MVP::Sequence 2.200001
Config::Model::FuseUI 1.243
Config::Merge::Perl undef
Alien::Sedna
Sedna
Config::Model::Annotation 1.243
Config::Model::AnyId 1.243
Config::Model::AnyThing 1.243
Config::Model::AutoRead 1.243
Config::Model::ObjTreeScanner 1.243
Config::Model::Backend::Fstab 1.243
Curses
Curses::UI
Config::Model
Config::Model::CursesUI
Tk::Pod
Tk::ROText
Tk::PNG
Tk::Adjuster
Tk::Balloon
Tk::BrowseEntry
Tk::Dialog
Tk::DialogBox
Tk::Frame
Tk::Menubutton
Tk::Pane
Tk::Photo
Tk::Pod::Text
Tk::Toplevel
Tk::Tree
Tk::DirSelect
Tk::DoubleClick
Tk::FontDialog
Config::Model::Value
Config::Model::TkUI
Config::Model::BackendMgr
App::Cme
Config::Model::Backend::OpenSsh::Ssh 1.216
Config::Model::SimpleUI 1.243
Config::Model::TermUI 1.243
Config::Model::Backend::OpenSsh::Sshd 1.216
Config::Model::Backend::PlainFile 1.243
Role::Basic
Config::Model::Backend::ShellVar 1.243
Regexp::RegGrp
Config::Model::Exception
Config::Model::Backend::Yaml 1.243
Config::Model::CheckList 1.243
Config::Model::Describe 1.243
Config::Model::DumpAsData 1.243
Config::Model::Dumper 1.243
Coro::EV 5.372
Config::Model::HashId 1.243
Set::IntSpan
Date::JD
Time::Unix
Net::FTP::Tiny
Net::HTTP::Tiny
Time::UTC
Math::Interpolator::Robust
Math::Interpolator::Source
Math::Interpolator::Knot
Math::Interpolator::Linear
Time::TT::Realisation
Config::Model::IdElementReference 1.243
Config::Model::Instance 1.243
Config::Model::Iterator 1.243
Config::Model::ListId 1.243
Config::Model::Loader 1.243
Config::Model::Node 1.243
Config::Model::Report 1.243
Config::Model::SearchElement 1.243
Config::Model::Searcher 1.235
Config::Model::ValueComputer 1.243
Config::Model::WarpedNode 1.243
Config::Model::WarpedThing 1.241
Config::Model::Warper 1.243
Config::OpenSSH::Authkey::Entry 0.14
Config::OpenSSH::Authkey::Entry::Options 0.14
Config::Options::Threaded 0.05
Config::XPath::Reloadable 0.16
Config::YAMLMacros::YAML undef
Config::ZOMG::Source::Loader 0.001000
Sub::Chain
Contextual::Return::Failure 0.0.2
Continuity::Adapt::FCGI undef
Continuity::Adapt::HttpDaemon undef
Continuity::Adapt::PSGI undef
Continuity::Inspector undef
Continuity::Mapper undef
Continuity::Request undef
Continuity::RequestHolder undef
ControlFreak::Command undef
ControlFreak::Commander undef
ControlFreak::Console undef
Crypt::Eksblowfish::Uklblowfish 0.009
ControlFreak::Logger undef
ControlFreak::Proxy undef
ControlFreak::Proxy::Process undef
ControlFreak::Service undef
ControlFreak::Socket undef
ControlFreak::Util undef
Convert::ASCII::Armor undef
Convert::ASN1::parser undef
Convert::Binary::C::Cached 0.76
Convert::Color::CMY 0.08
Convert::Color::CMYK 0.08
Convert::Color::HSL 0.08
Convert::Color::HSV 0.08
Convert::Color::RGB 0.08
Convert::Color::RGB16 0.08
Convert::Color::RGB8 0.08
Convert::Color::VGA 0.08
Convert::Color::X11 0.08
Convert::PEM::CBC undef
Convert::YText 0.1.2
Convert::yEnc::Decoder undef
Convert::yEnc::Entry undef
Convert::yEnc::RC undef
Coro::AIO 5.372
Coro::BDB 5.372
Coro::Debug 5.372
Coro::Handle 5.372
Coro::LWP 5.372
Coro::MakeMaker 5.372
Coro::RWLock 5.372
Coro::Select 5.372
Coro::Semaphore 5.372
Coro::SemaphoreSet 5.372
Coro::Signal 5.372
Coro::Socket 5.372
Coro::Specific 5.372
Coro::State 5.372
Coro::Storable 5.372
Coro::Timer 5.372
Coro::Util 5.372
Corona::Server undef
CouchDB::Client::DB undef
CouchDB::Client::DesignDoc undef
CouchDB::Client::Doc undef
Crypt::DH::GMP::Compat undef
Crypt::DSA::Key 1.16
Crypt::DSA::Key::PEM 1.16
Crypt::DSA::Key::SSH2 1.16
Crypt::DSA::KeyChain 1.16
Crypt::DSA::Signature 1.16
Crypt::DSA::Util 1.16
Crypt::Eksblowfish::Blowfish 0.009
Crypt::Eksblowfish::Family 0.009
Crypt::Eksblowfish::Subkeyed 0.009
Crypt::FNA::Validation 0.02
Crypt::Keyczar::AesKey undef
Crypt::Keyczar::Crypter undef
Crypt::Keyczar::DsaPrivateKey undef
Crypt::Keyczar::DsaPublicKey undef
Crypt::Keyczar::Encrypter undef
Crypt::Keyczar::Engine undef
Crypt::Keyczar::FileReader undef
Crypt::Keyczar::FileWriter undef
Crypt::Keyczar::HmacKey undef
Crypt::Keyczar::HmacSHA224Key undef
Crypt::Keyczar::HmacSHA256Key undef
Error::Hierarchy
Error::Hierarchy::Util
Crypt::Keyczar::HmacSHA384Key undef
Crypt::Keyczar::HmacSHA512Key undef
Crypt::Keyczar::Key undef
Crypt::Keyczar::KeyMetadata undef
Crypt::Keyczar::KeyVersion undef
Crypt::Keyczar::Manager undef
WWW::Curl::Easy
Crypt::Keyczar::Reader undef
Crypt::Keyczar::RsaPrivateKey undef
Crypt::Keyczar::RsaPublicKey undef
Crypt::Keyczar::Signer undef
Crypt::Keyczar::Tool undef
Module::CoreList::DBSchema
App::CPANIDX::Tables
JSYNC
Crypt::Keyczar::Util undef
Crypt::Keyczar::Verifier undef
Crypt::Keyczar::Writer undef
Test::Group
Crypt::OpenSSL::CA
Crypt::OpenSSL::Bignum::CTX undef
Crypt::OpenSSL::Cloner::x509asn1 undef
Crypt::NULL
Crypt::OpenToken::Cipher undef
Crypt::OpenToken::Cipher::AES128 undef
Crypt::OpenToken::Cipher::AES256 undef
Crypt::OpenToken::Cipher::DES3 undef
Net::Whois::IANA
Regexp::IPv6
Net::Whois::IP
Crypt::OpenToken::Cipher::null undef
Crypt::OpenToken::KeyGenerator undef
Crypt::OpenToken::Serializer undef
Crypt::OpenToken::Token undef
Crypt::PBKDF2::Hash 0.110461
Crypt::PBKDF2::Hash::DigestHMAC 0.110461
Crypt::PBKDF2::Hash::HMACSHA1 0.110461
Crypt::PBKDF2::Hash::HMACSHA2 0.110461
Crypt::RIPEMD160::MAC 0.01
Crypt::Primes
Crypt::RSA::DataFormat undef
Crypt::RSA::Debug undef
Crypt::RSA::ES::OAEP 1.99
Crypt::RSA::ES::PKCS1v15 1.99
Crypt::RSA::Errorhandler undef
Crypt::RSA::Key 1.99
Crypt::RSA::Key::Private 1.99
Crypt::RSA::Key::Private::SSH undef
Crypt::RSA::Key::Public 1.99
Crypt::RSA::Key::Public::SSH undef
Crypt::RSA::Primitives undef
Crypt::RSA::SS::PKCS1v15 1.99
Crypt::RSA::SS::PSS 1.99
Crypt::Random::Generator undef
Crypt::Random::Provider::File undef
Crypt::Random::Provider::devrandom undef
Crypt::Random::Provider::devurandom undef
Crypt::Random::Provider::egd undef
Crypt::Random::Provider::rand undef
Crypt::Random::Source::Base 0.07
Crypt::Random::Source::Base::File 0.07
Crypt::Random::Source::Base::Handle 0.07
Crypt::Random::Source::Base::Proc 0.07
Crypt::Random::Source::Base::RandomDevice 0.07
Crypt::Random::Source::Factory 0.07
Curses::UI::Progressbar 1.10
Curses::UI::Radiobuttonbox 1.10
Curses::UI::Searchable 1.10
Curses::UI::TextEditor 1.5
Curses::UI::TextEntry 1.10
Curses::UI::TextViewer 1.10
Curses::UI::Widget 1.12
Curses::UI::Window 1.10
Cursor undef
CursorBase undef
Cwd 3.33
Array::Iterator
D64::Disk::Image
D64::Disk::Dir::Entry 0.02
D64::Disk::Dir::Iterator 0.02
D64::Disk::Image::File 0.01
DAIA::Availability 0.30
DAIA::Available 0.28
DAIA::Department 0.27
DAIA::Document 0.27
DAIA::Entity 0.29
DAIA::Institution 0.27
DAIA::Item 0.30
DAIA::Limitation 0.27
DAIA::Message 0.27
DAIA::Object 0.30
DAIA::Response 0.28
DAIA::Storage 0.27
DAIA::Unavailable 0.28
DB::Pluggable::BreakOnTestNumber 1.101051
DB::Pluggable::Constants 1.101051
DB::Pluggable::Plugin 1.101051
DBD::DBM 0.06
DB::Pluggable::TypeAhead 1.101051
DBD::File 0.40
DBD::ExampleP 12.014310
DBD::Gofer 0.014282
DBD::Gofer::Policy::Base 0.010087
DBD::Gofer::Policy::classic 0.010087
DBD::Gofer::Policy::pedantic 0.010087
DBD::Gofer::Policy::rush 0.010087
DBD::Gofer::Transport::Base 0.014120
DBD::Gofer::Transport::corostream undef
DBD::Gofer::Transport::null 0.010087
DBD::Gofer::Transport::pipeone 0.010087
DBD::Gofer::Transport::stream 0.014598
DBD::NullP 12.014563
DBD::Proxy 0.2004
DBD::SQLAnywhere::GetInfo undef
DBD::Sponge 12.010002
DBD::Sys::CompositeTable 0.102
DBD::Sys::Plugin 0.102
DBD::Sys::Plugin::Any 0.102
DBD::Sys::Plugin::Any::FileSys 0.102
DBD::Sys::Plugin::Any::FileSysDf 0.102
DBD::Sys::Plugin::Any::NetIfconfigWrapper undef
DBD::Sys::Plugin::Any::NetInterface undef
DBD::Sys::Plugin::Any::Procs 0.102
DBD::Sys::Plugin::Meta 0.1
DBD::Sys::Plugin::Meta::AllTables 0.102
DBD::Sys::Plugin::Unix 0.102
DBD::Sys::Plugin::Unix::Groups 0.102
DBD::Sys::Plugin::Unix::Logins 0.102
DBD::Sys::Plugin::Unix::Lsof 0.102
DBD::Sys::Plugin::Unix::Users 0.102
DBD::Sys::Plugin::Win32 0.102
DBD::Sys::Plugin::Win32::Groups 0.102
DBD::Sys::Plugin::Win32::Procs 0.102
DBD::Sys::Plugin::Win32::Users 0.102
DBD::Sys::PluginManager 0.102
DBD::Sys::Table 0.1
DBD::mysql::GetInfo undef
DBI::Const::GetInfo::ANSI 2.008696
DBI::Const::GetInfo::ODBC 2.011373
DBI::Const::GetInfoType 2.008696
DBI::Const::GetInfoReturn 2.008696
DBI::DBD 12.014600
DBI::DBD::Metadata 2.014213
DBI::DBD::SqlEngine 0.03
DBI::Easy::Helper undef
DBI::Easy::Record undef
DBI::Easy::Record::Collection undef
DBI::Easy::Vendor::Base undef
DBI::Easy::Vendor::mysql undef
DBI::Easy::Vendor::oracle undef
DBI::Easy::Vendor::postgresql undef
DBI::Gofer::Execute 0.014282
DBI::Gofer::Request 0.012536
DBI::Gofer::Response 0.011565
DBI::Gofer::Serializer::Base 0.009949
DBI::Gofer::Serializer::DataDumper 0.009949
DBI::Gofer::Serializer::Storable 0.009949
DBI::Gofer::Transport::Base 0.012536
DBI::Gofer::Transport::pipeone 0.012536
DBI::Profile 2.014123
DBI::Gofer::Transport::stream 0.012536
DBI::ProfileData 2.010007
DBI::ProfileDumper 2.013956
DBI::ProfileDumper::Apache 2.014120
DBI::ProfileSubs 0.009395
DBI::ProxyServer 0.3005
DBI::SQL::Nano 1.014600
DBI::Util::CacheMemory 0.010314
DBI::Util::_accessor 0.009478
DBICx::Hooks::Registry 0.003
DBICx::TestDatabase::Subclass undef
Tie::Cache
DBIx::Array::Export 0.18
DBIx::Class::AccessorGroup undef
DBIx::Class::Admin undef
DBIx::Class::CDBICompat undef
DBIx::Class::CDBICompat::Iterator undef
DBIx::Class::CDBICompat::SQLTransformer undef
DBIx::Class::Candy::Exports 0.002000
DBIx::Class::Cursor undef
DBIx::Class::DB undef
DBIx::Class::DeploymentHandler::Dad 0.001005
DBIx::Class::DeploymentHandler::DeployMethod::SQL::Translator 0.001005
DBIx::Class::DeploymentHandler::DeployMethod::SQL::Translator::Deprecated 0.001005
DBIx::Class::DeploymentHandler::Deprecated 0.001005
DBIx::Class::DeploymentHandler::HandlesDeploy 0.001005
DBIx::Class::DeploymentHandler::HandlesVersionStorage 0.001005
DBIx::Class::DeploymentHandler::HandlesVersioning 0.001005
DBIx::Class::DeploymentHandler::Logger 0.001005
DBIx::Class::DeploymentHandler::Types 0.001005
DBIx::Class::DeploymentHandler::VersionHandler::DatabaseToSchemaVersions 0.001005
DBIx::Class::DeploymentHandler::VersionHandler::ExplicitVersions 0.001005
DBIx::Class::DeploymentHandler::VersionHandler::Monotonic 0.001005
DBIx::Class::DeploymentHandler::VersionStorage::Deprecated 0.001005
DBIx::Class::DeploymentHandler::VersionStorage::Deprecated::Component 0.001005
DBIx::Class::DeploymentHandler::VersionStorage::Deprecated::VersionResult 0.001005
DBIx::Class::DeploymentHandler::VersionStorage::Deprecated::VersionResultSet 0.001005
DBIx::Class::DeploymentHandler::VersionStorage::Standard 0.001005
DBIx::Class::DeploymentHandler::VersionStorage::Standard::Component 0.001005
DBIx::Class::DeploymentHandler::VersionStorage::Standard::VersionResult 0.001005
DBIx::Class::DeploymentHandler::VersionStorage::Standard::VersionResultSet 0.001005
DBIx::Class::DeploymentHandler::WithApplicatorDumple 0.001005
DBIx::Class::DeploymentHandler::WithReasonableDefaults 0.001005
DBIx::Class::EncodedColumn::Crypt 0.01
DBIx::Class::EncodedColumn::Crypt::Eksblowfish::Bcrypt 0.00001
DBIx::Class::EncodedColumn::Crypt::OpenPGP 0.01
DBIx::Class::EncodedColumn::Digest 0.00001
DBIx::Class::Exception undef
DBIx::Class::FilterColumn undef
DBIx::Class::Fixtures::DBI undef
DBIx::Class::Fixtures::DBI::Pg undef
DBIx::Class::Fixtures::DBI::mysql undef
DBIx::Class::Fixtures::Schema undef
DBIx::Class::Graph::Role::Result 1.03
DBIx::Class::Graph::Role::ResultSet 1.03
DBIx::Class::Graph::Wrapper 1.03
DBIx::Class::Helper::IgnoreWantarray 2.007000
DBIx::Class::Helper::JoinTable 2.007000
DBIx::Class::Helper::Random 2.007000
DBIx::Class::Helper::ResultSet 2.007000
DBIx::Class::Helper::ResultSet::AutoRemoveColumns 2.007000
DBIx::Class::Helper::ResultSet::IgnoreWantarray 2.007000
DBIx::Class::Helper::ResultSet::Me 2.007000
DBIx::Class::Helper::ResultSet::Random 2.007000
DBIx::Class::Helper::ResultSet::RemoveColumns 2.007000
DBIx::Class::Helper::ResultSet::ResultClassDWIM 2.007000
DBIx::Class::Helper::ResultSet::SetOperations 2.007000
DBIx::Class::Helper::ResultSet::Union 2.007000
DBIx::Class::Helper::ResultSet::VirtualView 2.007000
DBIx::Class::Helper::Row::JoinTable 2.007000
DBIx::Class::Helper::Row::NumifyGet 2.007000
DBIx::Class::Helper::Row::OnColumnChange 2.007000
DBIx::Class::Helper::Row::RelationshipDWIM 2.007000
DBIx::Class::Helper::Row::StorageValues 2.007000
DBIx::Class::Helper::Row::SubClass 2.007000
DBIx::Class::Helper::Row::ToJSON 2.007000
DBIx::Class::Helper::Schema::GenerateSource 2.007000
DBIx::Class::Helper::SubClass 2.007000
DBIx::Class::Helper::VirtualView 2.007000
DBIx::Class::Helpers::Util 2.007000
DBIx::Class::InflateColumn undef
DBIx::Class::InflateColumn::FS::ResultSet undef
DBIx::Class::InflateColumn::File undef
DBIx::Class::InflateColumn::Serializer::JSON undef
DBIx::Class::InflateColumn::Serializer::Storable undef
Crypt::Random::Source::Strong 0.07
Crypt::Random::Source::Strong::devrandom 0.07
Crypt::Random::Source::Weak 0.07
Crypt::Random::Source::Weak::devurandom 0.07
Crypt::Random::Source::Weak::rand 0.07
Crypt::SSLeay::CTX undef
Crypt::SSLeay::Conn undef
Crypt::SSLeay::Err undef
Crypt::SSLeay::MainContext undef
Crypt::SSLeay::X509 undef
Curses::Toolkit::Event 0.202
Curses::Toolkit::Event::Content 0.202
Curses::Toolkit::Event::Content::Changed 0.202
Curses::Toolkit::Event::Focus 0.202
Curses::Toolkit::Event::Focus::In 0.202
Curses::Toolkit::Event::Focus::Out 0.202
Curses::Toolkit::Event::Key 0.202
Curses::Toolkit::Event::Mouse 0.202
Curses::Toolkit::Event::Mouse::Click 0.202
Curses::Toolkit::Event::Shape 0.202
Curses::Toolkit::EventListener 0.202
Curses::Toolkit::EventListener::Key 0.202
Curses::Toolkit::Object 0.202
Curses::Toolkit::Object::Coordinates 0.202
Curses::Toolkit::Object::Flags 0.202
Curses::Toolkit::Object::MarkupString 0.202
Curses::Toolkit::Object::Shape 0.202
Curses::Toolkit::Role 0.202
Curses::Toolkit::Role::Focusable 0.202
Curses::Toolkit::Signal 0.202
Curses::Toolkit::Signal::Clicked 0.202
Curses::Toolkit::Signal::Content 0.202
Curses::Toolkit::Signal::Content::Changed 0.202
Curses::Toolkit::Signal::Focused 0.202
Curses::Toolkit::Signal::Focused::In 0.202
Curses::Toolkit::Signal::Focused::Out 0.202
Curses::Toolkit::Theme 0.202
Curses::Toolkit::Theme::Default 0.202
Curses::Toolkit::Theme::Default::Color 0.202
Curses::Toolkit::Theme::Default::Color::BlueWhite 0.202
Curses::Toolkit::Theme::Default::Color::Pink 0.202
Curses::Toolkit::Theme::Default::Color::Yellow 0.202
Curses::Toolkit::Types 0.202
Curses::Toolkit::Widget 0.202
Curses::Toolkit::Widget::Bin 0.202
Curses::Toolkit::Widget::Border 0.202
Curses::Toolkit::Widget::Button 0.202
Curses::Toolkit::Widget::Container 0.202
Curses::Toolkit::Widget::Entry 0.202
Curses::Toolkit::Widget::GenericButton 0.202
Curses::Toolkit::Widget::HBox 0.202
Curses::Toolkit::Widget::HPaned 0.202
Curses::Toolkit::Widget::HProgressBar 0.202
Curses::Toolkit::Widget::HScrollBar 0.202
Curses::Toolkit::Widget::Label 0.202
Curses::Toolkit::Widget::Paned 0.202
Curses::Toolkit::Widget::ProgressBar 0.202
Curses::Toolkit::Widget::ScrollArea 0.202
Curses::Toolkit::Widget::VBox 0.202
Curses::Toolkit::Widget::VPaned 0.202
Curses::Toolkit::Widget::VProgressBar 0.202
Curses::Toolkit::Widget::VScrollBar 0.202
Curses::Toolkit::Widget::Window 0.202
Curses::Toolkit::Widget::Window::Dialog 0.202
Curses::Toolkit::Widget::Window::Dialog::About 0.202
Curses::UI::Buttonbox 1.10
Curses::UI::Calendar 1.10
Curses::UI::Checkbox 1.11
Curses::UI::Color 0.01
Curses::UI::Common 1.10
Curses::UI::Container 1.11
Curses::UI::Dialog::Basic 1.10
Curses::UI::Dialog::Calendar 1.10
Curses::UI::Dialog::Dirbrowser 1.0
Curses::UI::Dialog::Error 1.10
Curses::UI::Dialog::Filebrowser 1.10
Curses::UI::Dialog::Progress 1.10
Curses::UI::Dialog::Question 1.00
Curses::UI::Dialog::Status 1.10
Curses::UI::Label 1.11
Curses::UI::Language undef
Curses::UI::Language::chinese undef
Curses::UI::Language::czech undef
Curses::UI::Language::dutch undef
Curses::UI::Language::english undef
Curses::UI::Language::french undef
Curses::UI::Language::german undef
Curses::UI::Language::italian undef
Curses::UI::Language::japanese undef
Curses::UI::Language::norwegian undef
Curses::UI::Language::polish undef
Curses::UI::Language::portuguese undef
Curses::UI::Language::russian undef
Curses::UI::Language::slovak undef
Curses::UI::Language::spanish undef
Curses::UI::Language::turkish undef
Curses::UI::Listbox 1.3
Curses::UI::Menubar 1.10
Curses::UI::Notebook 1.0001
Curses::UI::PasswordEntry 1.10
DBIx::Class::InflateColumn::Serializer::YAML undef
DBIx::Class::Journal::EvalWrap undef
DBIx::Class::KiokuDB 1.19
DBIx::Class::KiokuDB::EntryProxy 1.19
DBIx::Class::MooseColumns::Meta::Attribute undef
DBIx::Class::MooseColumns::Meta::Attribute::DBICColumn undef
DBIx::Class::MooseColumns::Meta::Attribute::DBICColumn::Inflated undef
Test::Aggregate
DBIx::Class::MooseColumns::Meta::Role::Attribute undef
DBIx::Class::MooseColumns::Meta::Role::Attribute::DBICColumn undef
DBIx::Class::MooseColumns::Meta::Role::Attribute::DBICColumn::Inflated undef
DBIx::Class::Ordered undef
DBIx::Class::PK undef
DBIx::Class::PK::Auto undef
Class::Data::Accessor
DBIx::Class::PK::Auto::DB2 undef
DBIx::Class::PK::Auto::MSSQL undef
DBIx::Class::PK::Auto::MySQL undef
DBIx::Class::PK::Auto::Oracle undef
DBIx::Class::PK::Auto::Pg undef
DBIx::Class::PK::Auto::SQLite undef
DBIx::Class::QueryLog::Query undef
DBIx::Class::QueryLog::Transaction undef
DBIx::Class::Relationship undef
DBIx::Class::Relationship::Base undef
DBIx::Class::Relationship::BelongsTo undef
DBIx::Class::ResultClass::HashRefInflator undef
MooseX::Iterator
DBIx::Class::ResultSet::Faceter::Facet undef
DBIx::Class::ResultSet::Faceter::Facet::CodeRef undef
DBIx::Class::ResultSet::Faceter::Facet::Column undef
DBIx::Class::ResultSet::Faceter::Facet::HashRef undef
DBIx::Class::Result::Validation
DBIx::Class::Result::ColumnData
DBIx::Class::ResultSet::Faceter::Result undef
DBIx::Class::ResultSet::Faceter::Types undef
DBIx::Class::ResultSet::Graph 1.03
DBIx::Class::ResultSet::I18NColumns undef
DBIx::Class::ResultSet::ProxyField undef
DBIx::Class::ResultSetColumn undef
DBIx::Class::ResultSetManager undef
DBIx::Class::ResultSource undef
DBIx::Class::ResultSource::Table undef
DBIx::Class::ResultSource::View undef
DBIx::Class::SQLMaker undef
DBIx::Class::ResultSourceHandle undef
DBIx::Class::Row undef
DBIx::Class::SQLMaker::LimitDialects undef
DBIx::Class::Schema::Journal 0.01
DBIx::Class::Schema::Journal::DB undef
DBIx::Class::Schema::Journal::DB::AuditHistory undef
DBIx::Class::Schema::Journal::DB::AuditLog undef
DBIx::Class::Schema::Journal::DB::ChangeLog undef
DBIx::Class::Schema::Journal::DB::ChangeSet undef
DBIx::Class::Schema::KiokuDB 1.19
DBIx::Class::Schema::Loader::Base 0.07010
DBIx::Class::Schema::Loader::DBI 0.07010
DBIx::Class::Schema::Loader::DBI::ADO 0.07010
DBIx::Class::Schema::Loader::DBI::ADO::MS_Jet 0.07010
DBIx::Class::Schema::Loader::DBI::ADO::Microsoft_SQL_Server 0.07010
DBIx::Class::Schema::Loader::DBI::Component::QuotedDefault 0.07010
DBIx::Class::Schema::Loader::DBI::DB2 0.07010
DBIx::Class::Schema::Loader::DBI::Informix 0.07010
DBIx::Class::Schema::Loader::DBI::InterBase 0.07010
DBIx::Class::Schema::Loader::DBI::MSSQL 0.07010
DBIx::Class::Schema::Loader::DBI::ODBC 0.07010
DBIx::Class::Schema::Loader::DBI::ODBC::ACCESS 0.07010
DBIx::Class::Schema::Loader::DBI::ODBC::Firebird 0.07010
DBIx::Class::Schema::Loader::DBI::ODBC::Microsoft_SQL_Server 0.07010
DBIx::Class::Schema::Loader::DBI::ODBC::SQL_Anywhere 0.07010
DBIx::Class::Schema::Loader::DBI::Oracle 0.07010
DBIx::Class::Schema::Loader::DBI::Pg 0.07010
DBIx::Class::Schema::Loader::DBI::SQLAnywhere 0.07010
DBIx::Class::Schema::Loader::DBI::SQLite 0.07010
DBIx::Class::Schema::Loader::DBI::Sybase 0.07010
DBIx::Class::Schema::Loader::DBI::Sybase::Common 0.07010
DBIx::Class::Schema::Loader::DBI::Sybase::Microsoft_SQL_Server 0.07010
DBIx::Class::Schema::Loader::DBI::Writing 0.07010
DBIx::Class::Schema::Loader::DBI::mysql 0.07010
DBIx::Class::Schema::Loader::Optional::Dependencies undef
DBIx::Class::Schema::Loader::RelBuilder 0.07010
DBIx::Class::Schema::Loader::RelBuilder::Compat::v0_040 0.07010
DBIx::Class::Schema::Loader::RelBuilder::Compat::v0_05 0.07010
DBIx::Class::Schema::Loader::RelBuilder::Compat::v0_06 0.07010
DBIx::Class::Schema::PopulateMore::Command undef
DBIx::Class::Schema::PopulateMore::Inflator undef
DBIx::Class::Schema::PopulateMore::Inflator::Date undef
DBIx::Class::Schema::PopulateMore::Inflator::Env undef
DBIx::Class::Schema::PopulateMore::Inflator::Find undef
DBIx::Class::Schema::Versioned undef
DBIx::Class::Schema::PopulateMore::Inflator::Index undef
DBIx::Class::Schema::PopulateMore::Visitor undef
DBIx::Class::Serialize::Storable undef
DBIx::Class::StartupCheck undef
DBIx::Class::Storage undef
DBIx::Class::Storage::DBI undef
DBIx::Class::Storage::DBI::ACCESS undef
DBIx::Class::Storage::DBI::ADO::MS_Jet undef
DBIx::Class::Storage::DBI::ADO::MS_Jet::Cursor undef
DBIx::Class::Storage::DBI::ADO::Microsoft_SQL_Server undef
DBIx::Class::Storage::DBI::AutoCast undef
DBIx::Class::Storage::DBI::Cursor undef
DBIx::Class::Storage::DBI::DB2 undef
DBIx::Class::Storage::DBI::Firebird undef
DBIx::Class::Storage::DBI::Firebird::Common undef
DBIx::Class::Storage::DBI::Informix undef
DBIx::Class::Storage::DBI::InterBase undef
DBIx::Class::Storage::DBI::MSSQL undef
DBIx::Class::Storage::DBI::NoBindVars undef
DBIx::Class::Storage::DBI::ODBC undef
DBIx::Class::Storage::DBI::ODBC::ACCESS undef
DBIx::Class::Storage::DBI::ODBC::DB2_400_SQL undef
DBIx::Class::Storage::DBI::ODBC::Firebird undef
DBIx::Class::Storage::DBI::ODBC::Microsoft_SQL_Server undef
DBIx::Class::Storage::DBI::ODBC::SQL_Anywhere undef
DBIx::Class::Storage::DBI::Oracle undef
DBIx::Class::Storage::DBI::Oracle::Generic undef
DBIx::Class::Storage::DBI::Oracle::WhereJoins undef
DBIx::Class::Storage::DBI::Pg undef
DBIx::Class::Storage::DBI::Replicated undef
DBIx::Class::Storage::DBI::Replicated::Balancer undef
POE::API::Peek
DBIx::Class::Storage::DBI::Replicated::Balancer::First undef
DBIx::Class::Storage::DBI::Replicated::Balancer::Random undef
DBIx::Class::Storage::DBI::Replicated::Pool undef
DBIx::Class::Storage::DBI::Replicated::Replicant undef
DBIx::Class::Storage::DBI::Replicated::WithDSN undef
DBIx::Class::Storage::DBI::SQLAnywhere undef
DBIx::Class::Storage::DBI::SQLAnywhere::Cursor undef
DBIx::Class::Storage::DBI::SQLite undef
DBIx::Class::Storage::DBI::Sybase undef
DBIx::Class::Storage::DBI::Sybase::ASE undef
DBIx::Class::Storage::DBI::Sybase::ASE::NoBindVars undef
DBIx::Class::Storage::DBI::Sybase::FreeTDS undef
DBIx::Class::Storage::DBI::Sybase::MSSQL undef
DBIx::Class::Storage::DBI::Sybase::Microsoft_SQL_Server undef
DBIx::Class::Storage::DBI::mysql undef
DBIx::Class::Storage::DBI::Sybase::Microsoft_SQL_Server::NoBindVars undef
DBIx::Class::Storage::DBI::UniqueIdentifier undef
DBIx::Class::Storage::Statistics undef
DBIx::Class::Storage::TxnScopeGuard undef
DBIx::Class::Tree::AdjacencyList undef
DBIx::Class::Tree::AdjacencyList::Ordered undef
DBIx::Class::UTF8Columns undef
DBIx::Class::UUIDColumns::UUIDMaker undef
DBIx::Class::UUIDColumns::UUIDMaker::APR::UUID undef
DBIx::Class::UUIDColumns::UUIDMaker::Data::GUID undef
Module::Collect
XML::Tidy
SVG::TT::Graph
Time::Duration::Parse::AsHash
DBIx::Class::UUIDColumns::UUIDMaker::Data::UUID undef
DBIx::Class::UUIDColumns::UUIDMaker::Data::Uniqid undef
DBIx::Class::UUIDColumns::UUIDMaker::UUID undef
DBIx::Class::UUIDColumns::UUIDMaker::UUID::Random undef
DBIx::Class::UUIDColumns::UUIDMaker::Win32::Guidgen undef
DBIx::Class::UUIDColumns::UUIDMaker::Win32API::GUID undef
DBIx::Connector::Driver 0.44
DBIx::Connector::Driver::MSSQL 0.44
DBIx::Connector::Driver::Oracle 0.44
DBIx::Connector::Driver::Pg 0.44
DBIx::Connector::Driver::SQLite 0.44
DBIx::Connector::Driver::mysql 0.44
DBIx::DBH::Legacy 0.2
DBIx::DBH::Pg undef
DBIx::DBH::SQLite undef
DBIx::DBH::Sybase undef
DBIx::DBH::mysql undef
DBIx::DBHResolver::Strategy 0.01
DBIx::DBHResolver::Strategy::Key 0.01
DBIx::DBHResolver::Strategy::List 0.01
DBIx::DBHResolver::Strategy::Range 0.01
DBIx::DBHResolver::Strategy::Remainder 0.10
DBD::Oracle
DBIx::DBO
DBIx::DBSchema::Column 0.14
DBIx::DBSchema::DBD 0.07
DBIx::DBSchema::DBD::Oracle 0.01
DBIx::DBSchema::DBD::Pg 0.17
Log::Any::Adapter::Dispatch
DBIx::ObjectMapper::Relation undef
DBIx::ObjectMapper::Relation::BelongsTo undef
DBIx::ObjectMapper::Relation::HasMany undef
DBIx::ObjectMapper::Relation::HasOne undef
DBIx::ObjectMapper::Relation::ManyToMany undef
DBIx::ObjectMapper::SQL undef
DBIx::ObjectMapper::SQL::Base undef
DBIx::ObjectMapper::SQL::Delete undef
DBIx::ObjectMapper::SQL::Insert undef
DBIx::ObjectMapper::SQL::Select undef
DBIx::ObjectMapper::SQL::Set undef
DBIx::ObjectMapper::SQL::Update undef
DBIx::ObjectMapper::Session undef
DBIx::ObjectMapper::Session::Array undef
DBIx::ObjectMapper::Session::Cache undef
DBIx::ObjectMapper::Session::ObjectChangeChecker undef
DBIx::ObjectMapper::Session::Search undef
DBIx::ObjectMapper::Session::UnitOfWork undef
DBIx::ObjectMapper::Utils undef
XML::SimpleObject
DBIx::QueryByName::DbhPool undef
DBIx::QueryByName::FromXML undef
DBIx::QueryByName::Logger undef
DBIx::QueryByName::QueryPool undef
DBIx::QueryByName::Result::HashIterator undef
DBIx::QueryByName::Result::Iterator undef
DBIx::QueryByName::Result::ScalarIterator undef
DBIx::QueryByName::SthPool undef
DBIx::ResultSet::Connector 0.14
Timer::Simple
Set::DynamicGroups
Sub::Chain::Group
DBIx::RoboQuery::ResultSet 0.012025
DBIx::RoboQuery::Util 0.012025
Class::MakeMethods
DBIx::AnyDBD
DBIx::SQLEngine::Cache::BasicCache undef
DBIx::SQLEngine::Cache::TrivialCache undef
DBIx::SQLEngine::Criteria undef
DBIx::SQLEngine::Criteria::And undef
DBIx::SQLEngine::Criteria::Comparison undef
DBIx::SQLEngine::Criteria::Compound undef
DBIx::SQLEngine::Criteria::Equality undef
DBIx::SQLEngine::Criteria::Greater undef
DBIx::SQLEngine::Criteria::HashGroup undef
DBIx::SQLEngine::Criteria::Lesser undef
DBIx::SQLEngine::Criteria::Like undef
DBIx::SQLEngine::Criteria::LiteralSQL undef
DBIx::SQLEngine::Criteria::Not undef
DBIx::SQLEngine::Criteria::Or undef
DBIx::SQLEngine::Criteria::StringComparison undef
DBIx::SQLEngine::Driver undef
DBIx::SQLEngine::Driver::AnyData undef
DBIx::SQLEngine::Driver::CSV undef
DBIx::SQLEngine::Driver::Informix undef
DBIx::SQLEngine::Driver::MSSQL undef
DBIx::SQLEngine::Driver::Mysql undef
DBIx::SQLEngine::Driver::NullP undef
DBIx::SQLEngine::Driver::Oracle undef
DBIx::SQLEngine::Driver::Pg undef
DBIx::SQLEngine::Driver::SQLite undef
DBIx::SQLEngine::Driver::Sybase undef
DBIx::SQLEngine::Driver::Sybase::MSSQL undef
DBIx::SQLEngine::Driver::Trait::DatabaseFlavors undef
DBIx::SQLEngine::Driver::Trait::NoAdvancedFeatures undef
DBIx::SQLEngine::Driver::Trait::NoColumnTypes undef
DBIx::SQLEngine::Driver::Trait::NoComplexJoins undef
DBIx::SQLEngine::Driver::Trait::NoJoins undef
DBIx::SQLEngine::Driver::Trait::NoLimit undef
DBIx::SQLEngine::Driver::Trait::NoPlaceholders undef
DBIx::SQLEngine::Driver::Trait::NoSequences undef
DBIx::SQLEngine::Driver::Trait::NoUnions undef
DBIx::SQLEngine::Driver::Trait::PerlDBLib undef
DBIx::SQLEngine::Driver::XBase undef
DBIx::SQLEngine::Record::Accessors undef
DBIx::SQLEngine::Record::Base undef
DBIx::SQLEngine::Record::Cache undef
DBIx::SQLEngine::Record::Class undef
DBIx::SQLEngine::Record::Extras undef
DBIx::SQLEngine::Record::Hooks undef
DBIx::SQLEngine::Record::PKey undef
DBIx::SQLEngine::Record::Table undef
DBIx::SQLEngine::RecordSet::NextPrev undef
DBIx::SQLEngine::RecordSet::PKeySet undef
DBIx::SQLEngine::RecordSet::Set undef
DBIx::SQLEngine::Schema::Column undef
DBIx::SQLEngine::Schema::ColumnSet undef
DBIx::SQLEngine::Schema::Table undef
DBIx::SQLEngine::Schema::TableSet undef
DBIx::SQLEngine::Utility::CloneWithParams undef
DBIx::SQLite::Simple::Table undef
Cache::Simple::TimedExpiry
Class::ReturnValue
DBIx::SearchBuilder::Handle undef
DBIx::SearchBuilder::Handle::Informix undef
DBIx::SearchBuilder::Handle::ODBC undef
DBIx::SearchBuilder::Handle::Oracle undef
DBIx::SearchBuilder::Handle::Pg undef
DBIx::SearchBuilder::Handle::SQLite undef
DBIx::SearchBuilder::Handle::Sybase undef
DBIx::SearchBuilder::Handle::mysql undef
DBIx::SearchBuilder::Handle::mysqlPP undef
DBIx::SearchBuilder::Record undef
DBIx::SearchBuilder::Record::Cachable undef
DBIx::SearchBuilder::SchemaGenerator undef
DBIx::SearchBuilder::Unique 0.01
DBIx::Simple::Result::RowObject undef
DBIx::SimpleQuery::Object undef
DBIx::Skinny::DBD undef
DBIx::Skinny::DBD::Base undef
DBIx::Skinny::DBD::Oracle undef
DBIx::Skinny::DBD::Pg undef
DBIx::Skinny::DBD::SQLite undef
DBIx::Skinny::DBD::mysql undef
DBIx::Skinny::Iterator undef
DBIx::Skinny::Mixin undef
DBIx::Skinny::Profiler undef
DBIx::Skinny::Profiler::Trace undef
DBIx::Skinny::Row undef
DBIx::Skinny::SQL undef
DBIx::Skinny::SQL::Oracle undef
DBIx::Skinny
DBIx::Skinny::Schema::Loader::DBI undef
DBIx::Skinny::Schema::Loader::DBI::Pg undef
DBIx::Skinny::Schema::Loader::DBI::SQLite undef
DBIx::Skinny::Schema::Loader::DBI::mysql undef
DBIx::Skinny::Util undef
Data::Validator
SQL::Maker::SQLType
SQL::NamedPlaceholder
DBIx::Sunny::Schema undef
DBIx::TextIndex::DBD undef
DBIx::TextIndex::DBD::Pg 0.26
DBIx::TextIndex::DBD::SQLite 0.26
DBIx::TextIndex::DBD::mysql 0.26
DBIx::TextIndex::Exception 0.26
DBIx::TextIndex::QueryParser 0.26
DBIx::TextIndex::StopList::cz undef
DBIx::TextIndex::StopList::en undef
DBIx::TextIndex::TermDocsCache 0.26
DBM::Deep::Array undef
Error::Hierarchy::Container
DBM::Deep::Engine undef
Badger 0.06
DBM::Deep::Engine::DBI undef
DBM::Deep::Engine::File undef
DBM::Deep::Hash undef
DBM::Deep::Iterator undef
DBM::Deep::Iterator::DBI undef
DBM::Deep::Iterator::File undef
B::Lint 1.12
DBM::Deep::Iterator::File::BucketList undef
DBM::Deep::Iterator::File::Index undef
DBM::Deep::Null undef
DBM::Deep::Sector undef
DBM::Deep::Sector::DBI undef
DBM::Deep::Sector::DBI::Reference undef
DBM::Deep::Sector::DBI::Scalar undef
DBM::Deep::Sector::File undef
DBM::Deep::Sector::File::BucketList undef
DBM::Deep::Sector::File::Data undef
DBM::Deep::Sector::File::Index undef
DBM::Deep::Sector::File::Null undef
DBM::Deep::Sector::File::Reference undef
DBM::Deep::Sector::File::Scalar undef
DBM::Deep::Storage undef
DBM::Deep::Storage::DBI undef
DBM::Deep::Storage::File undef
DBM_Filter 0.03
DBM_Filter::compress 0.02
DBM_Filter::encode 0.02
DBM_Filter::int32 0.02
DBM_Filter::null 0.02
DBM_Filter::utf8 0.02
DBR::Admin undef
DBR::Admin::Exception undef
DBR::Admin::Utility undef
Data::Properties::YAML
Data::Properties::JSON
ASP4
DBR::Admin::Window 1
DBR::Admin::Window::EnumList 1
Bootylicious::Plugin::AjaxLibLoader 0.05
DBR::Admin::Window::FieldList 1
DBR::Admin::Window::FieldRelationshipList 1
DBR::Admin::Window::InstanceList 1
DBR::Admin::Window::MainMenu 1
DBR::Admin::Window::RelationshipList 1
DBR::Admin::Window::SchemaList 1
DBR::Admin::Window::TableList 1
DBR::Common undef
DBR::Config undef
DBR::Config::Field undef
DBR::Config::Field::Anon undef
Pod::MinimumVersion
Perl::Critic::Utils::PPI
Perl::Critic::Pulp
DBR::Config::Field::Common undef
DBR::Config::Instance undef
DBR::Config::MetaSpec undef
DBR::Config::Relation undef
DBR::Config::ScanDB undef
DBR::Config::Schema undef
DBR::Config::Scope undef
DBR::Config::SpecLoader undef
DBR::Config::Table undef
DBR::Config::Table::Anon undef
DBR::Config::Table::Common undef
DBR::Config::Trans undef
DBR::Config::Trans::Dollars undef
DBR::Config::Trans::Enum undef
DBR::Config::Trans::Percent undef
DBR::Config::Trans::UnixTime undef
DBR::Handle undef
DBR::Interface::DBRv1 undef
Text::Quote 0.3
DBR::Interface::Object undef
DBR::Manual undef
DBR::Misc::Connection undef
DBR::Misc::Connection::Mysql undef
DBR::Misc::Connection::SQLite undef
DBR::Misc::Session undef
DBR::Query undef
DBR::Query::Part undef
DBR::Query::Part::AndOr undef
XML::Atom::Service
Devel::UseAnyFunc
DBR::Query::Part::Compare undef
Atompub 0.3.4
DBR::Query::Part::Join undef
DBR::Query::Part::Set undef
DBR::Query::Part::Subquery undef
DBR::Query::Part::Value undef
DBR::Util::Logger undef
DBR::Util::Operator undef
DEBUG undef
DFA::Generate 2.01
DNS::Oterica::App 0.100001
DNS::Oterica::Hub 0.100001
DNS::Oterica::Location 0.100001
DNS::Oterica::Node 0.100001
DNS::Oterica::Node::Domain 0.100001
DNS::Oterica::Node::Host 0.100001
DNS::Oterica::NodeFamily 0.100001
DNS::Oterica::RecordMaker::Diagnostic 0.100001
DBIx::DBSchema::DBD::SQLite 0.02
DBIx::DBSchema::DBD::Sybase 0.03
DBIx::DBSchema::DBD::mysql 0.08
DBIx::DBSchema::Index 0.1
DBIx::DBSchema::Table 0.08
DBIx::DBSchema::_util undef
SQL::Abstract::More
Hash::Type
File::Tabular
List::Categorize
DBIx::DataModel::Schema undef
DBIx::DataModel::Schema::Generator undef
DBIx::DataModel::Source undef
DBIx::DataModel::Statement undef
DBIx::DataModel::Statement::JDBC undef
DBIx::FileStore::ConfigFile undef
DBIx::FileStore::UtilityFunctions undef
DBIx::Inspector::Column undef
DBIx::Inspector::Driver::Base undef
DBIx::Inspector::Driver::Pg undef
DBIx::Inspector::Driver::SQLite undef
DBIx::Inspector::Driver::mysql undef
DBIx::Inspector::ForeignKey undef
DBIx::Inspector::ForeignKey::Pg undef
DBIx::Inspector::Iterator undef
DBIx::Inspector::Iterator::Null undef
DBIx::Inspector::Table undef
DBIx::Interp 1.10
DBIx::Log4perl::Constants undef
DBIx::Log4perl::db undef
DBIx::Log4perl::st undef
DBIx::Migration::Classes::Change undef
DBIx::NoSQL::Class undef
DBIx::NoSQL::ClassScaffold undef
DBIx::NoSQL::Entity undef
DBIx::NoSQL::Model undef
DBIx::NoSQL::Model::Field undef
DBIx::NoSQL::Model::Index undef
DBIx::NoSQL::Search undef
DBIx::NoSQL::Stash undef
DBIx::NoSQL::Storage undef
DBIx::NoSQL::Store undef
DBIx::NoSQL::TypeMap undef
DBIx::NoSQLite 0.0017
DBIx::ObjectMapper::Engine undef
DBIx::ObjectMapper::Engine::DBI undef
DBIx::ObjectMapper::Engine::DBI::Driver undef
DBIx::ObjectMapper::Engine::DBI::Driver::SQLite undef
DBIx::ObjectMapper::Engine::DBI::Driver::mysql undef
DBIx::ObjectMapper::Engine::DBI::Iterator undef
DBIx::ObjectMapper::Engine::DBI::Transaction undef
DBIx::ObjectMapper::Iterator undef
DBIx::ObjectMapper::Iterator::Base undef
DBIx::ObjectMapper::Log undef
DBIx::ObjectMapper::Mapper undef
DBIx::ObjectMapper::Mapper::Accessor undef
DBIx::ObjectMapper::Mapper::Attribute undef
DBIx::ObjectMapper::Mapper::Attribute::Array undef
DBIx::ObjectMapper::Mapper::Attribute::Hash undef
DBIx::ObjectMapper::Mapper::Attribute::Property undef
DBIx::ObjectMapper::Mapper::Constructor undef
DBIx::ObjectMapper::Mapper::Instance undef
Attribute::Lexical 0.003
DBIx::ObjectMapper::Metadata undef
DBIx::ObjectMapper::Metadata::Declare undef
DBIx::ObjectMapper::Metadata::Polymorphic undef
Captcha::reCAPTCHA 0.93
DBIx::ObjectMapper::Metadata::Query undef
DBIx::ObjectMapper::Metadata::Sugar undef
DBIx::ObjectMapper::Metadata::Table undef
DBIx::ObjectMapper::Metadata::Table::Column undef
DBIx::ObjectMapper::Metadata::Table::Column::Base undef
DBIx::ObjectMapper::Metadata::Table::Column::Connect undef
DBIx::ObjectMapper::Metadata::Table::Column::Desc undef
DBIx::ObjectMapper::Metadata::Table::Column::Func undef
DBIx::ObjectMapper::Metadata::Table::Column::Type undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::Array undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::BigInt undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::Binary undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::Bit undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::Boolean undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::Date undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::Datetime undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::Float undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::Int undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::Interval undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::Mush undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::Numeric undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::SmallInt undef
DBIx::ObjectMapper::Metadata::Table::Column::Type::String undef
Carp::Diagnostics 0.05
DBIx::ObjectMapper::Metadata::Table::Column::Type::Text undef
AAAA::Crypt::DH 0.04
AC::DC 1
AC::MrGamoo 1
AC::Yenta 1
ACH::Builder 0.09
ACME::PM::Voronezh 0.02
AI::MegaHAL 0.07
AI::NaiveBayes1 2.006
AI::Pathfinding::AStar::Rectangle 0.23
Hash::AsObject
AI::Prolog 0.741
AIX::LVM 1.1
AMF::Perl 0.15
Storable::AMF
AMF::Connection 0.21
API::CPanel 0.09
API::Plesk 2.00_1
ARSObject 0.54
ASP4::PSGI 0.002
Router::Generic
Apache2::RequestRec
ASP4x::Router 0.020
Abilities 0.2
AUBBC 4.05
Advent::Bundles 0.05
Aspect 0.96
B::Hooks::Parser 0.09
Archlinux::Term 0.03
TheSchwartz
MojoX::Session
Parse::Debian::PackageDesc
Migraine
Arepa 0.81
WWW::Facebook::API
Catalyst::Plugin::Facebook 0.2
Argv 1.26
Asm::X86 0.10
Aspect::Library::Timer 1.05
Aspect::Loader 0.03
Astro::FITS::CFITSIO
Tk::Zinc
Astro::FITS::Header
Starlink::AST
Astro::SIMBAD::Client 0.019
Astro::SpaceTrack 0.051
Astro::XSPEC::Model::Parse 0.01
Async::Defer 0.9.0
Async::MergePoint 0.03
Atheme 0.0001
Dancer::Plugin
Devel::DProf
Dancer::Test
DBICx::Sugar
Dancer::Plugin::DBIC
AtomBus 1.0404
Attribute::Constant 0.02
Attribute::Final 1.3
Attribute::Handlers 0.88
Attribute::Method::Tags 0.11
Attribute::Signature 1.10
Attribute::SubName 1.101420
CPAN::Testers::Metabase 1.999001
Attribute::Util 1.06
Math::FFT
Audio::Analyzer 0.21
Getopt::Euclid
MooseX::SemiAffordanceAccessor
Audio::MPD::Common::Item
Audio::MPD::Common::Output
Audio::MPD::Common::Stats
Audio::MPD::Common::Status
Test::Corpus::Audio::MPD
Audio::MPD 1.111200
Audio::Audacious undef
Audio::Beep 0.11
Audio::FLAC::Header 2.4
Audio::FindChunks 2.00
Audio::MPD::Common 1.110550
Audio::Metadata 0.15
Audio::Scan 0.87
Audio::Musepack 1.0.1
AnyEvent::TermKey
File::Copy::Link
Text::Format
Audio::Nama 1.073
Audio::RPLD 0.003
Audio::Wav 0.12
Auth::Yubikey_WebClient 2.00
Authen::CAS::External 0.06
Authen::Bitcard 0.89
IO::LockedFile
Authen::Htpasswd 0.161
Authen::Libwrap 0.22
Authen::Simple::Atheme 0.3
Authen::Smb 0.96
Authen::Users 0.17
AutoLoader 5.71
AutoRole 0.03
AutoSession 0.01
CPANDB 0.14
Autocache 0.004
B::C 1.27
B::Foreach::Iterator 0.07
B::Lint::StrictOO 0.04
B::OPCheck 0.29
B::RecDeparse 0.05
BBC::Radio::ProgrammesSchedules 0.08
BackPAN::Index 0.40
BDB::Wrapper 0.42
BSD::Resource 1.2904
Device::SerialPort
RingBuffer
BT368i 1.00
Barcode::Code93 0.02
Module::Extract::VERSION
BackPAN::Version::Discover 0.01
OAuth::Lite
Bench 0.02
Devel::Platform::Info
Math::GMPz
Math::Primality
Text::Template::Simple
Sys::Info::Base
Unix::Processors
Sys::Info::Driver::Linux
Sys::Info
Math::MatrixReal
Data::YAML::Reader
Data::YAML::Writer
Benchmark::Perl::Formance 0.22
Benchmark::Perl::Formance::Cargo 0.02
Benchmark::Serialize 0.08
Benchmark::Timer 0.7102
BerkeleyDB::Manager 0.12
Best 0.12
LaTeX::ToUnicode
BibTeX::Parser 0.63
Text::LevenshteinXS
Text::Names
Biblio::Citation::Compare 0.01
Biblio::Refbase 0.04
Biblio::Thesaurus 0.37
Bing::Search 0.0003
Math::NumberCruncher
Bio::AGP::LowLevel undef
Bio::BMI 1.0
Text::Sass
Bio::FdrFet 0.04
File::Tail
Bio::LITE::Taxonomy 0.06
Brickyard 1.111110
Bio::LITE::Taxonomy::NCBI 0.06
Bio::LITE::Taxonomy::NCBI::Gi2taxid 0.03
Bio::LITE::Taxonomy::RDP 0.02
Bio::MAGETAB 1.2
Bio::Phylo 0.36
Bio::Protease 1.102690
Statistics::Distributions
Bio::SDRS 0.02
Bit::Grep 0.01
Bit::Util 0.01
Block::NamedVar 0.007
Blog::Simple::HTMLOnly 0.05
BlueCoat::SGOS 0.94
Bootylicious::Plugin::TocJquery 0.06
Bot::Training::MegaHAL 0.02
Bot::Training::StarCraft 0.02
Tk::NumEntry
Tk::Widget
Tk::FireButton
Bot::Training 0.04
Bot::BasicBot::Pluggable 0.91
IO::SessionData
XML::Parser::Lite
DIME::Tools
Apache
SOAP::Lite
JIRA::Client
Bot::BasicBot::Pluggable::Module::JIRA 0.02
RT::Client::REST
RT::Client::REST::Ticket
Bot::BasicBot::Pluggable::Module::RT 0.20
Bot::BasicBot::Pluggable::Module::XKCD 0.06
Sys::Prctl
Net::Twitter::Lite
Term::Sk
File::CountLines
Test::Script::Run
Pod::Abstract
Pod::Section
Hailo
Bot::Twatterhose 0.04
Bracket 1.04
Bread::Board 0.18
Bread::Board::Declare 0.06
Brackup 1.10
Buffer::Transactional 0.02
Business::AU::Ledger 0.88
Business::AuthorizeNet::CIM 0.04
Business::CA::GST 0.02
Class::Roles
Test::Role
Business::CardInfo 0.10
Business::Cart::Generic 0.80
Business::DK::CPR 0.07
Business::DK::FI 0.02
Business::DK::PO 0.05
Business::EDI 0.05
Business::FedEx::RateRequest 0.95
Proc::ChildError
IPC::System::Options
Data::ModeMerge
Data::Sah::Normalize
Data::Sah::Resolve
Nodejs::Util
String::Wildcard::Bash
PERLANCAR::Module::List
subroutines
Data::Sah::Coerce
Data::Sah::CoerceCommon
Lingua::EN::Numbers::Ordinate
File::ShareDir::Tarball
Sah::SpecTest
Module::Installed::Tiny
Scalar::Util::Numeric::PP
Text::sprintfn
Regexp::Grammars
Language::Expr
Language::Expr::Interpreter::var_enumer
Data::Sah::Util::Type::Date
String::LineNumber
String::PerlQuote
Data::Clean
Data::Clean::FromJSON
Sah::Schema::rinci::function_meta
Perinci::Sub::Normalize
YAML::Old
Data::Sah::Util::Type
Getopt::Long::Negate::EN
Getopt::Long::Util
Perinci::Sub::GetArgs::Array
Data::Clean::JSON
Perinci::Sub::GetArgs::Argv
Perl::osnames
Sub::Iterator
Perinci::Sub::Wrapper
Test::Perinci::Sub::Wrapper
Locale::Messages
Locale::TextDomain::UTF8
Perinci::Object::Metadata
Perinci::Sub::PropertyUtil
Perinci::Sub::Property::result::table
Locale::Set
Perinci::Sub::Gen
Perinci::Sub::Gen::AccessTable
Locale::ID::Locality
Locale::ID::Province
Business::ID::NIK 0.03
Business::IS::PIN 0.06
Business::KontoCheck 3.6
Business::LiveDrive 0.01
Business::OnlinePayment::CardFortress 0.02
Business::OnlinePayment::IPPay 0.06
Business::OnlinePayment::Iridium 0.11
Business::OnlinePayment::Litle 0.901
Business::OnlinePayment::PaymenTech 2.04
Business::OnlinePayment::SynapseGateway 0.01
Business::OnlinePayment::eSelectPlus 0.03
Business::PayPal 0.04
Business::PayPal::NVP 1.06
Business::PayflowPro::Reporting 0.01
Business::RO::CNP 0.03
Business::Shipping 3.0.0
Business::TNTPost::NL 0.11
Pod::Xhtml
Syntax::Highlight::Perl::Improved
CGI::Application::Plugin::ViewCode 1.02
Business::Tax::VAT::Validation 0.20
Business::Tax::VAT::Validation::DE 0.1
CPAN::Mini::Devel::Recent 0.06
CPAN::Mini::Extract 1.21
CPAN::ParseDistribution 1.3
CPAN::Mini::Inject 0.27_03
Number::Bytes::Human
CPAN::Mirror::Server::HTTP 0.02
CPAN::PackageDetails 0.25_05
CPAN::Recent::Uploads 0.02
Term::Title
CPAN::Reporter::Smoker 0.22
CPAN::Reporter::Smoker::Safer 0.04
CPAN::Testers 0.03
CPAN::Testers::Common::Article 0.42
CPAN::Testers::Common::Utils 0.002
CPAN::Testers::Config 0.001
CPAN::Testers::Data::Addresses 0.06
CPAN::Testers::Data::Release 0.03
CPAN::Testers::Data::Uploads 0.17
CPAN::Testers::Data::Uploads::Mailer 0.03
CPAN::Testers::Report 1.999001
CPAN::Testers::WWW::Statistics 0.88
Google::Chart
CPANPLUS 0.9010
CPAN::Visitor 0.002
Module::Depends::Intrusive
CPAN::Unwind 0.06
HTML::Spry::DataSet
CPAN::WWW::Top100::Generator 0.10
CPAN::YACSmoke 0.03
CPANPLUS::Dist::Arch 1.12
CPANPLUS::Dist::Gentoo 0.11
CPANPLUS::Dist::Mageia 1.110471
CPANPLUS::Internals::Source::CPANIDX 0.04
CPANPLUS::Internals::Source::CPANMetaDB 0.04
CPANPLUS::YACSmoke 0.62
LEOCHARRE::Dir
CSS::DOM 0.14
Net::SMTP_auth
Net::Address::IP::Local
Net::SNMP
CPM 1.51
CPS 0.11
CPU::Emulator::Memory 1.1002
CPU::Z80::Disassembler 0.04
CSS 1.09
HTML::Query
CSS::Inliner 3042
Catalyst::Authentication::Store::LDAP 1.012
CSS::Minifier::XS 0.08
CSS::Packer 1.001_001
CSS::LESSp 0.85
CSS::Simple 3044
Cache::FastMmap 1.36
Catalyst::Plugin::Session::PerUser 0.05
Catalyst::Plugin::Session::Store::FastMmap 0.14
CSS::Squish 0.10
CVS::Metrics 0.18
Cache 2.04
Cache::KyotoTycoon 0.12
Cache::KyotoTycoon::REST 0.03
Cache::Memcached 1.29
Cache::Memcached::Fast 0.19
Test::Memcached
Cache::Memcached::AnyEvent 0.00020
Cache::Memcached::Fast::CGI 0.06
Cache::Memcached::Mock 0.06
Cache::Memcached::Tie 0.09
Cache::Memcached::libmemcached 0.03001
Tree::R
Cache::Range 0.01
Term::ANSIColor::Markup
Calendar::Plugin::Renderer
Date::Exception
Date::Utils
Astro::Utils
Date::Bahai::Simple
Calendar::Bahai 0.06
Date::Hijri::Simple
Calendar::Hijri 0.02
Date::Persian::Simple
Calendar::Persian 0.07
Catalyst::Model::LDAP 0.17
Date::Saka::Simple
Calendar::Saka 0.09
Calendar::Simple 1.21
Calendar::Schedule 1.06
Captcha::Peoplesign 0.00005
Captcha::reCAPTCHA::Mailhide 0.94
Capture::SystemIO 0.01
Capture::Tiny 0.10
Capture::Tiny::Extended 0.102
Carp::Always::Color 0.04
Catalyst::TraitFor::Request::BrowserDetect 0.02
Carp::POE 0.09
Carp::Source 1.101420
Thrift
Cassandra::Lite 0.0.4
Catalyst::Action::Serialize::SimpleExcel 0.015
Catalyst::Plugin::Cache 0.10
Catalyst::ActionRole::BuildDBICResult 0.01
Catalyst::ActionRole::ExpiresHeader 0.01
Catalyst::ActionRole::MatchRequestMethod 0.03
Catalyst::ActionRole::NotCacheableHeaders 0.02
Facebook::Graph
Catalyst::Authentication::Credential::Facebook::OAuth2 0.02
Catalyst::Plugin::FormValidator 0.094
Catalyst::Authentication::Credential::OAuth 0.03
Net::OpenID::Common
Net::OpenID::Consumer
Catalyst::Engine::HTTP
LWPx::ParanoidAgent
Catalyst::Authentication::Credential::OpenID 0.16_02
Catalyst::Authentication::Credential::RPX 0.10053905
Catalyst::Authentication::Credential::RemoteHTTP 0.04
Catalyst::Authentication::Credential::Twitter 0.02001
Catalyst::Authentication::Credential::YubiKey 0.04
Catalyst::Authentication::Store::DBI::ButMaintained 0.01
Catalyst::Authentication::Store::Fey::ORM 0.001
Array::RefElem
Catalyst::Controller::BindLex 0.05
Catalyst::Controller::DirectoryDispatch 0.01
Catalyst::Controller::SOAP 1.22
Catalyst::Controller::WrapCGI 0.030
Catalyst::Engine::Apache 1.16
Catalyst::Engine::Embeddable 0.000003
Catalyst::Engine::HTTP::Prefork 0.51
Catalyst::Engine::PSGI 0.12
Catalyst::Helper::Model::Email 0.04
Catalyst::Log::Log4perl 1.04
Catalyst::Model::Bitcoin 0.02
Catalyst::Model::CouchDB 0.02
Catalyst::Model::Facebook 0.006
Catalyst::Model::KiokuDB 0.12
Catalyst::Model::LDAP::FromAuthentication 0.02
Catalyst::Plugin::Widget 0.04
Catalyst::Model::MongoDB 0.10
Role::REST::Client
Catalyst::Model::REST 0.16
Catalyst::Model::SOAP 1.5
CatalystX::CRUD::YUI 0.025
Catalyst::Model::Sedna 0.004
Catalyst::Model::XML::Feed 0.04
Catalyst::Plugin::AccessLog 1.04
Catalyst::Plugin::Alarm 0.05
Catalyst::Plugin::AuthenCookie 0.07
Catalyst::Plugin::Authorization::Roles 0.08
Catalyst::View::Component::SubInclude 0.10
Catalyst::Plugin::Bread::Board 0.03
Catalyst::Plugin::Browser 0.08
Catalyst::Plugin::C3 0.03
Catalyst::Plugin::Cache::FastMmap 0.9
Catalyst::Plugin::Compress::Bzip2 0.05
Catalyst::Plugin::Compress::Zlib 0.05
Catalyst::Plugin::CustomErrorMessage 0.06
Catalyst::Plugin::DefaultEnd 0.08
Catalyst::Plugin::ErrorCatcher 0.0.8.8
FormValidator::Simple
Catalyst::Plugin::FormValidator::Simple 0.15
Catalyst::Plugin::HTML::Scrubber 0.02
Catalyst::Plugin::HashedCookies 1.110231
Catalyst::Plugin::I18N::PathPrefix 0.03
Catalyst::Plugin::Log4perl::Simple 0.003
Catalyst::Plugin::Log::Dispatch 0.121
Catalyst::Plugin::Log::Handler 0.08
Catalyst::Plugin::Log::Log4perl 0.01
Catalyst::Plugin::Navigation 1.002
Catalyst::Plugin::PageCache 0.31
Encode::ZapCP1252
Catalyst::Plugin::Params::Demoronize 1.14
Catalyst::Plugin::Params::Nested 0.04
Catalyst::Plugin::RedirectAndDetach 0.03
Catalyst::Plugin::RunAfterRequest 0.04
Catalyst::Plugin::Session::AsObject 0.04
Catalyst::Plugin::Session::State::URI 0.15
Catalyst::Plugin::Session::Store::Cache 0.01
Catalyst::Plugin::Session::Store::DBI 0.16
KiokuDB::Backend::BDB::GIN
Catalyst::Plugin::Session::Store::KiokuDB 0.02
Catalyst::Plugin::Session::Store::MongoDB 0.02
Template::Semantic 0.06
Catalyst::Plugin::Setenv 0.03
WWW::Sitemap::XML
Catalyst::Plugin::Sitemap 1.0.0
Class::Trait 0.31
Catalyst::TraitFor::Component::ConfigPerSite 0.03
Catalyst::TraitFor::Model::DBIC::Schema::QueryLog::AdoptPlack 0.04
Tatsumaki 0.1012
Chemistry::Bond::Find 0.23
Chemistry::Canonicalize 0.11
CGI::Application::Plugin::Authentication 0.19
Catalyst::TraitFor::Model::DBIC::Schema::ResultRoles 0.0104
Chemistry::Mol 0.37
Chemistry::Ring 0.20
Catalyst::TraitFor::Model::DBIC::Schema::PerRequestSchema
Catalyst::TraitFor::Model::DBIC::Schema::WithCurrentUser undef
Catalyst::TraitFor::Request::DecodedParams 0.02
I18N::AcceptLanguage
Catalyst::TraitFor::Request::PerLanguageDomains 0.03
Catalyst::View::ByCode 0.13
Catalyst::View::Clevery 0.002
ClearCase::Wrapper 1.16
Catalyst::View::HTML::Mason 0.15
Catalyst::View::Haml 0.01
Catalyst::View::JavaScript::Minifier::XS 2.101000
Catalyst::View::Mason 0.18
Catalyst::View::Mason2 0.02
Catalyst::View::PDF::Reuse 0.04
Catalyst::View::SVGTTGraph 0.02
Catalyst::View::TT::Alloy 0.00003
Catalyst::View::Tenjin 0.042
Catalyst::View::Text::Template 0.003
Catalyst::View::Wkhtmltopdf 0.0003
Catalyst::View::Xslate 0.00012
CatalystX::AppBuilder 0.00008
CatalystX::AuthenCookie 0.02
CatalystX::Controller::Sugar 0.09
CatalystX::Controller::Sugar::ActionPack 0.0401
CatalystX::Declare 0.015
CatalystX::FacebookURI 0.02
CatalystX::Features 0.20
Object::Destroyer
CatalystX::Restarter::GTK 0.04
CatalystX::Routes 0.02
CatalystX::SimpleAPI 0.04
CatalystX::Starter 0.07
CatalystX::UriForStatic 0.01
CatalystX::Widget::Paginator 0.04
CharsetDetector 2.0.2
Chart::Gnuplot 0.15
Chart::Gnuplot::Pie 0.03
Chart::Math::Axis 1.06
Chart::OFC2 0.08_02
Chart::Weather::Forecast 0.04
Check::UnitCheck 0.12
Chat::Envolve 1.0003
Chemistry::File::SMILES 0.47
Math::Assistant
String::CRC::Cksum
Chemistry::Harmonia 0.05
Chess::FIDE 1.10
Chess::PGN::Moves 0.05
Chess::PGN::Parse 0.19
Child::Socket 0.002
String::Palindrome
String::Clean::XSS
Tie::Filehandle::Preempt::Stdin
IO::Any
JSON::Util
Sys::Path
CGI::Info 0.07
Chloro 0.02
Test::CheckChanges 0.14
Text::NeatTemplate
Chooser 2.0.0
Cindy 0.18
Citrix 0.25
Class::Accessor::Fast::Contained 1.01
Class::AutoClass 1.54
Class::Accessor::Chained 0.01
Class::Accessor::Fast::WithBuilder 0.01
Class::Adapter 1.07
Class::Accessor::Fast::XS 0.04
Class::Action 0.4
Class::AutoAccess 0.03
Term::Clui 1.64
Business::UPS::Tracking 1.07
Business::WebMoney 0.11
C::Scan::Constants 1.018
CAM::PDF 1.53
CASCM::Wrapper 0.05
CCCP::Encode 0.03
CDB_File 0.97_01
CCCP::HTML::Truncate 0.04
CGI::Application::Emulate::PSGI 0.02
CGI::Alert 2.03
CGI::Application::Bouquet::Rose 1.05
CGI::Application::Plugin::AutoRunmode 0.17
CGI::Application::Plugin::Config::Perl 1.50
Config::Auto
CGI::Application::Plugin::ConfigAuto 1.32
CGI::Application::Plugin::DBH 4.00
CGI::Application::Plugin::DBIC::Schema 0.3
CGI::Application::Plugin::DebugScreen 1.00
CGI::Easy 1.0.0
CGI::Application::Plugin::DetectAjax 0.06
CGI::Application::Plugin::DevPopup 1.06
CGI::Application::Plugin::Forward 1.06
CGI::Application::Plugin::ErrorPage 1.21
CGI::Application::Plugin::FillInForm 1.15
HTML::Template::Compiled
CGI::Application::Plugin::HTCompiled 1.05
HTML::Template::Plugin::Dot
CGI::Application::Plugin::HTDot 0.07
HTML::Prototype
CGI::Application::Plugin::HTMLPrototype 0.20
Devel::Caller::IgnoreNamespaces
Sub::Prototype
Sub::WrapPackages
CGI::Application::Plugin::LogDispatch 1.02
CGI::Application::Plugin::Redirect 1.00
CGI::Application::Plugin::Output::XSV 1.02
CGI::Application::Plugin::ParsePath 0.01
CGI::Application::Plugin::TT 1.05
CGI::Application::Plugin::Stream 2.10
HTML::SuperForm
CGI::Application::Plugin::SuperForm 0.5
CGI::Application::Plugin::TT::Any 0.110080
Test::POE::Stopping 1.07
CGI::Application::Plugin::ValidateRM 2.3
CGI::Application::Standard::Config 1.01
CGI::Application::Util::Diff 1.03
CGI::Auth::Basic 1.21
CGI::Auth::FOAF_SSL 1.001
CGI::Capture 1.14
CGI::Cookie::Splitter 0.02
CGI::Easy::SendFile 1.0.0
CGI::Easy::URLconf 1.0.0
CGI::Echo 1.08
CGI::Explorer 2.10
CGI::Formalware 1.16
CGI::Minimal 1.29
Data::MultiValuedHash
CGI::MultiValuedHash 1.09
Net::Subnet
Locale::Object::Country
WWW::RT::CPAN
CGI::Lingua 0.15
CGI::ProgressBar 0.05
CGI::Prototype 0.9053
CGI::Session::Driver::dbic 1.01
CGI::Session::Driver::odbc 1.05
CGI::Session::Driver::oracle 1.05
Test::Strict
CGI::Session::Driver::redis 0.2
CGI::Session::ExpireSessions 1.12
CGI::Session::MembersArea 2.06
CGI::Session::Serialize::yaml 4.26
CGI::Simple 1.113
Test::Magpie 0.05
CGI::TabPane 1.08
CGI::Uploader 2.18
CGI::WebToolkit 0.08
CHI::Driver::DBI 1.24
CHI::Driver::Memcached 0.13
Test::Unit 0.25
CLI::Dispatch 0.12
CLI::Framework 0.05
Class::Throwable 0.11
CNC::Cog 0.061
COPS::Client 0.03
CPAN 1.97_51
CPAN::AuthorsSearch 0.05
URI::ToDisk
CPAN::Cache 0.02
CPAN::Changes 0.17
CPAN::DistnameInfo 0.12
CPAN::Inject 1.13
CPAN::Faker 0.007
CPAN::FindDependencies 2.34
Error::Hierarchy::Test
CPAN::Meta 2.110930
CPAN::Meta::YAML 0.003
CPAN::Mini::Devel 0.03
Class::Classless::C3 1.00
Class::Component 0.17
Class::Constant 0.06
Class::Contract 1.14
Class::DBI::AbstractSearch 0.07
Class::DBI::Cascade::Plugin::Nullify 0.05
Class::DBI::ClassGenerator 1.03
Class::DBI::Lite 1.019
Class::DBI::Plugin 0.03
Class::DBI::Plugin::DigestColumns 0.04
Class::Discover 1.000003
Class::DBI::Plugin::Type 0.02
Class::DBI::Schema2Code 1.05
Tree::XPathEngine 0.05
Class::Data::ConfigHash 0.00002
Tie::Hash::Indexed 0.05
Class::DOES 1.00
Class::Date 1.1.10
Crypt::Password
Class::Declarative 0.08
Metabase
Class::Declare 0.17
Class::Declare::Attributes 0.08
Class::Default 1.51
Class::Easy 0.16
Class::Dot 2.0.0_15
Class::Fields 0.204
Class::Generate 1.11
Class::Gomor 1.02
Class::Interfaces 0.04
Class::Injection 1.08
Class::Lego 0.004
Class::Light 0.01003
Class::Sniff 0.08
Class::Lego::Constructor 0.004
Class::Measure 0.04
Class::Member 1.6
Class::Method::Debug 1.101420
Class::Method::Modifiers::Fast 0.041
Class::ModuleByFile 0.01
HTML::Table
Class::Monadic 0.04
HTML::Table::FromDatabase
Class::Observable 1.04
Class::ParamParser 1.041
Class::OOorNO 0.011
Class::Plugin::Util 0.009
Class::Private 0.05
Class::Rebless 0.09
Class::Root 0.02
Class::Scaffold 1.102280
Class::Spiffy 0.15
Class::Tree 1.27
Log::Any::Adapter::TAP
Test::NiceDump
Net::Stomp
Dancer::Plugin::Stomp
Class::Value::Net 1.110250
Class::XPath 1.4
Class::Virtual 0.06
Class::XSAccessor::Compat 0.01
Classic::Perl 0.02
ClearCase::Argv 1.50
ClearCase::ClearPrompt 1.31
ClearCase::SyncTree 0.56
Unix::Syslog 1.1
ClearCase::Wrapper::MGi 0.26
Clone::Any 1.01
Dancer::Plugin::Redis
Dancer::ModuleLoader
Dancer::Plugin::REST
Dancer::Plugin::ProxyPath
Dancer::Plugin::Progress
ClearPress 398
Clone::Closure 0.06
Task::BeLike::Cebjyre 0.1.0
Task::BeLike::SHARYANTO::Devel 0.02
Perl::Critic::Dynamic
Template::Flute 0.0006
Task::Bot::Training 0.02
Math::Random::MT::Perl
MooX::ClassAttribute
Math::Random::MT
Data::SimplePassword
ACL::Lite
Nitesi
Dancer::Plugin::Database::Core
Dancer::Plugin::Database
Test::Database
Nitesi::DBI
Dancer::Plugin::Nitesi
Task::CPANPLUS::Metabase 0.08
Dancer::Plugin::Passphrase
SMS::Send
Dancer::Plugin::SMS
String::Compare::ConstantTime
Session::Storage::Secure
Dancer::Cookie
Dancer::Cookies
Dancer::Session::Abstract
Dancer::Session::Cookie
WWW::LibraryThing::Covers
Dancer::Plugin::LibraryThing
Dancer::Plugin::DebugToolbar
Validate::Tiny
Dancer::Plugin::ValidateTiny
Dancer::FileUtils
Dancer::Plugin::Preprocess::Sass
Dancer::Response
Dancer::Plugin::Auth::Basic
Dancer::Logger::Abstract
Dancer::Logger::Spinner
Dancer::Logger::Log4perl
Dancer::Config
Dancer::Logger
Dancer::Session
Plack::Middleware::Debug::Base
Dancer::Debug
Dancer::Logger::PSGI
Dancer::Logger::Pipe
Dancer::Logger::Syslog
Dancer::Middleware::Rebase
Dancer::Plugin::Auth::Twitter
Dancer::Plugin::Browser
Dancer::SharedData
Dancer::Factory::Hook
Dancer::Plugin::Cache::CHI
Dancer::Plugin::DebugDump
Net::SMTP::SSL
Dancer::Plugin::Email
Dancer::Plugin::FlashMessage
Dancer::Exception
Dancer::Plugin::Feed
Dancer::Plugin::FlashNote
Dancer::Plugin::FormValidator
Dancer::Plugin::FormattedOutput
Dancer::Plugin::Memcached
Dancer::Plugin::MemcachedFast
Dancer::Plugin::MobileDevice
Dancer::Plugin::Mongo
Dancer::Engine
Dancer::Plugin::Params::Normalization
Crypt::Password::Util
File::Flock::Retry
Unix::Passwd::File
Authen::PAM
Authen::Simple::PAM
Dancer::Plugin::Auth::Extensible
Dancer::Plugin::SimpleCRUD
Dancer::Plugin::SiteMap
Dancer::Serializer::Abstract
Dancer::Serializer::UUEncode
Dancer::Session::KiokuDB
Dancer::Session::Memcached
Dancer::Session::MongoDB
Dancer::Session::PSGI
Dancer::Session::Storable
Dancer::Template::Haml
Dancer::Template::Abstract
Dancer::Template::Alloy
Plack::Handler::FCGI
Dancer::Template::Mason
Dancer::Template::HtmlTemplate
Dancer::Template::Mason2
Text::MicroTemplate::File
Dancer::Template::MicroTemplate
Dancer::Template::MojoTemplate
Dancer::Template::TemplateFlute
Template::Sandbox
Dancer::Template::TemplateSandbox
Dancer::Template::Tiny
Dancer::Template::Tenjin
Dancer::App
Dancer::Template::Xslate
Dancer::Logger::ColorConsole
Dancer::Plugin::Async
Dancer::Plugin::Auth::Htpasswd
Dancer::Plugin::Auth::Tiny
Dancer::Plugin::Bcrypt
Dancer::Plugin::DirectoryView
Dancer::Plugin::EncodeID
Dancer::Plugin::EscapeHTML
Dancer::Plugin::Hosts
Dancer::Plugin::NYTProf
Dancer::Plugin::TimeRequests
Dancer::Plugin::XML::RSS
Dancer::Session::CHI
Task::Dancer 0.15
Task::Deprecations::5_14 1.00
Module::Build::Pluggable::XSUtil
Digest::BLAKE2
Digest::BLAKE
Digest::BMW
Digest::ECHO
Digest::EdonR
Digest::GOST
Digest::Fugue
Digest::Groestl
Digest::Hamsi
Digest::JH
Digest::Keccak
Digest::MD6
Digest::Luffa
Digest::Perl::MD4
Digest::SHA::PurePerl
Digest::SHAvite3
Digest::SIMD
Digest::Skein
Digest::Shabal
Digest::Whirlpool
CryptX
Digest::CubeHash::XS
Task::Digest 0.04
XML::LibXML::Debugging
HTML::HTML5::Sanity
HTML::HTML5::Writer
HTML::HTML5::Outline
HTML::HTML5::Builder
XML::LibXML::PrettyPrint
Task::HTML5 0.101
Test::Perl::Critic::Progressive 0.03
Task::Metabase undef
Perl::Critic::Storable
Perl::Critic::Config
Perl::Critic::PetPeeves::JTRAMMELL
Unicode::Map8 0.13
Perl::Critic::Compatibility
File::PathList
Perl::Critic::StricterSubs
Perl::Critic::More
Perl::Critic::Nits
Perl::Critic::Swift
Perl::Critic::Itch
Perl::Critic::Moose
Task::Perl::Critic 1.007
Task::SDK::Moose 0.03
Number::RecordLocator
Net::Jifty
Net::Hiveminder
Module::Install::Repository
Module::Install::AutoManifest
Task::SDK::Perigrin 0.05
Tatsumaki::Template::Markapl 0.3
TeX::DVI 1.01
TeX::Hyphen 1.01
Template::Alloy 1.016
TeX::Hyphen::Pattern 0.04
Unicode::Map
Jcode
Unicode::MapUTF8 1.11
Tee 0.14
Telephony::CountryDialingCodes 1.04
Tempest 2010.09.26
Template::AutoFilter 0.110080
Template::Benchmark 1.09_01
Template::Context::Cacheable 0.02
Template::Directive::XSSAudit 1.03
Template::Extract 0.41
Template::Plugin::CSV 0.04
Template::Flute::Style::CSS 0.0002
Template::Plugin::Cycle 1.06
Template::Parser::CET 0.05
Template::Plugin::Class 0.14
Template::Plugin::DateTime::Format 0.02
Template::Plugin::Devel::StackTrace 0.02
Template::Plugin::FilterVMethods 0.05
Template::Plugin::GoogleLaTeX 0.03
HTML::Strip
Template::Plugin::HTML::Strip 0.01
Template::Plugin::HTML_NonAsc 0.03
Template::Plugin::Haml 0.1.1
Template::Plugin::JavaScript 0.02
Template::Plugin::ListCompare 0.05
Template::Plugin::UTF8Decode 0.01
Template::Plugin::ScalarUtil 1.110920
Pod::Abstract::BuildNode
Template::Plugin::UTF8toANSI 0.01
Template::Plugin::XML::Unescape 0.02
Template::Provider::FromDATA 0.11
Template::Preprocessor::TTML 0.0101
Template::Provider::Encoding 0.10
Template::ShowStartStop v1.0.0
Template::Simple 0.04
Template::Stash::AutoEscaping 0.0301
TemplateM 3.01
Teng 0.10
Term::ANSIColor::Print 0.07
Term::ANSIColorx::ColorNicknames 2.7185
Term::ANSIScreen 1.42
Term::Cap 1.12
Term::ExtendedColor 0.224
Term::Completion 0.91
UR 0.30
Term::ExtendedColor::TTY 0.023
Term::GentooFunctions 1.3605
Test::Assertions 1.054
Term::Highlight 1.2
Term::HiliteDiff 0.10
Term::Menu::Hierarchical 0.95
Term::RawInput 1.14
Term::Shell 0.02
Term::Menus 1.79
Term::Shell::Enhanced 1.101420
Term::ReadPassword 0.11
Term::ReadLine::TTYtter 1.2
Term::StatusBar 1.18
Term::Report 1.18
Term::Shell::MultiCmd 1.07
IO::Easy
Project::Easy
Term::VT102 0.91
Test::HTML::Differences 0.02
Term::VT102::Boundless 0.04
Encode::IMAPUTF7
Protocol::IMAP 0.002
Term::ShellUI 0.9
Term::VT102::Incremental 0.04
Test::Differences::Color 0.05
Test 1.25_02
Test::Able::Runner 1.001
Test::Apache::RewriteRules 1.0.1
Qudo 0.0213
Test::App::CPANIDX::Database 0.04
Test::BinaryData 0.013
Test::Bomb 0.007
Test::Builder::Clutch 0.02
Test::CPAN::Meta::YAML 0.17
Test::Builder::Mock::Class 0.0203
Test::CompanionClasses 1.101370
Test::CPAN::Changes::ReallyStrict 0.1.3
Test::CheckManifest 1.24
Test::Class::Filter::Tags 0.11
Test::Command::Simple 0.03
Test::Cookbook 0.05
Test::Cukes 0.10
Test::DBIx::Class::Schema 0.01015
Test::Data 1.22
Test::mysqld
Test::DataLoader::MySQL 0.1.0
Test::DeepFile 0.003
Test::Dependencies 0.12
Test::Device::SerialPort 0.05
Test::DependentModules 0.10
Test::Dir 1.013
Test::Exports 1
Test::HTTP::Server::Simple 0.11
File::DirCompare
File::is
Test::Dirs 0.03
Test::Email 0.04
Protocol::Redis
Tie::Redis 0.21
Time::Format 1.11
Graphics::ColorUtils
Spreadsheet::ParseXLSX
Data::Peek
Spreadsheet::Read
Test::Excel 1.23
Test::FITesque 0.03
Test::File::Cleaner 0.03
Test::File::Content 1.0.1
Test::Fixme 0.04
Test::FileReferenced 0.02
Test::RDF 0.22
Test::Fixture::DBI 0.06
Test::FormValidator 0.07
Test::Groonga 0.04
Test::HTTP 0.16
Test::Harness 3.23
Test::Inline 2.212
Internals
XML::Easy
Test::XML::Easy 0.01
Test::Harness::Straps 0.30
Test::JSON::Meta 0.11
Test::JSYNC 0.01
Test::Leaner 0.02
Module::Used
Test::Module::Used 0.2.3
Test::Log::Dispatch 0.03
Test::Mock::LWP::Dispatch 0.02
Test::Magic 0.21
Test::Deep::UnorderedPairs
Test::Mock::Redis 0.05
Test::Mock::Apache2 0.02
Test::Mock::Test 0.01
Test::MockObject::Extra undef
Test::NeedsDisplay 1.07
Test::POE::Client::TCP 1.08
Test::POE::Server::TCP 1.14
Tk::JPEG
Test::Net::RabbitMQ 0.06
Test::Override::UserAgent 0.004
Test::MockTime::DateCalc 6
XML::LibXML::SAX::ChunkParser
Mixin::Event::Dispatch
Protocol::XMPP 0.004
Test::Nginx 0.13
File::Finder
Test::PerlTidy 20110323
Test::Pod::Spelling::CommonMistakes 1.000
Test::Pockito 0.02
Test::Prereq 1.037_02
Test::Reporter 1.57
Test::Reporter::Transport::Metabase 1.999008
Test::Reporter::Transport::Net::SMTP::Authen 1.02
Test::Reporter::Transport::Socket 0.16
Test::Rest 0.03
Test::Signature 1.10
Test::Run undef
Test::Skip::UnlessExistsExecutable 0.041
Test::Simple 0.98
Test::Smoke 1.44
Test::Story 0.07
Test::Structure 0.04
Test::Sys::Info 0.20
Test::StructuredObject 0.01000009
Test::Sweet 0.02
Test::TestCoverage 0.11
Test::Tester 0.107
Test::Tiny 0.02
Test::Usage 0.08
Test::Unit::ITestRunner 0.03
Test::Uses 0.01
Test::WWW::Selenium 1.24
Test::Weaken 3.006000
Test::Wrapper 0.2.1
Test::XT 0.03_02
Test::XMLElement 0.04
Test::YAML::Meta 0.19
TestML 0.21
Test::use::ok 0.02
Tree::DAG_Node::XPath 0.11
Texinfo::Menus 1.02
Tie::Cycle 1.17
Thread::Pool::Simple 0.25
Thread::Queue 2.12
Thread::Semaphore 2.12
Ticketmaster 1.02
TiddlyWeb::Wikrad 0.8
Data::Random
Tie::Array::Random 1.01
Tie::CArray 0.15
Tie::Array::Iterable 0.03
enum
Tie::Cache::LRU 20110205
Tie::Cache::Autoupdater 0.1
Tie::DataUUID 1.00
Tie::DiskUsage 0.21
Tie::FieldVals 0.6202
Tie::File 0.97_02
Tie::Handle::CSV 0.13
Tie::Hash::Identity 0.01
Tie::Hash::DBD 0.07
UNIVERSAL::DOES 0.004
XML::CompactTree
Treex::PML 2.06
Tree::Template::Declare 0.4
Tree::Transform::XSLTish 0.3
Module::Reload
App::whichpm
Lingua::Interset
Lingua::Interset::FeatureStructure
PerlIO::via::gzip
Treex::Core 0.05222
Try::Tiny::WarnCaught 0.01
URI 1.58
URI::PathAbstract 0.01
URI::Encode 0.03
URI::CrawlableHash 0.02
Unicode::Normalize 1.10
URI::ParseSearchString 3.441
URI::SmartURI 0.031
Net::OAuth::Simple
WWW::TypePad 0.3001
URI::Template::Restrict 0.06
URI::UTF8::Punycode 0.96
URI::ftpes 0.01
URI::ftps 0.02
URI::imap 1.01
URI::imaps undef
URI::tag 0.02
URI::scp 0.02
WWW::Tumblr 4.1
URL::Checkout 1.05
Ubic 1.28
Morpheus
Ubic::Result
Ubic::Daemon
Ubic::Service::Skeleton
Ubic::Service::Memcached 2.01
Unicode::Casing 0.06
Unicode::Collate 0.74
Unicode::Escape 0.0.2
Unicode::Regex::Set 0.02
Unicode::Transform 0.40
Data::Dump::XML
Web::App 1.20
Unix::ConfigFile 0.06
List::Cycle
Math::Fibonacci
Math::Fibonacci::Phi
Unix::PID 0.23
Unix::Groups 0.01
Test2::Tools::Explain
Overload::FileCheck
Test::MockFile
Unix::PID::Tiny 0.9
Unix::PasswdFileOps 0.2
User 1.9
User::Identity 0.93
Unix::Uptime 0.3701
Text::vFile::asData
Util::Timeout 0.01
V 0.13
VANAMBURG::SEMPROG::SimpleGraph 0.010
Sys::Load
HTML::Template::Pro
MooseX::Method
Text::Diff::Parser
SVN::Core
VCI 0.7.1
VCS::Lite 0.09
VCS::SCCS 0.17
VKontakte::API 0.04
VMware::API::LabManager 1.9
VOIP::MS 0.02
VOMS::Lite 0.12
VT::API 0.12
Hash::Flatten
Validation::Class undef
Dist::Zilla::Plugin::Homepage
Pod::Weaver::Section::Availability 1.110091
Validator::Custom 0.1422
Pod::Weaver::Section::BugsAndLimitations 1.102670
Variable::Lazy 0.03
Variable::OnDestruct 0.03
Pod::Weaver::Section::SourceGitHub 0.54
Pod::Weaver::PluginBundle::Author::DOHERTY 0.005
Net::Telnet
Varnish::CLI 0.03
Vend::Payment::Nova undef
Venn::Chart 1.02
Version::Requirements 0.101020
Video::CPL 0.10
Pod::Weaver::Section::Installation 1.101420
Pod::Weaver::PluginBundle::MARCEL 1.102460
Video::Flvstreamer 0.04
Video::Subtitle::SBV 0.01
Video::Webcam::DCS6620 0.0201
VideoLan::Client 0.12
App::CLI
Vimana 2.23
VisionDB::Read 0.04
Vroom 0.25
Voldemort 0.11
Prima 1.29_01
WWW::Salesforce 0.14
WWW::Scraper::ISBN 0.25
Pod::Simple 3.16
Pod::Weaver::Role::Preparer
Pod::Weaver::Plugin::EnsureUniqueSections 0.103531
Pod::Perldoc 3.15_02
Pod::Perldocs 0.17
Pod::PseudoPod::XHTML 1.01
Shell::Perl
Squatting
Text::VimColor
Pod::Server 1.12
Pod::Weaver::Section::WarrantyDisclaimer 0.103511
Pod::Simple::FromTree 0.000
Pod::Weaver::Section::BugsRT undef
Pod::Weaver::Role::SectionReplacer 1.00
Pod::Simple::Wiki 0.14
Pod::Weaver::PluginBundle::FLORA 0.03
Pod::Stripper 0.22
Pod::Stupid 0.005
HTML::Stream
Pod::Tree 1.17
Pod::Text::Ansi 0.05
Pod::Usage::CommandLine 0.04
Pod::WSDL 0.061
Pod::Weaver::Plugin::SubSpec 0.03
Pod::Weaver::PluginBundle::AYOUNG 0.14
Pod::Weaver::PluginBundle::Apocalyptic 0.002
Pod::Weaver::PluginBundle::RTHOMPSON 0.103510
Webinject 1.67
Pod::Weaver::PluginBundle::ReplaceBoilerplate 1.00
Queue::Base 2.201
Pod::Weaver::Section::ClassMopper 0.02
Pod::XML 0.99
Pod::Weaver::Section::Template 0.01
Pod::Wordlist::hanekomu 1.110090
Polycom::Contact::Directory 0.05
Portable 1.14
PostScript::File 2.02
Astro::MoonPhase
PostScript::Calendar 1.00
PostScript::Font 1.04
PostScript::MailLabels 2.30
PostScript::PPD 0.0202
MooseX::AttributeTree
PostScript::Report 0.08
PostgreSQL::PLPerl::NYTProf 1.002
Postscript::TextDecode 0.4
PowerDNS::API::Client 0.10
SOAP::Transport::TCP
XMLRPC::Lite
Getopt::Std::Strict
LEOCHARRE::CLI2
LEOCHARRE::Debug
WordPress::XMLRPC
Probe::Perl 0.01
Proc::Background 1.10
Printer::EVOLIS 0.01
Proc::Info::Environment 0.01
Proc::Forking 1.50
Process 0.28
Privileges::Drop
Proc::Launcher 0.0.35
Proc::PID::File 1.27
Proc::Reliable 1.16
Proc::NiceSleep 0.86
Proc::Simple 1.27
Process::YAML 0.28
Math::Big::Factors
Project::Euler 0.20
Project::Libs 0.01
Prompt::Timeout 1.04_01
Time::Progress 1.7
Protocol::PostgreSQL 0.006
XML::Atom::SimpleFeed 0.86
Prophet 0.743
Protocol::WebSocket 0.00901
Tk::Canvas::GradientColor 1.05
XS::Object::Magic
Alien::hiredis
Protocol::Redis::XS 0.02
Protocol::XMLRPC 0.08
Apache::Admin::Config
Apache::ConfigFile
NicTool
Quota
Provision::Unix 1.00
Qudo::Driver::DBI 0.03
Sub::Throttle
Qudo::Parallel::Manager 0.06
Queue::Mmap 0.06
Quiki 0.11
Tk::Canvas
Tk::Derived
Tk::MMutil
Tie::Hash::Random 1.02
Tie::Hash::Sorted 0.10
Tie::Hash::Vivify 1.01
Tk::ProgressBar::Mac 1.2
Tie::Ispell 0.05
Tie::REHash 1.04
Tk::Stderr 1.2
Tie::RefHash::Weak 0.09
Tie::RefHash 1.38
Tie::RegexpHash 0.15
Tie::Scalar::Timeout 2.101420
Tie::Simple 1.03
Tie::SecureHash 1.03
Tie::TZ 9
Tie::UnionHash 0.02
Tie::StoredOrderHash 0.22
Tie::Watch 1.301
Tiffany 0.04
Time::HiRes 1.9721
Time::Business 0.16
Time::DayOfWeek 1.6.A6FFxZB
Time::Checkpoint::Sequential 0.001
Time::Duration::Locale 7
Time::Elapsed 0.31
Time::Local 1.2000
Time::Epoch 0.02
Time::GPS 0.002
Time::OlsonTZ::Data 0.201106
Time::NT 0.007
IPC::Filter
Time::OlsonTZ::Download 0.001
Time::Piece 1.20_01
Time::Out 0.11
Time::PT
Date::Range
Time::Piece::Range 1.2
Time::Piece::Month 1.00
Time::Period 1.23
Time::Piece::MySQL
Time::Piece::Over24 0.007
Time::Simple 0.06
Time::SoFar 1.00
Time::UTC::Now 0.008
Time::Stamp 1.001
Time::StopWatchWithMessage 0.02
Time::TAI 0.003
Math::Decimal
Time::TAI::Now 0.003
Time::TCB 0.001
Time::TCG 0.001
Time::TT 0.004
Time::UTC_SLS 0.003
Locale::Wolowitz
Time::Verbal undef
TinyURL::RU 0.07
dateheader
TipJar::MTA 0.34
XML::TreePuller 0.1.3_01
Tk::Chart 1.16
Tk::Clock 0.29
Error::Hierarchy::Internal::DBI
Tk::ColoredButton 1.04
Tk::NumEntryPlain
Tk::Date 0.44
Config::Std
Tk::Dressing 1.04
Tk::ForDummies::Graph 1.14
Tk::FullKeypad 1.0
Tk::Help 0.3
Tk::HistEntry 0.43
Syntax::Highlight::Engine::Kate
Tk::ROSyntaxText 1.001
Tree::DAG_Node::Persist 1.05
Tk::Role::HasWidgets 1.111050
Tk::Sugar 1.093190
Tk::Role::Dialog 1.101480
Tk::TableMatrix 1.23
Tk::TextANSIColor 0.15
Tk::Xcursor 0.02
TomTom::WEBFLEET::Connect 2.01
Tool::Bench 0.002
Touch 0.01
Traceroute::Similar 0.16
Transmission::Client 0.0602
Tree 1.01
Tree::File 0.111
Tree::Numbered
Tree::Numbered::DB 1.02.2
Javascript::Menu
Tree::Numbered::Tools 1.04
Tree::Predicate 0.03
Tree::Simple::View 0.17
Tree::Simple::VisitorFactory 0.10
WWW::Scraper::ISBN::A1Books_Driver 0.03
WWW::Scraper::ISBN::AmazonDE_Driver 0.21
WWW::Scraper::ISBN::BarnesNoble_Driver 0.06
WWW::Scraper::ISBN::Blackwell_Driver 0.01
WWW::Scraper::ISBN::BookDepository_Driver 0.05
WWW::Scraper::ISBN::Driver 0.18
WWW::Scraper::ISBN::Booktopia_Driver 0.12
Yahoo::Marketing 7.03
WWW::Scraper::ISBN::EdenBooks_Driver 0.03
WWW::Scraper::ISBN::EmporiumBooks_Driver 0.06
Web::Hippie 0.36
WWW::Scraper::ISBN::Foyles_Driver 0.03
HTML::DOM
HTML::DOM::NodeList::Magic
HTML::DOM::EventTarget
HTML::DOM::View
HTML::DOM::Element::Form
HTML::DOM::Interface
WWW::Scripter 0.021
JE
JE::Destroyer
WWW::Scripter::Plugin::JavaScript 0.006
WWW::Scraper::ISBN::GoogleBooks_Driver 0.04
WWW::Scraper::ISBN::ORA_Driver 0.18
WWW::Scraper::ISBN::OpenLibrary_Driver 0.04
WWW::Scraper::ISBN::Pearson_Driver 0.17
WWW::Scraper::ISBN::PickABook_Driver 0.01
WWW::Scraper::ISBN::Record 0.17
Return::Value
Email::Send
WWW::Search 2.565
WWW::Scraper::ISBN::TheNile_Driver 0.08
WWW::Scraper::ISBN::WHSmith_Driver 0.01
WWW::Scraper::ISBN::Waterstones_Driver 0.02
Wx::Perl::VirtualTreeCtrl 1.017
WWW::Scraper::ISBN::Wheelers_Driver 0.07
Wx::Perl::ProcessStream 0.30
HTML::DOM::Event
HTML::DOM::Exception
WWW::Scraper::ISBN::WordPower_Driver 0.04
WWW::Scraper::ISBN::Yahoo_Driver 0.18
WWW::Scraper::Typo3 1.01
XML::DOM::Lite 0.15
constant::lexical
WWW::Scripter::Plugin::Ajax 0.09
WebService::MusicBrainz 0.93
WWW::SearchResult
WWW::Search::Test
WWW::Search::Ebay 2.254
WWW::Search::PharmGKB 2.01
WWW::Search::Rambler 1.2
WWW::Shorten::Bitly 1.14
WWW::Shorten::Googl 0.99
WWW::Search::Yandex 0.05
WWW::Shorten::Google 0.01
WWW::Shorten::ISSM undef
WWW::Shorten::IsGd 0.002
WWW::Shorten::SCK 0.001
WWW::Shorten::VGd 0.003
WWW::Shorten::ptl 0.04
WWW::Robot
WWW::Sitemapper 1.110340
WWW::SolveMedia 1.1
Error::Pure::Output::Text
Error::Pure
Class::Utils
File::Object
WWW::Splunk 1.03
WWW::TamperData 0.09
WWW::Translate::Apertium 0.16
WWW::TypePad::CmdLine 0.02
WWW::UPS::Detail 0.1
Net::SAJAX
WWW::USF::Directory 0.003
WebService::Solr 0.12
WWW::USF::WebAuth 0.003
WWW::UserAgent::Random 0.03
WWW::Vimeo::Simple 0.06
WWW::Weather::Yahoo 0.03
WWW::Wikipedia 2.00
WWW::Wookie 0.03
WWW::XKCD::AsText 0.002
WWW::Yahoo::Horoskop 0.1
XML::Hash::LX 0.06
WWW::Yandex::Catalog::LookupSite 0.06
WWW::Yandex::MailForDomain 0.2
WWW::Yandex::PhoneDetector 1.07
XML::TreePP 0.41
Web::Simple 0.008
WWW::YouTube::Download 0.26
WWW::YouTube::Info 0.04
WWW::YouTube::Info::Simple 0.05
WWW::YourFileHost 0.08
WWW::iloxx::Detail 0.1
WWW::phpBB 0.08
Weather::Google 0.05
Weather::Bug 0.25
Object::Tiny::RW
XML::Validate::LibXML
Weather::Underground::Forecast 0.06
WebNano::Controller
WebNano::Renderer::TT 0.002
WebNano 0.006
Weather::WWO 0.04
Web::App::Lib::EntityRecord 0.03
Web::Dispatcher::Simple 0.11
Web::oEmbed 0.04
HTML::TreeBuilder::LibXML
Web::Query 0.05
WebDAO 2.0_12
Web::oEmbed::Common 0.04
WebNano::Controller::CRUD 0.004
XML::FeedPP 0.42
WebService::8tracks 0.01
WebService::Aladdin 0.0703
WebService::Async 0.03
WebService::Async::Google::TranslateV1_0 0.03
WebService::Autotask 1.000
WebService::Bitly 0.05
WebService::Blogger 0.14
WebService::CityGrid::Search 0.04
WebService::ClinicalTrialsdotGov 0.04
WebService::EastCoJp::Dictionary 0.02

=head1 CONFIGURATION

Summary of my perl5 (revision 5 version 13 subversion 2) configuration:
  Commit id: e0de7c21b08329275a76393ac4d80aae90ac428e
  Platform:
    osname=linux, osvers=2.6.32-2-amd64, archname=x86_64-linux
    uname='linux k81 2.6.32-2-amd64 #1 smp fri feb 12 00:01:47 utc 2010 x86_64 gnulinux '
    config_args='-Dprefix=/home/src/perl/repoperls/installed-perls/perl/v5.13.2-213-ge0de7c2 -Dinstallusrbinperl=n -Uversiononly -Dusedevel -des -Ui_db'
    hint=recommended, useposix=true, d_sigaction=define
    useithreads=undef, usemultiplicity=undef
    useperlio=define, d_sfio=undef, uselargefiles=define, usesocks=undef
    use64bitint=define, use64bitall=define, uselongdouble=undef
    usemymalloc=n, bincompat5005=undef
  Compiler:
    cc='cc', ccflags ='-fno-strict-aliasing -pipe -fstack-protector -I/usr/local/include -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64',
    optimize='-O2',
    cppflags='-fno-strict-aliasing -pipe -fstack-protector -I/usr/local/include'
    ccversion='', gccversion='4.4.4', gccosandvers=''
    intsize=4, longsize=8, ptrsize=8, doublesize=8, byteorder=12345678
    d_longlong=define, longlongsize=8, d_longdbl=define, longdblsize=16
    ivtype='long', ivsize=8, nvtype='double', nvsize=8, Off_t='off_t', lseeksize=8
    alignbytes=8, prototype=define
  Linker and Libraries:
    ld='cc', ldflags =' -fstack-protector -L/usr/local/lib'
    libpth=/usr/local/lib /lib /usr/lib /lib64 /usr/lib64
    libs=-lnsl -lgdbm -ldb -ldl -lm -lcrypt -lutil -lc -lgdbm_compat
    perllibs=-lnsl -ldl -lm -lcrypt -lutil -lc
    libc=/lib/libc-2.11.2.so, so=so, useshrplib=false, libperl=libperl.a
    gnulibc_version='2.11.2'
  Dynamic Linking:
    dlsrc=dl_dlopen.xs, dlext=so, d_dlsymun=undef, ccdlflags='-Wl,-E'
    cccdlflags='-fPIC', lddlflags='-shared -O2 -L/usr/local/lib -fstack-protector'

=head1 AUTHOR

This Bundle has been generated automatically by the autobundle routine in CPAN.pm.
