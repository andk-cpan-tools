package Bundle::qr_sensitive;

$VERSION = '0.01';

1;

__END__

=head1 NAME

Bundle::qr_sensitive

=head1 SYNOPSIS

=head1 CONTENTS

Aspect
CGI::Alert
Data::Dump
Data::Dump::PHP
Exception::Base
Fennec
Getopt::LL
JE
Moose
Regexp::Assemble
Regexp::Log
Regexp::RegGrp
Search::Tools
Slay::Makefile::Gress
Template::Alloy
Test::Base
Test::ConsistentVersion
Test::Deep
Test::Exception::LessClever
Test::Log::Log4perl
Test::LongString
Test::Magic
Test::Output
Test::Unit
Test::Warn
Text::CSV
XML::Twig
YAML
YAML::Syck
YAML::XS
YAPE::Regex::Explain

=head1 DESCRIPTION

http://rt.perl.org/rt3//Public/Bug/Display.html?id=78008 has a large
part of the CPAN modules that were broken by the new stringification
of C<< qr// >> introduced in v5.13.5-11-gfb85c04.

In the following weeks more modules were discovered that were broken
and some of them were even fixed before they entered into the 78008
ticket.

Trying to gather them all in this bundle.

=head1 AUTHOR

Reini Urban
