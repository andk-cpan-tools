package Bundle::Snapshot_2011_05_03_00;

$VERSION = '0.01';

1;

__END__

=head1 NAME

Bundle::Snapshot_2011_05_03_00 - Snapshot of installation on k81 on Tue May  3 08:04:38 2011

=head1 SYNOPSIS

perl -MCPAN -e 'install Bundle::Snapshot_2011_05_03_00'

=head1 CONTENTS
Package::Stash
Apache::LogFormat::Compiler
MooseX::Attribute::ENV
Time::Warp
DBIx::Class::DynamicDefault
Data::Perl
Sub::Exporter::ForMethods
MooseX::Types
DateTime::Format::Flexible
boolean
DateTime::Set
DateTime::Event::Recurrence
DateTime::Format::Natural
DateTime::Event::ICal
Clone
DateTime::Format::ICal
Set::Infinite
Class::Accessor::Chained::Fast
Date::Parse
DateTimeX::Easy
strictures
DBIx::Class::TimeStamp
MooX::HandlesVia
Hash::Merge
Sub::Exporter::Progressive
Filesys::Notify::Simple
Type::Tiny
Sub::Quote
MooseX::NonMoose
Path::Router
namespace::autoclean
DateTime::Format::SQLite
DateTime::Format::MySQL
DBIx::Class::Schema::PopulateMore
Time::Zone
StackTrace::Auto
List::SomeUtils::XS
Clone::PP
List::UtilsBy
DateTime::Format::Builder
POSIX::strftime::Compiler
Class::Factory::Util
DateTime::Format::Strptime
DateTime::Locale
DateTime
DateTime::TimeZone
Params::ValidationCompiler
Class::Singleton
Exception::Class
Specio
Specio::Declare
Specio::Library::String
List::SomeUtils
Class::Data::Inheritable
MooX::StrictConstructor
Class::Method::Modifiers
Params::Validate
Devel::GlobalDestruction
Plack::Request
Moose
Package::DeprecationManager
Module::Runtime
Module::Runtime::Conflicts
Eval::Closure
List::AllUtils
Plack::App::Path::Router
Devel::OverloadInfo
HTTP::Throwable
Class::Load
Throwable
Safe::Isa
Types::Standard
Type::Utils
IO::String
HTTP::Headers::ActionPack
Web::Machine
Lingua::Stem::It
Text::German
Lingua::Stem::Snowball::Da
Lingua::Stem::Snowball::Se
Lingua::Stem::Snowball::No
Lingua::Stem::Fr
Lingua::GL::Stemmer
Lingua::PT::Stemmer
Lingua::Stem
Memoize::ExpireLRU
SQL::Translator
Parse::RecDescent
Lingua::EN::Words2Nums
Lingua::EN::FindNumber
XML::Writer
Lingua::EN::Tagger
Lingua::EN::Number::IsOrdinal
bareword::filehandles
Package::Variant
Class::Load::XS
Class::C3::Componentised
multidimensional
Package::Stash::XS
indirect
Import::Into
Devel::StackTrace
DBIx::Class
Carp::Clan
B::Hooks::EndOfScope
Lingua::EN::Inflect
SQL::Abstract
Variable::Magic
Data::Page
Module::Implementation
Moo
Role::Tiny
Module::Find
Config::Any
Data::Dumper::Concise
Class::Accessor
Context::Preserve
Algorithm::C3
Class::Accessor::Grouped
Class::C3
Stream::Buffered
MRO::Compat
Lingua::EN::Inflect::Number
Cookie::Baker
Class::XSAccessor
Test::TCP
Hash::MultiValue
HTTP::Headers::Fast
Devel::StackTrace::AsHTML
Test::Warn
Sub::Name
IO::Handle::Util
Exporter::Tiny
asa
Lingua::Stem::Ru
Text::Diff
Algorithm::Diff
Test::Differences
List::MoreUtils
Path::Class
Text::Unidecode
Lingua::EN::Inflect::Phrase
DBD::SQLite
Class::Inspector
WebAPI::DBIC
File::ShareDir
Scope::Guard
UNIVERSAL::require
Alien::Web::HalBrowser
Alien::Web
DBIx::Class::UUIDColumns
String::CamelCase
Data::Visitor
Sort::Naturally
Tie::ToObject
DateTime::Format::Pg
Data::Dump
DBIx::Class::Schema::Loader
Data::Dump::Streamer
Class::Unload
IO::All
String::ToIdentifier::EN
DBICx::TestDatabase
Mojolicious 1.22
Mojolicious::Plugin::OAuth2
namespace::clean
Plack::Util
Alien::astyle
Module::Build
DBI
Plack
Module::Util
DBIx::Simple
DBIx::Simple::Class
Toadfarm
Mojolicious::Plugin::DSC
Swagger2
Mojolicious::Plugin::SemanticUI
XSLoader
Test::Without::Module
File::HomeDir
Text::Markdown
Sub::Uplevel
Test::Exception
Text::MultiMarkdown
Email::Address
Ado
Ado::Plugin::Vest
PHP::Serialization
Class::Tiny
Test::File::ShareDir::Dist
PHP::Serialization::XS
DateTime::Locale::Base
Test::Mojo::Session
File::ShareDir::Install
Test::MockTime
Test::Requires
File::Slurp
Test::Name::FromLine
Archive::Extract
Test::Time
Lexical::SealRequireHints
Test::SharedFork
Test::Deep
Plack::Builder
Test::DBIx::Class
HTTP::Body
Cpanel::JSON::XS
DBIx::Class::InflateColumn::FS
PerlIO::utf8_strict
Mixin::Linewise::Readers
Devel::Symdump
Pod::Eventual::Simple
Pod::Coverage::CountParents
Dist::CheckConflicts
Sub::Identify
Devel::Hide
B::Hooks::OP::Check
DBIx::Class::IntrospectableM2M
Test::NoWarnings
CPAN::Meta::Check
Test::TempDir::Tiny
List::Util
Data::Printer
CGI
Test::Compile::Internal
Test::Warnings
Test::Needs
Test::CleanNamespaces
Test2::Plugin::NoWarnings
ExtUtils::Config
ExtUtils::Helpers
ExtUtils::InstallPaths
DBIx::Class::Fixtures
Module::Build::Tiny
Devel::Confess
Test::LeakTrace
Pod::Coverage::TrustPod
Test::CPAN::Meta
Test::Pod
File::Temp
Test::Pod::Coverage
Sort::Key
Test::Most
Test::Compile
DateTime::Fo
Test::HTTP::Response
MooX::Types::MooseLike::Base
Test::Output
YAML::Tiny
ToolSet
Test::FailWarnings
Mojolicious::Plugin::WebAPI
Dir::Self
IO::Pipely
Constant::Generate
POE
Log::Fu
POE::Sugar::Attributes
Test::API
Test::Modern
Array::Assign
Path::Tiny
ExtUtils::H2PM
POE::Test::Loops
Hook::LexWrap
PPI
Devel::Declare
Devel::Declare::MethodInstaller::Simple
Devel::CheckCompiler
Mouse
Any::Moose
Const::Fast
Method::Signatures
Test::Class
Couchbase::Client
JSON::RPC2
JSON::RPC2::Client
JSON::RPC2::Server
Mojolicious::Plugin::JSONRPC2
MojoX::JSONRPC2::HTTP
Cwd::Guard
Module::Build::XSUtil
File::Remove
Test::Object
Test::SubCalls
HTTP::Response::Stringable
WebService::Client
Image::ExifTool 8.50
WebService::Algolia
Image::JpegTran
B::Size2
Parse::Readelf
lib::abs
ex::lib
constant::def
Marpa::R2
B::Generate
Perl6::Contexts
Sort::Versions
Log::Handler
JSON::Validator
NgxQueue
Config::AutoConf
Text::SimpleTable
Test::Stream
Genealogy::Gedcom::Date
IO::Scalar
CGI::Simple::Cookie
CGI::Struct
Class::C3::Adopt::NEXT
Getopt::Long::Descriptive
aliased
MooseX::Emulate::Class::Accessor::Fast
MooseX::Role::Parameterized
MooseX::Getopt
HTTP::Request::AsCGI
Catalyst
Catalyst::Action::RenderView
Catalyst::Controller
Catalyst::Model
URI::ws
Catalyst::Component::InstancePerContext
Plack::Middleware::RemoveRedundantBody
CatalystX::Component::Traits
MooseX::Traits::Pluggable
Tree::Simple::Visitor::FindByUID
Tree::Simple
Catalyst::Model::DBIC::Schema
Catalyst::Plugin::ConfigLoader
Catalyst::Plugin::Unicode::Encoding
Catalyst::Runtime
MooseX::MarkAsMethods
MooseX::Types::LoadableClass
Plack::Middleware::ReverseProxy
MooseX::MethodAttributes::Role::AttrContainer::Inheritable
Dist::Zilla::PluginBundle::Basic
Dist::Zilla::Plugin::PodWeaver
Dist::Zilla::Plugin::SurgicalPkgVersion
Pod::Weaver
Pod::Elemental::PerlMunger
Version::Next
MooseX::Types::Path::Tiny
MooseX::Types::Stringlike
Git::Wrapper
Dist::Zilla::Role::GitConfig
MooseX::Has::Sugar
Config::Identity
Tie::IxHash
Pod::Elemental
Pod::Weaver::Config::Assembler
String::Truncate
File::Find::Rule
Term::Encoding
Number::Compare
Perl::PrereqScanner
CPAN::Uploader
Module::Path
Log::Dispatchouli
Sub::Exporter::GlobExporter
MooseX::LazyRequire
MooseX::SetOnce
Config::MVP
Config::MVP::Reader::INI
MooseX::Types::Perl
Config::INI::Reader
Config::MVP::Assembler
Config::MVP::Assembler::WithBundles
Config::MVP::Reader
Config::MVP::Reader::Findable::ByExtension
Config::MVP::Reader::Finder
Config::MVP::Section
Term::UI
MooseX::OneArgNew
Log::Message::Simple
Log::Message
Role::Identifiable::HasIdent
IO::TieCombine
Role::HasMessage
String::Flogger
String::Errf
Software::License
String::Formatter
Text::Template
Data::Section
Log::Dispatch::Array
App::Cmd::Tester
App::Cmd::Setup
Catalyst::ScriptRunner
Software::LicenseUtils
Dist::Zilla::Plugin::CopyReadmeFromBuild
Plack::Test::ExternalServer
Catalyst::View::JSON
Catalyst::View::TT
Log::Dispatch
Log::Dispatch::File
Log::Dispatch::Screen
Log::Dispatch::Syslog
Catalyst::Utils
Template::Timer
Dancer
AppConfig
Template
String::RewritePrefix
Plack::Middleware::MethodOverride
Dist::Zilla::Plugin::PkgVersion
Dist::Zilla
Plack::Middleware::FixMissingBodyInRedirect
Tree::Simple::Visitor::FindByPath
MooseX::Role::WithOverloading
Catalyst::Plugin::Session
Catalyst::Plugin::Session::State::Cookie
Catalyst::Plugin::Session::Store::Dummy
Test::WWW::Mechanize::Catalyst
WWW::Mechanize
Test::WWW::Mechanize
HTTP::Server::Simple
HTTP::Server::Simple::CGI
Carp::Assert::More
HTML::TreeBuilder
HTML::Form
Object::Signature
Test::LongString
Carp::Assert
Plack::Response
Plack::Runner
Test::Trap
Parse::Yapp
CGI::Cookie
Parse::Eyapp
Function::Parameters
Tie::Hash::Method
Text::Glob
DBD::Pg
IPC::System::Simple
Test::PostgreSQL
Dist::Zilla::Plugin::ArchiveRelease
Test::WWW::Mechanize::PSGI
Test::utf8
Catalyst::Plugin::AutoCRUD
Limper
Hash::Merge::Simple
B::Debug
MIME::Types
Devel::Cover
Dist::Zilla::App::Command::cover
Data::Munge
HTTP::Server::Simple::PSGI
Return::MultiLevel
Template::Tiny
Net::HTTP::Client
Dancer2
Dancer2::Test
Class::Accessor::Lite
HTTP::Parser::XS
Furl
Dancer::Plugin::Auth::Google
Runner::Init
Mojo::Pg
Postgredis
Data::Float
Math::Round
Switch
Convert::Pluggable
Net::DNS::Native
Pegex
YAML::XS
B::Keywords
App::Pod2CpanHtml
String::Format
Pod::Spell
Perl::Tidy
FFI::CheckLib
Readonly
PPIx::Utilities::Node
PPIx::Utilities::Statement
PPIx::QuoteLike
PPIx::Regexp
Perl::Critic
Inline
Log::Log4perl
Inline::C
Inline::CPP
Inline::Filters
Archive::Tar::Wrapper
Dist::Zilla::Plugin::Authority
Math::BigInt
Ref::Util
Dist::Zilla::Plugin::CheckChangesHasContent
Dist::Zilla::Plugin::CPANFile
ExtUtils::CppGuess
Dist::Zilla::Plugin::CheckPrereqsIndexed
Dist::Zilla::Role::ModuleMetadata
Math::BigInt::GMP
File::chdir
Module::Refresh
Module::ScanDeps
PadWalker
Pod::PseudoPod
Test::CPAN::Changes
Text::ASCIITable
Test::Number::Delta
ExtUtils::MakeMaker
Test::LWP::UserAgent
Dist::Zilla::Plugin::CheckExtraTests
Dist::Zilla::Plugin::CheckSelfDependency
Path::Iterator::Rule
BSON
Dist::Zilla::Plugin::ConfirmRelease
MongoDB
RPerl::Class
Test::RequiresInternet
Test::ConsistentVersion
Dist::Zilla::Plugin::ConsistentVersionTest
Importer
autobox
DBD::CSV
JSON::Any
Want
Dist::Zilla::Role::RegisterStash
WWW::Sixpack
Dancer2::Plugin::Sixpack
UNIVERSAL::isa
File::Share
Dist::Zilla::Role::DynamicConfig
UNIVERSAL::can
Dist::Zilla::Role::Stash::Plugins
Config::MVP::Slicer
Dist::Zilla::Stash::PodWeaver
syntax
autobox::Junctions
Syntax::Keyword::Junction
File::Slurp::Tiny
MooseX::TraitFor::Meta::Class::BetterAnonClassNames
MooseX::Util
Perl::Version
MooseX::Types::Common::String
MooseX::AttributeShortcuts
Dist::Zilla::Plugin::ContributorsFromGit
autobox::Core
MooseX::Meta::TypeConstraint::Mooish
Dist::Zilla::Plugin::SchwartzRatio
MetaCPAN::Client
Dist::Zilla::Plugin::Git::CommitBuild
Sys::Syslog
IPC::Run3
Devel::FindPerl
Software::License::None
Dist::Zilla::Plugin::Git::NextVersion
Test::File::ShareDir
Dist::Zilla::Role::FileWatcher
Dist::Zilla::Plugin::CJM
Test::Filename
Pod::Elemental::Document
Pod::Elemental::Element::Nested
Set::Scalar
Dist::Zilla::Plugin::CopyFilesFromBuild
Dist::Zilla::Plugin::Git::Describe
Dist::Zilla::MetaProvides::ProvideRecord
Pod::Markdown
Dist::Zilla::Plugin::MetaProvides
Dist::Zilla::Plugin::NoSmartCommentsTests
Dist::Zilla::Plugin::MetaProvides::Package
Dist::Zilla::Role::MetaProvider::Provider
Dist::Zilla::Plugin::GitHubREADME::Badge
Dist::Zilla::Plugin::Git::Remote::Check
Dist::Zilla::Plugin::Config::Git
Test::NoSmartComments
Pod::Elemental::Element::Pod5::Command
Pod::Elemental::Element::Pod5::Ordinary
Pod::Elemental::Element::Pod5::Region
Pod::Elemental::Element::Pod5::Verbatim
Dist::Zilla::Plugin::GitHub::Update
Exporter::Lite
AnyEvent
AnyEvent::HTTP
URI::FromHash
MooseX::Types::URI
Dist::Zilla::Plugin::GithubMeta
Pod::Elemental::Selectors
Dist::Zilla::Plugin::HasVersionTests
Config::TOML
Dist::Zilla::Plugin::InstallRelease
Test::HasVersion
Iterator
Iterator::Util
File::Find::Rule::Perl
Dist::Zilla::Plugin::MinimumPerl
Dist::Zilla::Plugin::MetaConfig
Crypt::SSLeay
Perl::MinimumVersion
recommended
Dist::Zilla::Util::Test::KENTNL
Dist::Zilla::Plugin::MetaJSON
Dist::Zilla::Role::Bootstrap
Dist::Zilla::Plugin::MetaNoIndex
Dist::Zilla::Plugin::Bootstrap::lib
Dist::Zilla::Util::ConfigDumper
Dist::Zilla::Plugin::MetaYAML
Dist::Zilla::Plugin::PodCoverageTests
Dist::Zilla::Plugin::Prepender
Dist::Zilla::Plugin::ReadmeAnyFromPod
Dist::Zilla::Plugin::Test::Compile
Dist::Zilla::PluginBundle::Git::CheckFor
Data::DPath
Dist::Zilla::Plugin::PromptIfStale
Dist::Zilla::Plugin::Test::MinimumVersion
Dist::Zilla::Plugin::PodSyntaxTests
Dist::Zilla::Plugin::PruneFiles
Parse::CPAN::Packages::Fast
Dist::Zilla::Plugin::Run::AfterMint
Dist::Zilla::Plugin::TaskWeaver
Dist::Zilla::Plugin::Signature
DBIx::Class::InflateColumn::Currency
MCE
Data::Currency
Finance::Currency::Convert::WebserviceX
Dist::Zilla::Plugin::RunExtraTests
Locale::Currency::Format
Dist::Zilla::Plugin::Travis::ConfigForReleaseBranch
Dist::Zilla::Plugin::Test::EOL
Dist::Zilla::Plugin::Test::NoTabs
Dist::Zilla::Plugin::Test::CheckDeps
AnyEvent::Twitter::Stream
Object::Event
AnyEvent::HTTPD
Test::MinimumVersion
Pod::Elemental::Transformer::Gatherer
Pod::Elemental::Transformer::Nester
Pod::Elemental::Transformer::Pod5
Pod::Elemental::Types
Sub::Defer
Test::Moose::More
Test::MockObject
Test::CheckDeps
TAP::SimpleOutput
Locale::TextDomain
MooseX::Types::Path::Class
Devel::BeginLift
Class::Iterator
File::Find::Iterator
constant::defer
App::PodLinkCheck::ParseLinks
App::PodLinkCheck::ParseSections
Dist::Zilla::Plugin::Test::Pod::LinkCheck
Test::Pod::LinkCheck
Devel::SimpleTrace
Dist::Zilla::PluginBundle::Git
Dist::Zilla::Plugin::Test::PodSpelling
Dist::Zilla::Plugin::Test::ReportPrereqs
Test::Spelling
Dist::Zilla::Plugin::TestRelease
Digest::SHA1
Net::Twitter
Net::OAuth
Test::Script
WWW::Shorten
Dist::Zilla::Plugin::Twitter
Dist::Zilla::Role::PluginBundle::Config::Slicer
Dist::Zilla::Role::PluginBundle::Easy
Dist::Zilla::Role::PluginBundle::PluginRemover
WWW::Shorten::Simple
Dist::Zilla::Plugin::UploadToCPAN
Devel::CheckBin
AnyEvent::CacheDNS
Smart::Comments
Dist::Zilla::App::Command::authordeps
Struct::Dumb
WWW::Shorten::TinyURL
Future
IO::Async::Loop
IO::Async::Stream
IO::Async::Handle
Moose::Autobox
IO::Async::Protocol::Stream
Pod::Weaver::Section::SeeAlso
Pod::Weaver::Plugin::StopWords
Pod::Weaver::Section::Collect
Pod::Weaver::Section::CollectWithIntro
Dist::Zilla::Role::Plugin
IO::Async::SSL
Net::Async::HTTP
Test::Deep::JSON
Dist::Zilla::Stash::PAUSE::Encrypted
Sys::Syscall
Object::Container
String::CRC32
Dist::Zilla::Role::TextTemplate
Module::CPANfile
Error
Gearman::Util
Danga::Socket
Gearman::Server
MouseX::Foreign
AnyEvent::Gearman
Pod::Weaver::Section::Contributors
Gearman::Worker
Git::Raw
Test::NoTabs
Dist::Zilla::Plugin::PrereqsClean
Dist::Zilla::Role::MetaCPANInterfacer
sanity
Test::EOL
CHI
Time::Duration::Parse
Time::Duration
Devel::CheckLib
Log::Any
Hash::MoreUtils
MooX::Types::MooseLike::Numeric
Digest::JHash
WWW::Mechanize::Cached
Net::OAuth::Message
autobox::Camelize
WWW::Mechanize::Cached::GZip
Pod::Elemental::Transformer::List
Scope::Upper
MooseX::NewDefaults
Dist::Zilla::Role::PluginBundle::Merged
MooseX::RelatedClasses
Test::Pod::Content
URI::Escape::XS
IO::AIO
Test::Identity
AnyEvent::AIO
AnyEvent::Redis
Dist::Zilla::PluginBundle::RSRCHBOY
HTTP::Tiny::Mech
MetaCPAN::API
String::Tagged
Heap
Devel::MAT
Scalar::String
MouseX::NativeTraits
MouseX::Traits
AnyMQ
Scalar::IfDefined
Test::Effects
Keyword::Simple
Hash::SharedMem
Authen::SASL
XML::Parser::Expat
Net::LibIDN
AnyEvent::XMPP
Future::Utils
WWW::Mechanize::GZip
Test::Refcount
Lexical::Failure
Archive::Any::Lite
Array::Diff
File::Find::Object
IO::Capture
IO::Async::Notifier
Pod::Strip
Test::Async::HTTP
Event
XML::Parser
Net::Async::Matrix
RPC::XML
App::Genpass
RPC::XML::Client
XML::NamespaceSupport
XML::SAX::Base
XML::SAX
XML::SAX::Expat
XML::Simple
RPC::XML::ParserFactory
String::Approx
Net::OpenNebula
Module::ExtractUse
VM::EC2
KiokuDB
Search::GIN::Driver
Dist::Zilla::PluginBundle::Author::MELO
KiokuDB::Backend
KiokuDB::Backend::Role::Clear
KiokuDB::Backend::Role::Concurrency::POSIX
KiokuDB::Backend::Role::GC
KiokuDB::Backend::Role::Query::GIN
KiokuDB::Test
KiokuDB::Backend::Role::Query::Simple
KiokuDB::Entry
KiokuDB::Backend::Role::Scan
KiokuDB::TypeMap
KiokuDB::Backend::Role::TXN
KiokuDB::Backend::Serialize::Delegate
Set::Object
KiokuDB::TypeMap::Entry::Naive
Dist::Zilla::PluginBundle::TestingMania
Search::GIN::Extract
MooseX::Blessed::Reconstruct
MooseX::YAML
KiokuDB::Backend::DBI
KiokuDB::TypeMap::Entry
Dist::Zilla::Plugin::Test::CPAN::Changes
Dist::Zilla::Plugin::Test::CPAN::Meta::JSON
Search::GIN::Query::Class
MooseX::Clone
Dist::Zilla::Plugin::Test::DistManifest
Data::Swap
MooseX::Clone::Meta::Attribute::Trait::NoClone
Data::Stream::Bulk
Cache::Ref
Hash::Util::FieldHash::Compat
Data::Stream::Bulk::Util
Module::CPANTS::Analyse
Pod::Weaver::Plugin::WikiDoc
Test::CPAN::Meta::JSON
Module::Manifest
Dist::Zilla::Plugin::Test::Perl::Critic
Dist::Zilla::Plugin::Test::Kwalitee
Dist::Zilla::Plugin::OurPkgVersion
Pod::WikiDoc
KiokuDB::Backend::Redis
IO::Socket::Timeout
Dist::Zilla::Plugin::Clean
PerlIO::via::Timeout
Dist::Zilla::Plugin::Test::Portability
Test::Portability::Files
Dist::Zilla::Plugin::Test::Synopsis
Dist::Zilla::Plugin::Test::UnusedVars
Test::Synopsis
Dist::Zilla::Plugin::Bugtracker
Test::Vars
Software::License::CC_BY_SA_3_0
Dist::Zilla::Plugin::Test::Version
Redis
Markapl
Template::Declare
Template::Declare::Tags
Class::ISA
String::BufferStack
File::NFSLock
Pod::Weaver::Section::Support
Dist::Zilla::Plugin::MojibakeTests
Getopt::Lucid
Dist::Zilla::Plugin::ReportVersions::Tiny
Test::DistManifest
Dist::Zilla::Plugin::Repository
Dist::Zilla::Plugin::Test::Pod::No404s
Test::Pod::No404s
Templer
Pod::Weaver::Plugin::Encoding
CPAN::Mini::Webserver
File::Type
Module::InstalledVersion
Parse::CPAN::Authors
App::Cache
URI::Find
Lingua::StopWords
PPI::HTML
Net::Rendezvous::Publish::Backend::Apple
CSS::Tiny
Carton
Search::QueryParser
local::lib
Parse::CPAN::Whois
Archive::Peek
Test::Mojibake
Search::Tokenizer
Parse::CPAN::Packages
Class::Accessor::Lvalue
Test::InDistDir
Unicode::CaseFold
Net::Rendezvous::Publish
Function::Fallback::CoreOrPP
Text::Transliterator::Unaccent
Regexp::Stringify
Data::Dmp
L
File::ChangeNotify
Data::Dump::OneLine
HTML::TableExtract
App::ArchiveDevelCover
Gen::Test::Rinci::FuncResult
Test::Timer
Perinci::Sub::Util
Regexp::Debugger
App::BashComplete
Devel::GlobalPhase
File::Next
Test::Spec
App::Adenosine
App::autotest
Plack::App::File
App::Ack
App::Nopaste
Test::File
App::HTTPThis
Protocol::Notifo
WebService::Notifo
MIME::Charset
Unicode::GCString
Guard
Coro
App::Uni
App::Whiff
Test::Mock::Guard
App::Notifo
App::cpanminus
App::cpanoutdated
File::Slurper
utf8::all
App::p
DBD::mysql
Getopt::ArgvFile
Params::Classify
Class::Mix
B::Hooks::OP::Check::EntersubForCV
Crypt::Eksblowfish::Bcrypt
SUPER
Archive::Zip
Devel::Cycle
Font::TTF
PDF::API2
Mo
App::pod2pdf
Crypt::PasswdMD5
Authen::Simple
Authen::Simple::Passwd
Async::Hooks
FCGI
CGI::Emulate::PSGI
CGI::Fast
CPAN::Mini
Test::MockModule
HTML::Restrict
CPAN::SQLite
CSS::Minifier
Text::SimpleTable::AutoWidth
Cache::BaseCache
Cache::Cache
Carp::Always
Class::Factory
Module::Reader
Cache::Object
Check::ISA
Cache::CacheFactory
Font::Metrics::Courier
URI::Query
Term::Size::Perl
Term::Size::Any
String::Trim
Parse::MIME
Devel::NYTProf
Digest::SHA3
Class::C3::XS
Crypt::PBKDF2
List::Compare
Font::Metrics::CourierBold
Crypt::CBC
Crypt::Blowfish
HTTP::CookieMonster
Font::Metrics::CourierBoldOblique
Font::Metrics::CourierOblique
Font::Metrics::Helvetica
Font::Metrics::HelveticaBold
Font::Metrics::HelveticaBoldOblique
Font::Metrics::HelveticaOblique
Font::Metrics::TimesBold
Font::Metrics::TimesBoldItalic
Code::TidyAll
Font::Metrics::TimesItalic
Text::Aligner
Code::TidyAll::Plugin::Perl::AlignMooseAttributes
Code::TidyAll::Plugin::Perl::IgnoreMethodSignaturesSimple
Code::TidyAll::Plugin::PerlTidy
Code::TidyAll::Plugin::PodTidy
File::Zglob
DBICx::Indexing
Dist::Zilla::Role::Releaser
Dist::Zilla::Role::Stash
Dist::Zilla::Plugin::Git::Tag
Dist::Zilla::Role::TestRunner
Test::Class::Most
Test::Perl::Critic
Clipboard
Config::GitLike
DBICx::DataDictionary
HTML::FormatText
DBICx::Shortcuts
Font::Metrics::TimesRoman
Mason::Tidy
Method::Signatures::Simple
Fennec::Lite
Meta::Builder
Exporter::Declare
Log::Contextual
Text::Brew
DBIx::Introspector
DBIx::Class::DeploymentHandler
DBIx::Class::Helpers
DBIx::Class::Candy
Dist::Zilla::Plugin::InstallGuide
Dist::Zilla::Plugin::PodPurler
Sys::UniqueID
Test::Roo
DBIx::Class::EncodedColumn
Dist::Zilla::Plugin::LatestPrereqs
AnyEvent::ReverseHTTP
DBIx::Class::InflateColumn::Serializer
DBIx::Class::InflateColumn::URI
Text::CSV_XS
DBIx::Connector
Plack::Server::ReverseHTTP
Class::Trigger
Object::InsideOut
Data::ObjectDriver
Date::Manip
Devel::Caller
Devel::LexAlias
DBIx::Tracer
MooseX::Object::Pluggable
Lexical::Persistence
DateTime::Format::DateParse
Devel::REPL
DateTime::Format::Excel
Math::Random::MT::Auto
Test::Inter
Date::Tiny
DateTime::Format::MSSQL
LWP::ConsoleLogger::Easy
Data::UUID::MT
ExtUtils::MakeMaker::CPANfile
Test::UseAllModules
Test::Kwalitee
Test::Version
AnyEvent::Handle::UDP
Devel::LeakGuard::Object
Test::Fork
GD
Net::Pcap
latest
Plack::Test::Agent
Devel::StackTrace::WithLexicals
Test::TinyMocker
Find::Lib
XML::Twig
Lexical::Sub
HTML::FormatText::WithLinks
Data::Serializer
Test::Memory::Cycle
Devel::TraceUse
Exception::Class::TryCatch
EV
XML::XPath
Gearman::Client
Email::Date::Format
Email::Simple
GraphViz
Email::Abstract
HTML::Lint
Catalyst::Plugin::Static::Simple
File::HomeDir::PathClass
File::MMagic
Test::HTML::Lint
Format::Human::Bytes
MooX::Options
Excel::Writer::XLSX
Message::Passing
Message::Passing::Redis
Sys::Hostname::Long
Git
Class::Container
HTML::Mason
Catalyst::Devel
Starman
MooseX::Daemonize
Net::Server
Module::Install
Github::Import
CatalystX::InjectComponent
CatalystX::Profile
Graph::Easy
HTTP::Parser
Cache::Ref::CART
Image::Size
Regexp::Common
File::ConfigDir
MooX::File::ConfigDir
Data::Record
Catalyst::Controller::ActionRole
Catalyst::Plugin::Authentication
MooX::ConfigFromFile
Data::Stream::Bulk::Callback
Task::Kensho::Toolchain
Pinto
Router::Simple
MooseX::StrictConstructor
MooseX::Aliases
MooseX::Types::Set::Object
Search::GIN::Driver::Hash
Search::GIN::Extract::Class
CatalystX::SimpleLogin
Term::Bash::Completion::Generator
Text::Colorizer
Math::GSL
Tree::Trie
CatalystX::REPL
Carp::REPL
MooseX::RelatedClassRoles
Expect::Simple
Search::GIN::Extract::Delegate
Cache::Ref::CLOCK
Catalyst::Plugin::Session::Store::Delegate
Test::TempDir
Task::Kensho::WebDev
DateTime::Format::W3CDTF
Test::Block
DateTime::Format::Mail
Locale::Maketext::Lexicon
String::Escape
Log::Any::Adapter::Log4perl
XML::Atom
XML::LibXML
Log::Any::Adapter
Mail::Field
Mail::Header
Plack::Component
MIME::Entity
Plack::App::Proxy
Modern::Perl
Mail::Internet
MIME::Parser
MIME::Lite
Plack::Middleware::Deflater
Data::HexDump::Range
Text::Pluralize
PSGI
Data::TreeDumper
Term::Size
Devel::Size
Plack::Middleware::Header
Plack::Middleware::Session
Net::FastCGI
JavaScript::Value::Escape
Plack::Middleware::ConsoleLogger
Plack::Middleware::Auth::Digest
CGI::Compile
IPC::Signal
Signal::Mask
Proc::Wait3
Starlet
Parallel::Prefork
Mail::Address
Twiggy
Net::Server::Coro
Corona
CGI::PSGI
Text::MicroTemplate
Plack::Middleware::Debug
Task::Plack
FCGI::Client
Mason
Mason::Plugin::Cache
Task::Kensho::ModuleDev
MooseX::HasDefaults
Mail::IMAPClient
MooseX::ConfigFromFile
Devel::PartialDump
MIME::WordDecoder
MooseX::Types::Structured
Parse::Method::Signatures
Parse::Method::Signatures::Param::Named
Parse::Method::Signatures::Param::Placeholder
Parse::Method::Signatures::TypeConstraint
Parse::Method::Signatures::Types
MooseX::Traits
DBIx::Class::QueryLog
Plack::Middleware::DBIC::QueryLog
Plack::Middleware::Debug::DBIC::QueryLog
MooseX::Meta::TypeConstraint::ForceCoercion
MooseX::Method::Signatures
MooseX::Method::Signatures::Meta::Method
MooseX::Method::Signatures::Types
MooseX::Types::VariantTable
MooseX::Declare
MooseX::MultiMethods
DBIx::Class::QueryLog::Analyzer
MooseX::Types::DateTime
POE::XS::Queue::Array
Task::Kensho::Testing
IRI
Module::Which
MooseX::HasDefaults::RO
Test::Manifest
NTS::Template
Net::SMTP::TLS::ButMaintained
Devel::CheckOS
Net::Bonjour
Net::Rendezvous
Params::Coerce
Alien::GvaScript
Pod::POM
Task::Kensho::DBDev
Pod::POM::View::HTML
Plack::Middleware::Debug::DBIProfile
Pod::Cpandoc
Pod::POM::Web
Encode::Newlines
Pod::Wrap
HTML::FillInForm
Mason::Plugin::HTMLFilters
Test::Cmd
Sereal::Encoder
Task::Devel::Cover::Recommended
prefork
XML::NamespaceFactory
XML::CommonNS
XML::Namespace
Algorithm::Combinatorics
MooseX::ArrayRef
Cache::LRU
Text::Table
Task::Kensho::Scalability
RDF::Trine
Mason::Plugin::RouterSimple
TryCatch
MooseX::App::Cmd
Task::Kensho::CLI
File::Find::Wanted
Data::Rmap
MasonX::ProcessDir
Poet
Any::Template::ProcessDir
Pod::Tidy
Plack::Session::Store::Cache
HTML::Tiny
Test::More::Diagnostic
Test::Expect
Sereal::Decoder
Devel::AssertOS
Test::JSON
Text::CSV
Term::ReadLine::Gnu
XML::LibXSLT
XML::Generator::PerlData
XML::Filter::BufferText
Math::Random::ISAAC
XML::SAX::Writer
B::Hooks::OP::PPAddr
Font::TFM
Test::HexString
Git::Version::Compare
System::Command
Git::Repository
Test::ClassAPI
Test::Requires::Git
Class::ErrorHandler
Feed::Find
DB_File
URI::Fetch
XML::Feed
RDF::aREF
RDF::NS
Lingua::JA::Romanize::Japanese
File::Flat
Devel::Callsite
File::chmod
Devel::Chitin
Algorithm::Dependency
Pod::Tests
Sub::Delete
Lingua::ZH::Romanize::Pinyin
Unicode::Unihan
Test::Able
Lingua::KO::Romanize::Hangul
Lingua::KO::Hangul::Util
Lingua::EN::Syllable
Unicode::Tussle
Data::MessagePack
Functional::Utility
SQL::Statement
HTML::Selector::XPath
Class::MixinFactory
ZMQ::LibZMQ3
XML::XPathEngine
Test::Resub
HTML::TreeBuilder::XPath
WWW::MobileCarrierJP
Unicode::String
Math::Int64
Any::URI::Escape
LWP::Online
Search::Elasticsearch
Search::Elasticsearch::Scroll
Log::Any::Adapter::Callback
Data::Transform::ExplicitMetadata
Log::Agent
Module::Pluggable::Fast
MouseX::Types
Smart::Args
Data::Denter
Statistics::Regression
MouseX::Types::Mouse
Data::Difflet
provide
Hash::MostUtils
DynaLoader::Functions
BenchmarkAnything::Storage::Frontend::Tools
Devel::CallChecker
Devel::CallParser
BenchmarkAnything::Storage::Frontend::Lib
BenchmarkAnything::Schema
BenchmarkAnything::Reporter
TAP::DOM
BenchmarkAnything::Config
App::Rad
DBIx::MultiStatementDo
HTTP::Link::Parser
JSON::Schema
Config::INI::Serializer
JSON::Hyper
JSON::Path
autovivification
SQL::SplitStatement
LV
Lingua::Boolean::Tiny
SQL::Tokenizer
BenchmarkAnything::Storage::Backend::SQL
Kelp
Sub::Infix
Kelp::Test
Inline::Module
Test::Mini
Text::Outdent
Data::Inspect
Kelp::Base
namespace::sweep
Env::Sanctify
Net::Domain::TLD
Acme::Damn
Statistics::CaseResampling
DateTime::Format::Duration
Sys::SigAction
RDF::Query
Data::Validate::Domain
Dumbbench
NetAddr::IP
Data::Validate::IP
Data::Validate::URI
XML::LibXML::Simple
Types::Path::Tiny
Types::UUID
Types::URI
LWP::UserAgent::CHICaching
UNIVERSAL::ref
Kavorka
Return::Type
Parse::Keyword
RDF::LDF
DAIA
URI::Template
Perl::Destruct::Level
Test2::Plugin::UTF8
Test::JSON::Entails
Object::Tiny
Data::Util
HOP::Stream
Path::AttrRouter
Server::Starter
Mock::Quick
HTTP::Online
Test::Routine
Email::Valid::Loose
FormValidator::Lite
Module::Build::Pluggable
HTML::Escape
HTML::Shakan
Sys::HostIP
Redis::ScriptCache
Browser::Open
Exporter::AutoClean
Test::Base::Less
Plack::Request::WithEncoding
Module::Build::Pluggable::PPPort
Bio::NEXUS
Config::IniFiles
Email::MIME::ContentType
Config::General
Proc::Daemon
Email::MIME::Encodings
Email::MessageID
Email::MIME::Creator
Email::MIME
Proc::ProcessTable
Scalar::Util::Numeric
Email::Valid
Compress::Snappy
Brannigan
Env::Path
Parallel::Iterator
Path::Abstract
Pod::Simple::Text::Termcap
Test::Lazy
IO::Interactive
Alien::TinyCC
Regexp::Assemble
MooX::Aliases
Getopt::Compact
HTML::Template
Pod::Readme
Object::Enum
Config::Onion
App::FatPacker
DBIx::Class::InflateColumn::Object::Enum
CGI::Application
CPAN::Perl::Releases
Directory::Scratch
accessors
Test::Files
TAP::Formatter::HTML
Compress::Bzip2
CPAN::Checksums
Data::Compare
File::Spec::Native
App::perlbrew
Dist::Metadata
Archive::Any::Create
Tapper::Config
MooseX::ClassAttribute
Text::Reform
Text::Autoformat
MooseX::Configuration
Exporter::Tidy
Proc::Fork
Proc::Terminator
Tapper::Schema
Tapper::Schema::TestrunDB
UUID::Tiny
Tapper::Model
Tapper::Schema::TestTools
Tapper::TAP::Harness
CGI::Expand
Data::Structure::Util
Tapper::Reports::DPath::TT
CGI::FormBuilder::Source::Perl
HTML::FormHandler
Data::Clone
CGI::FormBuilder
Template::Plugin::Autoformat
Catalyst::Authentication::Credential::HTTP
Catalyst::Authentication::Store::DBIx::Class
Catalyst::Manual
Catalyst::ActionRole::ACL
Catalyst::Plugin::I18N
Catalyst::Plugin::Session::Store::DBIC
Catalyst::Plugin::StackTrace
Catalyst::Plugin::Session::Store::File
Catalyst::Model::Adaptor
CatalystX::LeakChecker
FCGI::ProcManager
Task::Catalyst
XML::RSS
LWP::UserAgent::Determined
Text::Trim
Text::Hogan::Compiler
MooX::Role::Logger
Lingua::EN::PluralToSingular
DBIx::Admin::DSNManager
LockFile::Simple
Perl6::Junction
Mail::Sendmail
Template::Plugin::EnvHash
Net::OpenSSH
Tapper::Cmd
Tapper::Cmd::DbDeploy
Web::Scraper
Tapper::Cmd::Cobbler
Tapper::Cmd::Init
Tapper::Cmd::Notification
Tapper::Cmd::Precondition
Tapper::Cmd::Queue
Tapper::Cmd::Scenario
Tapper::Cmd::Testplan
Tapper::Cmd::Testrun
Tapper::Cmd::Requested
Tapper::Cmd::User
Tapper::Base
Tapper::Reports::DPath
Tapper::Reports::DPath::Mason
MooseX::Log::Log4perl
Tie::Function
HTML::Entities::Interpolate
DBIx::Admin::CreateTable
DBIx::Admin::TableInfo
PerlX::Assert
Dispatch::Class
Catalyst::View::Email
Data::Alias
Beam::Emitter
Try::Tiny::ByClass
Data::Section::Simple
Date::Simple
Set::Array
Tree::DAG_Node
XML::Bare
XML::Tiny
Text::TabularDisplay
Make
Net::Server::Fork
LWP::Protocol::PSGI
Syntax::Collector
Eval::LineNumbers
XT::Util
Test::RedisServer
Text::Unaccent
thanks
Time::ParseDate
Config::Checker
Config::YAMLMacros
Test::UNIXSock
String::CRC
DBM::Deep
FileHandle::Fmode
Number::WithError
Sys::Hostname::FQDN
List::EvenMoreUtils
PerlIO::Layers
Test::LoadAllModules
match::simple
Object::Simple
Tie::Function::Examples
DBD::Mock
Test::Lib
Module::Install::AuthorTests
Hash::FieldHash
B::Tools
HTML::Zoom
Test::Command
Monkey::Patch
HTML::HTML5::Entities
Perl6::Export::Attrs
B::Hooks::OP::Annotation
HTML::HTML5::Parser
Unicode::EastAsianWidth
Text::VisualWidth::PP
Unicode::EastAsianWidth::Detect
Compress::LZF
Caroline
Math::BaseCnv
Crypt::Random::TESHA2
Crypt::Random::Seed
Bytes::Random::Secure
Math::Prime::Util::GMP
Gtk2
MooseX::Types::XMLSchema
Exception::Tiny
Gtk2::Pango
Math::Prime::Util
Task::Kensho
Task::Kensho::WebCrawling
WWW::Selenium
HTTP::Thin
WWW::Mechanize::TreeBuilder
Term::ProgressBar::Quiet
Task::Kensho::Hackery
Task::Kensho::Async
Term::ProgressBar::Simple
Term::ProgressBar
Task::Kensho::Exceptions
Task::Kensho::Logging
Class::MethodMaker
Task::Kensho::Config
Task::Kensho::Dates
Task::Kensho::ExcelCSV
MooseX::SimpleConfig
Spreadsheet::WriteExcel::Simple
Digest::Perl::MD5
Spreadsheet::ParseExcel::Simple
Spreadsheet::ParseExcel
Task::Kensho::Email
Time::Tiny
DateTime::Tiny
Crypt::RC4
MooseX::Exception::Base
Email::MIME::Kit
Devel::StrictMode
OLE::Storage_Lite
Task::Kensho::XML
Task::Kensho::OOP
Task::Moose
Spreadsheet::WriteExcel
XML::SemanticDiff
Types::DateTime
Test::XML
Pod::Coverage::Moose
Test::Fixture::DBIC::Schema
MooseX::Types::Email
URL::Encode
Daemon::Daemonize
File::BaseDir
Unicode::Stringprep
File::DesktopEntry
Parser::MGC
File::MimeInfo::Magic
Test::Distribution
Net::TFTP
Linux::Personality
Tapper::Remote::Config
Tapper::Remote::Net
Devel::Backtrace
Text::Matrix
Perl4::CoreLibs
Shell
Log::Trace
Data::Types
Pod::Plainer
Class::Meta
Business::CreditCard
Class::Meta::Express
Data::Transpose
Text::PDF::TTFont0
PDF::Reuse
Module::Install::Template
MLDBM
Math::Base::Convert
Text::Conjunct
Class::Handle
Object::Pluggable
POE::Component::Syndicator
POE::Filter
POE::Filter::IRCD
DBIx::TransactionManager
Text::Soundex
SQL::QueryMaker
IRC::Utils
POE::Session
String::ShellQuote
POE::Component::IRC
Data::Page::NoTotalEntries
SQL::Maker
Net::Async::Webservice::Common
Bot::BasicBot
Catalyst::Action::REST
Test::Httpd::Apache2
Config::Find
Crypt::SaltedHash
URI::Find::Simple
MP3::Info
URI::Title
Test::Unit::Lite
Exception::Base
Symbol::Util
constant::boolean
Test::Assert
Exception::Died
Exception::Warning
Exception::Fatal
Test::Mock::Class
Test::YAML::Valid
Glib
Glib::Ex::FreezeNotify
Perl6::Slurp
Glib::Ex::SignalIds
Glib::Object::Subclass
Digest::Bcrypt
Class::Accessor::Installer
Pango
Data::Miscellany
Class::Accessor::Complex
Text::Clip
Term::EditorEdit
Data::Inherited
Excel::Template
SQL::Abstract::Plugin::InsertMulti
MooseX::Param
Class::Field
Sub::Override
Mail::POP3Client
DBIx::Inspector
Scalar::Util::Reftype
Net::SMTP::TLS
MooseX::Runnable
Acme::Teddy
File::ReadBackwards
PerlX::Maybe
MojoX::Log::Log4perl
String::Random
DateTime::Format::ISO8601
Log::Log4perl::Appender::TAP
Text::Sprintf::Named
Test::TableDriven
Class::Inner
Data::Lock
ExtUtils::PkgConfig
Cairo
Child
Parallel::Runner
Test::Exception::LessClever
File::Touch
Term::Prompt
Class::Tiny::Antlers
SQL::Abstract::Limit
Devel::Gladiator
Parallel::ForkManager
Minion
Test::Continuous
Net::SFTP::Foreign
Email::Sender::Simple
AWS::Signature4
Rex
Mojolicious::Plugin::Authentication
Log::Log4perl::CommandLine
HTTP::Exception
Pod::Text
Parse::CPAN::MirroredBy
MooseX::FollowPBP
B::Compiling
Pod::Usage
Devel::Declare::Parser
Module::Build::CleanInstall
Spreadsheet::XLSX
String::FriendlyID
TAP::Formatter::TextMate
Module::Metadata
Email::Sender
Apache::Htpasswd
Test::Mock::LWP
Net::AMQP::RabbitMQ
Module::Faker::Dist
Tenjin
Text::Textile
Test::Kit
Text::Xslate
Test::Inline::Extract
TextMate::JumpTo
Pod::Parser
Text::MicroMason
Time::y2038
Tatsumaki::Template undef
XS::Assert
lib::tiny
lib::tiny::findbin
Syntax::Keyword::Gather
PerlIO::locale
Text::Capitalize
lib::xi
Text::Wrap
Text::ParseWords
Task::BeLike::MELO
Parse::Syslog
Net::Inspect
Mock::Module
Class::XSAccessor::Heavy
Devel::hdb
IPC::Open3::Callback
Devel::TraceLoad
Class::PObject
Cache::Mmap
Event::ScreenSaver
HTTP::MobileAgent
XML::Liberal
IPC::Lock::WithTTL
Comment::Block
Task::DualLived
Filesys::POSIX
CPAN::Testers::ParseReport
String::Lookup
Devel::SawAmpersand
define
Test::Easy
YATT
Lexical::SingleAssignment
IO::Vectored
Syntax::Feature::QwComments
autobox::String::Inflector
Package::Strictures
String::Slice
Ask
WebService::Cryptsy
GraphViz2
Kelp::Module::JSON::XS
Encode::HP
Test::Valgrind
Perl6::GatherTake
JSON
Moops
Lexical::Accessor
true
MooX::late
MooseX::MungeHas
Catmandu
JSON::XS
List::Gather
forks
Plack::App::DAIA
Sub::Trigger::Lock
GitHub::Extract
REST::Neo4p
Test::Routine::AutoClear
Beam::Wire
Carp::Notify
Tangence::Server 0.05
Tangence::Registry 0.05
PerlIO::unicodeeol
Ark
DB_File::Lock
Google::ProtocolBuffers
Time::Moment
UnderscoreJS
underscore
Kafka
Queue::Q
Data::Undump
McBain
Resque
CHI::Memoize
Path::Mapper
MooseX::Getopt::Usage
Business::CompanyDesignator
MooseX::XSAccessor
Tangence::ObjectProxy
CGI::Application::PSGI
Catmandu::RDF
GraphViz::Makefile
GraphViz2::Marpa
Unix::Lsof
XT::Manager
DBIx::Class::FormTools
Class::ArrayObjects
Stream::Aggregate
PerlIO::text
DBIx::Custom
HTML::TagHelper
CHI::Cascade
PAUSE::Packages
B::Tap
Beam::Wire::Moose
Loop::Sustainable
Debuggit
MooX::Types::CLike
Fun
IO::Compress::Lzf
Session::Token
Plack::Middleware::Profiler::NYTProf
Config::ENV
Math::Decimal64
Linux::Setns
Statistics::LineFit
PAUSE::Permissions
Cache::Redis
File::Tee
Data::BitStream
Google::Data::JSON
Test::MemoryGrowth
Tie::DBIx::Class
I22r::Translate
HTML::Zoom::Parser::HH5P
Seis
Error::Pure::Output::JSON
W3C::SOAP
Parser::Combinators
returning
MooseX::Validation::Doctypes
XS::TCC
HTML::Grabber
Document::eSign::Docusign
Params::Lazy
Geo::Proj4
Tangence::Server::Context 0.05
Tangence::Stream 0.05
Tapper::CLI::API undef
Tapper::CLI::API::Command::download undef
Tapper::CLI::API::Command::upload undef
Tapper::CLI::DbDeploy undef
Tapper::CLI::DbDeploy::Command::init undef
Tapper::CLI::DbDeploy::Command::makeschemadiffs undef
Tapper::CLI::DbDeploy::Command::saveschema undef
Tapper::CLI::DbDeploy::Command::upgrade undef
Tapper::CLI::Testrun undef
Tapper::CLI::Testrun::Command::delete undef
Tapper::CLI::Testrun::Command::deletehost undef
Tapper::CLI::Testrun::Command::deleteprecondition undef
Tapper::CLI::Testrun::Command::deletequeue undef
Module::Extract::Namespaces
Tapper::CLI::Testrun::Command::freehost undef
Tapper::CLI::Testrun::Command::list undef
Tapper::CLI::Testrun::Command::listhost undef
Tapper::CLI::Testrun::Command::listprecondition undef
Tapper::CLI::Testrun::Command::listqueue undef
Tapper::CLI::Testrun::Command::new undef
Tapper::CLI::Testrun::Command::newhost undef
Tapper::CLI::Testrun::Command::newprecondition undef
Tapper::CLI::Testrun::Command::newqueue undef
Tapper::Installer::Base undef
Tapper::CLI::Testrun::Command::newscenario undef
Tapper::CLI::Testrun::Command::newtestplan undef
Tapper::CLI::Testrun::Command::rerun undef
Tapper::CLI::Testrun::Command::show undef
Tapper::CLI::Testrun::Command::updatehost undef
Tapper::CLI::Testrun::Command::updateprecondition undef
Tapper::CLI::Testrun::Command::updatequeue undef
Tapper::Installer::Precondition undef
Tapper::Installer::Precondition::Copyfile undef
Tapper::Installer::Precondition::Exec undef
Tapper::Installer::Precondition::Fstab undef
Tapper::Installer::Precondition::Image undef
Tapper::Installer::Precondition::Kernelbuild undef
Tapper::Installer::Precondition::PRC undef
Tapper::Installer::Precondition::Package undef
Tapper::Installer::Precondition::Rawimage undef
Tapper::Installer::Precondition::Repository undef
Tapper::Installer::Precondition::Simnow undef
Tapper::PRC::Testcontrol undef
Tapper::Reports::API::Daemon undef
Tapper::Reports::Receiver::Daemon undef
Tapper::Reports::Receiver::Util undef
Tapper::Schema::ReportsDB 3.000001
Tapper::Schema::ReportsDB::Result::Report undef
Tapper::Schema::ReportsDB::Result::ReportComment undef
Tapper::Schema::ReportsDB::Result::ReportFile undef
Tapper::Schema::ReportsDB::Result::ReportSection undef
Tapper::Schema::ReportsDB::Result::ReportTopic undef
Tapper::Schema::ReportsDB::Result::ReportgroupArbitrary undef
Tapper::Schema::ReportsDB::Result::ReportgroupTestrun undef
Tapper::Schema::ReportsDB::Result::ReportgroupTestrunStats undef
Tapper::Schema::ReportsDB::Result::Suite undef
Tapper::Schema::ReportsDB::Result::Tap undef
Tapper::Schema::ReportsDB::Result::User undef
Tapper::Schema::ReportsDB::Result::View010TestrunOverviewReports undef
Tapper::Schema::ReportsDB::Result::View020TestrunOverview undef
Tapper::Schema::ReportsDB::ResultSet::ReportgroupTestrun undef
Tapper::Schema::TestrunDB::Result::Host undef
Tapper::Schema::TestrunDB::Result::HostFeature undef
Tapper::Schema::TestrunDB::Result::Message undef
Tapper::Schema::TestrunDB::Result::PrePrecondition undef
Test::LectroTest
Proc::Guard
Tapper::Schema::TestrunDB::Result::Precondition undef
Tapper::Schema::TestrunDB::Result::Preconditiontype undef
Tapper::Schema::TestrunDB::Result::Queue undef
Tapper::Schema::TestrunDB::Result::QueueHost undef
Tapper::Schema::TestrunDB::Result::Scenario undef
Tapper::Schema::TestrunDB::Result::ScenarioElement undef
Tapper::Schema::TestrunDB::Result::State undef
Tapper::Schema::TestrunDB::Result::TestplanInstance undef
Tapper::Schema::TestrunDB::Result::Testrun undef
Tapper::Schema::TestrunDB::Result::TestrunPrecondition undef
Tapper::Schema::TestrunDB::Result::TestrunRequestedFeature undef
Tapper::Schema::TestrunDB::Result::TestrunRequestedHost undef
Tapper::Schema::TestrunDB::Result::TestrunScheduling undef
Tapper::Schema::TestrunDB::Result::Topic undef
Tapper::Schema::TestrunDB::Result::User undef
Tapper::Schema::TestrunDB::ResultSet::Host undef
Tapper::Schema::TestrunDB::ResultSet::Precondition undef
Tapper::Schema::TestrunDB::ResultSet::Queue undef
Tapper::Schema::TestrunDB::ResultSet::Testrun undef
Tapper::Schema::TestrunDB::ResultSet::TestrunScheduling undef
Tapper::Testplan::Generator undef
Tapper::Testplan::Plugins::Taskjuggler undef
Tapper::Testplan::Reporter undef
Tapper::Testplan::Reporter::Plugins 3.000001
Task::Deprecations::5_12 1.002
Tatsumaki::Application undef
Tatsumaki::Error undef
Tatsumaki::HTTPClient undef
Tatsumaki::Handler undef
Tatsumaki::MessageQueue undef
Tatsumaki::Request undef
Tatsumaki::Response undef
Tatsumaki::Server undef
Tatsumaki::Service undef
Tatsumaki::Template::Micro undef
TeX::AutoTeX 0.9
TeX::AutoTeX::ConcatPDF v0.9
TeX::AutoTeX::Config 1.7.2.7
TeX::AutoTeX::Exception 1.7.2.5
TeX::AutoTeX::File 1.36.2.5
TeX::AutoTeX::Fileset 1.8.2.4
TeX::AutoTeX::HyperTeX 1.10.2.6
TeX::AutoTeX::Log 1.10.2.4
TeX::AutoTeX::Mail 1.5.2.3
TeX::AutoTeX::PostScript 1.11.2.4
Test::Aggregate::Nested 0.364
TeX::AutoTeX::Process 1.14.2.5
TeX::AutoTeX::StampPDF 1.9.2.5
TeX::DVI::Parse 1.01
TeX::Hyphen::Pattern::Af_ZA 0.04
TeX::Hyphen::Pattern::Bg 0.00
TeX::Hyphen::Pattern::Ca 0.04
TeX::Hyphen::Pattern::Cs 0.04
TeX::Hyphen::Pattern::Cy 0.04
TeX::Hyphen::Pattern::Da 0.04
TeX::Hyphen::Pattern::De_1901 0.04
Test::CGI::Multipart 0.0.3
TeX::Hyphen::Pattern::De_1996 0.04
TeX::Hyphen::Pattern::De_DE 0.04
Convert::Color
TeX::Hyphen::Pattern::De_ch_1901 0.04
TeX::Hyphen::Pattern::El_monoton 0.00
TeX::Hyphen::Pattern::El_polyton 0.00
TeX::Hyphen::Pattern::En_gb 0.04
TeX::Hyphen::Pattern::En_us 0.04
Tree::Simple::Visitor
TeX::Hyphen::Pattern::Es 0.04
TeX::Hyphen::Pattern::Et 0.04
TeX::Hyphen::Pattern::Eu 0.04
TeX::Hyphen::Pattern::Fi 0.04
TeX::Hyphen::Pattern::Fr 0.04
TeX::Hyphen::Pattern::Ga 0.04
TeX::Hyphen::Pattern::Gl 0.04
TeX::Hyphen::Pattern::Grc 0.00
TeX::Hyphen::Pattern::Hr 0.04
TeX::Hyphen::Pattern::Hsb 0.04
TeX::Hyphen::Pattern::Hu 0.04
TeX::Hyphen::Pattern::Ia 0.04
TeX::Hyphen::Pattern::Id 0.04
TeX::Hyphen::Pattern::Is 0.00
TeX::Hyphen::Pattern::It 0.04
TeX::Hyphen::Pattern::Kmr 0.04
TeX::Hyphen::Pattern::La 0.04
TeX::Hyphen::Pattern::Lt 0.04
TeX::Hyphen::Pattern::Lv 0.04
TeX::Hyphen::Pattern::Mn_cyrl 0.00
TeX::Hyphen::Pattern::Nb 0.04
TeX::Hyphen::Pattern::Nl 0.04
TeX::Hyphen::Pattern::Nn 0.04
TeX::Hyphen::Pattern::No 0.04
TeX::Hyphen::Pattern::Pl 0.04
TeX::Hyphen::Pattern::Pt 0.04
TeX::Hyphen::Pattern::Pt_BR 0.04
TeX::Hyphen::Pattern::Ro 0.04
TeX::Hyphen::Pattern::Ru 0.00
TeX::Hyphen::Pattern::Sa 0.00
TeX::Hyphen::Pattern::Sh 0.04
TeX::Hyphen::Pattern::Sh_cyrl 0.00
TeX::Hyphen::Pattern::Sh_latn 0.04
TeX::Hyphen::Pattern::Sk 0.04
TeX::Hyphen::Pattern::Sl 0.04
TeX::Hyphen::Pattern::Sl_SI 0.04
TeX::Hyphen::czech 0.121
TeX::Hyphen::german 0.121
Tee::App 0.14
Tempest::Gd undef
Tempest::Graphicsmagick undef
Tempest::Imagemagick undef
Template::Alloy::Compile undef
Template::Alloy::Context undef
Template::Alloy::Exception undef
Template::Alloy::HTE undef
Template::Alloy::Iterator undef
Template::Alloy::Operator undef
Template::Alloy::Parse undef
Template::Alloy::Play undef
Template::Alloy::Stream undef
Template::Alloy::TT undef
Template::Alloy::Tmpl undef
HTML::Scrubber
Template::Alloy::VMethod undef
Template::Alloy::Velocity undef
Template::AutoFilter::Parser 0.110080
Template::Base 2.78
Template::Benchmark::Engine 1.09_01
Template::Benchmark::Engines::HTMLMacro 1.09_01
Template::Benchmark::Engines::HTMLMason 1.09_01
Template::Benchmark::Engines::HTMLTemplate 1.09_01
Template::Benchmark::Engines::HTMLTemplateCompiled 1.09_01
Template::Benchmark::Engines::HTMLTemplateExpr 1.09_01
Template::Benchmark::Engines::HTMLTemplateJIT 1.09_01
Template::Benchmark::Engines::HTMLTemplatePro 1.09_01
Template::Benchmark::Engines::MojoTemplate 1.09_01
Template::Benchmark::Engines::NTSTemplate 1.09_01
Template::Benchmark::Engines::ParseTemplate 1.09_01
Template::Benchmark::Engines::Solution 1.09_01
Template::Benchmark::Engines::TemplateAlloyHT 1.09_01
Template::Benchmark::Engines::TemplateAlloyTT 1.09_01
Template::Benchmark::Engines::TemplateSandbox 1.09_01
Template::Benchmark::Engines::TemplateTiny 1.09_01
Template::Benchmark::Engines::TemplateToolkit 1.09_01
Template::Benchmark::Engines::Tenjin 1.09_01
Template::Benchmark::Engines::TextClearSilver 1.09_01
Template::Benchmark::Engines::TextClevery 1.09_01
Template::Benchmark::Engines::TextClevy 1.08_01
Template::Benchmark::Engines::TextMicroMasonHM 1.09_01
Template::Benchmark::Engines::TextMicroMasonTeTe 1.09_01
Template::Benchmark::Engines::TextMicroTemplate 1.09_01
Template::Benchmark::Engines::TextMicroTemplateExtended 1.09_01
Template::Benchmark::Engines::TextTemplate 1.09_01
Template::Benchmark::Engines::TextTemplateSimple 1.09_01
Template::Benchmark::Engines::TextTemplet 1.09_01
Template::Mustache v0.5.1
Template::Benchmark::Engines::TextTmpl 1.09_01
Template::Benchmark::Engines::TextXslate 1.09_01
Template::Config 2.75
Template::Benchmark::Engines::TextXslateTT 1.09_01
Template::Context 2.98
Template::Constants 2.75
Template::Declare::Buffer undef
Template::Declare::TagSet undef
Template::Declare::TagSet::HTML undef
Template::Declare::TagSet::RDF undef
Template::Declare::TagSet::RDF::EM undef
Template::Directive 2.2
Template::Declare::TagSet::XUL undef
Template::Exception 2.7
Template::Document 2.79
Template::Extract::Compile 0.41
Template::Extract::Parser 0.41
Template::Filters 2.87
Template::Extract::Run 0.41
Template::Flute::Config undef
Template::Flute::Container undef
Template::Flute::Database::Rose undef
Template::Flute::Form undef
Template::Flute::HTML undef
Template::Flute::HTML::Table undef
Template::Flute::I18N undef
Template::Flute::Increment undef
Template::Flute::Iterator undef
Template::Flute::Iterator::JSON undef
Template::Flute::Iterator::Rose undef
Template::Flute::List undef
Template::Flute::Specification undef
Template::Flute::Specification::Scoped undef
Template::Flute::Specification::XML undef
Template::Flute::Utils undef
Template::Grammar 2.25
Template::Iterator 2.68
Template::Parser 2.89
Template::Plugin 2.7
Template::Plugin::DBI 2.65
Template::Namespace::Constants 1.27
Template::Plugin::Assert 1
Template::Plugin::CGI 2.7
Template::Plugin::Catalyst::View::PDF::Reuse undef
Template::Plugin::Catalyst::View::PDF::Reuse::Barcode undef
Template::Plugin::Datafile 2.72
Template::Plugin::Date 2.78
Template::Plugin::Directory 2.7
Template::Plugin::Dumper 2.7
Template::Plugin::Filter 1.38
Template::Plugin::File 2.71
Template::Plugin::Format 2.7
DateTime::Format::HTTP
Template::Plugin::HTML 2.62
Template::Plugin::Image 1.21
Template::Plugin::Iterator 2.68
Template::Plugin::Math 1.16
Template::Plugin::Pod 2.69
Template::Plugin::Procedural 1.17
Template::Plugin::PodGenerated 0.05
Template::Plugin::Scalar 1
Template::Plugin::String 2.4
Template::Plugin::Table 2.71
Template::Plugin::TwoStage::Test 0.06
Template::Plugin::URL 2.74
Template::Plugin::VMethods::VMethodContainer undef
Template::Plugin::View 2.68
Template::Plugin::Wrap 2.68
Template::Plugin::encoding 0.02
Template::Plugins 2.77
Template::Preprocessor::TTML::Base undef
Template::Provider 2.94
Template::Preprocessor::TTML::CmdLineProc undef
Template::Sandbox::Library 1.04_01
Template::Sandbox::NumberFunctions 1.04_01
Template::Sandbox::StringFunctions 1.04_01
Template::Semantic::Document undef
Template::Semantic::Filter undef
Template::Stash 2.91
Template::Service 2.8
Template::Stash::AutoEscaping::Escaped::Base undef
Template::Stash::AutoEscaping::Escaped::HTML undef
Template::Stash::AutoEscaping::Escaped::YourCode undef
Template::Stash::AutoEscaping::RawString undef
Template::Stash::Context 1.63
Template::Stash::ForceUTF8 0.03
Template::Test 2.75
Template::Stash::XS undef
Template::VMethods 2.16
Template::View 2.91
TemplateM::Galore 2.2
TemplateM::GaloreWin32 2.21
TemplateM::Simple 2.2
TemplateM::Util 2.2
Teng::Iterator undef
Teng::Plugin::BulkInsert undef
Teng::Plugin::Count undef
Teng::Plugin::FindOrCreate undef
Teng::Plugin::Pager undef
Teng::Plugin::Pager::MySQLFoundRows undef
Teng::Plugin::Replace undef
Teng::QueryBuilder undef
Teng::Row undef
Teng::Schema undef
Teng::Schema::Declare undef
Teng::Schema::Dumper undef
Teng::Schema::Loader undef
Teng::Schema::Table undef
Tenjin::Context 0.070001
Term::ANSIColor 3.00
Tenjin::Preprocessor 0.070001
Net::HTTP
LWP::Protocol::https
Tenjin::Template 0.070001
Tenjin::Util 0.070001
Test::Assertions::TestScript 1.018
Term::ANSIColorx::AutoFilterFH 2.7185
Term::Clui::FileSelect 1.64
Term::Complete 1.402
Term::Completion::Multi 0.90
Term::Completion::Path 0.90
Term::Completion::_POSIX undef
Term::Completion::_readkey undef
Term::Completion::_stty undef
Term::ReadLine 1.07
Term::Completion::_termsize undef
Data::Entropy::Algorithms
Term::ReadKey 2.30
Authen::Passphrase::RejectAll
Term::EditorEdit::Edit undef
Term::ExtendedColor::Xresources::Colorscheme 0.003
Term::HiliteDiff::_impl 0.10
Term::ReadLine::Gnu::XS 1.17
Dist::Zilla::Role::VersionProvider
Term::UI::History undef
Devel::PPPort
Test::Able::Cookbook undef
Test::Able::FatalException undef
Test::Able::Helpers undef
Test::Able::Method::Array undef
Test::Able::Object undef
Test::Able::Planner undef
Test::Able::Role undef
Test::Able::Role::Meta::Class undef
Test::Able::Role::Meta::Method undef
Test::Able::Runner::Role::Meta::Class 1.001
Test::Aggregate::Base 0.364
Test::Base::Filter undef
Test::Aggregate::Builder 0.364
Test::Apache::RewriteRules::ClientEnvs undef
Test::Bot::BasicBot::Pluggable 0.91
Test::Bot::BasicBot::Pluggable::Store 0.91
Test::Builder 0.98
Test::Builder::Mock::Class::Role::Meta::Class 0.0203
Test::Builder::Mock::Class::Role::Object 0.0203
Test::Builder::Module 0.98
Test::Builder::Tester 1.22
Test::Builder::Tester::Color 1.22
Test::CompanionClasses::Base 1.101370
Test::CompanionClasses::Engine 1.101370
Test::CGI::Multipart::Gen::Image 0.0.3
Test::DZil 4.200006
MongoDB
Test::CGI::Multipart::Gen::Text 0.0.3
Test::CPAN::Meta::JSON::Version 0.10
Test::CPAN::Meta::Version 0.18
Test::CPAN::Meta::YAML::Version 0.17
Test::Class::Load 0.35
Test::Class::MethodInfo 0.34
Test::Classy::Base undef
Test::Cmd::Common 1.05
Test::CompanionClasses::Engine_TEST 1.101370
Test::Cukes::Feature undef
Test::Cukes::Scenario undef
Test::DBIx::Class::Example::Schema undef
Test::DBIx::Class::Example::Schema::DefaultRS undef
Test::DBIx::Class::Example::Schema::Result undef
Test::DBIx::Class::Example::Schema::Result::CD undef
Test::DBIx::Class::Example::Schema::Result::CD::Artist undef
Test::DBIx::Class::Example::Schema::Result::CD::Track undef
Test::DBIx::Class::Example::Schema::Result::Company undef
Test::DBIx::Class::Example::Schema::Result::Company::Employee undef
Test::DBIx::Class::Example::Schema::Result::Job undef
Test::DBIx::Class::Example::Schema::Result::Person undef
Test::DBIx::Class::Example::Schema::Result::Person::Artist undef
Test::DBIx::Class::Example::Schema::Result::Person::Employee undef
Test::DBIx::Class::Example::Schema::Result::Phone undef
Test::DBIx::Class::Example::Schema::ResultSet undef
Test::DBIx::Class::FixtureCommand::Populate undef
Test::DBIx::Class::FixtureCommand::PopulateMore undef
Test::DBIx::Class::Role::FixtureCommand undef
Test::DBIx::Class::SchemaManager undef
Test::DBIx::Class::SchemaManager::Trait::SQLite undef
Test::DBIx::Class::SchemaManager::Trait::Testmysqld undef
Test::DBIx::Class::SchemaManager::Trait::Testpostgresql undef
Test::DBIx::Class::Types undef
SQL::ReservedWords
Log::Dispatch::Output
Test::Data::Array 1.22
Test::Data::Function 1.22
Test::Data::Scalar 1.22
Test::Data::Hash 1.22
Test::Database::Driver undef
Test::Database::Driver::CSV undef
Test::Database::Driver::DBM undef
Test::Database::Driver::Pg undef
Test::Database::Driver::SQLite undef
Test::Database::Driver::SQLite2 undef
Test::Database::Driver::mysql undef
Test::Database::Handle undef
Test::Database::Util undef
Test::Deep::All undef
Test::Deep::Any undef
Test::Deep::Array undef
Test::Deep::ArrayEach undef
Test::Deep::ArrayElementsOnly undef
Test::Deep::ArrayLength undef
Test::Deep::ArrayLengthOnly undef
Test::Deep::Blessed undef
Test::Deep::Boolean undef
Test::Deep::Cache undef
Test::Deep::Cache::Simple undef
Test::Deep::Class undef
Test::Deep::Cmp undef
Test::Deep::Code undef
Test::Deep::Hash undef
Test::Deep::HashEach undef
Test::Deep::HashElements undef
Test::Deep::HashKeys undef
Test::Deep::HashKeysOnly undef
Test::Deep::Ignore undef
Test::Deep::Isa undef
Test::Deep::ListMethods undef
Test::Deep::MM undef
Test::Deep::Methods undef
Test::Deep::NoTest undef
Test::Deep::Number undef
Test::Deep::Ref undef
Test::Deep::RefType undef
Test::Deep::Regexp undef
Test::Deep::RegexpMatches undef
Test::Deep::RegexpRef undef
Test::Deep::RegexpRefOnly undef
Test::Deep::RegexpVersion undef
Test::Deep::ScalarRef undef
Test::Deep::ScalarRefOnly undef
Test::Deep::Set undef
Test::Deep::Shallow undef
Test::Deep::Stack undef
Test::Deep::String undef
Test::Dependencies::Heavy undef
Test::Dependencies::Light undef
Test::Dir::Base 1.005
Test::Excel::Template::Plus 0.02
Test::FITesque::Fixture undef
Test::FITesque::Suite undef
Test::FITesque::Test undef
Test::File::Cleaner::State 0.03
Test::File::ShareDir::TempDirObject 0.1.1
Test::Fixture::DBI::Util 0.01
Test::Fixture::DBI::Util::SQLite 0.01
Test::Fixture::DBI::Util::mysql 0.02
Test::Folder 1.007
Test::Git 1.01
Test::Group::NoWarnings 0.01
Test::Group::Plan 0.01
Test::Group::Tester 0.01
Test::HTTP::Syntax undef
Test::Harness::Results 0.01
Test::Inline::Content 2.212
Test::Inline::Content::Default 2.212
Test::Inline::Content::Legacy 2.212
Test::Mini::Unit v1.0.3
Digest::CRC
Test::Inline::Content::Simple 2.212
Test::Inline::IO::File 2.212
Test::Inline::Script 2.212
Test::Inline::Section 2.212
Test::Inline::Util 2.212
Test::JSON::Meta::Version 0.09
Test::Lazy::Template undef
Test::Lazy::Tester undef
Test::LeakTrace::Script undef
Test::LectroTest::Compat undef
Test::LectroTest::FailureRecorder undef
Test::LectroTest::Generator 0.14
Test::LectroTest::Property undef
Test::LectroTest::TestRunner undef
Test::Magpie::ArgumentMatcher 0.05
Test::Magpie::Inspect 0.05
Test::Magpie::Invocation 0.05
Test::Magpie::Meta::Class 0.05
Test::Magpie::Mock 0.05
Test::Magpie::Role::HasMock 0.05
Test::Magpie::Role::MethodCall 0.05
Test::Magpie::Spy 0.05
Test::Magpie::Stub 0.05
Test::Magpie::Types 0.05
Test::Magpie::Util 0.05
Test::Magpie::When 0.05
Test::Markdent 0.17
Test::Mini::Assertions undef
Test::Mini::Logger undef
Test::Mini::Logger::TAP undef
Test::Mini::Runner undef
Test::Mini::TestCase undef
Test::Mini::Unit::SharedBehavior undef
Test::Mini::Unit::Sugar::Advice undef
Test::Mini::Unit::Sugar::Reuse undef
Test::Mini::Unit::Sugar::Shared undef
Test::Mini::Unit::Sugar::Test undef
Test::Perl::Critic::Policy 1.115
Test::Mini::Unit::Sugar::TestCase undef
Test::Mock::Class::Role::Meta::Class 0.0303
Test::Mock::Class::Role::Object 0.0303
Test::Mock::HTTP::Request 0.01
Test::Mock::LWP::UserAgent 0.01
Test::MockObject::Extends 1.09
Test::Mock::HTTP::Response 0.01
Test::Moose 2.0002
Test::Mojo undef
Test::MooseX::Daemonize 0.12
Test::Prereq::Build 1.037_02
Test::More 0.98
Test::Most::Exception 0.23
Test::Mouse undef
Test::Nginx::LWP 0.13
Test::Nginx::Socket 0.13
Test::Nginx::Util 0.13
Test::Path::Router 0.10
Test::NoPlan 0.0.6
Test::NoWarnings::Warning 1.02
Test::Object::Test 0.07
Crypt::Random::Source
Test::Output::Tie 0.14
Test::Override::UserAgent::Scope 0.004
Test::POP3 0.05
Test::Pockito::DefaultMatcher undef
Test::Pockito::Exported undef
Test::Pockito::Moose::Role undef
Dist::Zilla::Plugin::MetaData::BuiltWith
Test::RDF::Trine::Store undef
Module::Extract::Use
Test::Ranger 0.0.4
Test::Ranger::List 0.0.4
Test::Reporter::Transport 1.57
Test::Reporter::Transport::File 1.57
Test::Reporter::Transport::HTTPGateway 1.57
Test::Reporter::Transport::Mail::Send 1.57
Test::Reporter::Transport::Net::SMTP 1.57
Test::Reporter::Transport::Net::SMTP::TLS 1.57
Test::Rest::Commands undef
Test::Rest::Context undef
Test::Routine::Common 0.009
MooseX::Getopt::OptionTypeMap
Test::Routine::Compositor 0.009
Test::Routine::Manual::Demo 0.009
Test::Routine::Runner 0.009
Test::Routine::Util 0.009
Test::Routine::Test 0.009
Test::Run::Core 0.0123
Test::Run::Assert 0.03
Test::Run::Base undef
Test::Run::Base::PlugHelpers undef
Test::Weaken::Gtk2 37
Test::Run::Base::Plugger undef
Test::Run::CmdLine::Trap::ProveApp undef
Test::Run::Base::Struct undef
Test::Run::Class::Hierarchy undef
Test::Run::CmdLine::Drivers::Default undef
Test::Run::CmdLine::Iface undef
Test::Run::CmdLine::Plugin::BreakOnFailure 0.0.1
Test::Run::CmdLine::Prove 0.0120
Test::Run::CmdLine::Prove::App undef
Test::Run::CmdLine::Trap::Prove undef
Test::Run::Core_GplArt undef
Test::Run::Iface undef
Test::Run::Obj 0.0121
Test::Run::Obj::CanonFailedObj undef
Test::Run::Obj::Error undef
Test::Run::Obj::FailedObj undef
Test::Run::Obj::IntOrUnknown undef
Test::Run::Obj::IntOrUnknown::Moose 0.0121
Test::Run::Obj::TestObj undef
Test::Run::Obj::TotObj undef
Test::Run::Output undef
Test::Run::Plugin::BreakOnFailure 0.0.1
Test::Run::Plugin::CmdLine::Output undef
Test::Run::Sprintf::Named::FromAccessors undef
Test::Run::Straps::Base undef
Test::Run::Straps::EventWrapper undef
Test::Run::Straps::StrapsDetailsObj undef
Test::Run::Straps::StrapsTotalsObj undef
Test::Run::Straps_GplArt undef
Test::Run::Trap::Obj undef
Test::SQL::Translator 1.59
Test::SharedFork::Array undef
Test::SharedFork::Scalar undef
Test::SharedFork::Store undef
Test::Smoke::BuildCFG 0.009
Test::Smoke::FTPClient 0.011
Test::Smoke::Mailer 0.014
Locale::Currency
Plack::Test
Test::Smoke::Patcher 0.011
Test::Smoke::Policy 0.004
Test::Smoke::Reporter 0.035
Test::Smoke::Smoker 0.045
Crypt::OpenSSL::Random
Test::Smoke::SourceTree 0.008
Crypt::OpenSSL::RSA
Test::Smoke::Syncer 0.027
Exporter::Easy
Test::More::UTF8
Test::Smoke::SysInfo 0.042
Test::Smoke::Util 0.58
Convert::Color
Test::Taint
SQL::Translator::Schema::Table
Test::Story::File undef
Test::Story::Fixture undef
Digest::MD4
IO::Async::Test
Test::Story::Fixture::Selenium undef
Test::Pretty
Test::Story::Fixture::VMWare 0.01
Test::Story::TestCase undef
Test::Story::Tutorial undef
Test::StructuredObject::CodeStub 0.01000009
Test::StructuredObject::NonTest 0.01000009
Test::StructuredObject::SubTest 0.01000009
Test::StructuredObject::Test 0.01000009
Test::Tolerant 1.701
Test::StructuredObject::TestSuite 0.01000009
Test::Suite 0.032
Test::Sweet::Keyword::Test undef
Test::Sweet::Types undef
Crypt::DES
Test::Sys::Info::Driver 0.20
Test::TempDir::Factory undef
Test::TempDir::Handle undef
KiokuX::User
HTTP::Lite
Data::Integer
Test::Tester::Capture undef
Crypt::Rijndael
MooseX::Types::Authen::Passphrase
Test::Trap::Builder 0.2.1
Test::Trap::Builder::PerlIO 0.2.1
Test::Trap::Builder::SystemSafe 0.2.1
Test::Trap::Builder::TempFile 0.2.1
Test::Unit::Assert undef
Test::Unit::Assertion undef
Test::Unit::Assertion::Boolean undef
Plack::App::URLMap
Plack::Middleware::Auth::Basic
Encode
Test::Unit::Assertion::CodeRef undef
Test::Unit::Assertion::Exception undef
Test::Unit::Assertion::Regexp undef
Test::Unit::Debug undef
Test::Unit::Decorator undef
Test::Unit::Error undef
Test::Unit::Exception undef
Test::Unit::Failure undef
Test::Unit::HarnessUnit undef
Test::Unit::Listener undef
Test::Unit::Loader undef
Test::Unit::Procedural undef
Test::Unit::Result undef
Test::Unit::Runner undef
Test::Unit::Runner::Terminal undef
Test::Unit::Setup undef
Test::Unit::Test undef
Digest::MD5::File
Net::Amazon::S3
Test::Unit::TestCase undef
Plack::Middleware::ProxyMap
Test::Unit::TestRunner undef
Ouch
Test::Unit::TestSuite undef
Test::Unit::TkTestRunner undef
Test::Unit::Tutorial undef
Test::PAUSE::Permissions
Test::Unit::UnitHarness 1.1502
Test::Unit::Warning undef
Test::Without::Gtk2Things 37
Test::Workflow undef
Test::Workflow::Block undef
Test::Workflow::Layer undef
Test::Workflow::Meta undef
Test::Workflow::Test undef
DateTime::Format::Oracle
Convert::NLS_DATE_FORMAT
Test::XML::Assert 0.03
Test::XML::SAX 0.01
Test::YAML 0.72
Test::XML::Twig 0.01
Test::XML::XPath 0.03
PkgConfig
Test::XML::Compare
DBIx::Schema::DSL
IO::Storm
Sereal::Splitter
DestructAssign
Statistics::R::IO
Mojolicious::Plugin::MethodOverride
Expect
Memory::Leak::Hunter
Rose::Object
Rose::DateTime
Rose::URI
Rose::DB
Time::Clock
Net::HTTPS::Any
return::thence
lib::filter

=head1 CONFIGURATION

Summary of my perl5 (revision 5 version 13 subversion 2) configuration:
  Commit id: e0de7c21b08329275a76393ac4d80aae90ac428e
  Platform:
    osname=linux, osvers=2.6.32-2-amd64, archname=x86_64-linux
    uname='linux k81 2.6.32-2-amd64 #1 smp fri feb 12 00:01:47 utc 2010 x86_64 gnulinux '
    config_args='-Dprefix=/home/src/perl/repoperls/installed-perls/perl/v5.13.2-213-ge0de7c2 -Dinstallusrbinperl=n -Uversiononly -Dusedevel -des -Ui_db'
    hint=recommended, useposix=true, d_sigaction=define
    useithreads=undef, usemultiplicity=undef
    useperlio=define, d_sfio=undef, uselargefiles=define, usesocks=undef
    use64bitint=define, use64bitall=define, uselongdouble=undef
    usemymalloc=n, bincompat5005=undef
  Compiler:
    cc='cc', ccflags ='-fno-strict-aliasing -pipe -fstack-protector -I/usr/local/include -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64',
    optimize='-O2',
    cppflags='-fno-strict-aliasing -pipe -fstack-protector -I/usr/local/include'
    ccversion='', gccversion='4.4.4', gccosandvers=''
    intsize=4, longsize=8, ptrsize=8, doublesize=8, byteorder=12345678
    d_longlong=define, longlongsize=8, d_longdbl=define, longdblsize=16
    ivtype='long', ivsize=8, nvtype='double', nvsize=8, Off_t='off_t', lseeksize=8
    alignbytes=8, prototype=define
  Linker and Libraries:
    ld='cc', ldflags =' -fstack-protector -L/usr/local/lib'
    libpth=/usr/local/lib /lib /usr/lib /lib64 /usr/lib64
    libs=-lnsl -lgdbm -ldb -ldl -lm -lcrypt -lutil -lc -lgdbm_compat
    perllibs=-lnsl -ldl -lm -lcrypt -lutil -lc
    libc=/lib/libc-2.11.2.so, so=so, useshrplib=false, libperl=libperl.a
    gnulibc_version='2.11.2'
  Dynamic Linking:
    dlsrc=dl_dlopen.xs, dlext=so, d_dlsymun=undef, ccdlflags='-Wl,-E'
    cccdlflags='-fPIC', lddlflags='-shared -O2 -L/usr/local/lib -fstack-protector'

=head1 AUTHOR

This Bundle has been generated automatically by the autobundle routine in CPAN.pm.
