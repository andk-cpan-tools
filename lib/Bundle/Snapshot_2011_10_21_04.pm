package Bundle::Snapshot_2011_05_03_04;

$VERSION = '0.01';

1;

__END__

=head1 NAME

Bundle::Snapshot_2011_05_03_04 - Snapshot of installation on k81 on 2016-06-17

=head1 SYNOPSIS

perl -MCPAN -e 'install Bundle::Snapshot_2011_05_03_04'

=head1 CONTENTS

NetAddr::MAC 0.71
NetAthlon2::RAW 0.31
NetHack::Item 0.13
NetPacket 1.1.1
NexposeSimpleXML::Parser undef
Nginx::Engine 0.05
App::PNGCrush
Nginx::Engine::Cookies::Resolver 0.02
Nginx::Simple 0.06
Syntax::Highlight::CSS
Syntax::Highlight::HTML
POE::Component::NonBlockingWrapper::Base
WWW::Cache::Google
WWW::DoctypeGrabber
WWW::HTMLTagAttributeCounter
WWW::Lipsum
WWW::WebDevout::BrowserSupportInfo
WebService::HtmlKitCom::FavIconFromImage
POE::Component::CSS::Minifier
Nmap::Parser 1.2
Norma 0.02
Number::Convert 1.0
Number::Extreme 0.29
Number::Format::SouthAsian 0.07
Number::Tolerant 1.701
Number::Interval 0.05
Number::Phone::AU 0.02
POE::Component::Generic 0.1205
Number::Phone::FR 0.01
Number::Phone::JP 0.20110401
ODF::lpOD 1.110
OODoc::Template 0.14
ORDB::CPANUploads 1.06
ORLite::Pod 0.10
ORLite::Statistics 0.03
ORMesque 1.110422
OSS::LDAPops 1.024
OWL::Simple::Parser 0.09
Object::Annotate 0.021
Object::Array 0.060
Object::Episode 0.02
Exporter::Autoclean
Object::GlobalContainer 0.01
Object::Lazy 0.09
Object::Method 0.01
Object::New 0.03
Object::WithParams 0.3
Object::eBay 0.5.0
ObjectDBI 0.15
Objects::Collection 0.37
Alien::libtickit
Ogg::LibOgg 0.01
Oogly 0.31
Org::Parser 0.10
Oogly::Aagly 0.05
OpenOffice::OODoc 2.125
OpenOffice::Parse::SXC 0.03
POE::Component::FeedAggregator 0.902
OpenOffice::Wordlist 0.04
Operator::Util 0.03
Opt::Imistic 0.03
Ormlette 0.002
PAR::Repository::Web 0.03
PAR::WebStart 0.20
PApp::SQL 1.05
PBS::Client 0.09
PBS::Logs 0.05
PDF::Burst 1.19
PDF::Create 1.06
PDF::GetImages 1.17
PDL::IO::Export2D 0.01
PDL::Slatec
PDL::Stats 0.5.5
PHP::Include 0.32
PHP::Session 0.27
PHP::Session::DBI 0.24
PITA::Image 0.43
PLUTO 0.29
PNI 0.11
PNI::Node::Tk 0.04
POCSAG::Encode 1.00
POD2::Base 0.043
POD2::ES 5.12.3.07
POD2::PT_BR 0.06
POE::Component::AI::MegaHAL 1.18
Params::Validate::Checks 0.01
POE::Component::CPANIDX 0.10
POE::Component::Client::BigBrother 1.00
POE::Component::Client::Ident 1.16
POE::Component::Client::NRPE 0.18
POE::Component::Client::Pastebot 1.14
POE::Component::Client::Stomp::Utils 0.02
POE::Component::Client::Stomp 0.11
POE::Component::Daemon 0.1203
POE::Component::WWW::Shorten 1.20
POE::Component::DebugShell 1.412
POE::Component::DirWatch 0.300000
POE::Component::IRC::Plugin::BaseWrap 0.009
POE::Component::FastCGI 0.16
App::ColorNamer
POE::Component::JavaScript::Minifier
POE::Component::Syntax::Highlight::CSS
POE::Component::Syntax::Highlight::HTML
POE::Component::WWW::Alexa::TrafficRank
POE::Component::WWW::Cache::Google
POE::Component::WWW::DoctypeGrabber
POE::Component::WWW::GetPageTitle
POE::Component::WWW::Google::PageRank
POE::Component::WWW::HTMLTagAttributeCounter
POE::Component::WWW::Lipsum
POE::Component::WWW::WebDevout::BrowserSupportInfo
POE::Component::WebService::Validator::CSS::W3C
POE::Component::WebService::Validator::HTML::W3C
POE::Component::IRC::Plugin::ColorNamer 0.0101
POE::Component::IRC::Plugin::Eval 0.07
POE::Component::IRC::Plugin::URI::Find 1.08
Math::Random::OO
POE::Component::IRC::Plugin::Hailo 0.14
POE::Quickie 0.14
Image::ImageShack
POE::Component::IRC::Plugin::ImageMirror 0.14
POE::Component::IRC::Plugin::Karma 0.003
POE::Component::IRC::Plugin::MegaHAL 0.43
POE::Component::IRC::Plugin::MultiProxy 0.01
POE::Component::IRC::Plugin::QueryDNS 1.02
POE::Component::IRC::Plugin::QueryDNSBL 1.02
POE::Component::IRC::Plugin::RTorrentStatus 0.15
POE::Component::Lingua::Translate 0.06
POE::Component::OpenSSH 0.08
POE::Component::Pluggable 1.26
POE::Component::RSSAggregator 1.11
POE::Component::SASLAuthd 0.02
POE::Component::SSLify::NonBlock 0.41
POE::Component::Schedule 0.95
POE::Component::Server::MySQL 0.02
POE::Component::Server::RADIUS 1.04
POE::Component::Server::REST v33.46.48.48
POE::Component::Server::SimpleHTTP 2.08
POE::Component::Server::SimpleSMTP 1.50
POE::Component::Server::Syslog 1.20
POE::Component::Server::Twirc 0.12
POE::Component::SimpleDBI 1.30
DateTime::Format::Duration::ISO8601
Parse::Number::ID
DateTime::Format::Alami::EN
App::rsync::new2old
POE::Component::SmokeBox::Uploads::Rsync 1.000
POE::Component::SpreadClient 1.002
POE::Component::Supervisor 0.07
POE::Component::XUL 0.0600
POE::Component::YubiAuth 0.06
Test::Expr
Data::Dx
experimentals
Perl6::Controls
Test::Subunits
Perl6::Export
Perl6::Gather
Perl6::Variables
Perl6::Currying
POE::Declarative 0.09
POE::Declare::HTTP::Client 0.04
POE::Declare::HTTP::Server 0.05
POE::Filter::DHCPd::Lease 0.0502
POE::Filter::Postfix 0.003
POE::Filter::Redis 0.02
POE::Filter::SSL 0.22
POE::Loop::Event 1.304
POE::Loop::Tk 1.304
POE::SAPI 0.01
POE::SAPI::ConfigLoader 0.03
POE::SAPI::DBIO 0.03
POE::SAPI::HTTP 0.03
POE::SAPI::HandySubs 0.02
POE::SAPI::NetTools 0.03
POE::SAPI::Scheduler 0.02
POE::Test::Helpers 1.11
POE::Wheel::Sendfile 0.0100
POEx::HTTP::Server 0.0600
POEx::ProxySession 1.102760
POEx::PubSub 1.102740
POEx::Role::PSGIServer 1.103341
POEx::Role::SessionInstantiation 1.102610
POEx::Role::TCPClient 1.102740
Package::Alias 0.12
POEx::Trait::DeferredRegistration 1.102770
PPIx::EditorTools 0.11
POEx::Trait::ExtraInitialization 1.102770
POEx::WorkerPool 1.102740
POSIX::AtFork 0.02
POSIX::Open3 0.01
POSIX::RT::MQ
Term::Clear
Neo4j::Driver
MooseX::amine
Math::FractionManip
Log::Any::Adapter::Sentry::Raven
POSIX::RT::SharedMem 0.05
POSIX::RT::Timer 0.009
POSIX::bsearch 0.02
PPI::XS::Tokenizer 0.03
PPIx::IndexLines 0.05
PPIx::Utilities 1.001000
PPM::Make 0.98
Package::Generator 0.103
PPM::Repositories 0.18
Package::Aspect 0.01
Package::Butcher 0.02
Package::FromData 0.01
Package::Watchdog 0.09
Parse::ExuberantCTags
ORLite::Migrate
Text::FindIndent
Wx::Scintilla
Padre
Padre::Plugin::RunPerlExternal 0.51
PagSeguro 0.001
Palm::Doc 1.19
Palm::ProjectGutenberg 1.02
Paludis::ResumeState::Serialization 0.01000409
Parallel::Fork::BossWorker 0.04_1
Parallel::Fork::BossWorkerAsync 0.06
Parallel::Forker 1.232
Report::Generator 0.002
Parallel::Loops 0.07
Parallel::Queue 0.99
Parallel::Scoreboard 0.03
Parallel::Simple 0.01
Parallel::Simple::Dynamic 0.0.4
URI::Nested
URI::db
Parallel::SubFork 0.10
Parallel::Supervisor 0.03
UNIVERSAL::filename
Namespace::Dispatch
List::UtilsBy::XS
List::Objects::WithUtils
Params::Attr 1.00
Time::Duration::Concise::Localize
Date::Utility
Params::Util 1.04
Paranoid 0.27
Params::Validate::Checks::Integer 0.01
Parse::ACNS 0.03
Parse::Binary 0.11
Convert::Number::Armenian
Text::TEI::Markup
Text::Tradition
Parse::AFP 0.25
Parse::Apache::ServerStatus 0.10
Parse::BooleanLogic 0.09
Text::Xslate::Compiler
Text::Xslate::Parser
Text::Xslate::Symbol
Text::Xslate::Util
Text::Handlebars
HiD
Parse::CPAN::Meta 1.4401
Test::MockDateTime
Dancer2::FileUtils
Dancer2::Template::Tiny
Dancer2::Plugin::Auth::Extensible
Parse::CSV 1.00
Parse::CSV::Colnames 0.03
Parse::DNS::Zone 0.41
Parse::DebControl 2.005
System::Sub
Git::Sub
Crypt::DES_PP 1.00
Crypt::DH 0.06
Crypt::DH::GMP 0.00009
Crypt::Eksblowfish 0.009
Crypt::FNA 0.24
Crypt::GPG 1.63
Crypt::Keyczar 0.06
Perlbal 1.77
Crypt::OpenSSL::Cloner 0.01
Crypt::OpenSSL::Common 0.1
Crypt::OpenSSL::PBKDF2 0.02
Crypt::OpenToken 0.06
Crypt::Passwd::XS 0.507
Regexp::Ethiopic 0.15
Crypt::RSA 1.99
Crypt::RSA::Yandex 0.06
Crypt::RandPasswd 0.02
Crypt::Rijndael::MySQL 0.02
Crypt::Rijndael_PP 0.05
Crypt::Salt 0.01
Crypt::Scrypt 0.04
Crypt::SimpleGPG 0.3
Curses::Toolkit 0.202
Text::Convert::PETSCII
D64::Disk::BAM 0.01
D64::Disk::Dir 0.02
D64::Disk::Layout::Base 0.01
D64::File::PRG 0.02
DB::Pluggable 1.101051
DB::Pluggable::StackTraceAsHTML 1.100850
DBD::AnyData 0.110
DBD::Multi 0.16
DBD::SQLAnywhere 2.08
DBD::SQLite::Amalgamation 3.6.1.2
DBI::Easy 0.22
Rose::ObjectX::CAF 0.03
DBD::SQLite::FTS3Transitional undef
DBD::XBase 1.02
DBI::Wikileaks::AfWD 0.0.0
DBICx::Hooks 0.003
DBICx::MapMaker 0.03
DBICx::MaterializedPath 0.03
Perl6::Doc 0.47
DBIx::Admin::BackupRestore 1.17
DBIx::Array 0.21
Perl6::Perldoc::To::Ansi 0.11
DBIx::BulkLoader::Mysql 1.004
DBIx::Class::Cursor::Cached 1.001001
DBIx::Class::DateTime::Epoch 0.07
DBIx::Class::FrozenColumns 1
Parse::ErrorString::Perl 0.15
DBIx::Class::Graph 1.03
Parse::Dia::SQL 0.17
Parse::FSM 1.02
Parse::Flash::Cookie 0.09
Parse::Flexget 0.012
Parse::HTTP::UserAgent 0.20
Parse::Log::Smbd 0.02_01
Parse::MediaWikiDump 1.0.6
Parse::Pegex 0.02
SWISH::Prog::KSx 0.18
Parse::Perl 0.005
Parse::PhoneNumber::ID 0.05
Parse::QTEDI 0.18
Perl::Critic::Policy::logicLAB::ProhibitShellDispatch 0.01
Perl::Critic::Policy::logicLAB::ProhibitUseLib 0.01
Perl::Critic::Policy::logicLAB::RequireVersionFormat 0.03
Parse::RPM::Spec 0.08
Parse::RPN 2.63
Parse::RecDescent::FAQ 7.5
Parse::Snort 0.5
Parse::Stallion 2.00
Parse::Win32Registry 0.60
Parse::nm 0.07
ParseUtil::Domain 2.01
Parser::IPTables::Save 0.04
Passwd::Unix::Alt 0.52
Path::Class::Each 0.03
Path::Class::Versioned 0.04
Path::Extended 0.17
Path::Resolver 3.100451
PayflowPro 3101
Peco::Container 1.2
Perl6::Pod::Lib 0.03
Perl6::Say 0.16
Perl::APIReference 0.04
Perl::Critic::Policy::Dynamic::NoIndirect 0.05
Perl::Critic::Policy::logicLAB::ModuleBlacklist
Perl::Critic::Policy::logicLAB::RequirePackageNamePattern
Perl::Critic::Policy::logicLAB::RequireParamsValidate
Perl::Critic::Policy::logicLAB::RequireSheBang
Perl::Critic::logicLAB 0.02
Perl::ImportReport 0.1
Perl::Maker 0.01
Perl::Metrics::Simple 0.15
Perl::OSType 1.002
ShipIt 0.55
Perl::Squish 1.06
Perl::Strip 1.0
PerlIO::bitswap 0.000
PerlIO::via::EscStatus 10
PerlIO::fgets 0.01
PerlIO::if 0.002
PerlIO::rewindable 0.000
PerlIO::via::QuotedPrint 0.06
PerlIO::via::trap 0.09
Perlanet 0.54
Perlbal::Plugin::Syslogger 1.00
Persevere::Client 0.01
Pg::BulkCopy 0.14
Pg::CLI 0.03
Pg::DatabaseManager 0.05
Pg::Explain 0.52
Phone::Info 0.0.1
Plack::App::Apache::ActionWrapper 0.30.0
Plack::App::DAV 0.01
Plack::App::Proxy::Selective 0.10
Plack::App::TemplateToolkit 0.03
Plack::Builder::Conditionals 0.03
Plack::Client 0.06
Plack::Handler::AnyEvent::ReverseHTTP 0.04
Plack::Middleware::AddDefaultCharset 0.02
Plack::Middleware::AllowCrossSiteAJAX 0.02
Plack::Middleware::Auth::Htpasswd 0.02
Plack::Middleware::CSRFBlock 0.05
Plack::Middleware::ChromeFrame 0.02
Plack::Middleware::Compile 0.01
Plack::Middleware::Debug::CatalystPluginCache 0.101
Plack::Middleware::Debug::Profiler::NYTProf 0.06
Plack::Middleware::Debug::W3CValidate 0.04
Plack::Middleware::ESI 0.1
Plack::Middleware::File::Sass 0.02
Plack::Middleware::Firebug::Lite 0.2.3
Plack::Middleware::ForceEnv 0.02
Plack::Middleware::HTMLMinify 0.6.0
Plack::Middleware::HTMLify 0.1.1
Plack::Middleware::IEnosniff 0.01
Plack::Middleware::IPAddressFilter 0.02
Plack::Middleware::Image::Scale 0.007
Plack::Middleware::Log::Minimal 0.02
Plack::Middleware::NeverExpire 1.003
Plack::Middleware::NoDeflate 0.01
Plack::Middleware::NoMultipleSlashes 0.001
Plack::Middleware::OptionsOK 0.01
Plack::Middleware::Precompressed 1.005
Plack::Middleware::Reproxy 0.00001
Plack::Middleware::Rewrite 1.002
Scope::Container 0.04
Plack::Middleware::Scope::Container 0.01
Plack::Middleware::ServerStatus::Lite 0.01
Plack::Middleware::SocketIO 0.00903
Plack::Middleware::Static::Minifier 0.05
Plack::Middleware::StaticShared 0.01
Plack::Middleware::Test::StashWarnings 0.04
Plack::Middleware::UseChromeFrame 1.003
Plack::Middleware::Watermark 0.01
Plack::Middleware::XForwardedFor 0.103060
Plack::Middleware::XFrameOptions::All 0.2
Plack::Middleware::XSLT 0.10001
Plack::Server::AnyEvent 0.04
Plack::Session::Store::MongoDB 0.3
Plack::Session::Store::Redis 0.03
Pod::Elemental::Transformer::SynHi 0.100890
PlackX::RouteBuilder 0.03
Pod::Advent 0.24
Pod::Autopod 1.10
Pod::CYOA 0.001
Pod::Constants undef
Pod::Coverage::MethodSignatures 0.02
Pod::Elemental::Transformer::ExampleRunner 0.002
Pod::Elemental::Transformer::WikiDoc 0.093001
Pod::Escapes 1.04
Pod::Eventual 0.093330
Pod::Generated 0.05
Pod::Manual 0.08
Ref::Explicit 0.001
Pod::POM::View::TOC 0.02
R::YapRI 0.04
RDF::ACL 0.101
RDF::AllegroGraph::Easy 0.06
RDF::Crypt 0.001
RDF::Everywhere 0.001
RDF::Helper 1.99_01
RDF::Query::Functions::Buzzword::Util
RDF::Query::Functions::Buzzword::DateTime 0.001
RDF::RDFa::Linter 0.051
RDF::Trine::Node::Literal::XML 0.16
Regexp::DefaultFlags 0.01
RDF::TrineShortcuts 0.104
RDF::iCalendar 0.002
RDF::vCard::Babelfish 0.007
REST::Google 1.0.8
REST::Google::Apps::EmailSettings 1.1.6
REST::Google::Apps::Provisioning 1.1.9
RMI v0.08
RPC::Serialized 1.0801
RPC::XML::Parser::LibXML 0.05
RSA::Toolkit 0.01
RSS::NewsFeed::BBC 0.04
RSSycklr 0.12
RTF::Tokenizer 1.13
RTPG 0.92
RTSP::Server 0.01
RadioMobile 0.06
Random::Quantum 0.04
Rapid7::NeXpose::API 0.03
Razor2::Client::Agent undef
Rcs 1.05
TM 1.56
Redis::Queue 0.01
Redis::hiredis 0.9.2.6
Reflexive::Role::Collective 1.110100
Reflex 0.088
Reflexive::Role::DataMover 1.110030
Reflexive::Stream::Filtering 1.103450
Reflexive::Role::TCPServer 1.110100
Regex::Iterator 0.4
Regexp::Assemble::Compressed 0.02
Regexp::English 1.01
Regexp::Log 0.05
Regexp::Log::Common 0.05
ResourcePool 1.0106
Scope::Cleanup 0.001
ResourcePool::Resource::SOAP::Lite 1.0103
Rhetoric 0.05
Scope::Escape 0.004
Rinchi::XMLSchema 0.02
Robotics::IRobot 0.14
Role::HasPayload 0.005
Role::Log::Syslog::Fast 0.12
Rose::DBx::Bouquet 1.04
Rose::DBx::Garden::Catalyst 0.15
Rose::DBx::Object::Builder 0.09
Rose::DBx::Object::Indexed 0.008
Rose::DBx::Object::Renderer 0.76
Router::Simple::Sinatraish 0.02
Statistics::DependantTTest 0.03
Routes::Tiny 0.009011
Rubyish::Attribute 1.2
Rule::Engine 0.06
Run::Env 0.08
SAFT 0.1.0
SCGI 0.6
SDLx::Controller::Coro 0.03
SDLx::Widget 0.071
SIAM 0.05
SMS::AQL 0.08
SMS::CPAGateway 0.01
Sort::Key::OID
SNMP::Extension::PassPersist 0.05
SNMP::Trapinfo 1.02
Slay::Makefile 0.12
SOAP::WSDL 2.00.10
SOAP::ISIWoK 1.04
Text::ASCIIMathML
Text::Markmoredown
Git::Version
URI::git
Git::Helpers
SQL::Bibliosoph 2.22
SQL::DB 0.97_1
SQL::Easy 0.02
SQL::Interp 1.10
SQL::Tree 0.03
SQLite::More 0.10
SSH::Batch 0.024
STD 20101111
STUN::RFC_5389 0.1
STUN::Client 0.04
SVG::Rasterize 0.003006
SVG::Sparkline 0.35
SVN::Access 0.06
Safe 2.29
SVN::Class 0.16
SVN::Hooks 0.33
SVN::Notify::Filter::Markdown 0.04
SVN::Utils::ClientIP 0.02
Safe::Hole 0.13
Sakai::Nakamura 0.03
Scaffold 0.05
Scalar::Boolean undef
Scalar::Listify 0.02
Scalar::Number 0.006
Scalar::Random::PP 0.11
Scalar::Vec::Util 0.06
Schedule::AdaptiveThrottler 0.03
Schedule::At 1.11
Schedule::Pluggable 0.0.4
Scope::Container::DBI 0.03
Scope::Escape::Sugar 0.000
Scope::Session 0.02
Script::State 0.02
Script::Toolbox 0.31
Search::GIN 0.08
Search::GIN::Extract::AttributeIndexes 1.0.0
Search::OpenSearch::Engine::KSx 0.08
Search::OpenSearch::Server 0.05
Search::Sitemap 2.10
Search::WuManber 0.25
Sort::Key::Top 0.04
SelfLoader 1.18
SemanticWeb::OAI::ORE 0.95
Sendmail::Milter 0.18
Sendmail::PMilter 1.00
Sepia 0.991_05
Set::ConsistentHash 0.92
Spreadsheet::DataToExcel 0.0103
Set::Partition 0.03
Setup::File::Symlink 0.09
Setup::Text::Snippet 0.01
Setup::Unix::User 0.01
ShiftJIS::Regexp 1.01
ShipIt::Step::Facebook 0.01
Shipwright 2.4.24
SigAction::SetCallBack 0.01
Signal::StackTrace::CarpLike 0.01
Signals::XSIG 0.09
SimpleDB::Class 1.0502
Slay::Makefile::Gress 0.08
Slay::Maker 0.08
SmartMatch::Sugar 0.04
Sniffer::HTTP 0.22
Snort::Rule 1.07
Socialtext::WikiTest 0.07
Socket::Linux 0.01
Socket::Netlink 0.03
Socket::Netlink::Taskstats 0.02
Software::License::Beerware 0.1
Software::License::DWTFYWWI 0.01
Software::License::PD 1.001
Solution 0.0004
Sorauta::Util::Iterator 0.01
Sort::ArbBiLex 4.01
UNIVERSAL::Object
Spawn::Safe undef
Sphinx::Config 0.09
Sphinx::Log::Parser 0.03
Sphinx::Search 0.25_03
Sport::Analytics::SimpleRanking 0.20
Spreadsheet::Perl 0.10
String::Nysiis 1.00
Spreadsheet::ReadSXC 0.20
Spreadsheet::Wright 0.102
Spreadsheet::WriteExcel::Simple::Tabs 0.09
Spreadsheet::WriteExcelXML 0.13
Spreadsheet::XlateExcel 0.02
Squid::Guard 0.15
Statistics::Data::Rank
Statistics::ANOVA::Friedman
Statistics::ANOVA::JT
Statistics::ANOVA::KW
Statistics::ANOVA::Page
Statistics::ANOVA 0.07
Statistics::Autocorrelation 0.02
Statistics::Basic 1.6602
Statistics::Burst 0.2
Statistics::ChisqIndep 0.1
Statistics::EfficiencyCI 0.03
Statistics::FisherPitman 0.034
Statistics::Histogram 0.1
Statistics::R 0.07
Statistics::RVector 0.1
Statistics::TTest 1.1
Statistics::RankCorrelation 0.1203
Module::Build::FFI
FFI::Util
Alien::LZO
Alien::m4
Alien::Nettle
Alien::Libbz2
Alien::xz
Alien::Libarchive3
Storable 2.25
Sub::Exporter::Simple 1.103210
Statistics::SDT 0.04
Statistics::SocialNetworks 0.03
Statistics::Test::Sequence 0.01
Statistics::Test::RandomWalk 0.02
Storable::CouchDB 0.04
Store::CouchDB 1.7
String::BOM 0.3
String::Bash 1.110960
String::CP1251 0.01
String::Clean 0.031
Text::Tradition::Directory
Alien::NSS
String::Comments::Extract 0.023
String::Diff 0.04
String::FilenameStatic 0.01
Alien::ActiveMQ
String::Filter 0.01
String::MatchInterpolate 0.06
String::MkPasswd 0.03
String::ProperCase::Surname 0.02
String::Random::NiceURL 0.02
String::Similarity 1.04
String::Simrank 0.079
String::Substitution 1.000008
String::TagString 0.003
Convert::Color::mIRC
String::Tagged::IRC
Su 0.008
Sub::Args 0.08
Sub::Clean 1
Sub::Exporter 0.983
Sub::Filter 0.003
Sub::Frequency 0.02
Sub::Import 1.000
Sub::Information 0.10
Sub::Install 0.925
Sub::Op 0.02
Sub::Prototype::Util 0.09
Sub::Spec 0.13
Sub::Spec::BashComplete 0.12
Complete::TZ
Perinci::Sub::ArgEntity::timezone
Sub::Spec::CmdLine 0.27
Sub::Spec::Pod 0.13
Sub::Spec::Runner 0.12
Sub::StopCalls 0.01
Sub::Timekeeper 0.01
Supervisor 0.08
SweetPea 2.3664
SweetPea::Application 0.025
IO::FDPass
Proc::FastSpawn
AnyEvent::Fork
AnyEvent::DBI 2.1
SweetPea::Cli 0.08
Symbol::Glob 0.03
Symbol::Global::Name 0.01
Syntax::Feature::Function 0.001
Syntax::Feature::Method 0.001
Syntax::Highlight::Perl6 0.88
Sys::CPU 0.52
Sys::CpuAffinity 1.01
Sys::HostAddr 0.95
Sys::Info::Driver::Unknown 0.78
Sys::Mlockall 0.01
Sys::Mmap 0.15_01
Sys::Statistics::Linux 0.59_02
Sys::Trace 0.03
SysV::SharedMem 0.005
TAIR::Blast 1.01
TAP::Formatter::JUnit 0.08
TAP::Harness::JUnit 0.34
TAP::Parser::SourceHandler::MyTAP 3.23
TAP::Parser::SourceHandler::PHP 0.01
TAP::Parser::SourceHandler::pgTAP 3.26
TAP::Spec::Parser 0.05
TEI::Lite 0.60
TM::View 1.9
TMDB 0.02
TRD::Velocity 0.0.8
TUWF 0.1
AnyEvent::IRC 0.95
TV::ProgrammesSchedules::BBC 0.06
TV::ProgrammesSchedules::STAR 0.03
TV::ProgrammesSchedules::Sony 0.03
Table::Simple 0.02
Tail::Stat 0.17
Tangence 0.05
Tapper::CLI 3.000010
AnyEvent::RabbitMQ 1.03
Tapper::Doc 3.000010
Tapper::Installer 3.000010
Tapper::MCP::MessageReceiver 3.000004
Tapper::PRC 3.000010
AnyEvent::Serialize 0.05
Tapper::Remote 3.000010
Tapper::Reports::API 3.000002
Tapper::Reports::Receiver 3.000010
Tapper::Test 3.000010
Tapper::TestSuite::AutoTest 3.000010
Tapper::TestSuite::HWTrack 3.000010
Tapper::Testplan 3.000010
Task::App::ZofCMS 0.0109
AnyEvent::WebService::Tracks 0.02
AnyMongo 0.03
Apache2::ASP 2.46
Apache2::AuthenNTLM 0.02
Apache::ConfigParser 1.01
Apache::DBI 1.10
Apache::Htgroup 1.23
Apache::Sling 0.13
Apache::LogRegex 1.5
Apache::Session::LDAP 0.1
Apache::Session::NoSQL 0.1
Apache::TS::AdminClient 0.02
Apache::Test 1.36
ApacheLog::Compressor 0.003
Test2::Suite
Mojolicious::Plugin::UnicodeNormalize
HTML::Microdata
Outthentic::DSL
Outthentic
swat
Sparrow
AnyEvent::Beanstalk 1.110490
AnyEvent::Cron 0.021
AnyEvent::Curl::Multi 1.1
AnyEvent::Tools 0.12
AnyEvent::SerialPort
AnyEvent::CurrentCost 1.110792
AnyEvent::DBD::Pg 0.03
AnyEvent::Debounce 0.01
AnyEvent::Digg::Stream 0.03
AnyEvent::FastPing 2.01
AnyEvent::Feed 0.2
AnyEvent::FlashSocketPolicy 0.02
Text::Balanced 2.02
AnyEvent::ForkObject 0.08
AnyEvent::FriendFeed::Realtime 0.05
AnyEvent::Graphite 0.07
AnyEvent::Groonga 0.04
AnyEvent::HTTPBenchmark 0.09
AnyEvent::Handle::Throttle 0.000002005
AnyEvent::I3 0.07
AnyEvent::Inotify::Simple 0.02
AnyEvent::JSONRPC 0.15
AnyEvent::JSONRPC::Lite 0.13
AnyEvent::MP 1.29
AnyEvent::MPRPC 0.09
AnyEvent::MQTT 1.110880
AnyEvent::Memcached 0.05
AnyEvent::Monitor 0.32
AnyEvent::OWNet 1.110430
AnyEvent::POE_Reference 0.11
AnyEvent::Porttracker 0.1
AnyEvent::RPC 0.05
Crypt::HSXKPasswd
AnyEvent::Retry 0.03
AnyEvent::RetryTimer 0.1
AnyEvent::STOMP 0.6
AnyEvent::Subprocess 1.102912
AnyEvent::TFTPd 0.1303
Imager::Heatmap
Constant::Export::Lazy
Gnus::Newsrc
MooseX::ShortCut::BuildInstance
Data::Walk::Extracted
AnyEvent::WebArchive 0.02
AnyEvent::WebService::Notifo 0.001
Phash::FFI
Smart::Options
App::bovespa
App::Packager
App::PDF::Link
Net::Xero
WWW::Lengthen
WWW::Eksi
JSON::MaybeUTF8
Data::Chronicle
Sub::Versions
Module::Spy
Devel::CodeObserver
Test::Kantan
MooseX::Role::JSONObject
MooseX::AttributeDocumented
Syntax::Feature::Qs
Stenciller
String::Cushion
String::Stomp
Syntax::Feature::Qi
Dist::Zilla::Plugin::Stenciller::HtmlExamples
Mojolicious::Plugin::InstallablePaths
BenchmarkAnything::Storage::Frontend::HTTP
Test::Mojo::Trim
Stenciller::Plugin::ToMojoliciousTest
Dist::Zilla::Plugin::Stenciller::MojoliciousTests
Bytes::Random::Secure::Tiny
Call::Context
Crypt::Format
Math::ProvablePrime
Symbol::Get
Crypt::Perl
Net::ACME
Math::Random::Normal::Leva
App::Stacktrace
Net::Stomp::MooseHelpers::CanConnect
Net::Stomp::MooseHelpers::Exceptions
Net::Stomp::MooseHelpers::ReconnectOnFailure
Net::Stomp::Producer
DDP
Crypt::Argon2
List::Flatten
Regexp::Lexer
Perl::Lint
Tie::Handle::Scalar
Getopt::Alt
Set::Tiny
Imager::Trim
CPAN::Testers::TailLog
AI::ConfusionMatrix
Devel::INC::Sorted
Data::Mining::Apriori
Role::Tiny::With
Alien::Librdkafka
Kafka::Librd
Mojolicious::Plugin::Log::Elasticsearch
Readonly::Tiny
DBIx::TempDB
Linux::Clone
Test::Dist::Zilla::Build
Devel::DebugHooks
Devel::Timer
Date::Reformat
MaxMind::DB::Reader::XS
App::a2p
App::find2perl
PerlPowerTools
Selenium::Remote::Driver
Weasel
Weasel::Driver::Selenium2
Acme::Alien::DontPanic
Try::Tiny::Except
Devel::EnforceEncapsulation
Gtk2::Ex::Utils
Gtk2::Ex::Dialogs
Gtk2::Ex::Datasheet::DBI
Gtk2::Notify
App::MetaCPAN::Gtk2::Notify
DBIx::Diff::Schema
Module::Build::Pluggable::ReadmeMarkdownFromPod
Test::WithDB
Test::SQL::Schema::Versioned
Mojolicious::Quick
Mojolicious::Plugin::BasicAuthPlus
Data::Graph::Util
Test::WithDB::SQLite
Alien::Libarchive::Installer
Test2::Tools::Spec
Cache::Memory::Simple
Data::Validate::Type
Test::Type
Test::Routini
Acme 1.111111
Acme::24 0.03
Text::UpsideDown
Acme::3mxA undef
Acme::6502 0.76
Acme::6502::Tube 0.76
Acme::Akashic::Records 0.01
Acme::Anything 0.04
Acme::AwesomeQuotes 0.02
Acme::Base64 undef
Acme::BlahBlahBlah 0.01
Acme::CPANAuthors::Register undef
Acme::CPANAuthors::Austrian 1.100770
Acme::CPANAuthors 0.16
Acme::Buffy 1.5
Acme::CPANAuthors::AnyEvent 0.07
Acme::CPANAuthors::Booking 2010112701
Acme::CPANAuthors::British 0.04
Acme::CPANAuthors::Brazilian 0.14
Acme::CPANAuthors::British::Companies 0.04
Acme::CPANAuthors::CPANTS::FiveOrMore 2010072801
Acme::CPANAuthors::Canadian 0.0102
Acme::CPANAuthors::German 0.04
Acme::CPANAuthors::French 0.09
Acme::CPANAuthors::Chinese 0.19
Acme::CPANAuthors::Portuguese 0.04
Acme::CPANAuthors::DualLife 0.02
Acme::CPANAuthors::Icelandic 0.04
Acme::CPANAuthors::Turkish 0.20
Acme::CPANAuthors::Norwegian 0.2
Acme::CPANAuthors::Russian 1.06
Acme::CPANAuthors::Ukrainian 0.15
Acme::CPANAuthors::EU 2010081801
Acme::CPANAuthors::European 2010081801
Acme::CPANAuthors::Female 0.01
Acme::CPANAuthors::GitHub 0.02
Acme::CPANAuthors::Italian 0.01
Acme::CPANAuthors::Japanese 0.101014
Acme::CPANAuthors::Korean 0.061
Acme::CPANAuthors::Misanthrope 1.03
Acme::CPANAuthors::POE 0.34
Acme::CPANAuthors::Pumpkings 2010082001
Acme::CPANAuthors::Search 0.05
Acme::CPANAuthors::ToBeLike 0.10
Acme::CPANAuthors::Utils 0.14
Acme::CPANAuthors::Utils::Authors undef
Acme::CPANAuthors::Utils::CPANIndex undef
Acme::CPANAuthors::Utils::Kwalitee undef
Acme::CPANAuthors::Utils::Packages undef
Acme::CPANAuthors::You::re_using 0.03
Acme::EyeDrops 1.55
Acme::ChuckNorris 0.1
Acme::Crap 0.001002
Acme::Currency 3.01
Acme::Daily::Fail 1.06
Acme::EvilLicense 0.01
Acme::FizzBuzz 0.03
Acme::Ford::Prefect
Acme::Geo::Whitwell::Name 0.04
Acme::Gtk2::Ex::Builder 0.008
Acme::Hello 0.05
Acme::Hello::I18N 0.05
Acme::Hidek 40.0
Acme::Hidek::Congrats undef
Acme::Hyperindex 0.12
Acme::JavaTrace 0.08
Acme::Letter 0.01
Acme::MathProfessor::RandomPrime 2009121001
Acme::Minify 0.07
Acme::Module::Build::Tiny 0.05
Acme::Nest 0.02
Acme::Nothing 0.03
Acme::PM::Berlin::Meetings 201008.26
Acme::PM::Frankfurt::Meetings 0.15
Acme::ProgressBar 1.126
Acme::Pythonic 0.46
Acme::Study::Perl 0.0.2
Acme::Tanasinn 0.02
Acme::Tango 0.07
Acme::Time::Asparagus 1.12
Acme::Time::Aubergine 1.12
Acme::Time::Baby 2010090301
Acme::Time::DimSum 1.12
Acme::Time::Donut 1
Acme::Time::FooClock 1.10
Acme::Tools 0.13
Acme::Vuvuzela 0.02
Acme::W 0.03
Acme::eng2kor undef
Perl6::Form
Algorithm::Accounting
Algorithm::AhoCorasick 0.03
Algorithm::AhoCorasick::SearchMachine undef
Algorithm::BMI 0.04
Algorithm::Bayesian 0.5
Algorithm::BinPack 0.5
Algorithm::BloomFilter
Perl::Critic::Policy::Variables::ProhibitLoopOnHash
Perl::Critic::Freenode
Algorithm::CheckDigits 1.1.1
Algorithm::CheckDigits::M07_001 0.53
Algorithm::CheckDigits::M09_001 0.53
Algorithm::CheckDigits::M10_001 1.1.0
Algorithm::CheckDigits::M10_002 0.53
Algorithm::CheckDigits::M10_003 0.53
Algorithm::CheckDigits::M10_004 0.53
Algorithm::CheckDigits::M10_005 0.53
Algorithm::CheckDigits::M10_006 0.53
Algorithm::CheckDigits::M10_008 0.54
Algorithm::CheckDigits::M10_009 0.53
Algorithm::CheckDigits::M10_010 0.53
Algorithm::CheckDigits::M10_011 0.53
Algorithm::CheckDigits::M11_001 0.53
Algorithm::CheckDigits::M11_002 0.53
Algorithm::CheckDigits::M11_003 0.53
Algorithm::CheckDigits::M11_004 0.53
Algorithm::CheckDigits::M11_006 0.53
Algorithm::CheckDigits::M11_007 0.53
Algorithm::CheckDigits::M11_008 0.53
Algorithm::CheckDigits::M11_009 0.53
Algorithm::CheckDigits::M11_010 0.53
Algorithm::CheckDigits::M11_011 0.53
Algorithm::CheckDigits::M11_012 0.53
Algorithm::CheckDigits::M11_013 0.53
Algorithm::CheckDigits::M11_015 0.53
Algorithm::CheckDigits::M11_016 0.53
Algorithm::CheckDigits::M11_017 0.53
Algorithm::CheckDigits::M16_001 0.53
Algorithm::CheckDigits::M23_001 0.53
Algorithm::CheckDigits::M23_002 0.53
Algorithm::CheckDigits::M43_001 0.53
Algorithm::CheckDigits::M89_001 0.53
Algorithm::CheckDigits::M97_001 0.53
Algorithm::CheckDigits::M97_002 0.53
Algorithm::CheckDigits::MBase_001 0.53
Algorithm::CheckDigits::MBase_002 0.53
Algorithm::CheckDigits::MBase_003 0.53
Algorithm::CheckDigits::MXX_001 0.53
Algorithm::CheckDigits::MXX_002 0.53
Algorithm::CheckDigits::MXX_003 0.53
Algorithm::CheckDigits::MXX_004 0.53
Algorithm::CheckDigits::MXX_005 0.53
Algorithm::CheckDigits::MXX_006 0.53
Algorithm::Cluster 1.50
Algorithm::Cluster::Record undef
Algorithm::ConsistentHash::Ketama 0.00007
Algorithm::ConsistentHash::Ketama::Bucket undef
Algorithm::CouponCode 1.000
Algorithm::DecisionTree 1.41
Algorithm::Dependency::Ordered 1.110
Algorithm::Dependency::Item 1.110
Algorithm::Dependency::Source 1.110
Algorithm::Dependency::Source::File 1.110
Algorithm::Dependency::Source::HoA 1.110
Algorithm::Dependency::Source::Invert 1.110
Algorithm::Dependency::Weight 1.110
Algorithm::Diff::Callback 0.03
Algorithm::DiffOld 1.1
Algorithm::Easing
Algorithm::EquivalenceSets 1.101420
Algorithm::GooglePolylineEncoding
Algorithm::Hamming::Perl
Algorithm::Health::BFI 0.02
Algorithm::KMeans 1.21
Algorithm::KernelKMeans 0.03
Algorithm::KernelKMeans::PP 0.02
Algorithm::KernelKMeans::Util undef
Algorithm::KernelKMeans::XS 0.02_01
Algorithm::Kuhn::Munkres 1.0.7
Algorithm::Merge 0.08
Algorithm::NIN 1.02
Algorithm::Networksort 1.09
Algorithm::NGram
Algorithm::Numerical::Shuffle 2009110301
Algorithm::Permute 0.12
Logic::Minimizer
Algorithm::QuineMcCluskey
Algorithm::RandomizedTreesForBigData
Algorithm::SIN 0.06
Algorithm::SVM
Algorithm::Shape::RandomTree 0.01
Algorithm::Shape::RandomTree::Branch undef
Algorithm::Shape::RandomTree::Branch::Point undef
Algorithm::SpatialIndex 0.05
Algorithm::SpatialIndex::Bucket undef
Algorithm::SpatialIndex::Node undef
Algorithm::SpatialIndex::Storage undef
Algorithm::SpatialIndex::Storage::DBI 0.02
Algorithm::SpatialIndex::Storage::Memory undef
Algorithm::SpatialIndex::Strategy undef
Algorithm::SpatialIndex::Strategy::2D undef
Algorithm::SpatialIndex::Strategy::3D undef
Algorithm::SpatialIndex::Strategy::MedianQuadTree 0.02
Algorithm::SpatialIndex::Strategy::OctTree undef
Algorithm::SpatialIndex::Strategy::QuadTree undef
Algorithm::TSort 0.02
Algorithm::URL::Shorten 0.04
Algorithm::Voting
App::AYCABTU 0.03
App::AYCABTU::OO 0.15
App::Addex 0.022
App::Addex::AddressBook 0.022
App::Addex::AddressBook::AppleScript 0.002
App::Addex::AddressBook::LDAP 0.001
App::Addex::Entry 0.022
App::Addex::Entry::EmailAddress 0.022
App::Addex::Output 0.022
App::Addex::Output::Mutt 0.022
App::Addex::Output::Procmail 0.022
App::Addex::Output::SpamAssassin 0.022
App::Addex::Output::ToFile 0.022
App::Alice 0.19
App::Alice::Commands undef
App::Alice::Config undef
App::Alice::HTTPD undef
App::Alice::History undef
App::Alice::IRC undef
App::Alice::InfoWindow undef
App::Alice::Logger undef
App::Alice::MessageBuffer undef
App::Alice::MessageStore::Memory undef
App::Alice::MessageStore::Redis undef
App::Alice::Notifier::Growl undef
App::Alice::Notifier::LibNotify undef
App::Alice::Signal undef
App::Alice::Stream undef
App::Alice::Test::MockIRC undef
App::Alice::Test::NullHistory undef
App::Alice::Window undef
App::AutoBuild 0.03
App::BCVI::AutoInstall 0.1
App::BCVI::InstallManager 1.02
App::BackupTumblr 0.03
App::Benchmark 1.102310
App::Bondage 0.4.11
App::Bondage::Away 1.1
App::Bondage::Client 1.3
App::Bondage::Recall 1.5
App::Bondage::State 1.0
App::Bot::BasicBot::Pluggable 0.91
App::Bot::BasicBot::Pluggable::Terminal 0.91
App::Build 0.74
App::CLI::Command undef
App::CLI::Extension 1.3
App::CLI::Command::Help undef
App::CLI::Extension::Component::Config 1.3
App::CLI::Extension::Component::ErrorHandler 1.3
App::CLI::Extension::Component::InstallCallback 1.3
App::CLI::Extension::Component::OriginalArgv 1.3
App::CLI::Extension::Component::RunCommand 1.3
App::CLI::Extension::Component::Stash 1.3
App::CLI::Extension::Exception 1.3
App::CLI::Helper undef
App::CLI::Plugin::StackTrace 1.1
App::CPANIDX 0.30
App::CPAN::Fresh
App::CPANIDX::HTTP::Server 0.06
App::CPANIDX::Queries 0.30
App::CPANIDX::Renderer 0.30
App::Cmd 0.311
Class::WeakSingleton
Proc::SyncExec
Regexp::Tr
Set::IntSpan::Fast
Gtk2::Ex::DateSpinner::CellRenderer
Gtk2::Ex::ErrorTextDialog::Handler
Gtk2::Ex::History::Action
Gtk2::Ex::TextView::FollowAppend
Set::IntSpan::Fast::XS
App::Chart
App::Cmd::Command 0.311
App::Cleo
App::Cmd::Command::help 0.311
App::Cmd::ArgProcessor 0.311
App::Cmd::Command::commands 0.311
App::Cmd::Plugin 0.311
App::Cmd::Simple 0.311
App::Cmd::Subdispatch 0.311
App::Cmd::Subdispatch::DashedStyle 0.311
Math::Decimal128
App::Cpan 1.5701
Process::Status
App::Cronjob 1.102311
App::Daemon 0.11
App::Dispatcher 0.08
App::Distlinks 4
App::Distlinks::DBI 4
App::Distlinks::FileFind 4
App::Distlinks::URInFiles 4
App::Distlinks::URIterator 4
App::DoubleUp 0.2.1
App::Dthumb 0.2
App::Dthumb::Data 0.2
App::DuckDuckGo 0.006
IO::Prompt::Simple
IO::Page
App::GitGot 0.9.2
App::EditorTools 0.14
App::EditorTools::Command::InstallEmacs 0.03
App::EditorTools::Command::InstallVim 0.08
App::EditorTools::Command::IntroduceTemporaryVariable undef
App::EditorTools::Command::RenamePackage undef
App::EditorTools::Command::RenamePackageFromPath undef
App::EditorTools::Command::RenameVariable undef
App::EditorTools::CommandBase::Install undef
Inline::Lua
Jplugin
PHP::Interpreter
Ruby
App::EvalServer 0.07
App::EvalServer::Child 0.07
App::EvalServer::Language::Deparse 0.07
App::EvalServer::Language::J 0.07
App::EvalServer::Language::Lua 0.07
App::EvalServer::Language::PHP 0.07
App::EvalServer::Language::Perl 0.07
App::EvalServer::Language::Python 0.07
App::EvalServer::Language::Ruby 0.07
App::FatPacker::Trace undef
App::FileTools::BulkRename undef
App::FileTools::BulkRename::Common 0.06
App::FileTools::BulkRename::Docs 0.06
App::FileTools::BulkRename::Errors 0.06
App::FileTools::BulkRename::UserCommands 0.06
App::FileTools::BulkRename::UserCommands::AutoFormat 0.06
App::Framework::Lite 1.07
App::Framework::Lite::Object 2.002
App::Git::Meta
App::GitGot::Command 0.9.2
App::GitGot::Command::add 0.9.2
App::GitGot::Command::chdir 0.9.2
App::GitGot::Command::fork 0.9.2
App::GitGot::Command::list 0.9.2
App::GitGot::Command::remove 0.9.2
App::GitGot::Command::status 0.9.2
App::GitGot::Command::update 0.9.2
App::GitGot::Command::version 0.9.2
App::GitGot::Outputter 0.9.2
App::GitGot::Outputter::dark 0.9.2
App::GitGot::Outputter::light 0.9.2
App::GitHooks
App::GitHub 0.10
App::GitHubPullRequest
App::Google::Docs 0.05
App::Grok 0.25
App::Grok::Common 0.25
App::Grok::Parser::Pod5 0.25
App::Grok::Parser::Pod6 0.25
App::Grok::Resource::File 0.25
App::Grok::Resource::Functions 0.25
App::Grok::Resource::Spec 0.25
App::Grok::Resource::Tablet 0.25
App::Grok::Resource::u4x 0.25
App::HI 2.7184
App::Hachero
App::Hachero::Plugin::Analyze::AccessCount undef
App::Hachero::Plugin::Analyze::URI undef
App::Hachero::Plugin::Analyze::UserAgent undef
App::Hachero::Plugin::Base undef
App::Hachero::Plugin::Classify::Robot undef
App::Hachero::Plugin::Classify::UserAgent undef
App::Hachero::Plugin::Fetch::FTP undef
App::Hachero::Plugin::Fetch::Gunzip undef
App::Hachero::Plugin::Fetch::S3 undef
App::Hachero::Plugin::Filter::AccessTime undef
App::Hachero::Plugin::Filter::URI undef
App::Hachero::Plugin::Input::FTP undef
App::Hachero::Plugin::Input::File undef
App::Hachero::Plugin::Input::Stdin undef
App::Hachero::Plugin::Output::CSV undef
App::Hachero::Plugin::Output::DBIC undef
App::Hachero::Plugin::Output::Dump undef
App::Hachero::Plugin::Output::TT undef
App::Hachero::Plugin::OutputLine::HadoopMap undef
App::Hachero::Plugin::Parse::Common undef
App::Hachero::Plugin::Parse::HadoopReduce undef
App::Hachero::Plugin::Parse::Normalize undef
App::Hachero::Plugin::Summarize::NarrowDown undef
App::Hachero::Plugin::Summarize::Scraper undef
App::Hachero::Result undef
App::Hachero::Result::Data undef
App::Hachero::Result::PrimaryPerInstance undef
App::I18N 0.034
App::I18N::Command undef
App::I18N::Command::Auto undef
App::I18N::Command::Gen undef
App::I18N::Command::Help undef
App::I18N::Command::Import undef
App::I18N::Command::Initdb undef
App::I18N::Command::Lang undef
App::I18N::Command::Parse undef
App::I18N::Command::Server undef
App::I18N::Command::Status undef
App::I18N::Command::Update undef
App::I18N::Config undef
App::I18N::DB undef
App::I18N::I18N undef
App::I18N::Logger undef
App::I18N::Web undef
App::I18N::Web::Declare undef
App::I18N::Web::Handler undef
App::I18N::Web::View undef
App::Info 0.55
App::Info::HTTPD 0.55
App::Info::HTTPD::Apache 0.55
App::Info::Handler 0.55
App::Info::Handler::Carp 0.55
App::Info::Handler::Print 0.55
App::Info::Handler::Prompt 0.55
App::Info::Lib 0.55
App::Info::Lib::Expat 0.55
App::Info::Lib::OSSPUUID 0.55
App::Info::RDBMS 0.55
App::Info::RDBMS::PostgreSQL 0.55
App::Info::RDBMS::SQLite 0.55
App::Info::Request 0.55
App::Info::Util 0.55
App::JSONPretty 1
Plack::Middleware::Debug::CatalystStash
App::Jawk 0.01
App::JobLog 1.014
App::JobLog::Command 1.014
App::JobLog::Command::add 1.014
App::JobLog::Command::configure 1.014
App::JobLog::Command::done 1.014
App::JobLog::Command::edit 1.014
App::JobLog::Command::info 1.014
App::JobLog::Command::last 1.014
App::JobLog::Command::modify 1.014
App::JobLog::Command::parse 1.014
App::JobLog::Command::resume 1.014
PONAPI::CLI
App::JobLog::Command::summary 1.014
App::JobLog::Command::today 1.014
App::JobLog::Command::vacation 1.014
App::JobLog::Config 1.014
App::JobLog::Log 1.014
App::JobLog::Log::Day 1.014
App::JobLog::Log::Event 1.014
App::JobLog::Log::Format 1.014
App::JobLog::Log::Line 1.014
App::JobLog::Log::Synopsis 1.014
App::JobLog::Time 1.014
App::JobLog::TimeGrammar 1.014
App::JobLog::Vacation 1.014
App::JobLog::Vacation::Period 1.014
App::LDAP 0.05
App::LDAP::Command undef
App::LDAP::Command::Add undef
App::LDAP::Command::Add::Group undef
App::LDAP::Command::Add::Host undef
App::LDAP::Command::Add::Sudoer undef
App::LDAP::Command::Add::User undef
App::LDAP::Command::Del undef
App::LDAP::Command::Del::Group undef
POE::XS::Loop::EPoll
unless
App::Metabase::Relayd 0.24
App::LDAP::Command::Del::Host undef
App::LDAP::Command::Del::Sudoer undef
App::LDAP::Command::Del::User undef
App::LDAP::Command::Export undef
App::LDAP::Command::Help undef
App::LDAP::Command::Import undef
App::LDAP::Command::Passwd undef
App::LDAP::Config undef
App::LDAP::LDIF undef
App::LDAP::LDIF::Group undef
App::LDAP::LDIF::Host undef
App::LDAP::LDIF::Sudoer undef
App::LDAP::LDIF::User undef
Text::Truncate
POE::Component::IKC::ClientLite
App::MadEye 0.07
App::MadEye::Plugin::Agent::Base undef
App::MadEye::Plugin::Agent::DBI undef
App::MadEye::Plugin::Agent::DJabberd undef
App::MadEye::Plugin::Agent::DNS undef
App::MadEye::Plugin::Agent::FTP undef
App::MadEye::Plugin::Agent::FileSync undef
App::MadEye::Plugin::Agent::Gearmand undef
App::MadEye::Plugin::Agent::HTTP undef
App::MadEye::Plugin::Agent::Memcached undef
App::MadEye::Plugin::Agent::Mogilefsd undef
App::MadEye::Plugin::Agent::MySQLSlave undef
App::MadEye::Plugin::Agent::POP3S undef
App::MadEye::Plugin::Agent::Perlbal undef
App::MadEye::Plugin::Agent::Ping undef
App::MadEye::Plugin::Agent::Process undef
App::MadEye::Plugin::Agent::SMTP undef
App::MadEye::Plugin::Agent::SMTPTLS undef
App::MadEye::Plugin::Agent::SNMP::Disk undef
App::MadEye::Plugin::Agent::SNMP::IPAddress undef
App::MadEye::Plugin::Agent::SNMP::Process undef
App::MadEye::Plugin::Agent::SSLExpireDate undef
App::MadEye::Plugin::Agent::Sleep undef
App::MadEye::Plugin::Agent::WriteDisk undef
App::MadEye::Plugin::Base undef
App::MadEye::Plugin::Check::Flock undef
App::MadEye::Plugin::Check::Network undef
App::MadEye::Plugin::Check::User undef
App::MadEye::Plugin::Notify::Debug undef
App::MadEye::Plugin::Notify::Email undef
App::MadEye::Plugin::Notify::IKC undef
App::MadEye::Plugin::Notify::IMKayac undef
App::MadEye::Plugin::Notify::Nakanobu undef
App::MadEye::Plugin::Notify::XMPP undef
App::MadEye::Plugin::Worker::Gearman undef
App::MadEye::Plugin::Worker::Simple undef
App::MadEye::Rule undef
App::MadEye::Rule::DateTimeCron undef
App::MadEye::Rule::Interval undef
App::MadEye::Rule::Retry undef
App::MadEye::Util undef
Text::Padding
ORDB::CPAN::Mageia
RRDTool::OO
URPM
App::Magpie 1.110590
App::Magpie::App 1.110590
App::Magpie::App::Command 1.110590
App::Magpie::App::Command::bswait 1.110590
App::Magpie::App::Command::checkout 1.110590
App::Magpie::App::Command::config 1.110590
App::Magpie::App::Command::fixspec 1.110590
App::Magpie::App::Command::update 1.110590
App::Magpie::Config 1.110590
App::Maisha 0.15
App::Maisha::Plugin::Base 0.15
App::Maisha::Plugin::Identica 0.15
App::Maisha::Plugin::Test 0.15
App::Maisha::Plugin::Twitter 0.15
App::Maisha::Shell 0.15
Math::Factor::XS
Math::NumSeq
Gtk2::Ex::QuadButton
X11::Protocol
X11::Protocol::Other
Image::Base::X11::Protocol::Drawable
Image::Base::X11::Protocol::Pixmap
Image::Base::X11::Protocol::Window
Math::NumSeq::AlphabeticalLength
Math::NumSeq::AlphabeticalLengthSteps
Math::NumSeq::Aronson
Math::NumSeq::SevenSegments
Math::PlanePath::LCornerReplicate
Math::PlanePath::LCornerTree
Math::PlanePath::OneOfEight
Math::PlanePath::PisanoPeriod
Math::PlanePath::PisanoPeriodSteps
Math::PlanePath::ToothpickReplicate
Math::PlanePath::ToothpickSpiral
Math::PlanePath::ToothpickTree
Math::PlanePath::ToothpickUpist
X11::AtomConstants
X11::Protocol::WM
X11::Protocol::XSetRoot
App::MathImage 48
App::MathImage::Curses::Drawing 48
App::MathImage::Curses::Main 48
App::MathImage::Generator 48
App::MathImage::Gtk2::AboutDialog 48
App::MathImage::Gtk2::Drawing 48
App::MathImage::Gtk2::Drawing::Values 48
App::MathImage::Gtk2::Ex::AdjustmentBits 48
App::MathImage::Gtk2::Ex::DirButton 48
App::MathImage::Gtk2::Ex::GdkColorBits 48
App::MathImage::Gtk2::Ex::LayoutBits 48
App::MathImage::Gtk2::Ex::Menu::ForComboBox 48
App::MathImage::Gtk2::Ex::PixbufBits 48
App::MathImage::Gtk2::Ex::QuadScroll 48
App::MathImage::Gtk2::Ex::QuadScroll::ArrowButton 48
App::MathImage::Gtk2::Ex::ScrollButtons 48
App::MathImage::Gtk2::Ex::Splash 48
App::MathImage::Gtk2::Ex::SplashGdk 48
App::MathImage::Gtk2::Ex::Statusbar::PointerPosition 48
App::MathImage::Gtk2::Ex::ToolItem::ComboText 48
App::MathImage::Gtk2::Ex::ToolItem::ComboText::MenuItem 48
App::MathImage::Gtk2::Ex::ToolItem::ComboText::MenuView 48
App::MathImage::Gtk2::Generator 48
App::MathImage::Gtk2::Main 48
App::MathImage::Gtk2::OeisSpinButton 48
App::MathImage::Gtk2::PodDialog 48
App::MathImage::Gtk2::SaveDialog 48
App::MathImage::Gtk2::X11 48
App::MathImage::Image::Base::Caca 48
App::MathImage::Image::Base::LifeBitmap 48
App::MathImage::Image::Base::LifeRLE 48
App::Rad::Tester undef
App::MathImage::Image::Base::Magick 48
App::MathImage::Image::Base::Other 48
App::MathImage::Image::Base::X::Drawable 48
App::MathImage::Image::Base::X::Pixmap 48
App::MathImage::Iterator::Aronson 48
App::MathImage::Iterator::Simple::Aronson 48
App::MathImage::NumSeq::Array 48
App::MathImage::NumSeq::File 48
App::MathImage::NumSeq::FileWriter 48
App::MathImage::NumSeq::OeisCatalogue 48
App::MathImage::NumSeq::OeisCatalogue::Base 48
App::MathImage::NumSeq::OeisCatalogue::Plugin::BuiltinCalc 48
App::MathImage::NumSeq::OeisCatalogue::Plugin::BuiltinTable 48
App::MathImage::NumSeq::OeisCatalogue::Plugin::ZFiles 48
App::MathImage::NumSeq::Radix 48
App::MathImage::NumSeq::Sequence 48
App::MathImage::NumSeq::Sequence::AbundantNumbers 48
App::MathImage::NumSeq::Sequence::All 48
App::MathImage::NumSeq::Sequence::Aronson 48
App::MathImage::NumSeq::Sequence::Base4Without3 48
App::MathImage::NumSeq::Sequence::Beastly 48
App::MathImage::NumSeq::Sequence::BinaryLengths 48
App::MathImage::NumSeq::Sequence::ChampernowneBinary 48
App::MathImage::NumSeq::Sequence::ChampernowneBinaryLsb 48
App::MathImage::NumSeq::Sequence::Count::PrimeFactors 48
App::MathImage::NumSeq::Sequence::Cubes 48
App::MathImage::NumSeq::Sequence::Digits::DigitsModulo 48
App::MathImage::NumSeq::Sequence::Digits::Fraction 48
App::MathImage::NumSeq::Sequence::Digits::Ln2Bits 48
App::MathImage::NumSeq::Sequence::Digits::PiBits 48
App::MathImage::NumSeq::Sequence::Digits::Sqrt 48
App::MathImage::NumSeq::Sequence::Emirps 48
App::MathImage::NumSeq::Sequence::Even 48
App::MathImage::NumSeq::Sequence::Expression 48
App::MathImage::NumSeq::Sequence::Factorials 48
App::MathImage::NumSeq::Sequence::Fibonacci 48
App::MathImage::NumSeq::Sequence::GolayRudinShapiro 48
App::MathImage::NumSeq::Sequence::GoldenSequence 48
App::MathImage::NumSeq::Sequence::Lines 48
App::MathImage::NumSeq::Sequence::LinesLevel 48
App::MathImage::NumSeq::Sequence::LucasNumbers 48
App::MathImage::NumSeq::Sequence::MobiusFunction 48
App::MathImage::NumSeq::Sequence::Multiples 48
App::MathImage::NumSeq::Sequence::NumaronsonA 48
App::MathImage::NumSeq::Sequence::OEIS 48
App::MathImage::NumSeq::Sequence::OEIS::File 48
App::MathImage::NumSeq::Sequence::ObstinateNumbers 48
App::MathImage::NumSeq::Sequence::Odd 48
App::MathImage::NumSeq::Sequence::Padovan 48
App::MathImage::NumSeq::Sequence::Palindromes 48
App::MathImage::NumSeq::Sequence::PellNumbers 48
App::MathImage::NumSeq::Sequence::Pentagonal 48
App::MathImage::NumSeq::Sequence::Perrin 48
App::MathImage::NumSeq::Sequence::PlanePathCoord 48
App::MathImage::NumSeq::Sequence::PlanePathDelta 48
App::MathImage::NumSeq::Sequence::Polygonal 48
App::MathImage::NumSeq::Sequence::PrimeQuadraticEuler 48
App::MathImage::NumSeq::Sequence::PrimeQuadraticHonaker 48
App::MathImage::NumSeq::Sequence::PrimeQuadraticLegendre 48
App::MathImage::NumSeq::Sequence::Primes 48
App::MathImage::NumSeq::Sequence::Pronic 48
App::MathImage::NumSeq::Sequence::RadixWithoutDigit 48
App::MathImage::NumSeq::Sequence::RepdigitAnyBase 48
App::MathImage::NumSeq::Sequence::Repdigits 48
App::MathImage::NumSeq::Sequence::SafePrimes 48
App::MathImage::NumSeq::Sequence::SemiPrimes 48
App::MathImage::NumSeq::Sequence::SophieGermainPrimes 48
App::MathImage::NumSeq::Sequence::Squares 48
App::MathImage::NumSeq::Sequence::StarNumbers 48
App::MathImage::NumSeq::Sequence::TernaryWithout2 48
App::MathImage::NumSeq::Sequence::Tetrahedral 48
App::MathImage::NumSeq::Sequence::Triangular 48
App::MathImage::NumSeq::Sequence::Tribonacci 48
App::MathImage::NumSeq::Sequence::TwinPrimes 48
App::MathImage::NumSeq::Sequence::UndulatingNumbers 48
App::MathImage::NumSeq::Sparse 48
App::MathImage::PlanePath::Flowsnake 43
App::MathImage::PlanePath::OctagramSpiral 43
App::MathImage::Prima::About 48
App::MathImage::Prima::Drawing 48
App::MathImage::Prima::Generator 48
App::MathImage::Prima::Main 48
App::MathImage::Values 43
App::MathImage::Values::AbundantNumbers 43
App::MathImage::Values::All 43
App::MathImage::Values::Aronson 43
App::MathImage::Values::Base4Without3 43
App::MathImage::Values::Base::Radix 43
App::MathImage::Values::Beastly 43
App::MathImage::Values::BinaryLengths 43
App::MathImage::Values::ChampernowneBinary 43
App::MathImage::Values::ChampernowneBinaryLsb 43
App::MathImage::Values::CountPrimeFactors 43
App::MathImage::Values::Cubes 43
App::MathImage::Values::DigitsModulo 43
App::MathImage::Values::Emirps 43
App::MathImage::Values::Even 43
App::MathImage::Values::Expression 43
App::MathImage::Values::Factorials 43
App::MathImage::Values::Fibonacci 43
App::MathImage::Values::FractionDigits 43
App::MathImage::Values::GolayRudinShapiro 43
App::MathImage::Values::GoldenSequence 43
App::MathImage::Values::Lines 43
App::MathImage::Values::LinesLevel 43
App::MathImage::Values::Ln2Bits 43
App::MathImage::Values::LucasNumbers 43
App::MathImage::Values::MobiusFunction 43
App::MathImage::Values::Multiples 43
App::MathImage::Values::NumaronsonA 43
App::MathImage::Values::OEIS 43
App::Prove::Plugin::Distributed
Data::Table::Text
App::MathImage::Values::OEIS::Catalogue 43
App::MathImage::Values::OEIS::Catalogue::Plugin::Builtin 43
App::MathImage::Values::OEIS::File 43
App::MathImage::Values::ObstinateNumbers 43
App::MathImage::Values::Odd 43
App::MathImage::Values::Padovan 43
App::MathImage::Values::Palindromes 43
App::MathImage::Values::PellNumbers 43
App::MathImage::Values::Pentagonal 43
App::MathImage::Values::Perrin 43
App::MathImage::Values::PiBits 43
App::MathImage::Values::Polygonal 43
App::MathImage::Values::PrimeQuadraticEuler 43
App::MathImage::Values::PrimeQuadraticHonaker 43
App::MathImage::Values::PrimeQuadraticLegendre 43
App::MathImage::Values::Primes 43
App::MathImage::Values::Pronic 43
App::MathImage::Values::RadixWithoutDigit 43
App::MathImage::Values::RepdigitAnyBase 43
App::MathImage::Values::Repdigits 43
App::MathImage::Values::SafePrimes 43
App::MathImage::Values::SemiPrimes 43
App::MathImage::Values::SophieGermainPrimes 43
App::MathImage::Values::SqrtDigits 43
App::MathImage::Values::Squares 43
App::MathImage::Values::StarNumbers 43
App::MathImage::Values::TernaryWithout2 43
App::MiseEnPlace 0.14
App::MathImage::Values::Tetrahedral 43
App::MathImage::Values::Triangular 43
App::MathImage::Values::Tribonacci 43
App::MathImage::Values::TwinPrimes 43
App::MathImage::Values::UndulatingNumbers 43
App::MathImage::ValuesArray 43
App::MathImage::ValuesFile 43
App::MathImage::ValuesFileWriter 43
App::MathImage::ValuesSparse 43
App::MathImage::X11::Generator 48
App::MathImage::X11::Protocol::Extras 43
App::MathImage::X11::Protocol::GrabServer 43
App::MathImage::X11::Protocol::Splash 48
App::MathImage::X11::Protocol::WM 48
App::MathImage::X11::Protocol::XSetRoot 48
App::Metabase::Relayd::Plugin 0.24
App::Metabase::Relayd::Plugin::IRC 0.04
App::MrShell 2.0207_1
Music::PitchNum
Music::AtonalUtil
Music::Scales
Music::Canon
Music::Chord::Note
Music::Chord::Positions
File::LoadLines
String::Interpolate::Named
App::Music::ChordPro
App::Music::PlayTab 2.021
App::Music::PlayTab::Chord 1.010
App::Music::PlayTab::LyChord 1.006
App::Music::PlayTab::Note 1.005
App::Music::PlayTab::NoteMap 1.005
Music::LilyPondUtil
Music::Scala
Music::Tempo
Music::Tension
Parse::Range
Text::Roman
App::MusicTools
App::Mver 0.03
App::Mypp 0.11
App::Nopaste::Command undef
App::Nopaste::Service undef
App::Nopaste::Service::Codepeek undef
App::Nopaste::Service::Debian undef
App::Nopaste::Service::Gist undef
App::Nopaste::Service::Hpaste undef
App::Nopaste::Service::Madduck 0.01
App::Nopaste::Service::Mathbin 0.27
App::Nopaste::Service::PastebinCom undef
App::Nopaste::Service::Pastedance 0.04
App::Nopaste::Service::Pastie undef
App::Nopaste::Service::Shadowcat undef
App::Nopaste::Service::Snitch undef
WWW::Pastebin::Sprunge::Create
App::Nopaste::Service::Sprunge 0.004
App::Nopaste::Service::ssh undef
CGI::Application::Dispatch::PSGI
App::Office::CMS 0.92
App::Office::CMS::Controller 0.92
App::Office::CMS::Controller::Backup 0.92
App::Office::CMS::Controller::Content 0.92
App::Office::CMS::Controller::Design 0.92
App::Office::CMS::Controller::Initialize 0.92
App::Office::CMS::Controller::Page 0.92
App::Office::CMS::Controller::Search 0.92
App::Office::CMS::Controller::Site 0.92
App::Office::CMS::Database 0.92
App::Office::CMS::Database::Asset 0.92
App::Office::CMS::Database::Base 0.92
App::Office::CMS::Database::Content 0.92
App::Office::CMS::Database::Design 0.92
App::Office::CMS::Database::Event 0.92
App::Office::CMS::Database::Menu 0.92
App::Office::CMS::Database::Page 0.92
App::Office::CMS::Database::Site 0.92
App::Office::CMS::Util::Config 0.92
App::Office::CMS::Util::Create 0.92
App::Office::CMS::Util::Logger 0.92
App::Office::CMS::Util::Validator 0.92
App::Office::CMS::View 0.92
App::Office::CMS::View::Base 0.92
App::Office::CMS::View::Content 0.92
App::Office::CMS::View::Page 0.92
App::Office::CMS::View::Search 0.92
App::Office::CMS::View::Site 0.92
App::OpenVZ::BCWatch 0.03
Org::To::HTML
Org::To::HTML::WordPress
App::OrgUtils
App::PAUSE::cleanup 0.0012
App::Packer::PAR 0.91
App::Pastebin::sprunge 0.007
App::Pmhack 0.002
IO::WrapOutput
App::Pocoirc 0.42
App::Pocoirc::ReadLine 0.42
App::Pocoirc::Status 0.42
App::Pod2Epub 0.04
App::PodLinkCheck 7
App::PodPreview 0.002
App::PrereqGrapher
App::Prove 3.23
App::Prove::Plugin::HTML 0.09
App::Prove::State 3.23
App::Prove::State::Result 3.23
App::Prove::State::Result::Test 3.23
App::QuoteCC 0.10
App::QuoteCC::Input::Fortune 0.10
App::QuoteCC::Input::Text 0.10
App::QuoteCC::Input::YAML 0.10
App::QuoteCC::Output::C 0.10
App::QuoteCC::Output::Lua 0.10
App::QuoteCC::Output::Perl 0.10
App::QuoteCC::Role::Input 0.10
App::QuoteCC::Role::Output 0.10
App::RSS2Leafnode 53
App::RSS2Leafnode::Conf 53
App::Rad::Command undef
App::Rad::Config undef
App::Rad::Exclude 0.01
App::Rad::Help 0.03
App::Rad::Include 0.01
App::Rad::Option undef
App::Rad::Plugin::Abbrev undef
App::Rad::Plugin::Daemonize 0.1.7
App::Rad::Plugin::ReadLine 0.003
App::Rad::Plugin::ReadLine::Demo 0.003
App::Rad::Plugin::ValuePriority 0.02
App::Rad::Shell 0.01
App::Rcsync 0.01
App::Report::Generator 0.002
App::Report::Generator::Command::GenReport 0.002
App::Rgit 0.08
App::Rgit::Command 0.08
App::Rgit::Command::Each 0.08
App::Rgit::Command::Once 0.08
App::Rgit::Config 0.08
App::Rgit::Config::Default 0.08
App::Rgit::Guard 0.08
App::Rgit::Policy 0.08
App::Rgit::Policy::Default 0.08
App::Rgit::Policy::Interactive 0.08
App::Rgit::Policy::Keep 0.08
App::Rgit::Repository 0.08
App::Rgit::Utils 0.08
App::SD 0.74
App::SD::CLI undef
App::SD::CLI::Command undef
App::SD::CLI::Command::Attachment::Content undef
App::SD::CLI::Command::Attachment::Create undef
App::SD::CLI::Command::Browser undef
App::SD::CLI::Command::Clone undef
App::SD::CLI::Command::Help undef
App::SD::CLI::Command::Help::About undef
App::SD::CLI::Command::Help::Aliases undef
App::SD::CLI::Command::Help::Attachments undef
App::SD::CLI::Command::Help::Authors undef
App::SD::CLI::Command::Help::Comments undef
App::SD::CLI::Command::Help::Config undef
App::SD::CLI::Command::Help::Copying undef
App::SD::CLI::Command::Help::Environment undef
App::SD::CLI::Command::Help::History undef
App::SD::CLI::Command::Help::Intro undef
App::SD::CLI::Command::Help::Search undef
App::SD::CLI::Command::Help::Settings undef
App::SD::CLI::Command::Help::Sync undef
App::SD::CLI::Command::Help::Tickets undef
App::SD::CLI::Command::Help::ticket_summary_format undef
App::SD::CLI::Command::Init undef
App::SD::CLI::Command::Log undef
App::ZofCMS::Plugin::Base 0.0111
App::SD::CLI::Command::Publish undef
App::SD::CLI::Command::Server undef
App::SD::CLI::Command::Ticket::Attachment::Create undef
App::SD::CLI::Command::Ticket::Attachment::Search undef
App::SD::CLI::Command::Ticket::Basics undef
App::SD::CLI::Command::Ticket::Comment undef
App::SD::CLI::Command::Ticket::Comment::Create undef
App::SD::CLI::Command::Ticket::Comment::Update undef
App::SD::CLI::Command::Ticket::Comments undef
App::SD::CLI::Command::Ticket::Create undef
App::SD::CLI::Command::Ticket::Details undef
App::SD::CLI::Command::Ticket::Search undef
App::SD::CLI::Command::Ticket::Show undef
App::SD::CLI::Command::Ticket::Update undef
App::SD::CLI::Command::Version undef
App::SD::CLI::Dispatcher undef
App::SD::CLI::Model::Attachment undef
App::SD::CLI::Model::Ticket undef
App::SD::CLI::Model::TicketComment undef
App::SD::CLI::NewReplicaCommand undef
App::SD::Collection::Attachment undef
App::SD::Collection::Comment undef
App::SD::Collection::Ticket undef
App::SD::Config undef
App::SD::ForeignReplica undef
App::SD::ForeignReplica::PullEncoder undef
App::SD::ForeignReplica::PushEncoder undef
App::SD::Model::Attachment undef
App::SD::Model::Comment undef
App::SD::Model::Ticket undef
App::SD::Record undef
App::SD::Replica::debbugs undef
App::SD::Replica::debbugs::PullEncoder undef
App::SD::Replica::debbugs::PushEncoder undef
App::SD::Replica::gcode undef
App::SD::Replica::gcode::PullEncoder undef
App::SD::Replica::gcode::PushEncoder undef
App::SD::Replica::github undef
App::SD::Replica::github::PullEncoder undef
App::SD::Replica::github::PushEncoder undef
App::SD::Replica::hm undef
App::SD::Replica::hm::PullEncoder undef
App::SD::Replica::hm::PushEncoder undef
App::SD::Replica::lighthouse undef
App::SD::Replica::lighthouse::PullEncoder undef
App::SD::Replica::lighthouse::PushEncoder undef
App::SD::Replica::redmine undef
App::SD::Replica::redmine::PullEncoder undef
App::SD::Replica::redmine::PushEncoder undef
App::SD::Replica::rt undef
App::SD::Replica::rt::PullEncoder undef
App::SD::Replica::rt::PushEncoder undef
App::SD::Replica::trac undef
App::SD::Replica::trac::PullEncoder undef
App::SD::Replica::trac::PushEncoder undef
App::SD::Server undef
App::SD::Server::Dispatcher undef
App::SD::Server::View undef
App::SD::Test undef
App::SD::Util undef
App::SmokeBrew 0.28
App::SVN::Bisect 1.1
App::SmokeBox::PerlVersion 0.08
App::SmokeBrew::BuildPerl 0.28
App::SmokeBrew::IniFile 0.28
App::SmokeBrew::PerlVersion 0.28
App::SmokeBrew::Plugin 0.28
App::SmokeBrew::Plugin::BINGOS 0.06
App::SmokeBrew::Plugin::CPANPLUS::YACSmoke 0.28
App::SmokeBrew::Plugin::Null 0.28
App::SmokeBrew::Tools 0.28
App::SmokeBrew::Types 0.28
App::SnerpVortex 1.000
Fliggy
App::SocialCalc::Multiplayer 20100708
App::Software::License 0.01
App::Sqitch
App::StaticImageGallery 0.002
App::StaticImageGallery::Dir 0.002
App::StaticImageGallery::Image 0.002
App::StaticImageGallery::Style::Simple 0.002
App::StaticImageGallery::Style::Source::Dispatcher 0.002
App::StaticImageGallery::Style::Source::FromDir 0.002
App::StaticImageGallery::Style::Source::FromPackage 0.002
App::Staticperl
App::Syndicator 0.006
App::Sys::Info 0.20
Net::Twitter::Lite::WithAPIv1_1
App::Sysadmin::Log::Simple 0.003
App::TailRabbit 0.002
App::TailRabbit::Convertor::Null undef
Term::Filter::Callback
Term::TtyRec::Plus
App::Termcast 0.10
App::Twirc undef
App::Twirc::Plugin::BangCommands undef
App::Twirc::Plugin::SecondaryAccount undef
App::Twirc::Plugin::SquashWhiteSpace undef
Text::Keywords
Text::Tweet
App::Twitch 0.904
App::Upfiles 3
App::Upfiles::Conf 3
App::Upfiles::FTPlazy 3
App::Vimpl 0.05
App::XML::DocBook::Builder 0.0300
App::XML::DocBook::Docmake 0.0300
App::XUL 0.05
App::XUL::Object undef
App::XUL::XML undef
App::Xssh 0.7
App::Xssh::Config 0.7
App::Ylastic::CostAgent 0.006
App::ZofCMS 0.0221
App::ZofCMS::Config 0.0221
App::ZofCMS::Output 0.0221
App::ZofCMS::Plugin::AccessDenied 0.0101
App::ZofCMS::Plugin::AntiSpamMailTo 0.0101
App::ZofCMS::Plugin::AutoDump 0.0101
App::ZofCMS::Plugin::AutoEmptyQueryDelete 0.0101
App::ZofCMS::Plugin::AutoIMGSize 0.0102
App::ZofCMS::Plugin::Barcode 0.0103
App::ZofCMS::Plugin::BasicLWP 0.0104
App::ZofCMS::Plugin::BoolSettingsManager 0.0102
App::ZofCMS::Plugin::BreadCrumbs 0.0103
App::ZofCMS::Plugin::CSSMinifier 0.0104
App::ZofCMS::Plugin::Captcha 0.0102
App::ZofCMS::Plugin::Comments 0.0102
App::ZofCMS::Plugin::ConditionalRedirect 0.0101
App::ZofCMS::Plugin::ConfigToTemplate 0.0104
App::ZofCMS::Plugin::Cookies 0.0104
App::ZofCMS::Plugin::CurrentPageURI 0.0101
App::ZofCMS::Plugin::DBI 0.0402
App::ZofCMS::Plugin::DBIPPT 0.0104
App::ZofCMS::Plugin::DataToExcel 0.0101
App::ZofCMS::Plugin::DateSelector 0.0112
App::ZofCMS::Plugin::Debug::Dumper 0.0102
App::ZofCMS::Plugin::Debug::Validator::HTML 0.0102
App::ZofCMS::Plugin::DirTreeBrowse 0.0105
App::ZofCMS::Plugin::Doctypes 0.0101
App::ZofCMS::Plugin::FeatureSuggestionBox 0.0101
App::ZofCMS::Plugin::FileList 0.0101
App::ZofCMS::Plugin::FileToTemplate 0.0101
App::ZofCMS::Plugin::FileTypeIcon 0.0105
App::ZofCMS::Plugin::FileUpload 0.0114
App::ZofCMS::Plugin::FloodControl 0.0103
App::ZofCMS::Plugin::FormChecker 0.0341
App::ZofCMS::Plugin::FormFiller 0.0101
App::ZofCMS::Plugin::FormMailer 0.0222
App::ZofCMS::Plugin::FormToDatabase 0.0101
App::ZofCMS::Plugin::GetRemotePageTitle 0.0104
App::ZofCMS::Plugin::GoogleCalculator 0.0103
App::ZofCMS::Plugin::GooglePageRank 0.0103
App::ZofCMS::Plugin::GoogleTime 0.0103
App::ZofCMS::Plugin::HTMLFactory 0.0102
App::ZofCMS::Plugin::HTMLFactory::Entry 0.0101
App::ZofCMS::Plugin::HTMLFactory::PageToBodyId 0.0102
App::ZofCMS::Plugin::HTMLMailer 0.0102
App::ZofCMS::Plugin::ImageGallery 0.0203
App::ZofCMS::Plugin::ImageResize 0.0103
App::ZofCMS::Plugin::InstalledModuleChecker 0.0102
App::ZofCMS::Plugin::JavaScriptMinifier 0.0102
App::ZofCMS::Plugin::LinkifyText 0.0110
App::ZofCMS::Plugin::LinksToSpecs::CSS 0.0104
App::ZofCMS::Plugin::LinksToSpecs::HTML 0.0101
App::ZofCMS::Plugin::NavMaker 0.0103
App::ZofCMS::Plugin::PreferentialOrder 0.0103
App::ZofCMS::Plugin::QueryToTemplate 0.0102
App::ZofCMS::Plugin::QuickNote 0.0107
App::ZofCMS::Plugin::RandomBashOrgQuote 0.0102
App::ZofCMS::Plugin::RandomPasswordGenerator 0.0102
App::ZofCMS::Plugin::RandomPasswordGeneratorPurePerl 0.0103
App::ZofCMS::Plugin::Search::Indexer 0.0102
App::ZofCMS::Plugin::SendFile 0.0103
App::ZofCMS::Plugin::Session 0.0102
App::ZofCMS::Plugin::SplitPriceSelect 0.0103
App::ZofCMS::Plugin::StartPage 0.0102
App::ZofCMS::Plugin::StyleSwitcher 0.0102
App::ZofCMS::Plugin::Sub 0.0101
App::ZofCMS::Plugin::Syntax::Highlight::CSS 0.0102
App::ZofCMS::Plugin::Syntax::Highlight::HTML 0.0101
App::ZofCMS::Plugin::TOC 0.0103
App::ZofCMS::Plugin::TagCloud 0.0103
App::ZofCMS::Plugin::Tagged 0.0252
App::ZofCMS::Plugin::UserLogin 0.0213
App::ZofCMS::Plugin::UserLogin::ChangePassword 0.0110
App::ZofCMS::Plugin::UserLogin::ForgotPassword 0.0112
App::ZofCMS::Plugin::ValidationLinks 0.0101
App::ZofCMS::Plugin::YouTube 0.0104
App::ZofCMS::PluginReference 0.0107
App::ZofCMS::Template 0.0221
App::ZofCMS::Test::Plugin 0.0104
App::assh
App::colourhexdump 0.01011315
App::colourhexdump::ColourProfile 0.01011315
App::colourhexdump::DefaultColourProfile 0.01011315
App::colourhexdump::Formatter 0.01011315
App::cpanbaker 0.05
CPAN::Testers::Common::Client
App::cpanminus::reporter
App::distfind 1.101400
App::gh::API undef
App::gh::Command undef
App::gh::Command::All undef
App::gh::Command::Clone undef
App::gh::Command::Commit undef
App::gh::Command::Drop undef
App::gh::Command::Fork undef
App::gh::Command::Help undef
App::gh::Command::Import undef
App::gh::Command::Info undef
App::gh::Command::List undef
App::gh::Command::Network undef
App::gh::Command::Pull undef
App::gh::Command::Push undef
App::gh::Command::Recent undef
App::gh::Command::Search undef
App::gh::Command::Setup undef
App::gh::Command::Update undef
App::gh::Command::Upload undef
App::gh::Config undef
App::gh::Utils undef
App::gist 0.04
App::highlight 0.06
App::jt
App::lntree 0.0013
App::local::lib::helper 0.04
App::mkfeyorm 0.005
App::moduleshere 0.08
App::moduleswhere 0.02
App::perlhl 0.002
App::perlminlint
App::perlmv 0.41
App::perlmv::scriptlets::std 0.41
App::perlzonji 1.101610
App::perlzonji::Plugin::FoundIn 1.101610
App::perlzonji::Plugin::Label 1.101610
App::pmuninstall 0.16
App::perlzonji::Plugin::SpecialPackages 1.101610
App::perlzonji::Plugin::UseModule 1.101610
App::prove4air 0.0013
App::redisp 0.11
App::redisp::Commands 0.11
App::redisp::EvalWithLexicals 1.000000
App::starbucksloginator 0.0011
App::test::travis
AppConfig::Args 1.65
AppConfig::CGI 1.65
AppConfig::File 1.65
AppConfig::Getopt 1.65
AppConfig::State 1.65
AppConfig::Std 1.07
AppConfig::Sys 1.65
Archive::Any::Create::Tar undef
Archive::Any::Create::Zip undef
Archive::Any::Plugin undef
Archive::Any::Plugin::Tar undef
Archive::Any::Plugin::Zip 0.03
Archive::Cpio 0.08
Archive::Cpio::Common undef
Archive::Cpio::File undef
Archive::Cpio::FileHandle_with_pushback undef
Archive::Cpio::NewAscii undef
Archive::Cpio::ODC undef
Archive::Cpio::OldBinary undef
Archive::Libarchive::FFI
Archive::Peek::Tar undef
Archive::Peek::Zip undef
Archive::Tar 1.76
Archive::Tar::Constant 1.76
Archive::Tar::File 1.76
Archive::Unzip::Burst
Archive::Zip::Archive 1.30
Archive::Zip::BufferedFileHandle 1.30
Archive::Zip::Crypt
Archive::Zip::DirectoryMember 1.30
Archive::Zip::FileMember 1.30
Archive::Zip::Member 1.30
Archive::Zip::MemberRead 1.30
Archive::Zip::MockFileHandle 1.30
Archive::Zip::NewFileMember 1.30
Archive::Zip::StringMember 1.30
Archive::Zip::Tree 1.30
Archive::Zip::ZipFileMember 1.30
Array::Average 0.02
Array::Base 0.003
Array::Find 0.05
Array::Group 2.0a
Array::IntSpan 2.002
Array::IntSpan::Fields 1.005
Array::IntSpan::IP 1.01
Array::Iterator::BiDirectional 0.01
Array::Iterator::Circular 0.01
Array::Iterator::Reusable 0.01
Array::OrdHash 1.03
Array::Queue
Array::Split 1.103261
Array::Transpose 0.05
Array::Transpose::Ragged 0.01
App::reposdb
Data::Sah::Coerce::perl::date::str_alami_en
Git::Bunch 0.05
Git::Repository::Plugin::AUTOLOAD
Git::CPAN::Patch 0.6.0
Git::CPAN::Patch::Import 0.6.0
Git::Class 0.08
Git::Class::Cmd undef
Git::Class::Role::Add undef
Git::Class::Role::Bisect undef
Git::Class::Role::Branch undef
Git::Class::Role::Checkout undef
Git::Class::Role::Clone undef
Git::Class::Role::Commit undef
Git::Class::Role::Config undef
Git::Class::Role::Cwd undef
Git::Class::Role::Diff undef
Git::Class::Role::Error undef
Git::Class::Role::Execute undef
Git::Class::Role::Fetch undef
Git::Class::Role::Grep undef
Git::Class::Role::Init undef
Git::Class::Role::Log undef
Git::Class::Role::Merge undef
Git::Class::Role::Move undef
Git::Class::Role::Pull undef
Git::Class::Role::Push undef
Git::Class::Role::Rebase undef
Git::Class::Role::Remove undef
Git::Class::Role::Reset undef
Git::Class::Role::Show undef
Git::Class::Role::Status undef
Git::Class::Role::Tag undef
Git::Class::Worktree undef
Git::DescribeVersion::App 1.001011
Git::Hook::PostReceive
Git::Repository::Log
Text::Aspell
Text::SpellChecker
Gerrit::REST
Git::Hooks
Git::PurePerl::Actor undef
Git::PurePerl::Config undef
Git::PurePerl::DirectoryEntry undef
Git::PurePerl::Loose undef
Git::PurePerl::NewDirectoryEntry undef
Git::PurePerl::NewObject undef
Git::PurePerl::NewObject::Blob undef
Git::PurePerl::NewObject::Commit undef
Git::PurePerl::NewObject::Tag undef
Git::PurePerl::NewObject::Tree undef
Git::PurePerl::Object undef
Git::PurePerl::Object::Blob undef
Git::PurePerl::Object::Commit undef
Git::PurePerl::Object::Tag undef
Git::PurePerl::Object::Tree undef
Git::PurePerl::Pack undef
Git::PurePerl::Pack::WithIndex undef
Git::PurePerl::Pack::WithoutIndex undef
Git::PurePerl::PackIndex undef
Git::PurePerl::PackIndex::Version1 undef
Git::PurePerl::PackIndex::Version2 undef
Git::PurePerl::Protocol undef
Git::PurePerl::Protocol::File undef
Git::PurePerl::Protocol::Git undef
Git::PurePerl::Protocol::SSH undef
Git::Repository::Plugin::Log 1.01
Git::TagVersion
Text::ASCIITable::Wrap 0.2
Text::Abbrev 1.01
Text::BarGraph 1.1
Text::BasicTemplate
Text::Bayon 0.00002
Text::BibTeX 0.55
Text::BibTeX::BibFormat undef
Text::BibTeX::BibSort undef
Text::BibTeX::Entry undef
Text::BibTeX::File undef
Text::CSV::Separator 0.20
Text::CSV::Auto 0.06
Text::BibTeX::Name undef
Text::BibTeX::NameFormat undef
Text::BibTeX::Structure undef
Text::BibTeX::Value undef
Text::Bidi
Text::CSV::Auto::ExportTo 0.06
Text::CSV::Auto::ExportTo::CSV 0.06
Text::CSV::Auto::ExportTo::MySQL 0.02
Text::CSV::Auto::ExportToDB 0.06
Text::CSV::Auto::Plugin::ExportToCSV 0.06
Text::CSV::Auto::Plugin::ExportToMySQL 0.02
Text::CSV::Easy
Text::CSV::Encoded::Coder::Base 0.02
Text::CSV::Encoded::Coder::Encode 0.04
Text::CSV::Encoded::Coder::EncodeGuess 0.03
Text::CSV::Hashify
Text::CSV::R 0.3
Text::CSV::R::Matrix 0.3
Text::CSV::Slurp 0.9
Text::CSV_PP 1.29
Text::CSV_PP::Iterator 1.03
Text::ClearSilver 0.10.5.4
Text::ClearSilver::Compat undef
Text::ClearSilver::FunctionSet undef
Text::ClearSilver::FunctionSet::html undef
Text::ClearSilver::FunctionSet::string undef
Text::Clevery 0.0004
Text::Clevery::Context undef
Text::Clevery::Function undef
Text::Clevery::Modifier undef
Text::Clevery::Parser undef
Text::Clevery::Util undef
Text::Context::EitherSide 1.4
Text::Context 3.7
Text::Corpus::Summaries::Wikipedia 0.21
Text::DAWG 0.001
Text::DHCPparse 0.10
Text::DSV
Text::Darts 0.09
Text::DelimMatch 1.06
Text::Diff3 0.10
Text::Diff3::Base 0.08
Text::Diff3::Diff3 0.08
Text::Diff3::DiffHeckel 0.08
Text::Diff3::Factory 0.08
Text::Diff3::List 0.08
Text::Diff3::ListMixin 0.08
Text::Diff3::Range2 0.08
Text::Diff::Table 1.37
Text::Diff3::Range3 0.08
Text::Diff3::Text 0.08
Text::English 0.01
Combine::Keys
Text::Extract::MaketextCallPhrases 0.1
Text::Extract::Stuff 0.03
Text::Extract::Word 0.02
Text::ExtractWords 0.08
Text::FixedLengthMultiline 0.07
Text::FixedWidth 0.08
Text::Flow::Wrap 0.01
Text::Fold 0.4
Text::Fuzzy
Text::German::Adjektiv undef
Text::German::Ausnahme undef
Text::German::Cache undef
Text::German::Endung undef
Text::German::Regel undef
Text::German::Util undef
Text::German::Verb undef
Text::German::Vorsilbe undef
Text::GuessEncoding 0.07
Text::Hyphen 0.11
Text::Keywords::Container 0.900
Text::Keywords::Found 0.900
Text::Keywords::List 0.900
Text::KnuthPlass 1.02
Text::Language::Guess 0.02
Text::LineFold 2011.03
Text::Markdown::Discount
Text::Markup::HTML 0.12
Text::Markup::Markdown 0.12
Text::Markup::Mediawiki 0.12
Text::Markup::Multimarkdown 0.12
Text::Markup::None 0.12
Text::Markup::Pod 0.12
Text::Markup::Textile 0.12
Text::Markup::Trac 0.12
Text::MatchRatingCodex 0.01
Text::MediawikiFormat::Blocks undef
Text::Metaphone
Text::MicroMason::AllowGlobals undef
Text::MicroMason::ApacheHandler undef
Text::MicroMason::Base undef
Text::MicroMason::Cache::File undef
Text::MicroMason::Cache::Null undef
Text::MicroMason::Cache::Simple undef
Text::MicroMason::CatchErrors undef
Text::MicroMason::CompileCache undef
Text::MicroMason::Debug undef
Text::MicroMason::DoubleQuote undef
Text::MicroMason::Embperl undef
Text::MicroMason::ExecuteCache undef
Text::MicroMason::Filters undef
Text::MicroMason::Functions undef
Text::MicroMason::HTMLMason undef
Text::MicroMason::HTMLTemplate undef
Text::MicroMason::HasParams undef
Text::MicroMason::LineNumbers undef
Text::MicroMason::PLP 1.9
Text::MicroMason::PassVariables undef
Text::MicroMason::PostProcess undef
Text::MicroMason::QuickTemplate undef
Text::MicroMason::Safe undef
Text::MicroMason::ServerPages undef
Text::MicroMason::Sprintf undef
Text::MicroMason::StoreOne undef
Text::MicroMason::TemplateDir undef
Text::MicroMason::TemplatePath undef
Text::MicroMason::TextTemplate undef
Text::MicroTemplate::DataSection 0.01
Text::MicroTemplate::DataSectionEx 0.01
Text::MiniTmpl 1.1.1
Text::Multi 0.01
Text::Multi::Block 0.01
Text::Multi::Block::Code 0.01
Text::Multi::Block::HTML 0.01
Text::Multi::Block::Markdown 0.01
Text::Multi::Block::MediaWiki 0.01
Text::Multi::Block::Metadata 0.01
Text::Multi::Block::Pod 0.01
Text::Multi::Block::Textile 0.01
Text::Multi::Block::Trac 0.01
GD::Text::Align 1.18
Text::Multi::Block::Wiki 0.01
Text::Multi::Block::WikiCreole 0.01
Text::MultiPhone 0.01
Text::MultiPhone::de 1.010101
Text::MultiPhone::no 1.010101
Text::NSP 1.23
Text::NSP::Measures 0.97
Text::NSP::Measures::2D 0.97
Text::NSP::Measures::2D::CHI 1.03
Text::NSP::Measures::2D::CHI::phi 0.97
Text::NSP::Measures::2D::CHI::tscore 0.97
Text::NSP::Measures::2D::CHI::x2 0.97
Text::NSP::Measures::2D::Dice 0.97
Text::NSP::Measures::2D::Dice::dice 0.97
Text::NSP::Measures::2D::Dice::jaccard 0.97
Text::NSP::Measures::2D::Fisher 0.97
Text::NSP::Measures::2D::Fisher2 0.97
Text::NSP::Measures::2D::Fisher2::left 0.97
Text::NSP::Measures::2D::Fisher2::right 0.97
Text::NSP::Measures::2D::Fisher2::twotailed 0.97
Text::NSP::Measures::2D::Fisher::left 0.97
Text::NSP::Measures::2D::Fisher::right 0.97
Text::NSP::Measures::2D::Fisher::twotailed 0.97
Text::NSP::Measures::2D::MI 1.03
Text::NSP::Measures::2D::MI::ll 0.97
Text::NSP::Measures::2D::MI::pmi 0.97
Text::NSP::Measures::2D::MI::ps 0.97
Text::NSP::Measures::2D::MI::tmi 0.97
Text::NSP::Measures::2D::odds 0.97
Text::NSP::Measures::3D 0.97
Text::NSP::Measures::3D::MI 1.03
Text::NSP::Measures::3D::MI::ll 0.97
Text::NSP::Measures::3D::MI::pmi 0.97
Text::NSP::Measures::3D::MI::ps 0.97
Text::NSP::Measures::3D::MI::tmi 0.97
Text::NSP::Measures::4D 0.97
Text::NSP::Measures::4D::MI 1.03
Text::NSP::Measures::4D::MI::ll 0.97
Net::Prometheus
Text::OverlapFinder 0.03
Text::PDF::Array undef
Text::PDF::Bool undef
Text::PDF::Dict undef
Text::PDF::Filter undef
Text::PDF::Name undef
Text::PDF::Null undef
Text::PDF::Number undef
Text::PDF::Objind undef
Text::PDF::Page undef
Text::PDF::Pages undef
Text::PDF::SFont undef
Text::PDF::String undef
Text::Phonetic 2.03
Text::PDF::TTFont undef
Text::PDF::Utils undef
Text::Phonetic::Caverphone 1.0
Text::Phonetic::DaitchMokotoff undef
Text::Phonetic::DoubleMetaphone undef
Text::Phonetic::Koeln undef
Text::Phonetic::MatchRatingCodex 1.0
Text::Phonetic::Metaphone undef
Text::Phonetic::MultiPhone 1.0
Text::Phonetic::Nysiis 1.0
Text::Phonetic::Phonem undef
Text::Phonetic::Phonix undef
Text::Phonetic::Soundex undef
Text::Phonetic::Soundex::Length 1.0
Text::Phonetic::TransMetaphone 1.0
Text::Phonetic::VideoGame 0.03
Text::Pipe 0.10
Text::Pipe::Append 0.10
Text::Pipe::Base 0.10
Text::Pipe::Chomp 0.10
Text::Pipe::Chop 0.10
Text::Pipe::Code 0.10
Text::Pipe::List::First 0.10
Text::Pipe::List::Grep 0.10
Text::Pipe::List::Map 0.10
Text::Pipe::List::Max 0.10
Text::Pipe::List::MaxStr 0.10
Text::Pipe::List::Min 0.10
Text::Pipe::List::MinStr 0.10
Text::Pipe::List::Pop 0.10
Text::Pipe::List::Reduce 0.10
Text::Pipe::List::Shift 0.10
Text::Pipe::List::Shuffle 0.10
Text::Pipe::List::Size 0.10
Text::Pipe::List::Sort 0.10
Text::Pipe::List::Sum 0.10
Text::Pipe::Lowercase 0.10
Text::Pipe::LowercaseFirst 0.10
Text::Pipe::Multiplex 0.10
Text::Pipe::Prepend 0.10
Text::Pipe::Print 0.10
Text::Pipe::Repeat 0.10
Text::Pipe::Reverse 0.10
Text::Pipe::Say 0.10
Text::Pipe::Split 0.10
Text::Pipe::Stackable 0.10
Text::Pipe::Tester 0.10
Text::Pipe::Trim 0.10
Text::Pipe::Uppercase 0.10
Text::Pipe::UppercaseFirst 0.10
Object::By
Text::Placeholder 0.03
Text::Record::Deduper 0.07
Text::RecordParser::Object 1.4.0
Text::RecordParser::Tab 1.4.0
Text::Refer::Parser
Text::Sass::Expr 0.1
Text::Sass::Functions 0.1
Text::Scan 0.31
Text::Sentence 0.006
Text::Shellwords::Cursor 0.81
Text::Shorten 0.03
Text::Similarity 0.08
Text::Similarity::Overlaps 0.05
Text::Snippet 0.04
Text::Snippet::TabStop 0.04
Text::Snippet::TabStop::Basic 0.04
Text::Snippet::TabStop::Cursor 0.04
Text::Snippet::TabStop::Parser 0.04
Text::Snippet::TabStop::WithDefault 0.04
Text::Snippet::TabStop::WithTransformer 0.04
Text::SpamAssassin 2.001
Text::Starfish 1.16
Text::TOC 0.07
Text::TOC::Filter::Anon 0.07
Text::TOC::HTML 0.07
Text::TOC::InputHandler::HTML 0.07
Text::TOC::Node::HTML 0.07
Text::TOC::OutputHandler::HTML 0.07
Text::TOC::Role::Filter 0.07
Text::TOC::Role::InputHandler 0.07
Text::TOC::Role::Node 0.07
Text::TOC::Role::OutputHandler 0.07
Text::TOC::Types 0.07
Text::TOC::Types::Internal 0.07
Text::Tabs 2009.0305
Text::Template::Base 1.45
Text::Template::Library 0.04
Text::Template::Preprocess 1.45
Text::Template::Simple::Base::Compiler 0.83
Text::Template::Simple::Base::Examine 0.83
Text::Template::Simple::Base::Include 0.83
Text::Template::Simple::Base::Parser 0.83
Text::Template::Simple::Cache 0.83
Text::Template::Simple::Cache::ID 0.83
Text::Template::Simple::Caller 0.83
Text::Template::Simple::Compiler 0.83
Text::Template::Simple::Compiler::Safe 0.83
Text::Template::Simple::Constants 0.83
Text::Template::Simple::Dummy 0.83
Text::Template::Simple::IO 0.83
Text::Template::Simple::Tokenizer 0.83
Text::Template::Simple::Util 0.83
Text::Templet 3.0
Text::Tmpl 0.33
Text::Trac::BlockNode undef
Text::Trac::Blockquote undef
Text::Trac::Context undef
Text::Trac::Dl undef
Text::Trac::Heading undef
Text::Trac::Hr undef
Text::Trac::InlineNode undef
Text::Trac::LinkResolver undef
Text::Trac::LinkResolver::Attachment undef
Text::Trac::LinkResolver::Changeset undef
Text::Trac::LinkResolver::Comment undef
Text::Trac::LinkResolver::Log undef
Text::Trac::LinkResolver::Milestone undef
Text::Trac::LinkResolver::Report undef
Text::Trac::LinkResolver::Source undef
Text::Trac::LinkResolver::Ticket undef
Text::Trac::LinkResolver::Wiki undef
Text::Trac::Macro undef
Text::Trac::Macro::HelloWorld undef
Text::Trac::Macro::Timestamp undef
Text::Trac::Node undef
Text::Trac::Ol undef
Text::Trac::P undef
Text::Trac::Pre undef
Text::Trac::Table undef
Text::Trac::Text undef
Text::Trac::Ul undef
Graph::Reader::Dot
Text::Tradition::Error
Text::Tradition::Analysis
Text::TransMetaphone 0.07
Text::TransMetaphone::am 0.02
Text::TransMetaphone::ar 0.01
Text::TransMetaphone::chr 0.01
Text::TransMetaphone::el 0.01
Text::TransMetaphone::en_US 0.01
Text::TransMetaphone::gu 0.01
Text::TransMetaphone::he 0.01
Text::TransMetaphone::ja_hiragana 0.01
Text::TransMetaphone::ja_katakana 0.01
Text::TransMetaphone::ru 0.01
Text::TransMetaphone::ti 0.01
Text::Transliterator 1.00
Text::TreeFile 0.39
Text::Variations
Text::WikiFormat::Blocks undef
Text::WordDiff 0.05
Text::WordDiff::ANSIColor 0.05
Text::WordDiff::HTML 0.05
Text::WordGrams 0.04
Text::Wrapper 1.02
Text::Xatena 0.10
Text::Xatena::Inline undef
Text::Xatena::Inline::Aggressive undef
Text::Xatena::Inline::Base undef
Text::Xatena::LineScanner undef
Text::Xatena::Node undef
Text::Xatena::Node::Blockquote undef
Text::Xatena::Node::Comment undef
Text::Xatena::Node::DefinitionList undef
Text::Xatena::Node::List undef
Text::Xatena::Node::Pre undef
Text::Xatena::Node::Root undef
Text::Xatena::Node::Section undef
Text::Xatena::Node::SeeMore undef
Text::Xatena::Node::StopP undef
Text::Xatena::Node::SuperPre undef
Text::Xatena::Node::Table undef
Text::Xatena::Util undef
Text::Xslate::Bridge undef
Text::Xslate::Bridge::Alloy 1.0001
Text::Xslate::Bridge::TT2 1.0002
Text::Xslate::Bridge::TT2Like 0.00007
Text::Xslate::HashWithDefault undef
Text::Xslate::PP 1.1005
Text::Xslate::PP::Booster 1.0012
Text::Xslate::PP::Compiler undef
Text::Xslate::PP::Compiler::CodeGenerator undef
Text::Xslate::PP::Const undef
Text::Xslate::PP::Method undef
Text::Xslate::PP::Opcode 1.1005
Text::Xslate::PP::State undef
Text::Xslate::PP::Type::Macro undef
Text::Xslate::PP::Type::Pair undef
Text::Xslate::PP::Type::Raw undef
Text::Xslate::Runner undef
HTML::Template::Parser
Text::Xslate::Syntax::HTMLTemplate
Text::Xslate::Syntax::Kolon undef
Text::Xslate::Syntax::Metakolon undef
Text::Xslate::Syntax::TTerse undef
Text::Xslate::Type::Raw undef
Text::Zilla 0.003
Text::Zilla::Dir 0.003
Text::Zilla::Dir::DBIC 0.001
Text::Zilla::Dir::DBIC::ResultSet 0.001
Text::Zilla::Dir::FromHash 0.003
Text::Zilla::File 0.003
Text::Zilla::File::TT 0.001
Text::Zilla::Role::Dir 0.003
Text::Zilla::Role::Dir::Base 0.003
Text::Zilla::Role::Dir::ShortBase 0.003
Text::Zilla::Role::Dir::Stash 0.003
Text::Zilla::Role::File 0.003
Text::Zilla::Role::File::Base 0.003
Text::Zilla::Role::File::ShortBase 0.003
Text::Zilla::Role::File::Stash 0.003
Text::Zilla::Role::File::TT 0.001
Text::Zilla::Role::MetaClass 0.003
Text::Zilla::Role::Rights 0.003
Text::Zilla::Role::Stash 0.003
Text::Zilla::Types 0.003
Text::vCard::Addressbook undef
Text::vCard::Node undef
Net::Statsd
Variable::OnDestruct::Scoped
ReadonlyX
Plack::Middleware::Session::Simple
MojoX::Session::Simple
Web::Request
Web::Response
OX
Crypt::OpenPGP::Armour undef
Crypt::OpenPGP::Buffer undef
Crypt::OpenPGP::CFB undef
Crypt::OpenPGP::Certificate undef
Crypt::OpenPGP::Cipher undef
Crypt::OpenPGP::Ciphertext undef
Crypt::OpenPGP::Compressed undef
Crypt::OpenPGP::Config undef
Crypt::OpenPGP::Constants undef
Crypt::OpenPGP::Digest undef
Crypt::OpenPGP::ErrorHandler undef
Crypt::OpenPGP::Key undef
Crypt::OpenPGP::Key::Public undef
Crypt::OpenPGP::Key::Public::DSA undef
Crypt::OpenPGP::Key::Public::ElGamal undef
Crypt::OpenPGP::Key::Public::RSA undef
Crypt::OpenPGP::Key::Secret undef
Crypt::OpenPGP::Key::Secret::DSA undef
Crypt::OpenPGP::Key::Secret::ElGamal undef
Crypt::OpenPGP::Key::Secret::RSA undef
Crypt::OpenPGP::KeyBlock undef
Crypt::OpenPGP::KeyRing undef
Crypt::OpenPGP::KeyServer undef
Crypt::OpenPGP::MDC undef
Crypt::OpenPGP::Marker undef
Crypt::OpenPGP::Message undef
Crypt::OpenPGP::OnePassSig undef
Crypt::OpenPGP::PacketFactory undef
Crypt::OpenPGP::Plaintext undef
Crypt::OpenPGP::S2k undef
Crypt::OpenPGP::SKSessionKey undef
Crypt::OpenPGP::SessionKey undef
Crypt::OpenPGP::Signature undef
Crypt::OpenPGP::Signature::SubPacket undef
Crypt::OpenPGP::Trust undef
Crypt::OpenPGP::UserID undef
Crypt::OpenPGP::Util undef
Crypt::OpenPGP::Words undef
MojoX::NetstringStream
IPC::Open3::Simple
Crypt::RSA::Parse
Protocol::ACME
Lexical::Importer
Tcl
WebService::Dropbox
Sereal
Devel::QuickCover
Slack::RTM::Bot
Dios
String::Normal
OrePAN2
Graph::Simple
Data::Consumer
Mail::Action
Mail::SimpleList
PPIx::Shorthand
PPIx::Refactor
Mojo::DOM58
DOM::Tiny
Config::Grammar
Locale::PO
CallBackery
Lab::Measurement
Hash::Map
Term::Chrome
List::Objects::Types
Quote::Code
Switch::Plain
Types::LoadableClass
Defaults::Modern
POSIX::1003
Protocol::Redis::Faster
Mojo::Redis
Mojo::Redis2
Devel::FindRef
Devel::Leak::Cb
Minion::Backend::File
Mojar
Mojar::Cron
Minion::Backend::Storable
Mojar::Message
Params::CheckCompiler
Plack::App::ServiceStatus
Redis::DistLock
Mojolicious::Plugin::JSUrlFor
Mojolicious::Plugin::Notifications
Test::Fake::HTTPD
WebService::Simple::Parser::JSON
WebService::Pushover
ExtUtils::ModuleMaker
uni::perl
Math::Permute::Array
Math::Histogram
String::Defer
Hash::MD5
MooseX::Types::PerlVersion
DBIx::Schema::Changelog
Math::Intersection::StraightLine
Courriel
macro
Test::Weaken::ExtraBits
Catalyst::View::XML::Feed
Redis::LeaderBoard
Perl::Lexer
JRPC
Memory::Stats
Stepford
Data::Faker
WWW::Desk
Getopt::Long::DescriptivePod
Crypt::NSS::X509
Date::ISO
Dist::Zilla::Role::ErrorLogger
Dist::Zilla::Plugin::Hook
Mojolicious::Command::listdeps
Crypt::MatrixSSL
Filter::Crypto
Parallel::Benchmark
Mojolicious::Plugin::ChromeLogger
Getopt::Config::FromPod
CLDR::Number::Format::Percent
LWP::ConsoleLogger
GitHub::MergeVelocity
Unicode::Truncate
Sisimai
Tickit
NNexus
Mojolicious::Plugin::Qaptcha
WebService::SQLFormat
Archive::Tar::Builder
Dancer::Plugin::RPC
Mango
Mandel
Tie::Util
Acme::AsciiEmoji
Moonshine::Test
MooX::ReturnModifiers
MooX::VariantAttribute
Debug::Statements
LMDB_File
XML::Loy
Dancer2::Plugin::Auth::Tiny
Dancer2::Plugin::HTTP::Auth::Extensible
Weasel::Widgets::Dojo
Alien::Thrust
Thrust
Test::Simpler
Filter::Encoding
Geo::GoogleEarth::AutoTour
Dist::Zilla::Plugin::AutoPrereqsFast
Test::Mock::Wrapper
Mojo::Server::TCP
Minion::Command::minion::jobx
App::Cmd::Plugin::Prompt
App::Critique
Mojolicious::Plugin::OpenAPI
Unicode::Security
Bread::Board::Svc
Data::XLSX::Parser
BSON::Decode
Data::IEEE754::Tools
YAML::LoadBundle
MapReduce::Framework::Simple
MooX::ChainedAttributes
Mew
Meta::Grapher::Moose
URI::Normalize
Plack::Handler::Stomp
MetaCPAN::Clients
Test::HTML::Content
Sub::Inspector
YATT::Lite
Dist::Zilla::Plugin::CheckBin
String::Unescape
App::GitGitr
HiD::Generator::Sass
Template::Plugin::Markdown
Task::BeLike::GENEHACK
Dancer2::Plugin::EncryptID
Locale::TextDomain::OO::Util
Dancer2::Plugin::Flash
Dancer2::Plugin::Redis
XML::Compile::SOAP::Daemon::Dancer2
FusionInventory::Agent::Task undef
FusionInventory::Agent::Task::Base undef
FusionInventory::Agent::Task::Inventory undef
FusionInventory::Agent::Task::Inventory::AccessLog undef
FusionInventory::Agent::Task::Inventory::DeviceID undef
FusionInventory::Agent::Task::Inventory::IpDiscover undef
FusionInventory::Agent::Task::Inventory::IpDiscover::IpDiscover undef
FusionInventory::Agent::Task::Inventory::IpDiscover::Nmap undef
FusionInventory::Agent::Task::Inventory::OS::AIX undef
FusionInventory::Agent::Task::Inventory::OS::AIX::CPU undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Controller undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Domains undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Drives undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Hardware undef
FusionInventory::Agent::Task::Inventory::OS::AIX::IPv4 undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Mem undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Memory undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Modems undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Networks undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Slots undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Software undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Sounds undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Storages undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Users undef
FusionInventory::Agent::Task::Inventory::OS::AIX::Videos undef
FusionInventory::Agent::Task::Inventory::OS::BSD undef
FusionInventory::Agent::Task::Inventory::OS::BSD::Archs::Alpha undef
FusionInventory::Agent::Task::Inventory::OS::BSD::Archs::Sgimips undef
FusionInventory::Agent::Task::Inventory::OS::BSD::Archs::Sparc undef
FusionInventory::Agent::Task::Inventory::OS::BSD::Archs::i386 undef
FusionInventory::Agent::Task::Inventory::OS::BSD::CPU undef
FusionInventory::Agent::Task::Inventory::OS::BSD::Domains undef
FusionInventory::Agent::Task::Inventory::OS::BSD::Drives undef
FusionInventory::Agent::Task::Inventory::OS::BSD::IPv4 undef
FusionInventory::Agent::Task::Inventory::OS::BSD::Mem undef
FusionInventory::Agent::Task::Inventory::OS::BSD::Networks undef
FusionInventory::Agent::Task::Inventory::OS::BSD::Storages undef
FusionInventory::Agent::Task::Inventory::OS::BSD::Sys undef
FusionInventory::Agent::Task::Inventory::OS::BSD::Uptime undef
FusionInventory::Agent::Task::Inventory::OS::Generic undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Dmidecode undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Dmidecode::Battery undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Dmidecode::Bios undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Dmidecode::Memory undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Dmidecode::Ports undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Dmidecode::Slots undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Dmidecode::UUID undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Environement undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Hostname undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Ipmi undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Lspci undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Lspci::Controllers undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Lspci::Modems undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Lspci::Sounds undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Lspci::Videos undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Packaging undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Packaging::BSDpkg undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Packaging::ByHand undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Packaging::Deb undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Packaging::Gentoo undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Packaging::Pacman undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Packaging::RPM undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Packaging::Slackware undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Printers::Cups undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Processes undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Screen undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Storages::HP undef
FusionInventory::Agent::Task::Inventory::OS::Generic::USB undef
FusionInventory::Agent::Task::Inventory::OS::Generic::Users undef
FusionInventory::Agent::Task::Inventory::OS::HPUX undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::Bios undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::CPU undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::Controller undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::Domains undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::Drives undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::MP undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::Memory undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::Networks undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::Slots undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::Software undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::Storages undef
FusionInventory::Agent::Task::Inventory::OS::HPUX::Uptime undef
FusionInventory::Agent::Task::Inventory::OS::Linux undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::ARM undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::ARM::CPU undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::Alpha undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::Alpha::CPU undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::MIPS undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::MIPS::CPU undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::PowerPC undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::PowerPC::CPU undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::PowerPC::Various undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::SPARC undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::SPARC::CPU undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::i386 undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::i386::CPU undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::m68k undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Archs::m68k::CPU undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Distro::LSB undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Distro::NonLSB undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Domains undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Drives undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Inputs undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Mem undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Network::IPv4 undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Network::Networks undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Network::iLO undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Sounds undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Storages undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Storages::3ware undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Storages::Adaptec undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Storages::Lsilogic undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Storages::ServeRaid undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Sys undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Uptime undef
FusionInventory::Agent::Task::Inventory::OS::Linux::Video undef
FusionInventory::Agent::Task::Inventory::OS::MacOS undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Bios undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::CPU undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Domains undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Drives undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Hostname undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::IPv4 undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Mem undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Networks undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Packages undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Printers undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Sound undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Storages undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::USB undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Uptime undef
FusionInventory::Agent::Task::Inventory::OS::MacOS::Video undef
FusionInventory::Agent::Task::Inventory::OS::Solaris undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::Bios undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::CPU undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::Controllers undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::Domains undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::Drives undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::IPv4 undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::Mem undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::Memory undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::Networks undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::Packages undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::Slots undef
FusionInventory::Agent::Task::Inventory::OS::Solaris::Storages undef
FusionInventory::Agent::Task::Inventory::OS::Win32 undef
FusionInventory::Agent::Task::Inventory::OS::Win32::AntiVirus undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Bios undef
FusionInventory::Agent::Task::Inventory::OS::Win32::CPU undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Chassis undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Controller undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Drives undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Env undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Inputs undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Memory undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Modem undef
GD::Graph 1.44
FusionInventory::Agent::Task::Inventory::OS::Win32::Networks undef
FusionInventory::Agent::Task::Inventory::OS::Win32::OS undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Ports undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Printers undef
GD::Polyline 0.2
FusionInventory::Agent::Task::Inventory::OS::Win32::Slots undef
GD::Simple undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Software undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Sounds undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Storages undef
FusionInventory::Agent::Task::Inventory::OS::Win32::USB undef
FusionInventory::Agent::Task::Inventory::OS::Win32::User undef
FusionInventory::Agent::Task::Inventory::OS::Win32::Video undef
FusionInventory::Agent::Task::Inventory::OcsDeploy undef
FusionInventory::Agent::Task::Inventory::Virtualization::Hpvm undef
FusionInventory::Agent::Task::Inventory::Virtualization::Libvirt undef
FusionInventory::Agent::Task::Inventory::Virtualization::Parallels undef
FusionInventory::Agent::Task::Inventory::Virtualization::Qemu undef
FusionInventory::Agent::Task::Inventory::Virtualization::SolarisZones undef
FusionInventory::Agent::Task::Inventory::Virtualization::VirtualBox undef
FusionInventory::Agent::Task::Inventory::Virtualization::Virtuozzo undef
FusionInventory::Agent::Task::Inventory::Virtualization::VmWareDesktop undef
FusionInventory::Agent::Task::Inventory::Virtualization::VmWareESX undef
FusionInventory::Agent::Task::Inventory::Virtualization::Vmsystem undef
FusionInventory::Agent::Task::Inventory::Virtualization::Vserver undef
FusionInventory::Agent::Task::Inventory::Virtualization::Xen undef
FusionInventory::Agent::Task::Inventory::Virtualization::Xen::XM undef
FusionInventory::Agent::Task::Inventory::WinRegistry undef
FusionInventory::Agent::Task::OcsDeploy::P2P undef
FusionInventory::Agent::Task::Ping undef
FusionInventory::Agent::Task::WakeOnLan undef
FusionInventory::Agent::Tools undef
FusionInventory::Agent::Tools::Win32 undef
FusionInventory::Agent::XML::Query undef
FusionInventory::Agent::XML::Query::Inventory undef
FusionInventory::Agent::XML::Query::Prolog undef
FusionInventory::Agent::XML::Query::SimpleMessage undef
FusionInventory::Agent::XML::Response undef
FusionInventory::Agent::XML::Response::Inventory undef
FusionInventory::Agent::XML::Response::Prolog undef
FusionInventory::Agent::XML::Response::SimpleMessage undef
FusionInventory::Compress undef
FusionInventory::Logger undef
FusionInventory::LoggerBackend::File undef
FusionInventory::LoggerBackend::Stderr undef
FusionInventory::LoggerBackend::Syslog undef
G undef
GCC::Node::Binary undef
GCC::Node::Block undef
GCC::Node::Comparison undef
GCC::Node::Constant undef
GCC::Node::Declaration undef
GCC::Node::Exceptional undef
GCC::Node::Expression undef
GCC::Node::Reference undef
GCC::Node::SideEffect undef
GCC::Node::Type undef
GCC::Node::Unary undef
GCC::Tree undef
GD::Barcode::COOP2of5 0.01
GD::Barcode::Code39 1.1
GD::Barcode::EAN13 1.1
GD::Barcode::EAN8 1.1
GD::Barcode::IATA2of5 0.01
GD::Barcode::ITF 0.01
GD::Barcode::Industrial2of5 0.01
GD::Barcode::Matrix2of5 0.01
GD::Barcode::NW7 1.1
GD::Barcode::QRcode 0.01
GD::Barcode::UPCA 1.1
GD::Barcode::UPCE 1.1
GD::Group 1
GD::SecurityImage::GD 1.70
GD::SecurityImage::Magick 1.70
GD::SecurityImage::Styles 1.70
GD::Text::Wrap 1.20
GIS::Distance::Formula::Cosine undef
GIS::Distance::Formula::GeoEllipsoid undef
GIS::Distance::Formula::GreatCircle undef
GIS::Distance::Formula::Haversine undef
GIS::Distance::Formula::MathTrig undef
GIS::Distance::Formula::Polar undef
GIS::Distance::Formula::Vincenty undef
GO::Basic undef
GO::Handlers::abstract_prolog_writer undef
GO::Handlers::abstract_sql_writer undef
GO::Handlers::base undef
GO::Handlers::go_def undef
GO::Handlers::go_ont undef
GO::Handlers::go_xref undef
GO::Handlers::godb_prestore undef
GO::Handlers::lexanalysis undef
GO::Handlers::lexanalysis2sql undef
GO::Handlers::obj undef
GO::Handlers::obj_storable undef
GO::Handlers::obj_yaml undef
GO::Handlers::obo undef
GO::Handlers::obo_godb_flat undef
GO::Handlers::obo_html undef
GO::Handlers::obo_sxpr undef
GO::Handlers::obo_text undef
GO::Handlers::obo_xml undef
GO::Handlers::owl undef
GO::Handlers::owl_to_obo_text undef
GO::Handlers::pathlist undef
GO::Handlers::prolog undef
GO::Handlers::rdf undef
GO::Handlers::summary undef
GO::Handlers::sxpr undef
GO::Handlers::tbl undef
GO::Handlers::text_html undef
GO::Handlers::xml undef
GO::Handlers::xsl_base undef
GO::IO::Analysis undef
GO::IO::Blast undef
GO::IO::Dotty undef
GO::IO::OBDXML undef
GO::IO::ObanOwl undef
GO::IO::RDFXML undef
GO::IO::XML undef
GO::IO::go_assoc undef
GO::Metadata::Panther undef
GO::Model::Association undef
GO::Model::CrossProduct undef
GO::Model::DB undef
GO::Model::Evidence undef
GO::Model::GeneProduct undef
GO::Model::Graph undef
GO::Model::GraphIterator undef
GO::Model::GraphNodeInstance undef
GO::Model::LogicalDefinition undef
GO::Model::Modification undef
GO::Model::Ontology undef
GO::Model::Path undef
GO::Model::Property undef
GO::Model::Relationship undef
GO::Model::Restriction undef
GO::Model::Root undef
GO::Model::Seq undef
GO::Model::Species undef
GO::Model::Term undef
GO::Model::TreeIterator undef
GO::Model::Xref undef
GO::ObjCache undef
GO::ObjFactory undef
GO::Parsers::ParserEventNames undef
GO::Parsers::base_parser undef
GO::Parsers::generic_tagval_parser undef
GO::Parsers::go_assoc_parser undef
GO::Parsers::go_def_parser undef
GO::Parsers::go_ids_parser undef
GO::Parsers::go_ont_parser undef
GO::Parsers::go_xref_parser undef
GO::Parsers::locuslink_parser undef
GO::Parsers::mesh_parser undef
GO::Parsers::mgi_assoc_parser undef
GO::Parsers::ncbi_taxon_names_parser undef
GO::Parsers::ncbi_taxonomy_parser undef
GO::Parsers::obj_emitter undef
GO::Parsers::obj_storable_parser undef
GO::Parsers::obj_yaml_parser undef
GO::Parsers::obo_parser undef
GO::Parsers::obo_text_parser undef
GO::Parsers::obo_xml_parser undef
GO::Parsers::owl_parser undef
GO::Parsers::references_parser undef
GO::Parsers::refgenomes_parser undef
GO::Parsers::unknown_format_parser undef
GO::Parsers::xrf_abbs_parser undef
GO::Utils undef
GPS::Serial 1.00
GRID::Cluster::Handle undef
GRID::Cluster::Result undef
GRID::Machine::IOHandle undef
GRID::Machine::MakeAccessors undef
GRID::Machine::Process undef
GRID::Machine::Result undef
Game::ConnectFour 0.011
Games::ABC_Path::Solver 0.0.3
Games::ABC_Path::Solver::App 0.0.3
Games::ABC_Path::Solver::Base 0.0.3
Games::ABC_Path::Solver::Move 0.0.3
Games::ABC_Path::Solver::Move::LastRemainingCellForLetter 0.0.3
Games::ABC_Path::Solver::Move::LastRemainingLetterForCell 0.0.3
Games::ABC_Path::Solver::Move::LettersNotInVicinity 0.0.3
Games::ABC_Path::Solver::Move::ResultsInASuccess 0.0.3
Games::ABC_Path::Solver::Move::ResultsInAnError 0.0.3
Games::ABC_Path::Solver::Move::TryingLetterForCell 0.0.3
Games::Affenspiel::Board undef
Games::AlphaBeta 0.4.7
Games::AlphaBeta::Position 0.1.2
Games::AlphaBeta::Reversi 0.1.5
Games::FrozenBubble::CStuff undef
Games::FrozenBubble::Config undef
Games::FrozenBubble::LevelEditor undef
Games::FrozenBubble::MDKCommon undef
Games::FrozenBubble::Net undef
Games::FrozenBubble::NetDiscover undef
Games::FrozenBubble::Stuff undef
Games::FrozenBubble::Symbols undef
Games::NES::ROM::Database undef
Games::NES::ROM::Format::INES undef
Games::NES::ROM::Format::UNIF undef
Games::Roguelike::Area 0.5.259
Games::Roguelike::Area::Wilderness undef
Games::Roguelike::Console 0.4.256
Games::Roguelike::Console::ANSI 0.4.258
Games::Roguelike::Console::Curses 0.4.233
Games::Roguelike::Console::Dump 0.4.233
Games::Roguelike::Console::Win32 0.4.247
Games::Roguelike::Item undef
Games::Roguelike::Mob undef
Games::Roguelike::Utils 0.4.236
Games::Roguelike::Utils::Pov_C 0.4.236
Games::Roguelike::World 0.4.256
Games::Roguelike::World::Daemon 0.4.253
Games::SGF::Go 0.993
Games::SGF::Util 0.993
Games::Sequential 0.4.2
Games::Sequential::Position 0.3.1
Games::Solitaire::Verify::App::CmdLine undef
Games::Solitaire::Verify::Base 0.09
Games::Solitaire::Verify::Card 0.09
Games::Solitaire::Verify::Column 0.09
Games::Solitaire::Verify::Exception 0.09
Games::Solitaire::Verify::Foundations 0.09
Games::Solitaire::Verify::Freecells 0.09
Games::Solitaire::Verify::Move 0.09
Games::Solitaire::Verify::Solution 0.09
Games::Solitaire::Verify::State 0.09
Games::Solitaire::Verify::VariantParams 0.09
Games::Solitaire::Verify::VariantsMap 0.09
Games::Tournament 0.18
Games::Tournament::Card 0.18
Games::Tournament::Contestant 0.18
Games::Tournament::Contestant::Swiss 0.18
Games::Word::Wordlist 0.05
Games::Tournament::Contestant::Swiss::Preference 0.18
Games::Tournament::Swiss::Bracket 0.18
Games::Tournament::Swiss::Config 0.18
Games::Tournament::Swiss::Procedure 0.18
Games::Tournament::Swiss::Procedure::Dummy 0.18
Games::Tournament::Swiss::Procedure::FIDE 0.18
Gearman::JobStatus undef
Gearman::Objects undef
Gearman::ResponseParser undef
Gearman::ResponseParser::Taskset undef
Gearman::Server::Client undef
Gearman::Server::Job undef
Gearman::Server::Listener undef
Gearman::Task undef
Gearman::Taskset undef
Gedcom::Date::Approximated 0.06
Gedcom::Date::Interpreted 0.06
Gedcom::Date::Period 0.06
Gedcom::Date::Phrase 0.06
Gedcom::Date::Range 0.06
Gedcom::Date::Simple 0.06
Genezzo::BasicHelp 1.12
Genezzo::Block::RDBArray undef
Genezzo::Block::RDBlkA undef
Genezzo::Block::RDBlk_NN undef
Genezzo::Block::RDBlock undef
Genezzo::Block::RowDir 7.01
Genezzo::Block::Std 7.01
Genezzo::Block::Util 1.02
Genezzo::BufCa::BCFile undef
Genezzo::BufCa::BufCa 7.03
Genezzo::BufCa::BufCaElt 7.07
Genezzo::BufCa::DirtyScalar undef
Genezzo::BufCa::PinScalar undef
Genezzo::Dict 7.29
Genezzo::GenDBI 0.72
Genezzo::Havok 7.19
Genezzo::Havok::Basic undef
Genezzo::Havok::DebugUtils 1.10
Genezzo::Havok::Examples undef
Genezzo::Havok::OO_Examples undef
Genezzo::Havok::SQLCompare 1.02
Genezzo::Havok::SQLScalar 1.24
Genezzo::Havok::SysHelp 1.08
Genezzo::Havok::SysHook 7.14
Genezzo::Havok::UserExtend 7.05
Genezzo::Havok::UserFunctions 1.05
Genezzo::Havok::Utils 1.10
Genezzo::Index::bt2 7.01
Genezzo::Index::bt3 undef
Genezzo::Index::btHash undef
Genezzo::Parse::SQL undef
Genezzo::Plan 7.06
Genezzo::Plan::MakeAlgebra 7.02
Genezzo::Plan::QueryRewrite 1.07
Genezzo::Plan::TypeCheck 7.17
Genezzo::PushHash::HPHRowBlk undef
Genezzo::PushHash::PHArray undef
Genezzo::PushHash::PHFixed undef
Genezzo::PushHash::PHNoUpdate undef
Genezzo::PushHash::PushHash undef
Genezzo::PushHash::hph undef
Genezzo::RawIO undef
Genezzo::Row::RSBlock undef
Genezzo::Row::RSDual undef
Genezzo::Row::RSExpr undef
Genezzo::Row::RSFile undef
Genezzo::Row::RSIdx1 undef
Genezzo::Row::RSJoinA undef
Genezzo::Row::RSTab undef
Genezzo::SpaceMan::SMExtent 1.42
Genezzo::SpaceMan::SMFile 7.16
Genezzo::SpaceMan::SMFreeBlock 1.03
Genezzo::SpaceMan::SMHook 1.30
Genezzo::TSHash undef
Genezzo::Tablespace 7.14
Genezzo::TestSQL undef
Genezzo::TestSetup undef
Genezzo::Util 7.19
Genezzo::XEval 7.05
Genezzo::XEval::Prepare 7.10
Genezzo::XEval::SQLAlter 7.01
Genome::Env::GENOME_DEV_MODE undef
Genome::Env::GENOME_MODEL_DATA undef
Genome::Env::GENOME_MODEL_ROOT undef
Genome::Env::GENOME_MODEL_TESTDIR undef
Genome::Env::GENOME_NO_REQUIRE_USER_VERIFY undef
Genome::Env::GENOME_VIEW_CACHE undef
Genome::Model::Tools undef
Genome::Model::Tools::Example1 undef
Genome::Model::Tools::Example2 undef
Genome::Model::Tools::Install undef
Genome::Site undef
Genome::Sys undef
Genome::Vocabulary undef
Gentoo::MirrorList::Mirror 1.0.0
Gentoo::Overlay::Category 0.02004319
Gentoo::Overlay::Package 0.02004319
Gentoo::Overlay::Types 0.02004319
Geo::Address::Mail::Standardizer::Results undef
Geo::Address::Mail::US undef
Geo::Coder::Many::Bing undef
Geo::Coder::Many::Generic undef
Geo::Coder::Many::Googlev3 undef
Geo::Coder::Many::Mapquest 0.01
Geo::Coder::Many::Multimap undef
Geo::Coder::Many::OSM 0.01
Geo::Coder::Many::Response undef
Geo::Coder::Many::Scheduler undef
Geo::Coder::Many::Scheduler::OrderedList undef
Geo::Coder::Many::Scheduler::Selective undef
Geo::Coder::Many::Scheduler::UniquenessScheduler undef
Geo::Coder::Many::Scheduler::UniquenessScheduler::WRR undef
Geo::Coder::Many::Scheduler::UniquenessScheduler::WeightedRandom undef
Geo::Coder::Many::SimpleGeo 0.01
Geo::Coder::Many::Util undef
Geo::Coordinates::Converter::Datum undef
Geo::Coordinates::Converter::Datum::Grs67 undef
Geo::Coordinates::Converter::Datum::Grs80 undef
Geo::Coordinates::Converter::Datum::Jgd2000 undef
Geo::Coordinates::Converter::Datum::Tokyo undef
Geo::Coordinates::Converter::Datum::Wgs72 undef
Geo::Coordinates::Converter::Datum::Wgs84 undef
Geo::Coordinates::Converter::Format undef
Geo::Coordinates::Converter::Format::Degree undef
Geo::Coordinates::Converter::Format::Dms undef
Geo::Coordinates::Converter::Format::IArea 0.01
Geo::Coordinates::Converter::Point undef
Geo::Coordinates::Converter::Point::Geohash undef
Geo::GoogleEarth::Pluggable::Base 0.12
Geo::GoogleEarth::Pluggable::Contrib::LineString 0.09
Geo::GoogleEarth::Pluggable::Contrib::LinearRing 0.09
Geo::GoogleEarth::Pluggable::Contrib::Point 0.13
Geo::GoogleEarth::Pluggable::Folder 0.10
Geo::GoogleEarth::Pluggable::NetworkLink 0.09
Geo::GoogleEarth::Pluggable::Placemark 0.12
Geo::GoogleEarth::Pluggable::Plugin::Default 0.13
Geo::GoogleEarth::Pluggable::Plugin::Others 0.09
Geo::GoogleEarth::Pluggable::Plugin::Style 0.09
Geo::GoogleEarth::Pluggable::Style 0.09
Geo::GoogleEarth::Pluggable::StyleBase 0.09
Geo::GoogleEarth::Pluggable::StyleMap 0.09
Geo::KML::Util 0.92
Geo::Parse::OSM::Multipass 0.40
Geo::Parse::OSM::Singlepass 0.40
Geo::Postcode::Location 0.12
Geo::Postcodes::Update 0.311
Geo::ShapeFile::Point 2.52
Geo::ShapeFile::Shape 2.52
Geo::TigerLine::Record 0.01
Geo::TigerLine::Record::1 0.03
Geo::TigerLine::Record::2 0.03
Geo::TigerLine::Record::4 0.03
Geo::TigerLine::Record::5 0.03
Geo::TigerLine::Record::6 0.03
Geo::TigerLine::Record::7 0.03
Geo::TigerLine::Record::8 0.03
Geo::TigerLine::Record::A 0.03
Geo::TigerLine::Record::Accessor 0.03
Geo::TigerLine::Record::B 0.03
Geo::TigerLine::Record::C 0.03
Geo::TigerLine::Record::E 0.03
Geo::TigerLine::Record::H 0.03
Geo::TigerLine::Record::I 0.03
Geo::TigerLine::Record::M 0.03
Geo::TigerLine::Record::P 0.03
Geo::TigerLine::Record::Parser 0.01
Geo::TigerLine::Record::R 0.03
Geo::TigerLine::Record::S 0.03
Geo::TigerLine::Record::T 0.03
Geo::TigerLine::Record::U 0.03
Geo::TigerLine::Record::Z 0.03
Geography::JapanesePrefectures::Unicode undef
Geography::NationalGrid::GB 1.3
Geography::NationalGrid::IE 1.2
Geometry::Primitive::Bezier undef
Geometry::Primitive::Dimension undef
Geometry::Primitive::Ellipse undef
Geometry::Primitive::Equal undef
Geometry::Primitive::Line undef
Geometry::Primitive::Polygon undef
Geometry::Primitive::Shape undef
Getopt::Complete::Args undef
Getopt::Complete::Cache undef
Getopt::Complete::Compgen undef
Getopt::Complete::LazyOptions undef
Getopt::Complete::Options undef
Getopt::Euclid::HierDemo undef
Getopt::Flex::Config 1.07
Getopt::Flex::Spec 1.07
Getopt::Flex::Spec::Argument 1.07
Getopt::Long::Descriptive::Opts 0.090
Getopt::Long::Descriptive::Usage 0.090
Getopt::Lucid::Exception 0.19
Getopt::Std 1.06
GitMeta::GMF undef
GitMeta::Github undef
GitMeta::SshDir undef
Gitalist::ActionRole::FilenameArgs undef
Gitalist::ContentMangler::Resolver undef
Gitalist::ContentMangler::Resolver::Default undef
Gitalist::ContentMangler::Transformer::SyntaxHighlight undef
Gitalist::Controller undef
Gitalist::Controller::Fragment undef
Gitalist::Controller::Fragment::Ref undef
Gitalist::Controller::Fragment::Repository undef
Gitalist::Controller::LegacyURI undef
Gitalist::Controller::OPML undef
Gitalist::Controller::Ref undef
Gitalist::Controller::Repository undef
Gitalist::Controller::Root undef
Gitalist::Git::CollectionOfRepositories undef
Gitalist::Git::CollectionOfRepositories::FromDirectory undef
Gitalist::Git::CollectionOfRepositories::FromDirectory::WhiteList undef
Gitalist::Git::CollectionOfRepositories::FromDirectoryRecursive undef
Gitalist::Git::CollectionOfRepositories::FromListOfDirectories undef
Glib::Ex::EnumBits 12
Glib::Ex::SignalBits 12
Glib::Ex::SourceIds 12
Glib::MakeHelper 0.03
Gitalist::Git::Types undef
Gitalist::Model::CollectionOfRepos undef
Gitalist::Model::ContentMangler undef
Gitalist::URIStructure::Fragment::WithLog undef
Gitalist::URIStructure::Ref undef
Gitalist::URIStructure::Repository undef
Gitalist::URIStructure::WithLog undef
Gitalist::Utils undef
Gitalist::View::Default undef
Gitalist::View::SyntaxHighlight undef
Glib::CodeGen 0.03
Glib::Ex::ConnectProperties::Element 14
Glib::Ex::ConnectProperties::Element::child 14
Glib::Ex::ConnectProperties::Element::model_rows 14
Glib::Ex::ConnectProperties::Element::object 14
Glib::Ex::ConnectProperties::Element::response_sensitive 14
Glib::Ex::ConnectProperties::Element::widget 14
Glib::Ex::ConnectProperties::Element::widget_allocation 14
Glib::Ex::TieProperties 12
Glib::GenPod 0.03
Glib::ParseXSDoc 1.003
Global::Context::AuthToken 0.001
Global::Context::AuthToken::Basic 0.001
Global::Context::Env 0.001
Global::Context::Env::Basic 0.001
Global::Context::Stack 0.001
Global::Context::Stack::Basic 0.001
Global::Context::StackFrame 0.001
Global::Context::StackFrame::Basic 0.001
Global::Context::Terminal 0.001
Global::Context::Terminal::Basic 0.001
Gloom::Doc undef
GnuPG::Fingerprint undef
GnuPG::Handles undef
GnuPG::HashInit undef
GnuPG::Key undef
GnuPG::Options undef
GnuPG::PrimaryKey undef
GnuPG::PublicKey undef
GnuPG::Revoker undef
GnuPG::SecretKey undef
GnuPG::Signature undef
GnuPG::SubKey undef
GnuPG::Tie undef
GnuPG::Tie::ClearSign undef
GnuPG::Tie::Decrypt undef
GnuPG::Tie::Encrypt undef
GnuPG::Tie::Sign undef
GnuPG::UserAttribute undef
GnuPG::UserId undef
Google::AJAX::Library::Carp undef
Google::Chart::Axis undef
Google::Chart::Axis::Item undef
Google::Chart::Axis::Style undef
Google::Chart::Data::Extended undef
Google::Chart::Data::Simple undef
Google::Chart::Data::Text undef
Google::Chart::Fill undef
Google::Chart::Fill::LinearGradient undef
Google::Chart::Fill::LinearStripes undef
Google::Chart::Fill::Solid undef
Google::Chart::Grid undef
Google::Chart::Legend undef
Google::Chart::Margin undef
Google::Chart::Marker undef
Google::Chart::QueryComponent undef
Google::Chart::QueryComponent::Simple undef
Google::Chart::Title undef
Google::Chart::Type::QRcode undef
Google::Chart::Type::Radar undef
Google::Chart::Type::Simple undef
Google::Chart::Type::SparkLine undef
Google::Chart::Type::XY undef
Google::Chart::Types undef
Google::ProtocolBuffers::CodeGen undef
Gtk2::CodeGen 0.03
Gtk2::Ex::ActionTooltips 37
Gtk2::Ex::ComboBox::Enum 29
Google::ProtocolBuffers::Codec undef
Gtk2::Ex::ComboBox::PixbufType 29
Gtk2::Ex::ComboBox::Text 29
Gtk2::Ex::CrossHair 20
Gtk2::Ex::ContainerBits 37
Google::ProtocolBuffers::Compiler undef
Gtk2::Ex::EntryBits 37
Google::ProtocolBuffers::Constants undef
Gtk2::Ex::GdkBits 37
Google::Search::Carp undef
Google::Search::Error undef
Google::Search::Page undef
Google::Search::Response undef
Google::Search::Result undef
Gtk2::Ex::KeySnooper 37
Google::Voice::Call undef
Gtk2::Ex::Menu::EnumRadio 29
Gtk2::Ex::MenuBits 37
Gtk2::Ex::MenuItem::Subclass 37
Gtk2::Ex::Lasso 20
Google::Voice::Feed undef
Gtk2::Ex::PixbufBits 37
Google::Voice::SMS::Message undef
Grades::Types 0.12
Gtk2::Ex::Statusbar::DynamicContext 37
Gtk2::Ex::SyncCall 37
Gtk2::Ex::Statusbar::MessageUntilKey 37
Gtk2::Ex::TextBufferBits 37
Graph::AdjacencyMap undef
Graph::AdjacencyMap::Heavy undef
Gtk2::Ex::ToolItem::ComboEnum 29
Gtk2::Ex::ToolItem::OverflowToDialog 37
Graph::AdjacencyMap::Light undef
Gtk2::Ex::TreeModel::ImplBits 37
Gtk2::Ex::ToolbarBits 37
Gtk2::Ex::TreeModelFilter::Change 37
Gtk2::Ex::TreeModelBits 37
Gtk2::Ex::TreeModelFilter::Draggable 3
Gtk2::Ex::Units 37
Gtk2::Ex::WidgetEvents 37
Gtk2::Ex::TreeViewBits 37
Graph::AdjacencyMap::Vertex undef
Graph::AdjacencyMatrix undef
Graph::Attribute undef
Graph::BitMatrix undef
Graph::Easy::As_ascii 0.22
Graph::Easy::As_graphml 0.03
Graph::Easy::As_graphviz 0.31
Graph::Easy::As_txt 0.15
Graph::Easy::As_vcg 0.05
Graph::Easy::Attributes 0.32
Graph::Easy::Base 0.12
Graph::Easy::Edge 0.31
Graph::Easy::Edge::Cell 0.29
Graph::Easy::Group 0.22
Graph::Easy::Group::Anon 0.02
Graph::Easy::Group::Cell 0.14
Graph::Easy::Layout 0.29
Graph::Easy::Layout::Chain 0.09
Graph::Easy::Layout::Force 0.01
Graph::Easy::Layout::Grid 0.07
Graph::Easy::Layout::Path 0.16
Graph::Easy::Layout::Repair 0.08
Graph::Easy::Layout::Scout 0.25
Graph::Easy::Marpa::Parser 0.51
Graph::Easy::Marpa::Test 0.51
Graph::Easy::Node 0.38
Graph::Easy::Node::Anon 0.11
Graph::Easy::Node::Cell 0.10
Graph::Easy::Node::Empty 0.06
Graph::Easy::Parser 0.35
Graph::Easy::Parser::Graphviz 0.17
Graph::Easy::Parser::VCG 0.06
Graph::Fast::GraphPM undef
Graph::MSTHeapElem 0.01
Graph::Matrix undef
Graph::SPTHeapElem 0.01
Graph::TransitiveClosure undef
Graph::TransitiveClosure::Matrix undef
Graph::Traversal undef
Graph::Traversal::BFS undef
Graph::Traversal::DFS undef
Graph::Undirected undef
Graph::UnionFind undef
GraphViz::Data::Grapher 0.01
GraphViz::No 0.01
GraphViz::Parse::RecDescent 0.01
GraphViz::Parse::Yacc 0.01
GraphViz::Parse::Yapp 0.01
GraphViz::Regex 0.02
GraphViz::Small 0.01
GraphViz::XML 0.01
Graphics::Color::CMYK undef
Graphics::Color::Equal undef
Graphics::Color::HSL undef
Graphics::Color::HSV undef
Graphics::Color::Types undef
Graphics::Color::YIQ undef
Graphics::Color::YUV undef
Graphics::ColorNames::CSS 1.13
Graphics::ColorNames::IE 1.13
Graphics::ColorNames::Netscape 2.11
Graphics::ColorNames::SVG 1.13
Graphics::ColorNames::Windows 2.11
Graphics::ColorNames::X 2.11
Growl::Any::Base undef
Growl::Any::CocoaGrowl undef
Growl::Any::DesktopNotify undef
Growl::Any::GrowlGNTP undef
Growl::Any::GrowlNotifySend undef
Growl::Any::IOHandle undef
Growl::Any::MacGrowl undef
Growl::Any::NetGrowl undef
Growl::Any::NetGrowlClient undef
Growl::Any::Null undef
Growl::Any::Win32MSAgent undef
Gtk2::Ex::CellLayout::BuildAttributes 5
Gtk2::Ex::DateSpinner::PopupForEntry 8
Gtk2::Ex::ErrorTextDialog::SaveDialog 9
Gtk2::Ex::FreezeChildNotify 37
Gtk2::Ex::History::Button 7
Gtk2::Ex::History::Dialog 7
Gtk2::Ex::History::ListStore 7
Gtk2::Ex::History::Menu 7
Gtk2::Ex::History::MenuToolButton 7
Gtk2::Ex::Menu::EnumRadio::Item 29
Gtk2::Ex::MenuView::Menu 4
Gtk2::Ex::PodViewer::Parser undef
Gtk2::Ex::TiedMenuChildren 5
Gtk2::Ex::TiedTreePath 5
Gtk2::Ex::ToolItem::OverflowToDialog::Dialog 37
Gtk2::Helper 0.02
Gtk2::SimpleList 0.15
Gtk2::SimpleMenu 0.5
Gtk2::TestHelper 0.02
HTML::Acid 0.0.3
HTML::Acid::Buffer 0.0.3
HTML::AsSubs 4.2
HTML::Barcode::1D undef
HTML::Barcode::2D undef
Barcode::Code128
HTML::Barcode::Code128 undef
HTML::Barcode::Code93 undef
Text::QRCode
HTML::Barcode::QRCode undef
HTML::CalendarMonth::DateTool 1.25
HTML::CalendarMonth::DateTool::Cal 1.25
HTML::CalendarMonth::DateTool::DateCalc 1.25
HTML::CalendarMonth::DateTool::DateManip 1.25
HTML::CalendarMonth::DateTool::DateTime 1.25
HTML::CalendarMonth::DateTool::Ncal 1.25
HTML::CalendarMonth::DateTool::TimeLocal 1.25
HTML::CalendarMonth::Locale 1.25
HTML::DOM::Attr 0.047
HTML::DOM::CharacterData 0.047
HTML::DOM::Comment 0.047
HTML::DOM::DocumentFragment 0.047
HTML::DOM::Element 0.047
HTML::DOM::Element::Table 0.047
HTML::DOM::Event::Mouse 0.047
HTML::DOM::Event::Mutation 0.047
HTML::DOM::Event::UI 0.047
HTML::DOM::Implementation 0.047
HTML::DOM::NamedNodeMap 0.047
HTML::DOM::Node 0.047
HTML::DOM::NodeList 0.047
HTML::DOM::Text 0.047
HTML::Display::Common 0.39
HTML::Display::Debian 0.39
HTML::Display::Dump 0.39
HTML::Display::Galeon 0.39
HTML::Display::MozRepl 0.50
HTML::Display::Mozilla 0.39
HTML::Display::OSX 0.39
HTML::Display::Opera 0.39
HTML::Display::Phoenix 0.39
HTML::Display::TempFile 0.39
HTML::Display::Win32 0.39
HTML::Display::Win32::IE 0.39
HTML::Display::Win32::OLE 0.39
HTML::EasyForm::Trait::Field 0.001
HTML::EditableTable::Horizontal 0.21
HTML::EditableTable::Javascript undef
HTML::EditableTable::Vertical 0.21
HTML::Element::traverse 4.2
HTML::ElementGlob 1.18
HTML::Entities 3.68
HTML::ElementRaw 1.18
HTML::ElementSuper 1.18
HTML::Entities::Numbered::Table 0.01
HTML::ExtractContent::Util undef
HTML::Feature::Base undef
HTML::Feature::Decoder undef
HTML::Feature::Engine undef
HTML::Feature::Engine::GoogleADSection undef
HTML::Feature::Engine::LDRFullFeed undef
HTML::Feature::Engine::TagStructure undef
HTML::Feature::Fetcher undef
HTML::Feature::FrontParser undef
HTML::Feature::Result undef
HTML::FillInForm::Lite::Compat 1.09
HTML::Filter 3.57
HTML::Filter::Callbacks::Tag undef
PONAPI::Tools
PONAPI::Server
Template::Plugin::Number::Format
Template::Plugin::LinkTarget
Template::Plugin::NoFollow
Template::Plugin::Trac
Template::Plugin::PerlTidy
Template::Pure
Mail::RFC822::Address
Mojolicious::Plugin::Vparam
Mojolicious::Plugin::InputValidation
Geo::IP2Location
TheSchwartz::Error undef
TheSchwartz::ExitStatus undef
TheSchwartz::FuncMap undef
TheSchwartz::Job undef
TheSchwartz::JobHandle undef
TheSchwartz::Worker undef
Thread 3.02
Thrift::XS::CompactProtocol undef
Throwable::Error 0.102080
Thrift::XS::MemoryBuffer undef
Throwable::X::AutoPayload 0.004
Throwable::X::Meta::Attribute::Payload 0.004
Throwable::X::Types 0.006
Throwable::X::WithIdent 0.004
Throwable::X::WithMessage 0.004
Throwable::X::WithMessage::Errf 0.004
Throwable::X::WithTags 0.004
TiddlyWeb::EditPage 0.04
TiddlyWeb::Resting 0.1
TiddlyWeb::Resting::DefaultRester 0.02
TiddlyWeb::Resting::Getopt 0.01
TiddlyWeb::Wikrad::Listbox undef
TiddlyWeb::Wikrad::PageViewer undef
TiddlyWeb::Wikrad::Window undef
Tie::Array 1.03
Tie::Array::DBD 0.07
Tie::Array::Iterable::BackwardIterator 0.03
Tie::Array::Iterable::ForwardIterator 0.03
Tie::Cache::LRU::Array undef
Tie::Cache::LRU::LinkedList undef
Tie::Cache::LRU::Virtual undef
Tie::FieldVals::Join 0.6202
Tie::FieldVals::Row 0.6202
Tie::FieldVals::Row::Join 0.6202
Tie::FieldVals::Select 0.6202
Tie::Hash 1.03
Tie::Handle 4.2
Tie::Handle::CSV::Array undef
Tie::Handle::CSV::Hash undef
Tie::Hash::NamedCapture 0.06
Tie::Memoize 1.1
Tie::Redis::Hash 0.21
Tie::Redis::List 0.21
Tie::Redis::Scalar 0.21
Tie::Scalar 1.02
Tie::Simple::Array 1.01
Tie::Simple::Handle 1.01
Tie::Simple::Hash 1.03
Tie::Simple::Scalar undef
Tie::Simple::Util 1.01
Tie::StdHandle 4.2
Tie::SubstrHash 1.00
Tiffany::HTML::Template undef
Tiffany::HTML::Template::Pro undef
Tiffany::TT undef
Tiffany::Template::Declare undef
Tiffany::Template::Semantic undef
Tiffany::Text::ClearSilver undef
Tiffany::Text::MicroMason undef
Tiffany::Text::MicroTemplate::Extended undef
Tiffany::Text::MicroTemplate::File undef
Tiffany::Text::Xslate undef
Tiffany::Util undef
Time::CTime 99.062201
Time::DaysInMonth 99.1117
Time::Duration::Filter 7
Time::Duration::LocaleObject 7
Time::Duration::en_PIGLATIN 7
Time::Elapsed::Lang::DE 0.31
Time::Elapsed::Lang::EN 0.31
Time::Elapsed::Lang::TR 0.31
Time::Fields 1.2.565EHOV
Time::Frame 1.2.565EHOV
Time::JulianDay 2003.1125
Time::Seconds undef
Time::TAI::Realisation_TAI 0.003
Time::TCG::Realisation 0.001
Time::TZ 9
Time::Timezone 2006.0814
Time::UTC::Segment 0.007
Time::gmtime 1.03
Time::localtime 1.02
Time::tm 1.00
Time::y2038::Everywhere 20100403
Tk::After 4.008
Tk::Animation 4.008
Tk::Bitmap 4.004
Tk::Button 4.010
Tk::Chart::Areas 1.03
Tk::Chart::Bars 1.03
Tk::Chart::Boxplots 1.03
Tk::Chart::Lines 1.04
Tk::Chart::Mixed 1.02
Tk::Chart::Pie 1.03
Tk::Chart::Utils 1.02
Tk::Checkbutton 4.006
Tk::Clipboard 4.009
Tk::Cloth 2.03
Tk::CmdLine 4.007
Tk::ColorEditor 4.012
Tk::Configure 4.009
Tk::DirTree 4.021
Tk::Dirlist 4.004
Tk::DragDrop 4.015
Tk::DragDrop::Common 4.005
Tk::DragDrop::Rect 4.012
Tk::DragDrop::SunConst 4.004
Tk::DragDrop::SunDrop 4.006
Tk::DragDrop::SunSite 4.007
Tk::DragDrop::XDNDDrop 4.007
Tk::DragDrop::XDNDSite 4.007
Tk::DropSite 4.008
Tk::DummyEncode 4.007
Tk::English 4.006
Tk::Entry 4.018
Tk::ErrorDialog 4.008
Tk::Event 4.024
Tk::Event::IO 4.009
Tk::FBox 4.021
Tk::FileSelect 4.018
Tk::FloatEntry 4.004
Tk::Font 4.004
Tk::ForDummies::Graph::Areas 1.10
Tk::ForDummies::Graph::Bars 1.10
Tk::ForDummies::Graph::Boxplots 1.06
Tk::ForDummies::Graph::Lines 1.11
Tk::ForDummies::Graph::Mixed 1.03
Tk::ForDummies::Graph::Pie 1.08
Tk::ForDummies::Graph::Utils 1.08
Tk::HList 4.015
Tk::IO 4.005
Tk::IconList 4.007
Tk::Image 4.011
Tk::InputO 4.004
Tk::ItemStyle 4.004
Tk::LabEntry 4.006
Tk::LabFrame 4.010
Tk::Label 4.006
Tk::Labelframe 4.003
Tk::Listbox 4.015
Tk::MMtry 4.011
Tk::MainWindow 4.014
Tk::MakeDepend 4.014
Tk::Menu 4.023
Tk::Menu::Item 4.005
Tk::Menubar 4.006
Tk::Message 4.006
Tk::More 5.03
Tk::MsgBox 4.002
Tk::Mwm 4.004
Tk::NBFrame 4.004
Tk::Optionmenu 4.014
Tk::Panedwindow 4.004
Tk::Pixmap 4.004
Tk::Pod::Cache 5.01
Tk::Pod::FindPods 5.11
Tk::Pod::Search 5.10
Tk::Pod::Search_db 5.06
Tk::Pod::SimpleBridge 5.03
Tk::Pod::Styles 5.04
Tk::Pod::Tree 5.04
Tk::Pod::Util 5.03
Tk::Pretty 4.006
Tk::ProgressBar 4.014
Tk::ROTextANSIColor 0.01
Tk::Radiobutton 4.006
Tk::Region 4.006
Tk::Reindex 4.006
Tk::ReindexedROText 4.004
Tk::ReindexedText 4.004
Tk::Scale 4.004
Tk::Scrollbar 4.010
Tk::Spinbox 4.007
Tk::Stats 4.004
Tk::Submethods 4.005
Tk::TFrame 2.02
Tk::TList 4.006
Tk::Table 4.015
Tk::TableMatrix::Spreadsheet 1.23
Tk::TableMatrix::SpreadsheetHideRows 1.23
Tk::Text 4.028
Tk::Text::Tag 4.004
Tk::TextEdit 4.004
Tk::TextList 4.006
Tk::TextUndo 4.015
Tk::Tiler 4.013
Tk::TixGrid 4.010
Tk::Trace 4.009
Tk::WinPhoto 4.005
Tk::Wm 4.015
Tk::X 4.005
Tk::X11Font 4.007
Tk::Xlib 4.004
Tk::Xrm 4.005
Tk::install 4.004
Tk::widgets 4.005
TomTom::WEBFLEET::Connect::Response undef
Tool::Bench::Item 0.002
Tool::Bench::Report::JSON 0.002
Tool::Bench::Report::Text 0.002
Transmission::AttributeRole undef
Transmission::Session undef
Transmission::Stats undef
Transmission::Torrent undef
Transmission::Torrent::File undef
Transmission::Types undef
Transmission::Utils undef
Tree::Binary 1.01
Tree::DAG_Node::Persist::Create 1.05
Tree::Fast 1.01
Tree::File::YAML 0.111
Tree::Predicate::Leaf 0.03
Tree::Simple::View::ASCII 0.02
Tree::Simple::View::DHTML 0.13
Tree::Simple::View::HTML 0.12
Tree::Simple::View::Exceptions 0.01
Tree::Simple::Visitor::BreadthFirstTraversal 0.02
Tree::Simple::Visitor::CreateDirectoryTree 0.02
Tree::Simple::Visitor::FindByNodeValue 0.02
Tree::Simple::Visitor::FromNestedArray 0.02
Tree::Simple::Visitor::FromNestedHash 0.03
Tree::Simple::Visitor::GetAllDescendents 0.02
Tree::Simple::Visitor::LoadClassHierarchy 0.02
Tree::Simple::Visitor::LoadDirectoryTree 0.02
Tree::Simple::Visitor::PathToRoot 0.02
Tree::Simple::Visitor::PostOrderTraversal 0.02
Tree::Simple::Visitor::PreOrderTraversal 0.01
Tree::Simple::Visitor::Sort 0.03
Tree::Simple::Visitor::ToNestedArray 0.02
Tree::Simple::Visitor::ToNestedHash 0.02
Tree::Simple::Visitor::VariableDepthClone 0.03
Tree::Template::Declare::DAG_Node 0.4
Tree::Template::Declare::HTML_Element 0.4
Tree::Template::Declare::LibXML 0.4
Tree::Transform::XSLTish::Context 0.3
Tree::Transform::XSLTish::Transformer 0.3
Tree::Transform::XSLTish::Utils 0.3
Tree::XPathEngine::Boolean undef
Tree::XPathEngine::Expr undef
Tree::XPathEngine::Function undef
Tree::XPathEngine::Literal undef
Tree::XPathEngine::LocationPath undef
Tree::XPathEngine::NodeSet undef
Tree::XPathEngine::Number undef
Tree::XPathEngine::Root undef
Tree::XPathEngine::Step undef
Tree::XPathEngine::Variable undef
Sereal::Path
Treex::Block::Read::BaseReader 0.05222
Treex::Block::Read::BaseTextReader 0.05222
Treex::Block::Read::Sentences 0.05222
Treex::Block::Read::Text 0.05222
Treex::Block::Read::Treex 0.05222
Treex::Block::Util::DefinedAttr 0.05222
Treex::Block::Util::Eval 0.05222
Treex::Block::Util::SetGlobal 0.05222
Treex::Block::Write::Sentences 0.05222
Treex::Block::Write::Text 0.05222
Treex::Block::Write::Treex 0.05222
Treex::Core::Block 0.05222
Treex::Core::Bundle 0.05222
Treex::Core::BundleZone 0.05222
Treex::Core::Common 0.05222
Treex::Core::Config 0.05222
Treex::Core::DocZone 0.05222
Treex::Core::Document 0.05222
Treex::Core::DocumentReader 0.05222
Treex::Core::Log 0.05222
Treex::Core::Node 0.05222
Treex::Core::Node::A 0.05222
Treex::Core::Node::EffectiveRelations 0.05222
Treex::Core::Node::InClause 0.05222
Treex::Core::Node::N 0.05222
Treex::Core::Node::Ordered 0.05222
Treex::Core::Node::P 0.05222
Treex::Core::Node::T 0.05222
Treex::Core::Resource 0.05222
Treex::Core::Run 0.05222
Treex::Core::Scenario 0.05222
Treex::Core::TredView 0.05222
Treex::Core::Zone 0.05222
Treex::PML::Alt 2.06
Treex::PML::Backend::CSTS 2.06
Treex::PML::Backend::CSTS::Csts2fs 2.06
Treex::PML::Backend::CSTS::Fs2csts 2.06
Treex::PML::Backend::FS 2.06
Treex::PML::Backend::NTRED 2.06
Treex::PML::Backend::PML 2.06
Treex::PML::Backend::PMLTransform 2.06
Treex::PML::Backend::Storable 2.06
Treex::PML::Backend::TEIXML 2.06
Treex::PML::Factory 2.06
Treex::PML::Backend::TrXML 2.06
Treex::PML::IO 2.06
Treex::PML::Container 2.06
Treex::PML::Document 2.06
Treex::PML::FSFormat 2.06
Treex::PML::Instance 2.06
Treex::PML::Node 2.06
Treex::PML::Instance::Common 2.06
Treex::PML::Instance::Reader 2.06
Treex::PML::Instance::Writer 2.06
Treex::PML::List 2.06
Treex::PML::Schema 2.06
Treex::PML::Schema::Alt 2.06
Treex::PML::Schema::Attribute 2.06
Treex::PML::Schema::CDATA 2.06
Treex::PML::Schema::Choice 2.06
Treex::PML::Schema::Constant 2.06
Treex::PML::Schema::Constants 2.06
Treex::PML::Schema::Container 2.06
Treex::PML::Schema::Copy 2.06
Treex::PML::Schema::Decl 2.06
Treex::PML::Schema::Derive 2.06
Treex::PML::Schema::Element 2.06
Treex::PML::Schema::Import 2.06
Treex::PML::Schema::List 2.06
Treex::PML::Schema::Member 2.06
Treex::PML::Schema::Reader 2.06
Treex::PML::Schema::Root 2.06
Treex::PML::Schema::Seq 2.06
Treex::PML::Schema::Struct 2.06
Treex::PML::Schema::Template 2.06
Treex::PML::Schema::Type 2.06
Treex::PML::Schema::XMLNode 2.06
Treex::PML::Struct 2.06
Treex::PML::Seq 2.06
Treex::PML::Seq::Element 2.06
Treex::PML::StandardFactory 2.06
Twiggy::Server undef
Twiggy::Server::SS undef
UDDI::Lite 0.712
UNIVERSAL 1.07
UNIVERSAL::Object::ID v0.1.1
UNIVERSAL::Object::Method undef
UR::All 0.30
UR::BoolExpr 0.30
UR::BoolExpr::Template 0.30
UR::BoolExpr::Template::And 0.30
UR::BoolExpr::Template::Composite 0.30
UR::BoolExpr::Template::Or 0.30
UR::BoolExpr::Template::PropertyComparison 0.30
UR::BoolExpr::Template::PropertyComparison::Between 0.30
UR::BoolExpr::Template::PropertyComparison::Equals 0.30
UR::BoolExpr::Template::PropertyComparison::False 0.30
UR::BoolExpr::Template::PropertyComparison::GreaterOrEqual 0.30
UR::BoolExpr::Template::PropertyComparison::GreaterThan 0.30
UR::BoolExpr::Template::PropertyComparison::In 0.30
UR::BoolExpr::Template::PropertyComparison::LessOrEqual 0.30
UR::BoolExpr::Template::PropertyComparison::LessThan 0.30
UR::BoolExpr::Template::PropertyComparison::Like 0.30
UR::BoolExpr::Template::PropertyComparison::Matches 0.30
UR::BoolExpr::Template::PropertyComparison::NotIn 0.30
UR::BoolExpr::Template::PropertyComparison::NotLike 0.30
UR::BoolExpr::Template::PropertyComparison::True 0.30
UR::BoolExpr::Util 0.30
UR::Change 0.30
UR::Context undef
UR::Context::DefaultRoot 0.30
UR::Context::ObjectFabricator 0.30
UR::Context::Process 0.30
UR::Context::Root 0.30
UR::Context::Transaction 0.30
UR::DBI 0.30
UR::DataSource 0.30
UR::DataSource::CSV 0.30
UR::DataSource::Code 0.30
UR::DataSource::Default 0.30
UR::DataSource::File 0.30
UR::DataSource::FileMux 0.30
UR::DataSource::Meta 0.30
UR::DataSource::MySQL 0.30
UR::DataSource::Oracle 0.30
UR::DataSource::Pg 0.30
UR::DataSource::RDBMS 0.30
UR::DataSource::RDBMS::BitmapIndex 0.30
UR::DataSource::RDBMS::Entity 0.30
UR::DataSource::RDBMS::FkConstraint 0.30
UR::DataSource::RDBMS::FkConstraintColumn 0.30
UR::DataSource::RDBMS::PkConstraintColumn 0.30
UR::DataSource::RDBMS::Table 0.30
UR::DataSource::RDBMS::Table::View::Default::Text 0.30
UR::DataSource::RDBMS::TableColumn 0.30
UR::DataSource::RDBMS::TableColumn::View::Default::Text 0.30
UR::DataSource::RDBMS::UniqueConstraintColumn 0.30
UR::DataSource::RemoteCache 0.30
UR::DataSource::SQLite undef
UR::DataSource::ValueDomain 0.30
UR::Debug 0.30
UR::DeletedRef 0.30
UR::Env::UR_COMMAND_DUMP_STATUS_MESSAGES 0.30
UR::Env::UR_CONTEXT_BASE 0.30
UR::Env::UR_CONTEXT_CACHE_SIZE_HIGHWATER 0.30
UR::Env::UR_CONTEXT_CACHE_SIZE_LOWWATER 0.30
UR::Env::UR_CONTEXT_MONITOR_QUERY 0.30
UR::Env::UR_CONTEXT_ROOT 0.30
UR::Env::UR_DBI_DUMP_STACK_ON_CONNECT 0.30
UR::Env::UR_DBI_EXPLAIN_SQL_CALLSTACK 0.30
UR::Env::UR_DBI_EXPLAIN_SQL_IF 0.30
UR::Env::UR_DBI_EXPLAIN_SQL_MATCH 0.30
UR::Env::UR_DBI_EXPLAIN_SQL_SLOW 0.30
UR::Env::UR_DBI_MONITOR_DML 0.30
UR::Env::UR_DBI_MONITOR_EVERY_FETCH 0.30
UR::Env::UR_DBI_MONITOR_SQL 0.30
UR::Env::UR_DBI_NO_COMMIT 0.30
UR::Env::UR_DEBUG_OBJECT_PRUNING 0.30
UR::Env::UR_DEBUG_OBJECT_RELEASE 0.30
UR::Env::UR_IGNORE 0.30
UR::Env::UR_NR_CPU 0.30
UR::Env::UR_STACK_DUMP_ON_DIE 0.30
UR::Env::UR_STACK_DUMP_ON_WARN 0.30
UR::Env::UR_TEST_QUIET 0.30
UR::Env::UR_USED_MODS 0.30
UR::Env::UR_USE_ANY 0.30
UR::Env::UR_USE_DUMMY_AUTOGENERATED_IDS 0.30
UR::Exit 0.30
UR::ModuleBase 0.30
UR::ModuleConfig 0.30
UR::ModuleLoader 0.30
UR::Namespace 0.30
UR::Namespace::Command 0.30
UR::Namespace::Command::Base 0.30
UR::Namespace::Command::Browser undef
UR::Namespace::Command::CreateCompletionSpecFile undef
UR::Namespace::Command::Define 0.30
UR::Namespace::Command::Define::Class 0.30
UR::Namespace::Command::Define::Datasource 0.30
UR::Namespace::Command::Define::Datasource::File 0.30
UR::Namespace::Command::Define::Datasource::Mysql 0.30
UR::Namespace::Command::Define::Datasource::Oracle 0.30
UR::Namespace::Command::Define::Datasource::Pg 0.30
UR::Namespace::Command::Define::Datasource::Rdbms 0.30
UR::Namespace::Command::Define::Datasource::RdbmsWithAuth 0.30
UR::Namespace::Command::Define::Datasource::Sqlite 0.30
UR::Namespace::Command::Define::Db 0.30
UR::Namespace::Command::Define::Namespace 0.30
UR::Namespace::Command::Diff undef
UR::Namespace::Command::Diff::Rewrite undef
UR::Namespace::Command::Diff::Update undef
UR::Namespace::Command::Export undef
UR::Namespace::Command::Export::DbicClasses undef
UR::Namespace::Command::Info undef
UR::Namespace::Command::Init 0.30
UR::Namespace::Command::List 0.30
UR::Namespace::Command::List::Classes 0.30
UR::Namespace::Command::List::Modules 0.30
UR::Namespace::Command::List::Objects 0.30
UR::Namespace::Command::Old 0.30
UR::Namespace::Command::Old::DiffRewrite 0.30
UR::Namespace::Command::Old::DiffUpdate 0.30
UR::Namespace::Command::Old::ExportDbicClasses 0.30
UR::Namespace::Command::Old::Info 0.30
UR::Namespace::Command::Old::Redescribe 0.30
UR::Namespace::Command::Redescribe undef
UR::Namespace::Command::Rename undef
UR::Namespace::Command::Rename::Class undef
UR::Namespace::Command::Rewrite undef
UR::Namespace::Command::RunsOnModulesInTree 0.30
UR::Namespace::Command::Sys 0.30
UR::Namespace::Command::Sys::ClassBrowser 0.30
UR::Namespace::Command::Update 0.30
UR::Namespace::Command::Update::ClassDiagram 0.30
UR::Namespace::Command::Update::Classes undef
UR::Namespace::Command::Update::ClassesFromDb 0.30
UR::Namespace::Command::Update::Pod 0.30
UR::Namespace::Command::Update::RenameClass 0.30
UR::Namespace::Command::Update::RewriteClassHeader 0.30
UR::Namespace::Command::Update::SchemaDiagram 0.30
UR::Namespace::Command::Update::TabCompletionSpec 0.30
UR::Object 0.30
UR::Object::Accessorized 0.30
UR::Object::Command::FetchAndDo 0.30
UR::Object::Command::List 0.30
UR::Object::Command::List::Style 0.30
UR::Object::Ghost 0.30
UR::Object::Index 0.30
UR::Object::Inheritance undef
UR::Object::Iterator 0.30
UR::Object::Property 0.30
UR::Object::Property::ID undef
UR::Object::Property::Unique undef
UR::Object::Property::View::Default::Text 0.30
UR::Object::Property::View::DescriptionLineItem::Text 0.30
UR::Object::Property::View::ReferenceDescription::Text 0.30
UR::Object::Reference undef
UR::Object::Reference::Property undef
UR::Object::Reference::View::DescriptionLineItem::Text undef
UR::Object::Set 0.30
UR::Object::Set::View::Default::Json 0.30
UR::Object::Set::View::Default::Xml 0.30
UR::Object::Tag 0.30
UR::Object::Type 0.30
UR::Object::Type::DBICModuleWriter undef
Mojo::UserAgent::Mockable
Cache::Elasticache::Memcache
UR::Object::Type::View::Default::Text 0.30
UR::Object::Type::View::Default::Xml 0.30
UR::Object::UserOperation undef
UR::Object::Value 0.30
UR::Object::View 0.30
UR::Object::View::Aspect 0.30
UR::Object::View::Default::Gtk 0.30
UR::Object::View::Default::Gtk2 0.30
UR::Object::View::Default::Html 0.30
UR::Object::View::Default::Json 0.30
UR::Object::View::Default::Text 0.30
UR::Object::View::Default::Xml 0.30
UR::Object::View::Default::Xsl 0.30
UR::Object::View::Lister::Text 0.30
UR::Object::View::Toolkit 0.30
UR::Object::View::Toolkit::Gtk undef
UR::Object::View::Toolkit::Gtk2 undef
UR::Object::View::Toolkit::Text 0.30
UR::Observer 0.30
UR::Report 0.30
UR::Service::DataSourceProxy 0.30
UR::Service::JsonRpcServer 0.30
UR::Service::RPC::Executer 0.30
UR::Service::RPC::Message 0.30
UR::Service::RPC::Server 0.30
UR::Service::RPC::TcpConnectionListener 0.30
UR::Singleton 0.30
UR::Test 0.30
UR::Util 0.30
UR::Value 0.30
UR::Value::ARRAY 0.30
UR::Value::Blob 0.30
UR::Value::CSV 0.30
UR::Value::DateTime 0.30
UR::Value::Decimal 0.30
UR::Value::DirectoryPath 0.30
UR::Value::FOF 0.30
UR::Value::FilePath 0.30
UR::Value::FilesystemPath 0.30
UR::Value::HASH 0.30
UR::Value::Integer 0.30
UR::Value::Iterator 0.30
UR::Value::Number 0.30
UR::Value::PerlReference 0.30
UR::Value::SCALAR 0.30
UR::Value::Set 0.30
UR::Value::Text 0.30
UR::Value::URL 0.30
UR::Vocabulary 0.30
URI::Escape 3.30
URI::Fetch::Response undef
URI::Find::Schemeless 20100505
URI::Heuristic 4.19
URI::IRI undef
URI::QueryParam undef
URI::Split undef
URI::Template::Restrict::Expansion undef
URI::Title::Image undef
URI::Title::MP3 undef
URI::Title::PDF undef
URI::URL 5.03
URI::WithBase 2.19
URI::_foreign undef
URI::_generic undef
URI::_idna undef
URI::_ldap 1.11
URI::_login undef
URI::_punycode 0.03
URI::_query undef
URI::_segment undef
URI::_server undef
URI::_userpass undef
URI::data undef
URI::file 4.20
URI::file::Base undef
URI::file::FAT undef
URI::file::Mac undef
URI::file::OS2 undef
URI::file::QNX undef
URI::file::Unix undef
URI::file::Win32 undef
URI::ftp undef
URI::gopher undef
URI::http undef
URI::https undef
URI::ldap 1.11
URI::ldapi undef
URI::ldaps undef
URI::mailto undef
URI::mms undef
URI::news undef
URI::nntp undef
URI::poe undef
URI::pop undef
URI::rlogin undef
URI::rsync undef
URI::rtsp undef
URI::rtspu undef
URI::sip 0.10
URI::sips undef
URI::snews undef
URI::socks undef
URI::ssh undef
URI::telnet undef
URI::tn3270 undef
URI::urn undef
URI::urn::isbn undef
URI::urn::oid undef
Ubic::AccessGuard 1.28
Ubic::Admin::Setup 1.28
Ubic::Catalog 1.19
Ubic::Cmd 1.28
Ubic::Cmd::Results 1.28
Ubic::Credentials 1.28
Ubic::Credentials::OS::MacOSX 1.28
Ubic::Credentials::OS::POSIX 1.28
Ubic::Credentials::OS::Windows 1.28
Ubic::Daemon::OS 1.28
Ubic::Daemon::OS::Linux 1.28
Ubic::Daemon::OS::POSIX 1.28
Ubic::Daemon::PidState 1.28
Ubic::Daemon::Status 1.28
Ubic::Lockf 1.28
Ubic::Lockf::Alarm 1.28
Ubic::Logger 1.28
Ubic::Multiservice 1.28
Ubic::Multiservice::Dir 1.28
Ubic::Multiservice::Simple 1.28
Ubic::Persistent 1.28
Ubic::Ping 1.28
Ubic::Ping::Service 1.28
Ubic::PortMap 1.28
Ubic::Result::Class 1.28
Ubic::Run 1.28
Ubic::Service 1.28
Ubic::Service::Common 1.28
Ubic::Service::SimpleDaemon 1.28
Ubic::Service::Utils 1.28
Ubic::Settings 1.28
Ubic::Settings::ConfigFile 1.28
Ubic::SingletonLock 1.28
Ubic::Watchdog 1.28
Unicode::CharName 1.07
Unicode::Collate::CJK::Big5 0.65
Unicode::Collate::CJK::GB2312 0.65
Unicode::Collate::CJK::JISX0208 0.64
Unicode::Collate::CJK::Korean 0.66
Unicode::Collate::CJK::Pinyin 0.65
Unicode::Collate::CJK::Stroke 0.65
Unicode::Collate::Locale 0.74
Unicode::Normalize::Mac undef
Unicode::Stringprep::BiDi 1.10
Unicode::Stringprep::Mapping 1.10
Unicode::Stringprep::Prohibited 1.10
Unicode::Stringprep::Unassigned 1.10
Unicode::Stringprep::_Common 1.10
Unicode::UCD 0.28
Unicornify::URL 1.03
Unix::AliasFile 0.06
Unix::AutomountFile 0.06
Unix::GroupFile 0.06
Unix::PasswdFile 0.06
Unix::Processors::Info 2.042
Unix::Uptime::BSD 0.3701
Unix::Uptime::BSD::XS 0.3701
Unix::Uptime::Linux 0.3701
Unzip::Passwd 0.0.14
User::Identity::Archive 0.93
User::Identity::Archive::Plain 0.93
User::Identity::Collection 0.93
User::Identity::Collection::Emails 0.93
User::Identity::Collection::Locations 0.93
User::Identity::Collection::Systems 0.93
User::Identity::Collection::Users 0.93
User::Identity::Item 0.93
User::Identity::Location 0.93
User::Identity::System 0.93
User::grent 1.01
User::pwent 1.00
VCI::Abstract::Commit undef
VCI::Abstract::Committable undef
VCI::Abstract::Diff undef
VCI::Abstract::Diff::File undef
VCI::Abstract::Directory undef
VCI::Abstract::File undef
VCI::Abstract::FileContainer undef
VCI::Abstract::History undef
VCI::Abstract::Project undef
VCI::Abstract::ProjectItem undef
VCI::Abstract::Repository undef
VCI::Util undef
VCI::VCS::Bzr 0.7.1
VCI::VCS::Bzr::Commit undef
VCI::VCS::Bzr::Committable undef
VCI::VCS::Bzr::Directory undef
VCI::VCS::Bzr::File undef
VCI::VCS::Bzr::History undef
VCI::VCS::Bzr::Project undef
VCI::VCS::Bzr::Repository undef
VCI::VCS::Cvs 0.7.1
VCI::VCS::Cvs::Commit undef
VCI::VCS::Cvs::Diff undef
VCI::VCS::Cvs::Directory undef
VCI::VCS::Cvs::File undef
VCI::VCS::Cvs::History undef
VCI::VCS::Cvs::Project undef
VCI::VCS::Cvs::Repository undef
VCI::VCS::Git 0.7.1
VCI::VCS::Git::Commit undef
VCI::VCS::Git::Committable undef
VCI::VCS::Git::Diff undef
VCI::VCS::Git::Directory undef
VCI::VCS::Git::File undef
VCI::VCS::Git::History undef
VCI::VCS::Git::Project undef
VCI::VCS::Git::Repository undef
VCI::VCS::Hg 0.7.1
VCI::VCS::Hg::Commit undef
VCI::VCS::Hg::Committable undef
VCI::VCS::Hg::Diff undef
VCI::VCS::Hg::Directory undef
VCI::VCS::Hg::File undef
VCI::VCS::Hg::History undef
VCI::VCS::Hg::Project undef
VCI::VCS::Hg::Repository undef
VCI::VCS::Svn 0.7.1
VCI::VCS::Svn::Commit undef
VCI::VCS::Svn::Committable undef
VCI::VCS::Svn::Directory undef
VCI::VCS::Svn::File undef
VCI::VCS::Svn::FileOrDirectory undef
VCI::VCS::Svn::History undef
VCI::VCS::Svn::Project undef
VCI::VCS::Svn::Repository undef
VCS::Lite::Delta 0.09
VKontakte::API::OAuth 0.02
VOMS::Lite::AC 0.12
VOMS::Lite::ASN1Helper 0.12
VOMS::Lite::Attribs::DBHelper 0.12
VOMS::Lite::Audit undef
VOMS::Lite::Base64 0.12
VOMS::Lite::CertKeyHelper 0.12
VOMS::Lite::KEY undef
VOMS::Lite::MyProxy 0.12
VOMS::Lite::OID undef
Verilog::Getopt 3.306
VOMS::Lite::PEMHelper 0.12
Verilog::Netlist 3.306
VOMS::Lite::PROXY 0.12
VOMS::Lite::REQ 0.12
VOMS::Lite::RSAHelper 0.12
VOMS::Lite::RSAKey 0.12
VOMS::Lite::VOMS 0.12
VOMS::Lite::X509 0.12
VUser::Google::ApiProtocol 1.0.0
VUser::Google::ApiProtocol::V2_0 0.5.1
VUser::Google::EmailSettings 0.1.0
VUser::Google::EmailSettings::V2_0 0.1.0
VUser::Google::ProvisioningAPI 0.25
VUser::Google::ProvisioningAPI::V1_0 0.11
VUser::Google::ProvisioningAPI::V2_0 0.25
VUser::Google::ProvisioningAPI::V2_0::EmailListEntry 0.2.0
VUser::Google::ProvisioningAPI::V2_0::EmailListRecipientEntry 0.2.0
VUser::Google::ProvisioningAPI::V2_0::NicknameEntry 0.2.0
VUser::Google::ProvisioningAPI::V2_0::UserEntry 0.2.0
Validator::Custom::Basic::Constraints undef
Validator::Custom::Constraint undef
Validator::Custom::Result undef
Variable::Lazy::Guts 0.03
Verilog::EditFiles 3.306
Verilog::Language 3.306
Verilog::Netlist::Cell 3.306
Verilog::Netlist::ContAssign 3.306
Verilog::Netlist::Defparam 3.306
Verilog::Netlist::File 3.306
Verilog::Netlist::Interface 3.306
Verilog::Netlist::Logger 3.035
Verilog::Netlist::ModPort 3.306
Verilog::Netlist::Module 3.306
Verilog::Netlist::Net 3.306
Verilog::Netlist::Pin 3.306
Verilog::Netlist::Port 3.306
Verilog::Netlist::Subclass 3.306
Verilog::Parser 3.306
Verilog::Preproc 3.306
Verilog::SigParser 3.306
Verilog::Std 3.306
Video::CPL::Annotation 0.10
Video::CPL::AnnotationList 0.10
Video::CPL::Cue 0.10
Video::CPL::DirectoryList 0.10
Video::CPL::Layout 0.10
Video::CPL::MXML 0.10
Video::CPL::MXMLField 0.10
Video::CPL::Story 0.10
Video::CPL::Target 0.10
Video::CPL::TargetList 0.10
Vimana::Archive undef
Vimana::Command undef
Vimana::Command::Download undef
Vimana::Command::Help undef
Vimana::Command::Info undef
Vimana::Command::Install undef
Vimana::Command::Installed undef
Vimana::Command::Rate undef
Vimana::Command::Remove undef
Vimana::Command::Search undef
Vimana::Command::Update undef
Vimana::Command::Upgrade undef
Vimana::GitInstall undef
Vimana::Index undef
Vimana::Installer undef
Vimana::Installer::Auto undef
Vimana::Installer::Makefile undef
Vimana::Installer::Meta undef
Vimana::Installer::Rakefile undef
Vimana::Installer::Text undef
Vimana::Installer::Vimball undef
Vimana::Logger undef
Vimana::Record undef
Vimana::Recursive 0.38
Vimana::Util undef
Vimana::VimOnline undef
Vimana::VimOnline::ScriptPage undef
Vimana::VimOnline::Search undef
Voldemort::Connection undef
Voldemort::Message undef
Voldemort::ProtoBuff::BaseMessage undef
Voldemort::ProtoBuff::Connection undef
Voldemort::ProtoBuff::DeleteMessage undef
Voldemort::ProtoBuff::GetMessage undef
Voldemort::ProtoBuff::PutMessage undef
Voldemort::ProtoBuff::Spec2 undef
Voldemort::Store undef
Vroom::OO 0.15
W3C::XMLSchema::Attribute 0.0.2
W3C::XMLSchema::AttributeGroup 0.0.2
W3C::XMLSchema::ComplexType 0.0.2
W3C::XMLSchema::Element 0.0.2
W3C::XMLSchema::Group 0.0.2
W3C::XMLSchema::Sequence 0.0.2
WAP::wbxml::WbRules undef
WSRF::SSLDaemon 1.04
WSST::CodeTemplate 0.1.1
WSST::Generator 0.1.1
WSST::Schema 0.1.1
WSST::Schema::Base 0.1.1
WSST::Schema::Data 0.1.1
WSST::Schema::Error 0.1.1
WSST::Schema::Method 0.1.1
WSST::Schema::Node 0.1.1
WSST::Schema::Param 0.1.1
WSST::Schema::Return 0.1.1
WSST::Schema::Test 0.1.1
WSST::SchemaParser 0.1.1
WSST::SchemaParser::YAML 0.1.1
WSST::SchemaParserManager 0.1.1
WWW::3Taps::API::Types undef
WWW::AUR::Iterator undef
WWW::AUR::Login undef
WWW::AUR::Maintainer undef
WWW::AUR::PKGBUILD undef
WWW::AUR::Package undef
WWW::AUR::Package::File undef
WWW::AUR::RPC undef
WWW::AUR::URI undef
WWW::AUR::UserAgent undef
WWW::AUR::Var 0.01
WWW::Adblock::RegexFilter 0.02
WWW::AdventCalendar::Article 1.001
WWW::AdventCalendar::Config 1.001
WWW::Analytics::MultiTouch::Tabular undef
WWW::BigDoor 0.1.1
WWW::BigDoor::Attribute undef
WWW::BigDoor::Award undef
WWW::BigDoor::Currency undef
WWW::BigDoor::CurrencyBalance undef
WWW::BigDoor::CurrencyType undef
WWW::BigDoor::EndUser undef
WWW::BigDoor::Good undef
WWW::BigDoor::Leaderboard undef
WWW::BigDoor::Level undef
WWW::BigDoor::NamedAward undef
WWW::BigDoor::NamedAwardCollection undef
WWW::BigDoor::NamedGood undef
WWW::BigDoor::NamedGoodCollection undef
WWW::BigDoor::NamedLevel undef
WWW::BigDoor::NamedLevelCollection undef
WWW::BigDoor::NamedTransaction undef
WWW::BigDoor::NamedTransactionGroup undef
WWW::BigDoor::Profile undef
WWW::BigDoor::Resource undef
WWW::BigDoor::URL undef
WWW::Contact::AOL 0.39
WWW::Contact::BG::Abv 0.26
WWW::Contact::BG::Mail 0.26
WWW::Contact::Base 0.16
WWW::Contact::CN::163 0.28
WWW::Contact::Gmail 0.24
WWW::Contact::GoogleContactsAPI 0.23
WWW::Contact::Hotmail 0.41
WWW::Contact::Indiatimes 0.16
WWW::Contact::Lycos 0.16
WWW::Contact::Mail 0.16
WWW::Contact::Plaxo 0.16
WWW::Contact::Rediffmail 0.30
WWW::Contact::Yahoo 0.40
WWW::Curl::Form 4.19_9903
WWW::Curl::Multi undef
WWW::Curl::Share undef
WWW::DDG 0.004
WWW::DuckDuckGo::Icon 0.004
WWW::DuckDuckGo::Link 0.004
WWW::DuckDuckGo::ZeroClickInfo 0.004
WWW::Facebook::API::Admin undef
WWW::Facebook::API::Application undef
WWW::Facebook::API::Auth undef
WWW::Facebook::API::Canvas undef
WWW::Facebook::API::Comments undef
WWW::Facebook::API::Connect undef
WWW::Facebook::API::Data undef
WWW::Facebook::API::Events undef
WWW::Facebook::API::Exception undef
WWW::Facebook::API::FBML undef
WWW::Facebook::API::FQL undef
WWW::Facebook::API::Feed undef
WWW::Facebook::API::Friends undef
WWW::Facebook::API::Groups undef
WWW::Facebook::API::Intl undef
WWW::Facebook::API::Links undef
WWW::Facebook::API::LiveMessage undef
WWW::Facebook::API::Message undef
WWW::Facebook::API::Notes undef
WWW::Facebook::API::Notifications undef
WWW::Facebook::API::Pages undef
WWW::Facebook::API::Permissions undef
WWW::Facebook::API::Photos undef
WWW::Facebook::API::Profile undef
WWW::Facebook::API::SMS undef
WWW::Facebook::API::Status undef
WWW::Facebook::API::Stream undef
WWW::Facebook::API::Users undef
WWW::Facebook::API::Video undef
WWW::Finger::BitworkingFingerProtocol 0.101
WWW::Finger::CPAN 0.101
WWW::Finger::Fingerpoint 0.101
WWW::Finger::Webfinger 0.101
WWW::Finger::_GenericRDF 0.101
WWW::Formspring::Question undef
WWW::Formspring::Response undef
WWW::Formspring::User undef
WWW::Foursquare::RSS::Checkin 0.01
WWW::Getsy::Client undef
WWW::Getsy::OAuth undef
WWW::Getsy::Types undef
WWW::GoodData::Agent 1.0
WWW::Google::Contacts::Base 0.28
WWW::Google::Contacts::Contact 0.28
WWW::Google::Contacts::ContactList 0.28
WWW::Google::Contacts::Data 0.28
WWW::Google::Contacts::Group 0.28
WWW::Google::Contacts::GroupList 0.28
WWW::Google::Contacts::InternalTypes 0.28
WWW::Google::Contacts::Meta::Attribute::Trait::XmlField 0.28
WWW::Google::Contacts::Photo 0.28
WWW::Google::Contacts::Roles::CRUD 0.28
WWW::Google::Contacts::Roles::HasTypeAndLabel 0.28
WWW::Google::Contacts::Roles::List 0.28
WWW::Google::Contacts::Server 0.28
WWW::Google::Contacts::Type::Base 0.28
WWW::Google::Contacts::Type::Birthday 0.28
WWW::Google::Contacts::Type::CalendarLink 0.28
WWW::Google::Contacts::Type::Category 0.28
WWW::Google::Contacts::Type::ContactEvent 0.28
WWW::Google::Contacts::Type::Email 0.28
WWW::Google::Contacts::Type::ExternalId 0.28
WWW::Google::Contacts::Type::Gender 0.28
WWW::Google::Contacts::Type::GroupMembership 0.28
WWW::Google::Contacts::Type::Hobby 0.28
WWW::Google::Contacts::Type::IM 0.28
WWW::Google::Contacts::Type::Jot 0.28
WWW::Google::Contacts::Type::Language 0.28
WWW::Google::Contacts::Type::Name 0.28
WWW::Google::Contacts::Type::Organization 0.28
WWW::Google::Contacts::Type::PhoneNumber 0.28
WWW::Google::Contacts::Type::PostalAddress 0.28
WWW::Google::Contacts::Type::Priority 0.28
WWW::Google::Contacts::Type::Rel 0.28
WWW::Google::Contacts::Type::Relation 0.28
WWW::Google::Contacts::Type::Sensitivity 0.28
WWW::Google::Contacts::Type::UserDefined 0.28
WWW::Google::Contacts::Type::Website 0.28
WWW::Google::Contacts::Type::When 0.28
WWW::Google::Contacts::Types 0.28
WWW::IRail::API::Client::LWP 0.003
WWW::IRail::API::Connections 0.003
WWW::IRail::API::Liveboard 0.003
WWW::IRail::API::Stations 0.003
WWW::IRail::API::Vehicle 0.003
WWW::Live::Auth::ApplicationToken undef
WWW::Live::Auth::ConsentToken undef
WWW::Live::Auth::Offer undef
WWW::Live::Auth::SecretKey undef
WWW::Live::Auth::Utils undef
WWW::Live::Contacts::Address 1.0.1
WWW::Live::Contacts::Collection 1.0.1
WWW::Live::Contacts::Contact 1.0.1
WWW::Live::Contacts::Email 1.0.1
WWW::Live::Contacts::PhoneNumber 1.0.1
WWW::Mechanize::Firefox::DSL 0.50
WWW::Mechanize::Firefox::Examples 0.50
WWW::Mechanize::FormFiller::Value 0.10
WWW::Mechanize::FormFiller::Value::Callback 0.10
WWW::Mechanize::FormFiller::Value::Default 0.10
WWW::Mechanize::FormFiller::Value::Fixed 0.10
WWW::Mechanize::FormFiller::Value::Interactive 0.10
WWW::Mechanize::FormFiller::Value::Keep 0.10
WWW::Mechanize::FormFiller::Value::Random 0.10
WWW::Mechanize::FormFiller::Value::Random::Chars 0.10
WWW::Mechanize::FormFiller::Value::Random::Date 0.10
WWW::Mechanize::FormFiller::Value::Random::Word 0.10
WWW::Mechanize::Image undef
WWW::Mechanize::Link undef
WWW::MenuGrinder::Plugin::ActivePath 0.06
WWW::MenuGrinder::Plugin::DefaultTarget 0.06
WWW::MenuGrinder::Plugin::FileReloader 0.06
WWW::MenuGrinder::Plugin::Hotkey 0.06
WWW::MenuGrinder::Plugin::Localize 0.06
WWW::MenuGrinder::Plugin::NullLoader 0.06
WWW::MenuGrinder::Plugin::NullOutput 0.06
WWW::MenuGrinder::Plugin::NullTransform 0.06
WWW::MenuGrinder::Plugin::RequirePrivilege 0.06
WWW::MenuGrinder::Plugin::SimpleLoader 0.06
WWW::MenuGrinder::Plugin::Variables 0.06
WWW::MenuGrinder::Plugin::XMLLoader 0.06
WWW::MenuGrinder::Plugin::YAMLLoader 0.06
WWW::MenuGrinder::Role::BeforeMogrify 0.06
WWW::MenuGrinder::Role::ItemMogrifier 0.06
WWW::MenuGrinder::Role::Loader 0.06
WWW::MenuGrinder::Role::Mogrifier 0.06
WWW::MenuGrinder::Role::OnInit 0.06
WWW::MenuGrinder::Role::Output 0.06
WWW::MenuGrinder::Role::Plugin 0.06
WWW::MenuGrinder::Visitor 0.06
WWW::Mixi::Scraper::Mech undef
WWW::Mixi::Scraper::Plugin undef
WWW::Mixi::Scraper::Plugin::ListBookmark undef
WWW::Mixi::Scraper::Plugin::ListComment undef
WWW::Mixi::Scraper::Plugin::ListDiary undef
WWW::Mixi::Scraper::Plugin::ListEcho undef
WWW::Mixi::Scraper::Plugin::ListMessage undef
WWW::Mixi::Scraper::Plugin::NewBBS undef
WWW::Mixi::Scraper::Plugin::NewFriendDiary undef
WWW::Mixi::Scraper::Plugin::NewMusic undef
WWW::Mixi::Scraper::Plugin::NewVideo undef
WWW::Mixi::Scraper::Plugin::RecentEcho undef
WWW::Mixi::Scraper::Plugin::ResEcho undef
WWW::Mixi::Scraper::Plugin::ShowCalendar undef
WWW::Mixi::Scraper::Plugin::ShowFriend undef
WWW::Mixi::Scraper::Plugin::ShowLog undef
WWW::Mixi::Scraper::Plugin::ShowSchedule undef
WWW::Mixi::Scraper::Plugin::ViewBBS undef
WWW::Mixi::Scraper::Plugin::ViewDiary undef
WWW::Mixi::Scraper::Plugin::ViewEcho undef
WWW::Mixi::Scraper::Plugin::ViewEvent undef
WWW::Mixi::Scraper::Plugin::ViewMessage undef
WWW::Mixi::Scraper::Utils undef
WWW::NOS::Open::Article 0.02
WWW::NOS::Open::AudioFragment 0.02
WWW::NOS::Open::Broadcast 0.02
WWW::NOS::Open::DayGuide 0.02
WWW::NOS::Open::Document 0.02
WWW::NOS::Open::Exceptions 0.02
WWW::NOS::Open::Interface 0.02
WWW::NOS::Open::MediaResource 0.02
WWW::NOS::Open::Resource 0.02
WWW::NOS::Open::Result 0.02
WWW::NOS::Open::TypeDef 0.02
WWW::NOS::Open::Version 0.02
WWW::NOS::Open::Video 0.02
WWW::NewsReach::Category 0.06
WWW::NewsReach::Client 0.06
WWW::NewsReach::Comment 0.06
WWW::NewsReach::NewsItem 0.06
WWW::NewsReach::Photo 0.06
WWW::NewsReach::Photo::Instance 0.06
WWW::NicoSound::Download::Sound undef
WWW::OReillyMedia::Store::Book 0.04
WWW::OpenSearch::Agent undef
WWW::OpenSearch::Description undef
WWW::OpenSearch::Image undef
WWW::OpenSearch::Query undef
WWW::OpenSearch::Request undef
WWW::OpenSearch::Response undef
WWW::OpenSearch::Url undef
WWW::PGXN::Distribution v0.12.0
WWW::PGXN::Extension v0.12.0
WWW::PGXN::Mirror v0.12.0
WWW::PGXN::Tag v0.12.0
WWW::PGXN::User v0.12.0
WWW::Pastebin::Sprunge::Retrieve 0.006
WWW::RobotRules::AnyDBM_File 6.00
WWW::Salesforce::Constants undef
WWW::Salesforce::Deserializer undef
WWW::Salesforce::Serializer undef
WWW::Salesforce::Simple undef
WWW::Scraper::ISBN::AmazonUK_Driver 0.25
WWW::Scraper::ISBN::AmazonUS_Driver 0.25
WWW::Scripter::Plugin::JavaScript::JE 0.007
WWW::Scripter::WindowGroup 0.021
WWW::Scripter::_Mechanize 1.66
WWW::Scripter::_Mechanize::Image undef
WWW::Scripter::_Mechanize::Link undef
WWW::Search::Ebay::Auctions 1.009
WWW::Search::Ebay::BuyItNow 1.014
WWW::Search::Ebay::ByEndDate 2.032
WWW::Search::Ebay::BySellerID 2.011
WWW::Search::Ebay::Category 2.004
WWW::Search::Ebay::Motors 1.016
WWW::Search::Ebay::Stores 1.018
WWW::Search::Null undef
WWW::Search::Null::Count 1.016
WWW::Search::Null::Empty 1.009
WWW::Search::Null::Error 1.011
WWW::Search::Null::NoVersion undef
WWW::Search::Result 1.005
WWW::Search::Simple undef
WWW::Selenium::Util undef
WWW::Shorten::LinkToolbot 1.90
WWW::Shorten::Linkz 1.90
WWW::Shorten::MakeAShorterLink 1.90
WWW::Shorten::Metamark 1.91
WWW::Shorten::TinyClick 1.90
WWW::Shorten::TinyURL::RU 0.07
WWW::Shorten::generic 1.92
WWW::Sitemap::XML::URL 1.103500
WWW::Sitemap 0.002
WWW::Sitemap::XML::Types 1.103500
WWW::Sitemap::XML::URL::Interface 1.103500
WWW::SitemapIndex::XML 1.103500
WWW::SitemapIndex::XML::Sitemap 1.103500
WWW::SitemapIndex::XML::Sitemap::Interface 1.103500
WWW::Sitemapper::Tree 1.110340
WWW::Sitemapper::Types 1.110340
WWW::Splunk::API 1.10
WWW::Splunk::XMLParser undef
WWW::TheMovieDB::Search 0.01
WWW::TypePad::ApiKeys undef
WWW::TypePad::Applications undef
WWW::TypePad::Assets undef
WWW::TypePad::AuthTokens undef
WWW::TypePad::Badges undef
WWW::TypePad::Blogs undef
WWW::TypePad::CodeGen undef
WWW::TypePad::Error undef
WWW::TypePad::Events undef
WWW::TypePad::ExternalFeedSubscriptions undef
WWW::TypePad::Favorites undef
WWW::TypePad::Groups undef
WWW::TypePad::ImportJobs undef
WWW::TypePad::Noun undef
WWW::TypePad::Nouns undef
WWW::TypePad::ObjectTypes undef
WWW::TypePad::Relationships undef
WWW::TypePad::Users undef
WWW::TypePad::Util undef
WWW::USF::Directory::Entry 0.003
WWW::USF::Directory::Entry::Affiliation 0.003
WWW::USF::Directory::Exception 0.003
WWW::USF::Directory::Exception::MethodArguments 0.003
WWW::USF::Directory::Exception::TooManyResults 0.003
WWW::USF::Directory::Exception::UnknownResponse 0.003
WWW::Vimeo::Simple::Activity 0.06
WWW::Vimeo::Simple::Album 0.06
WWW::Vimeo::Simple::Channel 0.06
WWW::Vimeo::Simple::Group 0.06
WWW::Vimeo::Simple::User 0.06
WWW::Vimeo::Simple::User::Activity 0.06
WWW::Vimeo::Simple::Video 0.06
WWW::Wikipedia::Entry undef
WWW::Wookie::Connector::Exceptions 0.03
WWW::Wookie::Connector::Service 0.03
WWW::Wookie::Connector::Service::Interface 0.03
WWW::Wookie::Server::Connection 0.03
WWW::Wookie::User 0.03
WWW::Wookie::Widget 0.03
WWW::Wookie::Widget::Category 0.03
WWW::Wookie::Widget::Instance 0.03
WWW::Wookie::Widget::Instances 0.03
WWW::Wookie::Widget::Property 0.03
WWW::iTunesConnect 1.16
Water 0.01
Weather::Bug::Alert 0.25
Weather::Bug::CompactWeather 0.25
Weather::Bug::DateParser 0.25
Weather::Bug::Forecast 0.25
Weather::Bug::Location 0.25
Weather::Bug::Quantity 0.25
Weather::Bug::SevenDayForecast 0.25
Weather::Bug::Station 0.25
Weather::Bug::Temperature 0.25
Weather::Bug::Weather 0.25
Web::App::Config undef
Web::App::Config::Screen undef
Web::App::Helper undef
Web::App::Lib::EntityCollection undef
Web::App::Lib::IO undef
Web::App::Presenter undef
Web::App::Presenter::AsIs undef
Web::App::Presenter::JSON undef
Web::App::Presenter::XMLDump undef
Web::App::Presenter::XSLT undef
Web::App::Request undef
Web::App::Request::CGI undef
Web::App::Request::ModPerl undef
Web::App::Request::ModPerl2 undef
Web::App::Response undef
Web::App::Session undef
Web::App::Session::Cookie undef
Web::App::Test undef
Web::Dispatch undef
Web::Dispatch::Node undef
Web::Dispatch::ParamParser undef
Web::Dispatch::Parser undef
Web::Dispatch::Predicates undef
Web::Dispatch::ToApp undef
Web::Dispatch::Wrapper undef
Web::Dispatcher::Simple::Request undef
Web::Dispatcher::Simple::Response undef
Web::Gui 0.63
Web::Hippie::App::JSFiles undef
Web::Hippie::Handle::MXHR undef
Web::Hippie::Handle::WebSocket undef
Web::Hippie::Pipe 0.01
Web::Scraper::Filter undef
Web::Scraper::LibXML undef
Web::oEmbed::Response undef
WebDAO::Base undef
WebDAO::CVapache2 undef
WebDAO::CVcgi undef
WebDAO::CVfcgiutf8 undef
WebDAO::Comp::ListEnv undef
WebDAO::Component undef
WebDAO::Container undef
WebDAO::Element undef
WebDAO::Engine undef
WebDAO::FCGI::ProcManager 0.17
WebDAO::Lex undef
WebDAO::Lexer::Lbase undef
WebDAO::Lexer::Linclude undef
WebDAO::Lexer::Lmethod undef
WebDAO::Lexer::Lobject undef
WebDAO::Lexer::Lobjectref undef
WebDAO::Lexer::Lregclass undef
WebDAO::Lexer::Ltext undef
WebDAO::Lib::MethodByPath undef
WebDAO::Lib::RawHTML undef
WebDAO::Response undef
WebDAO::Session undef
WebDAO::SessionID undef
WebDAO::SessionSH undef
WebDAO::Sessionco undef
WebDAO::Store::Abstract undef
WebDAO::Store::Storable undef
WebDAO::Test undef
WebNano::FindController 0.006
WebNano::Renderer::TTiny 0.006
WebService::8tracks::Response undef
WebService::8tracks::Session undef
WebService::Aladdin::Item undef
WebService::Aladdin::Item::Book undef
WebService::Aladdin::Item::DVD undef
WebService::Aladdin::Item::Music undef
WebService::Aladdin::Items undef
WebService::Aladdin::Parser undef
WebService::Async::Converter::Function undef
WebService::Async::Converter::JSON undef
WebService::Async::Converter::Raw undef
WebService::Async::Converter::XMLSimple undef
WebService::Async::Parser::JSON undef
WebService::Async::Parser::Raw undef
WebService::Async::Parser::XMLSimple undef
WebService::Async::Request undef
WebService::Async::ResponseCache undef
WebService::Async::Result undef
WebService::Async::Role::Converter undef
WebService::Async::Role::Parser undef
WebService::Bitly::Entry undef
WebService::Bitly::Result undef
WebService::Bitly::Result::BitlyProDomain undef
WebService::Bitly::Result::Clicks undef
WebService::Bitly::Result::ClicksByDay undef
WebService::Bitly::Result::ClicksByMinute undef
WebService::Bitly::Result::Countries undef
WebService::Bitly::Result::Expand undef
WebService::Bitly::Result::HTTPError undef
WebService::Bitly::Result::Info undef
WebService::Bitly::Result::Lookup undef
WebService::Bitly::Result::Referrers undef
WebService::Bitly::Result::Shorten undef
WebService::Bitly::Result::Validate undef
WebService::Bitly::Util undef
WebService::Blogger::AtomReading 0.14
WebService::Blogger::Blog 0.14
WebService::Blogger::Blog::Entry 0.14
WebService::ClinicalTrialsdotGov::Parser undef
WebService::ClinicalTrialsdotGov::Reply undef
WebService::ClinicalTrialsdotGov::Request undef
WebService::ClinicalTrialsdotGov::SearchResult undef
WebService::ClinicalTrialsdotGov::Study undef
WebService::EastCoJp::Dictionary::GetDicItemParameter 0.02
WebService::EastCoJp::Dictionary::ParameterConstraints 0.01
WebService::EastCoJp::Dictionary::SearchDicItemParameter 0.02
WebService::Edgecast::auto::Administration::Element::ArrayOfCustomerOriginInfo 0.01.00
WebService::Etsy::Methods undef
WebService::Etsy::Resource undef
WebService::Etsy::Response undef
WebService::FuncNet::Predictor::Logable undef
WebService::FuncNet::Predictor::Logger undef
WebService::FuncNet::Predictor::Operation undef
WebService::FuncNet::Predictor::Operation::ScorePairwiseRelations undef
WebService::FuncNet::Predictor::Operation::ScorePairwiseRelations::Response undef
WebService::FuncNet::Predictor::Operation::ScorePairwiseRelations::Result undef
WebService::FuncNet::Predictor::Types undef
WebService::GData::Base 0.0203
WebService::GData::BaseCollection 0.0102
WebService::GData::Batch undef
WebService::GData::Batch::Entry undef
WebService::GData::Batch::Response undef
WebService::GData::ClientLogin 0.0106
WebService::GData::Collection 0.0104
WebService::GData::Constants 1.06
WebService::GData::Error 1.02
WebService::GData::Error::Entry 1.02
WebService::GData::Feed 0.0108
WebService::GData::Feed::Entry 0.0104
WebService::GData::Iterator undef
WebService::GData::Node 0.0103
WebService::GData::Node::APP undef
WebService::GData::Node::APP::Control undef
WebService::GData::Node::APP::Draft undef
WebService::GData::Node::APP::Edited undef
WebService::GData::Node::AbstractEntity 0.0103
WebService::GData::Node::Atom undef
WebService::GData::Node::Atom::AtomEntity 0.0102
WebService::GData::Node::Atom::Author undef
WebService::GData::Node::Atom::AuthorEntity 0.0102
WebService::GData::Node::Atom::Category undef
WebService::GData::Node::Atom::Content undef
WebService::GData::Node::Atom::Entry undef
WebService::GData::Node::Atom::EntryEntity 0.0101
WebService::GData::Node::Atom::Feed undef
WebService::GData::Node::Atom::FeedEntity 0.0101
WebService::GData::Node::Atom::Generator undef
WebService::GData::Node::Atom::Id undef
WebService::GData::Node::Atom::Link undef
WebService::GData::Node::Atom::Logo undef
WebService::GData::Node::Atom::Name undef
WebService::GData::Node::Atom::Published undef
WebService::GData::Node::Atom::Summary undef
WebService::GData::Node::Atom::Title undef
WebService::GData::Node::Atom::Updated undef
WebService::GData::Node::Atom::Uri undef
WebService::GData::Node::Author undef
WebService::GData::Node::AuthorEntity 0.0101
WebService::GData::Node::Category undef
WebService::GData::Node::Content undef
WebService::GData::Node::GD undef
WebService::GData::Node::GD::AditionalName undef
WebService::GData::Node::GD::Agent undef
WebService::GData::Node::GD::AttendeeStatus undef
WebService::GData::Node::GD::AttendeeType undef
WebService::GData::Node::GD::City undef
WebService::GData::Node::GD::Comments undef
WebService::GData::Node::GD::Country undef
WebService::GData::Node::GD::Deleted undef
WebService::GData::Node::GD::Email undef
WebService::GData::Node::GD::EntryLink undef
WebService::GData::Node::GD::EventStatus undef
WebService::GData::Node::GD::ExtendedProperty undef
WebService::GData::Node::GD::FamilyName undef
WebService::GData::Node::GD::FeedLink undef
WebService::GData::Node::GD::FormattedAddress undef
WebService::GData::Node::GD::GivenName undef
WebService::GData::Node::GD::Housename undef
WebService::GData::Node::GD::Im undef
WebService::GData::Node::GD::Money undef
WebService::GData::Node::GD::Name undef
WebService::GData::Node::GD::Neighborhood undef
WebService::GData::Node::GD::OrgDepartment undef
WebService::GData::Node::GD::OrgJobDescription undef
WebService::GData::Node::GD::OrgName undef
WebService::GData::Node::GD::OrgSymbol undef
WebService::GData::Node::GD::OrgTitle undef
WebService::GData::Node::GD::Organization undef
WebService::GData::Node::GD::OriginalEvent undef
WebService::GData::Node::GD::PhoneNumber undef
WebService::GData::Node::GD::Pobox undef
WebService::GData::Node::GD::PostalAddress undef
WebService::GData::Node::GD::Postcode undef
WebService::GData::Node::GD::Rating undef
WebService::GData::Node::GD::Recurrence undef
WebService::GData::Node::GD::RecurrenceException undef
WebService::GData::Node::GD::Region undef
WebService::GData::Node::GD::Reminder undef
WebService::GData::Node::GD::ResourceId undef
WebService::GData::Node::GD::Street undef
WebService::GData::Node::GD::StructuredPostalAddress undef
WebService::GData::Node::GD::Subregion undef
WebService::GData::Node::GD::Transparency undef
WebService::GData::Node::GD::Visibility undef
WebService::GData::Node::GD::When undef
WebService::GData::Node::GD::Where undef
WebService::GData::Node::GD::Who undef
WebService::GData::Node::GML undef
WebService::GData::Node::GML::Point undef
WebService::GData::Node::GML::Pos undef
WebService::GData::Node::GeoRSS undef
WebService::GData::Node::GeoRSS::Where undef
WebService::GData::Node::Link undef
WebService::GData::Node::Media undef
WebService::GData::Node::Media::Category undef
WebService::GData::Node::Media::Content undef
WebService::GData::Node::Media::Credit undef
WebService::GData::Node::Media::Description undef
WebService::GData::Node::Media::Group undef
WebService::GData::Node::Media::GroupEntity 0.0102
WebService::GData::Node::Media::Keywords undef
WebService::GData::Node::Media::Player undef
WebService::GData::Node::Media::Rating undef
WebService::GData::Node::Media::Restriction undef
WebService::GData::Node::Media::Thumbnail undef
WebService::GData::Node::Media::Title undef
WebService::GData::Node::Name undef
WebService::GData::Node::OpenSearch undef
WebService::GData::Node::OpenSearch::ItemsPerPage undef
WebService::GData::Node::OpenSearch::StartIndex undef
WebService::GData::Node::OpenSearch::TotalResults undef
WebService::GData::Node::PointEntity 0.0101
WebService::GData::Node::Summary undef
WebService::GData::Node::Title undef
WebService::GData::Node::Uri undef
WebService::GData::Query 0.0205
WebService::GData::Serialize 0.0101
WebService::GData::Serialize::XML 0.0103
WebService::GData::YouTube 0.0202
WebService::GData::YouTube::Constants 1.02
WebService::GData::YouTube::Feed 0.0101
WebService::GData::YouTube::Feed::Comment 0.0104
WebService::GData::YouTube::Feed::Complaint 0.0101
WebService::GData::YouTube::Feed::Playlist 0.0101
WebService::GData::YouTube::Feed::PlaylistLink 0.0102
WebService::GData::YouTube::Feed::UserProfile 0.0101
WebService::GData::YouTube::Feed::Video 0.0109
WebService::GData::YouTube::Query 0.0102
WebService::GData::YouTube::StagingServer undef
WebService::GData::YouTube::YT 0.0101
WebService::GData::YouTube::YT::AboutMe undef
WebService::GData::YouTube::YT::AccessControl undef
WebService::GData::YouTube::YT::Age undef
WebService::GData::YouTube::YT::AspectRatio undef
WebService::GData::YouTube::YT::Books undef
WebService::GData::YouTube::YT::Company undef
WebService::GData::YouTube::YT::CountHint undef
WebService::GData::YouTube::YT::Derived undef
WebService::GData::YouTube::YT::Duration undef
WebService::GData::YouTube::YT::FirstName undef
WebService::GData::YouTube::YT::Gender undef
WebService::GData::YouTube::YT::GroupEntity 0.0101
WebService::GData::YouTube::YT::Hobbies undef
WebService::GData::YouTube::YT::Hometown undef
WebService::GData::YouTube::YT::Incomplete undef
WebService::GData::YouTube::YT::LastName undef
WebService::GData::YouTube::YT::Location undef
WebService::GData::YouTube::YT::Media::Content undef
WebService::GData::YouTube::YT::Media::Credit undef
WebService::GData::YouTube::YT::Movies undef
WebService::GData::YouTube::YT::Music undef
WebService::GData::YouTube::YT::Occupation undef
WebService::GData::YouTube::YT::Position undef
WebService::GData::YouTube::YT::Private undef
WebService::GData::YouTube::YT::Rating undef
WebService::GData::YouTube::YT::Recorded undef
WebService::GData::YouTube::YT::Relationship undef
WebService::GData::YouTube::YT::School undef
WebService::GData::YouTube::YT::State undef
WebService::GData::YouTube::YT::Statistics undef
WebService::GData::YouTube::YT::Status undef
WebService::GData::YouTube::YT::Uploaded undef
WebService::GData::YouTube::YT::Username undef
WebService::GData::YouTube::YT::Videoid undef
WebService::Google::Closure::Response undef
WebService::Google::Closure::Type::Error undef
WebService::Google::Closure::Type::Stats undef
WebService::Google::Closure::Type::Warning undef
WebService::Google::Closure::Types undef
WebService::Google::Reader::Constants undef
WebService::Google::Reader::Feed undef
WebService::Google::Reader::ListElement undef
WebService::Hatena::Fotolife::Entry undef
WebService::KuronekoYamato 0.0.3
WebService::MusicBrainz::Artist 0.93
WebService::MusicBrainz::Label 0.93
WebService::MusicBrainz::Query 0.93
WebService::MusicBrainz::Release 0.93
WebService::MusicBrainz::ReleaseGroup 0.93
WebService::MusicBrainz::Response 0.93
WebService::MusicBrainz::Response::Alias 0.93
WebService::MusicBrainz::Response::AliasList 0.93
WebService::MusicBrainz::Response::Artist 0.93
WebService::MusicBrainz::Response::ArtistList 0.93
WebService::MusicBrainz::Response::Disc 0.93
WebService::MusicBrainz::Response::DiscList 0.93
WebService::MusicBrainz::Response::ISRC 0.93
WebService::MusicBrainz::Response::ISRCList 0.93
WebService::MusicBrainz::Response::Label 0.93
WebService::MusicBrainz::Response::LabelList 0.93
WebService::MusicBrainz::Response::Metadata 0.93
WebService::MusicBrainz::Response::Puid 0.93
WebService::MusicBrainz::Response::PuidList 0.93
WebService::MusicBrainz::Response::Rating 0.93
WebService::MusicBrainz::Response::Relation 0.93
WebService::MusicBrainz::Response::RelationList 0.93
WebService::MusicBrainz::Response::Release 0.93
WebService::MusicBrainz::Response::ReleaseEvent 0.93
WebService::MusicBrainz::Response::ReleaseEventList 0.93
WebService::MusicBrainz::Response::ReleaseGroup 0.93
WebService::MusicBrainz::Response::ReleaseGroupList 0.93
WebService::MusicBrainz::Response::ReleaseList 0.93
WebService::MusicBrainz::Response::Tag 0.93
WebService::MusicBrainz::Response::TagList 0.93
WebService::MusicBrainz::Response::Track 0.93
WebService::MusicBrainz::Response::TrackList 0.93
WebService::MusicBrainz::Response::UserRating 0.93
WebService::MusicBrainz::Response::UserTag 0.93
WebService::MusicBrainz::Response::UserTagList 0.93
WebService::MusicBrainz::Track 0.93
WebService::NFSN::Account 0.09
WebService::NFSN::DNS 0.09
WebService::NFSN::Email 0.09
WebService::NFSN::Member 0.09
WebService::NFSN::Object 0.09
WebService::NFSN::Site 0.09
WebService::Nestoria::Search::MetadataResponse 1.020000
WebService::Nestoria::Search::Request 1.020000
WebService::Nestoria::Search::Response 1.020000
WebService::Nestoria::Search::Result 1.020000
WebService::NiigataUnyu 0.0.6
WebService::PutIo::Files undef
WebService::PutIo::Messages undef
WebService::PutIo::Result undef
WebService::PutIo::Subscriptions undef
WebService::PutIo::Transfers undef
WebService::PutIo::URLs undef
WebService::PutIo::User undef
WebService::Rackspace::CloudFiles::Container undef
WebService::Rackspace::CloudFiles::Object undef
WebService::SagawaKyubin 0.0.5
WebService::Simple::Parser undef
WebService::Simple::Parser::XML::Feed undef
WebService::Simple::Parser::XML::LibXML undef
WebService::Simple::Parser::XML::Lite undef
WebService::Simple::Parser::XML::Simple undef
WebService::Simple::Response 0.02
WebService::Solr::Document undef
WebService::Solr::Field undef
WebService::Solr::Query undef
WebService::TVRage::Episode undef
WebService::TVRage::EpisodeList undef
WebService::TVRage::EpisodeListRequest undef
WebService::TVRage::Show undef
WebService::TVRage::ShowList undef
WebService::TVRage::ShowSearchRequest undef
WebService::TWFY::Request 0.01
WebService::TWFY::Response 0.01
WebService::Tumblr::Dispatch undef
WebService::Tumblr::Result undef
WebService::UMLSKS::ConnectUMLS undef
WebService::UMLSKS::DisplayInfo undef
WebService::UMLSKS::GetAllowablePaths undef
WebService::UMLSKS::GetCUIs undef
WebService::UMLSKS::GetNeighbors undef
WebService::UMLSKS::GetParents undef
WebService::UMLSKS::GetUserData undef
WebService::UMLSKS::Query undef
WebService::UMLSKS::ValidateTerm undef
WebService::Validator::HTML::W3C::Error undef
WebService::Validator::HTML::W3C::Warning undef
WebService::VaultPress::Partner::Request::GoldenTicket 0.05
WebService::VaultPress::Partner::Request::History 0.05
WebService::VaultPress::Partner::Request::Usage 0.05
WebService::VaultPress::Partner::Response 0.05
WebService::Yahoo::BOSS::Result undef
WebService::Yahoo::BOSS::ResultSet undef
WebSource::Cache undef
WebSource::DB undef
WebSource::Envelope undef
WebSource::Extract undef
WebSource::Extract::form undef
WebSource::Extract::regexp undef
WebSource::Extract::xslt undef
WebSource::Fetcher undef
WebSource::Fetcher::xml undef
WebSource::File undef
WebSource::Filter undef
WebSource::Filter::distance undef
WebSource::Filter::script undef
WebSource::Filter::tests undef
WebSource::Filter::type undef
WebSource::Format undef
WebSource::Logger undef
WebSource::Map undef
WebSource::Module undef
WebSource::Parser undef
WebSource::Query undef
WebSource::Queue undef
WebSource::Soap undef
WebSource::XMLParser undef
WebSource::XMLSender undef
Webservice::InterMine::Constraint undef
Webservice::InterMine::Constraint::Binary undef
Webservice::InterMine::Constraint::Multi undef
Webservice::InterMine::Constraint::Role::Logical undef
Webservice::InterMine::Constraint::Role::Operator undef
Webservice::InterMine::Constraint::Role::Template undef
Webservice::InterMine::Constraint::Role::Templated undef
Webservice::InterMine::Constraint::SubClass undef
Webservice::InterMine::Constraint::Ternary undef
Webservice::InterMine::Constraint::Unary undef
Webservice::InterMine::ConstraintFactory undef
Webservice::InterMine::Join undef
Webservice::InterMine::LogicParser undef
Webservice::InterMine::LogicalOperator undef
Webservice::InterMine::LogicalSet undef
Webservice::InterMine::Path undef
Webservice::InterMine::PathDescription undef
Webservice::InterMine::PathFeature undef
Webservice::InterMine::Query undef
Webservice::InterMine::Query::Core undef
Webservice::InterMine::Query::Handler undef
Webservice::InterMine::Query::Roles::ExtendedQuery undef
Webservice::InterMine::Query::Roles::HTMLTable undef
Webservice::InterMine::Query::Roles::QueryUrl undef
Webservice::InterMine::Query::Roles::ReadInAble undef
Webservice::InterMine::Query::Roles::Runnable undef
Webservice::InterMine::Query::Roles::Saved undef
Webservice::InterMine::Query::Roles::ScriptAble undef
Webservice::InterMine::Query::Roles::Templated undef
Webservice::InterMine::Query::Roles::Uploadable undef
Webservice::InterMine::Query::Roles::WriteExcel undef
Webservice::InterMine::Query::Roles::WriteOutAble undef
Webservice::InterMine::Query::Roles::WriteOutLegacy undef
Webservice::InterMine::Query::Roles::WriteOutYaml undef
Webservice::InterMine::Query::Saved undef
Webservice::InterMine::Query::SavedHandler undef
Webservice::InterMine::Query::Scripted undef
Webservice::InterMine::Query::Template undef
Webservice::InterMine::Query::TemplateHandler undef
Webservice::InterMine::ResultIterator undef
Webservice::InterMine::ResultIterator::Role::HTMLTableRow undef
Webservice::InterMine::ResultObject undef
Webservice::InterMine::Role::Described undef
Webservice::InterMine::Role::Logical undef
Webservice::InterMine::Role::ModelOwner undef
Webservice::InterMine::Role::Named undef
Webservice::InterMine::Service undef
Webservice::InterMine::SortOrder undef
Webservice::InterMine::TemplateConstraintFactory undef
Webservice::InterMine::TemplateFactory undef
WiX3::Exceptions 0.009100
WiX3::Role::Traceable 0.009101
WiX3::Traceable 0.009100
WiX3::XML::Component 0.010002
WiX3::XML::ComponentRef 0.009100
WiX3::XML::CreateFolder 0.010
WiX3::XML::Custom 0.010
WiX3::XML::CustomAction 0.010
WiX3::XML::Directory 0.010002
WiX3::XML::DirectoryRef 0.010
WiX3::XML::Environment 0.009100
WiX3::XML::Feature 0.010
WiX3::XML::FeatureRef 0.010
WiX3::XML::File 0.010002
WiX3::XML::Fragment 0.009100
WiX3::XML::Fragment::CreateFolder 0.009100
WiX3::XML::Icon 0.009100
WiX3::XML::InstallExecuteSequence 0.010
WiX3::XML::Merge 0.009100
WiX3::XML::MergeRef 0.009100
WiX3::XML::Property 0.010002
WiX3::XML::RegistryKey 0.010002
WiX3::XML::RegistryValue 0.010002
WiX3::XML::RemoveFolder 0.009100
WiX3::XML::Role::Fragment 0.009100
WiX3::XML::Role::GeneratesGUID 0.009100
WiX3::XML::Role::InnerText 0.010002
WiX3::XML::Role::Tag 0.009102
WiX3::XML::Role::TagAllowsChildTags 0.010
WiX3::XML::Shortcut 0.009100
WiX3::XML::WixVariable 0.010
Wiki::Toolkit::Feed::Atom 0.02
Wiki::Toolkit::Feed::Listing undef
Wiki::Toolkit::Feed::RSS 0.11
Wiki::Toolkit::Formatter::Default 0.02
Wiki::Toolkit::Formatter::Multiple 0.02
Wiki::Toolkit::Formatter::WikiLinkFormatterParent 0.01
Wiki::Toolkit::Plugin 0.04
Wiki::Toolkit::Search::Base 0.01
Wiki::Toolkit::Search::DBIxFTS 0.05
Wiki::Toolkit::Search::Plucene 0.01
Wiki::Toolkit::Search::SII 0.09
Wiki::Toolkit::Setup::DBIxFTSMySQL 0.04
Wiki::Toolkit::Setup::Database 0.09
Wiki::Toolkit::Setup::MySQL 0.10
Wiki::Toolkit::Setup::Pg 0.10
Wiki::Toolkit::Setup::SII 0.03
Wiki::Toolkit::Setup::SQLite 0.10
Wiki::Toolkit::Store::Database 0.29
Wiki::Toolkit::Store::MySQL 0.04
Wiki::Toolkit::Store::Pg 0.06
Wiki::Toolkit::Store::SQLite 0.05
Wiki::Toolkit::TestConfig::Utilities 0.06
Wiki::Toolkit::TestLib 0.04
WordPress::API::Category undef
WordPress::API::MediaObject undef
WordPress::API::Page undef
WordPress::API::Post undef
WordPress::Base::Content 1.02
WordPress::Base::Data::Author undef
WordPress::Base::Data::Category undef
WordPress::Base::Data::MediaObject undef
WordPress::Base::Data::Object 1.05
WordPress::Base::Data::Page undef
WordPress::Base::Data::Post undef
WordPress::Base::Date 1.04
WordPress::Base::Object undef
WordPress::Base::Text undef
Workflow::Action 1.10
Workflow::Action::InputField 1.09
Workflow::Action::Mailer 1.01
Workflow::Action::Null 1.03
Workflow::Base 1.08
Workflow::Condition 1.07
Workflow::Condition::Evaluate 1.02
Workflow::Condition::HasUser 1.05
Workflow::Config 1.13
Workflow::Config::Perl 1.03
Workflow::Config::XML 1.05
Workflow::Context 1.05
Workflow::Exception 1.09
open 1.07
Workflow::Factory 1.20
Workflow::History 1.10
Workflow::Persister 1.10
Workflow::Persister::DBI 1.19
Workflow::Persister::DBI::AutoGeneratedId 1.06
Workflow::Persister::DBI::ExtraData 1.05
Workflow::Persister::DBI::SequenceId 1.05
Workflow::Persister::File 1.11
Workflow::Persister::RandomId 1.03
Workflow::Persister::SPOPS 1.07
Workflow::Persister::UUID 1.03
Workflow::State 1.15
Workflow::Validator 1.05
Workflow::Validator::HasRequiredField 1.05
Workflow::Validator::InEnumeratedType 1.04
Workflow::Validator::MatchesDateFormat 1.06
Katsubushi::Client
Wasm
App::plasm
FFI::C
Pcore
Getopt::EX::i18n
Code::ART
Wx::AUI 0.01
Wx::App undef
Wx::ArtProvider undef
Wx::Calendar 0.01
Wx::DND 0.01
Wx::DateTime 0.01
Wx::Demo::Source undef
Wx::DemoModules::lib::BaseModule undef
Wx::DemoModules::lib::DataObjects undef
Wx::DemoModules::lib::Utility undef
Wx::DemoModules::wxAUI undef
Wx::DemoModules::wxAboutDialog undef
Wx::DemoModules::wxArtProvider undef
Wx::DemoModules::wxBitmapButton undef
Wx::DemoModules::wxBookCtrl undef
Wx::DemoModules::wxBoxSizer undef
Wx::DemoModules::wxCalendarCtrl undef
Wx::DemoModules::wxCaret undef
Wx::DemoModules::wxCheckListBox undef
Wx::DemoModules::wxChoice undef
Wx::DemoModules::wxClipboard undef
Wx::DemoModules::wxCollapsiblePane undef
Wx::DemoModules::wxColourDialog undef
Wx::DemoModules::wxColourPickerCtrl undef
Wx::DemoModules::wxComboBox undef
Wx::DemoModules::wxComboCtrl undef
Wx::DemoModules::wxContextHelpButton undef
Wx::DemoModules::wxDND undef
Wx::DemoModules::wxDatePickerCtrl undef
Wx::DemoModules::wxDirDialog undef
Wx::DemoModules::wxDirPickerCtrl undef
Wx::DemoModules::wxDocView undef
Wx::DemoModules::wxFileDialog undef
Wx::DemoModules::wxFilePickerCtrl undef
Wx::DemoModules::wxFlexGridSizer undef
Wx::DemoModules::wxFontDialog undef
Wx::DemoModules::wxFontPickerCtrl undef
Wx::DemoModules::wxGauge undef
Wx::DemoModules::wxGraphicsContext undef
Wx::DemoModules::wxGrid undef
Wx::DemoModules::wxGridCER undef
Wx::DemoModules::wxGridER undef
Wx::DemoModules::wxGridSizer undef
Wx::DemoModules::wxGridTable undef
Wx::DemoModules::wxHVScrolledWindow undef
Wx::DemoModules::wxHeaderCtrl undef
Wx::DemoModules::wxHeaderCtrlSimple undef
Wx::DemoModules::wxHelpController undef
Wx::DemoModules::wxHelpEvent undef
Wx::DemoModules::wxHtmlDynamic undef
Wx::DemoModules::wxHtmlListBox undef
Wx::DemoModules::wxHtmlTag undef
Wx::DemoModules::wxHtmlWindow undef
Wx::DemoModules::wxHyperlinkCtrl undef
Wx::DemoModules::wxListBox undef
Wx::DemoModules::wxListCtrl undef
Wx::DemoModules::wxLocale undef
Wx::DemoModules::wxMDI undef
Wx::DemoModules::wxMediaCtrl undef
Wx::DemoModules::wxMultiChoiceDialog undef
Wx::DemoModules::wxOverlay undef
Wx::DemoModules::wxPasswordEntryDialog undef
Wx::DemoModules::wxPopupWindow undef
Wx::DemoModules::wxPrinting undef
Wx::DemoModules::wxProgressDialog undef
Wx::DemoModules::wxRadioBox undef
Wx::DemoModules::wxRadioButton undef
Wx::DemoModules::wxRichTextCtrl undef
Wx::DemoModules::wxScrollBar undef
Wx::DemoModules::wxScrolledWindow undef
Wx::DemoModules::wxSearchCtrl undef
Wx::DemoModules::wxSimpleHtmlListBox undef
Wx::DemoModules::wxSingleChoiceDialog undef
Wx::DemoModules::wxSlider undef
Wx::DemoModules::wxSpinButton undef
Wx::DemoModules::wxSpinCtrl undef
Wx::DemoModules::wxSpinCtrlDouble undef
Wx::DemoModules::wxSplashScreen undef
Wx::DemoModules::wxSplitterWindow undef
Wx::DemoModules::wxStaticBitmap undef
Wx::DemoModules::wxStaticText undef
Wx::DemoModules::wxTextEntryDialog undef
Wx::DemoModules::wxTipDialog undef
Wx::DemoModules::wxToolBar undef
Wx::DemoModules::wxTreeCtrl undef
Wx::DemoModules::wxTreeListCtrl 0.10
Wx::DemoModules::wxVListBox undef
Wx::DemoModules::wxVScrolledWindow undef
Wx::DemoModules::wxValidator undef
Wx::DemoModules::wxWizard undef
Wx::DemoModules::wxXrc undef
Wx::DemoModules::wxXrcCustom undef
Wx::DocView 0.01
Wx::DropSource undef
Wx::Event undef
Wx::FS 0.01
Wx::Grid 0.01
Wx::Help 0.01
Wx::Html 0.01
Wx::Locale undef
Wx::MDI 0.01
Wx::Media 0.01
Wx::Menu undef
Wx::Mini 0.98_01
Wx::Overload::Driver undef
Wx::Overload::Handle undef
Wx::Perl::Carp 0.01
Wx::Perl::DataWalker::CurrentLevel 0.01
Wx::Perl::DirTree::Linux 0.02
Wx::Perl::DirTree::Win32 0.02
Wx::Perl::Packager::Base 0.27
Wx::Perl::Packager::Linux 0.27
Wx::Perl::Packager::MSWin 0.27
Wx::Perl::Packager::MacOSX 0.27
Wx::Perl::Packager::Mini 0.27
Wx::Perl::SplashFast 0.02
Wx::Perl::TextValidator 0.01
Wx::Perl::VirtualDirSelector 1.008
Wx::Print 0.01
Wx::RadioBox undef
Wx::RichText 0.01
Wx::STC 0.01
Wx::Socket 0.01
Wx::Timer undef
Wx::XRC 0.01
Wx::build::MakeMaker::Any_OS undef
Wx::build::MakeMaker::Any_wx_config undef
Wx::build::MakeMaker::Core undef
Wx::build::MakeMaker::Hacks undef
Wx::build::MakeMaker::MacOSX_GCC undef
Wx::build::MakeMaker::Win32 undef
XML::Atom::Entry undef
Wx::build::MakeMaker::Win32_MSVC undef
Wx::build::MakeMaker::Win32_MinGW undef
XML::Atom::Feed undef
XML::Atom::Link undef
XML::Atom::Person undef
Wx::build::Options undef
Wx::build::Utils undef
X11::Terminal::GnomeTerminal 0.3
X11::Terminal::Konsole 0.3
X11::Terminal::XTerm 0.3
XBase 1.02
XBase::Base 1.02
XBase::Index 1.02
XBase::Memo 1.02
XBase::SDBM undef
XBase::SQL 0.233
XCAP::Client::Connection undef
XCAP::Client::Document undef
XML::Atom::Thing undef
XML::Atom::Util undef
XCAP::Client::Element undef
XML::Amazon::Collection undef
XML::Amazon::Item undef
XML::Ant::BuildFile::Element::Arg 0.214
XML::Ant::BuildFile::FileList 0.205
XML::Ant::BuildFile::Project 0.214
XML::Ant::BuildFile::Project::FileList 0.204
XML::Ant::BuildFile::Project::Target 0.204
XML::Ant::BuildFile::Resource 0.214
XML::Ant::BuildFile::Resource::FileList 0.214
XML::Ant::BuildFile::Resource::Path 0.214
XML::Ant::BuildFile::ResourceContainer 0.214
XML::Ant::BuildFile::Role::HasProjects 0.214
XML::Ant::BuildFile::Role::InProject 0.214
XML::Ant::BuildFile::Target 0.214
XML::Ant::BuildFile::Task 0.214
XML::Ant::BuildFile::Task::Concat 0.214
XML::Ant::BuildFile::Task::Copy 0.214
XML::Ant::BuildFile::Task::Java 0.214
XML::Ant::BuildFile::TaskContainer 0.214
XML::Ant::Properties 0.214
XML::Atom::Atompub undef
XML::Atom::Base undef
XML::Atom::Categories undef
XML::Atom::Category undef
XML::Atom::Client undef
XML::Atom::Collection undef
XML::Atom::Content undef
XML::Atom::ErrorHandler undef
XML::Atom::Server undef
XML::Atom::Syndication::Category undef
XML::Atom::Syndication::Content undef
XML::Atom::Syndication::Entry undef
XML::Atom::Syndication::Feed undef
XML::Atom::Syndication::Generator undef
XML::Atom::Syndication::Link undef
XML::Atom::Syndication::Object undef
XML::Atom::Syndication::Writer undef
XML::Atom::Syndication::Person undef
XML::Atom::Syndication::Source undef
XML::Atom::Syndication::Text undef
XML::Atom::Syndication::Thing undef
XML::Atom::Syndication::Util undef
XML::Atom::Workspace undef
XML::Catalogs v1.0.3
XML::Checker::Parser undef
XML::Compile::C14N::Util 0.10
XML::Compile::Iterator 1.21
XML::Compile::SOAP12::Client 2.02
XML::Compile::SOAP12::Operation 2.02
XML::Compile::SOAP12::Server 2.02
XML::Compile::SOAP12::Util 2.02
XML::Compile::SOAP::Client 2.23
XML::Compile::SOAP::Daemon::CGI 3.00
XML::Compile::SOAP::Daemon::LWPutil 3.00
XML::Compile::SOAP::Daemon::NetServer 3.00
XML::Compile::SOAP::Extension 2.23
XML::Compile::Schema 1.21
XML::Compile::SOAP::HTTPDaemon 3.00
XML::Compile::SOAP::Operation 2.23
XML::Compile::SOAP::Server 2.23
XML::Compile::SOAP::Trace 2.23
XML::Compile::SOAP::Util 2.23
XML::Compile::SOAP::WSA::Util 0.12
XML::Compile::SOAP::WSS 0.11
XML::Compile::Schema::BuiltInFacets 1.21
XML::Compile::Schema::BuiltInTypes 1.21
XML::Compile::Transport::SOAPHTTP 2.23
XML::Compile::Schema::Instance 1.21
XML::Compile::Schema::NameSpaces 1.21
XML::Compile::Schema::Specs 1.21
XML::Compile::Translate 1.21
XML::Compile::Translate::Reader 1.21
XML::Compile::Translate::Template 1.21
XML::Compile::Translate::Writer 1.21
XML::Compile::Transport 2.23
XML::Compile::Util 1.21
XML::Compile::WSS::Util 0.11
XML::Compile::XOP 2.23
XML::Compile::XOP::Include 2.23
XML::DOM::Lite::Constants undef
XML::DOM::Lite::Document undef
XML::DOM::Lite::Node undef
XML::DOM::Lite::NodeIterator undef
XML::DOM::Lite::NodeList undef
XML::DOM::Lite::Parser undef
XML::DOM::Lite::Serializer undef
XML::DOM::Lite::XPath undef
XML::DOM::Lite::XSLT undef
XML::DOM::PerlSAX undef
XML::DOM::ValParser undef
XML::DTD::AttDef 0.09
XML::Feed::Entry undef
XML::DTD::AttList 0.09
XML::DTD::Automaton 0.09
XML::DTD::Comment 0.09
XML::DTD::Component 0.09
XML::DTD::ContentModel 0.10
XML::DTD::Element 0.09
XML::DTD::Entity 0.09
XML::DTD::EntityManager 0.09
XML::DTD::Error 0.09
XML::DTD::FAState 0.09
XML::DTD::Ignore 0.09
XML::DTD::Include 0.09
XML::DTD::Notation 0.09
XML::DTD::PERef 0.09
XML::DTD::PI 0.09
XML::DTD::Parser 0.09
XML::DTD::Text 0.09
XML::ESISParser 0.08
XML::Easy::Content 0.008
XML::Easy::Element 0.008
XML::Easy::Text 0.008
XML::Element 3.09
XML::Elemental::Characters undef
XML::Elemental::Document undef
XML::Elemental::Element undef
XML::Elemental::Node undef
XML::Elemental::SAXHandler 0.11
XML::Elemental::Util undef
XML::Entities::Data undef
XML::ExtOn::Attributes undef
XML::ExtOn::Context undef
XML::ExtOn::Element undef
XML::ExtOn::IncXML undef
XML::ExtOn::SAX12ExtOn undef
XML::ExtOn::TieAttrs 0.01
XML::ExtOn::TieAttrsName undef
XML::ExtOn::Writer undef
XML::Feed::Aggregator::Deduper 0.0400
XML::Feed::Aggregator::Sort 0.0400
XML::Feed::Content undef
XML::Feed::Deduper::DB_File undef
XML::Feed::Deduper::Role undef
XML::Feed::Format::Atom undef
XML::Feed::Format::RSS undef
XML::Filter::Distributor 0.1
XML::Filter::DocSplitter 0.2
XML::Filter::Merger 0.2
XML::Filter::Moose undef
XML::Filter::SAX2toSAX1 0.03
XML::Filter::Tee 0.1
XML::Generator::DOM 0.2
XML::Generator::Moose undef
XML::Grammar::Fiction 0.1.7
XML::Grammar::Fiction::App::FromProto 0.1.7
XML::Grammar::Fiction::App::ToDocBook 0.1.7
XML::Grammar::Fiction::App::ToHTML 0.1.7
XML::Grammar::Fiction::Base 0.1.7
XML::Grammar::Fiction::Err 0.1.7
XML::Grammar::Fiction::Event undef
XML::Grammar::Fiction::FromProto 0.1.7
XML::Grammar::Fiction::FromProto::Nodes 0.1.7
XML::Grammar::Fiction::FromProto::Parser 0.1.7
XML::Grammar::Fiction::FromProto::Parser::LineIterator 0.1.7
XML::Grammar::Fiction::FromProto::Parser::QnD 0.1.7
XML::Grammar::Fiction::FromProto::Parser::XmlIterator 0.1.7
XML::Grammar::Fiction::RNG_Renderer undef
XML::Grammar::Fiction::RendererBase 0.1.7
XML::Grammar::Fiction::Struct::Tag 0.1.7
XML::Grammar::Fiction::ToDocBook 0.1.7
XML::Grammar::Fiction::ToHTML 0.1.7
XML::Grammar::FictionBase::TagsTree2XML undef
XML::Grammar::Fortune::Synd::App undef
XML::Grammar::Fortune::Synd::Heap::Elem undef
XML::Grammar::Fortune::ToText 0.0500
XML::Grammar::Screenplay::App::FromProto undef
XML::Grammar::Screenplay::App::ToDocBook undef
XML::Grammar::Screenplay::App::ToHTML undef
XML::Grammar::Screenplay::Base undef
XML::Grammar::Screenplay::FromProto::Parser undef
XML::Grammar::Screenplay::FromProto::Parser::QnD undef
XML::Handler::BuildDOM undef
XML::Handler::CanonXMLWriter 0.08
XML::Handler::Sample undef
XML::Handler::Subs 0.08
XML::Handler::XMLWriter 0.08
XML::LibXML::Boolean 1.70
XML::LibXML::Common 1.70
XML::LibXML::ErrNo 1.70
XML::LibXML::Error 1.70
XML::LibXML::Literal 1.70
XML::LibXML::NodeList 1.70
XML::LibXML::NodeList::Iterator 1.03
XML::LibXML::Reader 1.70
XML::LibXML::SAX 1.70
XML::LibXML::Number 1.70
XML::LibXML::SAX::Builder 1.70
XML::LibXML::SAX::Generator 1.70
XML::LibXML::XPathContext 1.70
XML::LibXML::SAX::Parser 1.70
XML::LibXSLT::Easy::Batch undef
XML::LibXSLT::Easy::Batch::CLI undef
XML::LibXSLT::Easy::CLI undef
XML::Normalize::LibXML 0.2
XML::Output 0.09
XML::Parser::LiteCopy 0.720.00
XML::Parser::Style::Debug undef
XML::Parser::Style::ETree 0.09
XML::Parser::Style::Elemental 0.72
XML::Parser::Style::Objects undef
XML::Parser::Style::Stream undef
XML::Parser::Style::Subs undef
XML::Parser::Style::Tree undef
XML::PatAct::Amsterdam 0.08
XML::PatAct::MatchName 0.08
XML::PatAct::ToObjects 0.08
XML::Perl2SAX 0.08
XML::RPC::UA::Curl undef
XML::RSS::Headline 2.32
XML::RSS::Headline::Fark 2.32
XML::RSS::Headline::PerlJobs 2.32
XML::RSS::Headline::UsePerlJournals 2.32
XML::RSS::LibXML::ImplBase undef
XML::RSS::LibXML::MagicElement 0.02
XML::RSS::LibXML::Namespaces undef
XML::RSS::LibXML::Null undef
XML::RSS::LibXML::V0_9 undef
XML::RSS::LibXML::V0_91 undef
XML::RSS::LibXML::V0_92 undef
XML::RSS::LibXML::V1_0 undef
XML::RSS::LibXML::V2_0 undef
XML::RSS::Parser::Characters undef
XML::RSS::Parser::Feed 1.0
XML::RSS::Parser::Lite 0.10
XML::RSS::Parser::Util undef
XML::RSS::Private::Output::Base undef
XML::RSS::Private::Output::Roles::ImageDims undef
XML::RSS::Private::Output::Roles::ModulesElems undef
XML::RSS::Private::Output::V0_9 undef
XML::RSS::Private::Output::V0_91 undef
XML::RSS::Private::Output::V1_0 undef
XML::Rabbit::Node 0.0.4
XML::RSS::Private::Output::V2_0 undef
XML::Rabbit::Role::Document 0.0.4
XML::Rabbit::RootNode 0.0.4
XML::Rabbit::Role::Node 0.0.4
XML::Rabbit::Trait::XPath 0.0.4
XML::Rabbit::Trait::XPathObject 0.0.4
XML::Rabbit::Trait::XPathObjectList 0.0.4
XML::Rabbit::Trait::XPathObjectMap 0.0.4
XML::Rabbit::Trait::XPathValue 0.0.4
XML::Rabbit::Trait::XPathValueList 0.0.4
XML::Rabbit::Trait::XPathValueMap 0.0.4
XML::RelaxNG::Compact::DataModel 0.15
XML::SAX2Perl 0.08
XML::SAX::ByRecord 0.1
XML::SAX::DocumentLocator undef
XML::SAX::EventMethodMaker 0.1
XML::SAX::ExpatXS::Encoding undef
XML::SAX::Machine 1
XML::SAX::Machines::ConfigDefaults undef
XML::SAX::Machines::ConfigHelper undef
XML::SAX::Manifold 0.1
XML::SAX::Pipeline 0.1
XML::SAX::PurePerl 0.96
XML::SAX::PurePerl::DebugHandler undef
XML::SAX::PurePerl::Exception undef
XML::SAX::PurePerl::Productions undef
XML::SAX::PurePerl::Reader undef
XML::SAX::PurePerl::Reader::Stream undef
XML::SAX::PurePerl::Reader::String undef
XML::SAX::PurePerl::Reader::URI undef
XML::SAX::Tap 0.1
XML::SAX::Writer::XML undef
XML::Schematron::LibXSLT undef
XML::Schematron::Schema undef
XML::Schematron::SchemaReader undef
XML::Schematron::Test undef
XML::Schematron::XPath undef
XML::Schematron::XSLTProcessor undef
XML::SemanticDiff::BasicHandler 0.94
XML::SimpleObject::Enhanced 0.53
XML::Smart::Base64 1.0
XML::Smart::DTD 0.01
XML::Smart::Data 0.01
XML::Smart::Entity 0.01
XML::Smart::HTMLParser 1.0
XML::Smart::Parser 1.2
XML::Smart::Tie undef
XML::Smart::Tree 1.0
XML::Smart::XPath 0.01
XML::Stream::IO::Select::Win32 1.23_04
XML::Stream::Namespace 1.23_04
XML::Stream::Node 1.23_04
XML::Stream::Parser 1.23_04
XML::Stream::Parser::DTD 1.23_04
XML::Stream::Tree 1.23_04
XML::Stream::XPath 1.23_04
XML::Stream::XPath::Op 1.23_04
XML::Stream::XPath::Query 1.23_04
XML::Stream::XPath::Value 1.23_04
XML::TMX::FromPO undef
XML::TMX::Query 0.1
XML::TMX::Reader 0.22
XML::TMX::Writer 0.23
XML::Tags undef
XML::TinyXML::Node 0.30
XML::TinyXML::NodeAttribute 0.30
XML::TinyXML::Selector 0.30
XML::TinyXML::Selector::XPath 0.30
XML::TinyXML::Selector::XPath::Axes 0.30
XML::TinyXML::Selector::XPath::Context 0.30
XML::TinyXML::Selector::XPath::Functions 0.30
XML::Toolkit::App 0.02
XML::Toolkit::Builder undef
XML::Toolkit::Builder::ClassRegistry undef
XML::Toolkit::Builder::ClassTemplate undef
XML::Toolkit::Builder::Filter undef
XML::Toolkit::Builder::FilterNS undef
XML::Toolkit::Builder::NamespaceRegistry undef
XML::Toolkit::Cmd undef
XML::Toolkit::Cmd::ClassTemplate undef
XML::Toolkit::Cmd::Command::generate undef
XML::Toolkit::Cmd::Command::validate undef
XML::Toolkit::Config undef
XML::Toolkit::Config::Container undef
XML::Toolkit::Generator undef
XML::Toolkit::Generator::Default undef
XML::Toolkit::Generator::Interface undef
XML::Toolkit::Loader undef
XML::Toolkit::Loader::Filter undef
XML::Toolkit::Loader::FilterNS undef
XML::Toolkit::MetaDescription undef
XML::Toolkit::MetaDescription::Attribute undef
XML::Toolkit::MetaDescription::Trait undef
XML::Toolkit::Trait::NoXML undef
XML::TreePuller::Constants 0.1.7
XML::TreePuller::Element 0.1.0
XML::Twig::XPath 0.02
XML::Validate::Base 1.009
XML::Validate::MSXML 1.018
XML::Validate::Xerces 1.021
XML::Validator::Schema::Attribute undef
XML::Validator::Schema::AttributeLibrary undef
XML::Validator::Schema::AttributeNode undef
XML::Validator::Schema::ComplexTypeNode undef
XML::Validator::Schema::ElementLibrary undef
XML::Validator::Schema::ElementNode undef
XML::Validator::Schema::ElementRefNode undef
XML::Validator::Schema::Library undef
XML::Validator::Schema::ModelNode undef
XML::Validator::Schema::Node undef
XML::Validator::Schema::Parser undef
XML::Validator::Schema::RootNode undef
XML::Validator::Schema::SimpleType undef
XML::Validator::Schema::SimpleTypeNode undef
XML::Validator::Schema::TypeLibrary undef
XML::Validator::Schema::Util undef
XML::XPath::Boolean undef
XML::XPath::Builder undef
XML::XPath::Expr undef
XML::XPath::Function undef
XML::XPath::Literal undef
XML::XPath::LocationPath undef
XML::XPath::Node undef
XML::XPath::Node::Attribute undef
XML::XPath::Node::Comment undef
XML::XPath::Node::Element undef
XML::XPath::Node::Namespace undef
XML::XPath::Node::PI undef
XML::XPath::Node::Text undef
XML::XPath::NodeSet undef
XML::XPath::Number undef
XML::XPath::Parser undef
XML::XPath::PerlSAX undef
XML::XPath::Root undef
XML::XPath::Step undef
XML::XPath::Variable undef
XML::XPath::XMLParser undef
XML::XPathEngine::Boolean undef
XML::XPathEngine::Expr undef
XML::XPathEngine::Function undef
XML::XPathEngine::Literal undef
XML::XPathEngine::LocationPath undef
XML::XPathEngine::NodeSet undef
XML::XPathEngine::Number undef
XML::XPathEngine::Root undef
XML::XPathEngine::Step undef
XML::XPathEngine::Variable undef
XML::XPathScript::Processor::B 1.54
XML::XPathScript::Processor::LibXML 1.54
XML::XPathScript::Processor::XPath 1.54
XML::XPathScript::Stylesheet::DocBook2LaTeX 1.54
XML::XPathScript::Template 1.54
XML::XPathScript::Template::Tag 1.54
XML::XSPF::Base undef
XML::XSPF::Track undef
XML::XSS::Comment 0.3.1
XML::XSS::Document 0.3.1
XML::XSS::Element 0.3.1
XML::XSS::ProcessingInstruction 0.3.1
XML::XSS::Role::Renderer 0.3.1
XML::XSS::Role::StyleAttribute 0.3.1
XML::XSS::StyleAttribute 0.3.1
XML::XSS::Stylesheet::HTML2TD 0.3.1
XML::XSS::Template 0.3.1
XML::XSS::Text 0.3.1
XMLRPC::Test 0.712
XMLRPC::Transport::HTTP 0.712
XMLRPC::Transport::POP3 0.712
XMLRPC::Transport::TCP 0.712
XS::Object::Magic::Install::Files undef
XUL::Gui::Manual undef
XUL::Gui::Tutorial undef
Xtract::LZMA 0.13
Xtract::Publish 0.13
Xtract::Scan 0.13
Xtract::Scan::SQLite 0.13
YAML::Any 0.72
Xtract::Scan::mysql 0.13
Xtract::Table 0.13
YAML::Dumper 0.72
YAML::Dumper::Base 0.72
YAML::Dumper::Syck undef
YAML::Error 0.72
YAML::Loader 0.72
YAML::Marshall 0.72
YAML::Loader::Base 0.72
YAML::Loader::Syck undef
YAML::Node 0.72
YAML::Perl::Base undef
YAML::Perl::Composer undef
YAML::Perl::Constructor undef
YAML::Perl::Dumper undef
YAML::Perl::Emitter undef
YAML::Perl::Error undef
YAML::Perl::Events undef
YAML::Perl::Loader undef
YAML::Perl::Nodes undef
YAML::Perl::Parser undef
YAML::Perl::Processor undef
YAML::Perl::Reader undef
YAML::Perl::Representer undef
YAML::Perl::Resolver undef
YAML::Perl::Scanner undef
YAML::Perl::Serializer undef
YAML::Perl::Stream undef
YAML::Perl::Tokens undef
YAML::Perl::Writer undef
YAML::Tag 0.72
YAML::Types 0.72
YAML::XS::LibYAML undef
YAPE::Regex::Element 4.00
YUI::Loader::Cache undef
YUI::Loader::Cache::Dir undef
YUI::Loader::Cache::URI undef
YUI::Loader::Carp undef
YUI::Loader::Catalog undef
YUI::Loader::Entry undef
YUI::Loader::IncludeExclude undef
YUI::Loader::Item undef
YUI::Loader::List undef
YUI::Loader::Manifest undef
YUI::Loader::Source undef
YUI::Loader::Source::Dir undef
YUI::Loader::Source::Internet undef
YUI::Loader::Source::URI undef
YUI::Loader::Source::YUIDir undef
YUI::Loader::Source::YUIHost undef
Yahoo::Marketing::APT::Account undef
Yahoo::Marketing::APT::AccountDescriptor undef
Yahoo::Marketing::APT::AccountResponse undef
Yahoo::Marketing::APT::AccountService undef
Yahoo::Marketing::APT::Ad undef
Yahoo::Marketing::APT::AdAttributes undef
Yahoo::Marketing::APT::AdCopyResponse undef
Yahoo::Marketing::APT::AdDeliveryMode undef
Yahoo::Marketing::APT::AdDeliveryModeDescriptor undef
Yahoo::Marketing::APT::AdDeliveryModeResponse undef
Yahoo::Marketing::APT::AdDeliveryModeService undef
Yahoo::Marketing::APT::AdGroup undef
Yahoo::Marketing::APT::AdGroupNonGuaranteedSettings undef
Yahoo::Marketing::APT::AdGroupResponse undef
Yahoo::Marketing::APT::AdLinkingSettings undef
Yahoo::Marketing::APT::AdMarker undef
Yahoo::Marketing::APT::AdMarkerResponse undef
Yahoo::Marketing::APT::AdMarkerService undef
Yahoo::Marketing::APT::AdMarkerSettings undef
Yahoo::Marketing::APT::AdOperationsTeamMember undef
Yahoo::Marketing::APT::AdResponse undef
Yahoo::Marketing::APT::AdService undef
Yahoo::Marketing::APT::AdTagParameters undef
Yahoo::Marketing::APT::AdTagService undef
Yahoo::Marketing::APT::Address undef
Yahoo::Marketing::APT::AdjustmentCertifiedFeed undef
Yahoo::Marketing::APT::AdjustmentInboundFeed undef
Yahoo::Marketing::APT::AdjustmentInboundFeedAddRequest undef
Yahoo::Marketing::APT::AdjustmentInboundFeedAddResponse undef
Yahoo::Marketing::APT::AdjustmentOrder undef
Yahoo::Marketing::APT::AdjustmentOrderRequest undef
Yahoo::Marketing::APT::AdjustmentPlacement undef
Yahoo::Marketing::APT::AdjustmentPlacementRequest undef
Yahoo::Marketing::APT::AdjustmentService undef
Yahoo::Marketing::APT::AgencyContract undef
Yahoo::Marketing::APT::AlternateImage undef
Yahoo::Marketing::APT::ApiFault undef
Yahoo::Marketing::APT::Approval undef
Yahoo::Marketing::APT::ApprovalAction undef
Yahoo::Marketing::APT::ApprovalDetail undef
Yahoo::Marketing::APT::ApprovalObject undef
Yahoo::Marketing::APT::ApprovalService undef
Yahoo::Marketing::APT::ApprovalTask undef
Yahoo::Marketing::APT::ApprovalTaskComment undef
Yahoo::Marketing::APT::ApprovalTaskService undef
Yahoo::Marketing::APT::ApprovalWorkflow undef
Yahoo::Marketing::APT::ApprovalWorkflowResponse undef
Yahoo::Marketing::APT::ApprovalWorkflowService undef
Yahoo::Marketing::APT::Approver undef
Yahoo::Marketing::APT::AttributeSetting undef
Yahoo::Marketing::APT::AudienceSegmentCategory undef
Yahoo::Marketing::APT::AudienceSegmentCategoryResponse undef
Yahoo::Marketing::APT::AudienceSegmentCategoryService undef
Yahoo::Marketing::APT::AudienceSegmentDescriptor undef
Yahoo::Marketing::APT::AudienceSharingRule undef
Yahoo::Marketing::APT::AudienceSharingRuleResponse undef
Yahoo::Marketing::APT::AudienceSharingRulesService undef
Yahoo::Marketing::APT::BaseRate undef
Yahoo::Marketing::APT::BaseRateResponse undef
Yahoo::Marketing::APT::BasicResponse undef
Yahoo::Marketing::APT::BidDescriptor undef
Yahoo::Marketing::APT::BillingProfile undef
Yahoo::Marketing::APT::BillingTermsValue undef
Yahoo::Marketing::APT::BlockingImpact undef
Yahoo::Marketing::APT::BookingLimit undef
Yahoo::Marketing::APT::BookingLimitResponse undef
Yahoo::Marketing::APT::BumperVideoActOne undef
Yahoo::Marketing::APT::BusinessProfile undef
Yahoo::Marketing::APT::BuyerProfile undef
Yahoo::Marketing::APT::Capability undef
Yahoo::Marketing::APT::ClickThroughURLSettings undef
Yahoo::Marketing::APT::ClickableVideoActOne undef
Yahoo::Marketing::APT::ClickableVideoActTwo undef
Yahoo::Marketing::APT::CommonTargetingService undef
Yahoo::Marketing::APT::CompanionAdSetting undef
Yahoo::Marketing::APT::CompanyLink undef
Yahoo::Marketing::APT::Complaint undef
Yahoo::Marketing::APT::ComplaintReason undef
Yahoo::Marketing::APT::ComplaintResolution undef
Yahoo::Marketing::APT::ComplaintResponse undef
Yahoo::Marketing::APT::CompositeURL undef
Yahoo::Marketing::APT::ConditionalFilter undef
Yahoo::Marketing::APT::ConditionalFilterResponse undef
Yahoo::Marketing::APT::Contact undef
Yahoo::Marketing::APT::ContactResponse undef
Yahoo::Marketing::APT::ContactService undef
Yahoo::Marketing::APT::ContentTargetingAttributes undef
Yahoo::Marketing::APT::ContentTopic undef
Yahoo::Marketing::APT::CopyAdGroupRequest undef
Yahoo::Marketing::APT::CostFeed undef
Yahoo::Marketing::APT::CostFeedService undef
Yahoo::Marketing::APT::CustomContentCategory undef
Yahoo::Marketing::APT::CustomContentCategoryDescriptor undef
Yahoo::Marketing::APT::CustomContentCategoryResponse undef
Yahoo::Marketing::APT::CustomGeoArea undef
Yahoo::Marketing::APT::CustomGeoAreaDescriptor undef
Yahoo::Marketing::APT::CustomGeoAreaResponse undef
Yahoo::Marketing::APT::CustomGeoAreaService undef
Yahoo::Marketing::APT::CustomSection undef
Yahoo::Marketing::APT::CustomSectionDescriptor undef
Yahoo::Marketing::APT::CustomSectionResponse undef
Yahoo::Marketing::APT::CustomSegment undef
Yahoo::Marketing::APT::CustomSegmentResponse undef
Yahoo::Marketing::APT::CustomSegmentService undef
Yahoo::Marketing::APT::CustomizableFlashCreative undef
Yahoo::Marketing::APT::CustomizableFlashCreativeResponse undef
Yahoo::Marketing::APT::CustomizableFlashCreativeService undef
Yahoo::Marketing::APT::CustomizableFlashTemplate undef
Yahoo::Marketing::APT::CustomizableFlashTemplateResponse undef
Yahoo::Marketing::APT::CustomizableFlashTemplateService undef
Yahoo::Marketing::APT::DateRange undef
Yahoo::Marketing::APT::DayPartingTargeting undef
Yahoo::Marketing::APT::DealParticipantDetails undef
Yahoo::Marketing::APT::DefaultBaseRate undef
Yahoo::Marketing::APT::DefaultBaseRateResponse undef
Yahoo::Marketing::APT::DeliveryFormat undef
Yahoo::Marketing::APT::DeliveryGoal undef
Yahoo::Marketing::APT::DeliveryStats undef
Yahoo::Marketing::APT::Discount undef
Yahoo::Marketing::APT::EditorialService undef
Yahoo::Marketing::APT::EnhancedBasicResponse undef
Yahoo::Marketing::APT::Error undef
Yahoo::Marketing::APT::FilterService undef
Yahoo::Marketing::APT::FlashCreative undef
Yahoo::Marketing::APT::FlashCreativeResponse undef
Yahoo::Marketing::APT::FlashCreativeService undef
Yahoo::Marketing::APT::Folder undef
Yahoo::Marketing::APT::FolderItem undef
Yahoo::Marketing::APT::FolderResponse undef
Yahoo::Marketing::APT::FolderService undef
Yahoo::Marketing::APT::GeographicalTargetingService undef
Yahoo::Marketing::APT::GuaranteedDeal undef
Yahoo::Marketing::APT::GuaranteedDealApproval undef
Yahoo::Marketing::APT::ImageCreative undef
Yahoo::Marketing::APT::ImageCreativeResponse undef
Yahoo::Marketing::APT::ImageCreativeService undef
Yahoo::Marketing::APT::ImageFlashVariable undef
Yahoo::Marketing::APT::ImageFlashVariableConstraints undef
Yahoo::Marketing::APT::ImageFlashVariableValue undef
Yahoo::Marketing::APT::InteractivePharmaceuticalVideoActFour undef
Yahoo::Marketing::APT::InteractivePharmaceuticalVideoActOne undef
Yahoo::Marketing::APT::InteractivePharmaceuticalVideoActThree undef
Yahoo::Marketing::APT::InteractivePharmaceuticalVideoActTwo undef
Yahoo::Marketing::APT::Inventory undef
Yahoo::Marketing::APT::InventoryIdentifier undef
Yahoo::Marketing::APT::InventoryIdentifierResponse undef
Yahoo::Marketing::APT::InventoryIdentifierService undef
Yahoo::Marketing::APT::InventoryOptIn undef
Yahoo::Marketing::APT::InventoryOptInResponse undef
Yahoo::Marketing::APT::InventorySearchFilter undef
Yahoo::Marketing::APT::InventorySearchService undef
Yahoo::Marketing::APT::LibraryAdComment undef
Yahoo::Marketing::APT::LibraryAdCommentResponse undef
Yahoo::Marketing::APT::LibraryAdInformationService undef
Yahoo::Marketing::APT::LibraryBumperVideoAd undef
Yahoo::Marketing::APT::LibraryBumperVideoAdResponse undef
Yahoo::Marketing::APT::LibraryBumperVideoAdService undef
Yahoo::Marketing::APT::LibraryClickToSiteOverlayVideoAd undef
Yahoo::Marketing::APT::LibraryClickToSiteOverlayVideoAdResponse undef
Yahoo::Marketing::APT::LibraryClickToSiteOverlayVideoAdService undef
Yahoo::Marketing::APT::LibraryClickToVideoOverlayVideoAd undef
Yahoo::Marketing::APT::LibraryClickToVideoOverlayVideoAdResponse undef
Yahoo::Marketing::APT::LibraryClickToVideoOverlayVideoAdService undef
Yahoo::Marketing::APT::LibraryClickableVideoAd undef
Yahoo::Marketing::APT::LibraryClickableVideoAdResponse undef
Yahoo::Marketing::APT::LibraryClickableVideoAdService undef
Yahoo::Marketing::APT::LibraryCustomHTMLAd undef
Yahoo::Marketing::APT::LibraryCustomHTMLAdResponse undef
Yahoo::Marketing::APT::LibraryCustomHTMLAdService undef
Yahoo::Marketing::APT::LibraryCustomizableFlashAd undef
Yahoo::Marketing::APT::LibraryCustomizableFlashAdResponse undef
Yahoo::Marketing::APT::LibraryCustomizableFlashAdService undef
Yahoo::Marketing::APT::LibraryFlashAd undef
Yahoo::Marketing::APT::LibraryFlashAdResponse undef
Yahoo::Marketing::APT::LibraryFlashAdService undef
Yahoo::Marketing::APT::LibraryImageAd undef
Yahoo::Marketing::APT::LibraryImageAdResponse undef
Yahoo::Marketing::APT::LibraryImageAdService undef
Yahoo::Marketing::APT::LibraryInteractivePharmaceuticalVideoAd undef
Yahoo::Marketing::APT::LibraryInteractivePharmaceuticalVideoAdResponse undef
Yahoo::Marketing::APT::LibraryInteractivePharmaceuticalVideoAdService undef
Yahoo::Marketing::APT::LibraryThirdPartyAd undef
Yahoo::Marketing::APT::LibraryThirdPartyAdResponse undef
Yahoo::Marketing::APT::LibraryThirdPartyAdService undef
Yahoo::Marketing::APT::Link undef
Yahoo::Marketing::APT::LinkApproval undef
Yahoo::Marketing::APT::LinkContact undef
Yahoo::Marketing::APT::LinkParticipantDetails undef
Yahoo::Marketing::APT::LinkResponse undef
Yahoo::Marketing::APT::LinkingProfileService undef
Yahoo::Marketing::APT::LinkingService undef
Yahoo::Marketing::APT::LinkingSettings undef
Yahoo::Marketing::APT::LocationService undef
Yahoo::Marketing::APT::MakeGoodOrderCredit undef
Yahoo::Marketing::APT::MakeGoodOrderCreditResponse undef
Yahoo::Marketing::APT::MonthOfYear undef
Yahoo::Marketing::APT::NonGuaranteedDeal undef
Yahoo::Marketing::APT::NonGuaranteedDealApproval undef
Yahoo::Marketing::APT::Order undef
Yahoo::Marketing::APT::OrderAgency undef
Yahoo::Marketing::APT::OrderContact undef
Yahoo::Marketing::APT::OrderCredit undef
Yahoo::Marketing::APT::OrderCreditResponse undef
Yahoo::Marketing::APT::OrderDiscount undef
Yahoo::Marketing::APT::OrderFee undef
Yahoo::Marketing::APT::OrderFeeResponse undef
Yahoo::Marketing::APT::OrderResponse undef
Yahoo::Marketing::APT::OrderService undef
Yahoo::Marketing::APT::OverlayVideoActOne undef
Yahoo::Marketing::APT::OverlayVideoActTwo undef
Yahoo::Marketing::APT::Palette undef
Yahoo::Marketing::APT::PaletteResponse undef
Yahoo::Marketing::APT::PaletteService undef
Yahoo::Marketing::APT::PiggybackPixel undef
Yahoo::Marketing::APT::Pixel undef
Yahoo::Marketing::APT::PixelFrequency undef
Yahoo::Marketing::APT::PixelResponse undef
Yahoo::Marketing::APT::PixelService undef
Yahoo::Marketing::APT::Placement undef
Yahoo::Marketing::APT::PlacementAdAttributes undef
Yahoo::Marketing::APT::PlacementAudienceTargetingAttributes undef
Yahoo::Marketing::APT::PlacementContentTargetingAttributes undef
Yahoo::Marketing::APT::PlacementDetail undef
Yahoo::Marketing::APT::PlacementEditEndDate undef
Yahoo::Marketing::APT::PlacementEditImpression undef
Yahoo::Marketing::APT::PlacementEditPrice undef
Yahoo::Marketing::APT::PlacementEditSettings undef
Yahoo::Marketing::APT::PlacementFrequencyTargetingAttributes undef
Yahoo::Marketing::APT::PlacementGuaranteedPriceSettings undef
Yahoo::Marketing::APT::PlacementGuaranteedSettings undef
Yahoo::Marketing::APT::PlacementNonGuaranteedPriceSettings undef
Yahoo::Marketing::APT::PlacementNonGuaranteedSettings undef
Yahoo::Marketing::APT::PlacementResponse undef
Yahoo::Marketing::APT::PlacementService undef
Yahoo::Marketing::APT::PlacementTarget undef
Yahoo::Marketing::APT::PlacementTargetDescriptor undef
Yahoo::Marketing::APT::PlacementTargetingDetails undef
Yahoo::Marketing::APT::PlacementTransferResponse undef
Yahoo::Marketing::APT::Privilege undef
Yahoo::Marketing::APT::ProfileContact undef
Yahoo::Marketing::APT::PublisherSelector undef
Yahoo::Marketing::APT::ROITarget undef
Yahoo::Marketing::APT::RateAdjustment undef
Yahoo::Marketing::APT::RateAdjustmentResponse undef
Yahoo::Marketing::APT::RateCard undef
Yahoo::Marketing::APT::RateCardResponse undef
Yahoo::Marketing::APT::RateCardService undef
Yahoo::Marketing::APT::ReconciliationComment undef
Yahoo::Marketing::APT::ReconciliationRule undef
Yahoo::Marketing::APT::ReferralSegment undef
Yahoo::Marketing::APT::ReferralSegmentResponse undef
Yahoo::Marketing::APT::ReferralSegmentService undef
Yahoo::Marketing::APT::Region undef
Yahoo::Marketing::APT::RegionLevel undef
Yahoo::Marketing::APT::RegionProbability undef
Yahoo::Marketing::APT::Report undef
Yahoo::Marketing::APT::ReportColumn undef
Yahoo::Marketing::APT::ReportInfo undef
Yahoo::Marketing::APT::ReportRequest undef
Yahoo::Marketing::APT::ReportService undef
Yahoo::Marketing::APT::ReportStatus undef
Yahoo::Marketing::APT::ReportingTag undef
Yahoo::Marketing::APT::ReportingTagResponse undef
Yahoo::Marketing::APT::ReportingTagService undef
Yahoo::Marketing::APT::Role undef
Yahoo::Marketing::APT::RoleResponse undef
Yahoo::Marketing::APT::RoleService undef
Yahoo::Marketing::APT::SalesTeamMember undef
Yahoo::Marketing::APT::ScheduledReport undef
Yahoo::Marketing::APT::ScheduledReportResponse undef
Yahoo::Marketing::APT::SellerProfile undef
Yahoo::Marketing::APT::SellingRule undef
Yahoo::Marketing::APT::SellingRuleResponse undef
Yahoo::Marketing::APT::SellingRulesService undef
Yahoo::Marketing::APT::Service undef
Yahoo::Marketing::APT::Site undef
Yahoo::Marketing::APT::SiteAccess undef
Yahoo::Marketing::APT::SiteAccessResponse undef
Yahoo::Marketing::APT::SiteClassificationService undef
Yahoo::Marketing::APT::SiteDescriptor undef
Yahoo::Marketing::APT::SiteResponse undef
Yahoo::Marketing::APT::SiteService undef
Yahoo::Marketing::APT::SiteStructure undef
Yahoo::Marketing::APT::SiteStructureSettings undef
Yahoo::Marketing::APT::SourceOwner undef
Yahoo::Marketing::APT::Tag undef
Yahoo::Marketing::APT::TargetingAttribute undef
Yahoo::Marketing::APT::TargetingAttributeDescriptor undef
Yahoo::Marketing::APT::TargetingAttributeDescriptorWithAny undef
Yahoo::Marketing::APT::TargetingAttributeDetails undef
Yahoo::Marketing::APT::TargetingAttributeValue undef
Yahoo::Marketing::APT::TargetingAttributeValueRequest undef
Yahoo::Marketing::APT::TargetingDictionaryService undef
Yahoo::Marketing::APT::TargetingProfile undef
Yahoo::Marketing::APT::TargetingProfileResponse undef
Yahoo::Marketing::APT::TargetingProfileService undef
Yahoo::Marketing::APT::TargetingRule undef
Yahoo::Marketing::APT::TextFlashVariable undef
Yahoo::Marketing::APT::TextFlashVariableConstraints undef
Yahoo::Marketing::APT::TextFlashVariableValue undef
Yahoo::Marketing::APT::TimePeriod undef
Yahoo::Marketing::APT::TimeRange undef
Yahoo::Marketing::APT::UniversalFilter undef
Yahoo::Marketing::APT::UniversalFilterResponse undef
Yahoo::Marketing::APT::UpdatePlacementResponse undef
Yahoo::Marketing::APT::UploadService undef
Yahoo::Marketing::APT::Url undef
Yahoo::Marketing::APT::User undef
Yahoo::Marketing::APT::UserResponse undef
Yahoo::Marketing::APT::UserService undef
Yahoo::Marketing::APT::VideoCreative undef
Yahoo::Marketing::APT::VideoCreativeAddRequest undef
Yahoo::Marketing::APT::VideoCreativeAddResponse undef
Yahoo::Marketing::APT::VideoCreativeResponse undef
Yahoo::Marketing::APT::VideoCreativeService undef
Yahoo::Marketing::APT::Visit undef
Yahoo::Marketing::APT::YahooPremiumBehavioralSegmentTargeting undef
Yahoo::Marketing::APT::ZippedCreativesResponse undef
Yahoo::Marketing::Account undef
Yahoo::Marketing::AccountAggregate undef
Yahoo::Marketing::AccountBalance undef
Yahoo::Marketing::AccountCarrierConfig undef
Yahoo::Marketing::AccountCarrierSetting undef
Yahoo::Marketing::AccountService undef
Yahoo::Marketing::Ad undef
Yahoo::Marketing::AdCarrierConfig undef
Yahoo::Marketing::AdCarrierEditorialReasonInfo undef
Yahoo::Marketing::AdCarrierEditorialReasons undef
Yahoo::Marketing::AdCarrierSetting undef
Yahoo::Marketing::AdEditorialReasons undef
Yahoo::Marketing::AdGroup undef
Yahoo::Marketing::AdGroupCarrierConfig undef
Yahoo::Marketing::AdGroupCarrierSetting undef
Yahoo::Marketing::AdGroupForecastRequestData undef
Yahoo::Marketing::AdGroupOptimizationGuidelines undef
Yahoo::Marketing::AdGroupOptimizationGuidelinesResponse undef
Yahoo::Marketing::AdGroupResponse undef
Yahoo::Marketing::AdGroupService undef
Yahoo::Marketing::AdResponse undef
Yahoo::Marketing::AdService undef
Yahoo::Marketing::Address undef
Yahoo::Marketing::AgeRange undef
Yahoo::Marketing::AgeTarget undef
Yahoo::Marketing::AmbiguousGeoMatch undef
Yahoo::Marketing::ApiFault undef
Yahoo::Marketing::Authorization undef
Yahoo::Marketing::BasicReportRequest undef
Yahoo::Marketing::BasicReportService undef
Yahoo::Marketing::BasicResponse undef
Yahoo::Marketing::BidInformation undef
Yahoo::Marketing::BidInformationRequest undef
Yahoo::Marketing::BidInformationService undef
Yahoo::Marketing::BidUpdateInfo undef
Yahoo::Marketing::BillingUser undef
Yahoo::Marketing::BucketType undef
Yahoo::Marketing::BudgetingService undef
Yahoo::Marketing::BulkDownloadStatusInfo undef
Yahoo::Marketing::BulkDownloadStatusResponse undef
Yahoo::Marketing::BulkService undef
Yahoo::Marketing::BulkUploadStatusResponse undef
Yahoo::Marketing::BulkUploadTokenUrlResponse undef
Yahoo::Marketing::Campaign undef
Yahoo::Marketing::CampaignCarrierConfig undef
Yahoo::Marketing::CampaignCarrierSetting undef
Yahoo::Marketing::CampaignOptimizationGuidelines undef
Yahoo::Marketing::CampaignOptimizationGuidelinesResponse undef
Yahoo::Marketing::CampaignResponse undef
Yahoo::Marketing::CampaignService undef
Yahoo::Marketing::Capability undef
Yahoo::Marketing::Carrier undef
Yahoo::Marketing::CarrierBidInformation undef
Yahoo::Marketing::CarrierBidInformationRequest undef
Yahoo::Marketing::CombinedAccountStatus undef
Yahoo::Marketing::Company undef
Yahoo::Marketing::CompanyService undef
Yahoo::Marketing::ComplexType undef
Yahoo::Marketing::ConverterService undef
Yahoo::Marketing::CreditCardInfo undef
Yahoo::Marketing::DateTimeAccessor undef
Yahoo::Marketing::DayPart undef
Yahoo::Marketing::DayPartingTarget undef
Yahoo::Marketing::DayPartingTargeting undef
Yahoo::Marketing::DemographicTargeting undef
Yahoo::Marketing::DownloadFile undef
Yahoo::Marketing::EditorialReasons undef
Yahoo::Marketing::Error undef
Yahoo::Marketing::ErrorType undef
Yahoo::Marketing::ExcludedWord undef
Yahoo::Marketing::ExcludedWordResponse undef
Yahoo::Marketing::ExcludedWordsService undef
Yahoo::Marketing::FileOutputFormat undef
Yahoo::Marketing::ForecastClickData undef
Yahoo::Marketing::ForecastCreative undef
Yahoo::Marketing::ForecastImpressionData undef
Yahoo::Marketing::ForecastKeyword undef
Yahoo::Marketing::ForecastKeywordBatch undef
Yahoo::Marketing::ForecastKeywordBatchResponse undef
Yahoo::Marketing::ForecastKeywordBatchResponseData undef
Yahoo::Marketing::ForecastKeywordResponse undef
Yahoo::Marketing::ForecastRequestData undef
Yahoo::Marketing::ForecastResponse undef
Yahoo::Marketing::ForecastResponseData undef
Yahoo::Marketing::ForecastService undef
Yahoo::Marketing::GenderTarget undef
Yahoo::Marketing::GeoLocation undef
Yahoo::Marketing::GeoLocationProbability undef
Yahoo::Marketing::GeoLocationProbabilitySet undef
Yahoo::Marketing::GeoTarget undef
Yahoo::Marketing::GeographicalDictionaryService undef
Yahoo::Marketing::HistoricalKeyword undef
Yahoo::Marketing::HistoricalKeywordResponse undef
Yahoo::Marketing::HistoricalKeywordResponseData undef
Yahoo::Marketing::HistoricalRequestData undef
Yahoo::Marketing::HistoricalResponseData undef
Yahoo::Marketing::Keyword undef
Yahoo::Marketing::KeywordCarrierConfig undef
Yahoo::Marketing::KeywordCarrierEditorialReasonInfo undef
Yahoo::Marketing::KeywordCarrierEditorialReasons undef
Yahoo::Marketing::KeywordCarrierSetting undef
Yahoo::Marketing::KeywordEditorialReasons undef
Yahoo::Marketing::KeywordForecastRequestData undef
Yahoo::Marketing::KeywordInfoRequestType undef
Yahoo::Marketing::KeywordInfoResponseType undef
Yahoo::Marketing::KeywordInfoType undef
Yahoo::Marketing::KeywordOptimizationGuidelines undef
Yahoo::Marketing::KeywordOptimizationGuidelinesResponse undef
Yahoo::Marketing::KeywordParticipationStatus undef
Yahoo::Marketing::KeywordRejectionReasons undef
Yahoo::Marketing::KeywordResearchService undef
Yahoo::Marketing::KeywordResponse undef
Yahoo::Marketing::KeywordService undef
Yahoo::Marketing::LocationService undef
Yahoo::Marketing::MasterAccount undef
Yahoo::Marketing::MasterAccountService undef
Yahoo::Marketing::MobileDictionaryService undef
Yahoo::Marketing::Network undef
Yahoo::Marketing::NetworkDistribution undef
Yahoo::Marketing::NetworkTarget undef
Yahoo::Marketing::OptInAdGroupResponse undef
Yahoo::Marketing::OptInCampaignResponse undef
Yahoo::Marketing::OptInStatus undef
Yahoo::Marketing::PageRelatedKeywordRequestType undef
Yahoo::Marketing::PaymentInfo undef
Yahoo::Marketing::PaymentMethodInfo undef
Yahoo::Marketing::Preferences undef
Yahoo::Marketing::ProcessStatus undef
Yahoo::Marketing::RangeDefinitionRequestType undef
Yahoo::Marketing::RangeDefinitionResponseType undef
Yahoo::Marketing::RangeDefinitionType undef
Yahoo::Marketing::RangeValueType undef
Yahoo::Marketing::RelatedKeywordRequestType undef
Yahoo::Marketing::RelatedKeywordResponseType undef
Yahoo::Marketing::RelatedKeywordType undef
Yahoo::Marketing::ReportDownloadInfo undef
Yahoo::Marketing::ReportInfo undef
Yahoo::Marketing::Response undef
Yahoo::Marketing::ResponseStatusType undef
Yahoo::Marketing::Role undef
Yahoo::Marketing::Service undef
Yahoo::Marketing::SetGeographicLocationResponse undef
Yahoo::Marketing::SpendLimit undef
Yahoo::Marketing::SponsoredSearchMinBidRequest undef
Yahoo::Marketing::SubphraseKeywordRequestType undef
Yahoo::Marketing::Summary undef
Yahoo::Marketing::TacticSpendCap undef
Yahoo::Marketing::TargetingAttribute undef
Yahoo::Marketing::TargetingAttributeDescriptor undef
Yahoo::Marketing::TargetingConverterService undef
Yahoo::Marketing::TargetingDictionaryService undef
Yahoo::Marketing::TargetingPremium undef
Yahoo::Marketing::TargetingProfile undef
Yahoo::Marketing::TargetingService undef
Yahoo::Marketing::TargetingType undef
Yahoo::Marketing::Token undef
Yahoo::Marketing::UpdateForAd undef
Yahoo::Marketing::UpdateForAdCarrierConfig undef
Yahoo::Marketing::UpdateForAdCarrierSetting undef
Yahoo::Marketing::UpdateForKeyword undef
Yahoo::Marketing::UpdateForKeywordCarrierConfig undef
Yahoo::Marketing::UpdateForKeywordCarrierSetting undef
Yahoo::Marketing::User undef
Yahoo::Marketing::UserAuthorization undef
Yahoo::Marketing::UserManagementService undef
Yahoo::Marketing::VaultService undef
Yahoo::Marketing::Warning undef
Yahoo::Search::Request 20100614.1
Yahoo::Search::Response 20100614.1
Yahoo::Search::Result 20100614.1
Yandex::Tools::ProcessList undef
YellowBot::API::Request 0.92
YellowBot::API::Response 0.92
YourBrainForOnceDude 0.12
ZCS::Admin::Elements::AddAccountAliasRequest undef
ZCS::Admin::Elements::AddAccountAliasResponse undef
ZCS::Admin::Elements::AuthRequest undef
ZCS::Admin::Elements::AuthResponse undef
ZCS::Admin::Elements::CheckPasswordStrengthRequest undef
ZCS::Admin::Elements::CheckPasswordStrengthResponse undef
ZCS::Admin::Elements::CreateAccountRequest undef
ZCS::Admin::Elements::CreateAccountResponse undef
ZCS::Admin::Elements::CreateCosRequest undef
ZCS::Admin::Elements::CreateCosResponse undef
ZCS::Admin::Elements::CreateDistributionListRequest undef
ZCS::Admin::Elements::CreateDistributionListResponse undef
ZCS::Admin::Elements::CreateDomainRequest undef
ZCS::Admin::Elements::CreateDomainResponse undef
ZCS::Admin::Elements::DelegateAuthRequest undef
ZCS::Admin::Elements::DelegateAuthResponse undef
ZCS::Admin::Elements::DeleteAccountRequest undef
ZCS::Admin::Elements::DeleteAccountResponse undef
ZCS::Admin::Elements::DeleteCosRequest undef
ZCS::Admin::Elements::DeleteCosResponse undef
ZCS::Admin::Elements::DeleteDistributionListRequest undef
ZCS::Admin::Elements::DeleteDistributionListResponse undef
ZCS::Admin::Elements::DeleteDomainRequest undef
ZCS::Admin::Elements::DeleteDomainResponse undef
ZCS::Admin::Elements::DisableArchiveRequest undef
ZCS::Admin::Elements::DisableArchiveResponse undef
ZCS::Admin::Elements::EnableArchiveRequest undef
ZCS::Admin::Elements::EnableArchiveResponse undef
ZCS::Admin::Elements::Error undef
ZCS::Admin::Elements::ExportMailboxRequest undef
ZCS::Admin::Elements::ExportMailboxResponse undef
ZCS::Admin::Elements::GetAccountInfoRequest undef
ZCS::Admin::Elements::GetAccountInfoResponse undef
ZCS::Admin::Elements::GetAccountMembershipRequest undef
ZCS::Admin::Elements::GetAccountMembershipResponse undef
ZCS::Admin::Elements::GetAccountRequest undef
ZCS::Admin::Elements::GetAccountResponse undef
ZCS::Admin::Elements::GetAllAccountsRequest undef
ZCS::Admin::Elements::GetAllAccountsResponse undef
ZCS::Admin::Elements::GetAllAdminAccountsRequest undef
ZCS::Admin::Elements::GetAllAdminAccountsResponse undef
ZCS::Admin::Elements::GetAllCosRequest undef
ZCS::Admin::Elements::GetAllCosResponse undef
ZCS::Admin::Elements::GetAllDomainsRequest undef
ZCS::Admin::Elements::GetAllDomainsResponse undef
ZCS::Admin::Elements::GetAllServersRequest undef
ZCS::Admin::Elements::GetAllServersResponse undef
ZCS::Admin::Elements::GetCosRequest undef
ZCS::Admin::Elements::GetCosResponse undef
ZCS::Admin::Elements::GetDistributionListRequest undef
ZCS::Admin::Elements::GetDistributionListResponse undef
ZCS::Admin::Elements::GetDomainRequest undef
ZCS::Admin::Elements::GetDomainResponse undef
ZCS::Admin::Elements::GetServerRequest undef
ZCS::Admin::Elements::GetServerResponse undef
ZCS::Admin::Elements::ModifyAccountRequest undef
ZCS::Admin::Elements::ModifyAccountResponse undef
ZCS::Admin::Elements::ModifyCosRequest undef
ZCS::Admin::Elements::ModifyCosResponse undef
ZCS::Admin::Elements::ModifyDomainRequest undef
ZCS::Admin::Elements::ModifyDomainResponse undef
ZCS::Admin::Elements::PurgeMovedMailboxRequest undef
ZCS::Admin::Elements::PurgeMovedMailboxResponse undef
ZCS::Admin::Elements::RemoveAccountAliasRequest undef
ZCS::Admin::Elements::RemoveAccountAliasResponse undef
ZCS::Admin::Elements::RenameAccountRequest undef
ZCS::Admin::Elements::RenameAccountResponse undef
ZCS::Admin::Elements::RenameCosRequest undef
ZCS::Admin::Elements::RenameCosResponse undef
ZCS::Admin::Elements::SearchDirectoryRequest undef
ZCS::Admin::Elements::SearchDirectoryResponse undef
ZCS::Admin::Elements::SetPasswordRequest undef
ZCS::Admin::Elements::SetPasswordResponse undef
ZCS::Admin::Elements::context undef
ZCS::Admin::Elements::detail undef
ZCS::Admin::Interfaces::Admin::AdminSoap12 undef
ZCS::Admin::Typemaps::Admin undef
ZCS::Admin::Types::Account undef
ZCS::Admin::Types::AccountBy undef
ZCS::Admin::Types::AccountSpecifier undef
ZCS::Admin::Types::Alias undef
ZCS::Admin::Types::ArchiveSpecifier undef
ZCS::Admin::Types::ChangeSpecifier undef
ZCS::Admin::Types::Cos undef
ZCS::Admin::Types::CosBy undef
ZCS::Admin::Types::CosItemAttribute undef
ZCS::Admin::Types::Dl undef
ZCS::Admin::Types::DlBy undef
ZCS::Admin::Types::Domain undef
ZCS::Admin::Types::DomainBy undef
ZCS::Admin::Types::ExportMailboxSpecifier undef
ZCS::Admin::Types::FaultDetail undef
ZCS::Admin::Types::FaultDetailError undef
ZCS::Admin::Types::GetAccountSpecifier undef
ZCS::Admin::Types::GetCosSpecifier undef
ZCS::Admin::Types::GetDlSpecifier undef
ZCS::Admin::Types::GetDomainSpecifier undef
ZCS::Admin::Types::GetServerSpecifier undef
ZCS::Admin::Types::HeaderContext undef
ZCS::Admin::Types::IntBool undef
ZCS::Admin::Types::ItemAttribute undef
ZCS::Admin::Types::PurgeMovedMailboxResult undef
ZCS::Admin::Types::PurgeMovedMailboxSpecifier undef
ZCS::Admin::Types::ServerBy undef
ZCS::Admin::Types::UserAgentSpecifier undef
ZCS::Admin::Types::ZimbraServer undef
ZCS::Admin::Types::sessionId undef
ZCS::LocalConfig::Command 1
ZCS::LocalConfig::File 1
ZCS::LocalConfig::_base 1
ZConf::Cron::GUI 0.0.0
ZConf::backends::file 1.0.0
ZConf::backends::ldap 0.0.2
ZConf::template 0.0.0
ZConf::template::GUI 0.0.0
Zen::Koan 0.02
Zenoss::Connector undef
Zenoss::Error undef
Zenoss::MetaHelper undef
Zenoss::Response undef
Zenoss::Router undef
Zenoss::Router::DetailNav undef
Zenoss::Router::Device undef
Zenoss::Router::Events undef
Zenoss::Router::Messaging undef
Zenoss::Router::Mib undef
Zenoss::Router::Network undef
Zenoss::Router::Process undef
Zenoss::Router::Report undef
Zenoss::Router::Service undef
Zenoss::Router::Template undef
Zenoss::Router::Tree undef
Zenoss::Router::ZenPack undef
Parse::ABNF
Grammar::Formal
Graph::SomeUtils
Grammar::Graph
accessors::chained 1.01
above undef
accessors::classic 1.01
accessors::ro 1.01
accessors::rw 1.01
aker undef
arXiv::FileGuess undef
attributes 0.12
autodie::exception 2.10
autodie::exception::system 2.10
autodie::hints 2.10
bigint 0.25
bigrat 0.25
blib 1.05
bytes 1.04
cPanel::PublicAPI::Utils undef
cPanel::PublicAPI::WHM 1
cPanel::PublicAPI::WHM::CachedVersion 1
cPanel::StateFile 0.503_02
cPanel::StateFile::FileLocker 0.503_02
cPanel::TaskQueue::ChildProcessor 0.503_02
cPanel::TaskQueue::Ctrl 0.503_02
cPanel::TaskQueue::PluginManager 0.503_02
cPanel::TaskQueue::Processor 0.503_02
cPanel::TaskQueue::Scheduler 0.503_02
cPanel::TaskQueue::Task 0.503_02
charnames 1.11
deprecate 0.01
diagnostics 1.20
eBay::API::Simple::Finding undef
eBay::API::Simple::HTML undef
eBay::API::Simple::Merchandising undef
eBay::API::Simple::RSS undef
eBay::API::Simple::Shopping undef
eBay::API::Simple::Trading undef
eBay::API::SimpleBase undef
encoding 2.6_01
fake undef
feature 1.17
fields 2.15
filetest 1.02
inc::Module::Install 1.01
inc::Module::Install::DSL 1.01
inc::latest 0.3800
inc::latest::private 0.3800
integer 1.00
less 0.03
lib 0.63
lib::core::only undef
locale 1.00
metaclass 2.0002
mixin::with 0.07
more 0.200
mro 1.03
Pod::Elemental::Transformer::Splint
nextgen::blacklist undef
oEdtk::AddrField undef
oEdtk::C7Doc 0.04
oEdtk::C7Tag 0.04
oEdtk::Config 0.03
oEdtk::DBAdmin 0.1
oEdtk::DateField undef
oEdtk::Dict 0.05
oEdtk::Doc 0.01
oEdtk::EDM 0.12
oEdtk::FPField undef
oEdtk::Field undef
oEdtk::Main 0.5001
oEdtk::Messenger 0.001
oEdtk::Outmngr 0.1094
oEdtk::Record undef
oEdtk::RecordParser undef
oEdtk::Run 0.01
oEdtk::SignedField undef
oEdtk::Spool 0.019
oEdtk::TexDoc 0.01
oEdtk::TexTag 0.01
oEdtk::Tracking 0.1
oEdtk::Util 0.01
oEdtk::XPath undef
oEdtk::libC7 0.0056
oEdtk::libDev 0.3126
oEdtk::libXls 0.4627
oEdtk::logger 1.0323
oEdtk::trackEdtk 0.0034
oEdtk::tuiEdtk 0.0031
Class::Accessor::Inherited::XS
Sub::Disable
Devel::PrettyTrace
Devel::Unstate
PerlX::bash
MooX::Pression
Compress::LZ4
Geo::Compass::Variation
Test2::Plugin::IOSync
Test2::Plugin::OpenFixPerlIO
Devel::Trace::Syscall
ojo undef
ok 0.02
Hash::StoredIterator
oo undef
oose 2.0002
ops 1.02
ouse undef
overload 1.10
overload::numbers undef
overloading 0.01
Carp::Fix::1_25
perl5i v2.6.1
perl5i::0 v2.6.1
perl5i::1 v2.6.1
perl5i::2 v2.6.1
perl5i::VERSION v2.6.1
perl5i::latest v2.6.1
pp 0.992
private 0.04
protected 0.04
public 0.04
App::ListRevDeps
re 0.11
readline 1.0303
self::implicit 0.34
sigtrap 1.04
sort 2.01
utf8 1.08
strict 1.04
subs 1.00
true::VERSION 0.18
v6::perlito undef
vars 1.02
warnings 1.10
version::vpp 0.88
version::vxs 0.88
vmsish 1.02
re::engine::GNU
warnings::register 1.01
Task::Latemp 0.0100
AnyEvent::Filesys::Notify
AnyEvent::Fork::RPC
Array::Heap
AnyEvent::Fork::Pool
Mojo::mysql
Mock::MonkeyPatch
Beam::Runner
Minion::Backend::mysql
Mojo::SQLite
Minion::Backend::SQLite
Beam::Minion
AnyEvent::Connector
AnyEvent::WebSocket::Client
AnyEvent::Future
Filter::signatures
Future::HTTP
WWW::Mechanize::Chrome
Text::ANSITable::SQLStyleSheet
Graph::Feather
Algorithm::ConstructDFA2
WebService::Prowl::AnyEventHTTP
Crypt::RSA::Blind
JavaScript::Duktape::XS
Module::Versions::Report
Sort::Naturally::XS
Git::Database
Fluent::Logger
Dancer2::Logger::Fluent
Locale::Utils::PlaceholderBabelFish
Locale::Utils::PlaceholderMaketext
Locale::Utils::PlaceholderNamed
MooX::Singleton
Tie::Sub
Locale::TextDomain::OO
DTL::Fast
Quote::Ref
Config::FileManager
Lingua::Numending
Unicode::BiDiRule
AtExit
Math::Matrix::MaybeGSL
Lingua::Identifier
Encode::EUCJPMS
Jcode::CP932
Test::Collectd::Plugins
Panda::Install
CPP::catch::test
CPP::panda::lib
Panda::Export
Panda::Lib
Variable::Disposition
Ryu
Raisin
JSONY
Class::Accessor::Children
XML::OverHTTP
WebService::Recruit::Dokoiku
Mo::xxx
RosettaCode
JSON::RPC2::TwoWay
JobCenter::Client::Mojo
Mercury
C::Tokenize
Text::LineNumber
XS::Check
Patro
Bit::Manip
App::yajg
String::SQLColumnName
DBIx::Class::Helper::ResultSet::CrossTab
Async::ContextSwitcher
Algorithm::Pair::Swiss
String::CamelSnakeKebab
MooX::Commander
MooX::Failover
UTF8::R2
Dita::PCD
Test::Mockify
File::Digest
Math::ScientificNotation::Util
Module::XSOrPP
Package::Abbreviate
Permute::Named::Iter
TableData::Object::aohos
Bencher::Backend
Array::Set
List::Collection
Bencher::Scenario::SetOperationModules
CPAN::Testers::Schema
Test::CGI::External
File::Scan::ClamAV
Date::Calc::Endpoints
PostScript::Simple
Vector::QRCode::EPS
Log::Any::Adapter::Handler
Throwable::Factory
MarpaX::Languages::M4
Data::Edit::Xml
Term::Filter
Web::ComposableRequest
Rapi::Blog
Do
Chart::GGPlot
FASTX::Reader
Alt::Data::Frame::ButMore
Geo::Gpx
Devel::CompileLevel
Zydeco
Geo::Index
Proch::N50
UTF8::R2
With::Roles
XS::Install
next::XS
Web::Components
Log::Any::Adapter::MojoLog
re::engine::Oniguruma
Alien::SwaggerUI
App::Spec
CPAN::Testers::WWW::Reports::Query::Reports
SMS::Send::Sergel::Simple::HTTP
Device::Chip::PCF8574
Statistics::Sampler::Multinomial
XML::Hash::XS
Test::Snapshot
File::FcntlLock
Dpkg
CPAN::Testers::API
Acme::Holy::Shit::Here::Come::s::An::S
Types::Bool
HTML5::DOM
Lingua::EN::Nums2Words
Lingua::EN::Inflexion
App::sigfix
Finance::GDAX::API
Finance::CoinbasePro::API::CLI
Babble
CPAN::Audit
MooseX::ExtraArgs
Chart::Plotly
HTTP::API::Client
RPC::Switch::Client
Data::TableReader
BEGIN::Lift
MOP
Method::Traits
Moxie
JSON::API
Template::DBI
DBIx::DataFactory
SQL::Beautify
MySQL::Util
Shell::Tools
IPC::Run3::Shell
Badge::Simple
HTML::Inspect
URI::cpan
Test::JSON::Schema::Acceptance
BikePower
App::plockf
Math::Float128
XS::libcatch
App::ListNewCPANDists
Test::BDD::Cucumber
Regexp::Parser
Database::Async
Tie::CheckVariables
Crypt::MagicSignatures::Key
App::Greple
Music::BachChoralHarmony
ETL::Yertl -- too often hangs, keep near the end
Benchmark::Perl::Formance::Plugin::Mandelbrot -- must be the last because it crashes very hard very often

=head1 CONFIGURATION

Summary of my perl5 (revision 5 version 13 subversion 2) configuration:
  Commit id: e0de7c21b08329275a76393ac4d80aae90ac428e
  Platform:
    osname=linux, osvers=2.6.32-2-amd64, archname=x86_64-linux
    uname='linux k81 2.6.32-2-amd64 #1 smp fri feb 12 00:01:47 utc 2010 x86_64 gnulinux '
    config_args='-Dprefix=/home/src/perl/repoperls/installed-perls/perl/v5.13.2-213-ge0de7c2 -Dinstallusrbinperl=n -Uversiononly -Dusedevel -des -Ui_db'
    hint=recommended, useposix=true, d_sigaction=define
    useithreads=undef, usemultiplicity=undef
    useperlio=define, d_sfio=undef, uselargefiles=define, usesocks=undef
    use64bitint=define, use64bitall=define, uselongdouble=undef
    usemymalloc=n, bincompat5005=undef
  Compiler:
    cc='cc', ccflags ='-fno-strict-aliasing -pipe -fstack-protector -I/usr/local/include -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64',
    optimize='-O2',
    cppflags='-fno-strict-aliasing -pipe -fstack-protector -I/usr/local/include'
    ccversion='', gccversion='4.4.4', gccosandvers=''
    intsize=4, longsize=8, ptrsize=8, doublesize=8, byteorder=12345678
    d_longlong=define, longlongsize=8, d_longdbl=define, longdblsize=16
    ivtype='long', ivsize=8, nvtype='double', nvsize=8, Off_t='off_t', lseeksize=8
    alignbytes=8, prototype=define
  Linker and Libraries:
    ld='cc', ldflags =' -fstack-protector -L/usr/local/lib'
    libpth=/usr/local/lib /lib /usr/lib /lib64 /usr/lib64
    libs=-lnsl -lgdbm -ldb -ldl -lm -lcrypt -lutil -lc -lgdbm_compat
    perllibs=-lnsl -ldl -lm -lcrypt -lutil -lc
    libc=/lib/libc-2.11.2.so, so=so, useshrplib=false, libperl=libperl.a
    gnulibc_version='2.11.2'
  Dynamic Linking:
    dlsrc=dl_dlopen.xs, dlext=so, d_dlsymun=undef, ccdlflags='-Wl,-E'
    cccdlflags='-fPIC', lddlflags='-shared -O2 -L/usr/local/lib -fstack-protector'



=head1 AUTHOR

This Bundle has been generated automatically by the autobundle routine in CPAN.pm.
