#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--sleep=f>

Sleep that much between two calls to distrofacts.

=item B<--verbose|v!>

Even more verbose than we already are by default (but this may change)

=back

=head1 DESCRIPTION

Our first iteration is just a letter of intent that we want to become
better on empty pages.

=head1 HISTORY

Up to commit 32b522a we were using

  redis->srandmember("analysis:distv:legalset")

but that started to dry out mid-July 2014, so we switched to a
postgres implementation.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use Time::HiRes qw(sleep);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{sleep} //= 30;

my($workdir);
use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
}
use IPC::ConcurrencyLimit;

my($basename) = File::Basename::basename(__FILE__);
my $limit = IPC::ConcurrencyLimit->new
    (
     max_procs => 1,
     path      => "$workdir/IPC-ConcurrencyLimit-$basename",
    );
my $limitid = $limit->get_lock;
if (not $limitid) {
    warn "Another process appears to be still running. Exiting.";
    exit(0);
}

use Redis;
my $redis = Redis->new(reconnect => 120, every => 1000);
sub mypgdbi () {
    require DBI;
    my $dbi = DBI->connect ("dbi:Pg:dbname=analysis");
}
my $sth = mypgdbi()->prepare("SELECT EXTRACT(EPOCH FROM cuti_ts) from distlookup where distv=?");
my $sth2 = mypgdbi()->prepare("SELECT cuti_ts is null as N0, count(*) from distlookup group by N0");
my $sth3 = mypgdbi()->prepare("SELECT distv from distlookup where (cuti_ts is null or now() - cuti_ts > ?) and distv not like '%-' limit 1");
my $start_days = 20;
setpriority(0, 0, 5); # renice
while () {
    $sth3->execute("$start_days days");
    if ($sth3->rows <= 0){
        if ($start_days > 1) {
            $start_days--;
            sleep 1;
            next;
        } else {
            last;
        }
    }
    my($distv) = $sth3->fetchrow_array;
    next if $distv =~ /-$/; # MyConfig2- KeywordsSpider- BitArray1- (last one reported to Barbie)
    warn "Running 'cnntp-solver.pl --cpanstats_distrofacts $distv'" if $Opt{verbose};
    my @system = ($^X,"$FindBin::Bin/cnntp-solver.pl", "--cpanstats_distrofacts", $distv);
    0 == system(@system) or die "Problems while running '@system'";
    my($legalset_total) = $redis->scard("analysis:distv:legalset");
    my($calctimestamp_total) = $redis->zcard("analysis:distv:calctimestamp");
    my($uncounted_total, $counted_total);
    {
        $sth2->execute;
        while (my @row = $sth2->fetchrow_array) {
            if ($row[0]) {
                $uncounted_total = $row[1];
            } else {
                $counted_total = $row[1];
            }
        }
    }
    warn "distv[$distv]redis:legalset_total[$legalset_total]redis:calctimestamp_total[$calctimestamp_total]pg:uncounted_total[$uncounted_total]pg:counted_total[$counted_total]\n";
    sleep $Opt{sleep};
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
