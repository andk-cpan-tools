#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME

parse-strace-output-for-perlincludes.pl

=head1 SYNOPSIS

parse-strace-output-for-perlincludes.pl [OPTIONS] strace.out-file

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--perl=s>

Full path to a perl from which we can query the INC paths

=back

=head1 DESCRIPTION

=head1 HISTORY

Needed for perl bin/parse-strace-output-for-perlincludes.pl --perl /home/sand/src/perl/repoperls/installed-perls/host/k93jammy/v5.39.4/6567/bin/perl ~sand/tmp/strace-6567.out

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use ExtUtils::MakeMaker;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use Digest::MD5 qw(md5_hex);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{perl} //= $^X;
my $strace_out_file = shift @ARGV or die "missing argument for strace-out-file on commandline";

warn "perl=$Opt{perl}";
my $cmd = qq{$Opt{perl} -le 'my %seen; print join q{|}, grep { ! \$seen{\$_}++ } sort { length \$b <=> length \$a } \@INC'};
warn "cmd=$cmd";
my $inc_regexp = qx{$cmd};
warn "inc_regexp=$inc_regexp";
open my $fh, '<', $strace_out_file or die "Could not open $strace_out_file: $!";
my %S;
my $i = 0;
while (<$fh>) {
    next if /ENOENT/;
    next unless m{"($inc_regexp)/(.*?\.pm)"};
    my($path,$mod) = ($1,$2);
    unless ($S{$mod}++){
        $i++;
        my $abspath = "$path/$mod";
        my $data = do { open my $fh2, "<", $abspath or die "Could not open $abspath: $!"; local $/; <$fh2> };
        my $digest = md5_hex $data;
        my $version = MM->parse_version($abspath);
        printf "%5d %-48s %s %s\n", $i, $mod, $digest, $version;
    }
}



# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
