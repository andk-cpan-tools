#!/usr/bin/perl

=head1 NAME

testadozen - randomly pick several perls to test a distro

=head1 SYNOPSIS

 testadozen [options] distro ...

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--dir=s>

Default: /home/src/perl/repoperls/installed-perls/perl

The directory where the perls are found

=item B<--n=i>

Default: 12

The number of tests to run.

=item B<--pat=s>

Default: v*

Only pick directories matching the glob.

=back

=head1 DESCRIPTION



=cut

use Getopt::Long;
use Hash::Util qw(lock_keys);
use Pod::Usage qw(pod2usage);
use Text::Glob qw(match_glob);
use Time::HiRes qw(sleep);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

my(@distro) = @ARGV or pod2usage(1);

$Opt{n} = 12 unless defined $Opt{n};

$Opt{pat} = "v*" unless defined $Opt{pat};

$Opt{dir} = "/home/src/perl/repoperls/installed-perls/perl" unless defined $Opt{dir};

opendir my $dh, $Opt{dir} or die "Could not open '$Opt{dir}': $!";
my @cand;
for my $dirent (readdir $dh) {
    next if $dirent =~ /^\.\.?$/;
    if ($Opt{pat}) {
        next unless match_glob $Opt{pat}, $dirent;
    }
    push @cand, $dirent;
}
for (my $i = 0; $i < $Opt{n}; $i++) {
    my $j = int rand @cand;
    my $dirent = splice @cand, $j, 1;
    warn "$dirent\n";
    for my $distro (@distro) {
        0 == system "$Opt{dir}/$dirent/bin/perl -MCPAN -e 'install q[$distro]'";
        sleep 0.2;
    }
}

1;
