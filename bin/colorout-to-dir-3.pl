#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

 colorout-to-dir-3.pl file

 eg: perl ~k/sources/CPAN/andk-cpan-tools/bin/colorout-to-dir-3.pl --html megalog-2012-04-24T21:44:13.log

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--html!>

Do not produce XML but additional stuff so we get XHTML

=back

=head1 DESCRIPTION

ALERT: program does not work anymore. Bitrot. Missing comments in the
code make it hard to understand why it worked. See
/home/sand-legacy/andk-cpan-tools.localdisk-deprecated/logs/megainstall.20090215T0811.out
for possibly the youngest left over megainstall file with accompanying
directory of XML results.

The two versions colorout-to-dir.pl and colorout-to-dir-2.pl were
written at the time of the megainstall target. This
colorout-to-dir-3.pl is a reincarnation because our megalog files are
nearly identical to the megainstall files. It now has a manpapage and
we want to add options instead of writing more and more variations. We
have turned on warnings because it was not really hard to do and maybe
it helps us to understand better what we're doing.

The bad things that can happen:

(1) too many lines left over in residuum.yml. Why is there a
SPURKIS!accessors-1.01.xml that does not start with 'Running
install...'?

(2) two things in one: why is there so much about CSS-DOM and
HTML-Encoding within SPROUT!HTML-DOM-0.053.html?

=head1 SEE ALSO

compare-megalogdirs.pl

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=\|\!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(1);
}

# translating
#!/usr/bin/perl -0777 -nl
my $parsefile = shift or pod2usage(1);

use Encode qw(decode);
use List::MoreUtils qw(uniq);
use Time::HiRes qw(sleep time);
use YAML::Syck;

our $VERIFY_XML = 0;
if ($VERIFY_XML) {
  require XML::LibXML;
  our $p = XML::LibXML->new;
}
$_ = do { open my $fh, $parsefile or die "Could not open '$parsefile': $!"; local $/; <$fh> };
chomp;
our $start = time;
our($perl) = m!perl\|\-\>\s(/home\S+/installed-perls/(?:.*?)/bin/perl)\s!;
unless ($perl) {
  die "ALERT: Could not find a valid path to a perl in '$parsefile', maybe remove this irritating file";
}
our($perl_path) = eval { dirname(dirname($perl)) };
if (!$perl_path || $@) {
  die "ALERT: problem on dirname during '$parsefile': perl => '$perl': $@";  
}
our $outdir = $parsefile or die "not true '$parsefile': $!";
warn "Converting '$outdir'\n";
$outdir =~ s/.(out|log)$/.d/ or die "...";
mkpath $outdir;
our $exte = $Opt{html} ? ".html" : ".xml";
my $have_warned = {};

sub mystore ($$$$){
  my($shortdistro,$log,$ok,$seq) = @_;
  my $outfile = $shortdistro;
  $outfile =~ s!\.(tar.gz|tgz|tar.bz2|tbz|zip)?$!$exte!;
  $outfile =~ s|$|$exte| unless $outfile =~ /\Q$exte\E$/;
  $outfile =~ s|/|!|g;
  $outfile =~ s|^|$outdir/|;
  # megalog-2012-04-08T09:37:22.log
  my($time) = $outdir =~ /(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})/;
  open my $fh, ">:utf8", $outfile or die "Could not open > '$outfile': $!";
  my $i = 0;
  $ok = "COULD_NOT_PARSE" unless defined $ok;
 ESTRING: for ($time,$perl_path,$shortdistro,$ok) {
    $i++;
    unless (defined) {
      warn "undefined something during shortdistro=$shortdistro,ok=$ok,seq=$seq,i=$i";
      next ESTRING;
    }
    s!\&!\&amp;!g;
    s!"!&quot;!g;
    s!<!&lt;!g;
    s!>!&gt;!g;
  }
  my $ulog = eval { require Encode::Detect; decode("Detect",$log) };
  if ($@) {
    unless ($have_warned->{Detect}++) {
      warn "error during decode/detect: $@";
      warn "during shortdistro=$shortdistro,ok=$ok,seq=$seq,i=$i";
    }
    $ulog = $log;
  }
  my $dumper = Dumpvalue->new(unctrl => "unctrl");
  $ulog =~ s/([\x00-\x09\x0b\x0c\x0e-\x1f])/ $dumper->stringify($1,1) /ge;
  $ulog =~ s|^</span>||;
  $ulog .= q|</span>| if $ulog =~ /<span[^<>]+>[^<]+$/;
  if ($Opt{html}) {
      $ulog = qq{<html xmlns="http://www.w3.org/1999/xhtml"><head><title>$shortdistro</title></head><body>
<h2>Time: $time</h2>
<h2>Perl: $perl_path</h2>
<h2>Distro: $shortdistro</h2>
<h2>Status: $ok</h2>
<h2>Seq: $seq</h2>
<pre style="font-size: x-large;"><b>$ulog</b></pre>
</body></html>};
  } else {
      $ulog = qq{<distro time="$time" perl="$perl_path" distro="$shortdistro" ok="$ok" seq="$seq">$ulog</distro>\n};
  }
  if ($VERIFY_XML) {
    our $p;
    die "cannot parse '$shortdistro': [$ulog]" unless eval { $p->parse_string($ulog); 1 };
  }
  print $fh $ulog;
  close $fh or die "Could not close '$outfile': $!";
  sleep 1/96;
}

sub measure ($) {
  warn sprintf "[%s] since last measure[%.4f]\n", shift, time - $start;
  $start = time;
}

# the first part is a duplication of colorterm-to-html.pl which I
# wrote for my Munich talk:

s/={10}monitoring proc \d+ perl \S+ secs [0-9\.]+={7}\n//g;
my%h=("&"=>"&amp;",q!"!=>"&quot;","<"=>"&lt;",">"=>"&gt;");
s/([&"<>])/$h{$1}/g;
measure("&\"<>");
s!\e\[1;3[45](?:;\d+)?m(.*?)\e\[0m!<span style="color: blue">$1</span>!sg;
measure("blue");
s!\e\[1;31(?:;\d+)?m(.*?)\e\[0m!<span style="color: red">$1</span>!sg;
measure("red");
#s!\n!<br/>\n!g;
s!\r\n!\n!g;
measure("CRLF");

our $HTMLSPANSTUFF = qr/(?:<[^<>]+>)*/;
{
  my @lines = split /\n/, $_;
  measure("split");
  my %seq; # $seq{$shortdistro} = [];
  my @longdistro;
  my @shortdistro;
  my @leader;
  my $invalid;
  my $i = 0;
 LINE: while (defined($_ = shift @lines)) {
    s!.+?\r!!g;
    # $DB::single = /WWW-Sitemapper-1.110340/;
    if (0) {
    } elsif (/
               >\S+\Q is up to date \E\(
             /x) {
      # lines we ignore.
      next LINE;
    } elsif (/
               \Q>Running install for module '\E
             |
               \QWARNING: This key\E
             |
               \QSignature for \E
             |
               \QPrimary key fingerprint: \E
             |
               \QD i s t r o P r e f s\E
             |
               \Q\.yml\[\d+\]\E
             /x) {
      push @leader, $_;
      next LINE;
    } elsif (
             m!<span[^<>]+>
               (?:
                 Running\s(?:make|Build)\sfor\s(.+)
                 |
                 Checksum\sfor\s(.+)\sok
               )
               </span>
              !x
            ) {
      # lines where we say they introduce a new block and we expect
      # the following lines all to belong to this distro.
      # $DB::single=1;
      my $d = $1 || $2;
      $d =~ s|^.+/id/([A-Z]/[A-Z][A-Z]/)|$1|;
      if (!@longdistro or $d ne $longdistro[-1]) {
        push @longdistro, $d;
        $d =~ s|^[A-Z]/[A-Z][A-Z]/||;
        push @shortdistro, $d;
        $seq{$shortdistro[-1]} ||= [];
      }
      if (@leader) {
        push @{$seq{$shortdistro[-1]}}, @leader;
        @leader = ();
      }
    } elsif (@shortdistro and m|[ ]{2}\Q$shortdistro[-1]\E| and !m|----\s.+\s----|) {
      #### $DB::single = $shortdistro[-1] eq "JILLROWE/Runner-Init-2.23.tar.gz";
      # this may be the end of the block
      push @{$seq{$shortdistro[-1]}}, $_;
      my $end = 0;
      my $ok;
      if ($lines[0] =~ /[ ]{2}.+[ ]install[ ].*?--\s+((?:NOT\s)?OK|NA)/) {
        $ok = $1;
        push @{$seq{$shortdistro[-1]}}, shift @lines;
        $end = 1;
      } elsif ($lines[0] =~ /[ ]{2}(.+)\s+--\s+((?:NOT\s)?OK|NA)/) {
        my $command = $1;
        $ok = $2;
        push @{$seq{$shortdistro[-1]}}, shift @lines;
        if (    $ok =~ /\bNOT\b/
            and $command =~ /\b(?:make|Build|(?:Build|Makefile)\.PL|test)$/) {
          if ($invalid) {
            $ok = "DEPEFAIL";
          }
          $end = 1;
        }
        if ($lines[0] =~ /\bPrepending\b.*\bPERL5LIB\b/) {
          push @{$seq{$shortdistro[-1]}}, shift @lines;
        }
        if ($lines[0] =~ />Running.*test/
            && $lines[1] =~ />[ ]{2}/) {
          push @{$seq{$shortdistro[-1]}}, shift @lines;
          push @{$seq{$shortdistro[-1]}}, shift @lines;
          $end=1;
        }
        if ($lines[0] =~ />Running.*install/
            && $lines[1] =~ />[ ]{2}/) {
          push @{$seq{$shortdistro[-1]}}, shift @lines;
          push @{$seq{$shortdistro[-1]}}, shift @lines;
          $end=1;
        }
        if ($lines[0] =~ />\/\/hint\/\//) {
          push @{$seq{$shortdistro[-1]}}, shift @lines;
          push @{$seq{$shortdistro[-1]}}, shift @lines while $lines[0] =~ /^\s/;
          $end=1;
        }
      } elsif ($lines[0] =~ /Already done|Won't repeat unsuccessful test|Tests succeeded but|one dependency not OK|\d+ dependencies missing/) {
        push @{$seq{$shortdistro[-1]}}, shift @lines;
        $ok = "DEPEFAIL";
        $end=1;
      }
      if ($end) {
        # if we reached the end of one distro we pop this one off the
        # end of the "stack" of distros we have seen starting.
        $i++;
        unless ($i % 500){
          measure($i);
        }
        my $log = join "", map { "$_\n" } @{$seq{$shortdistro[-1]}};
        mystore($shortdistro[-1],$log,$ok,$i);
        $invalid = undef;
        delete $seq{$shortdistro[-1]};
        pop @longdistro;
        pop @shortdistro;
      }
      next LINE;
    } elsif (/CPAN::Reporter: test results were not valid, Prerequisite missing:|Warning: Prerequisite.*failed when processing/) {
      $invalid = 1;
    }
    # every line can be collected under the name of the last distro we
    # have identified. Before the first one we have nothing to collect
    if (@shortdistro) {
      push @{$seq{$shortdistro[-1]}}, $_;
    }
  } # while @lines
  # The residuum is a hash of distros we did not see finishing.
  # Usually this means they failed early or horribly
  my $residuumi = 0;
  for my $k (keys %seq) {
    $i++;
    $residuumi++;
    my $log = join "", map { "$_\n" } @{$seq{$k}};
    mystore($k,$log,"RESIDUUM",$i);
    delete $seq{$k};
  }
  measure($i);
  #open my $rfh, ">", "$outdir/residuum.yml" or die;
  #print $rfh YAML::Syck::Dump(\%seq);
  #close $rfh or die;
  warn "wrote a total of $i reports (among which $residuumi in state RESIDUUM)";
}

if (-e $perl) {
  local $@;
  eval {
    open my $fh, ">", "$outdir/perl-V.txt" or die "Could not open >$outdir/perl-V.txt: $!";
    open my $pfh, "-|", $perl, "-V" or die "cannot fork: $!";
    while (<$pfh>) {
      print $fh $_;
    }
    close $pfh or die "perl died during -V";
    close $fh or die "could not write '$outdir/perl-V.txt': $!";
  };
  if ($@) {
    warn "Error while trying to run -V: '$@'";
  }
}

__END__


# Local Variables:
# mode: cperl
# cperl-indent-level: 2
# End:
