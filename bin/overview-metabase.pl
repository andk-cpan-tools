#!/usr/bin/perl

# arguments can be package names: their versions are appended to each line, space separated

=head1 NAME

....pl - 

=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut


my $optpod = <<'=back';

=item B<--bbc!>

Only output the history of projects that seem to be BBCs. NOTE DANGER:
demanding that 5.13 be among the testing perl versions. Option has a
Verfallsdatum.

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Read the local collection from the logfile from metabase, write one line per distro.

=head1 TODO


=head1 AUTHOR

=cut

use strict;
use Getopt::Long;
use Pod::Usage qw(pod2usage);

use Sort::Versions;
use YAML::Syck ();
use Time::HiRes qw(sleep time);

my @opt = $optpod =~ /B<--(\S+)>/g;
our %Opt;
GetOptions
    (
     \%Opt,
     @opt,
    ) or pod2usage(1);

pod2usage(0) if $Opt{help};

my $storefile = "metabase-log.txt";
chdir "/home/k/sources/CPAN/andk-cpan-tools" or die;
open my $fh, $storefile or die;
# [2010-10-29T21:05:10Z] [Chris Williams (BINGOS)] [pass] [RIZEN/Chat-Envolve-0.0100.tar.gz] [i386-dragonfly-64int] [perl-v5.10.0] [373b43ee-e3a0-11df-9e2d-9e9e6e8696e0] [2010-10-29T21:05:10Z]
my @qr = (qr/\[([^\]]+)\]/) x 8;
local $" = " ";
my %H;
my $line = 0;
LINE: while (<$fh>) {
    my($date,$tester,$result,$path,$arch,$perl,$uuid,$ts) = my @f = /@qr/ or next;
    my $h = $H{$path}{history} ||= [];
    $H{$path}{intro} ||= ++$line;
    push @$h, {result => $result, ts => $ts, perl => $perl, tester => $tester};
}
my $boring = 0;
HISTORY: for my $path (sort { $H{$a}{intro} <=> $H{$b}{intro} } keys %H) {
    my %S;
    for my $rec (@{$H{$path}{history}}) {
        my $result = $rec->{result};
        next if $result =~ /^(na|unknown)$/;
        $S{$result}++;
    }
    if (keys %S <= 1){
        $boring++;
        next HISTORY;
    }
    my $distroreport = "";
    $distroreport .= sprintf "%s:\n", $path;

    my $seen_fail = 0;
    my $maybe_bbc = 1;
    my $seen_513 = 0;
    for my $rec (sort {versioncmp($a->{perl},$b->{perl}) || $b->{result} cmp $a->{result}} @{$H{$path}{history}}) {
        next if $rec->{result} =~ /^(na|unknown)$/;
        $distroreport .= sprintf "  %-15s %s    %s\n", @$rec{"perl","result","tester"};
        $seen_fail = 1 if $rec->{result} eq "fail";
        $maybe_bbc = 0 if $seen_fail && $rec->{result} eq "pass";
        $seen_513 = 1 if $rec->{perl} =~ /5\.13\./;
    }
    if ($Opt{bbc}) {
        if ($maybe_bbc && $seen_513) {
            print $distroreport;
        }
    } else {
        print $distroreport;
    }
    # , join(" ", map {$_->{result}} );
}
printf "(boring distros: %d)\n", $boring;

