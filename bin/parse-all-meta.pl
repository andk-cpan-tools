#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--cpan=s>

CPAN mirror root directory. Defaults to /home/ftp/pub/PAUSE

=item B<--help|h!>

This help

=item B<--parser=s>

defaults to Parse::CPAN::Meta. Also supported: YAML::Syck

=back

=head1 DESCRIPTION



=cut

use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

$Opt{cpan} ||= "/home/ftp/pub/PAUSE";
my $counter = 0;
use File::Find;
find
    ({
      wanted => sub {
          return unless /\.meta$/;
          my $ok;
          if ($Opt{parser} eq "Parse::CPAN::Meta") {
              require Parse::CPAN::Meta;
              unlink "/tmp/meta.yaml" or die $! if -e "/tmp/meta.yaml";
              symlink $File::Find::name, "/tmp/meta.yaml";
              $ok = eval { Parse::CPAN::Meta->load_file("/tmp/meta.yaml"); };
              unlink "/tmp/meta.yaml" or die $!;
          } elsif ($Opt{parser} eq "YAML::Syck") {
              require YAML::Syck;
              $ok = eval { YAML::Syck::LoadFile($File::Find::name); };
          } elsif ($Opt{parser} eq "YAML::XS") {
              require YAML::XS;
              $ok = eval { YAML::XS::LoadFile($File::Find::name); };
          }
          return if $ok;
          $counter++;
          warn "$Opt{parser} could not parse $counter\: $File::Find::name\:\n$@\n";
      },
      no_chdir => 1,
     },
     $Opt{cpan} . "/authors/id"
    );
