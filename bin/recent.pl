#!/usr/bin/perl

=head1 NAME

recent - 

=head1 SYNOPSIS

 watch -t -n 20 'perl ~k/sources/CPAN/GIT/trunk/bin/recent.pl -n 25 --burn-in-protection'

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--alternative=i>

=item B<--allowdups!>

Normally we suppress Foo-3.33 when we have already seen Foo-4.44. With
this option we show all versions.

=item B<--burn-in-protection!>

=item B<--linetrim=i>

=item B<--localroot=s>

Defaults to C</home/ftp/pub/PAUSE/authors>. Path to authors directory.

=item B<--n=i>

=item B<--showsize!>

=item B<--skip=s>

Set::IntSpan list of lines to skip

=item B<--statefile=s>

Statefile of the smoker

=back

=head1 DESCRIPTION

Show most recent uploads according to the RECENT file and mark the
currently processing one (according to ~/.cpan/loop-over-recent.state
with a star.

The burn-in-protection changes something from time to time. This also
cleans up STDERR remnants that otherwise might annoy the user of
watch(1).

=cut

use strict;
use warnings;

use CPAN::DistnameInfo;
eval {require Time::Duration};
our $HAVE_TIME_DURATION = !$@;
# use YAML::Syck;

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

use Set::IntSpan;
my $sis = $Opt{skip} ? Set::IntSpan->new($Opt{skip}) : 0;

$Opt{n}||=40;

if (-e "/home/k/sources/rersyncrecent/lib/") {
  require lib;
  lib->import("/home/k/sources/rersyncrecent/lib/");
  lib->unimport(".");
}
require File::Rsync::Mirror::Recent;
use CPAN::Version;
unless (CPAN::Version->vge($File::Rsync::Mirror::Recent::VERSION, '0.4.5')) {
  warn "WARNING: loaded version File::Rsync::Mirror::Recent::VERSION='$File::Rsync::Mirror::Recent::VERSION' maybe not sufficient. Loaded from $INC{'File/Rsync/Mirror/Recent.pm'}\n";
}

my $statefile = $Opt{statefile} //= "/home/sand/.cpan/loop-over-recent.state";
my $max_epoch_worked_on = 0;

my $rx = qr!\.(tar.gz|tar.bz2|zip|tgz|tbz)$!; # see also loop-over...

if (-e $statefile) {
  local $/;
  my $state = do { open my $fh, $statefile or die "Couldn't open '$statefile': $!";
                   <$fh>;
                 };
  chomp $state;
  $state||=0;
  $state += 0;
  $max_epoch_worked_on = $state if $state;
}
$Opt{localroot} ||= "/home/ftp/pub/PAUSE/authors";
$Opt{localroot} =~ s|/*$|/|; # ensure trailing slash
my $rf = File::Rsync::Mirror::Recent->new
    (
     localroot => $Opt{localroot},
     local => "$Opt{localroot}RECENT.recent",
    );
my $have_a_current = 0;
my $recent_events = $rf->news(max=>10*$Opt{n});
{
  my %seen;
  $recent_events = [ grep { $_->{path} =~ $rx
                                && $_->{type} eq "new";
                          } @$recent_events ];
  unless ($Opt{allowdups}) {
    $recent_events = [ grep { my $d = CPAN::DistnameInfo->new($_->{path});
                              no warnings 'uninitialized';
                              !$seen{$d->dist}++
                            } @$recent_events ];
  }
  for my $re (@$recent_events) {
    if ($re->{epoch} == $max_epoch_worked_on) {
      $re->{is_current} = 1;
      $have_a_current = 1;
    }
  }
}

my $prev_skip_indicator;
sub maybe_paint_skip_indicator ($) {
  my($i) = shift;
  if (! defined $prev_skip_indicator or $i != $prev_skip_indicator+1) {
    print "       [...]\n";
  }
  $prev_skip_indicator = $i;
}

my $count = 0;
my $intro_done = 0;
ITEM: for my $i (0..$#$recent_events) {
  my $item = $recent_events->[$i];
  ++$count;
  if ($sis && $sis->member($count)) {
    maybe_paint_skip_indicator($count);
    next ITEM;
  }
  my $mark = "";
  my $epoch = $item->{epoch};
  unless ($intro_done++) {
    if ($HAVE_TIME_DURATION) {
      printf "   %s since latest upload\n", Time::Duration::duration(time - $epoch);
    } else {
      printf "   %7d seconds since latest upload (no Time::Duration?)\n", time - $epoch;
    }
  }
  if ($max_epoch_worked_on) {
    if ($item->{is_current}) {
      $mark = "*";
    } elsif (!$have_a_current
             && $max_epoch_worked_on > $item->{epoch}
             && $i > 0
             && $max_epoch_worked_on < $recent_events->[$i-1]->{epoch}) {
      printf "%1s %s\n", "*", scalar localtime $max_epoch_worked_on;
    }
  }
  my $size = "";
  if ($Opt{showsize}) {
    my $abs = $Opt{localroot} . $item->{path};
    $size = -s $abs ? sprintf(" %7d ",-s $abs) : "_________";
  }
  my $line = sprintf
      (
       "%4d %1s %s %s%s\n",
       $count,
       $mark,
       scalar localtime $epoch,
       $size,
       substr($item->{path},8),
      );
  if (my $trim = $Opt{linetrim}) {
    if (length $line > $trim) {
      substr($line,$trim-3) = "...\n";
    }
  }
  if ($Opt{"burn-in-protection"}) {
    chomp $line;
    while (rand 30 < 1) {
      $line = " $line";
    }
    if (length($line) > 80) {
      while (length($line) > 80){
        chop($line);
      }
      substr($line,80-1,1) = rand(30)<1 ? "_" : ">";
    }
    while (length($line) < 80){
      $line .= rand(30)<1 ? "_" : " ";
    }
    if (rand(30)<1) {
      $line =~ s/ /_/g;
    }
    $line .= "\n";
  }
  print $line;
  if ($Opt{n} && $count>=$Opt{n}) {
    last ITEM;
  }
}
if (0 == $count) {
  print sprintf "  found nothing of interest in %s\n", $rf->rfile;
} elsif ($Opt{"burn-in-protection"} && $count < $Opt{n}) {
  while ($count < $Opt{n}) {
    my $line = "";
    while (length($line) < 80){
      $line .= rand(30)<1 ? "_" : " ";
    }
    print "$line\n";
    $count++;
  }
}
__END__

# Local Variables:
# mode: cperl
# cperl-indent-level: 2
# End:
