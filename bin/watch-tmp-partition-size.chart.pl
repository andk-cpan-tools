use strict;
use Time::Local;
my $file = shift || "bin/watch-tmp-partition-size.out";
open my $fh, $file or die "Could not open '$file': $!";
my(@keys,@values);
my($t,$score,@t);
my $i = 0;
while (<$fh>) {
    chomp;
    (@t) = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})/;
    if (@t) {
        $t[0]-=1900;
        $t[1]--;
        $t = timelocal(reverse @t);
        @t = ();
    }
    ($score) = $1 if m|\d+\s+\d+\s+\d+\s+(\d+)\%\s+/tmp|;
    if ($t && $score && !(++$i % 67)) {
    # if ($t && $score) {
        if ($t > 1290241849 && $t < 1290249769) {
            push @keys, $t;
            push @values, $score;
        }
        $t = $score = "";
        $i = 0;
    }
}
require Chart::Clicker;
require Chart::Clicker::Axis::DateTime;
require Chart::Clicker::Data::Series;
require Chart::Clicker::Data::DataSet;

# build the chart
my $chart = Chart::Clicker->new(width => 1200, height => 400);

# build the series (static here, will usually be supplied arrayrefs from elsewhere)
$DB::single++;
my $series = Chart::Clicker::Data::Series->new
    (
     keys    =>      \@keys,
     values  =>      \@values,
    );

# build the dataset
my $dataset = Chart::Clicker::Data::DataSet->new
    (
     series  =>      [ $series ],
    );

# add the dataset to the chart
$chart->add_to_datasets($dataset);
my $def = $chart->get_context('default');
my $dtaxis = Chart::Clicker::Axis::DateTime->new
    (
     format => '%Y-%m-%d %H:%M',
     position => 'bottom',
     orientation => 'horizontal'
    );
$def->domain_axis($dtaxis);
my $yaxis = Chart::Clicker::Axis->new
    (
     format => "%s",
     position => 'left',
     orientation => 'vertical',
     range => Chart::Clicker::Data::Range->new
     ({
       lower => 0,
       upper => 100,
      }),
    );
$def->range_axis($yaxis);

my $title = "title";
$chart->title->text($title);
$chart->legend->visible(0);
# $chart->title->font->size(20);

use Chart::Clicker::Renderer::Point;
my $renderer = Chart::Clicker::Renderer::Point->new
    (
     opacity => .75,
    );

$renderer->shape
    (
     Geometry::Primitive::Circle->new({radius => 1})
    );


# assign the renderer to the default context
$chart->set_renderer($renderer);

# write the chart to a file
$chart->write_output("watch-tmp-partition-size.png");
