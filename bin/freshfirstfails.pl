#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

$0 [OPTIONS]

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--days=f>

Defaults to 7. Is translated to

  now() - interval 'N days'

=item B<--help|h!>

This help

=item B<--minpostdate=s>

Looks like an integer but stands for a month in the format YYYYMM.
Actual DB range is 199908 to, at the time of this writing, 202202.
Used to generate where clauses such as

   ... where postdate >= $dbh->quote($Opt{minpostdate}) ...

Defaults to current month in local time minus two years;

B<Note: this should be changed to be a moving value similar to days> (once we are satisfied with the alpha version)

=tiem B<--showfails=i>

Defaults to 3.

=item B<--urls!>

Provide relevant URLs and parts of URLs after each line

=back

=head1 DESCRIPTION

=head1 HISTORY

=head1 DEPLOYMENT

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use POSIX ();
use Hash::Util qw(lock_keys);
use List::AllUtils qw(any);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
unless (defined $Opt{minpostdate}) {
    my @date = localtime();
    $date[5] += 1900;
    $date[4]++;
    $Opt{minpostdate} = sprintf "%04d%02d", $date[5]-2, $date[4];
}
$Opt{days} //= 7;
my $showfails = $Opt{showfails} //= 3;

use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;

my $ann = "/home/andreas/src/analysis-cpantesters-annotate/annotate.txt"; # !!! needs config support
open my $fh, $ann or die "Could not open $ann: $!";
my(%Annotations,%HalfAnnotations);
while (<$fh>){
    my($d,$r) = /(\S+)\s+(.+)/;
    $Annotations{$d} = $r;
    $d =~ s/-v?[\d\._]+//;
    $HalfAnnotations{$d} = $r;
}

use LWP::UserAgent;
use JSON::XS;
use CPAN::DistnameInfo;
use List::AllUtils qw(reduce);
use CPAN::Version;

my $ua = LWP::UserAgent->new();
my $jsonxs = JSON::XS->new->indent(0);

use DBI;

my $pgdbh = DBI->connect("dbi:Pg:dbname=analysis") or die "Could not connect to 'analysis': $DBI::err";
my $sth0 = $pgdbh->prepare("create temporary table x as (select dist, version, sum(case when state='pass' then cnt else 0 end) passes, sum(case when state='fail' then cnt else 0 end) fails from (select dist, version, state, count(*) cnt from cpanstats where postdate >= ? AND fulldate < now() - interval '$Opt{days} days' group by dist, version, state) x group by dist, version)");
my $sth0i = $pgdbh->prepare("CREATE INDEX xdv ON x (dist, version)");
my $sth1 = $pgdbh->prepare("select passes, fails from x where dist=? AND version=?");
my $sth2 = $pgdbh->prepare("select version, passes, fails from x where dist=? order by version");
my $sth3 = $pgdbh->prepare("select dist, version, state, fulldate, tester, perl, guid from cpanstats where (postdate >= ?) AND (fulldate > (now() - interval '$Opt{days} days')) order by fulldate"); # ignoring race between two values of now()
my $sth4 = $pgdbh->prepare("update x set passes=? where dist=? AND version=?");
my $sth5 = $pgdbh->prepare("update x set fails=? where dist=? AND version=?");
my $sth6 = $pgdbh->prepare("INSERT INTO x values (?,?,0,0)");

binmode \*STDOUT, ":utf8";

{
    $sth0->execute($Opt{minpostdate});
    $sth0i->execute();
    $sth3->execute($Opt{minpostdate});

    my $i = 0;
    my $total = $sth3->rows;
    my %cand;
  ROW: while (my($dist,$version,$state,$fulldate,$tester,$perl,$guid) = $sth3->fetchrow_array) {
        ++$i;
        $sth1->execute($dist, $version);
        if ($sth1->rows == 0) {
            $sth6->execute($dist, $version);
            $sth1->execute($dist, $version);
        }
        my($passes, $fails) = $sth1->fetchrow_array;
        if ($state eq 'pass') {
            $passes++;
            $sth4->execute($passes, $dist, $version);
            next ROW;
        } elsif ($state eq 'fail') {
            unless ($fails) {
                $cand{$dist}{$version}=1;
            }
            $fails++;
            $sth5->execute($fails, $dist, $version);
        } else {
            next ROW;
        }
        next ROW unless $passes;
        next ROW unless $cand{$dist};
        next ROW unless $fails && $fails <= $showfails;
        my $short_tester = length($tester) < 40 ? $tester : substr($tester,0,40);
        $fulldate =~ s/:00\+00$//;
        printf "%6d %-39s %-16s %-11s %5d %2d %s %s %s\n", $i, $dist, $version, $perl, $passes, $fails, $fulldate, $guid, $short_tester;
        if ($Opt{urls}) {
            my $distv =  sprintf "%s-%s", $dist, $version;
            print "        $distv\n";
            my %w = ( distv => $distv, version => $version ); # work from metacpanapiquery, maybe not needed
            my $query = sprintf "http://fastapi.metacpan.org/v1/release/_search?q=distribution:%s&fields=name,date,status,version,author,archive&size=400&_source=tests", $dist;
            my $resp = $ua->get($query);
            unless ($resp->is_success) {
                warn sprintf "No success visiting '%s': %s; sleeping %.3f\n",
                    $query, $resp->code;
                next ROW;
            }
            # print $query;
            my $jsontxt = $resp->decoded_content;
            my $j = eval { $jsonxs->decode($jsontxt); };
            if (!$j || $@) {
                my $err = $@ || "unknown error";
                die "Error while decoding '$jsontxt': $err";
            }
            my $hits = $j->{hits}{hits};
            my($matchingrelease) = grep { $_->{fields}{name} eq $distv } @$hits;
            unless ($matchingrelease) {
                warn "Did not find release for $distv\n";
                next ROW;
            }
            my($releasedate) = $matchingrelease->{fields}{date};
            my($archive) = $matchingrelease->{fields}{archive};
            print "        releasedate: $releasedate\n";
            my($author) = $matchingrelease->{fields}{author};
            print "$author/$archive\n";
            # $DB::single = 1;
            # warn "x $matchingrelease";
            my $report_url = sprintf "http://www.cpantesters.org/cpan/report/$guid";
            print "$report_url\n";
        }
    }
    $i = 0;
    my $displayallcands = 0;
    if ($displayallcands) {
      DIST: for my $dist (sort keys %cand) {
            printf "%4d %s\n", ++$i, $dist;
            $sth2->execute($dist);
            my $j = 0;
          VERSION: while (my($version, $passes, $fails) = $sth2->fetchrow_array) {
                next VERSION unless $cand{$dist}{$version};
                my $anno_comment = "";
                if (my $anno = $Annotations{sprintf "%s-%s", $dist, $version}) {
                    $anno_comment = " anno: $anno";
                }
                elsif (my $half_anno = $HalfAnnotations{$dist}) {
                    $anno_comment .= " hanno: $half_anno";
                }
                printf "    %4d %-20s %8d %8d%s\n", ++$j, $version, $passes, $fails, $anno_comment;
            }
        }
    }
    $pgdbh->do("DROP TABLE x");
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
