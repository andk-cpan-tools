#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION



=cut

use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

# print "ssh andreas\@analysis ls -l ";
while (<DATA>) {
    my($cnt, $author, $vdistro) = m/\s*(\d+)\s*\[(.+?)\/(.+)\]/;
    $vdistro =~ s/.+\///;
    $vdistro =~ s/\.gz$//;
    unless (-e "tmp/$vdistro") {
        printf "rsync -vaP andreas\@analysis:/home/andreas/data/cpantesters/reports/%s/%s tmp/\n", substr($vdistro,0,1), $vdistro;
    }
    printf "tar -C tmp -xvf tmp/$vdistro\n";
}

__END__
     62 [CFUHRMAN/Log-Fine-0.65.tar.gz]
     33 [NICZERO/Mojar-Cron-0.321.tar.gz]
     11 [SZABGAB/Class-Date-1.1.15.tar.gz]
     10 [NICZERO/Mojar-Cron-0.321.tar.gz]
      9 [ANDK/CPAN-2.14.tar.gz]
      6 [DOY/Spreadsheet-ParseXLSX-0.24.tar.gz]
      5 [RKELSCH/Graphics-Framebuffer-5.85.tar.gz]
      5 [MUIR/modules/Time-ParseDate-2015.103.tar.gz]
      4 [ILMARI/Class-Unload-0.09.tar.gz]
      4 [DOY/Reply-0.40.tar.gz]
      2 [MUIR/modules/Time-ParseDate-2015.103.tar.gz]
      2 [INGY/YAML-1.15.tar.gz]
      2 [ANDK/CPAN-2.14.tar.gz]
      1 [WANGQ/App-Fasops-0.4.3.tar.gz]
      1 [TINITA/YAML-LibYAML-0.62.tar.gz]
      1 [SREZIC/CPAN-Plugin-Sysdeps-0.09.tar.gz]
      1 [RWSTAUNER/Test-Aggregate-0.373.tar.gz]
      1 [RSRCHBOY/MooseX-Role-XMLRPC-Client-0.06.tar.gz]
      1 [RENTOCRON/Net-Flotum-0.05.tar.gz]
      1 [PERLANCAR/Module-XSOrPP-0.11.tar.gz]
      1 [PERLANCAR/Module-XSOrPP-0.11.tar.gz]
      1 [OMEGA/PDF-Table-0.9.11.tar.gz]
      1 [NICZERO/Mojar-2.051.tar.gz]
      1 [KABLAMO/WebService-Site24x7-0.01.tar.gz]
      1 [JACKB/Dancer2-Plugin-Tail-0.001.tar.gz]
      1 [INGY/Spiffy-0.46.tar.gz]
      1 [GWYN/File-ShareDir-Install-0.10.tar.gz]
      1 [GAAS/WWW-RobotRules-6.02.tar.gz]
      1 [GAAS/HTTP-Negotiate-6.01.tar.gz]
      1 [GAAS/HTTP-Cookies-6.01.tar.gz]
      1 [GAAS/HTML-Parser-3.72.tar.gz]
      1 [EXODIST/Test-Simple-1.302033.tar.gz]
      1 [EXODIST/Test2-Suite-0.000038.tar.gz]
      1 [ETHER/App-Nopaste-1.005.tar.gz]
      1 [ECKARDT/Script-Toolbox-0.49.tar.gz]
      1 [DANIEL/Ogg-Vorbis-Header-PurePerl-1.0.tar.gz]
      1 [CORION/Future-HTTP-0.01.tar.gz]
      1 [CELOGEEK/MooX-Options-4.022.tar.gz]
      1 [CANID/Lock-Server-1.7.tar.gz]
      1 [BINGOS/POE-Component-SmokeBox-Dists-1.08.tar.gz]
      1 [AUFFLICK/HTTP-Daemon-SSL-1.04.tar.gz]
      1 [AJPAGE/Bio-Roary-3.6.2.tar.gz]
      1 [ABRAXXA/ExtJS-Generator-DBIC-0.002.tar.gz]
      1 [ABRAXXA/ExtJS-Generator-DBIC-0.001.tar.gz]
