#!/home/src/perl/repoperls/installed-perls/perl/pVNtS9N/perl-5.8.0@32642/bin/perl -d

# use 5.010;
use strict;
use warnings;

=head1 NAME

cnntp-scrape - collect metadata of postings to nntp.perl.org

=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--baseuri=s>

defaults to C<http://www.nntp.perl.org/group>.

=item B<--group=s>

defaults to C<perl.cpan.testers>.

=item B<--workdir=s>

defaults to something like ~/var/...

=back

=head1 DESCRIPTION

https://svn.develooper.com/projects/cnntp/ is the code base that
produces the pages. I found the link on
http://www.nntp.perl.org/about/

We want to download all metadata of all postings of the current month,
i.e. subject, from, id and write it to a permanent place. We are not
interested much in data from the month before, so we should parse out
which month we are in and use it as a label for the resulting DB file.
We are not interested in the page id because this changes every hour.

Maybe we do not want to parse the html but rather some RSS feed? I see
in the svn log that there were atom and rss activities but I find no
links on the pages.

/group/$1/rss/posts.xml in apache/sites/cnntp.tmpl sounds interesting.
Indeed, http://www.nntp.perl.org/group/perl.cpan.testers/rss/posts.xml
gives me something RSSish starting with
http://www.nntp.perl.org/group/perl.cpan.testers/2009/01/msg2987597.html
and ending with
http://www.nntp.perl.org/group/perl.cpan.testers/2009/01/msg2987558.html
But the month has started with
http://www.nntp.perl.org/group/perl.cpan.testers/2009/01/msg2967157.html
So, forget RSS.

objects:

B<month>, name, article_min, article_max, current_paging. The month
changes only when we visit the default page,
http://www.nntp.perl.org/group/perl.cpan.testers/

current_paging which can change at any time: I just clicked on 116
which was the last but on the resulting page I see now 118 is the
last. Two consecutive clicks are not necessarily providing adjacent
datasets. current_paging has attributes: max (int), seen (hash)

articles can be thread leaders or not. If they are thread leaders we
must read them to get at the other articles in the thread. We have the
subject already but not the author. article_subject, article_from,
article_id. whether an article is a thread leader or not can change
over time, even while we are reading, we should not record the fact.
Oh well, we must, of course, record it as long as we have not read
them.

=head2 first round algorithm fails

I thought I'd get all articles in a range and be finished when I
actually have them. But neither can I determine the two borders
reliably nor can I get intervals complete.

So the algorithm must change. Read 1..n. Stop when either 20 minutes
have passed or n is reached or end is reached. Then sleep. Good
enough.

=head2 BUGS

Plenty.

=over

=item how to deal with holes

How come that we have holes? For example 3081767 is missing today. It
was a posting on Jan 17th. All could be so correct and simple without
the holes. 17th january is today. 3081851 is current max. That's 84
below max. The current first page goes down to 3081762 but I do not
see x1767. x1767 is:

  PASS local-lib-1.003001 i686-linux-thread-multi-64int-ld 2.6.16.38

The string "local-lib" is not on pages 1-10, so this is not a
threading issue. The posting is simply missing in the pages. The
missing postings are not a fault of this program it seems.

We just passed a full wallclock hour. And nntp.perl.org has expired
all pages. Reading page1 again, gives a max of 3081942, that is 92
more. So I should find x1767 on page one or two. But it isn't. Page 1
has visible 1830-1942, page2 1594-1668!!! No shift-reload corrects for
that. Apparently immediately after the hour there are new pages and
old pages mixed. Of course my reading algorithm cannot cope with that.

Potential solution: goto nntp to fill holes.

=item why not nntp?

NNTP xhdr is far superior to the method deployed here. xpat would--in
theory--be even better but it is extremeley slow so that I believe
they first filter by pattern and then by range.

  % time perl -le '
      use Net::NNTP; use YAML::XS qw(Dump);
      use List::Pairwise qw(mapp grepp);
      my $nntp=Net::NNTP->new("nntp.perl.org");
      my(undef,undef,$last) = $nntp->group("perl.cpan.testers");
      my %fail = grepp { $b =~ /^FAIL / } %{$nntp->xhdr("Subject", [$last-200, $last])};
      print Dump \%fail;
      '
  ---   
  '4346542': FAIL Log-Accounting-SVN-0.01 i686-linux-thread-multi-ld 2.6.28-11-generic
  '4346547': FAIL Parse-SVNDiff-0.03 i686-linux-thread-multi 2.6.28-11-generic   
  '4346552': FAIL Tie-Scalar-MarginOfError-0.03 i686-linux-thread-multi-ld 2.6.28-11-generic
  '4346556': FAIL Pointer-0.11 i686-linux-thread-multi 2.6.28-11-generic
  [...]
  perl -le 0.08s user 0.02s system 0% cpu 10.059 total

=item why not a database?

With a database we could slowly fill the full 4 or 5 millions and
already use it for a web page. But then we should really work with
jifty or catalyst again. Or download the existing sqlite database and
then start adding to it?

Abandoning the current codebase because I'd have to delete nearly
everything here. Calling it cnntp-refresh.pl

=back

=cut

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use LWP::UserAgent;
use List::Util qw(max min);
use Pod::Usage qw(pod2usage);
use Set::IntSpan::Fast::XS ();
use Set::IntSpan::Fast ();
use URI ();
use XML::LibXML;
use YAML::Syck;

our %Opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

$Opt{group} ||= "perl.cpan.testers";
$Opt{baseuri} ||= "http://www.nntp.perl.org/group";

my $workdir = $Opt{workdir} ||= File::Spec->catdir(vardir(\%Opt),"workdir");
chdir $workdir
    or die("Couldn't change to $workdir: $!");

while () {
    my $page1 = read_default_page(\%Opt);
    my $page_uris = $page1->{current_paging};
    my $month = $page1->{month};
    my $db = read_local_db_for_month(\%Opt,$month);
    $db->{workdir} = $workdir;
    my $min_article = $db->{min_article}; # must be before merging!
    merge_page_into_db($page1,$db);
    my %pages_seen;
    $pages_seen{"page1.html"} = 1;
    $db->{pages_seen} = \%pages_seen;
    unless ($min_article) {
        # first time here in this month
        my($last_page_uri) = $page_uris->[-1];
        my $page2 = read_a_page($last_page_uri);
        merge_page_into_db($page2,$db);
        my($tpagename) = $last_page_uri =~ m|.*/([^/]+)$|;
        $pages_seen{$tpagename} = 1;
    }
 COLLECT: while () {
    PAGE: for my $uri (@$page_uris) {
            last COLLECT if have_seen_enough($db); # the only way out

            my($tpagename) = $uri =~ m|.*/([^/]+)$|;
            next PAGE if $pages_seen{$tpagename};

            my $page2 = read_a_page($uri);
            merge_page_into_db($page2,$db);
            write_current_month(\%Opt,$db);
            my $freshest_page_uris = $page2->{current_paging};
            my($higest_page_id_outer) = $page_uris->[-1] =~ /page(\d+)\.html/;
            my($higest_page_id_inner) = $freshest_page_uris->[-1] =~ /page(\d+)\.html/;
            if ($higest_page_id_outer < $higest_page_id_inner) {
                # they have renumbered
                $page_uris = $freshest_page_uris;
                %pages_seen = ();
                next COLLECT;
            }
            $pages_seen{$tpagename} = 1;
        }
        warn " ### highly suspect! ###";
        sleep 180;
    }
    write_current_month(\%Opt,$db);
    sleep 3600;
}

{
    my $ua;
    sub _ua {
        return $ua if $ua;
        $ua = LWP::UserAgent->new
            (
             keep_alive => 1,
             timeout => 300,
             env_proxy => 1,
            );
        $ua->parse_head(0);
        $ua;
    }
}

{
    my $xp;
    sub _xp {
        return $xp if $xp;
        $xp = XML::LibXML->new;
        $xp->keep_blanks(0);
        $xp->clean_namespaces(1);
        #my $catalog = __FILE__;
        #$catalog =~ s|ParseReport.pm$|ParseReport/catalog|;
        #$xp->load_catalog($catalog);
        return $xp;
    }
}

sub write_current_month {
    my($opt,$db) = @_;
    my $month = $db->{month};
    my $path = dbpath($opt,$month);
    mkpath dirname $path;
    my $set = Set::IntSpan::Fast->new($db->{set_as_string}||"");
    for my $a (keys %{$db->{articles}}) {
        $set->add($a);
    }
    $db->{set_as_string} = $set->as_string;
    YAML::Syck::DumpFile("$path.new", $db);
    rename "$path.new", $path or die "Could not rename: $!";
}

sub have_seen_enough {
    my($db) = @_;
    return unless exists $db->{min_article};
    my $shouldhave = 0;
    my $miss = 0;
    for (my $i = $db->{min_article}; $i <= $db->{max_article}; $i++) {
        $shouldhave++;
        if (!exists $db->{articles}{$i}) {
            $miss++;
        }
    }
    warn "missing[$miss]shouldhave[$shouldhave]\n";
    return 1 if $miss < 0.1 * $shouldhave;
    return 1 if $db->{pages_seen}{"page24.html"};
    return;
}

sub mybasename {
    my($p) = @_;
    $p =~ s|.*/||;
    return $p;
}

sub fetch_and_parse {
    my($uri,$as) = @_;
    $as ||= mybasename($uri);
 FETCH: {
        sleep 6; # let them breathe
        print STDERR "****Fetching $uri as $as...";
        my $resp = _ua->mirror($uri,$as);
        my $cheaders = "$as.header";
        if ($resp->is_success) {
            print STDERR "DONE\n";
            open my $fh, ">", $cheaders or die;
            for ($resp->headers->as_string) {
                print $fh $_;
                # print STDERR $_;
            }
        } elsif (304 == $resp->code) {
            print STDERR "DONE (not modified)\n";
            my $atime = my $mtime = time;
            utime $atime, $mtime, $cheaders;
        } elsif ($resp->code >= 500) {
            my $sleep = 120;
            warn sprintf
                (
                 "%s on downloading %s: %s; sleeping %d, then retrying",
                 $resp->code,
                 $uri,
                 $resp->status_line,
                 $sleep
                );
            sleep $sleep;
            redo FETCH;
        } else {
            warn sprintf
                (
                 "unexpected error on downloading %s: %s",
                 $uri,
                 $resp->status_line,
                );
            return;
        }
    }
    _preparse($as);
    my $xml = _xp->parse_html_file($as);
}

sub _preparse {
    my($file) = @_;
    my $content = do {open my $fh, $file or die; local $/; <$fh> };
    my $dumper = Dumpvalue->new(unctrl => "unctrl");
    if ($content =~ s/([\x00-\x09\x0b\x0c\x0e-\x1f])/ $dumper->stringify($1,1) /ge) {
        my(@stat) = stat $file or die;
        open my $fh, ">", $file or die;
        print $fh $content;
        close $fh or die;
        utime $stat[9], $stat[9], $file;
    }
}

sub read_default_page {
    my($opt) = @_;
    my $uri = "$opt->{baseuri}/$opt->{group}/";
    my $xml = fetch_and_parse($uri,"index.html");
    my $page = understand_a_page($xml,$uri);
}

sub read_a_page {
    my($uri) = @_;
    my $xml = fetch_and_parse($uri);
    my $page = understand_a_page($xml,$uri);
}

sub understand_a_page {
    my($xml,$baseuri) = @_;
    unless ($xml){
        require Carp;
        Carp::confess("understand_a_page called without or with an undefined \$xml argument");
    }
    # want month, pages, articles(subject,id,from,thread_leader)
    my($p1) = $xml->findnodes("/html/body/p[a[1] = 'Newest']");
    my @p1a = $p1->findnodes("a");
    shift @p1a while $p1a[0] && $p1a[0]->getAttribute("href") !~ m|/page\d|;
    pop @p1a while $p1a[-1] && $p1a[-1]->textContent =~ m|Oldest|;
    my @pages = map {
        URI->new($_->getAttribute("href"))->abs($baseuri)
    } @p1a;
    my($month) = $pages[0] =~ m|/(\d+/\d+)/page\d|;
    $month =~ s|/|-|g;

    my($table1) = $xml->findnodes("/html/body/table[\@class = 'article_list']");
    my @table1tr = $table1->findnodes("tr");
    my @articles = map {
        my($td1a) = $_->findnodes("td[1]/a");
        my $href = $td1a->getAttribute("href");
        my($id) = $href =~ /msg(\d+)\.html$/;
        my($td2) = $_->findnodes("td[2]");
        my($td3) = $_->findnodes("td[3]");
        my $from = $td3->textContent;
        $from =~ s/^\s+//;
        $from =~ s/\s+\z//;
        +{
          href => $href,
          id => $id,
          subject => $td1a->textContent,
          thread_leader => $td2->textContent =~ /^1\s/ ? 0 : 1,
          from => $from,
         }
    } @table1tr;
    for my $article (grep {$_->{thread_leader}>0} @articles) {
        my $thr_articles = read_thread_leader($article,$baseuri);
        push @articles, @$thr_articles;
    }
    return {
            baseuri => $baseuri, # for debugging
            month => $month,
            current_paging => \@pages,
            articles => \@articles,
           };
}

sub read_thread_leader {
    my($article,$baseuri) = @_;
    my $uri = URI->new($article->{href})->abs($baseuri);
    my $xml = fetch_and_parse($uri->as_string);
    $DB::single = !$xml;
    my($ul) = $xml->findnodes("/html/body/ul/ul");
    unless ($ul) {
        ($ul) = $xml->findnodes("/html/body/ul");
    }
    unless ($ul) {
        my $localtime = localtime;
        require YAML::Syck; print STDERR "Line " . __LINE__ . ", File: " . __FILE__ . "\n" . YAML::Syck::Dump({article => $article, baseuri => $baseuri, localtime => $localtime}); # XXX
        # e.g. http://www.nntp.perl.org/group/perl.cpan.testers/2009/01/msg3036775.html
        # which has some extreme html that cannot be parsed.
        return [];
    }
    my(@ul_li) = $ul->findnodes("li");
    my @articles;
    for (@ul_li) {
        my($a) = $_->findnodes("a") or next;;
        my $href = $a->getAttribute("href");
        my($id) = $href =~ /msg(\d+)\.html$/;
        my $subject = $a->textContent;
        $subject =~ s/^\s+//;
        $subject =~ s/\s+\Z//;
        my $from = $_->textContent;
        $from =~ s/.*by\s+//s;
        push @articles, {
                         href => $href,
                         id => $id,
                         subject => $subject,
                         thread_leader => 0,
                         from => $from,
                        };
    }
    return \@articles;
}

sub dbpath {
    my($opt,$month) = @_;
    sprintf "%s/%s.yml", vardir($opt), $month;
}

sub vardir {
    my($opt) = @_;
    sprintf
        (
         "%s/var/cnntp-scrape/%s",
         $ENV{HOME},
         $opt->{group},
        );
}

sub read_local_db_for_month {
    my($opt,$month) = @_;
    my $path = dbpath($opt,$month);
    my $y;
    if (-e $path) {
        $y = YAML::Syck::LoadFile($path);
    } else {
        $y = +{};
    }
    return $y;
}

sub merge_page_into_db {
    my($page,$db) = @_;
    if ($db->{month}) {
        die "Alert: mixing two months not allowed" if $page->{month} ne $db->{month};
    } else {
        $db->{month} = $page->{month};
    }
    my $max_page_article_id = max map {$_->{id}} @{$page->{articles}};
    if (!$db->{max_article} or $max_page_article_id > $db->{max_article}) {
        $db->{max_article} = $max_page_article_id;
    }
    my $min_page_article_id = min map {$_->{id}} @{$page->{articles}};
    if ($min_page_article_id < 2900000) {
        # $DB::single++;
        warn "broken page/baseuri[$page->{baseuri}] min_page_article_id[$min_page_article_id]";
    } else {
        if (!$db->{min_article} or $min_page_article_id < $db->{min_article}) {
            $db->{min_article} = $min_page_article_id;
        }
    }
    for my $article (@{$page->{articles}}) {
        my $id = $article->{id};
        $db->{articles}{$id} ||=
            {
             from => $article->{from},
             subject => $article->{subject},
            };
    }
}

1;
