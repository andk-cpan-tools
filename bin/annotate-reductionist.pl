#!/usr/bin/perl

# die " Deadly, don't use yet. We must implement date verification as well, at least ";

# Have eliminated 10 lines: Dancer2-Logger-Radis-0.001(=>0.002) Yote-0.1022(=>2.02) DBD-SQLAnywhere-2.08(=>2.13) Template-Mustache-1.0.0_0(=>1.1.0) Data-Rmap-0.64(=>0.65) Pcore-PDF-v0.1.0(=>v0.4.4) IO-Socket-Socks-0.71(=>0.74) Data-Sah-Resolve-0.003(=>0.007) Git-PurePerl-0.51(=>0.53) Archive-Zip-1.57(=>1_11)


# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--annotatefile=s>

Defaults to C<annotate.txt>. Filename to work on.

=item B<--debug|d!>

More output

=item B<--dry-run|n!>

Only the diagnostics, no overwrite

=item B<--help|h!>

This help

=item B<--max=i>

Stop after how many

=item B<--rand!>

If true, pick your sample randomly from all lines

=back

=head1 DESCRIPTION

Visits fastapi for each line of the annotate.txt file and deletes
lines that have a newer counterpart.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Getopt::Long;
use Hash::Util qw(lock_keys);
use Pod::Usage;
use Time::HiRes qw(sleep);
use Time::Piece;

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{annotatefile} //= "annotate.txt";

use LWP::UserAgent;
use JSON::XS;
use CPAN::DistnameInfo;
use List::AllUtils qw(reduce);
use CPAN::Version;

my $ua = LWP::UserAgent->new();
$ua->default_header("Accept-Encoding", "gzip");
my $jsonxs = JSON::XS->new->indent(0);

my @Sredu = my @Stmp = do { open my $fh, $Opt{annotatefile} or die; local $/="\n"; map {chop;$_} <$fh> };

my $max = defined $Opt{max} ? $Opt{max} : scalar @Stmp;
my $maxi = $max - 1;
my $sleep_on_error = 0.884;

my @DONE;

ANNO: for my $i (0..$maxi) {
    my $si = $Opt{rand} ? int rand @Stmp : $i;
    my($distv) = $Stmp[$si] =~ /(\S+)/;
    my %w = ( distv => $distv ); # work
    warn "\ndistv => $distv\n" if $Opt{debug};
    my $d = CPAN::DistnameInfo->new("A/AA/AAA/$distv.tar.gz");
    $w{version} = $d->version;
    warn "version     => $w{version}\n" if $Opt{debug};
    warn sprintf "left        => %d\n", $maxi-$i if $Opt{debug};
    my $query = sprintf "http://fastapi.metacpan.org/v1/release/_search?q=distribution:%s&fields=name,date,status,version,author&size=400&_source=tests", $d->dist;
    my $resp = $ua->get($query);
    unless ($resp->is_success) {
        warn sprintf "No success visiting '%s': %s; sleeiping %.3f\n",
            $query, $resp->code, $sleep_on_error;
        sleep $sleep_on_error;
        next ANNO;
    }
    # print $query;
    my $jsontxt = $resp->decoded_content;
    my $j = eval { $jsonxs->decode($jsontxt); };
    if (!$j || $@) {
        my $err = $@ || "unknown error";
        die "Error while decoding '$jsontxt': $err";
    }
    my $hits = $j->{hits}{hits};
    my($releasedate) = map { $_->{fields}{date} } grep { $_->{fields}{name} eq $distv } @$hits;
    unless ($releasedate) {
        warn "Did not find own releasedate for $distv\n";
        next ANNO;
    }
    warn "releasedate => $releasedate\n" if $Opt{debug};
    $w{cpanversion} = reduce {
        CPAN::Version->vgt($a,$b) ? $a : $b
        } map { $_->{fields}{version} } grep { $_->{fields}{date} ge $releasedate } @$hits;
    warn "cpanversion => $w{cpanversion}\n" if $Opt{debug};
    my $line = splice @Stmp, $si, 1;
    $w{eliminated} = 0;
    if (CPAN::Version->vgt($w{cpanversion},$w{version})) {
        my($highest_distro) = grep { $_->{fields}{version} eq $w{cpanversion} } @$hits;
        warn "releasedate => $highest_distro->{fields}{date}\n" if $Opt{debug};
        if (($highest_distro->{_source}{tests}{fail}||0) > 0) {
            my $fails = $highest_distro->{_source}{tests}{fail} == 1 ? "fail" : "fails";
            $w{whynot} = sprintf "found %d %s", $highest_distro->{_source}{tests}{fail}, $fails;
        } else {
            my $tepoch = Time::Piece->strptime($highest_distro->{fields}{date}, "%Y-%m-%dT%T")->epoch;
            if (time - $tepoch < 14*86400) {
                $w{whynot} = "too fresh";
            } else {
                my($redui) = grep { $Sredu[$_] eq $line } 0..$#Sredu;
                splice @Sredu, $redui, 1;
                $w{eliminated} = 1;
            }
        }
    }
    warn "eliminated  => $w{eliminated}\n" if $Opt{debug};
    if ($w{whynot}) {
        warn "whynot      => $w{whynot}\n" if $Opt{debug};
    }
    push @DONE, \%w;
}
my @elim = map {
    sprintf "%s(=>%s)", $_->{distv}, $_->{cpanversion};
} grep { $_->{eliminated} } @DONE;
if ($Opt{"dry-run"}) {
    my $lines = scalar @elim == 1 ? "line" : "lines";
    warn sprintf "Would have eliminated %d $lines: %s\n", scalar @elim, join(" ", @elim);
} else {
    my $lines = scalar @elim == 1 ? "line" : "lines";
    warn sprintf "Have eliminated %d %s: %s\n", scalar @elim, $lines, join(" ", @elim);
    open my $fh, ">", $Opt{annotatefile};
    print $fh "$_\n" for @Sredu;
    close $fh or die "Could not write $Opt{annotatefile}: $!";
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
