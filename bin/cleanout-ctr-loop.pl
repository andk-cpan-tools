#!/usr/bin/perl

=head1 NAME

....pl - 

=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut


my $optpod = <<'=back';

=item B<--fails-interactive!>

Only relevant, when C<--with-fails> is set.

Asks whether we want to send, ignore or discard a fail report;
C<ignore> means, the presented dirent will not be processed now and
stay in the C<ctr> directory (thus will show up next time again);
C<discard> means we put it into the C<discarded> directory; C<send>
means, it will be put into the sync directory;

One more option is C<quit> which will permanently set the option
C<--with-fails> to false for the rest of the running program; dirents
that have already been moved to the sync directory will be sent, all
other fail reports which are still in the C<ctr> directory will be
ignored.

=item B<--help|h!>

This help

=item B<--loop!>

Boolean defaults to true. When false, we stop after having gone
through the ctr directory once.

=item B<--transport-uri=s>

URI of the Metabase API, passed through to send_tr_reports.pl.

=item B<--with-fails!>

Boolean defaults to true since 2024-02-25 because it turned out we
have no recipe yet to deal with a shitload of fail reports that had
been held back;

=back

=head1 DESCRIPTION

~/var/ctr/ contains the three subdirs C<./sync/> and C<./process/> and
C<./done/>.

The move from the sync/ directory via the process/ directory to the
done/ directory is done by the send_tr_reports.pl script which we call
in the middle of our loop. If a file is later found in the process
directory, then we probably want to move it back to the ctr/ and watch
what happens.

Before that, this script here moves the candidates to be sent from the
ctr directory into the sync directory.

We use both sleep and inotify. From time to time we read the directory
anyway, but when inotify gets a CLOSE_WRITE event, then we do it
immediately. So if only one computer writes to the directory, we
rarely find two rpt files at once, most of the time only one and we
send it off as soon as it is 3 seconds old. If another computer writes
to the directory, inotify won't notice, and then we come at the usual
interval.

=head1 SEE ALSO

Who cleans up the reports in archive after a few years?
bin/smokehistory.pl

=head1 AUTHOR

=cut

use strict;
use ExtUtils::MakeMaker;
use Getopt::Long;
use File::Basename ();
use File::Path qw(mkpath);
use Pod::Usage qw(pod2usage);
use POSIX ();
use Linux::Inotify2;
use Time::HiRes qw(sleep);

my @opt = $optpod =~ /B<--(\S+)>/g;
our %Opt;
GetOptions
    (
     \%Opt,
     @opt,
    ) or pod2usage(1);

pod2usage(0) if $Opt{help};
$Opt{loop} //= 1;
$Opt{'with-fails'} //= 1;

use FindBin;
# use lib ("$FindBin::RealBin");

sub counting_inotify_backed_sleep ($$) {
    my($sleep,$inotify) = @_;
    my $eta = time+$sleep;
    local($|)=1;
    my $ts = POSIX::strftime "%FT%T", localtime;
    while () {
        my $left = $eta - time;
        last if $left < 0;
        my @events = $inotify->read;
        for my $ev (@events) {
            my $name = $ev->name;
            if ($name =~ /\.rpt/){
                # printf "!!! inotify event: %s\n", File::Basename::basename($name);
                print "!";
                return;
            }
        }
        printf "\r%s sleeping %d: %d    ", $ts, $sleep, $left;
        sleep 1;
    }
    print "\n";
}

my $ctr_dir = "/home/sand/var/ctr";
my $sync_dir = "$ctr_dir/sync";
my $discard_dir = "$ctr_dir/discarded";
my $inotify;
if ($inotify = new Linux::Inotify2) {
    $inotify->blocking(0);
    $inotify->watch
        (
         $ctr_dir,
         IN_CLOSE_WRITE()
        );
} else {
    warn "Unable to create new inotify object: $!";
}
$| = 1;
while () {
    my $t = time;
    my $ttgo = $t+300;
    {
        my $dh;
        # Could not opendir '/home/sand/var/ctr': Stale file handle at /home/sand/src/andk/andk-cpan-tools/bin/cleanout-ctr-loop.pl line 114.
        until (opendir $dh, $ctr_dir) {
            warn "Could not opendir '$ctr_dir': $!; Retrying in 117 seconds\n";
            sleep 117;
        }
        my $found_ctr = 0;
        my @dirent = readdir $dh;
      DIRENT: for my $dirent_i (0..$#dirent) {
            my $dirent = $dirent[$dirent_i];
            next DIRENT unless $dirent =~ /\.rpt$/;
            my $abs = "$ctr_dir/$dirent";
            unless (-s $abs){
                local $^T = time;
                if (-M $abs > 60/86400) {
                    unlink $abs;
                }
                next DIRENT;
            }
            if ($dirent =~ /^fail/) {
                if ($Opt{'with-fails'}) {
                    if ($Opt{'fails-interactive'}) {
                        print "\n===== BOF $abs =====\n\n";
                        system cat => $abs;
                        printf "\n\n===== EOF %s =====\n%d/%d\n", $abs, 1+$dirent_i, 1+$#dirent;
                        my $ans = ExtUtils::MakeMaker::prompt(" Send | Ignore | Discard | Quit ","s");
                        if ($ans =~ /^i/i) {
                            next DIRENT;
                        } elsif ($ans =~ /^d/i) {
                            my $abs_to = "$discard_dir/$dirent";
                            rename $abs, $abs_to or die "Could not rename $abs to $abs_to: $!";
                            next DIRENT;
                        } elsif ($ans =~ /^q/i) {
                            $Opt{'with-fails'} = 0;
                            next DIRENT;
                        }
                        # fall through
                    }
                    # fall through
                } else {
                    next DIRENT;
                }
            }
            if ($dirent =~ /^unknown/) {
                local $^T = time;
                if ( -M $abs > 7 ) {
                    unlink $abs or die "Could not unlink '$abs': $!";
                }
                next DIRENT;
            }
        WF_PROBABLY_CLOSED: while () {
                # wait until this file is older than 2 seconds
                local $^T = time;
                if (-M $abs > 3/86400) {
                    last WF_PROBABLY_CLOSED;
                } else {
                    sleep 1;
                }
            }
            my $abs_to = "$sync_dir/$dirent";
            rename $abs, $abs_to or die "Couldn't mv '$abs' '$abs_to': $!";
            $found_ctr = 1;
            print "+";
        }
        my $found_sync = 0;
        unless ($found_ctr) {
            opendir my $dh, $sync_dir or die "Could not opendir '$sync_dir': $!";
            for my $dirent (readdir $dh) {
                next unless $dirent =~ /\.rpt$/;
                my $abs = "$sync_dir/$dirent";
                next unless -s $abs;
                $found_sync = 1;
                last;
            }
        }
        if ($found_ctr || $found_sync) {
            print "\n";
            my @opt="$FindBin::RealBin/send_tr_reports.pl";
            if (my $tu = $Opt{'transport-uri'}) {
                push @opt, "--transport-uri=$tu";
            }
            unless (0 == system $^X, @opt) {
                my $sleep = 0.16;
                warn sprintf "ALERT: Running '%s %s' failed at %s UTC; sleeping $sleep", $^X, join(" ",@opt), scalar gmtime;
                $ttgo = time; # when we have a failing submission, we must come back here to work quickier
                sleep $sleep;
            }
            sleep 1;
        }
    }
    {
        my $donedir = "$ctr_dir/done";
        opendir my $dh, $donedir or die "Could not opendir $donedir\: $!";
        for my $dirent (readdir $dh) {
            my $abs = "$donedir/$dirent";
            next if -d $abs;
            my($todir) = $dirent =~ /(?:pass|fail|unknown|na)\.(.+?)-(?:v?\d)/;
            unless ($todir) {
                # lex EIJABB/MARC-Lint_1.53.tar.gz
                ($todir) = $dirent =~ /(?:pass|fail|unknown|na)\.(.+?)(?:v?\d)/;
            }
            die "no todir determined on dirent[$dirent]" unless $todir;
            my $first_letter = substr($todir,0,1);
            my $todir_abs = "$donedir/archive/$first_letter/$todir";
            -d $todir_abs or mkpath $todir_abs or die "could not mkdir $todir_abs\: $!";
            rename $abs, "$todir_abs/$dirent" or die "Could not rename $abs to $todir_abs\: $!";
        }
    }
    my $sleep = $ttgo - time;
    last unless $Opt{loop};
    if ($sleep >= 1 && $inotify) {
        counting_inotify_backed_sleep($sleep, $inotify);
    }
}
