#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION



=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

use Net::Pcap;
use Net::Pcap::Easy;
use Term::ANSIColor;

my $file = shift @ARGV or die "Usage:...";
my $dumper = new Dumpvalue tick => "\"";
my $pc;
{
    my $err;
    $pc = Net::Pcap::open_offline($file,\$err);
    die $err if $err;
}
my $cnt = 0;

XXX

my $open_connections = 0;
my @color_code =
    ("on_red",
     "on_green",
     "on_yellow",
     "yellow on_blue",
     "on_magenta",
     "on_cyan",
     "on_white",
    );
my(%reverse_portcolor, %open_connections, %startpack);
my(%Seen);

use Time::HiRes qw(gettimeofday); { our($cached_t, $cached_ts);sub itms {  my($t)=shift||[gettimeofday];  my $ts;  if ($cached_ts && $cached_t == $t->[0]){    $ts = $cached_ts;  } else { $cached_t = $t->[0]; $cached_ts = $ts = sub { sprintf q{%04d-%02d-%02dT%02d:%02d:%02d}, $_[5]+1900, $_[4] +1,@_[3, 2, 1, 0];}->(localtime $t->[0]);  }  return sprintf "%s.%06d", $ts, $t->[1];}}

my $npe = Net::Pcap::Easy->new
    (
     pcap => $pc,
     packets_per_loop => -1,
     udp_callback => sub {
         my ($npe, $ether, $ip, $udp, $header, $packet) = @_;
         $cnt++;

         # $DB::single = 1;

         # printf "%8d %-15s %-15s %5d %5d %d\n", $cnt, $ip->{src_ip}, $ip->{dest_ip}, $udp->{src_port}, $udp->{dest_port}, $udp->{data} ? length($udp->{data}) : 0;
         my $show = 1;
         if ( $Opt{port} ) {
             unless ( $udp->{src_port}==$Opt{port} || $udp->{dest_port}==$Opt{port} ) {
                 $show = 0;
             }
         }
         return unless $show;

         my(%dbmd);
         my(@dbmdfields) = qw(vers seq type status namelen keylen datalen data);
         @dbmd{@dbmdfields} = unpack "(N)7 A*", $udp->{data}; # typedef struct _dbmrsp from rz/lib/rdbm/dbmproto.h
         $dbmd{data} = "" if $dbmd{datalen} == 0 and $dbmd{namelen} == 0 and $dbmd{keylen}==0;

         my $color_on = color "on_white";
         if ( ! grep { $udp->{src_port} == $_ } 201, 203 ) {
             $color_on = color "black on_white";
         } elsif ( $ip->{src_ip} eq "192.168.96.210") { # effrafax
             $color_on = color "black on_magenta";
         } elsif ( $ip->{src_ip} eq "192.168.98.210") { # bartlett
             $color_on = color "black on_green";
         } elsif ( $ip->{src_ip} eq "192.168.96.253") { # greebo
             $color_on = color "black on_yellow";
         } elsif ( $ip->{src_ip} eq "192.168.100.223") { # deepthought
             $color_on = color "bold red on_magenta";
         } elsif ( $ip->{src_ip} eq "192.168.100.224") { # eddie
             $color_on = color "bold red on_yellow";
         } elsif ( $ip->{src_ip} eq "192.168.100.228") { # marvin
             $color_on = color "bold red on_green";
         }
         my $color_off = color "reset";
         my @record = map {defined $dbmd{$_} ? sprintf("%s%-7s %s%s\n", $color_on, $_, $color_off, $dbmd{$_}) : ()} @dbmdfields;
         my $record = join("", @record);
         printf
             (
              "%sUDP %5d %s %s:%s->%s:%s%s\n%s",
              $color_on,
              $cnt,
              itms([$header->{tv_sec}, $header->{tv_usec}]),
              $ip->{src_ip},
              $udp->{src_port},
              $ip->{dest_ip},
              $udp->{dest_port},
              $color_off,
              $record,
             );
     });

my $mcb = sub {
    my ($user_data, $header, $packet) = @_;
    my $ether = NetPacket::Ethernet->decode($packet);
    $npe->{_pp} ++;
    # $DB::single++;
    return $npe->_ipv4( $ether, NetPacket::IP  -> decode($ether->{data}), $header, $packet);
};

$npe->loop($mcb);



# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
