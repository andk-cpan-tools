#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--onlytar!>

Skip the run(s) with I<--focus != tar>

=back

=head1 DESCRIPTION

Calls bin/analysis-oldreports-extinction-program.pl several times with our favorite parameters

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

my $days_for_three = 240;
my @p = (
    { k => 3, m => $days_for_three, f => 'dir' }, # expensive, runs
                                                  # for a (feeled)
                                                  # hour, consider
                                                  # --onlytar
    { k => 12, m =>  20 }, # as long as we have more than 12, we remove everything older 20
    { k => 10, m =>  30 },
    { k =>  9, m =>  40 },
    { k =>  8, m =>  50 },
    { k =>  7, m =>  60 },
    { k =>  6, m =>  70 },
    { k =>  5, m =>  80 },
    { k =>  4, m =>  90 },
    { k =>  3, m => $days_for_three, f => 'tar' },
    { k =>  2, m => 1.2*$days_for_three }, # as long as we have more than 2, we remove everything older ...
    { k =>  1, m => 1.7*$days_for_three }, # as long as we have more than 1, we remove everything older ...
);

PARAMCOMBI: for my $p (@p) {
    $p->{k} ||= 3;
    $p->{m} ||= $days_for_three;
    $p->{f} ||= "tar";
    if ($Opt{onlytar} && $p->{f} ne 'tar') {
        warn "Info: skipping run for $p->{f} due 'onlytar'\n";
        next PARAMCOMBI;
    }
    my @system = ($^X,
                  "$FindBin::Bin/analysis-oldreports-extinction-program.pl",
                  "--yes",
                  "--untildf=48000000",
                  "--noni",
                  "--keepresults=$p->{k}",
                  "--minage=$p->{m}",
                  "--focus=$p->{f}",
                 );
    warn "Calling @system\n";
    0 == system @system or die "Error while running @system";
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
