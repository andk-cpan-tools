#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--dir=s>

Path to the directory containing all .slv files. Originally defaulted
to C</home/ftp/cnntp-solver-2009/workdir/solved>. For ds8143 changed
to C</home/andreas/data/cnntp-solver-2009/workdir/solved>

=item B<--help|h!>

This help

=item B<--name=s>

Name of a regression variable. Originally written for 1.005000_002.
Later defaulted to C<eq_1.005000_004>. Now changed to eq_0.99.

=item B<--pick=s@>

Instead of a readdir in the directory given in --dir, just pick the
names in this option. Useful for debugging.

=item B<--title=s>

Defaults to C<mod:Test::More>. Title of the regression.

=item B<--verbose!>

Reports which distro it is working on

=back

=head1 DESCRIPTION

Developed for the Test-Simple-1.005000_002 debakel in 2011. Parses all
.slv files we have for regressions matching title and variable and
shows R^2, Theta, and age of the slv file.

=head1 HISTORY

=head2 First incantation for 1.005000_002:

  /usr/local/perl-5.12.3/bin/perl -I CPAN-Blame/lib /tmp/pick-regression.pl | awk '$3<-0.75' | sort -nr -k 2 > pick-regression.out

Many hits with very low R^2 included. Slow. Started at 2011-12-29
11:37, finished at 11:51. Very stupid idea to filter in a pipe because
afterwards we want to see how many hits we have filtered out.

=head2 Second try (slightly manipulated):

  time /usr/local/perl-5.12.3/bin/perl -I CPAN-Blame/lib bin/pick-regression.pl | sort -nr --key=2,3 > pick-regression.out-20120101 &|

This gives us 840 lines among which we find plenty of tests working
well. Where do we want to cut?

  awk '$3<-0.75 && $2>0.35' pick-regression.out2 | wc
    103     309    6901

103 hits seems like a useful sample.

Issue posted: https://github.com/schwern/test-more/issues/249 with the
title C<100 candidates having bad experiences with 1.005000_002>

=head2 Third try 2013-10 when Test-Simple-0.99 was buggy:

I had no luck finding the interesting bits. I ran

  % awk '$3<=-0.75' pick-regression.out-Test-Simple-0.99-20131026-03 | sort -n -k 2

But for nearly all highscores it turned out that it was the co-variant
effect of having 0.99 in the newest results. Most of them were broken
for different reasons and since the newest tests were run with 0.99
they came out with high values. No idea what we can do about it short
of fixing *all* bugs. Or filter by gut feeling. A good thing is to
install previous versions again and again: this makes the older
version also appear among the candidates, not only the one we are
chasing. But it is not good enough. Open question.

=head2 Fourth try. Changing default for $Opt{name} to C< eq_1.302013_014 >.

2016-04-30: recommended: call me with C<< |& tee bin/pick-regression.out-`date +%F` >>
Now going to call with arguments: --name=eq_1.302014_009 and --title=mod:Test::More

=head2 Different name and title

  --title="qr:Unescaped left brace in regex is illegal"
  --name=eq_1

  % time ~/src/installed-perls/v5.16.0/4e6d/bin/perl -I CPAN-Blame/lib  bin/pick-regression.pl \
    --name=eq_1 --title="qr:Unescaped left brace in regex is illegal" \
    |& tee bin/pick-regression.out-`date +%FT%T`

This run took 3 hours.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);
use YAML::Syck;

use FindBin ();
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Model::Solved;

our %Opt;
lock_keys %Opt, map { /([^=!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

$Opt{dir} ||= "/home/andreas/data/cnntp-solver-2009/workdir/solved";
$Opt{name} ||= "eq_1.302013_014";
$Opt{title} ||= "mod:Test::More";

warn "title: $Opt{title}
name: $Opt{name}
";

opendir my $dh, $Opt{dir} or die "Could not opendir '$Opt{dir}': $!";
$|=1;
my @dirent;
if ($Opt{pick}) {
    @dirent = @{$Opt{pick}};
} else {
    @dirent = readdir $dh;
}
for my $dirent (@dirent) {
    next unless $dirent =~ /^(.+)\.slv$/;
    my $vdistro = $1;
    my $abs = "$Opt{dir}/$dirent";
    my $content = do {
        open my $fh, "<", $abs or die "Could not open: $!";
        local $/;
        <$fh>
    };
    my $age = -M $abs;
    my $yml_file = "$Opt{dir}/$vdistro.yml";
    my $author = "???";
    if (-e $yml_file) {
        my $y = YAML::Syck::LoadFile($yml_file);
        $author = $y->{author} if $y->{author};
    }
    if ($Opt{verbose}) {
        warn "Processing vdistro '$vdistro'\n";
    }
    my $regs = CPAN::Blame::Model::Solved->regression_tables($content);
    my($reg) = grep { $_->{name} eq $Opt{title} } @$regs or next;
    my($line) = grep { @$_ and $_->[0] =~ /='\Q$Opt{name}\E'/ } @{$reg->{regression}} or next;
    my $author_vdistro = sprintf "%s/%s", $author, $vdistro;
    printf "%-48s %-19s %6.3f %5.2f\n", $author_vdistro, $reg->{rsq}, $line->[1], $age;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
