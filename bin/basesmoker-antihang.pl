#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--longsleep=i>

After how many seconds we look again whether a new megalog file is
there. Defaults to 600.

=item B<--megalog=s>

Do not try to determine the megalog file, only follow this one.

=item B<--sleep=i>

After how many seconds we look again into the megalog file. Defaults to 15.

A longer value reduces the noise on the console, a shorter ensures
that we identify each problem earlier. For a while 120 was a good
default but then I discovered that PDL-2.006 was hung repeatedly
during testing and then I decided to reduce to 15. Now 150 in order to
have a whole day in one screen window. Update 2022-12-25: since we
were unable to kill Monoceros-0.28 on focal in a reasonable amount of
time, we once again switch back to 15.

=back

=head1 DESCRIPTION



=head1 BUGS

- distrofilenames hardcoded
- no globs

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}
our $HAVE_SHUFFLE;
BEGIN {
    $HAVE_SHUFFLE = eval { require Algorithm::Numerical::Shuffle; 1; };
}
use File::ReadBackwards;
use File::Which ();
use Getopt::Long;
use Hash::Util qw(lock_keys);
use List::Util qw(reduce);
use Pod::Usage qw(pod2usage);
use Proc::ProcessTable;
use Sys::Hostname ();

File::Which::which('lsof') or die "required external command 'lsof' not installed, refusing to run";

our %Opt;
lock_keys %Opt, map { /([^=|]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{sleep} = 15 unless defined $Opt{sleep};
$Opt{longsleep} = 600 unless defined $Opt{longsleep};

sub counting_sleep ($) {
    my $sleep = shift;
    my $start_time = time;
    my $start_time_str = scalar localtime $start_time;
    my $eta = $start_time + $sleep;
    local($|)=1;
    while () {
        my $left = $eta - time;
        last if $left < 0;
        printf "\r%s: sleeping %d: %d    ", $start_time_str, $sleep, $left;
        sleep 1;
    }
    print "\n";
}

sub last_megalog () {
  PICKMEGA: while () {
        my $dh;
        until (opendir $dh, ".") {
            warn "Couldn't opendir .: $!; Sleeping\n";
            counting_sleep $Opt{sleep};
        }
        my @have_mtime = grep { -e $_ } readdir $dh;
        local($^T) = time;
        my @mfiles = grep { -M $_ < 60/86400 }
            grep { /megalog-\d+-\d+-\d+T\d+:\d+:\d+.log$/ } @have_mtime;
        unless (@mfiles) {
            warn "Couldn't find any recently changed megalog file; Sleeping\n";
            counting_sleep $Opt{longsleep};
            next PICKMEGA;
        }
        if ($HAVE_SHUFFLE) {
            Algorithm::Numerical::Shuffle::shuffle(\@mfiles);
        } else {
            @mfiles = sort { $b cmp $a } @mfiles;
        }
        my $self_host;
        for my $mfile (@mfiles) {
            open my $fh, "<", $mfile or next;
            local $/ = "\n";
            while (<$fh>) {
                next unless m{^perl\|-\>\s+\S+installed-perls/(?:perl|host/([^/]+))/};
                my $megalog_host = $1 || "k83";
                $self_host //= Sys::Hostname::hostname;
                if ($self_host eq $megalog_host) {
                    return $mfile;
                }
            }
        }
        warn "Couldn't find an interesting megalog file; Sleeping\n";
        counting_sleep $Opt{longsleep};
    }
}

{
my $Lfile;
my $Lsize;
sub megalog_is_growing ($) {
    my($file) = @_;
    my $size0;
    if ($Lfile && $Lfile eq $file) {
        $size0 = $Lsize;
    } else {
        $size0 = -s $file;
        my $sleep = 3; # arbitrary
        counting_sleep $sleep;
    }
    my $size1 = -s $file;
    $Lfile = $file;
    $Lsize = $size1;
    my $growing;
    if ($size1 > $size0) {
        $growing = 1; # "growing";
    } else {
        $growing = 0; # "NOT growing";
    }
    warn sprintf "watching %s%s growing\n", $file, $growing ? "" : " NOT";
    return $growing;
}
}

sub kill_stalled_leaves ($) {
    my($proc) = @_;
    my $ppt = Proc::ProcessTable->new( 'enable_ttys' => 0 );
    my $ref = $ppt->table;
    my %parent_map = ($proc => 1);
    my %family;
    while () {
        my $keys = keys %parent_map;
        for my $p (@$ref) {
            next if $p->state eq "defunct"; # zombie
            if (exists $parent_map{$p->pid} && ! exists $family{$p->pid}) {
                $family{$p->pid} = $p;
            }
            next unless $parent_map{$p->ppid};
            $parent_map{$p->pid} = $p->ppid;
        }
        next if keys %parent_map > $keys;
        last;
    }
    my $FORMAT = "%-6s %-6s %-8s %-24s %s\n";
    printf($FORMAT, "PID", "PPID", "STAT", "START", "COMMAND");
    my @tokill;
    my %map2 = %parent_map;
    while (%map2) {
        my %values = map {($_ => 1)} values %map2; # parents
        my @leaves = grep { ! $values{$_} } keys %map2;
        unless (@tokill) {
            @tokill = @leaves;
        }
        my $tp = pop @leaves;
        delete $map2{$tp};
        my $p = delete $family{$tp}
            or die "Panic: unexpected non-value in family tp='$tp'";
        printf($FORMAT,
               $p->pid,
               $p->ppid,
               $p->state,
               scalar(localtime($p->start)),
               $p->cmndline);
    }
    warn "ATTENTION: About to kill @tokill\n";
    my $signaled = kill 15, @tokill;
    warn "successfully signaled $signaled processes with signal 15\n";
    sleep 2;
 TOKILL: for my $pid (@tokill) {
        if (kill 0, $pid) {
            warn "process $pid still alive? killing with SIGKILL\n";
            if (kill 9, $pid) {
                warn "sent signal successfully\n";
            } else {
                warn "could not signal $pid\: $!\n";
            }
        } else {
            warn "verified that $pid is dead\n";
        }
    }
}

# these times are observed values how long the slowest
# operation of each distro can take without output
# when it is not hanging. undef stands for not
# measured
my $TIMEOUTS =
{
    "ARCv2-1.05"                     => 60, # seen 4
    "Acme-FSM-v2.3.5"                => 180, # seen 62
    "Acme-Tests-0.03"                => 30, # seen hanging
    # "WOLDRICH/Acme-DependOnEverything-0.06.tar.gz"
    "Acme-DependOnEverything-0.06"   => 60, # scheint ein Bremser zu sein
    "Algorithm-LinearManifoldDataClusterer-1.01" => 60, # seen 2
    "Alien-Boost-ProgramOptions-1.7" => 1000, # seen 2, aber auch 418, da passiert irgendein Download
    "Alien-FLTK2-0.09296"            => undef,
    "Alien-KentSrc-0.4"              => 60, # seen 1, but also 2971
    "Alien-Plotly-Kaleido-0.003"     => 120, # seen 1; still runs 2000 seconds because we can hardly kill it
    "Alien-poetry-1.000006"          => 60, # downloads plenty of stuff, seems like it never finishes to download
    "Alien-QtSmoke-4.3.3"            => 60, # pass:fail=0:0 since 2009
    "Alien-SDL-1.444"                => undef,
    "Analizo-1.23.0"                 => 600, # seen 190
    "AnyEvent-AggressiveIdle-0.04"   => 120, # seen 3
    "AnyEvent-Fork-Pool-1.2"         => 30, # seen 2
    "AnyEvent-Gearman-0.10"          => 60, # seen 6, seen hanging with 5.22.2
    "AnyEvent-IRC-Server-0.03"       => 60, # seen 6
    "AnyEvent-JSONRPC-Lite-0.15"     => undef,
    "AnyEvent-RetryTimer-0.1"        => 60, # seen 2
    "AnyEvent-SIP-0.002"             => 60, # https://github.com/xsawyerx/anyevent-sip/issues/1
    "AnyEvent-Tickit-0.01"           => 60, # seen nothing
    "Apache-SessionX-2.01"           => undef,
    "App-shcompgen-0.321"            => 60, # seen 0.48
    "App-Tel-0.201601"               => 60, # seen 3
    "Argon-0.18"                     => 60, # seen 6
    "Attean-0.032"                   => 480,, # seen 175
    "Authen-Krb5-Easy-0.90"          => 30, # http://www.cpantesters.org/cpan/report/8f9d58e6-fbd7-11e6-91d1-8b9078ddaa53 looks like waiting for input, released 2002
    "DB_File-SV18x-kit-0.06"         => 60, # did not look
    "Badger-0.09"                    => 60, # seen 9, seen hanging as 5.12.5
    "Benchmark-Thread-Size-0.09"     => 60, # seen 3, seen > 2200 with 5.35.10
    "Bio-Medpost-0.04"               => 60, # seen nothing, abandoned
    "Business-OnlinePayment-Exact-0.01" => 60, # seen nothing
    "CGI-Application-4.50"           => 30,
    "CGI-Application-Server-0.063"   => 30, # seen 3
    "CGI-Compile-0.26"               => 180, # seen 1, 2, 26, 46; 2020-08-03
    "CGI-Debug-1.0"                  => 30, # abandoned since 2000?
    "CPANPLUS-Daemon-0.02"           => 120, # seen 1
    "CPAN-Reporter-Smoker-0.29"      => 300, # seen 41, but today >4000
    "CPAN-Testers-WWW-Reports-Query-Report-0.05" => 120, # no passes with perl >= 5.34
    "CSS-Inliner-3935"               => 10, #3934 whole job took 5 seconds
    "CSS-Sass-3.3.6"                 => 60, # seen 7, seen hanging with 5.12.5
    "Cache-Memcached-Semaphore-0.3"  => 60, # seems abandoned
    "Catalog-0.5"                    => 60, # 0 reports ever, fills disk quickly with "(default /home/httpd/cgi-bin, type '' for empty string) cgidir : /home/httpd/cgi-bin is not an existing directory"
    "Catalog-1.02"                   => 60, # 0 reports ever, fills disk quickly with "(default /home/httpd/cgi-bin, type '' for empty string) cgidir : /home/httpd/cgi-bin is not an existing directory"
    "Catalyst-View-Reproxy-0.05"     => 120, # seen 13
    "Catmandu-XSD-0.05"              => 60, # seen 10
    "Chart-GRACE-0.95"               => 60, # declared abandoned in 01.DISABLED
    "Class-Std-0.011"                => 10,
    "Cluster-Init-0.215"             => 120, # seen 4332! pass:fail:unknown=(0/7/1), abandoned since 2003
    "Concurrent-Object-1.07"         => 30, # seen 1
    "Continuity-1.6"                 => 30, # seen 2 (without skipping tests)
    "Coro-6.41"                      => 10,
    "Costume-Doublet-0.001"          => 120, # seen 0
    "DBIx-Broker-1.14"               => 60, # seen nothing, only red but one unavailable one
    "DBIx-ObjectMapper-0.3013"       => undef,
    "DBIx-SQLCrosstab-1.17"          => 300, # watched 120
    "DBIx-XMLServer-0.02"            => 60,
    "DBD-iPod-0.01"                  => 60, # seen 0
    "DDCCI-0.003"                    => 60, # seen 0: interactively asks whether we have a DDC/CI standard compliant monitor attached
    "DJabberd-0.85"                  => undef,
    "Daemon-Device-1.07"             => 60, # seen 7
    "Data-RecordStore-6.06"          => 60, # seen 2
    "Data-STUID-0.01"                => 60, # nothing seen
    "Data-Serializer-Sereal-1.05"    => 60, # seen 0
    "Date-LastModified-0.60"         => 120, # seen 30
    "Debug-Client-0.31"              => 120, # seen 38
    "Devel-Debug-DBGp-0.22"          => 120, # seen 21
    "Devel-GlobalDestruction-XS-0.03"=> 60, # seen 1, hanger on 5.8.2
    "Devel-TrackSIG-0.03"            => 10,
    "Devel-Trepan-0.73"              => 300,
    "Devel-Trepan-v1.0.0"            => 300,
    "Dist-Zilla-Plugin-StaticInstall-0.012" => 120, # seen 19
    "Dpkg-1.21.10"                   => 60, # seen 14
    "ETL-Yertl-0.043"                => 300, # seen 42
    "Emacs-EPL-0.7"                  => 30, # seen 1
    "Enbugger-2.016"                 => 300, # seen 38-96
    "Event-RPC-1.10"                 => 120, # seen 8
    "FAQ-OMatic-2.702"               => 60, # nothing seen
    "FAQ-OMatic-2.717"               => 60, # seen 1
    "FAQ-OMatic-2.719"               => 60, # seen 1
    "FFI-Platypus-0.40"              => 300, # seen 160
    "Feersum-1.405"                  => 30, # fails always anyway
    "File-BSDGlob-0.94"              => 30, # seen 1 https://rt.cpan.org/Ticket/Display.html?id=95586
    "File-Flock-2014.01"             => 90, # seen 14
    "File-Lock-Multi-1.02"           => 90, # seen 19
    "Forecast-IO-0.21"               => 30, # asks questions, https://rt.cpan.org/Public/Bug/Display.html?id=86798
    "FreeRADIUS-Database-0.06"       => 90, # seen 8
    "Games-AlphaBeta-v0.4.7"         => undef,
    "Games-Baseball-Scorecard-0.03"  => 10, # seen 0
    "Games-Rezrov-0.15"              => 120, # seen nothing
    "Games-Rezrov-0.20"              => 120, # seen nothing
    "Gearman-1.12"                   => 180, # seen 17
    "Gearman-Driver-0.02008"         => 600, # seen 104
    "Gearman-Starter-0.03"           => 60, # seen 2
    "Gearman-SlotManager-0.3"        => 10, # 2012, pass:fail=10:270
    "GH-0.69"                        => 10, # pass:fail=6:270
    "Hardware-1Wire-HA7Net-1.01"     => 60, # only red
    "Hash-SharedMem-0.005"           => 300, # seen 130
    "HPCI-0.75"                      => 900, # seen 432: difficult to kill, leave is a sleep most of the time
    "HTML-EmbeddedPerl-0.91"         => 30, # seen 0, https://rt.cpan.org/Ticket/Display.html?id=88731
    "HTML-EP-0.1135"                 => 10, # not current, but is indexed for HTML::EP::Examples::Glimpse
    "HTTP-Proxy-0.304"               => 30, # have reported the hang
    "HTTP-ProxySelector-Persistent-0.02" => 60, # seen nothing
    "HTTP-Server-EV-0.69"            => 60, # seen 3
    "HTTP-Server-Simple-0.52"        => 180, # seen 20
    "Image-Signature-0.01"           => 60, # seen 13, ~/.cpan/plugins/CPAN::Plugin::TraceDeps/20210806T140441.log
    "IO-EventMux-2.02"               => 60, # seen 1, https://rt.cpan.org/Ticket/Display.html?id=120205
    "IO-Socket-ByteCounter-v0.0.2"   => 60, # seen 0
    # OLEG/IO-Socket-Socks-Wrapper-0.17.tar.gz
    "IO-Socket-Socks-Wrapper-0.17"   => 120, # seen 40; t/10_event_loop_mojo.t ...... skipped: Mojolicious 4.85+ required for this test
    "IPC-AnyEvent-Gearman-0.8"       => 180, # seen 29
    "IPC-Exe-2.002001"               => 5,
    "IPC-Pipeline-0.8"               => 30,
    "IPC-Shareable-1.05"             => 120, # seen 13
    "IPC-Transit-1.171860"           => 300, # seen 107, only fails
    "IS-Init-0.93"                   => 300, # seen 174
    "JIRA-REST-Class-0.12"           => 60, # seen 3
    "JTM-Boilerplate-2.190700"       => 60, # seen 3
    "JTM-Boilerplate-2.211420"       => 60, # did not look
    "JTM-Boilerplate-2.241690"       => 60, # did not look
    "Job-Async-0.004"                => 60, # seen 1
    "Jvm-0.9.2"                      => 60, # seen nothing
    "Luka-1.07"                      => 60, # pass:fail = 0:74; was hanging for two hours
    "LWP-Protocol-Coro-http-v1.0.7"  => 10,
    "Lingua-POSAlign-0.01"           => 10, # seen 0
    "MListbox-1.11"                  => 120, # nothing seen
    "MMM-Text-Search-0.07"           => 300, # seen nothing
    "Mail-Abuse-1.026"               => 60, # just added to abandoned
    "Mail-MtPolicyd-2.05"            => 60, # seen nothing
    "Mail-SendEasy-1.2"              => 60, # seen nothing
    "Mail-SpamAssassin-Contrib-Plugin-IPFilter-1.2" => 180, # seen 25
    "Mail-Webmail-Yahoo-0.601"       => 60, # seen 1
    "Mason-Tidy-2.57"                => 60, # seen 1
    "Math-BigInt-Random-OO-0.04"     => 300, # seen 70, see https://rt.cpan.org/Ticket/Display.html?id=132452
    "Math-Matlab-0.08"               => 60, # seen 1
    "Math-Prime-Util-0.73"           => 60, # seen 7; https://rt.cpan.org/Ticket/Display.html?id=139739
    "MediaWiki-Bot-5.006003"         => 60, # seen 17
    "Minion-Backend-Storable-7.012"  => 60, # seen 3
    "Module-Build-0.4005"            => 60, # eh schon alt, egal
    "Module-Build-Convert-0.49"      => 60, # seen 1
    "Module-Refresh-0.17"            => 60, # seen 2, seen hanging in v5.12.5
    "Mojolicious-Plugin-AssetPack-1.13" => 120, # seen 25
    "Mojolicious-Plugin-AssetPack-1.24" => 120,
    "Mojolicious-Plugin-Minion-Starter-0.005" => 60, # seen 3
    "Mojolicious-Plugin-OAuth2-Server-0.48" => 120, # seen 20
    "Mojo-ACME-0.13"                 => 60, # seen 1
    "Mojo-Promise-Role-Repeat-0.006" => 120,
    "Monoceros-0.29"                 => 180, # seen 90
    "MooseX-Workers-0.24"            => 60, # seen 10
    "MySQL-TableInfo-1.01"           => 60, # nothing seen
    "Net-AMQP-RabbitMQ-0.007001"     => undef,
    "Net-AMQP-RabbitMQ-1.400000"     => 110, # seen 53 total, but no stalls>127
    "Net-AMQP-RabbitMQ-2.30000"      => 110,
    "Net-Async-Blockchain-0.003"     => 120, # seen 8
    "Net-Dropbear-0.16"              => 800, # seen 460 and 17
    "Net-FTPServer-1.125"            => 60, # much red, no release since 2012
    "Net-Goofey-1.4"                 => 60, # seen nothing
    "Net-HTTPS-NB-0.15"              => 60, # seen 7
    "Net-LDAP-Server-Test-0.14"      => undef,
    "Net-Netcat-0.05"                => undef,
    "Net-OpenID-JanRain-1.1.1"       => 60, # only red
    "Net-OpenSRS-0.06"               => 60, # seen "No tests defined"
    "Net-Proxy-Type-0.09"            => 60, # seen 7
    "Net-Statsd-Server-0.20"         => 60, # seen 11
    "Net-WhoisNG-0.09"               => 60, # seen 1; only fails since 2018
    "NetServer-Generic-1.03"         => 3600, # haengt, aber am Schluss gibts ok; heute aber nicht bei k93bionic/v5.31.2/2aaf
    "Number-Phone-3.5000"            => 600, # seen 420
    "Number-Phone-AU-0.02"           => 60, # seen 1
    "ODS-0.04"                       => 60, # seen 9
    "Object-Lazy-0.13"               => 30,
    "OpenFrame-Segment-Apache-1.20"  => 60, # seen 1
    "Otogiri-Plugin-BulkInsert-0.02" => 60, # seen 4
    "PBJ-JNI-0.1"                    => 120, # nothing seen
    "PDF-Create-1.43"                => 60, # seen 3
    "PDL-2.006"                      => undef,
    "POE-Component-Client-HTTP-0.949" => 120, # seen 33
    "POE-Component-Client-Keepalive-0.272" => 300, # seen 40
    # BINGOS/POE-Component-CPAN-YACSmoke-1.38.tar.gz
    "POE-Component-CPAN-Reporter-0.08" => 120, # seen 7
    "POE-Component-CPAN-YACSmoke-1.38" => 180, # seen 7
    "POE-Component-DirWatch-0.300004" => 180, # seen 26
    "POE-Component-Server-Inet-0.06" => 120, # seen 34
    "POE-Component-Server-SimpleHTTP-PreFork-2.10" => 1200, # seen 305
    "POE-Component-WWW-Pastebin-Bot-Pastebot-Create-0.003" => 60, # seen 0
    "POE-Loop-IO_Async-0.004"        => 120, # seen 36
    "POE-Quickie-0.18"               => undef,
    "POEx-Role-PSGIServer-1.150280"  => 30, # DEPREACATED
    "POE-XS-Loop-Poll-1.000"         => 180, # seen 74
    "POEx-Tickit-0.04"               => 60,
    "PPerl-0.25"                     => 60, # seen nothing
    "Parallel-DataPipe-0.12"         => 120, # seen 1
    "PerlQt-3.006"                   => 120, # seen 13
    "PerlQt-3.008"                   => 120, # seen 13
    "Perl6-Pod-Slide-0.10"           => 60, # seen 2
    "Plugin-Simple-1.01"             => 120, # seen 5
    "Pod-Trial-LinkImg-0.005"        => 30, # seen 1
    "Proch-N50-1.5.0"                => 60, # seen 3
    "Project-Euler-0.20"             => 10,
    "Prophet-0.751"                  => 120, # seen 40
    "RAS-HiPerARC-1.03"              => 60, # seen "Terminated" in http://www.cpantesters.org/cpan/report/4a40775e-7d78-11e9-ac9a-059c23201cad
    "RAS-PortMaster-1.16"            => 120, # seen nothing
    "RT-Client-REST-0.43"            => 20,
    "RT-Extension-ShiftPlanning-0.01.tar.gz" => 20, # seen 1
    "Redis-1.995"                    => 180, # seen 75
    "Redis-Jet-0.09"                 => 60, # seen 2
    "Rx-0.53"                        => 30, # asks questions
    "SNMP-Util-1.8"                  => 60, # abandoned
    "Search-Sitemap-2.13"            => 120, # seen 6
    "Senna-0.51"                     => 60, # https://rt.cpan.org/Ticket/Display.html?id=105318
    "Server-Starter-0.12"            => undef,
    "Server-Starter-0.19"            => undef,
    "Server-Starter-0.32"            => undef,
    "Server-Starter-0.34"            => 300, # seen 94
    "Sport-Analytics-NHL-1.53"       => 300, # seen 152
    "StandupGenerator-0.5"           => 60, # only red results and hanging
    "Starlet-0.31"                   => 300, # seen 16
    "Starman-0.4015"                 => 360, # seen 152
    "Sub-Call-Tail-0.05"             => 1,
    "Sudo-0.33"                      => 1800, # seen 941
    "Sybase-Xfer-0.1"                => 123, # nothing seen
    "Sybase-Xfer-0.63"               => 123, # nothing seen
    "Syntax-Keyword-Try-0.10"        => 60, # seen 4
    "Sys-SigAction-0.23"             => 90, # seen 11
    "Tangram-2.04"                   => 60, # nothing seen
    "Tapper-Testplan-4.1.2"          => undef,
    "Tcl-pTk-1.09"                   => 420, # seen 163
    "Test-HTTP-Server-Simple-0.11"   => 60, # seen 0
    "Test-Moose-More-0.027"          => 1, # haengt anscheinend immer, braucht keine Chance
    "Test-SFTP-1.10"                 => 180, # seen 24
    "Test-WWW-Simple-0.39"           => 120, # seen 38
    "Text-Glob-DWIW-0.01"            => 120, # seen 17
    "Thread-Isolate-0.05"            => 180, # seen 26
    "Thread-Queue-Multiplex-0.92"    => 1200, # seen 816
    "Thread-Workers-0.06"            => 60, # seen 11
    "Tk-804.030"                     => 10,
    "Tk-GridEntry-1.0"               => 60,
    "Tk-LockDisplay-1.3"             => 30, # nothing seen
    "Tk-MListbox-1.11"               => 120, # nothing seen
    "Tk-PopEntry-0.06"               => 60, # nothing seen
    "Tk-QuickTk-0.92"                => 60, # seen 0
    "Tk-TM-0.53"                     => 60,
    "Tk-Updown-1.0"                  => 30, # seen hanging when Tk is installed
    "TkUtil-Configure-0.03"          => 10, # seen 0
    "TripleStore-0.03"               => 0, # seen 60
    "Twiggy-0.1025"                  => 120, # seen 6
    "Twiggy-Prefork-0.08"            => 120, # seen 8
    "UNIVERSAL-isa-1.20171012"       => 60, # seen 1
    "Vim-Debug-0.904"                => 120, # seen 14
    "WWW-Google-Groups-0.09"         => 60, # always red
    "WWW-Search-Googlism-0.02"       => 60, # released 2003
    "WWW-VieDeMerde-0.21"            => 60, # seen nothing
    "WWW-UsePerl-Journal-0.26"       => 60, # seen 21
    "WebService-RESTCountries-0.3"   => 60, # seen 7 https://github.com/kianmeng/webservice-restcountries/issues/1
    "WebService-Async-UserAgent-0.006" => 60, # seen 1
    "What-1.00"                      => 60, # seen 0
    "Win32-MSAgent-0.07"             => 60, # nothing seen
    "Winamp-Control-0.2.1"           => 60, # vermute userinputversuch
    "X11-Protocol-0.56"              => 60, # seen ok 1..3
    "XAO-MySQL-1.0"                  => 10, # sometimes hangs
    "XML-ExtOn-0.17"                 => 60, # seen 1
    "XML-Grammar-Fortune-0.0501"     => undef,
    "Yandex-Disk-0.07"               => 2400, # seen 1775
    "ZeroMQ-PubSub-0.10"             => undef,
    "code-UnifdefPlus-v0.4.0"        => 60, # nothing seen
    "go-db-perl-0.04"                => 60, # no pass since 2009
    "forks-0.36"                     => 300, # seen 149
    "math-image-109"                 => undef,
    "nsapi_perl-0.24"                => 60, # seem nothing
    "perldap-1.4"                    => 60, # look like abandoned since 1999
};

while () {
    my $file = $Opt{megalog} || last_megalog;
    if (megalog_is_growing($file)) {
        my $bw = File::ReadBackwards->new( $file ) or
            die "can't read '$file' $!" ;
        my $log_line;
        my $proc;
        my $max = 0;
        my $min = 9999999999;
        while( defined( $log_line = $bw->readline ) ) {
            # hanging Starlet:
            next if $log_line =~
                /^(\QCan't exec "": No such file or directory at\E
                 |\Qnew worker \E\d+\Q seems to have failed to start, exit status\E
                 |\Qstarting new worker\E
                 )/x;
            last unless $log_line =~ /====/;
            next unless $log_line =~ /={10}monitoring proc (\d+) perl (\S+) secs ([0-9\.]+)={7}/; # external invariable
            $proc = $1;
            my $perl = $2;
            my $time = $3;
            $max = $time if $time > $max;
            $min = $time if $time < $min;
        }
        if ($proc) {
            my $stalled = $max - $min;
            if ($stalled > 3) { # arbitrary
                my $cwd = `lsof -p $proc | awk '\$4=="cwd"{print \$9}'`;
                chomp $cwd;
                warn "process $proc stalled for $stalled seconds in $cwd\n";
                my $timeout = 3600; # arbitrary
                while (my($distro,$ttimeout) = each %$TIMEOUTS) {
                    if ($cwd =~ m!/\Q$distro\E-!) {
                        $timeout = $ttimeout || 120;
                        last;
                    }
                }
                keys %$TIMEOUTS; # do we need to reset the iterator here?
                if ($stalled >= $timeout) { # arbitrary
                    warn sprintf "no output for %d seconds (timeout=%d)\n", $stalled, $timeout;
                    kill_stalled_leaves ($proc);
                }
            } else {
                warn "working OK\n";
            }
        } else {
            warn "is busy\n";
        }
    }
    counting_sleep $Opt{sleep}; # arbitrary
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
