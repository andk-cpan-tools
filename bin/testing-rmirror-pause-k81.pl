#!/usr/local/perl-5.10-uld/bin/perl

$ENV{USER}="andk"; # fill in your name
$ENV{RSYNC_PASSWORD} = shift or die; # fill in your passwd

use File::Rsync::Mirror::Recent;
my @rrr = map {
    File::Rsync::Mirror::Recent->new
            (
             ignore_link_stat_errors => 1,
             localroot => "/home/ftp/pub/PAUSE/$_",
             remote => "pause.perl.org::PAUSE/$_/RECENT.recent",
             max_files_per_connection => 200,
             rsync_options =>
             {
              port => 8732, # only for PAUSE
              compress => 1,
              links => 1,
              times => 1,
              timeout => 600,
              checksum => 0,
             },
             ttl => 10,
             verbose => 1,
             # verboselog => "/var/log/rmirror-pause.log",
             runstatusfile => "/var/log/rmirror-status-$_.state",
            )} "authors", "modules";
die "directory $_ doesn't exist, giving up" for grep { ! -d $_->localroot } @rrr;
while (){
    my $ttgo = time + 20;
    for my $rrr (@rrr){
        $rrr->rmirror ( "skip-deletes" => 1 );
    }
    my $sleep = $ttgo - time;
    if ($sleep >= 1) {
        # print STDERR "sleeping $sleep ... ";
        sleep $sleep;
    }
}
