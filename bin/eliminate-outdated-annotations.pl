#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Read from stdin a list of distros to eliminate. Rewrite annotate.txt
without them.

Clobbers annotate.txt without asking.

Was in use the first time 2013-11-02 and eliminated 784 annotation.
History on k83 says:

2014-02-20 03:38  ssh andreas@analysis cat filter-outdated-from-analysis-logfile.out | perl bin/eliminate-outdated-annotations.pl

The file filter-outdated-from-analysis-logfile.out was generated on analysis:

2014-02-20 02:36  ~/src/installed-perls/v5.16.0/4e6d/bin/perl bin/filter-outdated-from-analysis-logfile.pl bin/cnntp-solver.sh.out | tee ~/filter-outdated-from-analysis-logfile.out

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

our %M;
while (<>) {
    my($distro, $desc) = split " ", $_, 2;
    my $type = "eq";
    if ($distro =~ s/\.\.\.$//) {
        $type = "substr";
    }
    $M{$type}{$distro} = $desc;
}

my @lines = do { open my $fh, "annotate.txt" or die; local $/ = "\n"; <$fh> };
open my $fh, ">", "annotate.txt" or die;
my $skipped = 0;
ANNO: for my $line (@lines) {
    my($distro) = $line =~ /(\S+)/;
    if (exists $M{eq}{$distro}) {
        $skipped++;
        next ANNO;
    } else {
        for my $substr (keys %{$M{substr}}) {
            if (substr($distro,0,length($substr)) eq $substr) {
                $skipped++;
                next ANNO;
            }
        }
    }
    print $fh $line;
}
warn "rewrote annotate.txt and skipped[$skipped]\n";

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
