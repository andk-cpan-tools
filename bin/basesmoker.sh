#!/bin/sh

echo Deprecated, please use basesmoker.run.pl
exit

PERL=/home/sand/src/perl/repoperls/installed-perls/perl/v5.20.3/165a/bin/perl
BINDIR=`dirname $0`
set -x
set -e
while true; do
    # cd $SOLVER_VARDIR/workdir
    $PERL $BINDIR/basesmoker.pl -v
    if [ -e $BINDIR/basesmoker.stop ] ; then
        echo "Found $BINDIR/basesmoker.stop, stopping smoker"
        break
    else
	echo "basesmoker.sh is starting the next instance of basesmoker.pl"
    fi
done
