#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

$0 perl1 perl2

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Misnomer. We intended to write cronjob but settled with a one-off call
for a start. We provide a json file and a txt file.

The generation took about 1-2 hours.

The text I wrote to p5p after 5.18.1 vs. 5.18.2 RC2 was:

Specifically, there was *no* case where 5.18.1 has only passes and
5.18.2 has a fail. The following list of 118 distros presents all that
have at least one fail in RC2 and at least a pass in 5.18.1.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use POSIX ();
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
my($perl1,$perl2) = @ARGV;
$perl1 = '5.18.1' unless defined $perl1;
$perl2 = '5.18.2 RC4' unless defined $perl2;

use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;

use IPC::ConcurrencyLimit;

use DBI;

my($workdir);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
}
my($basename) = File::Basename::basename(__FILE__);
my $limit = IPC::ConcurrencyLimit->new
    (
     max_procs => 1,
     path      => "$workdir/IPC-ConcurrencyLimit-$basename",
    );
my $limitid = $limit->get_lock;
if (not $limitid) {
    warn "Another process appears to be still running. Exiting.";
    exit(0);
}

my $pgdbh = DBI->connect("dbi:Pg:dbname=analysis") or die "Could not connect to 'analysis': $DBI::err";

$pgdbh->do("create temporary table distnames as (select distinct dist, version from cpanstats where perl=? and state='fail') ", undef, $perl2) or die;
my $sth1 = $pgdbh->prepare("select dist, version from distnames");
my $sth2 = $pgdbh->prepare("select perl, state, count(*) from cpanstats where dist=? and version=? and perl in (?,?) and state in ('pass','fail') group by perl, state");

use JSON::XS;
my $jsonxs = JSON::XS->new->indent(1);
my $S = {};
my $ts = POSIX::strftime "%FT%T", gmtime($^T);

MAIN: {
    $sth1->execute;
    my $i = 0;
    my $rows = $sth1->rows;

    while (my($dist,$version) = $sth1->fetchrow_array) {
        $i++;
        $sth2->execute($dist,$version,$perl1,$perl2);
        my $s;
        my %seen = (pass => 0, fail => 0);
        while (my($perl,$state,$count) = $sth2->fetchrow_array) {
            $s->{$perl}{$state} = $count;
            $seen{$state}++;
        }
        warn "calculated $i/$rows\n";
        if ($seen{fail} == 2 and $seen{pass} == 0) {
            next;
        }
        if ($seen{fail} == 2 and $seen{pass} == 2) {
            next;
        }
        store($dist,$version,$s);
        warn "wrote $i/$rows\n";
    }
}

sub store {
    my($dist,$version,$s) = @_;
    $S->{$dist}{$version} = $s;
    my $outfile = "/home/andreas/var/compare-5181/result-$ts";
    {
        open my $fh, ">", "$outfile.new"  or die;
        print {$fh} $jsonxs->encode($S);
        close $fh or die $!;
        rename "$outfile.new", "$outfile.json" or die "Could not rename: $!";
    }
    {
        open my $fh, ">", "$outfile.new" or die;
        my $i = 0;
        for my $k (sort keys %$S){
            for my $k2 (sort keys %{$S->{$k}}){
                my $v = $S->{$k}{$k2};
                no warnings 'uninitialized';
                printf {$fh} "%3d %-56s  %3d %3d    %3d %3d\n", ++$i, "$k-$k2", $v->{$perl1}{pass}, $v->{$perl1}{fail}, $v->{$perl2}{pass}, $v->{$perl2}{fail};
            }
        }
        close $fh or die $!;
        rename "$outfile.new", "$outfile.txt" or die "Could not rename: $!";
    }
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
