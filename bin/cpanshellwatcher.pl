#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

I had to repeat this several times

 ps auxww|grep 'CPAN.*shell'
 while true; do echo -n `date`; grep VmData /proc/14305/status; sleep 12; done

Now implementing it in one program

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

use POSIX qw(strftime);

while () {
    open my $fh, "-|", ps => "axww" or die;
    while (<$fh>) {
        my($pid,undef,undef,undef,$cmd) = split " ", $_, 5;
        next unless $cmd =~ /CPAN\s.*shell/;
        open my $fh2, "<", "/proc/$pid/status" or next;
        while (<$fh2>) {
            # VmData:     7528 kB
            next unless /^VmData:(?:\s+)(\d+) kB/;
            printf "%s %5d %7d\n", strftime("%FT%T",localtime), $pid, $1;
        }
    }
    sleep 12;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
