#!/home/src/perl/repoperls/installed-perls/perl/pVNtS9N/perl-5.8.0@32642/bin/perl

# use 5.012; # dor
use strict;
use warnings;

=head1 NAME

cnntp-solver - run cpan-testers-parsereport -solve over a bunch from the cpantesters DB

=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--addctgargs=s>

Additional string to inject into the options that we pass to
ctgetreports. Typically used together with --pick when there is
something to grep for in the reports. E.g.

  --pick RDF-NS-20130402 --addctgargs " --q qr:'(Bad blank node identifier.*)'"

=item B<--calcsample=i>

Defaults to arbitrary 500 on normal runs and to 2500 if pick is set. 0
is treated as unlimited. If the number of passes and fails is so low
that the expected numbers in the sample would be too low, then the
sample is set higher, no matter what this parameter specifies.

=item B<--cpanstats_distrofacts=s>

For debugging only: string must consist of whitespace separated
arguments $dist and $version (App-MusicExpo 1.002) or the more common
$dist-$version (App-MusicExpo-1.002). Output is a JSON represenation
of what the function cpanstats_distrofacts(undef,$dist,$version)
returns.

=item B<--cpanstats_distrofacts_zero=s>

Like above but with resetting to zero beforehand.

=item B<--db_fixup_release_date=s>

For bug fixing only: string must consist of whitespace separated
arguments $dist and $version. The function db_fixup_release_date will
be called with these arguments and has the potential to fix a
I<allversions> bug. Output is a bit unstructured.

=item B<--leavewhennojobsleft!>

If this option is true the program finishes when it (1) is only
waiting for mincalctime and (2) the number of jobs in the jobsqueue
drops to zero. It is not used to make the program wait for a longer
time than mincalctime, the purpose is only to leave earlier when no
reason to wait exists anymore. The purpose of mincalctime is to keep
the shell loop around us trivial and the purpose of
leavewhennojobsleft is to get back to work asap when the jobqueue is
empty.

=item B<--maxfromannotate=i>

Per default we read all annotations from the file annotations.txt. But
the maximum number we want to read can be set lower here. Useful when
debugging the enqueing process of a certain package. Put this package
into the first line and run

 .../perl -d bin/cnntp-solver.pl --maxfromannotate=1 --maxfromsolved=0 --maxfromdb=0 --urgent

=item B<--maxfromdb=i>

Defaults to 666. Per default we read through the huge SQL query. We
switch gears in the middle from thorough reading every row to skipping
huge amounts of records randomly. But the maximum number we want to
read at all can be set lower here.

=item B<--maxfromsolved=i>

Per default we read through all already previously solved
calculations. But the maximum number we want to read can be set lower
here.

=item B<--mincalctime=i>

When nothing left to do, let the script sleep until this amount of
time has passed. Not interesting anymore. Redis queue takes over.

=item B<--pick=s>

Like a ctgetreports for this single item. We fake that it was found in
annotations and skip all other steps. Pick per default implies
sample=2500 but this can be overridden. Pick implies some kind of
force in that we do not calculate whether a recalc is necessary
depending on the age of a previous report. We simpy do it, regardless.

Maybe it's too much in the single parameter and we should separate some
aspects out?

=item B<--retrieve_distrofacts=s>

For debugging only: string must consist of whitespace separated
arguments $dist and $version. Output is a JSON represenation of what
the function retrieve_distrofacts(undef,$dist,$version) returns.

=item B<--onlystartwhennojobs!>

If this option is true the program does not start as long as there are
jobs in the queue.

=item B<--this_urgent=s>

For debugging only: string must consist of whitespace separated
arguments $dist and $version. Output is a JSON represenation of what
the function this_urgent(undef,$dist,$version) returns.

=item B<--urgent!>

Dangerous, use with care. Sets urgent globally for the whole run.
Useful during debugging to avoid having to play dirty tricks with
randomness.

=back

=head1 DESCRIPTION

Fulfills two roles on analysis: (1) enqueueing to redis is done by
running this script from crontabs with different options; (2)
dequeueing from redis is done by parallelized runs of this script with
the C<--pick> option.

=head1 FUNCTIONS

=over 8

=cut

#use lib qw(    /usr/local/perl-5.10.1-uld/lib/5.10.1/x86_64-linux-ld
#    /usr/local/perl-5.10.1-uld/lib/5.10.1
#    /usr/local/perl-5.10.1-uld/lib/site_perl/5.10.1/x86_64-linux-ld
#    /usr/local/perl-5.10.1-uld/lib/site_perl/5.10.1
#);
#use lib "/home/src/perl/CPAN-Testers-Common-Utils-0.002-82Cocq/blib/lib";
#use lib "/home/src/perl/CPAN-Testers-Common-DBUtils-0.05-zqYV7K/blib/lib";
#use lib "/home/src/perl/CPAN-Testers-Common-Article-0.42-4OIZRu/blib/lib";
#use lib "/home/src/perl/CPAN-Testers-Data-Generator-0.41-bzdKLH/blib/lib";
#use CPAN::Testers::Data::Generator;
    # methods discussed in CPAN::Testers::Common::DBUtils
    # schema in CPAN::WWW::Testers::Generator

use Digest::SHA;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);
use Pod::Usage qw(pod2usage);
our %Opt;
lock_keys %Opt, map { /([^=!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
unless (defined $Opt{calcsample}) {
    if ($Opt{pick}) {
        $Opt{calcsample} = 2500; # arbitrary
    } else {
        $Opt{calcsample} = 500; # arbitrary
    }
}
$Opt{maxfromdb} = 666 unless defined $Opt{maxfromdb};
sub mytouch ($$);
sub cpanstats_distrofacts;
sub retrieve_distrofacts;
sub this_urgent;

use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;
my($workdir,$cpan_home,$ext_src);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
    $cpan_home = $CPAN::Blame::Config::Cnntp::Config->{cpan_home};
    $ext_src = $CPAN::Blame::Config::Cnntp::Config->{ext_src};
}

use lib "$ext_src/cpan-testers-parsereport/lib"
    #, "/home/src/perl/Config-Perl-V-0.10-ymOAl_/blib/lib"
    #, "/home/src/perl/DateTime-Format-DateParse-0.04-eYwnxv/blib/lib"
    ;
use CPAN::Testers::ParseReport 0.002000; # we use it only indirectly but want
                                # to fail early if it is missing
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use Catalyst::Model;
use CPAN::Blame::Model::Solved;

use lib "$ext_src/rersyncrecent/lib";
use File::Rsync::Mirror::Recent;

use CPAN::DistnameInfo;
use DateTime;
use Time::Moment;
use lib "$ext_src/rersyncrecent/lib";
use LWP::UserAgent;
use List::Util qw(max min minstr reduce sum);
use List::MoreUtils qw(uniq);
use Storable qw(dclone);
use Term::ANSIColor;
use Time::Duration qw(duration);
use Time::HiRes qw(sleep);
use Time::Local qw(timegm);
use URI ();
use YAML::Syck;
use JSON::XS;
use Redis;

use version; our $VERSION = qv('1.1.1');

###### constants (mostly arbitrary) ######
use constant MUST_CONSIDER => 1200; # 2015-06-03: current pause makes nearly 1500 per day
                                    # 2016-03-28: s/1400/1000/: try out whether it feels faster
use constant MIN_CALCTIME => 20; # not interesting anymore, just a hot-loop-cooler
use constant MAX_CALCTIME => 60*3600; # nonsense since redis we are done in no time

###### debugging ######

if ($Opt{cpanstats_distrofacts_zero}) {
    $Opt{cpanstats_distrofacts} = $Opt{cpanstats_distrofacts_zero};
}
for my $func_name (qw(
    cpanstats_distrofacts
    retrieve_distrofacts
    db_fixup_release_date
    this_urgent
    )) {
    if (my $opt = $Opt{$func_name}) {
        my($dist,$version) = split " ", $opt;
        unless (defined $version && length $version){
            my $d = CPAN::DistnameInfo->new("FOO/$opt.tgz");
            $dist = $d->dist;
            $version = $d->version;
            unless (defined $version && length $version){
                die "argument '$opt' could not be split";
            }
        }
        no strict 'refs';
        my $func = \&{$func_name};
        my $href = $func->(undef,$dist,$version);
        my $jsonxs = JSON::XS->new->pretty(1)->canonical(1);
        print $jsonxs->encode($href);
        exit;
    }
}

###### subroutines ######

=item ok_value_and_distro



=cut
sub ok_value_and_distro {
    my($article) = @_;
    $article->{subject} =~ /(\S+)\s+(\S+)/;
}

=item mydbi



=cut
sub mydbi ($) {
    my $file = shift;
    require DBI;
    my $dbi = DBI->connect ("dbi:SQLite:dbname=$file");
}

=item mypgdbi



=cut
sub mypgdbi () {
    require DBI;
    my $dbi = DBI->connect ("dbi:Pg:dbname=analysis");
}

=item myredis



=cut
sub myredis () {
    my $redis = Redis->new(reconnect => 120, every => 1000);
}

=item my_get_query



=cut
sub my_get_query {
    my($dbi,$sql,@args) = @_;
    $dbi ||= mypgdbi();
    my $sth = $dbi->prepare($sql);
    my $rv = eval { $sth->execute(@args); };
    unless ($rv) {
        my $err = $sth->errstr;
        warn "Warning: error occurred while executing '$sql': $err";
    }
    my @rows;
    while (my(@trow) = $sth->fetchrow_array()) {
        push @rows, \@trow;
    }
    \@rows;
}

=item my_do_query



=cut
sub my_do_query {
    my($dbi,$sql,@args) = @_;
    local($dbi->{PrintError}) = 0;
    my $success = $dbi->do($sql,undef,@args);
    unless ($success) {
        my $err = $dbi->errstr;
        unless ($err =~ /duplicate key/) {
            require Carp;
            Carp::cluck(sprintf
                        (
                         "Warning: error occurred while executing sql[%s]with args[%s]: %s",
                         $sql,
                         join(":",map { defined $_ ? "'$_'" : "<undef>"} @args),
                         $err,
                        ));
        }
    }
    return $success;
}

=item cpan_lookup_dist



=cut
{
    package CPAN::Shell::devnull;
    sub myprint { return; }
    sub mywarn { return; }
    sub mydie { my($self,$why) = @_; warn "Caught error[$why]; continuing"; return; }
    sub mysleep { return; }
}
sub cpan_lookup_dist {
    my($mdbi,$dist,$version) = @_;
    return unless defined $dist && defined $version;
    my $distv = "$dist-$version";
    $mdbi ||= mypgdbi();
    my($author,$upload_date,$distroid) = db_memoized($mdbi,$distv);
    return ($author,$upload_date,$distroid)
        if $author && $upload_date && $distroid and $upload_date =~ /^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] /;

    # $DB::single = $dist eq "Net-Twitter" && $version eq "3.07002";
    # $DB::single = $dist eq "App-CLI-Plugin-Log-Message";

    use lib "$ext_src/cpanpm/lib";
    our $CPAN_SETUP_DONE;
    unless ($CPAN_SETUP_DONE++) {
        require CPAN;
        CPAN::HandleConfig->load;
        CPAN::Shell::setup_output();
        CPAN::Index->reload;
    }
    $CPAN::Frontend = "CPAN::Shell"; # alt: devnull
    my @ret = CPAN::Shell->expand("Distribution", "/\\/$distv/");
    my $best = reduce {
        my $la = length(mybasename($a->{ID}));
        my $lb = length(mybasename($b->{ID}));
        $la < $lb ? $a : $b
    } @ret;

    my($upload_date_authoritative) = 0;
    if ($best) {
        # $DB::single=1;
        my $oldyaml = read_ctx($distv);
        if ($oldyaml->{upload_date}
            and $oldyaml->{upload_date} =~ /^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] /
            and $oldyaml->{upload_date} !~ /\?/) {
            $upload_date = $oldyaml->{upload_date};
        } elsif ($best->{UPLOAD_DATE}) {
            # not expected to happen
            $upload_date = $best->{UPLOAD_DATE}
        } elsif (my @stat = stat "$cpan_home/authors/id/$best->{ID}") {
            # $DB::single++;
            my(@mtime) = gmtime $stat[9];
            $mtime[4]++;
            $mtime[5]+=1900;
            $upload_date = sprintf "%04d-%02d-%02dT%02d:%02d:%02dz", @mtime[reverse 0..5];
            $upload_date_authoritative = 1;
        } else {
            # S-L-O-W
            my($id) = $best->{ID};
            $id =~ s|^./../||;
            local($CPAN::Frontend) = "CPAN::Shell::devnull";
            CPAN::Shell->ls($id); # find out the upload date
            $upload_date = $best->upload_date || "???";
        }
        $distroid = $author = substr($best->{ID},5);
        $author =~ s|/.*||;
    } elsif (my $x = find_on_cpan($dist,$version)) {
        my $absdistro = "$cpan_home/authors/$x";
        ($author) = $absdistro =~ m{.././../(.+?)/};
        ($distroid) = $absdistro =~ m{.././../(.+)$};
        if (my(@stat) = stat $absdistro) {
            my(@mtime) = gmtime $stat[9];
            $mtime[4]++;
            $mtime[5]+=1900;
            $upload_date = sprintf "%04d-%02d-%02d", @mtime[5,4,3];
        } else {
            # warn "Alert: Could not stat '$absdistro', will try to continue: $!";
            $upload_date = "....-..-..";
        }
    } else {
        # next ARTICLE;
        $author = "";
        my(@now) = gmtime;
        $now[4]++;
        $now[5]+=1900;
        $upload_date = sprintf "%04d-%02d-%02d(?)", @now[5,4,3];
        # leaving $distroid = undef;
    }
    db_memoize($mdbi,$distv,$author,$upload_date,$distroid,$upload_date_authoritative);
    return ($author,$upload_date,$distroid);
}

=item db_memoized



=cut
sub db_memoized {
    my($dbi,$distv) = @_;
    my $sql = "SELECT author,upload_date,distroid FROM distlookup WHERE distv=?";
    my $rows = my_get_query
        (
         $dbi,
         $sql,
         $distv
        );
    return unless @$rows;
    if (@$rows==1) {
        return @{$rows->[0]};
    } else {
        my $n = @$rows;
        die "Panic: cannot happen: '$n' rows when querying primary key '$distv'";
    }
}

=item db_memoize($dbi,$distv)

=item db_memoize($dbi,$distv,$author,$upload_date,$distroid,$upload_date_authoritative)

First form reserves a record, second form fills it a bit. Undefined
values are ignored (*not* written as NULL).

=cut
sub db_memoize {
    my($dbi,$distv,$author,$upload_date,$distroid,$upload_date_authoritative) = @_;
    my $sql = "INSERT INTO distlookup (distv) VALUES (?)";
    my $success = eval { my_do_query
        (
         $dbi,
         $sql,
         $distv,
        )};
    my(%othersets) =
        (
         author                 => $author,
         upload_date            => $upload_date,
         distroid               => $distroid,
         $upload_date_authoritative ? (release_date_metacpan  => $upload_date) : (),
        );
    my(@set, @bind);
    while (my($k,$v) = each %othersets) {
        if (defined $v) {
            push @set, "$k=?";
            push @bind, $v;
        }
    }
    return unless @bind; # they had no data
    push @bind, $distv;
    $sql =
        sprintf "UPDATE distlookup SET %s WHERE distv=?",
            join(", ", @set);
    eval { my_do_query
               (
                $dbi,
                $sql,
                @bind,
               )};
    if ($@){
        warn "Error while processing '$distv' with sql[$sql]: $@";
    }
}

=item mybasename



=cut
sub mybasename {
    my($p) = @_;
    $p =~ s|.*/||;
    return $p;
}

READ_ANNOTATIONS: {
    my %anno_line;
    sub read_annotations {
        use File::chdir;
        local($CWD) = "$FindBin::Bin/..";
        unless ($Opt{pick}) {
            for (0,1) {
                last if 0 == system git => "pull"; # may fail
                sleep 1;
            }
        }
        my $annofile = "$FindBin::Bin/../annotate.txt";
        my $fh;
        unless (open $fh, $annofile) {
            # $DB::single=1;
            die "Could not";
        }

        my $anno = {};
        local $/ = "\n";
        my $i = 0;
    ANNOLINE: while (<$fh>) {
            chomp;
            next ANNOLINE if /^\s*$/;
            $i++;
            if (defined $Opt{maxfromannotate}) {
                last ANNOLINE if $i > $Opt{maxfromannotate};
            }
            my($dist,$splain) = split " ", $_, 2;
            $anno->{$dist} = $splain;
            $anno_line{$dist} = $i;
        }
        return $anno;
    }

}

=item find_on_cpan



=cut
sub find_on_cpan {
    my($dist,$version) = @_;
    return unless defined $dist;
    my $rf = File::Rsync::Mirror::Recent->new(local => "$cpan_home/authors/RECENT.recent");
    my $recent = $rf->news();
    my(@cand) = map
        {
            $recent->[$_]
        } grep
            {
                $recent->[$_]{path} =~ m{/\Q$dist\E-\Q$version\E}
                    &&  $recent->[$_]{path} !~ m{(meta|readme)$}
                        # it doesn't matter if it is deleted, we found it!
                        # && $recent->[$_]{type} eq "new"
                    } 0..$#$recent;
    my @path = uniq map { $_->{path} } @cand;
    if (@path == 0) {
        return;
    } elsif (@path != 1){
        # e.g.
        # 0  'id/D/DR/DROLSKY/Moose-0.89_02.tar.gz'
        # 1  'id/D/DR/DROLSKY/Moose-0.89_01.tar.gz'
        # 2  'id/D/DR/DROLSKY/Moose-0.89.tar.gz'
        my $best = reduce {
            my $la = length($a);
            my $lb = length($b);
            $la < $lb ? $a : $b
        } @path;
        # $DB::single++;
        return $best;
    } else {
        return $path[0];
    }
}

=item $href = cpanstats_distrofacts($dbi, $dist, $version, $distv, $lfh)

The original design of this function was and still is to query the
C<cpanstats> table for all reports on B<dist> and not B<distv>. It
then constructs the order of versions how they entered the database in
C<allversions> and count pass and fail and other for this version and
determine the third fail day.

A sample $href returned (slightly faked):

  {
     "allversions" : [
        {
           "date" : "201302071447",
           "version" : "0.01"
        },
        {
           "date" : "201302111128",
           "version" : "0.02"
        },
        {
           "date" : "201303270850",
           "version" : "0.03"
        },
        {
           "date" : "201401241127",
           "version" : "0.04"
        }
     ],
     "db_fails" : 3,
     "db_others" : 2,
     "db_passes" : 1748,
     "thirdfail" : "2013-12-19 14:54",
     "thirdfailid" : 37664541,
     "thirdpass" : "2011-04-17 14:04",
     "thirdpassid" : 11739866
  }

Since this function can also be called from the option
--cpanstats_distrofacts, the question is how well it works in
concurrency. I *think* the algorithm is OK but I did not try to test
for border cases. To avoid corruption we introduced the $lfh
parameter.

If the $lfh is false, we try to get a lockfilehandle and hold it to
the end. If it is a valid lockfilehandle, all the better.

=cut
sub cpanstats_distrofacts {
    my($dbi,$dist,$version,$distv,$lfh) = @_;
    die "ALERT: illegal distname [$dist]" if $dist =~ /'/;
    $distv ||= "$dist-$version";
    unless ($lfh) {
        my $slv_file = slv_file($distv);
        $lfh = lockfilehandle("$slv_file.LCK") or return;
    }
    $dbi ||= db_quickaccess_handle();
    my($oldfacts) = retrieve_distrofacts($dbi,$dist,$version,$distv);
    # CUT: we do not yet use the oldfacts
    my($fails,       $passes,      $others,
       $thirdfailid, $thirdpassid, $alreadycounted,
       $thirdfail,   $thirdpass,   $cuti_reset)
        = @{$oldfacts}{qw(fails       passes      others
                          thirdfailid thirdpassid alreadycounted
                          thirdfail   thirdpass   cuti_reset
                        )};
    my $allversions = dclone $oldfacts->{allversions};
    my($maxid) = do {
        my $sql0 = "SELECT MAX(id) FROM cpanstats";
        my $rows0 = my_get_query($dbi, $sql0);
        $rows0->[0][0];
    };
    my $maxid_ts = Time::Moment->now_utc->to_string; # 2014-02-09T09:59:11.664143Z
    $alreadycounted ||= 0;
    my $sql = "SELECT id,version,fulldate,state
 FROM cpanstats
 WHERE dist=?
 AND id > ?
 AND id <= ?
 AND type=2 ORDER BY id";
    my $rows = my_get_query($dbi, $sql, $dist, $alreadycounted, $maxid);
    my(%seen,%xseen,%is_authoritative);
    for my $av (@$allversions) {
        $seen{$av->{version}}++; # prevent duplicates
        $xseen{$av->{version}}{$av->{date}}++; # preserve earliest date from previous runs
        if ($av->{authoritative}) {
            $is_authoritative{$av->{version}} = 1;
        }
    }
    $fails ||= 0;
    $passes ||= 0;
    $others ||= 0;
    for my $row (@$rows) {
        my($id,$v,$date,$state) = @$row; # id,version,fulldate,state
        if (defined $v){
            unless ($seen{$v}++) {
                push @$allversions, { version => $v };
            }
            if (defined $version and $v eq $version) {
                if ($state eq "fail") {
                    $fails++;
                    if (!$thirdfailid && $fails == 3) {
                        $thirdfail = $date; # format 200909190440
                        $thirdfailid = $id;
                    }
                } elsif ($state eq "pass") {
                    $passes++;
                    if (!$thirdpassid && $passes == 3) {
                        $thirdpass = $date; # format 200909190440
                        $thirdpassid = $id;
                    }
                } else {
                    $others++
                }
            }
            $date = "" unless defined $date;
            $is_authoritative{$v} || $xseen{$v}{$date}++; # format 200909190440
        }
    }
    if ($thirdfail) {
        $thirdfail =~ s/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/$1-$2-$3 $4:$5z/;
    }
    if ($thirdpass) {
        $thirdpass =~ s/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/$1-$2-$3 $4:$5z/;
    }
    for my $v (@$allversions) {
        $v->{date} = minstr grep { length $_ } keys %{$xseen{$v->{version}}};
    }
    # sleep 0.25; # throttle
    my $facts =
        {
         allversions  => $allversions,
         thirdfail    => $thirdfail,
         thirdpass    => $thirdpass,
         thirdfailid  => $thirdfailid,
         thirdpassid  => $thirdpassid,
         db_fails     => $fails,
         db_passes    => $passes,
         db_others    => $others,
         db_maxid     => $maxid,
         db_maxid_ts  => $maxid_ts,
         cuti_reset   => $cuti_reset,
        };
    store_distrofacts($dbi,$dist,$distv,$oldfacts,$facts);
    return $facts;
}

=item store_distrofacts

Cache the pass/fail/other counts, thirdfailid and thirdpassid, and the
allversions structure. Remember the highest id up to which we have
counted/observed so far, so we can start counting there next time.

=cut
sub store_distrofacts {
    my($dbi,$dist,$distv,$oldfacts,$facts) = @_;
    no warnings 'uninitialized';
    {
        # do we need an insert first? See "insert missing" below
        db_memoize($dbi,$distv);

        # then an update
        my $sql = "UPDATE distlookup
SET cuti_fail=?,   cuti_pass=?,   cuti_others=?,
    thirdfailid=?, thirdpassid=?, counted_up_to_id=?,
    cuti_ts=?,     cuti_reset=?
WHERE distv=?";
        my(%otherwhere);
        my @bind =
            (
             @{$facts}{qw(db_fails    db_passes   db_others
                          thirdfailid thirdpassid db_maxid
                          db_maxid_ts cuti_reset)},
             $distv,
            );
        @otherwhere{qw(fails       passes      others
                       thirdfailid thirdpassid alreadycounted
                       cuti_reset)}
            = qw(cuti_fail   cuti_pass   cuti_others
                 thirdfailid thirdpassid counted_up_to_id
                 cuti_reset);
        while (my($k,$v) = each %otherwhere) {
            if (defined $oldfacts->{$k}) {
                $sql .= " AND $v=?";
                push @bind, $oldfacts->{$k};
            } else {
                $sql .= " AND $v IS NULL";
            }
        }
        my $sth = $dbi->prepare($sql);
        my $rv = $sth->execute(@bind);
        unless ($rv > 0) {
            my $bind = join ",", map { defined $_ ? "'$_'" : "<undef>" } @bind;
            warn "WARNING: $rv records updated with sql[$sql]bind[$bind]".
                " => insert missing!";
        }
    }
    my $oldav = { map {("$dist-$_->{version}" => $_->{date})}
                  @{$oldfacts->{allversions}||[]}
                };
    my $av = $facts->{allversions};
 DREF: for my $i (0..$#$av) {
        my $dref = $av->[$i] or next; # "distro-reference" { version=>"0.12", date=>"20070408" }
        my $tdistv = "$dist-$dref->{version}";
        my $tdate = $dref->{date};
        if ($i == $#$av and $tdate =~ /[0-9]{8}/) {
            eval { db_fixup_release_date($dbi,$dist,$dref->{version},$tdistv); };
            if ($@) {
                warn "Warning: fixup complained [$@]";
            }
        }
        next DREF if $tdate && $oldav->{$tdistv} && $tdate eq $oldav->{$tdistv};
        db_quickaccess_insert_or_update($dbi,$tdistv,undef,undef,$dist);
        db_memoize($dbi,$tdistv,undef,$tdate,undef,0);
    }
    my $redis = myredis;
    my($fail,$pass) = @{$facts}{qw(db_fails db_passes)};
    #### hset missing in our ubuntu redis, using zscore as a workaround
    $redis->zadd("analysis:distv:pass",$pass,$distv) if defined $pass;
    $redis->zadd("analysis:distv:fail",$fail,$distv) if defined $fail;

    return;
}

=item retrieve_distrofacts

The distlookup and distcontext tables hold aggregate data that are
generated from cpanstats (plus some). We update them on every run in
store_distrofacts and reuse them next time we come by so that we do
not have to read so much from cpanstats.

When a record has a zero for counted_up_to_id and one of the
pass/fail/etc values are set, the whole record is reset. This means, a
convenient way to restart counting is by resetting counted_up_to_id.
For convenience, this reset can be triggered with the
--cpanstats_distrofacts_zero option.

=cut
sub retrieve_distrofacts {
    my($dbi,$dist,$version,$distv) = @_;
    $dbi ||= db_quickaccess_handle();
    $distv ||= "$dist-$version";
    my $facts = {};
    my $sth = $dbi->prepare("SELECT cuti_fail,   cuti_pass,   cuti_others,
                                    thirdfailid, thirdpassid, counted_up_to_id,
                                    cuti_reset
 FROM distlookup WHERE distv=?");
    $sth->execute($distv);
    my(@row) = $sth->fetchrow_array;
    @{$facts}{qw(fails passes others thirdfailid thirdpassid alreadycounted cuti_reset)} = @row;
    my @av;
    my $reset = $Opt{cpanstats_distrofacts_zero} || !$facts->{alreadycounted};
    unless ($reset) {
        my $cuti_reset ||= 0;
        if (time - $cuti_reset > 86400*90) {
            if (rand(1) < 0.001) {
                $reset = 1;
            }
        }
    }
    if ($reset) {
        my $nil = {};
        @{$nil}{qw(db_fails db_passes db_others thirdfailid thirdpassid alreadycounted)} = ();
        $nil->{cuti_reset} = time;
        store_distrofacts($dbi,$dist,$distv,$facts,$nil);
        $facts = $nil;
    } else {
        $sth = $dbi->prepare("SELECT fulldate FROM cpanstats WHERE id=?");
        $sth->execute($facts->{thirdfailid});
        (@row) = $sth->fetchrow_array;
        $facts->{thirdfail} = $row[0];
        $sth->execute($facts->{thirdpassid});
        (@row) = $sth->fetchrow_array;
        $facts->{thirdpass} = $row[0];
        $sth = $dbi->prepare("SELECT distv FROM distcontext WHERE dist=?");
        my($sth2) = $dbi->prepare("SELECT upload_date, release_date_metacpan
 FROM distlookup
 WHERE distv=?");
        $sth->execute($dist);
        while (@row = $sth->fetchrow_array) {
            my($tdistv) = @row;
            my($v) = substr($tdistv,length($dist)+1);
            next unless $v;
            $sth2->execute($tdistv);
            my($tud,$trdmc) = $sth2->fetchrow_array;
            my($tdate) = $trdmc || $tud;
            next unless $tdate;
            $tdate =~ s/[^0-9]//g;
            next unless $tdate;
            substr($tdate,12)="" if length($tdate)>12;
            my $av = { version => $v, date => $tdate };
            $av->{authoritative} = 1 if $trdmc;
            push @av, $av;
        }
        @av = sort { $a->{date} cmp $b->{date}
                         || $a->{version} <=> $b->{version}
                             || $a->{version} cmp $b->{version}
                         } @av;
    }
    $facts->{allversions} = \@av;
    return $facts;
}

=item $href = distroquestion($dbi,$i,$distroquestlabel,$dist,$version,$lfh)

(Called from three different places, and reacting to their context)

If the $lfh is false, we try to get a lockfilehandle and hold it to
the end. If it is a valid lockfilehandle, all the better.

=cut
sub distroquestion {
    my($dbi,$i,$distroquestlabel,$dist,$version,$lfh) = @_;
    my $distv = "$dist-$version";
    unless ($lfh) {
        my $slv_file = slv_file($distv);
        $lfh = lockfilehandle("$slv_file.LCK") or return;
    }
    my $greenish = 0;
    my($distro_age);
    my($author,$upload_date,$distroid) = cpan_lookup_dist($dbi,$dist,$version);
    my($tell_upload_date);
    my($csdfacts) = cpanstats_distrofacts($dbi,$dist,$version,$distv,$lfh);
    my($allversions) = $csdfacts->{allversions};
    my($thirdfail) = $csdfacts->{thirdfail};
    my $passfail_overview =
        {
         pass  => $csdfacts->{db_passes},
         fail  => $csdfacts->{db_fails},
         other => $csdfacts->{db_others},
        };
    my $tell_passfail_overview;
    my $ret =
        {
         allversions => $allversions,
         author => $author,
         dist => $dist,
         distroid => $distroid,
         distv => $distv,
         passfail_overview => $passfail_overview,
         thirdfail => $thirdfail,
         upload_date => $upload_date,
         version => $version,
        };
    {
        my $color_on = "";
        my $color_off = "";
        if (0 == $passfail_overview->{pass}) {
            $color_on = Term::ANSIColor::color("red");
            $color_off = Term::ANSIColor::color("reset");
        } elsif ($passfail_overview->{pass} >= 3 && $passfail_overview->{fail}>=3) {
            $color_on = Term::ANSIColor::color("green");
            $color_off = Term::ANSIColor::color("reset");
            $greenish++;
        }
        $tell_passfail_overview = sprintf
            ("%s%3d:%3d:%3d%s",
             $color_on,
             $passfail_overview->{pass},
             $passfail_overview->{fail},
             $passfail_overview->{other},
             $color_off,
            );
    }
    {
        my($y,$m,$d,$hour,$minute,$second) = $upload_date =~ /(\d+)-(\d+)-(\d+)(?:T(\d+):(\d+):(\d+))?/;
        if (not defined $y) {
            # ....-..-..
            printf "Warning: no upload date for %s\n", $ret->{distv};
            return $ret;
        }
        my $dtu = DateTime->new
            (year => $y,
             month => $m,
             day => $d,
             hour => $hour||0,
             minute => $minute||0,
             second => $second||0,
             time_zone => "floating", ### should change to utc, but may soon be unreachable
            );
        my $dtn = DateTime->now;
        my $dur = $dtn->subtract_datetime_absolute($dtu);
        $distro_age = $dur->delta_seconds;
        my $color_on = "";
        my $maxyears = $CPAN::Blame::Config::Cnntp::Config->{maxyears} || 8;
        if ($distro_age >= $maxyears*365.25*86400) {
            $color_on = Term::ANSIColor::color("yellow");
        } elsif ( $upload_date =~ /\?/) {
            $color_on = Term::ANSIColor::color("yellow");
        } elsif ($distro_age > 0) {
            $color_on = Term::ANSIColor::color("green");
            $greenish++;
        } else {
            $color_on = Term::ANSIColor::color("red");
        }
        my $color_off = Term::ANSIColor::color("reset");
        $tell_upload_date = $color_on . substr($upload_date,2,8) . $color_off;
    }
    my $tell_current_version;
    {
        my $current_version = @$allversions ? $allversions->[-1]{version} : 0;
        my $color_on = "";
        if ($current_version eq $version) {
            $tell_current_version = "current";
            $color_on = Term::ANSIColor::color("green");
            $greenish++;
        } elsif ($Opt{pick}) {
            $tell_current_version = "<?>";
            $color_on = Term::ANSIColor::color("green");
        } else {
            $tell_current_version = "<$current_version";
            $color_on = Term::ANSIColor::color("red");
        }
        my $color_off = Term::ANSIColor::color("reset");
        $tell_current_version = sprintf "%s%s%s", $color_on,$tell_current_version,$color_off;
    }
    my $ddist = length($distv)<=52 ? $distv : (substr($distv,0,49)."...");
    unless ($i % 50) {
        print "gmtime now: ".gmtime()."\n";
    }
    printf(
           "%3d %-8s %s %s %s/%s (%s)\n",
           $i, $distroquestlabel,
           $tell_upload_date,
           $tell_passfail_overview,
           $author || "???",
           $ddist,
           $tell_current_version,
          );
    my $slv_content = slv_content($distv);
    my $rtables = CPAN::Blame::Model::Solved->regression_tables($slv_content);
    my $rsq1_regressions = join ", ", map { $_->{name} } grep { $_->{rsq}==1 } @$rtables;
    $ret->{greenish} = $greenish;
    $ret->{distro_age} = $distro_age;
    $ret->{rsq1_regressions} = $rsq1_regressions;
    # sleep 0.5; # throttle
    return $ret;
}

=item slv_file



=cut
sub slv_file {
    my($distv) = @_;
    sprintf "%s/solved/%s.slv", $workdir, $distv;
}

=item slv_content



=cut
sub slv_content {
    my($distv) = @_;
    my $slv_file = slv_file($distv);
    return unless -e $slv_file;
    my $content = do { open my $fh, $slv_file or die "Could not open '$slv_file': $!";
                   local $/; <$fh> };
}

=item lockfilehandle

=cut
sub lockfilehandle {
    my($lockfile) = @_;
    use Fcntl qw( :flock );
    mkpath dirname $lockfile;
    my $lfh;
    unless (open $lfh, "+<", $lockfile) {
        unless ( open $lfh, ">>", $lockfile ) {
            die "ALERT: Could not open >> '$lockfile': $!"; # XXX should not die
        }
        unless ( open $lfh, "+<", $lockfile ) {
            die "ALERT: Could not open +< '$lockfile': $!"; # XXX should not die
        }
    }
    if (flock $lfh, LOCK_EX|LOCK_NB) {
        print "Info[$$]: Got the lock, continuing";
    } else {
        print "FATAL[$$]: lockfile '$lockfile' locked by a different process";
        return;
    }
    return $lfh;
}


=item call_ctgetreports



=cut
sub call_ctgetreports {
    my($ctx,$ctxi,$ctxtotal) = @_;
    my($distv,$distro_age) = ($ctx->{distv},$ctx->{distro_age});
    my $slv_file = slv_file($distv);
    my @stat = stat ($slv_file . "dv.gz");
    unless (@stat) {
        @stat = stat ($slv_file . "dv");
    }
    my $slv_file_age = @stat ? time-$stat[9] : 0;
    # tuning this knob is always difficult for me. We are saying here
    # that we compare the age of the distro and the age of the
    # regression calculation we have around. Iow, older distros need
    # to be recalculated less often. If we increase this parameter
    # towards 1, we never calculate again because any result will be
    # younger than the distro. And if we go toward 0 we calculate
    # every time we come around. So if at 0.1 and with a very slow
    # pause2 we feel that 0.1 calculates too often, then we inc to
    # 0.25 to reduce the number of recalcs.
    my $must_run_ctgetreports;
    if ($Opt{pick}) {
        $must_run_ctgetreports = 1;
    } else {
        # tentative name for this variable? it is a relative age, a
        # threshold, and it guards recalculations:
        # RECALC_AGE_THRESHOLD? Sanity next time we come around
        $must_run_ctgetreports = !$slv_file_age || $slv_file_age > $distro_age*0.40; # arbitrary
    }
    # not allowed to go beyond this point without having a lock
    my $lfh = lockfilehandle("$slv_file.LCK") or return;
    if (!$must_run_ctgetreports && -e $slv_file && $slv_file_age > 86400) {
        # we want to overrule the decision for dists that have changed
        # much since last computation
        my $slv_content = slv_content($distv);
        my($calctimepass,$calctimefail) = (0,0);
        my @lines = split /\n/, $slv_content;
        for (@lines) {
            next unless /^ ([A-Z]+)/;
            if ($1 eq "PASS") {
                $calctimepass++;
            } elsif ($1 eq "FAIL") {
                $calctimefail++;
            }
        }
        if ($ctx->{passfail_overview}) {
            if (   $calctimepass && $ctx->{passfail_overview}{pass}/$calctimepass > 1.5  # arbitrary
                || $calctimefail && $ctx->{passfail_overview}{fail}/$calctimefail > 1.5  # arbitrary
               ) {
                $must_run_ctgetreports=1;
            }
        }
    }
    # its ugly to log within a subroutine and to log things that
    # belong to the outer scope and to pass parameters in we only need
    # for logging's sake:(
    printf
        ("DEBUG: at [%s UTC] for distv[%s] slv_file_age[%s] upload_date[%s] distro_age[%s] must_run_ctg[%s] progress[%d/%d]\n",
         scalar gmtime,
         $distv,
         $slv_file_age,
         $ctx->{upload_date},
         $distro_age,
         $must_run_ctgetreports,
         $ctxi,
         $ctxtotal,
        );
    if ($must_run_ctgetreports) {
        mkpath dirname $slv_file;
        my $dumpfile = $slv_file;
        $dumpfile =~ s/\.slv$/.slvdv/;
        if (-e "$dumpfile.gz") {
            0 == system "gunzip -f $dumpfile.gz" or die;
        }
        my $ctg = sprintf "%s/ctgetreports", dirname($^X);
        $ctg = "$ext_src/cpan-testers-parsereport/bin/ctgetreports";
        die "ctgetreports not installed" unless -e $ctg;
        my $ctg_cachedir = $CPAN::Blame::Config::Cnntp::Config->{ctgetreports_dir};
        my $hashed_ctg_parent;
        if (-d $ctg_cachedir) {
            my $letter = substr($distv,0,1);
            $hashed_ctg_parent = "$ctg_cachedir/$letter";
            if (-f "$hashed_ctg_parent/$distv.tar") {
                0 == system qq{tar -C "$hashed_ctg_parent" -xf "$hashed_ctg_parent/$distv.tar"}
                    or die "Problem while untarring $distv.tar";
            }
        } else {
            die "directory '$ctg_cachedir' doesn't exist. Care to create it?";
        }
        my $sample = $Opt{calcsample};
        if ($sample && $ctx->{passfail_overview}{fail} && $ctx->{passfail_overview}{pass}) {
            # correct the sample size if there is a small amount of either fails or passes
            my $NF = $ctx->{passfail_overview}{fail};
            my $NP = $ctx->{passfail_overview}{pass};
            my $CRIT = min($NP,$NF);
            my $N = $NF + $NP;
            if ($CRIT * $sample / $N < 3) {
                $sample = 1 + int(3.15 * $N / $CRIT); # 3.15 security margin over 3
            }
            if ($sample > $N){
                # emergency break 2014-12-24:
                # Class-C3-Componentised-1.001000 takes longer than 48
                # hours, we must avert those; if the result is really
                # interesting, we can expect that we'll get more fails
                # in the following days
                if ($sample > 3200) { # arbitrary: CGI-Struct with 3300 took 53000 seconds
                    warn "Alert: Late refusing of ctgetreports for $distv; sample too big: $sample";
                    return;
                } else {
                    $sample = 0;
                }
            }
        }
        my $sample_argument = $sample ? " --sample=$sample" : "";
        my $addctgargs = $Opt{addctgargs} || "";
        my $system =
            (
             qq{"$^X" -I "$ext_src/cpan-testers-parsereport/lib" "$ctg"}
             .qq{ --pce --cachedir="$hashed_ctg_parent/$distv"}
             .qq{ --q meta:from}
             .qq{ --q qr:'(.*version .+? required--this is only.*)'}
             .qq{ --q qr:'("[A-Za-z0-9_]+" is not exported by the [A-Za-z0-9_:]+ module.*)'}
             .qq{ --q qr:'(.*instance running.*)'}
             # cf Xref: k85.linux.bogus sent:7814
             #.qq{ --q qr:'(.*could.?n.t (?:open|connect).*)'}
             .qq{ --q qr:'(.*(?i:could.?n.t (?:open|connect|find)).*)'}
             .qq{ --q qr:'(.*Base class package.*)'}
             .qq{ --q qr:'(Can.t call method .*)'}
             .qq{ --q qr:'(Can.t use an undefined value.*)'}
             .qq{ --q qr:'(Can.t use string .*)'}
             .qq{ --q qr:'(Can.t modify division)'}
             .qq{ --q qr:'(Can\\047t locate loadable object)'}
             .qq{ --q qr:'(Connection (?:refused|reset by peer))'}
             .qq{ --q qr:'Constants from lexical variables potentially modified elsewhere are deprecated'}
             .qq{ --q qr:'(Can.t use keyword .*? as a label)'}
             .qq{ --q qr:'(Could not open .*)'}
             .qq{ --q qr:'(Address already in use)'}
             .qq{ --q qr:'(.*is deprecated (?:and will be removed)?)'}
             .qq{ --q qr:'(skip(?:ping |ped: | test).*)'}
             .qq{ --q qr:'(Please rerun the make command)'}
             .qq{ --q qr:'(v-string .* non-portable at \\S+)'}
             .qq{ --q qr:'(The module .* isn.t available on CPAN)'}
             .qq{ --q qr:'(Invalid version format)'}
             .qq{ --q qr:'did you mean \\\$\\] \\?'}
             .qq{ --q qr:'Unescaped left brace in regex is deprecated here, passed through'}
             .qq{ --q qr:'Unescaped left brace in regex is deprecated'}
             .qq{ --q qr:'Unescaped left brace in regex is illegal'}
             .qq{ --q qr:'Unknown warnings category'}
             .qq{ --q qr:'Non-ASCII character seen before =encoding'}
             .qq{ --q qr:'((Smartmatch|given|when) is experimental)'}
             .qq{ --q qr:'# (.+ alternative text .+ contains non-escaped .+)'}
             .qq{ --q qr:'=over without closing =back'}
             .qq{ --q qr:'starts or ends with whitespace'}
             .qq{ --q qr:'Acme::Override::INET replaced IO::Socket::INET'}
             .qq{ --q qr:'Invalid byte sequence'}
             .qq{ --q qr:'Insecure dependency in (require|eval)'}
             .qq{ --q qr:'Passing a list of values to enum'}
             .qq{ --q qr:'(undefined symbol: [A-Za-z0-9_]+ )'}
             .qq{ --q qr:'Bailout called\\.'}
             .qq{ --q qr:'Out of memory\\!'}
             .qq{ --q qr:'Prototype mismatch:'}
             .qq{ --q qr:'Connection reset by peer'}
             .qq{ --q qr:'depends on targets in skipped section'}
             .qq{ --q qr:'Network error'}
             .qq{ --q qr:'Insecure .+ while running with'}
             .qq{ --q qr:'must be adjacent in regex'}
             .qq{ --q qr:'locate object method .new. via package .Test2::Context.'}
             .qq{ --q qr:'Use of code point 0x[[:xdigit:]]+ is deprecated; the permissible max is 0x7FFFFFFF'}
             .qq{ --q qr:'# (.+ but done_testing.+ expects .+)'}
             .qq{ --q qr:'Non-zero wait status: ([0-9]+)'}
             .qq{ --q qr:'# OpenSSL version:\\s+\\047(.+?)\\047'}
             .qq{ --q qr:'attributes must come before the signature'}
             .qq{ --q qr:'illegal file descriptor'}
             .qq{ --q qr:'(got handshake key 0x[[:xdigit:]]+, needed 0x[[:xdigit:]]+)'}
             .qq{ --q qr:'(Cannot detect source of \\047.+?\\047!)'}
             .$addctgargs
             .qq{ --solve --solvetop=123$sample_argument}
             .qq{ --dumpfile=$dumpfile}
             .qq{ --minpass=3}
             .qq{ --transport=http_cpantesters_gzip}
             .qq{ --prefer-local-reports}
             .qq{ --vdistro=$distv $ctx->{dist} > $slv_file.new 2>&1}
             .q{ --filtercb='my $r = shift; $r->{"meta:ok"} = "FILTERED" if $r->{"qr:locate object method .new. via package .Test2::Context."}'}
             .q{ --filtercb='my $r = shift; $r->{"meta:ok"} = "FILTERED" if $r->{"qr:Use of code point 0x[[:xdigit:]]+ is deprecated; the permissible max is 0x7FFFFFFF"}'}
             .q{ --filtercb='my $r = shift; for (split " ", $r->{"conf:ccflags"}){$r->{"conf:ccflags~$_"} = 1}'}
            );
        warn "Info: running system[$system]";
        my $ret = process_ctg($system, $distv);
        # gzip: /home/ftp/cnntp-solver-2009/workdir/solved/Dist-Zilla-Plugin-Twitter-0.015-TRIAL.slvdv: No such file or directory
        my $redis = myredis;
        $redis->zrem("analysis:distv:eta",$distv);
        if ($ret==0) {
            unless (0 == system "gzip -9f $dumpfile") {
                warn "Warning: error while running gzip on '$dumpfile', continuing...";
                return;
            }
            unless (0 == system
                    qq{tar -C "$hashed_ctg_parent" --remo -cf "$hashed_ctg_parent/$distv.tar" "$distv"}) {
                warn "Warning: error while running tar cf on '$distv', continuing...";
                return;
            }
            my @stat = stat "$dumpfile.gz";
            $redis->zadd("analysis:distv:calctimestamp",$stat[9],$distv) if @stat;
            my $lc_ts = Time::Moment->from_epoch($stat[9])->to_string; # lc=last calc
            open my $zfh, "-|", "zcat", "$dumpfile.gz" or die "could not fork: $!";
            my $j = do {local $/; <$zfh>};
            use JSON::XS;
            my $h = JSON::XS->new->decode($j);
            my %passfail = map { $_ => $h->{"meta:ok"}{$_}{$_} } qw(PASS FAIL);
            for my $pf (qw(pass fail)) {
                my $redis_key = "analysis:distv:lc$pf"; # analysis:distv:lcpass
                my $pf_value = $passfail{uc $pf} // 0;  # $passfail{PASS}
                $redis->zadd($redis_key, $pf_value, $distv);
            }
            rename "$slv_file.new", $slv_file or die "Could not rename: $!";
            mytouch("slv",{name => $distv});
            return {
                    lfh => $lfh,
                    lc_ts => $lc_ts,
                    lc_pass => $passfail{PASS}//0,
                    lc_fail => $passfail{FAIL}//0,
                   }; # did it!
        } else {
            warn "Alert: Error running ctgetreports; command was [$system]";
            return;
        }
    } else {
        return;
    }
}

{
    my($old_highscore);
    # have seen complications possibly leading to huge CPU waste
    my %unwanted = map { $_ => 1 } (
        "Data-Sah-Coerce-0.040",
        "libxml-enno-1.02",
    );
    sub enqueue {
        my($distv,$want_ctgetreport,$ctxi) = @_;
        if ($unwanted{$distv}) {
            require Carp;
            Carp::cluck("$distv is generally unwanted, go away.");
            sleep 60;
            return;
        }
        my $redis = myredis;
        unless (defined $old_highscore) {
            my($topdistv,$highscore) = $redis->zrevrange("analysis:jobqueue:q",0,0,"withscores");
            $old_highscore = $highscore;
            $old_highscore //= 0;
        }
        my($thisscore) = $redis->zscore("analysis:jobqueue:q",$distv);
        $thisscore //= 0;
        my $score = $want_ctgetreport - $ctxi; # linear decreasing
        if ($ctxi < 40 && $old_highscore > $score) {
            # the first 40 deserve a better score than the old ones
            $score = $old_highscore + 40 - $ctxi;
        }
        if ($score<=0) {
            $score = 1;
        }
        if ($thisscore >= $score) {
            # nothing to do
            return;
        }
        my $debug = 1;
        if ($debug) {
            my($zcard) = $redis->zcard("analysis:jobqueue:q");
            warn "DEBUG: enqueueing ctxi[$ctxi] old_highscore[$old_highscore] score[$score] distv[$distv] zcard[$zcard]\n";
        }
        $redis->zadd("analysis:jobqueue:q", $score, $distv);
    }
}

=item process_ctg

Runs a ctgetreports job with all of our redis interaction

Revisited 2015-04-06: looks wrong to me. But remember, we have redis
1.2 on our ds8143 and that did not even have a hash. That's for
example why we used JSON::XS here, we could have used hash instead.
But I'm sure that the "pattern" that I was reading in the Redis
documentation at that time has changed significantly since then. Maybe
we should rewrite the code according to the current description.

=cut
sub process_ctg {
    my($system, $distv) = @_;
    my $start = time;
    my $redis = myredis;
    my $jsonxs = JSON::XS->new->indent(0);
    my $job_object = {distv=>$distv,start=>$start};
    my $json = $jsonxs->encode($job_object);
    my $sha1 = Digest::SHA->new(1);
    $sha1->add($json);
    my $hex = $sha1->hexdigest;
    my $id = $redis->get("analysis:jobqueue:jobs:$hex\:id");
    unless ($id) {
        my($nid) = $redis->incr("analysis:jobqueue:next.job.id");
        if (my($setnx) = $redis->setnx("analysis:jobqueue:jobs:$hex\:id" => $nid)) {
            $id = $nid;
        } else {
            warn "WARNING: Should never happen, it would mean that two jobs calc the same thing at the same time. json[$json]hex[$hex]";
            $id = $redis->get("analysis:jobqueue:jobs:$hex\:id");
        }
    }
    $redis->set("analysis:jobqueue:jobs:$id\:descr" => $json); # will set again afterwards
    $redis->set("analysis:jobqueue:jobs:$id\:hex" => $hex);
    $redis->sadd("analysis:jobqueue:runningjobs" => $id); # must srem
    my $ret = system $system;
    my $finished = time;
    my $took = $finished - $start;
    $job_object->{finished} = $finished;
    $job_object->{took} = $took;
    $job_object->{ret} = $ret;
    $json = $jsonxs->encode($job_object);
    $redis->set("analysis:jobqueue:jobs:$id\:descr" => $json); # setting again
    $redis->srem("analysis:jobqueue:runningjobs" => $id); # as promised
    $redis->rpush("analysis:jobqueue:recentjobs" => $id);
    while ($redis->llen("analysis:jobqueue:recentjobs") > 10000) {
        my($del_id) = $redis->lpop("analysis:jobqueue:recentjobs");
        my($del_hex) = $redis->get("analysis:jobqueue:jobs:$del_id\:hex");
        $redis->del("analysis:jobqueue:jobs:$del_hex\:id");
        $redis->del("analysis:jobqueue:jobs:$del_id\:descr");
        $redis->del("analysis:jobqueue:jobs:$del_id\:hex");
    }
    if ($took > 3600) {
        warn "Warning: slow calc on '$distv'; took[$took]";
    }
    return $ret;
}

=item read_ctx

Reads the context of this distv from the yaml file

=cut
sub read_ctx {
    my($distv) = @_;
    my $outfile = sprintf "%s/solved/%s.yml", $workdir, $distv;
    my @stat = stat $outfile or return;
    YAML::Syck::LoadFile($outfile);
}

=item store_ctx ($dbi,$ctx)

$dbi may be undef. (For the record, we often have no dbi handle
around, but if we have one, we want to pass it around. This has its
historical reason in sqlite's misbehaving when faced with concurrent
accesses. So we tried to keep the lifespan of dbi handles as short as
possible.)

Checks the hashref $ctx against the yaml file on disk, writes it if
something has changed. Then writes it also to the database. If this is
a first-time encounter and greenish is >= 3, it is also enqueued for a
first calc.

=cut
sub store_ctx {
    my($dbi,$ctx) = @_;
    my $outfile = sprintf "%s/solved/%s.yml", $workdir, $ctx->{distv};
    my @stat = stat $outfile;
    my $want_write = 0;
    my $want_enqueue = 0;
    {
        my $reduced_ctx = dclone $ctx;
        delete $reduced_ctx->{distro_age};
        my $curr_dump = YAML::Syck::Dump($reduced_ctx);
        if (@stat) {
            my $last_dump = do
                { open my $fh, $outfile or die "Could not open '$outfile': $!";
                  local $/;
                  <$fh>;
              };
            $want_write = $curr_dump ne $last_dump;
            no warnings 'uninitialized';
            if ($want_write and $ctx->{lastcalc} and $ctx->{passfail_overview}) {
                my $old_ctx = YAML::Syck::Load($last_dump);
                if ($ctx->{lastcalc} ne $old_ctx->{lastcalc}) {
                    # distlookup: last_calc_ts, lc_pass, lc_fail
                    db_store_lastcalc(
                        $dbi,
                        $ctx->{distv},
                        $ctx->{lastcalc},
                        $ctx->{lc_pass},
                        $ctx->{lc_fail},
                    );
                }
            }
        } else {
            $want_write = 1;
            if ($ctx->{greenish} && $ctx->{greenish} >= 3) {
                $want_enqueue = 1;
            }
        }
        if ($want_write) {
            mkpath dirname $outfile;
            open my $fh, ">", "$outfile.new" or die "Could not open > '$outfile.new': $!";
            print $fh $curr_dump;
            close $fh or die "Could not close: $!";
            rename "$outfile.new", $outfile or die "Could not rename: $!";
        }
        db_quickaccess_insert_or_update
            (
             undef,
             $ctx->{distv},
             $curr_dump,
             $ctx->{greenish},
             $ctx->{dist},
            );
    }
    if ($want_enqueue) {
        enqueue($ctx->{distv},0,1);
    }
    mytouch("yml",{name => $ctx->{distv}});
}

=item db_quickaccess_handle



=cut
sub db_quickaccess_handle {
    return CPAN::Blame::Config::Cnntp::common_quickaccess_handle();
}

=item db_quickaccess_insert_or_update

Convenience function for getting stuff into distcontext table. You
cannot set anything to null here, only to some value. Any value given
as undef is skipped in the generated update statement. Against common
best practice, this code first looks whether a record exists and only
does an insert if it does not exist. We believe this is OK since we
never want to delete records here.

Besides that, this function does nothing when the caller would not
change anything.

=cut
sub db_quickaccess_insert_or_update {
    my($dbi,$distv,$yaml,$greenish,$dist) = @_;
    $dbi ||= db_quickaccess_handle();
    my $sql = "SELECT yaml,greenish,dist FROM distcontext WHERE distv=?";
    my $rows = my_get_query
        (
         $dbi,
         $sql,
         $distv
        );
    if (!@$rows) {
        $sql = "INSERT INTO distcontext (distv) VALUES (?)";
        my_do_query
            (
             $dbi,
             $sql,
             $distv
            );
    }
    no warnings 'uninitialized';
    my(%othersets) = (yaml => $yaml, greenish => $greenish, dist => $dist);
    my(%oldvalues) =
        (
         yaml     => $rows->[0][0],
         greenish => $rows->[0][1],
         dist     => $rows->[0][2],
        );
    my(@set, @bind);
    while (my($k,$v) = each %othersets) {
        if (defined $v && $v ne $oldvalues{$k}) {
            push @set, "$k=?";
            push @bind, $v;
        }
    }
    return unless @bind;
    push @bind, $distv;
    # $sql = "UPDATE distcontext SET yaml=?, greenish=?, dist=? WHERE distv=?";
    $sql = sprintf "UPDATE distcontext SET %s WHERE distv=?", join(", ", @set);
    warn "DEBUG: sql[$sql] bind[@bind]";
    my_do_query($dbi, $sql, @bind);
}

=item db_quickaccess_delete

forbidden call, because we believe that we want to keep distv=>dist
relation here forever and in any case. We use that relation in
allversions stuff.

=cut
sub db_quickaccess_delete {
    my($distv) = @_;
    require Carp;
    Carp::confess("somebody called db_quickaccess_delete");
    my $dbi = db_quickaccess_handle();
    my $sql = "DELETE FROM distcontext WHERE distv=?";
    my_do_query
        (
         $dbi,
         $sql,
         $distv
        );
}

=item mytouch


=cut
sub mytouch ($$) {
    my($type,$message) = @_;
    my $file = sprintf "%s/solved/lastchange_%s.ts", $workdir, $type;
    open my $fh, ">", $file or die "Could not open >'$file': $!";
    print $fh YAML::Syck::Dump($message);
    close $fh or die "Could not close '$file': $!";
}

=item age_in_secs



=cut
sub age_in_secs {
    my($iso) = @_;
    my @date = unpack "a4 a1 a2 a1 a2", $iso; # 2006-07-04
    $date[2]--; # make month zero-based
    my $epoch = eval {  timegm(0,0,0,@date[4,2,0]);  };
    if ($@ || !$epoch) {
        require Carp;
        Carp::confess("ALERT: date[@date] iso[$iso]");
    }
    time - $epoch;
}

=item db_quickaccess_select_dist



=cut
sub db_quickaccess_select_dist {
    my($dbi,$distv) = @_;
    my $sql = "SELECT dist FROM distcontext WHERE distv=?";
    my $rows = my_get_query
        (
         $dbi,
         $sql,
         $distv
        );
    return unless @$rows>0;
    return $rows->[0][0];
}

=item db_fixup_release_date

Fetch a release date from metacpan and write it into
release_date_metacpan in distlookup. Dies if the field is not empty.

=cut
sub db_fixup_release_date {
    my($dbi,$dist,$version,$distv) = @_;
    $dbi ||= db_quickaccess_handle();
    $distv ||= "$dist-$version";
    my $sql = "SELECT release_date_metacpan FROM distlookup WHERE distv=?";
    my $rows = my_get_query
        (
         $dbi,
         $sql,
         $distv
        );
    if (defined $rows->[0][0] and length $rows->[0][0]) {
        die "Alert: found record for distv '$distv' where release_date_metacpan is '$rows->[0][0]'";
    }
    my $ua = LWP::UserAgent->new(agent => "analysis.cpantesters.org/$VERSION");
    $ua->default_header("Accept-Encoding", "gzip");
    my $jsonxs = JSON::XS->new->indent(0);
    # my $query = "http://api.metacpan.org/v0/release/_search?q=release.name:$distv&fields=name,date,status";
    my $query = "http://fastapi.metacpan.org/v1/release/_search?q=name:$distv&fields=name,date,status";
    my $resp = $ua->get($query);
    my $date;
    if ($resp->is_success) {
        my $content = $resp->decoded_content;
        my $mcpananswer = $jsonxs->decode($content);
        if (my $h = $mcpananswer->{hits}{hits}) {
            $date = $h->[0]{fields}{date};
        } else {
            warn sprintf "Warning: result from metacpan api has no hits: %s\n", $content;
        }
    } else {
        die sprintf "No success getting from metacpan: query '%s' response '%s'", $query, $resp->as_string;
    }
    warn sprintf "DEBUG: rows[%d] distv[%s] dist[%s] date[%s]\n", scalar @$rows, $distv, $dist, $date;
    $sql = "UPDATE distlookup set release_date_metacpan=? WHERE distv=?";
    my $success = my_do_query($dbi,$sql,$date,$distv);
    return
        +{
          success => $success,
         };
}

=item (void) db_store_lastcalc ($dbi, $distv, $last_calc_ts, $lc_pass, $lc_fail)

Stores the last calc's coordinates. Short-circuits if they are already there.

=cut
sub db_store_lastcalc {
    my($dbi, $distv, $last_calc_ts, $lc_pass, $lc_fail) = @_;
    $dbi ||= db_quickaccess_handle();
    my $sql = "SELECT last_calc_ts, lc_pass, lc_fail FROM distlookup WHERE distv=?";
    my $rows = my_get_query
        (
         $dbi,
         $sql,
         $distv
        );
    if (    defined $rows->[0][0]
        and length $rows->[0][0]
       ) {
        if (     $last_calc_ts eq $rows->[0][0]
             and $lc_pass == $rows->[0][1]
             and $lc_fail == $rows->[0][2]
           ) {
            return; # short circuit
        } else {
            $sql = "UPDATE distlookup SET last_calc_ts=?, lc_pass=?, lc_fail=? WHERE distv=?";
            my $success = my_do_query($dbi, $sql, $last_calc_ts, $lc_pass, $lc_fail, $distv);
            unless ($success) {
                warn "Warning: something went wrong storing lastcalc stuff";
            }
            return;
        }
    } else {
        warn "Warning: something wrong with this record, rows->[0][0] empty";
        return;
    }
}

=item db_quickaccess_fixup

as of 201401 never reached. Deprecated 201708.

=cut
sub db_quickaccess_fixup {
    my($dbi,$distv,$dist) = @_;
    die "db_quickaccess_fixup is deprecated";
}

=item cleanup_quick

as of 201401 never reached

=cut
sub cleanup_quick {
    my($touched_distv) = @_;
    my $slv_dir = sprintf("%s/solved", $workdir);
    opendir my $dh, $slv_dir or die "Could not opendir '$slv_dir': $!";
    my(%deleted_distv, %fixed_dist);
    my $i = 0;
    my @readdir = readdir $dh;
    warn sprintf "Info: readdir found %d entries in solv_dir[%s]\n", scalar @readdir, $slv_dir;
    my $skipped_cleanup_because_touched = 0;
 DIRENT: for my $dirent (@readdir) {
        next DIRENT if $dirent =~ /\~$/;
        $i++;
        my($basename,$exte) = $dirent =~ /(.+)\.(yml|slv|slvdv|slvdv\.gz|ts)$/;
        warn sprintf
            ("DEBUG: %s UTC: checking dirent %d: %s; total deleted: %d",
             scalar gmtime,
             $i,
             $dirent,
             scalar keys %deleted_distv,
            ) unless $i % 10000;
        next DIRENT unless defined $exte;
        my $dbi = db_quickaccess_handle();
        my $dist = db_quickaccess_select_dist($dbi,$basename);
        if ($dist and !$fixed_dist{$dist}++) {
            db_quickaccess_fixup($dbi,$basename,$dist);
        }
        next DIRENT if $exte =~ /^(yml|ts)$/;
        if ( $touched_distv->{$basename} ){
            $skipped_cleanup_because_touched++;
            # we may have to get a fresh context so we can delete
            # outdated modules now?
            next DIRENT;
        }
        next DIRENT if $deleted_distv{$basename};
        # boring, boring: since we do not delete old stuff in the
        # directory, this pretends to delete in quickaccess all
        # things already deleted, 15000 distros and growing
        #### print "DEBUG: deleting from quickaccess $basename\n";
        db_quickaccess_delete($basename);
        $deleted_distv{$basename}++;
    }
    warn "DEBUG: skipped cleanup because touched[$skipped_cleanup_because_touched]";
}

=item megaquery



=cut
sub megaquery {
    use IPC::ConcurrencyLimit;
    my($basename) = File::Basename::basename(__FILE__);
    my $limit = IPC::ConcurrencyLimit->new
        (
         max_procs => 1,
         path      => "$workdir/IPC-ConcurrencyLimit-$basename-megaquery",
        );
    my $limitid = $limit->get_lock;
    while (not $limitid) {
        warn "Another process appears to be running a megaquery, sleeping";
        sleep 300;
        $limitid = $limit->get_lock;
    }
    my $dbi = mypgdbi();
    my $sqlimit = 30_000_000;
    my $elasticity = 2000;
    if ($Opt{maxfromdb} && $Opt{maxfromdb} < $sqlimit/$elasticity) {
        # the $elasticity here is a pure guess that says if we want
        # 200 hits, we should read 200*$elasticity records; indeed we
        # got 119 record from reading 200000
        $sqlimit = $elasticity*$Opt{maxfromdb};
    }
    # memory bummer:
    my $sql = "SELECT id,state,dist,version,fulldate
 FROM cpanstats
 WHERE type=2 and state='fail'
 ORDER BY id DESC LIMIT $sqlimit";
    print "Info: about to megaquery ".gmtime()." UTC\n";
    my $rows = my_get_query($dbi, $sql);
    print "Info: megaquery done ".gmtime()." UTC\n";
    return $rows;
}

=item main_enqueuer



=cut
sub main_enqueuer {
    my($want_ctgetreport, $touched_distv, $distroquestlabel, $anno, $timetogo,$urgenttimetogo) = @_;
 CTG: for my $ctxi (0..$#$want_ctgetreport) {
        my $ctx = $want_ctgetreport->[$ctxi];
        my($dist,$distv,$version) = @{$ctx}{"dist","distv","version"};
        $touched_distv->{$distv}=1;
        if ($Opt{pick}) {
            my $didit = call_ctgetreports($ctx,1+$ctxi,1+$#$want_ctgetreport);
            if ($didit) {
                my $lfh = $didit->{lfh};
                # update the yml, we might have to parse something in the new reports
                my $dbi = mypgdbi();
                $ctx = distroquestion($dbi,1+$ctxi,$distroquestlabel,$dist,$version,$lfh);
                $ctx->{annotation} = $anno->{$distv};
                $ctx->{lastcalc} = $didit->{lc_ts};
                $ctx->{lc_pass} = $didit->{lc_pass};
                $ctx->{lc_fail} = $didit->{lc_fail};
                store_ctx($dbi,$ctx);
            }
        } else {
            enqueue($distv,$#$want_ctgetreport,$ctxi);
        }
        my $time = time;
        my $mustgo = 0;
        if ($time > $urgenttimetogo) {
            $mustgo = 1;
        } elsif ($ctxi == $#$want_ctgetreport){
            $mustgo = 1;
        } elsif (
            1
            && $time            >= $timetogo
            && $ctxi            >= MUST_CONSIDER
           ){
            $mustgo  = 1;
        }
        if ($mustgo) {
            my $utc = gmtime($timetogo);
            warn "DEBUG: proc[$$]ctxi[$ctxi]timetogo[$timetogo]that is[$utc UTC]";
            last CTG;
        }
    }
}

=item this_urgent

Originally the decision whether a new calc was due was taken by a
random function. We looked at the timestamp of the yaml file and of
the slvfile. Then we read the yaml file and there we concluded on the
upload date. Then we decided how often we want to calc a distro
depending on its age. We mixed some randomness in and said this is
urgent or not. Mostly crap.

Mixing in cpanstats_distrofacts and analysis:distv:calctimestamp?
Especially interested in pass/fail on last calc and pass/fail now.

In cpanstats_distrofacts I see only (for Crypt-Password-0.28)

   "db_fails" : 147,
   "db_maxid" : 41454838,
   "db_maxid_ts" : "2014-04-18T09:29:04.016700Z",
   "db_others" : 1,
   "db_passes" : 1923,
   "thirdfail" : "2012-02-19 02:40z",
   "thirdfailid" : "20108048",
   "thirdpass" : "2012-02-18 03:29z",
   "thirdpassid" : "20078591"

analysis:distv:calctimestamp has only 2500 entries at the moment, so
is not too interesting yet.

Nein, distlookup ist unser Kandidat. Diese pg Tabelle hat

 cuti_ts               | timestamp with time zone | 
 cuti_pass             | integer                  | 
 cuti_fail             | integer                  | 
 last_calc_ts          | timestamp with time zone | 
 lc_pass               | integer                  | 
 lc_fail               | integer                  | 

wobei cuti fuer counted_up_to_id steht. Und wobei last_calc_ts
anscheinend redundant auf analysis:distv:calctimestamp ist.

=cut
sub this_urgent {
    my($dbi,$dist,$version,$distv,$anno) = @_;
    $distv ||= "$dist-$version";
    $anno ||= read_annotations();

    $dbi ||= db_quickaccess_handle();
    my $sql = "SELECT last_calc_ts, lc_pass, lc_fail, cuti_ts, cuti_pass, cuti_fail
               FROM distlookup WHERE distv=?";
    my $rows = my_get_query
        (
         $dbi,
         $sql,
         $distv
        );
    my $expensive_type = 0; # 9 huge, 0 not
    my $lowactivity = "";
    my $maybeexpensive = "";
    if (!$rows->[0][4] || !$rows->[0][5]) {
        return +{ this_urgent => 0 };
    }
    unless (grep { ($rows->[0][$_]||0)==0 } 1,2,4,5) {
        my($lc_ts,$lc_p,$lc_f,$cnt_ts,$cnt_p,$cnt_f) = @{$rows->[0]};
        if ($cnt_p>1.025*$lc_p || $cnt_f>1.025*$lc_f) { # arbitrary
            my $lc_total = $lc_p + $lc_f;
            if ( $lc_total < 2400 ){
                return +{ this_urgent => 1 }; # activity
            } else {
                my($limiting) = min($cnt_p,$cnt_f);
                $maybeexpensive = "over 2400: $lc_p=>$cnt_p, $lc_f=>$cnt_f";
                if ( $lc_total/$limiting > 250 ) { # arbitrary
                    $expensive_type = 9;
                } else {
                    $expensive_type = 5;
                }
            }
        } elsif ($cnt_p < $lc_p || $cnt_f < $lc_f) { # arbitrary
            # we would like to get rid of such brethren; maybe run verify-distlookup-lcpassfail.pl
            warn "Sigh, $distv has cnt_p < lc_p || cnt_f < lc_f ($cnt_p < $lc_p || $cnt_f < $lc_f)";
            return +{ this_urgent => 0 }; # fix up bad numbers
        } elsif ($cnt_p == $lc_p && $cnt_f == $lc_f) {
            $lowactivity = "NO growth: $lc_p=>$cnt_p, $lc_f=>$cnt_f";
        } elsif ($cnt_p < 1.002*$lc_p && $cnt_f < 1.002*$lc_f) { # arbitrary
            $lowactivity = "small growth: $lc_p=>$cnt_p, $lc_f=>$cnt_f";
        }
    }

    my($csdfacts) = cpanstats_distrofacts($dbi,$dist,$version,$distv,undef);
    if (
        $csdfacts
        && ! $expensive_type
        && $csdfacts->{allversions}
        && "ARRAY" eq ref $csdfacts->{allversions}
        && scalar @{$csdfacts->{allversions}}
        && exists $csdfacts->{allversions}[-1]{version}
        && $csdfacts->{allversions}[-1]{version} ne $version
       ) {
        warn "DEBUG: this is urgent: $distv $version ne $csdfacts->{allversions}[-1]{version}";
        return +{ this_urgent => 1 };
    }
    if ($lowactivity) {
        return +{ this_urgent => 0, why_not_urgent => $lowactivity };
    }

    my $averagewait = 86400; # XXX arbitrary: the smaller the more
                             # calc; Actually: we use rand to avoid
                             # spikes; zero waits and a double of this
                             # wait are equally likely

    my $minimumwait = 1.9; # XXX arbitrary: the smaller the more calc;
                           # Actually: for every year of age of the
                           # distro we add this times $averagewait
                           # waiting time. Since no age below 1 is
                           # possible, this is the minimum wait;
                           # setting from 1.7.to 1.8 when the usually
                           # set task was 3500 but we only calculated
                           # 1000; either we increase the mincalc or
                           # reduce the pensum, I would guess;
                           # 20160527: taking back to (1.7, 2500),
                           # disk gets too full too quickly

    if ($maybeexpensive) {
        # XXX arbitrary: if we have indications that the calc takes a
        # longer time, push back
        $minimumwait = $expensive_type >= 6 ? 42 : 15;
    }
    if ($anno->{$distv} && $minimumwait < 5) {
        $minimumwait = 5; # e.g. GFUJI/MouseX-Getopt-0.34.tar.gz
    }

    my $slv_file = slv_file($distv);
    my @slvstat = stat ($slv_file . "dv.gz");
    @slvstat = stat $slv_file unless @slvstat;
    my $age_of_calc = @slvstat ? time-$slvstat[9] : 999999;

    my $yaml = read_ctx($distv);
    if ($anno->{$distv}
        and (!$yaml->{annotation} || $yaml->{annotation} ne $anno->{$distv})) {
        # looks like this annotation has not been stored yet
        return +{ this_urgent => 1 };
    }
    my ($this_urgent, $why_not_urgent) = (0,"");
    my $age_in_secs = eval { age_in_secs($yaml->{upload_date}) }; # distro age in secs
    if ($@ || !$age_in_secs) {
        my $err = $@;
        require Carp;
        Carp::cluck("WARNING: skipping '$yaml->{distv}' error[$@] upload_date[$yaml->{upload_date}] distv[$distv]");
        $this_urgent = 0;
        if ($err) {
            $why_not_urgent = $err;
        } else {
            $why_not_urgent = "no upload date";
        }
        return +{ this_urgent => $this_urgent, why_not_urgent => $why_not_urgent};
    }
    my $seconds_per_year = 86400*365.25;
    my $age_points = $age_in_secs < $seconds_per_year ?
        1 : $age_in_secs/$seconds_per_year; # between 1 and 8
    my $rand = int(rand(2*$averagewait));
    my $rand2 = int($age_points*($rand+$averagewait*$minimumwait));

    $this_urgent = $age_of_calc > $rand2; # XXX arbitrary
    warn sprintf "DEBUGurg: age_of_calc[%s] age_points[%.3f] rand[%s] rand2[%s] urg[%d]\n",
        $age_of_calc, $age_points, $rand, $rand2, $this_urgent;
    if (!$this_urgent) {
        if (!@slvstat) {
            $this_urgent = 1;
            warn "DEBUG urg2: urgent because no slvstat"; # never happens
        } else {
            $why_not_urgent = "age/point calculations";
        }
    }
    return +{ this_urgent => $this_urgent, why_not_urgent => $why_not_urgent};
}

my($timetogo,$urgenttimetogo);
{
    $timetogo       = $^T + ($Opt{mincalctime} || MIN_CALCTIME);
    $urgenttimetogo = MAX_CALCTIME + $^T;
}
MAIN: {
    my $mainentrytime = time;
    my $solved = [];
    my $anno = {};
    if ($Opt{onlystartwhennojobs}) {
        my $redis = myredis;
    WAITNOQ: while () {
            my($zcard) = $redis->zcard("analysis:jobqueue:q");
            last WAITNOQ if $zcard == 0;
            sleep 60;
        }
    }
    if ($Opt{pick}) {
        warn "saw --pick=$Opt{pick}, skipping readdir on solved/ directory";
        my $shadow_anno = read_annotations();
        if (exists $shadow_anno->{$Opt{pick}}) {
            $anno->{$Opt{pick}} = $shadow_anno->{$Opt{pick}};
        }
    } else {
        $anno = read_annotations();
        unless (defined $Opt{maxfromsolved} && 0 == $Opt{maxfromsolved}) {
            # looks in workdir/solved/ where we find eg. a2pdf-1.13.{slv,slvdv,yml}
            $solved = CPAN::Blame::Model::Solved->all("readdir");
        }
    }
    my(%seen,@want_ctgetreport);
    # revisit all our friends first
    my $touched_distv = {};
 ANNO_SOLVED: for my $set
        (
         {name => "anno", data => $anno}, # anno is a hashref
         {name => "solved", data => $solved}, # solved is an arrayref
        ) {
        my @k; # names like "a2pdf-1.13"
        my $urgent = $Opt{urgent} || 0;
        if (0) {
        } elsif ($Opt{pick}) {
            # pick trumps, the others don't play when we are a worker
            @k = $Opt{pick};
        } elsif ($set->{name} eq "anno") {
            @k = keys %{$set->{data}};
            if (defined $Opt{maxfromannotate}) {
                pop @k while @k > $Opt{maxfromannotate};
            }
        } elsif ($set->{name} eq "solved") {
            @k = map { $_->{distv} } @{$set->{data}};
            if (defined $Opt{maxfromsolved}) {
                pop @k while @k > $Opt{maxfromsolved};
            }
        } else {
            die;
        }
        my $distroquestlabel = substr($set->{name},0,4) . "-" . scalar @k; # gives a bit of orientation in the runlog
        my $i = 0;
    DISTV: for my $k (@k) {
            $touched_distv->{$k}=1;
            my $d = CPAN::DistnameInfo->new("FOO/$k.tgz");
            my $dist = $d->dist;
            my $version = $d->version;
            ++$i;
            next DISTV if $seen{$dist,$version}++;
            my($this_urgent, $why_not_urgent);
            my $slv_file = slv_file($k);
            if ($urgent) {
                $this_urgent = 1;
            } elsif (! -e ($slv_file."dv.gz") && ! -e ($slv_file."dv")) {
                $this_urgent = 1;
            } elsif ($Opt{pick}) {
                $this_urgent = 1;
            } else {
                my $href = this_urgent(undef,$dist,$version,$k,$anno);
                ($this_urgent, $why_not_urgent) = @{$href}{qw(this_urgent why_not_urgent)};
            }
            unless ($this_urgent){
                printf "Warning: skipping %s (%s)\n", $k, $why_not_urgent;
                next DISTV;
            }
            my $pgdbi = mypgdbi();
            my $ctx = distroquestion($pgdbi,$i,$distroquestlabel,$dist,$version,undef) or next;
            $ctx->{annotation} = $anno->{$k};
            store_ctx($pgdbi,$ctx);
            push @want_ctgetreport, $ctx;
        }
    } # ANNO_SOLVED
    my $rows = [];
    if ($Opt{pick}) {
    } else {
        unless (defined $Opt{maxfromdb} && 0==$Opt{maxfromdb}) {
            $rows = megaquery();
        }
    }
    my $skip_random = 0;
    my $i = 0;
    my $mindate;
    {
        my @now = gmtime;
        $now[4]++;
        $now[5]+=1900;
        $mindate = sprintf "%04d%02d%02d%02d%02d", $now[5]-$CPAN::Blame::Config::Cnntp::Config->{maxyears}, @now[reverse 1..4];
    }
    warn sprintf
        (
         "DEBUG: before going through megaquery result: array want_ctgetreport[%d] rows from megaquery[%d]\n",
         scalar @want_ctgetreport,
         scalar @$rows,
        );
    my $distroquestlabel = "B" . scalar @$rows;
    my $why_last_article = "";
 ARTICLE: while (my $row = shift @$rows) {
        sleep 0.3;
        my($id,$state,$dist,$version,$date) = @$row;
        unless ($id){
            $why_last_article = "no id in row[@$row]";
            last ARTICLE;
        }
        if ($date lt $mindate){
            $why_last_article = "date[$date] lt mindate[$mindate] i[$i]";
            last ARTICLE;
        }
        next ARTICLE unless defined $dist and defined $version;
        if ($seen{$dist,$version}++){
            $why_last_article = "last action was skipping $dist,$version (reaching last ARTICLE via next)";
            next ARTICLE;
        }
        # note: when we arrive here. We will display current $i in the leftmost column as in
        # 170 B2801844 13-10-29 147: 21:  0 KSTAR/Dancer-Plugin-DynamicConfig-0.04 (<0.07)
        $i++;
        if ($Opt{maxfromdb} && $i > $Opt{maxfromdb}) {
            last ARTICLE;
        }
        my $pgdbi = mypgdbi();
        my $ctx = distroquestion($pgdbi,$i,$distroquestlabel,$dist,$version,undef) or next;
        $ctx->{annotation} = $anno->{$ctx->{distv}};
        store_ctx($pgdbi,$ctx);
        if ($ctx->{greenish} && $ctx->{greenish} >= 3) {
            push @want_ctgetreport, $ctx;
        }
        if ($i >= 487) { # XXX arbitrary
            if ($skip_random) {
                # note: in 2013 we never reached 800, we reached the 8
                # year old distros before that, yesterday at $i==666,
                # and want_ctgetreport had 3823 entries
                if ($i >= 1200 && @want_ctgetreport >= 200) { # XXX arbitrary
                    $why_last_article = "i[$i] > xxx and want_ctgetreport > 150";
                    last ARTICLE;
                } elsif ($i >= 800 && @want_ctgetreport >= 300) { # XXX arbitrary
                    $why_last_article = "i[$i] > xxx and want_ctgetreport > 200";
                    last ARTICLE;
                }
            } else {
                printf "Debug: switching to skip_random mode\n";
                $skip_random = 1;
            }
        }
        if ($skip_random) {
            # compensate for the fact that we do not have and do not
            # even want to provide the computing power to calculate
            # all possible regressions simultaneously; yes, we are a
            # bit sloppy but do not believe anybody will notice; some
            # day we may want to stop this program to go into the
            # oldest data in the database because it will become
            # worthless; then we probably want to make sure all data
            # in a certain range get processed exactly once.
            my $skip = int rand 40000; # XXX arbitrary
            splice @$rows, 0, $skip;
        }
        sleep 0.1;
        $why_last_article = "simply reached end of loop";
    }
    warn sprintf "DEBUG: Reached last ARTICLE why?[%s] array want_ctgetreport[%d]", $why_last_article, scalar @want_ctgetreport;
    @want_ctgetreport = sort
        {
            # Wishlist: first sort criterium should be in favor of
            # those who have no calc yet at all. But then we might
            # schedule them too early because cpantesters are behind
            # log.txt
            ($b->{upload_date}||"") cmp ($a->{upload_date}||"")
            ||
            ($a->{distro_age}||0) <=> ($b->{distro_age}||0)
            ||
            $a <=> $b # crap, just the address because there is nothing better in sight
        } @want_ctgetreport;
    $distroquestlabel = "ctg-" . scalar @want_ctgetreport;
    main_enqueuer(\@want_ctgetreport, $touched_distv, $distroquestlabel, $anno, $timetogo,$urgenttimetogo);
    if ($Opt{pick}) {
        warn "nothing will be deleted, we're on a picking trip";
    } else {
        # XXX temporarily disabling cleanup_quick on 2014-01-01;
        # because we do not understand the exact implications of the
        # queuing system; but this for sure must be re-examined.
        #### cleanup_quick($touched_distv);
    WAITTOGO: while (time < $timetogo) {
            if ($Opt{leavewhennojobsleft}) {
                my $redis = myredis;
                my($zcard) = $redis->zcard("analysis:jobqueue:q");
                last WAITTOGO if $zcard == 0;
            }
            sleep 60;
        }
    }
}


1;

__END__

=back

=head1 BUGS

TRIAL?

Too slow update cycle

Often does not recognize when a newer version is available, maybe only
when the newer version is all-green or so?

Sqlite

=head1 HISTORICAL NOTES

show the most recent FAILs on cpantesters. Compare to annotations.txt
which I maintain manually in annotations.txt. Roughly the job is
organized as (1) update the database syncing with cpantesters, (2)
process (2.1) annotations, (2.2) previously solved regressions, (2.3)
database to schedule some 1000 batchjob-candidates with (2.3.1) is the
megaquery and (2.3.2) steps through it. (3) Run these batchjobs, many
of which are being skipped. (4) delete several thousand outdated files
and/or directories. Timings on 2013-10-29 and -30

  |         | 20131029 | 20131127 |
  |---------+----------+----------|
  | (1)     | 06:30:09 |          |
  | (2.1)   | 10:54:22 | 10:28:49 |
  | (2.2)   | 15:15:43 | 11:09:37 |
  | (2.3.1) | 20:41:42 | 13:26:49 |
  | (2.3.2) | 21:08:51 | 13:29:02 |
  | (3)     | 22:53:15 | 14:29:28 |
  | (4)     | 16:44:07 |          |
  | (T)     | 16:45:00 |          |

The schema of cpanstats.db once was considered to be

  0 | id       | 5379605                         |5430514
  1 | state    | pass                            |fail
  2 | postdate | 200909                          |
  3 | tester   | bingos@cpan.org                 |
  4 | dist     | Yahoo-Photos                    |Apache-Admin-Config
  5 | version  | 0.0.2                           |0.94
  6 | platform | i386-freebsd-thread-multi-64int |i386-freebsd
  7 | perl     | 5.10.0                          |
  8 | osname   | freebsd                         |
  9 | osvers   | 7.2-release                     |7.2-release
 10 | date     | 200909190440                    |200909190440

but has changed to

   id            INTEGER PRIMARY KEY, -- 1
   state         TEXT,                -- 2
   postdate      TEXT,                -- 3
   tester        TEXT,                -- 4
   dist          TEXT,                -- 5
   version       TEXT,                -- 6
   platform      TEXT,                -- 7
   perl          TEXT,                -- 8
   osname        TEXT,                -- 9
   osvers        TEXT,                -- 10
   date          TEXT,                -- 11
   guid          char(36) DEFAULT '', -- 12
   type          int(2) default 0)    -- 13

guid and type are new. guid replaces id but is in a different format. schema is generated by

  Revision history for Perl module CPAN::Testers::Data::Generator.

  0.41    18/03/2010
          - fixes to change the 'id' (was NNTP ID) to an auto incremental field.
          - reworked logic to better fit latest changes.
          - added repository to META.yml.
          - documentation updates.

  0.40    02/02/2010
          - fixes to accommodate GUID changes.
          - added support for 'type' field.

From reading the source, type seems to be 0 or 2 for valid records. 1
is for pause announcements and 3 is for disabled reports. IIUC 0 is
for old records and 2 for new ones. That would mean that there is a
constant number of records with type==0?

A few months ago we counted:

  sqlite> select type,count(*) from cpanstats group by type;
  0|1570
  1|123723
  2|6899753
  3|15131

and today:

  sqlite> select type,count(*) from cpanstats group by type;
  0|1570
  1|130858
  2|7745256
  3|15131

So we can rely on having to read only type==2 from now on.

But before we can do anything we must
s/CPAN::WWW::Testers::Generator::Database/CPAN::Testers::Data::Generator/g; or
something equivalent.

I see no reason why I should make use of the guid except maybe for
links to cpantesters. Internal stuff should work with id just like it
always did.

Update 2011-10-12 andk: schema change again, new schema is:

   id          INTEGER PRIMARY KEY,    -- 1
   guid        TEXT,                   -- 12
   state       TEXT,                   -- 2
   postdate    TEXT,                   -- 3
   tester      TEXT,                   -- 4
   dist        TEXT,                   -- 5
   version     TEXT,                   -- 6
   platform    TEXT,                   -- 7
   perl        TEXT,                   -- 8
   osname      TEXT,                   -- 9
   osvers      TEXT,                   -- 10
   fulldate    TEXT,                   -- is new, probably was date (11)
   type        INTEGER                 -- 13



=head1 TODO

open tickets?

