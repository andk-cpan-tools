#!perl


=head1 NAME

testing-rmirror-k75-k81 - 

=head1 SYNOPSIS



=head1 DESCRIPTION

We like to call it as root and not with sudo because of strange
permission problems on the .perldb file.

/usr/local/perl-5.10-uld/bin/perl -I /home/k/sources/rersyncrecent/lib -d /home/k/sources/CPAN/andk-cpan-tools/bin/testing-rmirror-k75-k81.pl

=cut


die "obsolete";
$ENV{USER}="sandy";
$ENV{RSYNC_PASSWORD} = "VteTseXA"; # password published knowingly

use strict;
use File::Rsync::Mirror::Recent;
my @rrr = map {
    my $statusfile = "/root/rmirror-status-$_.state";
    my $r;
    if (0 && -f $statusfile) { # 0 && because it's not yet tested 2009-04-25 akoenig 
        $r = File::Rsync::Mirror::Recent->thaw ( $statusfile );
    } else {
        $r = File::Rsync::Mirror::Recent->new
            (
             ignore_link_stat_errors   => 1,
             localroot                 => "/home/ftp/pub/PAUSE/$_",
             max_files_per_connection  => 22501,
             remote                    => "192.168.2.101::PAUSE/$_/RECENT.recent",
             tempdir                   => "/home/ftp/tmp",
             ttl                       => 10,
             rsync_options             =>
             {
              # port => 8732,
              # compress => 1,
              links => 1,
              times => 1,
              timeout => 600,
              checksum => 0,
              'omit-dir-times'  => 1, # not available before rsync 3.0.3
             },
             verbose                   => 1,
             # verboselog                => "/var/log/rmirror-k75-k81.log",
             _runstatusfile            => $statusfile,
            );
    }
    $r;
} "modules", "authors";
die "directory $_ doesn't exist, giving up" for grep { ! -d $_->localroot } @rrr;
while (){
    my $ttgo = time + 20;
    for my $rrr (@rrr){
        $rrr->rmirror ( );
    }
    # last;
    my $sleep = $ttgo - time;
    if ($sleep >= 1) {
        print STDERR "sleeping $sleep ... ";
        sleep $sleep;
    } else {
        printf STDERR "No time to sleep: ttgo[%s] time[%s]\n", $ttgo, time;
    }
}
