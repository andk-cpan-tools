#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--tail!>

With tail, the end of file is ignored the program waits for new lines.
Default is true on a tty, elsewhere default is false.

=back

=head1 DESCRIPTION



=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{tail} //= -t STDOUT;

use strict;
use JSON::XS;
use CPAN::Version;
use Time::Moment;
use Time::HiRes qw(sleep);
use Graph::Feather; # schade, schade, verlangt 5.22; ==> distroprefs
my $gf = Graph::Feather->new;

sub myts_to_tmom {
    my($ts) = @_;
    my @x0 = $ts =~ /^(\S+)-(\S+)-(\S+)[Tt ](\S+):(\S+):(\S+)\.(\d+)/
        or warn "strange date to parse '$ts' did not parse well";
    my $nano = $x0[6];
    $nano .= "0" while length($nano)<9;
    my $tm0 = eval { Time::Moment->new(
        year       => $x0[0],
        month      => $x0[1],
        day        => $x0[2],
        hour       => $x0[3],
        minute     => $x0[4],
        second     => $x0[5],
        nanosecond => $nano,
        offset     => 0,
        # precision  => 3,
        )} or die "error while feeding date (@x0) to Time::Moment: $@";
    return $tm0;
}

sub tsums {
    my $t = shift;
    my %t;
    for my $i (0..$#$t) {
        no warnings 'uninitialized';
        my $tt = 0;
        if ($t->[$i][1] && $t->[$i][2]) {
            my $tm0 = myts_to_tmom($t->[$i][1]);
            my $tm1 = myts_to_tmom($t->[$i][2]);
            $tt = $tm0->delta_nanoseconds($tm1) / 1_000_000_000;
        }
        # printf "                       %-14s %-26s  %-26s %12.2f\n", @{$t->[$i]}, $tt;
        $t{$t->[$i][0]}=$tt;
    }
    return \%t;
}

sub pred_walk {
    my($gf,$v) = @_;
    my @queue = map { [$_, 1] } $gf->predecessors($v);
    return sub {
        if (@queue) {
            my $node = shift @queue;
            my($name, $depth) = @$node;
            if (0 && $gf->get_vertex_attribute($name,"iscommand") > 0) {
                @queue = ();
            } else {
                push @queue, map {[$_, $depth+1]} $gf->predecessors($name);
            }
            return $node;
        } else {
            return;
        }
    }
}

our($LINE,%LINE,%S,%T);
my $file = shift or die "Usage: $0 file";
open my $gw_fh, $file or die "Could not open $file: $!";
$|=1;
my $curpos;
my $statusline;
my $stalledtime;
my $parallelo;
seek($gw_fh, 0, 1);
while () {
    LINE: for ($curpos = tell($gw_fh); <$gw_fh>; $curpos =tell($gw_fh)) {
        my $syslog = $1 if s/(^.+?)\{/{/;
        my $h = eval { decode_json($_); };
        if (! defined $h) {
            my $line = $_;
            my $errmsg = $@;
            my $dumper = Dumpvalue->new;
            $dumper->set_quote('"');
            my $line_dump = $dumper->stringify($line);
            my $errmsg_dump = $dumper->stringify($errmsg);
            warn "Could not parse line=$line_dump, errmsg=$errmsg_dump";
            next LINE;
        }
        if (exists $h->{CALLED_FOR} && ($h->{CALLED_FOR} eq "Chemistry::File::SMILES" || $h->{CALLED_FOR} eq "Chemistry::Ring")) {
            warn "Ignoring all loglines about $h->{CALLED_FOR} due circular deps noise";
            next LINE;
        }
        #next unless $h->{method} =~ /^post_(install)/;
        $h->{success} = "";
        if ($h->{make_test} && $h->{make_test} =~ /^(YES|NO)/) {
            my $yesno = $1;
            $h->{success} = $yesno;
            if ( $h->{method} =~ /^post_(?:install)/ ) {
                $S{$yesno}++;
            }
        } else {
            if ( exists $h->{make}                && $h->{make} =~ /^NO/
                 || exists $h->{writemakefile}    && $h->{writemakefile} =~ /^NO/
                 || exists $h->{unwrapped}        && $h->{unwrapped} =~ /^NO/
                 || exists $h->{signature_verify} && $h->{signature_verify} =~ /^NO/ ) {
                $h->{success} = "NO";
                if ( $h->{method} =~ /^post_(?:install)/ ) {
                    $S{NO}++;
                }
            }
        }
        $S{queue_size} = $h->{queue_size};
        my $line; # misnomer: I was thinking metaphorically of the
                  # line in the bundle file and/or the commandline the
                  # user has entered
        our %depth;
        if ($h->{reqtype} eq "c") {
            $line = $LINE{$h->{CALLED_FOR}} ||= ++$LINE;
            $line = sprintf "%5d", $line;
        } else {
            $line = $h->{reqtype};
        }
        $h->{line} = $line;
        if ($h->{success}) { # yes *or* no is success
            delete $depth{$h->{pretty_id}};
            $h->{depth}=1 + scalar keys %depth;
        } else {
            $depth{$h->{pretty_id}}=undef;
            $h->{depth}=scalar keys %depth;
        }
        $h->{mandatory_label} = $h->{reqtype} eq "c" ? '-' : $h->{mandatory} ? "m" : "o";
        my $x_minus_depth = 77 - $h->{depth};
        my ($ts) = $syslog =~ /^([-0-9]+[Tt ][:0-9]+(?:\.[0-9]+)?)/;
        if (1) {
            my $t = $T{$h->{pretty_id}} ||= [];
            my ($prepost,$phase) = $h->{method} =~ /(pre|post)_(.+)/;
            if ( $prepost eq "pre" ) {
                push @$t, [ $phase, $ts, undef ];
            } else {
                my $e = 0;
                for (my $i= $#$t; $i>=0; $i--) {
                    if ( $phase eq $t->[$i][0] ) {
                        if ( $t->[$i][2] ) {
                            push @$t, [ $phase, undef, $ts ];
                        } else {
                            $t->[$i][2] = $ts;
                        }
                        $e = 1;
                        last;
                    }
                }
                unless ( $e ){
                    push @$t, [ $phase, undef, $ts ];
                }
            }
            # print "             ($h->{pretty_id})";
        }
        my $t = tsums($T{$h->{pretty_id}});
#        $gf->add_edge($h->{CALLED_FOR},$h->{pretty_id});
#        if ($h->{reqtype} eq "c") {
#            $gf->set_vertex_attribute($h->{CALLED_FOR},"iscommand",1);
#        }
#        $gf->set_vertex_attribute($h->{CALLED_FOR},"class","M");
#        $gf->set_vertex_attribute($h->{pretty_id},"class","D");
#        for my $prereq_cat (keys %{$h->{prereq_pm}}) {
#            my $hash = $h->{prereq_pm}{$prereq_cat};
#            for my $mod (keys %$hash) {
#                $gf->add_edge($h->{pretty_id},$mod);
#            }
#        }
        if ( $h->{method} =~ /^post_(?:install)/ ) {
            $h->{success_label} = $h->{success} || "-";
            printf "\r%5s %s %2d %*s%-*s %-49s %-3s %8.3f %6d %s\n", @{$h}{qw(line mandatory_label depth depth)}, " ", $x_minus_depth, @{$h}{qw(pretty_id CALLED_FOR success_label)}, $t->{install}, $S{queue_size}, $ts;
#            my $iterator = pred_walk($gf,$h->{pretty_id});
#            my %seen;
#            while (my $p = $iterator->()) {
#                my($name, $depth) = @$p;
#                my $class = $gf->get_vertex_attribute($name,"class");
#                if ($class eq "D") {
#                    last if $seen{$name}++;
#                    printf "<========= pred %s(%d)[%s]\n", $name, $depth, $class;
#                }
#            }
        } elsif ($Opt{tail}) {
            $statusline = sprintf "\r%8s %-56s %11.2f %11.2f %11.2f %11.2f y%d:n%d %5d %s", "==t==",
                $h->{pretty_id},
                $t->{install}||0,
                $t->{test}||0,
                $t->{make}||0,
                $t->{get}||0,
                $S{YES}||0,
                $S{NO}||0,
                $S{queue_size}||0,
                $ts
                if defined $h->{pretty_id}
                && defined $ts;
            $stalledtime = myts_to_tmom($ts)->epoch;
        }
    }
    if ($Opt{tail}) {
        my $waiting = time - $stalledtime;
        $parallelo ^= 1;
        printf "%s %5d  %s", $statusline,
            $waiting,
            $parallelo ? "▱": "▰"
            if $statusline;
        sleep 1;
        seek($gw_fh, $curpos, 0);  # seek to where we had been
    } else {
        last;
    }
}

# print "yes $S{YES} no $S{NO} queue_size $S{queue_size}\n"

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
