#!/bin/sh

PERL=$HOME/src/installed-perls/v5.16.0/4e6d/bin/perl
BINDIR=`dirname $0`
CONFIG=$BINDIR/../CPAN-Blame/lib/CPAN/Blame/Config/Cnntp.pm
SOLVER_VARDIR=`$PERL $CONFIG solver_vardir`
EXT_SRC=`$PERL $CONFIG ext_src`
echo "PERL=$PERL
BINDIR=$BINDIR
CONFIG=$CONFIG
SOLVER_VARDIR=$SOLVER_VARDIR
EXT_SRC=$EXT_SRC
"
sleep 5
set -x
set -e
while true; do
    # cd $SOLVER_VARDIR/workdir
    # cp $SOLVER_VARDIR/workdir/cpanstats.db $SOLVER_VARDIR/workdir/cpanstats.db~
    # $PERL bin/refill-cpanstatsdb.pl --db=$SOLVER_VARDIR/workdir/cpanstats.db
    # cd $EXT_SRC/andk-cpan-tools
    git pull
    # $PERL -I $EXT_SRC/andk-cpan-tools/CPAN-Blame/lib $EXT_SRC/andk-cpan-tools/bin/sync-sqlite-postgres.pl --max=2500000 --db=dbi:SQLite:dbname=$SOLVER_VARDIR/workdir/cpanstats.db~
    $PERL -I $EXT_SRC/andk-cpan-tools/CPAN-Blame/lib $EXT_SRC/andk-cpan-tools/bin/cnntp-solver.pl --onlystartwhennojobs
    : no endless loop if there is a stopfile
    if [ -e $EXT_SRC/andk-cpan-tools/bin/cnntp-solver.stop ] ; then
      break
    fi
done
