#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--fromdir=s>

=item B<--help|h!>

=item B<--todir=s>

This help

=back

=head1 DESCRIPTION

Quick oneliner

For the record: whatever we pass to ctgetreports as --cachedir will
then be populated with two subdirectories, cpantesters-show/ and
nntp-testers/

So in the past we had one cpantesters-show/ and one nntp-testers/
directory and now need tens of thousands of such directories.

When this program was written, we did it wrongly because the above
knowledge was lost.

So after the first iteration we have

 top-cachedir/nntp-testers/a/autobox-2.55/

But what we wanted was

 top-cachedir/a/autobox-2.55/nntp-testers

What's the transformation in the second step?

cd top-cachedir/nntp-testers
for f in ?/*; do
  mkdir -p ../$f
  mv $f ../$f/nntp-testers
done
  

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

my $olddir = $Opt{fromdir} ||= "nntp-testers-20111125";
$Opt{todir} ||= "nntp-testers";
opendir my $dh, $olddir or die;
my $i = 0;
for my $dirent (readdir $dh) {
    next if $dirent =~ /^\.\.?$/;
    my $fh;
    if ($dirent =~ /\.gz$/) {
        open $fh, "-|", "zcat", "$olddir/$dirent" or die;
    } else {
        open $fh, "<", "$olddir/$dirent" or die;
    }
    my $distv;
    while (<$fh>) {
        next unless /CPAN Testers Reports: Report for (\S+?)</;
        $distv = $1;
        last;
    }
    next unless $distv;
    close $fh;
    my $todir = sprintf "$Opt{todir}/%s/%s", substr($distv,0,1), $distv;
    mkpath $todir;
    rename "$olddir/$dirent", "$todir/$dirent";
    print ".";
    $i++;
    print "($i)" unless $i % 1000;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
