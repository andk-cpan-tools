#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

 prereqcheck.pl perlglob module need

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Run what CPAN::Reporter runs when it checks the prerequisites with
loading one after the other in a separate process. At least the last
time I looked which was circa at CPAN::Reporter version 1.2004. After
that, Slaven discovered problematic cases which led to new releases.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

my($perlglob,$mod,$v) = @ARGV;
unless (defined $v) {
    pod2usage(1);
}
my @perls = glob $perlglob;
for my $perl (@perls) {
    my $PC = `$perl -MCPAN::Reporter::PrereqCheck -le 'print \$INC{"CPAN/Reporter/PrereqCheck.pm"}'`;
    chomp $PC;
    # echo DB_File 0 | $yourperl $PC
    warn "Doing the equivalent of:
  echo $mod $v | $perl $PC
";
    open my $fh, "|-", $perl, $PC or die "could not fork: $!";
    print $fh "$mod $v\n";
    close $fh or die "FAILED: $perl...: $!";
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
