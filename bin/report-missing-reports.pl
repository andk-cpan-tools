#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

This script is intended to be run once a day. It is a wrapper around
refill-cpanstatsdb-minutes.pl which finds missing reports in the
cpanstatsdb.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

use File::Basename ();
use File::Path ();
use DateTime ();

my($root) = "$ENV{HOME}/var/metabase-log/missings";
my($refillroot) = "$ENV{HOME}/var/refill-cpanstatsdb";
my($prg) = "$ENV{HOME}/src/andk-cpan-tools/bin/refill-cpanstatsdb-minutes.pl";

my $dt = DateTime->new( time_zone => 'UTC', year => 2012, month => 10, day => 8);
my $dtmax = DateTime->now( time_zone => 'UTC' );
$dtmax->subtract(days => 3);
while () {
    my $path = sprintf "%s/%s/%s.rpt", $root, $dt->year, $dt->ymd;
    if (! -e $path){
        File::Path::mkpath File::Basename::dirname $path;
        my $pickdate = $dt->ymd("");
        my $refillpath = sprintf "%s/%s/%s", $refillroot, $dt->year, $dt->month;
        my $command = sprintf "%s %s --pickdate=%s --collectguids %s > %s 2> %s.err", $^X, $prg, $pickdate, $refillpath, $path, $path;
        warn "RUNNING $command\n";
        0 == system $command or die;
        for ("$path.err"){
            unlink $_ unless -s $_;
        }
    }
    $dt->add( days => 1 );
    last if $dt > $dtmax;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
