#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION



=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

use XML::LibXML;
my $xml = do { open my $fh, 'mwiese141117.osm' or die; local $/; <$fh> };
my $dom = XML::LibXML->load_xml(string => $xml);
print <<EOIntro;
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="" version="1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
 <trk>
  <name>ACTIVE LOG221114</name>
EOIntro
my(%N);
for my $e ($dom->findnodes('/osm/node')) {
    my $id = $e->getAttribute('id');
    $N{$id}{lat} = $e->getAttribute('lat');
    $N{$id}{lon} = $e->getAttribute('lon');
}
for my $e ($dom->findnodes('/osm/way')) {
    my $w = $e->getAttribute('id');
    print "<trkseg>\n";
    for my $e2 ($e->findnodes('nd')) {
        my $ref = $e2->getAttribute('ref');
        printf qq{<trkpt lat="%s" lon="%s"/>\n}, $N{$ref}{lat}, $N{$ref}{lon};
    }
    print "</trkseg>\n";
}
print "</trk></gpx>\n";
# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
