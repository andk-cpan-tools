#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

We send all sand processes a SIGSTOP when one of our precious files
gets lost in the hope that the frozen jobs give us a clue about who
was doing what when the files got lost.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

use POSIX qw(strftime);

our @PRECIOUS = qw(
    /home/sand/.zshrc
    /home/sand/.zhistory
    /home/sand/.cpanreporter/config.ini
    /home/sand/.cpanreporter/metabase_id.json
);

PEACEFUL: while () {
    for my $f (@PRECIOUS) {
        unless (-e $f) {
            my $localtime = localtime;
            warn "ALERT $localtime: missing file '$f', going to stop all sand/perl programs";
            last PEACEFUL;
        }
    }
    sleep 2;
}

open my $fh, "-|", ps => "auxww" or die;
while (<$fh>) {
    my($user,$pid,undef,undef,undef,undef,undef,undef,undef,undef,$cmd) = split " ", $_, 11;
    next unless $user eq "sand";
    next if $pid == $$;
    next unless $cmd =~ /perl/;
    open my $fh2, "<", "/proc/$pid/status" or next;
    while (<$fh2>) {
        # VmData:     7528 kB
        next unless /^VmData:(?:\s+)(\d+) kB/;
        printf "%s %5d %7d: %s\n", strftime("%FT%T",localtime), $pid, $1, $cmd;
    }
    warn "Stopping now $pid\n";
    kill STOP => $pid or die "Could not stop: $!";;
}



# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
