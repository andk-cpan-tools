#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION



=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

use strict;
use YAML::XS;
use Time::Piece;
my @files = glob "~sand/.cpan/plugins/CPAN::Plugin::TraceDeps/*.log";
warn sprintf "%d files\n", scalar @files;
for my $file (@files){
    my $outfile = $file;
    $outfile =~ s/\.log$/.yaml/;
    if (-e $outfile){
        if (-M $outfile < -M $file){
            # warn "Skipping $file due $outfile being fresher\n";
            next;
        }
    }
    open my $fh, $file or die "could not open $file: $!";
    my $ts = localtime;
    warn "$ts: opened $file\n";
    my %S;
    while (<$fh>) {
        my $t = substr($_,0,19);
        next unless $t =~ /\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/;
        my($epoch) = Time::Piece->strptime("${t} GMT", "%Y-%Om-%Od %OH:%OM:%OS %Z")->[9];
        $S{T0} ||= $epoch;
        if (!defined $S{hostname} and /"hostname":"([^"]+)"/) {
            $S{hostname} = $1;
        }
        if (!defined $S{uniq_path_snippet} and defined $S{hostname} and m{(\Q$S{hostname}\E/.+?/.+?)/}) {
            $S{uniq_path_snippet} = $1;
        }
        $S{G}{$t}{T} = $epoch - $S{T0};
        $S{N}++;
        $S{G}{$t}{n}++;
        $S{G}{$t}{N} = $S{N};
        $S{nmax} ||= 0;
        $S{nmax} = $S{G}{$t}{n} if $S{G}{$t}{n} > $S{nmax};
    }
    close $fh;
    open $fh, ">", $outfile or die "Could not open > $outfile: $!";
    print {$fh} YAML::XS::Dump \%S;
    close $fh or die "Could not close $outfile: $!";
    $ts = localtime;
    warn "$ts: wrote $outfile\n";
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
