#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--max=i>

Defaults to 0, which stands for unlimited. Otherwise the number of
records to transfer.

=back

=head1 DESCRIPTION

Written AFTER quickaccess/distcontext. This time only for the
memoize/distlookup table.

As we have said before: Just the quickaccess db from sqlite to
postgres. Designed to be USED ONLY ONCE. It drops the table before
starting work, so if you run it twice, it's OK, but should only be
done if the first round was broke or so.

Performance? For 64888 records it took 608 seconds.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

our $SIGNAL = 0;
$SIG{INT} = $SIG{TERM} = sub { my $sig = shift; warn "Caught $sig\n"; $SIGNAL=1 };

use DBI;
use Time::HiRes qw(sleep time);
use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;
my($workdir,$cpan_home,$ext_src);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
    $cpan_home = $CPAN::Blame::Config::Cnntp::Config->{cpan_home};
    $ext_src = $CPAN::Blame::Config::Cnntp::Config->{ext_src};
}

my $dsn = "dbi:SQLite:dbname=$workdir/memoize.db"; # no option any more

sub my_do_query {
    my($dbi,$sql,@args) = @_;
    my $success = $dbi->do($sql,undef,@args);
    unless ($success) {
        my $err = $dbi->errstr;
        warn sprintf
            (
             "Warning: error occurred while executing sql[%s]with args[%s]: %s",
             $sql,
             join(":",map { defined $_ ? "'$_'" : "<undef>"} @args),
             $err,
            );
    }
    return $success;
}

my $sldbi = DBI->connect ($dsn); # return a dbi handle
my $slsth0 = $sldbi->prepare("SELECT count(*) from distlookup");
$slsth0->execute();
my($slcnt) = $slsth0->fetchrow_array();
warn "Found $slcnt records in the sqlite table";
my $slsql = "SELECT distv,author,upload_date,distroid from distlookup order by distv";
if ($Opt{max}) {
    $slsql .= " limit $Opt{max}";
    $slcnt = $Opt{max};
}
my $slsth = $sldbi->prepare($slsql);
$slsth->execute();

my $pgdbi = DBI->connect ("dbi:Pg:dbname=analysis");
my $pgsql = "DROP TABLE distlookup";
my_do_query($pgdbi, $pgsql);
$pgsql = "CREATE TABLE distlookup (
distv text primary key,
author text,
upload_date text,
distroid text)";
my_do_query($pgdbi, $pgsql);
my $pgsth2 = $pgdbi->prepare("INSERT INTO distlookup
 (distv,author,upload_date,distroid) values
 (?,     ?,     ?,       ?)");

my $i = 0;
my $lastreport = my $starttime = time;
$|=1;
print "\n";
ROW: while (my(@row) = $slsth->fetchrow_array) {
    $row[2] ||= 0;
    unless ($pgsth2->execute(@row)) {
        warn sprintf "ALERT: error inserting row/id[%s]: %s\n", $row[0], $sldbi->errstr;
        last ROW;
    }
    ++$i;
    my $eta = int((time - $^T)*($slcnt - $i)/$i);
    printf "\r%d/%d  remaining seconds: %d    ", $i, $slcnt, $eta;
    last ROW if $SIGNAL;
}
print "\n";
my $tooktime = time - $starttime;
warn "records transferred[$i] tooktime[$tooktime]\n";

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
