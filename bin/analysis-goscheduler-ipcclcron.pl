#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--dry-run|n!>

Tells us what it would do and exits.

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Starts the `main` cnntp-solver shellscript. Concurrency is managed by IPC::ConcurrencyLimit.

=cut


use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
BEGIN {
    push @INC, qw(       );
}
use CPAN::Blame::Config::Cnntp;

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use Time::Moment;

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

my($workdir);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
}
use IPC::ConcurrencyLimit;

my($basename) = File::Basename::basename(__FILE__);
my $limit = IPC::ConcurrencyLimit->new
    (
     max_procs => 1,
     path      => "$workdir/IPC-ConcurrencyLimit-$basename",
    );
my $limitid = $limit->get_lock;
if (not $limitid) {
    warn "Another process appears to be still running. Exiting.";
    exit(0);
}

my $logfile = __FILE__ . ".log";
sub appendlog {
    my($what) = @_;
    $what =~ s/\s*\z//;
    my $tm = Time::Moment->now_utc;
    open my $fh, ">>", $logfile or die "Could not open '$logfile': $!";
    print $fh "$tm $what\n";
    close $fh or die "Could not close '$logfile': $!";
}

appendlog("Starting as $$");
my $wd = "$FindBin::Bin/..";
chdir $wd or die "Could not chdir to $wd: $!";
$ENV{GOPATH} = "/home/andreas/gocode";
0==system qq{"/usr/bin/go" "run" "CPAN-Blame/golang/analysis-scheduler.go" >> $logfile 2>&1} or die "problem running scheduler (go), giving up";
appendlog("Finished as $$");

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
