#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--sleep=f>

Sleep that much between two annotations.

=item B<--showprogress!>

Reports progress.

=back

=head1 DESCRIPTION

Make sure all annotations are in the distcontext table. Intended as a
cronjob that runs only 4 times per day or so.

=head1 HISTORY

2015/2016 slaven sends so many annotations and wants to see them
sooner than the default.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use Time::HiRes qw(sleep);
use YAML::XS;

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{sleep} //= 0.03;

my($workdir);
use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
}
use IPC::ConcurrencyLimit;

my($basename) = File::Basename::basename(__FILE__);
my $limit = IPC::ConcurrencyLimit->new
    (
     max_procs => 1,
     path      => "$workdir/IPC-ConcurrencyLimit-$basename",
    );
my $limitid = $limit->get_lock;
if (not $limitid) {
    warn "Another process appears to be still running. Exiting.";
    exit(0);
}

sub mypgdbi () {
    require DBI;
    my $dbi = DBI->connect ("dbi:Pg:dbname=analysis");
}
my $sth = mypgdbi()->prepare("select yaml from distcontext where distv=?");
my $sth2 = mypgdbi()->prepare("update distcontext set yaml=? where distv=? and yaml=?");
setpriority(0, 0, 5); # renice
my $annofile = "$FindBin::Bin/../annotate.txt";
my $fh;
unless (open $fh, $annofile) {
    # $DB::single=1;
    die "Could not";
}
local $/ = "\n";
my $anno;

ANNOLINE: while (<$fh>) {
    chomp;
    next ANNOLINE if /^\s*$/;
    my($dist,$splain) = split " ", $_, 2;
    $anno->{$dist} = $splain;
}
close $fh;
my $cntdown = keys %$anno;
$| = 1;
my $updated = 0;
for my $distv (keys %$anno) {
    $sth->execute($distv);
    my($ystr) = $sth->fetchrow_array;
    unless ($ystr) {
        warn "Record n'existe: $distv\n";
    }
    if (my $y = eval { YAML::XS::Load($ystr) }) {
        if (!$y->{annotation} || $y->{annotation} ne $anno->{$distv}) {
            $y->{annotation} = $anno->{$distv};
            $sth2->execute(YAML::XS::Dump($y), $distv, $ystr);
            warn sprintf "%s: %s\n", scalar localtime, $distv;
            $updated++;
        }
    }
    --$cntdown;
    if ($Opt{showprogress}) {
        unless ($cntdown % 10) {
            printf "\r%8d %-60s", $cntdown, $distv;
        }
        unless ($cntdown) {
            print "\r";
            last;
        }
    }
    sleep $Opt{sleep};
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
