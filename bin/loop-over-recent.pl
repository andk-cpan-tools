#!/usr/bin/perl -- -*- Mode: cperl; coding: utf-8; cperl-indent-level: 4 -*-

# /home/src/perl/repoperls/installed-perls/perl/p5B38YI/perl-5.10.0@35088/bin/perl /home/k/sources/CPAN/ghub/cpanpm/bin/loop-over-recent.pl

=head1 NAME

loop-over-recent.pl - 

=head1 SYNOPSIS

 loop-over-recent.pl [OPTIONS]

=head1 OPTIONS

=over 8

=cut


my $optpod = <<'=back';

=item B<--action=s>

Defaults to C<install>. Other supported values are

  test
  force install
  notest install

=item B<--bisect!>

Takes one argument of the form C<v5.15.7-109-g035b682,v5.15.5>.
Incompatible with C<--perlglob>.

NOT IMPLEMENTED. Needs more work to find out whether a child had
success with its task.

=item B<--config_allow_installing_module_downgrades=s>

Defaults to 'no'. By setting it to anything else, we set
$CPAN::Config->{allow_installing_module_downgrades} to that value. Note that this string will
be passed to a subprocess, so will be subject to an evaluation there.

=item B<--config_allow_installing_outdated_dists=s>

Defaults to 'no'. By setting it to anything else, we set
$CPAN::Config->{allow_installing_outdated_dists} to that value. Note that this string will
be passed to a subprocess, so will be subject to an evaluation there.

=item B<--config_build_cache=s>

Defaults to 0. By setting it to anything!=0, we set
$CPAN::Config->{build_cache} to that value. Note that this string will
be passed to a subprocess, so will be subject to an evaluation there.

=item B<--config_build_dir=s>

Defaults to empty string. By setting it to anything, we set
$CPAN::Config->{build_dir} to that value. Note that this string will
be passed to a subprocess, so will be subject to an evaluation there.

=item B<--config_build_dir_reuse=s>

Defaults to 0. By setting it to anything!=0, we set
$CPAN::Config->{build_dir_reuse} to that value. Note that this string
will be passed to a subprocess, so will be subject to an evaluation
there.

=item B<--config_build_requires_install_policy=s>

Defaults to C<yes>. By setting it to anything, we set
$CPAN::Config->{build_requires_install_policy} to that value. Note
that this string will be passed to a subprocess, so will be subject to
an evaluation there.

=item B<--config_check_sigs=s>

Defaults to 1. By setting it to anything, we set
$CPAN::Config->{check_sigs} to that value. Note
that this string will be passed to a subprocess, so will be subject to
an evaluation there.

=item B<--config_ftpstats_size=s>

Defaults to C<0>. By setting it to anything, we set
$CPAN::Config->{ftpstats_size} to that value. Note
that this string will be passed to a subprocess, so will be subject to
an evaluation there.

=item B<--config_prefer_installer=s>

Defaults to empty string. By setting it to anything, we set
$CPAN::Config->{prefer_installer} to that value. Note
that this string will be passed to a subprocess, so will be subject to
an evaluation there.

=item B<--config_prefs_dir=s>

Defaults to empty string. By setting it to anything, we set
$CPAN::Config->{prefs_dir} to that value. Note
that this string will be passed to a subprocess, so will be subject to
an evaluation there.

=item B<--config_recommends_policy=s>

Defaults to C<1>. By setting it to anything, we set
$CPAN::Config->{recommends_policy} to that value. Note that this
string will be passed to a subprocess, so will be subject to an
evaluation there.

=item B<--config_test_report=s>

Defaults to C<1>. By setting it to anything, we set
$CPAN::Config->{test_report} to that value. Note that this string will
be passed to a subprocess, so will be subject to an evaluation there.

=item B<--config_use_sqlite=s>

Defaults to C<0>. By setting it to anything, we set
$CPAN::Config->{use_sqlite} to that value. Note that this string will
be passed to a subprocess, so will be subject to an evaluation there.

=item B<--debug!>

More verbose.

=item B<--distro=s@>

Completely different beast than the original program design: instead
of looping over recent we only run these CPAN shell object identifiers
against all perls in this config and stop after that. If combined with
--historical-excursions-arraysize, the latter is ignored.

Bonus feature: when the string contains dash(es) and no slash, then
dashes are turned into double colons, turning eg. C<Text-PO-Parser>
into C<Text::PO::Parser>.

Another bonus feature: string is split on comma and whitespace and
then converted to multiple --distro arguments.

Third bonus feature: if an argument looks like an http URL, it is
shortened to a distro argument.

=item B<--dotcpanhome=s>

defaults to C< $ENV{HOME}/.cpan >. The place passed to every
smoker-perl where it should pick the MyConfig from.

=item B<--help|h!>

This help

=item B<--historical-excursions-arraysize=i>

Defaults to 0, so no historical excursions are being done. If set,
whenever all work is done, we pick some random distro from the N most
recent distros to keep the CPU busy.

=item B<--hostname=s>

when determining the config file, use this hostname.

=item B<--inc=s@>

Add these paths with -I when starting the cpan shell.

=item B<--initial-backlog=i>

That many recentfiles we are trying to process iff this is a proper
loop run (no --distro arg). Defaults to 4096. Note: this is not the
number of distros, just the number of events in recentfiles, including
CHECKSUMS files and .meta and what have you;

=item B<--limits!>

Boolean, defaults to true. We apply some arbitrary limits to protect
our smokers. Set --nolimits to test without these limits. RTFS and
search for setrlimit to find out which arbitrary limits we are setting
at ther moment;

=item B<--log!>

Turn logging on. Defaults to off.

=item B<--makeperl!>

Boolean. Build new perls on demand. When we encounter a perl path that
does not exist, we try to get a hold of that corresponding directory
with a lock for competing processes and then call this program with
the usual parameters. When that other program finishes and we have no
perl, we log this in the corresponding directory and give up. The
logfile will then prevent further attempts.

If this option is used together with the --perlglob option, the result
depends on the results of the glob which may in turn depend on perls
already present, so may become confusing to debug. But it's OK to use
when the glob returns one or more not-existing perls, so we usually
bootstrap with one perl we want to have for all the following
operations.

=item B<--max=i>

Perform not more loops than this many.

=item B<--monitoring=s>

Defaults to the string C<default>, which means the parent process
printd a line like this every second:

    ==========monitoring proc 28683 perl /home/src/perl/repoperls/installed-perls/perl/v5.16.3/a2da secs 6.0000=======

Other possible values:

  silent       parent prints nothing

=item B<--mydistrobundles!>

Shortcut for several frequently used --distro=... options. Hardcoded
in the program. RTFS.

=item B<--parallel=i>

Defaults to 1 which means we never have more than one job running.
Specifies how many children we spawn when there is enough work to do.

=item B<--perlglob=s@>

Instead of using the perls from the configuration, use those specified
in the glob expression. Repeatable. Incompatible with C<--bisect>.

=item B<--perlglobexprandpick=i>

Only takes effect when perlglob is also set. The specified number of perls
will be used for the rest of the run, if possible, in random order.
Otherwise all perls in the glob expressions will be used.

=item B<--randomskip=f>

Value between 0 and 1. Defaults to 0 which means no test will be
skipped. A value of 0.25 will skip on average every fourth activity, a
value of 1 would skip all activities.

=item B<--reduce-perls-on-excursions!>

Boolean. Defaults to false. If true, we will only test with less perls
than we have parallel processes. The idea here is that hanging distros
do not let the whole machine go stale.

=item B<--runinperldb!>

Start the cpan shell with -d. In other words: use the perl debugger.

=item B<--skip_sanity_check!>

Normally we allocate big files every now and then to verify we have
disk space available.

=item B<--sleep=f>

Sleep that many seconds after every build.

=item B<--statefile=s>

File holding a timestamp of the most recent distro we have processed.

=item B<--test!>

do not install, only test. Equivalent to

  --action=test

=item B<--timeout=i>

Defaults to 45 minutes. Time allowed for one whole process of
installing/testing/running one job with one perl. When timeout
is reached the child process is killed and reaped.

=item B<--tmpdir=s>

In the two places where we use a temporary file or directory, use
this directory as base.

=item B<--tracedeps!>

Turn on the CPAN::Plugin::TraceDeps

=item B<--tracememory!>

Turn on the CPAN::Plugin::TraceMemory

=item B<--transient_build_dir!>

If true, a new tmp-directory-based build_dir is provided for every
cpan run. Intended to make concurrent invocations easy.

=item B<--use-02-packages!>

If true, we write a bundle file that contains all packages from
02packages file, shuffled, and that bundle gets added to the distros to
proceed.

=item B<--use-02-packages-trimmed=i>

If true, we write a bundle file that contains that number of packages
from 02packages file, shuffled, and that bundle gets added to the
distros to proceed.

=back

=head1 DESCRIPTION

Picks the 4096 (--initial-backlog) most recent uploads, filters out
outdated stuff, and installs the not outdated parts into a (sample of)
perl(s). The order goes from oldest to newest distro. After every
upload it writes a status kind of timestamp into something like
~/.cpan/loop-over-recent.state file (grep for $statefile). The status
item is the epoch according to the RECENT files. This ensures that a
new run of this program will not retry more than one distro from the
last run.

=head1 AUTHOR

%AUTHOR%

=cut

use strict;
use warnings;
our $HAVE_SHUFFLE = eval { require Algorithm::Numerical::Shuffle };
use BSD::Resource;
use CPAN::DistnameInfo;
use Cwd;
use Fcntl qw( :flock :seek O_RDONLY O_RDWR O_CREAT );
use File::Basename qw(fileparse dirname);
use File::Path qw(mkpath rmtree);
use File::Spec;
use File::Temp;
use Getopt::Long;
Getopt::Long::Configure("no_auto_abbrev");
use Pod::Usage qw(pod2usage);
use POSIX ":sys_wait_h";
use Sys::Hostname qw(hostname);
use Time::HiRes qw(sleep);
use Sys::Syslog;
our $HAVE_DD = eval { require Data::Dump };

{
    # inspired by/stolen from Slaven
    package CPAN::Shell::sleepless;
    # sub myprint { return; }
    # sub mywarn { return; }
    # sub mydie { my($self,$why) = @_; warn "Caught error[$why]; continuing"; return; }
    sub mysleep { return; }
}

# indirect dependencies:
use Sort::Versions;

if (-e "/home/k/sources/rersyncrecent/lib/") {
  require lib;
  lib->import("/home/k/sources/rersyncrecent/lib/");
  lib->unimport(".");
}
our $HAVE_RRR = eval { require File::Rsync::Mirror::Recent };

our $Signal = 0;
my @opt = $optpod =~ /B<--(\S+)>/g;
our %Opt;
GetOptions
    (
     \%Opt,
     @opt,
    ) or pod2usage(1);

pod2usage(0) if $Opt{help};
pod2usage(1) if @ARGV;

if ($Opt{"historical-excursions-arraysize"} && $Opt{distro}) {
  warn "WARNING: Found option --distro and option --historical-excursions-arraysize; ignoring the latter";
  $Opt{"historical-excursions-arraysize"}=0;
}
if ($Opt{"historical-excursions-arraysize"} && !$HAVE_SHUFFLE) {
  warn "WARNING: will fail soonish because Shuffle missing, better install Algorithm::Numerical::Shuffle now";
  sleep 2;
}
$Opt{limits} //= 1;
$Opt{tmpdir} ||= "/tmp";
$Opt{timeout} ||= 45*60; # PDL
$Opt{dotcpanhome} ||= "$ENV{HOME}/.cpan";
$Opt{parallel} ||= 1;
$Opt{action} ||= "install";
$Opt{monitoring} ||= "default";
$Opt{config_test_report} //= "1";
$Opt{config_use_sqlite} //= "0";
$Opt{config_allow_installing_module_downgrades} //= "no";
$Opt{config_allow_installing_outdated_dists} //= "no";
$Opt{config_build_cache} //= "0";
$Opt{config_build_dir} //= "";
$Opt{config_build_dir_reuse} //= 0;
$Opt{config_build_requires_install_policy} //= "yes";
$Opt{config_check_sigs} //= 1;
$Opt{config_ftpstats_size} //= 0;
$Opt{config_prefer_installer} //= "";
$Opt{config_prefs_dir} //= "";
$Opt{config_recommends_policy} //= 1;
if (defined $Opt{randomskip} && ($Opt{randomskip} < 0 || $Opt{randomskip} > 1)) {
  die "option --randomskip must be between 0 and 1 (inclusive); is $Opt{randomskip}\n";
}

{
  my $ts;
  sub my_timestamp () {
    return $ts if $ts;
    require Time::Piece;
    my $t = Time::Piece::localtime();$t->date_separator("");$t->time_separator("");
    $ts = $t->datetime;
  }
}
if ($Opt{log}) {
  require Log::ger;
  Log::ger->import;
  require Log::ger::Util;
  Log::ger::Util::set_level("debug");
  require Log::ger::Output;
  my $ts = my_timestamp;
  mkpath "$ENV{HOME}/var/log";
  Log::ger::Output->set(File => (path => "$ENV{HOME}/var/log/loop-over-$ts.log", lazy => 1));
  require Log::ger::Layout;
  Log::ger::Layout->import(Pattern => (format => '%d %H %P %p %c %L%% %m'));
  log_debug("Starting with opts: %s", \%Opt);
} else {
  *log_debug = sub { };
}
if (my $d = $Opt{distro}) {
  my @d2;
  for my $d0 (@$d) {
    # comma operations
    push @d2, split /[,\s]+/, $d0;
  }
  $d = $Opt{distro} = \@d2;
  for (@$d) {
    if (/-/ and not m|/|) {
      s/-/::/g;
    } else {
      s|^https?://.+?/authors/id/[A-Z]/[A-Z]{2}/||;
    }
  }
} elsif (! $HAVE_RRR) {
  die "this perl ($^X) has no File::Rsync::Mirror::Recent available, so we must have a --distro option specified";
}
if ($Opt{perlglob} && $Opt{bisect}) {
  die "The options perlglob and bisect cannot be used together";
}
if ($Opt{mydistrobundles}) {
  $Opt{distro} ||= [];
  my $date = "2011_10_21";
  my @snaps = glob "$Opt{dotcpanhome}/Bundle/Snapshot_${date}_*.pm";
  for my $s (@snaps) {
    my($n) = $s =~ /Snapshot_${date}_(\d+)/;
    push @{$Opt{distro}}, "Bundle::Snapshot_${date}_$n";
  }
}

sub whine_for_shuffle () {
  while (! $HAVE_SHUFFLE){
    warn "Note: this perl ($^X) has no Algorithm::Numerical::Shuffle, sleeping 60, then retry\n";
    sleep 60;
    last if $Signal;
    $HAVE_SHUFFLE = eval { require Algorithm::Numerical::Shuffle };
  }
}

if ($Opt{"use-02-packages"} || $Opt{"use-02-packages-trimmed"}) {
  whine_for_shuffle;
  my $dotcpan = $Opt{dotcpanhome};
  warn "DEBUG: dotcpan[$dotcpan] \$\$[$$]\n";
  my $prefer_200000 = 0;
  my(@p);
  if ($prefer_200000) { # the big numbe of package
    require Parse::CPAN::Packages::Fast;
    $Opt{distro} ||= [];
    my $pf = Parse::CPAN::Packages::Fast->new('/home/ftp/pub/PAUSE/modules/02packages.details.txt.gz');
    @p = grep { !/^Bundle::/ } $pf->packages;
  } else {
    # stolen from generate-resterampe
    open my $fh, "/home/ftp/pub/PAUSE/modules/02packages.details.txt" or die;
    my $line = 0;
    my $i = 0;
    my %SEEN2;
    while (<$fh>) {
      $line++;
      next if $line==1 .. /^$/;
      my @F = split " ", $_;
      my $pkg = $F[0];
      next if $pkg =~ /^Bundle::/; # we may want to have this as a Getopt
      my $dist = $F[2];
      next if $SEEN2{$dist}++;
      $i++;
      # printf "%6d %s\n", $i, $dist;
      push @p, $F[0];
    }
  }
  Algorithm::Numerical::Shuffle::shuffle(\@p);
  if (my $i = $Opt{"use-02-packages-trimmed"}) {
    pop @p while @p > $i;
  }
  mkpath "$dotcpan/Bundle";
  my $ts = my_timestamp;
  open my $fh, ">", "$dotcpan/Bundle/Packages02_$ts.pm" or die "Could not write: $!";
  print $fh qq{package Bundle::Packages02_$ts;\n1;\n\n=head1 NAME

Bundle::Packages02_$ts - shuffled 02packages file\n\n=head1 CONTENTS\n\n};
  for my $p (@p) {
    print $fh "$p\n";
  }
  print $fh qq{\n\n=head1 CONFIGURATION\n\n};
  close $fh or die "Could not close: $!";
  push @{$Opt{inc}}, $dotcpan;
  push @{$Opt{distro}}, "Bundle::Packages02_$ts";
}
log_debug("Opts after initializing: %s", \%Opt);
$SIG{INT} = sub {
  my $sig = shift;
  warn "Caught signal $sig, about to incr \$Signal to $Signal+1\n";
  $Signal++;
};

sub determine_perls {
  my($perls_config_file) = @_;
  my @perls;
  return unless $perls_config_file;
  return unless -e $perls_config_file;
  my $hostname = $Opt{hostname} || hostname();
  my $path_slice_for_perl = $hostname eq "k83" ? "perl" : "host/$hostname";
  if (open my $fh2, $perls_config_file) {
    while (<$fh2>) {
      chomp;
      s/#.*//; # remove comments
      next if /^\s*$/; # remove empty/white lines
      unless (m|/.+/|) {
        s|^|/home/sand/src/perl/repoperls/installed-perls/$path_slice_for_perl/|;
        s|$|/bin/perl|;
      }
      next if ! -x $_ && ! $Opt{makeperl};
      push @perls, $_;
    }
  }
  unless (@perls) {
    @perls = $^X;
  }
  \@perls;
}

sub single_child_parental_control {
  my($pid,$system,$perl,$upload) = @_;
  my $start = my $sleep_to = Time::HiRes::time;
 SUPERVISE: while (waitpid($pid, WNOHANG) <= 0) {
    $sleep_to++;
    my $this_sleep = $sleep_to - Time::HiRes::time;
    $this_sleep = 0.1 if $this_sleep < 0.1; # maybe to enable ^C at all (?)
    sleep($this_sleep);
    if ($Signal) {
      log_debug("Killing %s with -15", $pid);
      kill -15, $pid;
      for my $c (qw(k i l l e d)) {
        warn "_ $c _ " x 19;
        sleep 0.06;
      }
      if ($Signal>=2) {
        return;
      }
      $Signal = 0;
      sleep 0.5; # give them a chance to ^C immediately again
      my $ret = waitpid($pid, WNOHANG);
      if ($ret && $?) {
        warn "Warning: process $ret returned \$\?=$?.

Command was '@$system'
";
        sleep 6;
      }
      return;
    }
    my $have_waited = $sleep_to-$start;
    if ($Opt{monitoring} eq "default") {
      warn sprintf "==========monitoring proc %d perl %s secs %.4f=======\n", $pid, $perl, $have_waited;
    } elsif ($Opt{monitoring} eq "silent") {
      # do nothing
    }
    if ($have_waited >= $Opt{timeout}) {
      log_debug("Killing with -15 %s", { pid =>$pid, upload => $upload });
      kill -15, $pid;
      for my $c (qw(k i l l e d)) {
        warn "_ $c _ " x 19;
        sleep 0.05;
      }
      if (kill(0, $pid)) {
        log_debug("Killing with -9 %s", { pid =>$pid, upload => $upload });
        kill -9, $pid;
      }
      my $sleep = 2;
      warn "ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN\n";
      warn "      Something went wrong during\n";
      warn "      $perl\n";
      warn "      $upload->{path}\n";
      warn "      have_waited='$have_waited' Opt{timeout}=$Opt{timeout}'\n";
      warn "      (sleeping $sleep)\n";
      warn "ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN-ATTN\n";
      sleep $sleep;
    }
  }
  my $ipcs_mem_segments = `ipcs -m|grep sand|wc -l`;
  chop $ipcs_mem_segments;
  log_debug("End of job %s",
      {
       pid    => $pid,
       upload => $upload,
       ipcsm  => $ipcs_mem_segments,
      });
  if ($Opt{sleep}) {
    warn "Sleeping $Opt{sleep} seconds now\n";
    sleep $Opt{sleep};
  }
}

sub some_forks {
  my($chldr,$action,$distro,$perl,$upload,$combo,$cmbsn) = @_;
 FORK: {
    my $pid = fork;
    if (! defined $pid) { # contention
      warn "Contention '$!', sleeping 2";
      sleep 2;
      redo FORK;
    } elsif ($pid) { # parent
      push @$chldr, $pid;
      while (@$chldr >= $Opt{parallel}){
        for (my $i = $#$chldr; $i>=0; $i--){
          if (waitpid($chldr->[$i], WNOHANG) > 0){
            openlog "smoke", "pid", "local0";
            syslog "info", "reaped proc|->$chldr->[$i]";
            closelog;
            splice @$chldr, $i, 1;
          }
        }
        sleep 0.01; # wait until some job finishes, we are at maximum
      }
    } else { # child
      one_fork($action,$distro,$perl,$upload,$combo);
      exit;
    }
  } # FORK
  if ($Signal>=2) {
    return;
  }
  $cmbsn->{$perl,$upload->{path}} = $upload->{epoch};
}

sub one_fork {
  my($action,$distro,$perl,$upload,$combo) = @_;
  my $ipcs_mem_segments = `ipcs -m|grep sand|wc -l`;
  chop $ipcs_mem_segments;
  log_debug("Entering one_fork %s",
      {
       action   => $action,
       distro   => $distro,
       perl     => $perl,
       upload   => $upload,
       ipcsm    => $ipcs_mem_segments,
      });
  my $optinc = $Opt{inc} || [];
  my $perlswitches = $Opt{runinperldb} ? "-de" : "-e";
  my @system = (
                $perl,
                # "-Ilib",
                "-I$Opt{dotcpanhome}",
                "-M-lib='.'",
                (map { "-I$_" } @$optinc),
                "-MCPAN::MyConfig",
                "-MCPAN",
                $perlswitches,
               );
  my $bdir; # build directory
  my $func;
  {
    my @actions = split " ", $action;
    push @actions, $distro;
    my $f = shift @actions;
    $func = sprintf "%s(%s)", $f, join(",",map {"q{$_}"} @actions);
  }
  my $tracedeps = "";
  if ($Opt{tracedeps}) {
    $tracedeps = "CPAN::Shell->o(qw(conf plugin_list push CPAN::Plugin::TraceDeps));";
  }
  my $tracememory = "";
  if ($Opt{tracememory}) {
    $tracememory = "CPAN::Shell->o(qw(conf plugin_list push CPAN::Plugin::TraceMemory));";
  }
  my $commonshellcmd = qq{
{ package CPAN::Shell::sleepless; use base qw(CPAN::Shell); sub mysleep { return } }
\$CPAN::Frontend = "CPAN::Shell::sleepless";
\$SIG{XCPU} = sub { warn sprintf "%s: Caught inner SIGXCPU after %d seconds running\n", scalar localtime, time-\$^T };
\$CPAN::Config->{test_report}="$Opt{config_test_report}";
\$CPAN::Config->{use_sqlite}="$Opt{config_use_sqlite}";
\$CPAN::Config->{build_cache}="$Opt{config_build_cache}" if $Opt{config_build_cache};
\$CPAN::Config->{build_dir}="$Opt{config_build_dir}" if "$Opt{config_build_dir}";
\$CPAN::Config->{build_dir_reuse}="$Opt{config_build_dir_reuse}" if $Opt{config_build_dir_reuse};
\$CPAN::Config->{build_requires_install_policy}="$Opt{config_build_requires_install_policy}";
\$CPAN::Config->{check_sigs}="$Opt{config_check_sigs}";
\$CPAN::Config->{ftpstats_size}="$Opt{config_ftpstats_size}";
\$CPAN::Config->{prefer_installer}="$Opt{config_prefer_installer}" if length "$Opt{config_prefer_installer}";
\$CPAN::Config->{prefs_dir}="$Opt{config_prefs_dir}" if "$Opt{config_prefs_dir}";
\$CPAN::Config->{recommends_policy}="$Opt{config_recommends_policy}";
\$CPAN::Config->{allow_installing_module_downgrades}="$Opt{config_allow_installing_module_downgrades}";
\$CPAN::Config->{allow_installing_outdated_dists}="$Opt{config_allow_installing_outdated_dists}";
\$CPAN::Suppress_readline=1; CPAN::Shell->o(qw(conf)); $tracedeps $tracememory $func};

  if ($Opt{transient_build_dir}) {
    $bdir = File::Temp::tempdir(
                                "loop_over_bdir-$$-XXXXXX",
                                DIR => $Opt{tmpdir},
                                CLEANUP => 0, # we clean up ourselves
                                              # much earlier than
                                              # *this* program exit
                               ) or die $!;
    warn "DEBUG: bdir[$bdir] \$\$[$$]\n";
    push @system, "\$CPAN::Config->{build_dir}=q{$bdir}; $commonshellcmd",
  } else {
    push @system, "\$CPAN::Config->{build_dir_reuse}=0 unless defined \$CPAN::Config->{build_dir_reuse}; $commonshellcmd",
  }
  # 0==system @system or die;
 ONEFORK: while () {
    my $pid = fork;
    if (! defined $pid) { # contention
      warn "Contention '$!', sleeping 2";
      sleep 2;
    } elsif ($pid) { # parent
      my $ipcs_mem_segments = `ipcs -m|grep sand|wc -l`;
      chop $ipcs_mem_segments;
      log_debug("Forked %s", {
          distro => $distro,
          child  => $pid,
          parent => $$,
          ipcsm  => $ipcs_mem_segments,
      });
      single_child_parental_control($pid,\@system,$perl,$upload);
      if ($bdir) {
        warn sprintf "%s: About to rmtree '%s'", scalar localtime, $bdir;
        rmtree $bdir;
      }
      if ($Signal>=2) {
        return;
      }
      last ONEFORK;
    } else { # child
      openlog "smoke", "pid", "local0";
      syslog "info", "$combo\nproc|->$$";
      closelog;
      POSIX::setsid();
      my ($soft, $hard) = BSD::Resource::getrlimit(+BSD::Resource::RLIMIT_CPU);
      unless ($soft > 0) {
        if ($Opt{limits}) {
          BSD::Resource::setrlimit(+BSD::Resource::RLIMIT_CPU, 0.9*3600, 1*3600);
        }
      }
      exec @system or sleep 1; # give them a chance to ^C immediately again
    }
  }
}

sub makeperl {
  my($mperl) = @_;
  # '-Dprefix=/home/src/perl/repoperls/installed-perls/perl/v5.19.1/165a -Dmyhostname=k83 -Dinstallusrbinperl=n -Uversiononly -Dusedevel -des -Ui_db -DDEBUGGING=-g';
  # (have removed:) -Uuseithreads -Uuselongdouble
  # perl ~/src/andk/andk-cpan-tools/bin/makeperl.pl -j=4 --ud=UD --report --module=Moose
  my($perlroot) = $mperl =~ m|(.+)/bin/perl$|;
  unless (-d $perlroot) {
    File::Path::make_path($perlroot, { verbose => 1, mode => 0755 });
  }
  my($lockfile) = File::Spec->catfile($perlroot,"LOGCK");
  my $lfh;
  unless (open $lfh, "+<", $lockfile) {
    unless ( open $lfh, ">>", $lockfile ) {
      die "ALERT: Could not open >> '$lockfile': $!"; # XXX should not die
    }
    unless ( open $lfh, "+<", $lockfile ) {
      die "ALERT: Could not open +< '$lockfile': $!"; # XXX should not die
    }
  }
  if (flock $lfh, LOCK_EX|LOCK_NB) {
    warn "Info[$$]: Got the lock, continuing";
  } else {
    warn "FATAL[$$]: lockfile '$lockfile' locked by a different process; skipping this perl";
    return;
  }
  my(%map) =
      (
       "127e" => ["--ud=UD"],
       "165a" => ["--ud=UU"],
       "a2da" => ["--ud=DD"],
       "9980" => ["--ud=DU"],
       "8378" => ["--ud=UD", "--nodebugging"],
       "9ab7" => ["--ud=UU", "--nodebugging"],
       "7e7a" => ["--ud=DD", "--nodebugging"],
       "8005" => ["--ud=DU", "--nodebugging"],
       # with 2016 libswanted:
       "8942" => ["--ud=UD"],
       "79cc" => ["--ud=UU"],
       "109d" => ["--ud=DD"],
       "f7bf" => ["--ud=DU"],
       "8d81" => ["--ud=UD", "--nodebugging"],
       "f991" => ["--ud=UU", "--nodebugging"],
       "de40" => ["--ud=DD", "--nodebugging"],
       "13e0" => ["--ud=DU", "--nodebugging"],
       # k93msid bis xxxx
       "f11c" => ["--ud=DU"],
       "89ad" => ["--ud=UU"],
       "5ea4" => ["--ud=DD"],
       "2d7c" => ["--ud=UD"],
       "ea6b" => ["--ud=UD", "--debuggingoption=EBUGGING=none"],
       "d2d1" => ["--ud=UU", "--debuggingoption=EBUGGING=both"],
       "a1a1" => ["--ud=DD", "--debuggingoption=EBUGGING=both"],
       "276a" => ["--ud=DU", "--debuggingoption=EBUGGING=both"],
       "0a29" => ["--ud=UD", "--debuggingoption=EBUGGING=both"],
       # k93msid ab xxxx
       "1e0c" => ["--ud=DU"],
       "da1c" => ["--ud=UU"], # da1c produces 6fdb for 5.8.x where x in (3,6,9)
       "6fdb" => ["--ud=UU"],
       "8854" => ["--ud=DD"],
       "af11" => ["--ud=UD"],
       "8b31" => ["--ud=UD"],
       "f901" => ["--ud=DU"],
       "3889" => ["--ud=DD"],
       #??? "" => ["--ud=UD", "--debuggingoption=EBUGGING=none"],
       "bb7f" => ["--ud=UU", "--debuggingoption=EBUGGING=both"],
       "ac75" => ["--ud=DD", "--debuggingoption=EBUGGING=both"],
       "29fb" => ["--ud=DU", "--debuggingoption=EBUGGING=both"],
       "fc43" => ["--ud=UD", "--debuggingoption=EBUGGING=both"],
       "4da5" => ["--ud=UU", "--debuggingoption=EBUGGING=both", "--addopts=Dusemultiplicity"],
       "1d53" => ["--ud=UD", "--debuggingoption=EBUGGING=both", "--addopts=Dusemultiplicity"],
       "ec94" => ["--ud=UU", "--debuggingoption=EBUGGING=both", "--addopts=Dusequadmath"],
       "db45" => ["--ud=DU", "--debuggingoption=EBUGGING=both", "--addopts=Dusequadmath"],
       # note 2015-03-08: Today I succeeded for 5.8.9 with just the current hints/linux.sh
       # and that's why we now have a perl-5.8.9/165a
       # 2015-03-08 07:12  git checkout perl-5.8.9
       # 2015-03-08 07:31  git fetch
       # 2015-03-08 07:32  git show FETCH_HEAD:hints/linux.sh >| hints/linux.sh
       # 2015-03-08 07:32  perl /home/sand/src/andk/andk-cpan-tools/bin/makeperl.pl --j=6 --ud=rand --report --module=PAUSE::Packages
       "5da8" => ["--ud=UU",
                  "-j=1",
                  "--addopts=Dusedl",
                  "--addopts=Ddlsrc=dl_dlopen.xs",
                  "--addopts=Dlibs=-lnsl -ldb -ldl -lm -lcrypt -lutil -lc",
                  "--addopts=Dlibpth=/usr/lib/x86_64-linux-gnu /usr/local/lib /lib /usr/lib",
                 ],
      );
  my($hash,$pdir) = fileparse($perlroot); # ("127e","/home/sand/src/....")
  $pdir =~ s|/$||;
  my($tag) = fileparse($pdir);
  my $opts = $map{$hash};
  unless ($opts) {
     print $lfh "Could not determine arguments for hash[$hash] mperl[$mperl]\n";
     return;
  }
  my $bindir = dirname(__FILE__);
  my @system2 =
      (
       $^X,
       "$bindir/makeperl.pl",
       "--j=4",
       @$opts,
       "--report",
       "--notest", # TODO: FIXME: should only turn off testing when necessary
      );
  warn "prepare a clone to run system[@system2]";
  my $clonedir = File::Temp::tempdir
      (
       "loop_over_clonedir-$$-XXXXXX",
       DIR => $Opt{tmpdir},
       CLEANUP => 1, # we clean up
       # ourselves much earlier than *this* program exit
      );
  unless ($clonedir) {
     print $lfh "Could not create a clonedir: $!\n";
     return;
  }
  # cd /tmp && git clone /home/sand/src/perl/repoperls/perl5.git.perl.org/perl --no-single-branch perl-clone-$$ && cd perl-clone-$$/
  my $cwd = Cwd::cwd();
  chdir $clonedir or die "Could not chdir to $clonedir: $!"; # XXX should not die
  warn "Info: now in $clonedir";
  0==system("git", "clone", "/home/sand/src/perl/repoperls/perl5.git.perl.org/perl", "--no-single-branch", ".") # XXX hardcoded sand
      or die "problem building a clone"; # XXX should not die
  warn "Info: now have checked out perl";
  0==system("git", "checkout", $tag) or die "problem checking out tag '$tag'";
  warn "Info: checked out $tag";
  0==system @system2
      or die sprintf
      (
       "Alert: %s: problem building perl %s/%s with '%s'",
       scalar localtime,
       $tag,
       $hash,
       join(" ", @system2),
      );
  chdir $cwd or die "could not chdir back to '$cwd': $!"; # XXX should not die
  File::Path::remove_tree($clonedir);
}

sub read_recent_events {
  my($rf,$rx,$max) = @_;
  $max ||= $Opt{"initial-backlog"} ||= 4096;
  my $recent_events = $rf->news(max => $max);
  $recent_events = [ grep { $_->{path} =~ $rx and $_->{type} eq "new" } @$recent_events ];
  {
    my %seen;
    $recent_events = [ grep {
      my $path = $_->{path};
      my $d = CPAN::DistnameInfo->new($path);
      my $dist = $d->dist;
      # warn "no dist for path[$path]" unless $dist;
      $dist ? !$seen{$dist}++ : "";
    } @$recent_events ];
  }
  $recent_events;
}

sub iterate_over_perls {
  my($perls, $upload, $cmbsn, $action, $chldr) = @_;
  my @perlset = @$perls;
 PERL: while (@perlset) {
    my $perl = shift @perlset;
    last PERL if $Signal;
    unless (-e $perl) {
      # cautious code location. Once we have this robust, we would
      # prefer to move it into the forked process; but the
      # downside is that we do not know whether this process will
      # provide this perl;
      if ($Opt{"makeperl"}) {
        makeperl($perl);
      }
    }
    next PERL unless -e $perl;
    my $perl_version =
        do { open my $fh, "$perl -e \"print \$]\" |" or die "Couldn't open $perl: $!";
             <$fh>;
           };
    unless ($perl_version) {
      warn "Alert: could not determine perl version of '$perl', skipping";
      sleep 0.33;
      next PERL;
    }
    my $skip_this = 0;
    my $additional_skip_probability = 0;
    if ($upload->{path} =~ m{/JGNI/}) {
      $additional_skip_probability = 0.75;
    }
    if ($Opt{randomskip} && rand() <= $Opt{randomskip}) {
      $skip_this = 1;
    } elsif ($additional_skip_probability) {
      $skip_this = rand() <= $additional_skip_probability;
    }
    if ($skip_this) {
      require Term::ANSIColor;
      warn Term::ANSIColor::colored (["blue on_magenta"], "skipping due to randomskip=$Opt{randomskip} and additional_skip_probability=$additional_skip_probability\n");
      sleep 0.33;
      next PERL;
    }
    my $testtime = localtime;
    my($upload_epoch,$epoch_as_localtime);
    if ($upload->{epoch}){
      $upload_epoch = $upload->{epoch};
      $epoch_as_localtime = scalar localtime $upload->{epoch};
    } else {
      $epoch_as_localtime = $upload_epoch = "N/A";
    }
    my $combo = "perl|-> $perl (=$perl_version)\npath|-> $upload->{path}\n".
        "recv|-> $epoch_as_localtime (=$upload_epoch)\ntime|-> $testtime";
    if (0) {
    } elsif ($cmbsn->{$perl,$upload->{path}}){
      warn "dead horses combo $combo";
      sleep 2;
      next PERL;
    } else {
      warn "\n\n$combo\n\n\n";
      $ENV{PERL_MM_USE_DEFAULT} = 1;
      $ENV{AUTOMATED_TESTING} = 1 unless defined $ENV{AUTOMATED_TESTING} && length $ENV{AUTOMATED_TESTING};
      $ENV{PERL_CANARY_STABILITY_NOPROMPT}=1;
      # How do I make sure this DISPLAY is running?
      # while true; do date ; if ! ps auxww | grep -v grep | grep -q Xvfb ; then Xvfb :121 & fi; echo -n 'sleeping 60 '; sleep 60; done
      # alternatives: vncserver, Xnest, etc.
      $ENV{DISPLAY} = ":121";
      my $distro = $upload->{path};
      $distro =~ s|^id/||;
      if ($Opt{parallel} <= 1) {
        one_fork($action,$distro,$perl,$upload,$combo);
      } else {
        some_forks($chldr,$action,$distro,$perl,$upload,$combo,$cmbsn);
      } # if/else parallel
      if ($Signal>=2) {
        return;
      }
    } # if/else cmbsn
  } # PERL
}

my $recentfile = "/home/ftp/pub/PAUSE/authors/RECENT.recent";
MAIN : {
  # local is for reading the recentfiles, localroot is for reading the
  # files. one should go away.
  my $rf;
  $rf = File::Rsync::Mirror::Recent->new
      (
       localroot => "/home/ftp/pub/PAUSE/authors/",
       local => $recentfile,
      ) if $HAVE_RRR;
  my $hostname = $Opt{hostname} || hostname();
  if ($hostname =~ s/\..*//) {
    warn "Warning: hostname contained a dot, shortening to '$hostname'";
  }
  my $perls_config_file;
  if ($hostname eq "k75") {
    $perls_config_file = "$0.otherperls";
  } elsif ($Opt{perlglob} || $Opt{bisect}) {
    # deciding below
  } else {
    $perls_config_file = "$0.otherperls.$hostname";
    if (-f $perls_config_file) {
      warn "Using perls config '$perls_config_file'";
    } else {
      die "Could not find '$perls_config_file'";
    }
  }
  my $statefile;
  if ($Opt{statefile}) {
    $statefile = $Opt{statefile};
  } else {
    my $bbname = fileparse($0,qr{\.pl});
    $statefile = "$ENV{HOME}/.cpan/$bbname.state";
  }
  my $rx = qr!\.(tar.gz|tar.bz2|zip|tgz|tbz)$!;
  my $max_epoch_worked_on = 0;
  if (-e $statefile) {
    local $/;
    my $state = do { open my $fh, $statefile or die "Couldn't open '$statefile': $!";
                     <$fh>;
                   };
    chomp $state;
    $state ||= 0;
    $state += 0;
    $max_epoch_worked_on = $state if $state;
  }
  warn "max_epoch_worked_on[$max_epoch_worked_on] statefile[$statefile]";
  if ($Opt{limits}) {
    BSD::Resource::setrlimit(BSD::Resource::RLIMIT_CORE(),  40*1024*1024, 45*1024*1024);
  }
  if (@{$Opt{distro}||[]}) {
    # whenever we do not run in an endless loop, we want to limit
    # globally; trying to be generous because we saw so many runs exit
    # without an indication what went wrong
    if ($Opt{limits}) {
      BSD::Resource::setrlimit(+BSD::Resource::RLIMIT_CPU,   $Opt{timeout}, $Opt{timeout});
    }
    # I do not recall that I ever saw this signal handler acting
    #### $SIG{XCPU} = sub { warn sprintf "%s: Caught SIGXCPU after %d seconds running\n", scalar localtime, time-$^T };
  }
  # 2012-07-12: we had several oom-killer experiences that locked the
  # klatt machine up. oom killed the wrong processes first. The right
  # one would have been a perl with 3387649 kB:

  #### BSD::Resource::setrlimit(BSD::Resource::RLIMIT_RSS(),   3_000_000_000, 4_000_000_000);

  # 2012-07-29: the above limit to RSS did not prevent the OOM but the
  # following AS did:

  if ($Opt{limits}) {
    BSD::Resource::setrlimit(BSD::Resource::RLIMIT_AS(), 16*1024*1024*1024, 16*1024*1024*1024);
  }

  # file size to X
  # BSD::Resource::setrlimit(BSD::Resource::RLIMIT_FSIZE(), 4_000_000_000, 4_000_000_000);

  # 2017-12-22 saw a fork bomb in action, but NPROC is not the right
  # answer, at least no on this scale. The smoker produced many cannot
  # fork reports these days.

  # BSD::Resource::setrlimit(BSD::Resource::RLIMIT_NPROC(), 256, 512);

  my $cmbsn = {}; # was: %comboseen
  my $count_uploaditem = 0;
  my $chldr = []; # was: @children
 ITERATION: while () {
    last if $Signal;
    my $optdstrs = $Opt{distro}||[]; # was: @distro
    my $iteration_start = time;
    my $recent_events;
    my $historical_excursion = 0;
    if (@$optdstrs) {
      $recent_events = [map {+{path => $_}} @$optdstrs];
    } else {
      $recent_events = read_recent_events($rf,$rx);
      # $DB::single=1;
      if ($Opt{"historical-excursions-arraysize"}) {
        if ($max_epoch_worked_on >= $recent_events->[0]{epoch}) {
          $historical_excursion = 1;
          $recent_events = read_recent_events($rf,$rx,$Opt{"historical-excursions-arraysize"});
          whine_for_shuffle;
          Algorithm::Numerical::Shuffle::shuffle($recent_events);
        } else {
          warn sprintf "DEBUG: \$max_epoch_worked_on '%s' >= \$recent_events->[0] '%s'\n", $max_epoch_worked_on, $HAVE_DD ? Data::Dump::pp($recent_events->[0]) : $recent_events->[0];
          @$recent_events = reverse @$recent_events;
        }
      } else {
        @$recent_events = reverse @$recent_events;
      }
    }
    last if $Signal;
    my $perls;
    if ($Opt{perlglob}) {
      my @globs = @{$Opt{perlglob}};
      my @allperls;
      for my $glob (@globs) {
        my @perls = glob $glob;
        push @allperls, @perls;
      }
      my %seen;
      for my $i (reverse 0..$#allperls) {
        splice @allperls, $i, 1 if $seen{$allperls[$i]}++;
      }
      if (my $i = $Opt{perlglobexprandpick}) {
        if ($i < @allperls) {
          my @randperls;
          while (@randperls < $i) {
            push @randperls, splice @allperls, int(rand(scalar @allperls)), 1;
          }
          @allperls = @randperls;
        }
      }
      $perls = \@allperls;
    }
 UPLOADITEM: for my $upload (@$recent_events) {
      last if $Signal;
      unless (@$optdstrs) {
        next  UPLOADITEM unless $upload->{path} =~ $rx;
        next  UPLOADITEM unless $upload->{type} eq "new";
        next  UPLOADITEM if $upload->{path} =~ m|/perl-5\.[1-9]\d|;
        next  UPLOADITEM if $upload->{path} =~ m|A/AN/ANDK/.*CPAN-Test-Dummy-Perl5-|;
        if ($historical_excursion) {
          if ($cmbsn->{"ALL",$upload->{path}}) {
            if ($upload->{epoch} > $max_epoch_worked_on) {
              # we never know whether what was the mtime of the
              # latest is still the same one; we know, that pause may
              # touch a file (for unknown reasons)
              $max_epoch_worked_on = $upload->{epoch};
            }
            next UPLOADITEM;
          }
        } elsif ($upload->{epoch} < $max_epoch_worked_on) {
          warn sprintf "Already done: %s %.1f\n", substr($upload->{path},8), $upload->{epoch} unless keys %$cmbsn;
          sleep 0.03;
          next UPLOADITEM;
        } elsif ($upload->{epoch} == $max_epoch_worked_on) {
          if ($cmbsn->{"ALL",$upload->{path}}) {
            next UPLOADITEM;
          }
          warn "Maybe already worked on, we'll retry them: $upload->{path}";
        }
        if ($historical_excursion) {
          for (qw(h i s t o r i c a l)) {
            print "_$_" x 39;
            print "\n";
            sleep 0.1;
          }
        } else {
          {
            open my $fh, ">", $statefile or die "Could not open >$statefile\: $!";
            print $fh $upload->{epoch}, "\n";
            close $fh or die "Could not write: $!\nTry\n  echo '$upload->{epoch}' > '$statefile'\n ";
          }
          $max_epoch_worked_on = $upload->{epoch};
        }
      }
      if ($Opt{max} && ++$count_uploaditem > $Opt{max}){
        log_debug("Reached last loop %d", $count_uploaditem);
        last ITERATION;
      }
      sanity_check();
      $perls ||= determine_perls($perls_config_file) || [];
      my $action = $Opt{action};
      if ($upload->{path} =~ m{^D/DA/DAGOLDEN/CPAN-Reporter-\d+\.\d+_
                               /CPAN-Distribution-\d
                              }x){
        $action = "test";
      } elsif ($Opt{test}) {
        $action = "test";
      } elsif ($historical_excursion) {
        if ($action ne "test") {
          require CPAN;
          my $do = eval { CPAN::Shell->expand('Distribution', $upload->{path}) };
          if (!$do) {
            warn "Warning: '$upload->{path}' not in the index, will not install";
            $action = "test";
          }
        }
        if ($Opt{"reduce-perls-on-excursions"}) {
          while (@$perls > $Opt{parallel} + 1) {
            my $splice = rand scalar @$perls;
            splice @$perls, $splice, 1;
          }
        }
      }

      # XXX: we should compute exceptions for every distro that has a
      # higher numbered developer release. Say Foo-1.4801 is released
      # but we have already 1.48_51 installed. We do not want this
      # stable stuff. Test yes, so we should 'make test' instead of
      # 'make install'. The problem with this is that we do not know
      # what exactly is in the distro. So we must go through
      # CPAN::DistnameInfo somehow. It gets even more complicated when
      # the item here gets passed to a queuerunner because then the
      # decision if test or install shall be called cannot be made now,
      # it must be made when the job is actually started.

      # Update 2017-07-04:
      # https://github.com/eserte/srezic-misc/blob/master/scripts/cpan_recent_uploads2#L225
      # how slaven decides to limit operations on indexed distros (we
      # talked about the gotcha with downgrading XML::LibXML::Common)
      # Update 2017-09-06: commit
      # 6d12ff99b9b4a60494f7ab1bd959e1811cc3030b introduced this
      # algorithm

      if (@$optdstrs) {
        # need no sanity check on path or anything
      } else {
        my $abs = File::Spec->catfile($rf->localroot, $upload->{path});
        {
          local $| = 1;
          my $max = 1200;
          my $slept = 0;
          while (! -f $abs) {
            print ",";
            $slept += sleep 0.5;
            if ($slept > $max) {
              warn "Giving up waiting for '$abs', maybe already deleted?";
              next UPLOADITEM;
            }
          }
        }
      }
      iterate_over_perls($perls, $upload, $cmbsn, $action, $chldr);
      if ($Signal>=2) {
        last ITERATION;
      }
      $cmbsn->{"ALL",$upload->{path}} = $upload->{epoch};
      if (@$optdstrs) {
        next UPLOADITEM; # nothing can change that would influence us
      } elsif ($historical_excursion) {
        sleep 1;
        next ITERATION;
      } else {
        next ITERATION; # see what is new before simply going through the ordered list
      }
    } # UPLOADITEM
    if (@$optdstrs) {
      last ITERATION; # nothing left to do
    }
    my $minimum_time_per_loop = 15;
    while (time - $iteration_start < $minimum_time_per_loop) {
      my @stat = stat($recentfile);
      last if $stat[9] > $iteration_start;
      last if $Signal;
      sleep 1;
    }
    for my $k (keys %$cmbsn) {
      delete $cmbsn->{$k} if $cmbsn->{$k} < time - 60*60*24*2;
    }
    { local $| = 1; print "."; } # painting dots
  } # ITERATION

  log_debug("End of Iteration. Left-over children %d", scalar @$chldr);
  while (@$chldr){
    for (my $i = $#$chldr; $i; $i--){
      if (waitpid($chldr->[$i], WNOHANG) > 0){
        openlog "smoke", "pid", "local0";
        syslog "info", "reaped proc|->$chldr->[$i]";
        closelog;
        splice @$chldr, $i, 1;
      }
    }
    if ($Signal) {
      warn "giving up on signal";
      last;
    }
    warn "will have to wait for children[@$chldr]";
    while (my $c = pop @$chldr){
      if (waitpid($c, 0) > 0){
        warn "Finished child: $c";
        openlog "smoke", "pid", "local0";
        syslog "info", "reaped proc|->$c";
        closelog;
      }
    }
  }
  log_debug("all children reaped");
  print "all children reaped\n";

}

sub sanity_check {
  # cleanup => 1 only active at program exit, not at scope exit!
  return if $Opt{skip_sanity_check};
 DFTEST: while () {
    my $cannotcontinuenow = 0;
    for my $dir ($Opt{tmpdir},
                 ".",
                 "/var/tmp", # these bloody testers write everywhere,
                             # sometimes in /var/lib/ for databases,
                             # this is a proxy for this case
                ) {
      my $tmpdir = eval { File::Temp::tempdir(
                                       "loop-over-recent-XXXXXX",
                                       DIR => $dir,
                                      ) } or die "Could not create tempdir in '$dir': $!";
      my $ttt = "$tmpdir/testfreespace";
      open my $fh, ">", $ttt or die "Could not open > '$ttt': $!";
      for (1..100) {
        # 32 bytes times 2**15 equals 1 MB
        print $fh "f r e e e e s p a c e <%-1/8-\> " x 2**15;
        print $fh "\n";
      }
      my $success = 0;
      $success = close $fh;
      rmtree $tmpdir;
      unless ($success){
        warn "Couldn't close '$ttt': $!";
        $cannotcontinuenow = 1;
      }
    }
    if ($cannotcontinuenow) {
      warn "$$: n o   f r e e s p a c e\n" for 1..8;
      last if $Signal;
      sleep(30 + rand(180));
    } else {
      last DFTEST;
    }
  }
  open my $fh, "-|", "ipcs -m" or die "Could not fork ipcs -m: $!";
  my $shared_mem_segments = 0;
  while (<$fh>) {
    next unless /sand/;
    $shared_mem_segments++;
  }
  if ($shared_mem_segments > 4000) {
    die sprintf "Too many shared memory segments (%d) for further testing, try something like\n  %s\n", $shared_mem_segments, q{ipcs -m|grep sand|head -1004|awk '{print $2}'|xargs -n 1 ipcrm -m};
  } elsif ($shared_mem_segments > 1000) {
    warn sprintf "Info: (%s) allocated shared mem segments now: %d\n", $0, $shared_mem_segments;
  }
}

__END__

# Local Variables:
# mode: cperl
# cperl-indent-level: 2
# End:
