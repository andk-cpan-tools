#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

 localctgetreports.pl [OPTIONS] vdistro

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--dry-run|n!>

Only show the resulting command

=item B<--help|h!>

This help

=item B<--hostname=s>

Defaults to current host.

=item B<--solvetop=i>

Pass through as such to ctgetreports

=back

=head1 DESCRIPTION

Warning: local path conventions hard-coded

Warning II: and very ad-hoc, e.g. will not work on k93

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{solvetop} //= 9;
use Sys::Hostname qw(hostname);
my $hostname = hostname();
$Opt{hostname} //= $hostname;

my $vdistro = shift @ARGV or pod2usage(1);
my @perls0 = glob(qq{/home/sand/src/perl/repoperls/installed-perls/host/$Opt{hostname}/v5.*/*/bin/perl});
my @perls = grep {
    if (-x $_) {
        if (-x s/perl$/ctgetreports/r) {
            1;
        } else {
            0;
        }
    } else {
        0;
    }
} @perls0;

die "FIXME: Found no perl with a working ctgetreports (tried @perls0)" unless @perls;

use CPAN::DistnameInfo;
my $d = CPAN::DistnameInfo->new("A/AN/ANON/$vdistro.tar.gz");
my $dist = $d->dist;
my $first_letter = substr($dist,0,1);
my $glob = "/home/sand/var/ctr/done/archive/$first_letter/$dist/*$vdistro*";
my @glob = glob($glob);

die "No results matching glob('$glob')" unless @glob;
my $ctgr = my $perl = pop @perls; # newer one preferred
$ctgr =~ s/perl$/ctgetreports/;

my $filtercb = q{--filtercb='my $r = shift; for (split " ", $r->{"conf:ccflags"}){$r->{"conf:ccflags~$_"} = 1}'};
if ($Opt{'dry-run'}) {
    warn "system $perl $ctgr --solve --solvetop=$Opt{solvetop} $filtercb --reportfiles $glob\n";
} else {
    0 == system $perl, $ctgr, "--solve", "--solvetop=$Opt{solvetop}", $filtercb, "--reportfiles", @glob or die;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
