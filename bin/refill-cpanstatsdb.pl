#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

  ~/src/installed-perls/v5.34.0/c310/bin/perl bin/refill-cpanstatsdb.pl

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--finishlimit=i>

A query that yields a result with less rows than this number is the
signal to refrain from further refill queries and finish this program.
Defaults to 0 which means other limits are needed to stop this
program.

Note: before we invented the --sleeplimit and --sleeptime parameters,
this was the way how we stopped the program. Probably not needed
anymore.

=item B<--generate-redis-legalset!>

Boolean. Defaults to false. If true, writes something into Redis (RTFS).

=item B<--maxins=i>

No default, which means no limit. Maximum number of records to inject.
If set to zero, we test the surroundings, then exit.

=item B<--maxtime=i>

Maximum time in seconds this program should run. Defaults to 1770. If
set to zero, no limit.

=item B<--queryid=i>

Normally the database is asked for its max(id) and then the first
query is about one more than that with an open end. If --queryid is
specified, then we query that and only that and then finish the
program.

=item B<--sleeplimit=i>

A query that yields a result with less rows than this number is the
signal to sleep for $Opt{sleeptime} seconds before querying again.
Defaults to 500. Do not set it too low, it would produce an annoying
amount of logfiles.

=item B<--sleeptime=i>

For how long to sleep in the case of $Opt{sleeplimit} undercut.
Defaults to 150 seconds.

=item B<--throttletime=i>

Defaults to 30. If > 0, we wait at the end of each batch if we were
faster than this parameter (in secs).

=back

=head1 DESCRIPTION

Replacement for the job that downloaded the whole cpanstats.db and
gunzipped it.

Now we simply repeatedly fetch the descriptions for the next 2500
reports until the supply dries out. Thus we reach a new max, write all
the stuff to the db and let the other jobs work from there.

=head1 TODO

remove unneeded data, maybe split them out.

=head1 SEE ALSO

refill-cpanstatsdb-minutes.pl

CPAN::Testers::WWW::Reports::Query::Reports

=cut


use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;

use Dumpvalue;
use File::Basename ();
use File::Path ();
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use List::Util qw(min);
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use IPC::ConcurrencyLimit;
our $HAVE_REDIS;

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{finishlimit} ||= 0;
$Opt{sleeplimit} ||= 500;
$Opt{sleeptime} ||= 150;
$Opt{maxtime} = 1770 unless defined $Opt{maxtime};
$Opt{throttletime} //= 30;

if ($Opt{"generate-redis-legalset"}) {
    $HAVE_REDIS = eval { require Redis; 1 };
    if ($HAVE_REDIS){
        Redis->import();
    } else {
        die "you do not have Redis installed, but we need it to support generate-redis-legalset";
    }
}

my($workdir);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
}

my($basename) = File::Basename::basename(__FILE__);
my $limit = IPC::ConcurrencyLimit->new
    (
     max_procs => 1,
     path      => "$workdir/IPC-ConcurrencyLimit-$basename",
    );
my $limitid = $limit->get_lock;
if (not $limitid) {
    warn "Another process appears to be still running. Exiting.";
    exit(0);
}

use DBI;
use Time::HiRes qw(time);
use JSON::XS ();
use List::Util qw(max);
use CPAN::Testers::WWW::Reports::Query::Reports;

our $jsonxs = JSON::XS->new->indent(0);
our $redis;
if ( $HAVE_REDIS ) {
    $redis = Redis->new(reconnect => 120, every => 1000);
}

my($pgdbh,$pgsth,$pgmaxid,$nextid);
my %Partitions;
$pgdbh = DBI->connect("dbi:Pg:dbname=analysis") or die "Could not connect to 'analysis': $DBI::err";
if ($Opt{queryid}) {
    $pgmaxid = 0;
    $nextid = $Opt{queryid};
} else {
    my @tables = map { /^public\.cpanstats_(\d{6})$/ ? $1 : () } $pgdbh->tables("","public");
    %Partitions = map { $_ => 1 } @tables;
    my $Max = 0;
    for my $pd (sort @tables){
        my $sth = $pgdbh->prepare("select max(id) from cpanstats_$pd where postdate=?");
        $sth->execute($pd);
        my $max = $sth->fetchrow_array;
        $Max = $max if $max > $Max;
    }
    warn "INFO: In Pg found max id '$Max'";
    $pgmaxid = $Max;
    $nextid = $Max+1;
}
{
    my $sql = "INSERT INTO cpanstats
 (id,guid,state,postdate,tester,dist,version,platform,perl,osname,osvers,fulldate,type) values
 (?, ?,   ?,    ?,       ?,     ?,   ?,      ?,       ?,   ?,     ?,     ?,       ?)";
    $pgsth = $pgdbh->prepare($sql);
}
# oldestpostdate, copied from genoldestpostdate.pl:
my($sth_ins,$sth_exists);
{
    my $sql = "INSERT INTO oldestpostdate
 (dist,version,postdate) VALUES
 (?,   ?,      ?)";
    $sth_ins = $pgdbh->prepare($sql);
}
{
    my $sql = "SELECT COUNT(*) FROM oldestpostdate WHERE dist=? AND version=?";
    $sth_exists = $pgdbh->prepare($sql);
}
my $query = CPAN::Testers::WWW::Reports::Query::Reports->new;
my($inscount) = 0;
my($pg_n,$pg_time) = (0,0);
QUERY: while () {
    my $range = $Opt{queryid} ? $nextid : "$nextid-";
    my $batchstart = time;
    warn sprintf "%s: Next query range '%s'\n", scalar gmtime(), $range;
    my $result = $query->range($range);
    my $RRL = ref $result->{list}; # Ref of ResultList
    my $querycnt = $RRL eq "HASH" ? keys %{$result->{list}} : @{$result->{list}};
    my $thismax = $querycnt > 0 ? ($RRL eq "HASH" ? (max(keys %{$result->{list}})) : $result->{list}[-1]{id}) : undef;
    warn sprintf "%s: Got %d records from '%s' to '%s'\n", scalar gmtime(), $querycnt, $nextid, $thismax||"<UNDEF>";
    if (defined($Opt{maxins}) && $Opt{maxins} <= 0) {
        last QUERY;
    }
    unless ($thismax){
        if ($Opt{maxtime} && time+$Opt{sleeptime}-$^T >= $Opt{maxtime}) {
            last QUERY;
        } else {
            sleep $Opt{sleeptime};
            next QUERY;
        }
    }

    # so we have some work to do
    my @gmtime = gmtime;
    my $logfile = sprintf
        (
         "%s/var/refill-cpanstatsdb/%04d/%02d/%04d%02d%02dT%02d%02d-%d-MAX.json.gz",
         $ENV{HOME},
         1900+$gmtime[5],
         1+$gmtime[4],
         1900+$gmtime[5],
         1+$gmtime[4],
         @gmtime[3,2,1],
         $nextid,
        );
    File::Path::mkpath File::Basename::dirname $logfile;
    if (-e $logfile) {
        die "ALERT: found '$logfile', will not overwrite it";
    }
    open my $fh, "|-", "gzip -9c > $logfile" or die "Could not open gzip to '$logfile': $!";
    binmode($fh, ":utf8");
    my $next_log = time + 60;
    #   dist     => "Attribute-Overload",
    #   fulldate => 201205262229,
    #   guid     => "4454e538-a782-11e1-802a-3db30df65b4f",
    #   id       => 22285792,
    #   osname   => "linux",
    #   osvers   => "2.6.18-1.2798.fc6",
    #   perl     => "5.16.0 RC0",
    #   platform => "i686-linux-thread-multi-64int-ld",
    #   postdate => 201205,
    #   state    => "fail",
    #   tester   => "Khen1950fx\@aol.com",
    #   type     => 2,
    #   version  => "1.100710",
    my $i = 0;
    my $max_seen = 0;
 REC: for my $rec ( $RRL eq "HASH" ? (sort {$a->{id} <=> $b->{id}} values %{$result->{list}}) : @{$result->{list}}) {
        if (defined($Opt{maxins}) && $inscount >= $Opt{maxins}) {
            last REC;
        }
        if ($Opt{maxtime} && time-$^T >= $Opt{maxtime}) {
            last REC;
        }
        $max_seen = $rec->{id} if $rec->{id} > $max_seen;
        my $record = $rec;
        my $id = $record->{id};
        if ($id > $pgmaxid) {
            my $start = time;
            unless ($Partitions{$record->{postdate}}){ # partition anlegen
                $pgdbh->do("CREATE TABLE cpanstats_$record->{postdate} PARTITION OF cpanstats FOR VALUES IN (?)",undef,$record->{postdate})
                    or die sprintf "Error while trying create cpan_%s: %s", $record->{postdate}, $pgdbh->errstr;
                warn "INFO: partition cpanstats_$record->{postdate} created\n";
                $Partitions{$record->{postdate}}++;
            }
            $record->{fulldate} = sprintf "%04d-%02d-%02dT%02d:%02dz", unpack("a4a2a2a2a2", $record->{fulldate});
            $pgsth->execute($id,@{$record}{qw(guid state postdate tester dist version platform perl osname osvers fulldate type)});
            $pg_n++;
            $pg_time += time - $start;
            # oldestpostdate, copied from genoldestpostdate.pl:
            my($dist,$version,$postdate) = @{$record}{qw(dist version postdate)};
            $sth_exists->execute($dist,"");
            my($cnt0) = $sth_exists->fetchrow_array;
            unless ($cnt0) {
                $sth_ins->execute($dist,"",$postdate);
            }
            $sth_exists->execute($dist,$version);
            my($cnt1) = $sth_exists->fetchrow_array;
            unless ($cnt1) {
                $sth_ins->execute($dist,$version,$postdate);
            }
        }
        if ($Opt{"generate-redis-legalset"}) {
            my $distv = "$record->{dist}-$record->{version}";
            $redis->sadd("analysis:distv:legalset",$distv);
            #### hincrby not supported by our ubuntu redis
            #### if ($record->{state} eq "pass") {
            ####     $redis->hincrby("analysis:distv:pass",$distv,1);
            #### } elsif ($record->{state} eq "fail") {
            ####     $redis->hincrby("analysis:distv:fail",$distv,1);
            #### }
        }
        # ddx $record; # see also Data::Dump line
        print $fh $jsonxs->encode($record), "\n";
        $i++;
        if (time >= $next_log) {
            warn sprintf "%s: %d records inserted\n", scalar gmtime(), $i;
            $next_log += 60;
        }
        $inscount++;
    }
    close $fh or die "Could not close gzip to '$logfile': $!";
    my $finallogfile = $logfile;
    unless ($max_seen) {
        $max_seen = $nextid - 1;
    }
    $finallogfile =~ s/MAX/$max_seen/;
    rename $logfile, $finallogfile or die "Could not rename $logfile, $finallogfile: $!";
    if ($Opt{queryid}) {
        last QUERY;
    }
    if ( $Opt{finishlimit} && $querycnt < $Opt{finishlimit}) {
        last QUERY;
    }
    if (defined($Opt{maxins}) && $inscount >= $Opt{maxins}) {
        last QUERY;
    }
    my $sleeptime = 0;
    if ( $Opt{sleeplimit} && $querycnt < $Opt{sleeplimit} ) {
        $sleeptime = $Opt{sleeptime};
    }
    if ($Opt{maxtime} && time+$sleeptime-$^T >= $Opt{maxtime}) {
        last QUERY;
    }
    if ($sleeptime) {
        sleep $sleeptime;
    }
    my $batchtook = time - $batchstart;
    if ($Opt{throttletime} and $batchtook < $Opt{throttletime}) {
        my $throttle_sleep = $Opt{throttletime} - $batchtook;
        sleep $throttle_sleep;
    }
    $nextid = $thismax+1;
}
if ($pg_n) {
    warn sprintf "STATS: pg_time %.3f, recs written %d, pg avg ins time per rec %.5f\n", $pg_time, $pg_n, $pg_time/$pg_n;
}

# for the record: today I added the two:
# CREATE INDEX ixdist ON cpanstats (dist);  # took ca 30 minutes
# CREATE INDEX ixtypestate ON cpanstats (type, state);
# DROP INDEX ixvers;

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
