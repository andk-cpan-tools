#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--committed>

Compare the value for committed instead of statm_size.

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION



=head1 HISTORY

2021-11-26 12:30  perl bin/compare-n-memorytraces-by-statm_size.pl ~sand/.cpan/plugins/CPAN::Plugin::TraceMemory/202111{18,24}*.log

To compare bf47149ab7d554e57012d8880f54712adc40307a (from Nicholas
Clark) and k93bionic/v5.35.4-99-g36149847e2/7ee9



=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
my $statkey = $Opt{committed} ? "committed" : "statm_size";

use List::AllUtils qw(max);
use Time::Piece;
use YAML::XS;
use strict;

my(@files) = @ARGV;
my(@l, @s);
@s = ";;t;Chkpoints 36149847e2;Chkpoints bf47149ab7;memsize 36149847e2;memsize bf47149ab7\n";
for my $file (@files) {
    my($line) = qx(wc -l $file);
    # print $line;
    push @l, { lines => $line =~ /(\d+)/ };
    open my $fh, $file or die "Could not open $file: $!";
    while (<$fh>) {
        my($isotime) = substr($_,0,19);
        my($msize) = /"$statkey":"?(\d+)/;
        my $t0 =  Time::Piece->strptime($isotime,"%Y-%m-%d %T")->epoch;
        $l[-1]{isotime} = $isotime;
        $l[-1]{t0} = $t0;
        $l[-1]{fh} = $fh;
        $l[-1]{read} = 1;
        push @s, sprintf "1;%s;0%s%d;;%d\n", $isotime, ";"x(scalar @l), 1, $msize;
        # printf "1;1;%s;0%s%d\n", $isotime, ";"x(scalar @l), $msize;
        $l[-1]{written} = 1;
        last;
    }
}
my $max = max map { $_->{lines} } @l;
my $parts = 503; # prime
my $lines_per_part = $max/$parts;
# warn "lines_per_part=$lines_per_part";
$lines_per_part = 1 if $lines_per_part < 1;
LOOP: while () {
    # last LOOP;
    my $got_one = 0;
  FILE: for my $i (0..$#l) {
        my $h = $l[$i];
        my $read = $h->{read};
        next FILE if $read >= $h->{lines};
        my $next_fp = $h->{written} * $lines_per_part; # fp=floating point
        my $next = int ($next_fp + 0.5);
        $next = $h->{lines} if $next > $h->{lines}; # read the last line this time
        # warn "next_fp=$next_fp, next=$next for i=$i";
        my $fh = $h->{fh};
        until ($read >= $next-1) {
            <$fh>;
            $read++;
        }
        my $line = <$fh>;
        $read++;
        $h->{read} = $read;
        my($isotime) = substr($line,0,19);
        my($msize) = $line =~ /"$statkey":"?(\d+)/;
        my $t1 =  Time::Piece->strptime($isotime,"%Y-%m-%d %T")->epoch;
        push @s,
            sprintf "%d;%s;%d%s%d;;%d\n", ++$h->{written}, $isotime, $t1 - $h->{t0}, ";"x(1+$i), $read, $msize;
        #    printf "%d;%s;%d%s%d;;%d\n", ++$h->{written}, $isotime, $t1 - $h->{t0}, ";"x(1+$i), $read, $msize;
        $got_one++;
    }
    last if $got_one == 0;
}
# print YAML::XS::Dump \@s;
print @s;


# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
