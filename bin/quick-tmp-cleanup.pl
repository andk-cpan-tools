#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

First attempt believed that one would remove old files and then remove
directories. But we have plenty of old files that must not be deleted:
whenever a cpan shell has just unpacked a tarball, all these files
must be preserved.

Second attempt used find instead of finddepth and pruned directories
that were in use according to fuser. Additionally it required them to
be older than a day. And required files too to be older than a day to
be deleted. BUT: I forgot reloperl which must be kept intact
completely. E.g. /tmp/basesmoker-reloperl-cGFh

=cut

# die "BROKEN, DO NOT USE";

use English qw( -no_match_vars ) ;

die sprintf "ALERT: effective uid %d not allowed\n", $EFFECTIVE_USER_ID if $EFFECTIVE_USER_ID==0;

use Time::HiRes qw(sleep);

use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::ReadBackwards;
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);
use List::Util qw(reduce);
use Pod::Usage qw(pod2usage);
use Proc::ProcessTable;

our %Opt;
lock_keys %Opt, map { /([^=!\|]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

if ($Opt{help}) {
    pod2usage(0);
    exit;
}

use File::Find;
find
    ({
      wanted => sub {
          my($dev, $ino, $mode, $nlink, $uid, $gid, $rdev, $size, $atime, $mtime) = lstat($File::Find::name);
          if (-l _) {
              my($r) = readlink $File::Find::name;
              warn "symlink: $File::Find::name => $r\n";
          } elsif (! defined $mtime) {
              # skip
          } elsif ("/tmp" eq $File::Find::name) {
              # skip
          } elsif ( $File::Find::name =~ /tmp\/basesmoker-reloperl/ ) {
              # skip
              $File::Find::prune = 1;
          } elsif ( $File::Find::name =~ /tmp\/loop_over_bdir/ ) {
              # skip
              $File::Find::prune = 1;
          } elsif ( $File::Find::name =~ /tmp\/perl-clone-\d/ ) {
              # skip
              $File::Find::prune = 1;
          } elsif ( $File::Find::name =~ /tmp\/CPAN-Reporter-/ ) {
              # skip
              $File::Find::prune = 1;
          } elsif (-d _) {
              warn "About to run fuser on directory '$File::Find::name'\n";
              open my $fh, "-|", fuser => $File::Find::name or die;
              my $inuse = 0;
              while (<$fh>) {
                  my($file, $proc) = split " ", $_;
                  if ($proc){
                      $inuse = 1;
                      $File::Find::prune = 1;
                  }
              }
              if ($mtime+86400 < $^T) {
                  warn "keeping fresh directory: $File::Find::name ($^T)";
              } elsif ($inuse){
                  warn "directory in use: $File::Find::name\n";
              } elsif (rmdir($File::Find::name)) {
                  warn "rmdir: $File::Find::name\n";
                  $File::Find::prune = 1;
              } else {
                  warn "no rmdir: $File::Find::name ($!)";
                  return;
              }
          } elsif ($mtime+86400 < $^T) {
              warn "About to run fuser on day old file '$File::Find::name'\n";
              open my $fh, "-|", fuser => $File::Find::name or die;
              my $inuse = 0;
              while (<$fh>) {
                  my($file, $proc) = split " ", $_;
                  $inuse = 1 if $proc;
              }
              if ($inuse){
                  warn "file in use: $File::Find::name\n";
              } elsif (unlink($File::Find::name)) {
                  warn "unlink: $File::Find::name\n";
              } else {
                  warn "no unlink: $File::Find::name ($!)";
              }
          } else {
              my $secs_old = $^T-$mtime;
              # warn "keeping too young file: $File::Find::name ($secs_old seconds old)\n";
              return;
          }
          sleep 0.02;
      },
      no_chdir => 1,
     },
     "/tmp");
