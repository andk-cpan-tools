# see first posting http://use.perl.org/~LaPerla/journal/35252
# (Top rt.cpan.org requestors) (up to ticket 32022)

# second: http://use.perl.org/~LaPerla/journal/38195 (Who wrote all
# the RT tickets?) (up to ticket 42061)

=head1 NAME

query-rt-group-by-requestor - 

=head1 SYNOPSIS



=head1 OPTIONS

=over 2

=cut

my $optpod = <<'=back';

=item B<--fromdate=s>

No default. Try C<2009-01-01>

=item B<--todate=s>

No default. Try <2010-01-01>

=item B<--chunksize=i>

Default 396. We stop bothering rt.cpan.org after that many requests.

=item B<--help|h!>

TBD

=item B<--html!>

TBD

=item B<--inclself!>

defaults to 1, unreliable otherwise

=item B<--password=s>

TBD

=item B<--server=s>

TBD

=item B<--top=i>

TBD

=item B<--upto=i>

TBD

=item B<--username=s>

Defaults to C<ANDK>. Needs to be set to empty string if only
statistics are wanted. This can be done with

  --username ""        # not --username="" !

=item B<--withlastyear!>

TBD

=back

=head1 DESCRIPTION

RT::Client::REST broke at some point in time and I posted a mostly
clueless patch that seemed to work. At least the progam did not die
immediately. The patch was integrated into 0.35 and soon after that
ticket 35146 was opened that reverted a part of my patch. (Sidenote:
The patch is posted reverse)

Now with that patch this program again died quickly with

    HTTP::Message content must be bytes

So I decided to patch REST thusly:

--- /home/k/.cpan/build/RT-Client-REST-0.35-d_Mo_f/lib/RT/Client/REST.pm	2008-04-15 12:24:11.000000000 +0200
+++ /home/src/perl/repoperls/installed-perls/perl/pVNtS9N/perl-5.8.0@32642/lib/site_perl/5.10.0/RT/Client/REST.pm	2008-05-01 09:27:13.000000000 +0200
@@ -496,7 +496,8 @@
         # not sufficiently portable and uncomplicated.)
         $res->code($1);
         $res->message($2);
-        $res->decoded_content($text);
+        use Encode;
+        $res->content(Encode::encode_utf8($text));
         #$session->update($res) if ($res->is_success || $res->code != 401);
         if ($res->header('set-cookie')) {
             my $jar = HTTP::Cookies->new;




Of course this cannot be correct but for me it works right now quite
well but only because rt.cpan.org sends charset=utf-8 or so.

=cut

use strict;
use warnings;

use RT::Client::REST;
use Getopt::Long;
use List::Util qw(max);
use DateTime::Format::Strptime;
use Pod::Usage qw(pod2usage);
use YAML::Syck;

warn "Working with version $RT::Client::REST::VERSION";

# XXX yes, it's a stupid setup that requires an argument to
# (logically) boolean options
my %Config = (
              fromdate    => "", # "2009-01-01",
              todate      => "", # "2010-01-01",
              chunksize   => 396,
              html        => 0,
              inclself    => 1, # setting to 0 not reliable
              password    => '',
              server      => 'https://rt.cpan.org',
              top         => 70,
              upto        => 0,  # allows recalc against a point in the past
              username    => 'ANDK',
              withlastyear => 0, # well, hardcoded 2007; probably a crappy idea
             );

my @opt = $optpod =~ /B<--(\S+)>/g;
GetOptions(\my %config, @opt) or pod2usage(2);
if ($config{help}) {
  pod2usage(0);
}
while (my($k,$v) = each %config) {
  $Config{$k} = $v;
}

my $yaml_db_file = "$ENV{HOME}/sources/CPAN/data/query-rt-group-by-requestor.yml";
my $ALL;
if (-e $yaml_db_file) {
  warn "Loading yaml file '$yaml_db_file'";
  $ALL = YAML::Syck::LoadFile($yaml_db_file);
} else {
  warn "WARNING: yaml file '$yaml_db_file' not found!!!";
  sleep 3;
  $ALL = {};
}
my $curmax = max keys %{$ALL->{tickets} || {}};
$curmax ||= 0;
print "highest registered ticket number ATM: $curmax\n";
$curmax ||= 1;
FINDHOLES: for (my $i = 1; $i <= $curmax; $i++) {
  if (exists $ALL->{tickets}{$i}) {
  } else {
    $curmax = $i;
    last FINDHOLES;
  }
}
print "Max after findholes: $curmax\n";
TRIM: for (my $i = $curmax;;$i--) {
    my $ticket = $ALL->{tickets}{$i};
    $curmax = $i;
    if (keys %$ticket) {
        last;
    } else {
        delete $ALL->{tickets}{$i};
    }
}
print "Max after trim: $curmax\n";

my $nextmax = $curmax + $Config{chunksize};

my $rt = RT::Client::REST->new(
                               server  => $Config{server},
                               timeout => 300
                              );

if ($Config{username} && !$Config{password}) {
  use Term::ReadKey;
  local $|=1;
  print "Please enter the password of user '$Config{username}': ";
  ReadMode "noecho"; # Turn off controls keys
  my $pw = ReadLine;
  print "\n";
  ReadMode 0; # Reset tty mode before exiting
  $Config{password} = $pw;
}
if ($Config{username} && $Config{password}) {
  eval { $rt->login( username => $Config{username}, password => $Config{password} ); };
  die "Alert: Problem logging in: '$@'" if $@;

  my @ids;
  eval {
    @ids = $rt->search(
                       type    => 'ticket',
                       query   => qq[(Id >= $curmax and Id <= $nextmax)],
                      );
  };
  die "search failed: $@" if $@;

  my %ids;
  @ids{@ids} = ();
  $|=1;
  my $maxid = max @ids;
  print "filling $curmax..$maxid\n";
 ID: for my $id ($curmax..$maxid) {
    my $feedback;
    if ($ALL->{tickets}{$id}){
      $feedback = "E"; # existed before
    } else {
      if (exists $ids{$id}) {
      } elsif (keys %ids) {
      } else {
        print "\nStopping at $id.\n";
        last ID;
      }
      my $ticket;
      if (exists $ids{$id}) {
        $ticket = eval { $rt->show(type => 'ticket', id => $id) };
        if (!$ticket || $@) {
          warn "Emergency-saving YAML";
          YAML::Syck::DumpFile("$yaml_db_file.new", $ALL);
          rename "$yaml_db_file.new", $yaml_db_file;
          die $@;
        }
      } else {
        $ticket = {};
      }
      if (keys %$ticket) {
        $feedback = "w"; # wrote something interesting
      } else {
        $feedback = "e"; # empty
      }
      $ALL->{tickets}{$id} = $ticket;
    }
    print $feedback;
    delete $ids{$id};
    unless ($id % 37){
      print "z";
      YAML::Syck::DumpFile("$yaml_db_file.new", $ALL);
      rename "$yaml_db_file.new", $yaml_db_file;
      sleep 3;
    }
  }
  YAML::Syck::DumpFile("$yaml_db_file.new", $ALL);
  rename "$yaml_db_file.new", $yaml_db_file;
  print "filled\n";
}

sub who {
  my($v) = @_;
  my $who = $v->{Requestors} || $v->{Creator};
  return "" unless $who;
  if ($who =~ s/\@cpan\.org(,.*)?$//) {
    $who = uc $who;
  }
  my %alias = map { s/~A~/@/r } (
               'ANDY~A~PETDANCE.COM, ESUMMERS' => 'PETDANCE',
               'Marek.Rouchal~A~gmx.net'      => 'MAREKR',
               'marek.rouchal~A~infineon.com' => 'MAREKR',
               'aaron~A~FreeBSD.org'          => 'ACDALTON',
               'acferen~A~yahoo.com'          => 'Andrew Feren',
               'agentzh~A~gmail.com'          => 'AGENT',
               'alexchorny~A~gmail.com'       => 'CHORNY',
               'andreas.koenig~A~anima.de'    => 'ANDK',
               'andrey~A~kostenko.name'       => 'GUGU',
               'andy~A~petdance.com'          => 'PETDANCE',
               'a.r.ferreira~A~gmail.com'     => 'FERREIRA',
               'ask~A~develooper.com'         => 'ABH',
               'at~A~altlinux.org'            => 'ATOURBIN',
               'at~A~altlinux.ru'             => 'ATOURBIN',
               'audreyt~A~audreyt.org'        => 'AUDREYT',
               'autrijus~A~autrijus.org'      => 'AUDREYT',
               'barbie~A~missbarbell.co.uk'   => 'BARBIE',
               'blair~A~orcaware.com'         => 'BZAJAC',
               'bobtfish~A~bobtfish.net'      => 'BOBTFISH',
               'chad.a.davis~A~gmail.com'     => 'CADAVIS',
               'chris~A~clotho.com'           => 'CLOTHO',
               'corion~A~corion.net'          => 'CORION',
               'cpan~A~ali.as'                => 'ADAMK',
               'cpan~A~audreyt.org'           => 'AUDREYT',
               'cpan~A~chrisdolan.net'        => 'CDOLAN',
               'cpan~A~clotho.com'            => 'CLOTHO',
               'cpan~A~pjedwards.co.uk'       => 'STIGPJE',
               'ddascalescu+perl~A~gmail.com' => 'DANDV',
               'dan.horne~A~redbone.co.nz'    => 'DHORNE',
               'davem~A~iabyn.com'            => 'DAVEM',
               'david~A~davidfavor.com'       => 'David Favor',
               'david~A~landgren.net'         => 'DLAND',
               'david.tulloh~A~AirservicesAustralia.com' => 'LORDLOD',
               'dha~A~panix.com'              => 'DHA',
               'dmacks~A~netspace.org'        => 'DMACKS',
               'florent.angly~A~gmail.com'    => 'FANGLY',
               'felix.ostmann~A~thewar.de'    => 'SADRAK',
               'frank.wiegand~A~gmail.com'    => 'FWIE',
               'gbarr~A~pobox.com'            => 'GBARR',
               'gregoa~A~debian.org'          => 'GREGOA',
               'he~A~NetBSD.org'              => 'Havard Eidnes',
               'imacat~A~mail.imacat.idv.tw'  => 'IMACAT',
               'ivorw-cpan~A~xemaps.com'      => 'IVORW',
               'jdhedden~A~1979.usna.com'     => 'JDHEDDEN',
               'jesse~A~bestpractical.com'    => 'JESSE',
               'jesse~A~fsck.com'             => 'JESSE',
               'jhi~A~iki.fi'                 => 'JHI',
               'jozef~A~kutej.net'            => 'JKUTEJ',
               'julian~A~mehnle.net'          => 'JMEHNLE',
               'kmx~A~volny.cz'               => 'KMX',
               'leon~A~astray.com'            => 'LBROCARD',
               'leonerd-cpan~A~leonerd.org.uk' => 'LEONERD',
               'livingcosmos~A~gmail.com'     => 'TBONE', # http://www.livingcosmos.org/about/
               'malmberg~A~Encompasserve.org' => 'John Malmberg',
               'mark~A~summersault.com'       => 'MARKSTOS',
               'mark~A~twoshortplanks.com'    => 'MARKF',
               'matthew~A~mdarwin.ca'         => 'MDARWIN',
               'merlyn~A~stonehenge.com'      => 'MERLYN',
               'mnodine~A~alum.mit.edu'       => 'NODINE',
               'm.nooning~A~comcast.net'      => 'MNOONING',
               'mst~A~shadowcat.co.uk'        => 'MSTROUT',
               'mstevens~A~etla.org'          => 'MSTEVENS',
               'nadim~A~khemir.net'           => 'NKH',
               'nick~A~ccl4.org'              => 'NWCLARK',
               'nigel.metheringham~A~Dev.intechnology.co.uk' => 'NIGELM',
               'njh~A~bandsman.co.uk'         => 'NJH',
               'njh~A~ecs.soton.ac.uk'        => 'NJH',
               'nospam-abuse~A~bloodgate.com' => 'TELS',
               'ntyni~A~iki.fi'               => 'Niko Tyni',
               'nothingmuch~A~woobling.org'   => 'NUFFIN',
               'otto.hirr~A~olabinc.com'      => 'OTTO',
               'perl-rt~A~misterwhipple.com'  => 'MRWHIPPLE',
               'perl~A~evancarroll.com'       => 'ECARROLL',
               'pjacklam~A~online.no'         => 'PJACKLAM',
               'rafl~A~debian.org'            => 'FLORA',
               'rcaputo~A~pobox.com'          => 'RCAPUTO',
               'ron~A~savage.net.au'          => 'RSAVAGE',
               'rurban~A~x-ray.at'            => 'RURBAN',
               'salvatore.bonaccorso~A~gmail.com' => 'Salvatore Bonaccorso',
               'sartak~A~gmail.com'           => 'SARTAK',
               'shlomif~A~iglu.org.il'        => 'SHLOMIF',
               'schmorp~A~schmorp.de'         => 'MLEHMANN',
               'schwern~A~pobox.com'          => 'MSCHWERN',
               'schwern~A~bestpractical.com'  => 'MSCHWERN',
               'slaven~A~rezic.de'            => 'SREZIC',
               'slaven~A~cpan'                => 'SREZIC',
               'slaven.rezic~A~berlin.de'     => 'SREZIC',
               'steve.hay~A~uk.radan.com'     => 'SHAY',
               'steve~A~fisharerojo.org'      => 'SMPETERS',
               'steven~A~knowmad.com'         => 'WMCKEE',
               'stro~A~railways.dp.ua'        => 'STRO',
               'taro.nishino~A~gmail.com'     => 'TNISHINO',
               'mail~A~tobyinkster.co.uk'     => 'TOBYINK',
               'todd.e.rinaldo~A~jpmorgan.com'=> 'TODDR',
               'toddr~A~null.net'             => 'TODDR',
               'tokuhirom+cpan~A~gmail.com'   => 'TOKUHIROM',
               'tony~A~develop-help.com'      => 'TONYC',
               # JKEGL is it in https://rt.cpan.org/Ticket/Display.html?id=46925
               'user42~A~zip.com.au',         => 'JKEGL', # why did I believe 'KRYDE',?
               'ville.skytta~A~iki.fi'        => 'SCOP',
               'wb8tyw~A~gmail.com'           => 'John Malmberg',
               'william~A~knowmad.com'        => 'WMCKEE',
               'xdaveg~A~gmail.com'           => 'DAGOLDEN',
               'xenoterracide~A~gmail.com'    => 'XENO',
               'zefram~A~fysh.org'            => 'ZEFRAM',
               'cpan~A~zoffix.com'            => 'ZOFFIX',
              );
  $who = $alias{$who} || $who;
  if ($Config{inclself}) {
    # fall through
  } else {
    if ($who && $v->{Owner} && $who eq $v->{Owner}) {
      return;
    }
    if ($who eq "TONYC" and $v->{Queue} eq "Imager") {
      # too many tickets never get an owner, so inclself should stay
      # default until we know how to reliably identify tickets to self
      return;
    }
  }
  return $who;
}

sub users_2007 {
  my $postedlist = <<'EOL';
     1: guest      486
     2: SREZIC     393
     3: MSCHWERN   385
     4: ANDK       347
     5: ADAMK      317
     6: MARKSTOS   304
     7: CHORNY     248
     8: RRWO       205
     9: WMCKEE     161
    10: SMPETERS   136
    11: TONYC      132
    12: LGODDARD   129
    13: CDOLAN     117
    14: JDHEDDEN   114
    15: RCAPUTO    110
    16: RJBS       101
    17: MAREKR     101
    18: MTHURN      98
    19: ATOURBIN    85
    20: PETDANCE    84
    21: SAPER       79
    22: BARBIE      72
    23: JESSE       71
    24: NKH         62
    25: IMACAT      60
    26: CORION      60
    27: ACDALTON    59
    28: DAGOLDEN    58
    29: CLOTHO      57
    30: RSAVAGE     57
    31: MERLYN      52
    32: HANENKAMP   51
    33: Niko Tyni   51
    34: TELS        49
    35: LTHEGLER    47
    36: MARKF       47
    37: PODMASTER   47
    38: JPIERCE     47
    39: GROUSSE     46
    40: BZAJAC      46
    41: KANE        44
    42: JONASBN     44
    43: STENNIE     43
    44: SPOON       43
    45: MUENALAN    41
    46: JJORE       41
    47: RURBAN      40
    48: SHLOMIF     37
    49: JOHANL      36
    50: dsteinbrunner@pobox.com   36
    51: SHAY        36
    52: DLAND       36
    53: IVORW       35
    54: mcummings@gentoo.org   34
    55: FERREIRA    34
    56: NUFFIN      34
    57: NIKC        33
    58: STIGPJE     33
    59: arnaud@underlands.org   33
    60: MSTEVENS    32
    61: DHA         31
    62: ABH         31
    63: NJH         31
    64: SCOP        30
    65: KWILLIAMS   30
    66: STRO        30
    67: SMYLERS     30
    68: perl@infotrope.net   29
    69: DHORNE      29
    70: dave@riverside-cms.co.uk   28
    71: SMUELLER    28
    72: dhoworth@mrc-lmb.cam.ac.uk   27
    73: ISHIGAKI    27
    74: JKEENAN     26
    75: JMEHNLE     26
    76: DMUEY       26
    77: tom@eborcom.com   25
    78: cpan@fireartist.com   25
    79: JROCKWAY    25
    80: NODINE      25
    81: DMITRI      24
    82: JHI         24
    83: RENEEB      24
    84: jpo@di.uminho.pt   24
    85: DOM         24
    86: cweyl@alumni.drew.edu   24
    87: BOOK        23
    88: OVID        23
    89: DAXIM       22
    90: tony@develop-help.com   22
    91: THALJEF     22
    92: CNANDOR     21
    93: GHENRY      21
    94: CLACO       21
    95: perl@crystalflame.net   21
    96: YANICK      21
    97: m.romani@spinsoft.it   21
    98: MARKOV      20
    99: DJERIUS     20
    100: LEIRA       20
EOL

  my %p;
  for my $line (split /\n/, $postedlist) {
    my($pos,$name,$count) = $line =~ /(\d+):\s(\S.+\S)\s+(\d+)$/;
    $p{$name} = { pos => $pos, count => $count };
  }
  return \%p;
}

keys %{$ALL->{tickets}}; # reset iterator
my %S;
TICKET: while (my($k,$v) = each %{$ALL->{tickets}}) {
  if (my $upto = $Config{upto}) {
    next TICKET if $k > $upto;
  }
  my $fromdate = $Config{fromdate};
  my $todate = $config{todate};
  if ($fromdate or $todate) {
    my $date = $v->{Created} or next TICKET;
    my $dt;
 DATEFMT: for my $pat (
                       "%a %b %d %T %Y", # Sun Sep 28 12:23:12 2008
                       "%a, %d %b %Y %T %z", # Sun, 28 Sep 2008 12:23:12 +0100
                       "%b %d, %Y %R", # July 10,...
                       "%b  %d, %Y %R", # July  4,...
                       "%Y-%m-%d %H:%M:%S", # 2010-01-20 20:40:53 for ticket 53865
                      ) {
      $dt = eval {
        my $p = DateTime::Format::Strptime->new
            (
             locale => "en",
             time_zone => "UTC",
             pattern => $pat,
            );
        $p->parse_datetime($date)
      };
      last DATEFMT if $dt;
    }
    unless ($dt) {
      die "Could not parse date[$date] for ticket $k";
    }
    if ($fromdate and $dt->datetime lt $fromdate
        or $todate and $dt->datetime ge $todate) {
      next TICKET;
    }
  }
  my $who = who($v);
  next TICKET unless $who;
  $S{$who}++;
}
my $top = 1;
printf "%s\n", $Config{html} ? "<dl>" : "";
my $longestname = 0;
my $showtop = $Config{top} || 40;
for my $k (sort {$S{$b} <=> $S{$a}} keys %S) {
  $longestname = length $k if length $k > $longestname;
  last if $top >= $showtop;
  $top++;
}
$top = 1;
my $p;
my $sprintf = "%s%2s: %-".$longestname."s %4d%s\n";
if ($Config{withlastyear}) {
  $sprintf = "%s%2s: %-".$longestname."s %4d (%2s) %3s%s\n";
  $p = users_2007();
}
my $last_score = 0;
for my $k (sort {$S{$b} <=> $S{$a}} keys %S) {
  my $score = $S{$k};
  my $top_or_empty = $score == $last_score ? "" : $top;
  my $x = sprintf
      (
       $sprintf,
       $Config{html} ? "<code>" : " ",
       $top_or_empty,
       $k,
       $score,
       $p ? ( $p->{$k}{pos} || "-", $p->{$k}{pos} ? $S{$k}-$p->{$k}{count} : "-") : (),
       $Config{html} ? "</code><br/>" : "",
      );
  $x =~ s/ /&nbsp;/g if $Config{html};
  print $x;
  last if $top >= $showtop;
  $top++;
  $last_score = $score;
}
printf "%s\n", $Config{html} ? "</dl>" : "";

# Local Variables:
# mode: cperl
# cperl-indent-level: 2
# End:
