#!/usr/bin/env perl
use strict;
use warnings;
use local::lib;

unless ( $ENV{RSYNC_PASSWORD} ) {
    $ENV{RSYNC_PASSWORD} = shift or die "RSYNC_PASSWORD not set and no argument???";
}

use File::Rsync::Mirror::Recent;
my $rrr = File::Rsync::Mirror::Recent->new
  (
   ignore_link_stat_errors => 1,
   localroot => "/home/rrrcpan/target",
   tempdir   => "/home/rrrcpan/tmp",
   remote => 'andk@cpan-rsync-master.perl.org::CPAN/RECENT.recent',
   max_files_per_connection => 5000,
   rsync_options => {
                     compress => 1,
                     links => 1,
                     'safe-links' => 1,
                     times => 1,
                     checksum => 0,
                    },
   verbose => 1,
   _runstatusfile => "/home/rrrcpan/recent-rmirror-state.yml",
   _logfilefordone => "/home/rrrcpan/recent-rmirror-donelog.log",
  );
$rrr->rmirror ( "skip-deletes" => 0, loop => 1 );
