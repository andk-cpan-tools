=pod 

The inspiration came from Brandon Byrd in the form of the topic
I<looking at CPAN's Health from a macro-level>

And then I said: the most interesting graph we came up with would be a
(x-axis: time, y-axis: number of pass/fail/unknown/na(stacked)). Let
me try. And this is the result.

I used the data I gather from log.txt on analysis and this explains
the interval of the chart: I have these data quickly accessible.

=cut

use strict;
use Chart::Clicker;
use Chart::Clicker::Axis::DateTime;
use Chart::Clicker::Data::Series;
use Chart::Clicker::Data::DataSet;
use DateTime::Format::Strptime;
use Chart::Clicker::Renderer::StackedArea;

# build the chart
my $chart = Chart::Clicker->new(width => 650, height => 400);

my $yfile = __FILE__;
$yfile =~ s/.pl$/.yaml/;
use YAML::Syck;
my $y = YAML::Syck::LoadFile $yfile;
my @X = sort keys %$y;
pop @X;
my $strp = DateTime::Format::Strptime->new
    (
     pattern   => '%Y-%m-%d',
     # locale    => 'UTC',
     time_zone => 'UTC',
     on_error  => 'croak',
    );
my @Xepoch = map {
    my $dt = $strp->parse_datetime($_);
    $dt->epoch;
} @X;

my @series;
for my $state (qw(pass/10 fail unknown na)) {
    my(@v);
    if ($state =~ m!(.+)/(\d+)!) {
        my($statename,$divisor) = ($1,$2);
        @v = map {$y->{$_}{$statename}/$divisor} @X;
    } else {
        @v = map {$y->{$_}{$state}} @X;
    }
    push @series, Chart::Clicker::Data::Series->new
        (
         name    =>      $state,
         keys    =>      \@Xepoch,
         values  =>      \@v,
        );
}
# build the dataset
my $dataset = Chart::Clicker::Data::DataSet->new
    (
     series  =>      \@series
    );

# add the dataset to the chart
$chart->add_to_datasets($dataset);
my $def = $chart->get_context('default');
my $dtaxis = Chart::Clicker::Axis::DateTime->new
    (
     format => '%Y-%m-%d',
     position => 'bottom',
     orientation => 'horizontal'
    );
$def->domain_axis($dtaxis);
my $yaxis = Chart::Clicker::Axis->new
    (
     format => "%s",
     position => 'left',
     orientation => 'vertical',
     range => Chart::Clicker::Data::Range->new
     ({
       lower => 0,
       upper => 4000,
      }),
    );
$def->range_axis($yaxis);

my $title = "cpan health check";
$chart->title->text($title);
$chart->legend->visible(1);
# $chart->title->font->size(20);

# 1351728000,1354320000,1356998400
$def->domain_axis->tick_values([$Xepoch[0],grep {
    my $dt = DateTime->from_epoch(epoch => $_);
    $dt->day == 1;
} @Xepoch]);

my $area = Chart::Clicker::Renderer::StackedArea->new(opacity => .6);
$area->brush->width(3);
$def->renderer($area);
$def->renderer->additive(1);

# write the chart to a file
my($png) = __FILE__;
$png =~ s/\.pl$/.png/;
$chart->write_output($png);
