#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

$0 [OPTIONS]

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--minpostdate=i>

Looks like an integer but stands for a month in the format YYYYMM.
Actual DB range is 199908 to, at the time of this writing, 202202.
Used to generate where clauses such as

   ... where postdate >= $dbh->quote($Opt{minpostdate}) ...

Defaults to 201607.

=item B<--newperl=s@>

C<5.20.0 RC1>

=item B<--oldperl=s@>

C<5.18.2>



=back

=head1 DESCRIPTION

We generate a json file and a txt file. The latter for myself to view
the upcoming contents easily, the json for the webserver.

The generation took about 1-2 hours in the beginning but it got slower
and slower, the more reports arrived.

=head1 HISTORY

formerly known as cron-comparing-5181-5182rc2.pl

=head1 DEPLOYMENT

There were some rather long lines in the crontab that had to be edited
whenever a new release appeared. Something like

58 17 28 * * /home/andreas/src/installed-perls/v5.16.0/4e6d/bin/perl /home/andreas/src/andk-cpan-tools/bin/beforemaintrelease.pl --newperl="5.27.4" --oldperl=5.26.0 > /home/andreas/beforemaintrelease-cron.out 2>&1

Having more than one pair of old and new perl was allowed. Running
them every few days was also usual

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use POSIX ();
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{oldperl} = ['5.18.2']     unless $Opt{oldperl} && @{$Opt{oldperl}};
$Opt{newperl} = ['5.20.0 RC1'] unless $Opt{newperl} && @{$Opt{newperl}};
$Opt{minpostdate} = '201607';
die "the arrayrefs for oldperl and newperl must be of same length, "
    . "found lengths %d and %d, cannot continue",
    scalar @{$Opt{oldperl}},
    scalar @{$Opt{newperl}}
    unless scalar @{$Opt{oldperl}}==scalar @{$Opt{newperl}};

use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;

my $HAVE_IPCCL = eval { require IPC::ConcurrencyLimit; 1 };
die unless $HAVE_IPCCL;

use DBI;

my($workdir);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
}
my($basename) = File::Basename::basename(__FILE__);
my $limit = IPC::ConcurrencyLimit->new
    (
     max_procs => 1,
     path      => "$workdir/IPC-ConcurrencyLimit-$basename",
    );
my $limitid = $limit->get_lock;
if (not $limitid) {
    warn "Another process appears to be still running. Exiting.";
    exit(0);
}

my $pgdbh = DBI->connect("dbi:Pg:dbname=analysis") or die "Could not connect to 'analysis': $DBI::err";
my $sth0 = $pgdbh->prepare("SELECT COUNT(*) FROM cpanstats WHERE postdate >= ? and perl = ?");
#### my $sth2 = $pgdbh->prepare("select perl, state, count(*) from cpanstats where postdate >= ? and dist = ? and version = ? and perl in (?, ?) and state in ('pass','fail') group by perl, state");
my $sth3 = $pgdbh->prepare("SELECT COUNT(distinct(dist||version)) FROM cpanstats WHERE postdate >= ? and perl = ?");
use JSON::XS;
my $jsonxs = JSON::XS->new->indent(1)->canonical(1);

for my $i (0..$#{$Opt{newperl}}) {
    my($perl1,$perl2);
    $perl1 = $Opt{oldperl}[$i];
    $perl2 = $Opt{newperl}[$i];
    unless ($perl1 && $perl2) {
        die "options oldperl and newperl are mandatory, found $perl1 vs $perl2";
    }
    $sth0->execute($Opt{minpostdate}, $perl2);
    my($total) = $sth0->fetchrow_array;
    $sth3->execute($Opt{minpostdate}, $perl2);
    # mnemotechnics: newperl and perl2 and rc all associated with the release candidate
    my($totalrcdists) = $sth3->fetchrow_array;

    # on the days 2016062[12] the smoker was of DCOLLINS was broken

    # update 2022-02-09: ad-hoc decision: ignore all tests before 201607 (abandon 70M)

    $pgdbh->do("CREATE temporary TABLE distnames$i AS
 ( SELECT DISTINCT dist, version FROM cpanstats
             WHERE postdate >= ?
               AND state = 'fail'
               AND perl = ?
 ) ", undef, $Opt{minpostdate}, $perl2) or die;
    my $sth1 = $pgdbh->prepare("select dist, version from distnames$i");

    my $ts = POSIX::strftime "%FT%T", gmtime(time);
    my $S =
        {
         "!ALL" => {
                    perls        => [ $perl1, $perl2 ],
                    $perl2       => $total,
                    totalrcdists => $totalrcdists,
                    datetime     => $ts,
                   },
        };
    #### warn "starting matrix at $ts";
    $pgdbh->do("CREATE temporary TABLE matrix$i AS
  ( SELECT perl, state, cpanstats.dist, cpanstats.version, count(*) cnt
    FROM cpanstats
    JOIN distnames$i ON (distnames$i.dist=cpanstats.dist AND distnames$i.version=cpanstats.version)
    WHERE postdate >= '$Opt{minpostdate}' and perl in ('$perl1', '$perl2') and state in ('pass','fail')
    GROUP BY perl, state, cpanstats.dist, cpanstats.version
  )") or die "error while creating temporary table 2: $DBI::err";
    my $ts2 = POSIX::strftime "%FT%T", gmtime(time);
    #### warn "finished matrix at $ts2";
    my $sth5 = $pgdbh->prepare("SELECT matrix$i.perl, matrix$i.state, matrix$i.cnt from matrix$i WHERE matrix$i.dist=? and matrix$i.version=?");

 MAIN: {
        $sth1->execute;
        my $i = 0;
        my $rows = $sth1->rows;

        while (my($dist,$version) = $sth1->fetchrow_array) {
            $i++;
            #### $sth2->execute($Opt{minpostdate}, $dist, $version, $perl1, $perl2);
            #### $DB::single = $dist eq "Lingua-Ogmios" && $version eq '0.011';
            $sth5->execute($dist, $version);
            my $s;
            my %seen = (pass => 0, fail => 0);
            #### while (my($perl,$state,$count) = $sth2->fetchrow_array) {
            while (my($perl,$state,$count) = $sth5->fetchrow_array) {
                $s->{$perl}{$state} = $count;
                $seen{$state}++;
            }
            my $ignore = 0;
            if ($seen{pass} == 0) {
                $ignore=1;
            }
            if ($seen{fail} == 2 and $seen{pass} == 2) {
                $ignore=1;
            }
            store($dist,$version,$s,$S,$ts,$perl1,$perl2,$i,$rows,$ignore);
        }
    }
    $pgdbh->do("DROP TABLE matrix$i");
    $pgdbh->do("DROP TABLE distnames$i");
}

sub write_new_line {
    my($S, $dist, $version, $s, $ts, $row_i, $rows_total, $perl1, $perl2, $serialized_perl_versions) = @_;
    $S->{"!CAND"}{$dist}{$version} = $s;
    my $outfile = "$ENV{HOME}/var/beforemaintrelease/result-$serialized_perl_versions";
    File::Path::mkpath File::Basename::dirname $outfile;
    { #JSON
        open my $fh, ">", "$outfile.new"  or die;
        print {$fh} $jsonxs->encode($S);
        close $fh or die $!;
        rename "$outfile.new", "$outfile.json" or die "Could not rename: $!";
    }
    { #TXT
        open my $fh, ">", "$outfile.new" or die;
        for my $k (sort keys %{$S->{"!CAND"}}){ # $k='Date-Formatter'
            for my $k2 (sort keys %{$S->{"!CAND"}{$k}}){ # $k2='0.11'
                my $v = $S->{"!CAND"}{$k}{$k2};
                no warnings 'uninitialized';
                printf {$fh} "%-56s  %3d %3d    %3d %3d\n",
                                     "$k-$k2",
                                            $v->{$perl1}{pass}, $v->{$perl1}{fail},
                                                       $v->{$perl2}{pass}, $v->{$perl2}{fail};
            }
        }
        close $fh or die $!;
        rename "$outfile.new", "$outfile.txt" or die "Could not rename: $!";
    }
}

sub store {
    my($dist,$version,$s,$S,$ts,$perl1,$perl2,$row_i,$rows_total,$ignore) = @_;
    my $serialized_perl_versions = join ":", map {
        my($lex) = lc($_);
        $lex =~ s/[\s\:\-]//g;
        $lex
    } $perl1, $perl2;
    unless ($ignore) {
        write_new_line($S, $dist, $version, $s, $ts, $row_i, $rows_total, $perl1, $perl2, $serialized_perl_versions);
    }
    if ($row_i == $rows_total) {
        use File::Spec ();
        my $overviewfile = "$ENV{HOME}/var/beforemaintrelease/overview.json";
        File::Path::mkpath File::Basename::dirname $overviewfile;
        my $lfh;
        until ($lfh = lockfilehandle($overviewfile)) {
            sleep 1;
        }
        my $slurp = do { local $/; <$lfh> };
        $slurp ||= "{}";
        my $O = $jsonxs->decode($slurp);
        $O->{$serialized_perl_versions} = $ts;
        seek $lfh, 0, 0;
        print {$lfh} $jsonxs->canonical->encode($O);
        truncate $lfh, tell $lfh;
        close $lfh or die "Could not close '$overviewfile': $!";
    }
}

sub lockfilehandle {
    my($lockfile) = @_;
    use Fcntl ();
    use File::Basename ();
    use File::Path ();
    File::Path::mkpath File::Basename::dirname $lockfile;
    my $lfh;
    unless (open $lfh, "+<", $lockfile) {
        unless ( open $lfh, ">>", $lockfile ) {
            die "ALERT: Could not open >> '$lockfile': $!";
        }
        unless ( open $lfh, "+<", $lockfile ) {
            die "ALERT: Could not open +< '$lockfile': $!";
        }
    }
    if (flock $lfh, Fcntl::LOCK_EX|Fcntl::LOCK_NB) {
        # print "Info[$$]: Got the lock, continuing";
        return $lfh;
    } else {
        # print "FATAL[$$]: lockfile '$lockfile' locked by a different process";
        return undef;
    }
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
