#!/usr/bin/perl

use Sys::Hostname;
die "not yet ported to ds8143" if hostname eq "ds8143";

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--dir=s>

defaults to C</home/ftp/cpantesters/reports>

=item B<--help|h!>

This help

=item B<--interactive|i!>

Defaults to true. If set to false (--nointeractive or --noi), you can
remove hundreds of thousands of file without being asked a single
question.

=item B<--numversions=i>

Defaults to 2 which means look at the two most recent versions for
whatever we're doing. Everything older is discarded considered
outdated.

=back

=head1 DESCRIPTION

On analysis: walk through all distro-directories we have and remove outdated ones.

Born as a copy of smokehistory.pl

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath rmtree);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);
use List::MoreUtils qw(uniq);
use List::Util qw(first);
use Term::Prompt qw(prompt);
use Text::Format;
use YAML::Syck;
use version;

our %Opt;
lock_keys %Opt, map { /([^=!\|]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
$Opt{dir} ||= "/home/ftp/cpantesters/reports";
$Opt{interactive} = 1 unless defined $Opt{interactive};
$Opt{numversions} ||= 2;

opendir my $dh, $Opt{dir} or die;
my %S; # for the summary at the end
my $tf = Text::Format->new(firstIndent=>0);
$|=1;
LETTERDIR: for my $dirent (sort readdir $dh) { # A B C
    next unless $dirent =~ /^[A-Za-z]$/;
    opendir my $dh2, "$Opt{dir}/$dirent" or die;
    my %vdistro;
 DISTRO: for my $dirent2 (readdir $dh2) { # Dpchrist-ExtUtils-MakeMaker-1.015
        next if $dirent2 =~ /^\.\.?$/;
        my $vdistro = $dirent2;
        my($distro,$version,$mod) = $vdistro =~ /(\S+)-(v?\d+(?:[\.\d]*[^-]*))(-(?:TRIAL|withoutworldwriteables|fix))?$/;
        unless (defined $version) {
            # Angel102
            ($distro,$version) = $vdistro =~ /(\D+)(\d+)/;
        }
        unless (defined $version){
            die "could not parse version from $vdistro";
        }
        $version=~s/[^\d\.]+$//;
        1 while $version=~s/([\d\.])[^\d\.]+.*/$1/;
        $version=~s/\.$//;
        # print "DEBUG: parse version[$version] of distro[$distro](vdistro[$vdistro])\n";
        my $numversion = eval {version->new($version)->numify};
        if (not defined $numversion) {
            die "ERROR: Could not parse version[$version] of distro[$distro](vdistro[$vdistro]): $@";
        } elsif ($@) {
            die "Panic: we have a \$\@[$@] but a numversion[$numversion] too";
        }
        $vdistro{$distro}{$numversion} = $vdistro;
    }
    for my $distro (keys %vdistro) {
        my $v = $vdistro{$distro};
        my @vdistros_sorted_by_version = sort { $a <=> $b } keys %$v;
        while (@vdistros_sorted_by_version > $Opt{numversions}) {
            my $cldir = $v->{$vdistros_sorted_by_version[0]};
            my $absdir = "$Opt{dir}/$dirent/$cldir";
            opendir my $dh3, "$absdir/nntp-testers" or die "Could not open '$absdir/nntp-testers': $!";
            my @cldirent = grep { /\.gz$/ } readdir $dh3;
            my $report = sprintf
                    ("==> Having reports from
 %d versions of
 %s in directory
 %s (%s). The oldest of those,
 %s, has
 %d reports\n\n",
                     scalar @vdistros_sorted_by_version,
                     $distro,
                     $cldir, join(",",@vdistros_sorted_by_version),
                     $vdistros_sorted_by_version[0],
                     scalar @cldirent,
                    );
            print $tf->format($report);
            my $answer;
            if ($Opt{interactive}) {
                $answer = lc prompt "x", "Shall I delete '$absdir'? (y/n/q)", "", "y";
            } else {
                $answer = 'y';
            }
            if ($answer eq "q") {
                last LETTERDIR;
            } elsif ($answer eq "n") {
            } elsif ($answer eq "y") {
                rmtree $absdir;
                if ($Opt{interactive}) {
                    warn sprintf "Deleted %d reports.\n", scalar @cldirent;
                }
            }
            shift @vdistros_sorted_by_version;
        }
    }
}
warn YAML::Syck::Dump \%S;

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
