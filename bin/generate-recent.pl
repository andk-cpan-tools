#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME

generate-recent.pl -

=head1 SYNOPSIS

 generate-recent.pl [OPTIONS]

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--jobs|j=i>

Parameter to pass to 'makeperl.pl'. Defaults to 1 which is usually
what we want. But in rare situations, when for example TEST produces a
fail but harness produces a pass, this comes in handy.

Update 2017-12-11 akoenig: I do not understand what I wanted to say
with that.

=item B<--keepfilesonerror!>

Do not do any cleanup on error, just bail out with an error.

=item B<--sleep=i>

Default: 2700; time to sleep after every git pull loop

=item B<--udonce=s>

One of DD DU UD UU. The default mechanism to pick between the four is
usually good, but when we know that perl is broken for, say,
C<uselongdouble>, then we may have to pick something manually and then
let the default mechanism take over again.

=back

=head1 DESCRIPTION

Runs makeperl.pl in a loop. Stops when no patches have arrived.

The loop is to run installed-perls-overview and inspect the list from
top to bottom. It is expected that the list is based on I<git
describe> and sorted like the trunk. And when the current 'git
describe' matches the first line, then we have nothing to do.

We set --jobs to 1 because at the moment we are much faster (>100
times) than the smoker and sleep long times between invocations. If we
were yet faster we would only waste both disk and CPU.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
Getopt::Long::Configure("no_auto_abbrev");
use Hash::Util qw(lock_keys);
use Pod::Usage;

our %Opt;
lock_keys %Opt, map { /([^=!\|]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

if ( $Opt{help} ) {
    pod2usage(0);
}
$Opt{jobs} //= 1;
$Opt{sleep} //= 2700;

use YAML::Syck;
use POSIX;
use Sys::Hostname ();

my @valid_debuggingoptions = (("EBUGGING=both")x1, ("EBUGGING=-g")x5, ("EBUGGING=none")x7);

my $waitabit = 0;
LOOP: while () {
    if ( $waitabit > time ) {
        my $sleep = $waitabit - time;
        $sleep = 1 if $sleep < 1;
        sleep $sleep;
    } else {
        $waitabit = time +120; # arbitrary
    }
    open my $fh, "-|", $^X, "$FindBin::Bin/installed-perls-overview.pl", "--max=100", "--minperl=v5.15.7";
    my(@perls,%perls_seen);
 OVLINE: while (<$fh>) {
        my($perl) = split " ", $_, 2;
        my $hostname = Sys::Hostname::hostname;
        $hostname =~ s/\..*//;
        my $perl_or_host;
        if ($hostname eq "k83") {
            $perl_or_host = "perl";
        } else {
            $perl_or_host = "host/$hostname";
        }
        # Can't exec "/home/sand/src/perl/repoperls/installed-perls/perl/v5.23.7-35-g6002757/3bd2/bin/perl": No such...
        my $absperl = sprintf "/home/sand/src/perl/repoperls/installed-perls/%s/%s/bin/perl", $perl_or_host, $perl;
        open my $fh2, "-|", $absperl, "-V:useithreads|uselongdouble";
        my @stat = stat $absperl;
        my $mtime = POSIX::strftime "%FT%TZ", gmtime $stat[9];
        my %conf;
        while (<$fh2>) {
            my($k,$v) = /(\w+)='(\w*)';/;
            $conf{$k} = $v || 'undef';
        }
        next OVLINE if $perls_seen{join ",", @conf{qw(useithreads uselongdouble)}}++;
        push @perls, +{ perl => $perl, mtime => $mtime, %conf };
        last OVLINE if @perls >= 4;
    }
    warn YAML::Syck::Dump \@perls;
    my @system_git = (git => "pull");
    while () {
        last if 0 == system @system_git;
        warn sprintf "%s UTC: git poll has failed, sleeping 900 for the next retry\n", scalar gmtime;
        sleep 900; # arbitrary
    }
    my $describe = `git describe`;
    warn $describe;
    chomp $describe;
    if ($describe eq
        substr($perls[0]{perl},0,index($perls[0]{perl},"/"))) {
        my $sleep = $Opt{sleep};
        my $eta = time+$sleep;
        local($|)=1;
        while () {
            my $left = $eta - time;
            last if $left < 0;
            printf "\r%d    ", $left;
            sleep 1;
        }
        print "\n";
        next LOOP;
    }
    my $ud;
    if (defined $Opt{udonce}) {
        $ud = delete $Opt{udonce};
    } elsif (@perls >= 4) {
        $ud = sprintf "%s%s",
            uc(substr($perls[3]{useithreads}||qw(D U)[int rand 2], 0, 1)),
                uc(substr($perls[3]{uselongdouble}||qw(D U)[int rand 2], 0, 1));
    } else {
        my %pickud = map { $_ => 1 } qw(UU UD DU DD);
        for my $p (@perls) {
            my $thisud = sprintf "%s%s",
            uc(substr($p->{useithreads}, 0, 1)),
                uc(substr($p->{uselongdouble}, 0, 1));
            delete $pickud{$thisud};
        }
        $ud = (keys %pickud)[int rand scalar keys %pickud];
    }
    my $debuggingoption = $valid_debuggingoptions[int rand scalar @valid_debuggingoptions];
    my @system_mp =
        ($^X,
         $DB::VERSION ? ("-d") : (),
         "$FindBin::Bin/makeperl.pl",
         "--jobs=$Opt{jobs}",
         "--ud=$ud",
         "--report",
         "--debuggingoption=$debuggingoption",
         "--nopatchperl", # I do not expect that we ever want to turn this on for bleadperl
         $Opt{keepfilesonerror} ? "--keepfilesonerror" : (),
        );
    warn YAML::Syck::Dump \@system_mp;
    sleep 3; # just so that the message can be seen
    0 == system @system_mp or die "Alert: problem running system[@system_mp]";
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
