use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Most probably a one-off script because we hope to get this in munin then.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use Fcntl ":seek";
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);
use Time::HiRes qw(sleep time);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

my $mb_log = "$ENV{HOME}/var/metabase-log/metabase.log-proc.log.1";
# 20121018T195811:INFO[13627]: Newlines written: 506, Keeping in mem records: 1000

my $refill_dir = "$ENV{HOME}/var/refill-cpanstatsdb";
# .../2012/10/20121027T2308-24182412-24182626.json.gz => 215 records

my $Pstamp_s = "";
my $Ptell = 202000;
for (my $t = 1328313600; $t < time; $t+=300) {
    my($mb_cnt,$refill_cnt) = (0,0);
    my @time = gmtime($t);
    $time[4]++;
    $time[5]+=1900;
    my $stamp_s = sprintf "%04d%02d%02dT%02d%02d%02d", @time[5,4,3,2,1,0];
    open my $fh, $mb_log or die "Could not open $mb_log\: $!";
    seek $fh, $Ptell, SEEK_SET;
    while (<$fh>) {
        next unless /^(\d{8}T\d{6}):INFO\[\d+\]: Newlines written:\s(\d+),/;
        my($tstamp,$tcnt) = ($1,$2);
        last if $tstamp gt $stamp_s;
        next if $tstamp lt $Pstamp_s;
        next unless $tcnt;
        $mb_cnt+=$tcnt;
    }
    $Ptell = tell $fh;
    $Ptell -= 512; # certainly too much
    opendir my $dh, $refill_dir or die "Could not opendir $refill_dir\: $!";
    my %logfiles;
    for my $y (sort readdir $dh) {
        next unless $y =~ /^(\d{4})$/;
        last if $y gt $stamp_s;
        next if $y lt substr($stamp_s,0,4);
        opendir my $dh2, "$refill_dir/$y" or die "Could not opendir $refill_dir/$y\: $!";
        for my $m (sort readdir $dh2) {
            next unless $m =~ /^(\d{2})$/;
            last if "$y$m" gt $stamp_s;
            next if "$y$m" lt substr($stamp_s,0,6);
            opendir my $dh3, "$refill_dir/$y/$m" or die "Could not opendir $refill_dir/$y/$m\: $!";
            for my $logfile (sort readdir $dh3) {
                next unless $logfile =~ /(\d{8}T\d{4})-(\d+)-(\d+)\.json\.gz$/;
                my($tstamp,$tstart,$tend) = ($1,$2,$3);
                last if $tstamp gt $stamp_s;
                next if $tstamp le substr($Pstamp_s,0,13);
                my $tcnt = $tend - $tstart + 1;
                $refill_cnt+=$tcnt;
                $logfiles{$logfile}=1;
            }
        }
    }
    next unless $mb_cnt || $refill_cnt;
    printf "%s %d %8d %8d %s\n", $stamp_s, $t, $mb_cnt, $refill_cnt, join("|",sort keys %logfiles);
    $Pstamp_s = $stamp_s;
}
