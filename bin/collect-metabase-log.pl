#!/usr/bin/perl

=head1 NAME

....pl - 

=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut


my $optpod = <<'=back';

=item B<--help|h!>

This help

=item B<--once!>

Not a loop, just once.

=item B<--storefile=s>

Defaults to C<metabase-log.txt>.

=item B<--quiet!>

No diagnostic output

=back

=head1 DESCRIPTION

It's a cronjob but it runs forever until killed. Later starting
cronjobs give up quickly when they do not get the lock.

=head1 BACKGROUND

Read the logfile from metabase.cpantesters.org, write the lines we
have not yet seen into file specified as C<storefile>. In the
following we collect a few facts we believe to know.

An example of a line in log.txt would be

[2012-10-06T16:30:00Z] [Chris Williams (BINGOS)] [pass] [RUBYKAT/Dist-Zilla-Plugin-GitFmtChanges-0.005.tar.gz] [i86pc-solaris-thread-multi-64int] [perl-v5.14.2] [12d49d6e-0fd3-11e2-bdcc-373e3b6b8117] [2012-10-06T16:30:00Z]

That same report in cpanstats.db would be

sqlite> select * from cpanstats where id=23763028;
23763028|12d428d4-0fd3-11e2-bdcc-373e3b6b8117|pass|201210|xxxx@xxxxxxx.xxxxxxxxx.xx.xx ((xxxxxxx x))|Dist-Zilla-Plugin-GitFmtChanges|0.005|i86pc-solaris-thread-multi-64int|5.14.2|solaris|2.11|201210061630|2

What is this relation between the two GUIDs? Part 2..5 are equal, part
1 is different, that seems to be a rule.

 | log.txt                              | barbie                               |       id |
 | 12d49d6e-0fd3-11e2-bdcc-373e3b6b8117 | 12d428d4-0fd3-11e2-bdcc-373e3b6b8117 | 23763028 |
 | 2012-10-06T16:30:00Z                 | 201210061630                         |          |
 | 12519d2e-0fd3-11e2-8e23-9a24f233959e | 12518654-0fd3-11e2-8e23-9a24f233959e | 23763027 |
 | 2012-10-06T16:29:59Z                 | 201210061629                         |          |
 | 117831ba-0fd3-11e2-9e90-da8a1df5d9f4 | 1177fb5a-0fd3-11e2-9e90-da8a1df5d9f4 | 23763026 |
 | 2012-10-06T16:29:59Z                 | 201210061629                         |          | 

This means we have no common id. But:

 | field    | type    | comment                        | useful?                     |
 |----------+---------+--------------------------------+-----------------------------|
 | id       | INTEGER | invented                       |                             |
 | guid     | TEXT    | similar but different          | maybe but distrusted        |
 | state    | TEXT    | same                           | yes                         |
 | postdate | TEXT    | only month                     |                             |
 | tester   | TEXT    | different via some db mapping  |                             |
 | dist     | TEXT    | cut out of fully composed name | yes, but not fully reliable |
 | version  | TEXT    | "                              | yes, but not fully reliable |
 | platform | TEXT    | same                           | yes                         |
 | perl     | TEXT    | cut out of longer variant      | yes, but not fully reliable |
 | osname   | TEXT    | missing in log.txt             |                             |
 | osvers   | TEXT    | missing in log.txt             |                             |
 | fulldate | TEXT    | same but seconds cut off       | yes (at least very likely)  |
 | type     | INTEGER | missing in log.txt             |                             |

So we must make up a compound key that is uniq most of the time but
sometimes not. The key is comprised of the columns that we considered
useful above. The new table shall just collect compound keys and
matching guids and a third column tells the source of the guid. Then
we can find 1:1 mappings and N:M mappings. A bug is a N:M mapping with
N!=M.

I believe we're better off when refill-cpanstatsdb.pl logs what it
does.

=head1 TODO


=head1 AUTHOR

=cut

use strict;
use File::Basename ();
use File::Path ();
use File::ReadBackwards ();
use FindBin ();
use Getopt::Long;
use Pod::Usage qw(pod2usage);
use LWP::UserAgent ();
use YAML::Syck ();
use Time::HiRes qw(sleep time);
use JSON::XS;
use Time::Piece;
sub mydie ($);
sub mylog ($);

my @opt = $optpod =~ /B<--(\S+)>/g;
our %Opt;
GetOptions
    (
     \%Opt,
     @opt,
    ) or pod2usage(1);

pod2usage(0) if $Opt{help};

sub self_mtime () {
    my(@stat) = stat __FILE__;
    $stat[9];
}

sub mydie ($) {
    my($mess) = @_;
    mylog $mess;
    warn $mess;
    exit;
}

my $storefile = $Opt{storefile} ||= "metabase-log.txt";
my $proclogfile = "$storefile-proc.log";
File::Path::mkpath File::Basename::dirname $storefile;
my($cached_time,$cached_ts) = (0,"");

sub mylog ($) {
    my($message) = @_;
    open my $fh, ">>", $proclogfile or die "Could not open >> '$proclogfile': $!";
    $message =~ s/\s*\z/\n/;
    if ( time != $cached_time ) {
        $cached_time = time;
        my @t = gmtime;
        $t[5]+=1900;
        $t[4]++;
        $cached_ts = sprintf "%04d%02d%02dT%02d%02d%02d", @t[5,4,3,2,1,0];
    }
    print $fh "$cached_ts:$message";
}

my $url = "http://metabase.cpantesters.org/tail/log.txt";
my $fetchfile = "$storefile.mirrorer";
my $lockfile = "$fetchfile.LCK";
my $self_mtime = self_mtime;
my $tltj_statusfile = "$ENV{HOME}/var/metabase-log/log-as-json.status";

sub tltj_status {
    # we are confident that we can read the TLTJ status file now without
    # locking because we govern the call to TLTJ;
    my $json = JSON::XS->new->pretty(1)->utf8(1)->indent(1)->space_before(1)->space_after(0);
    my $status_content = do { open my $fh, $tltj_statusfile or mydie "could not open: $!"; local $/; <$fh>};
    my $status;
    if ($status_content) {
        $status = eval { $json->decode($status_content) };
        if (!$status || $@) {
            my $err = $@ || "unknown error";
            mydie "Could not parse $tltj_statusfile, cannot continue: $err";
        }
    } else {
        $status = {};
    }
    mylog sprintf "Info[%s]: Found tltj status tell value: '%s', size storefile %s", $$, $status->{tell}, -s $storefile;
    $status;
}

use Fcntl qw( :flock :seek O_RDONLY O_RDWR O_CREAT );
my $lfh;
unless (open $lfh, "+<", $lockfile) {
    unless ( open $lfh, ">>", $lockfile ) {
        mylog "ALERT: Could not open >> '$lockfile': $!";
        die;
    }
    unless ( open $lfh, "+<", $lockfile ) {
        mylog "ALERT: Could not open +< '$lockfile': $!";
        die;
    }
}
if (flock $lfh, LOCK_EX|LOCK_NB) {
    mylog "Info[$$]: Got the lock, continuing";
} else {
    mylog "FATAL[$$]: lockfile '$lockfile' locked by a different process; cannot continue";
    exit;
}
my $tltj = tltj_status();
my $tell = $tltj->{tell};
my $can_continue = $tell == -s $storefile;
unless ($can_continue) {
    require File::ReadBackwards;
    my $bw = File::ReadBackwards->new( $storefile ) or
        mydie "can't read '$storefile': $!" ;
    while( defined( my $log_line = $bw->readline ) ) {
        if ($log_line eq "...\n") {
            $tell += 4;
        } else {
            last;
        }
    }
}
$can_continue = $tell == -s $storefile;
if ($tell == -s $storefile) {
    # we do not lock the status file because the whole system only
    # works if neither $storefile gets altered nor the statusfile; but
    # we do have the $lfh on the other lockfile and that should
    # guarantee standstill; and should $tell ever not be equal to the
    # size of the $storefile, then we should not get past this point.
    my $t = localtime;
    $t->time_separator("");
    $t->date_separator("");
    my $ts = sprintf "%sz", $t->datetime;
    unless (rename $storefile, "$storefile-$ts") {
        mydie "Could not rename $storefile to $storefile-$ts: $!";
    }
    mylog("Info: Renamed $storefile to $storefile-$ts");
    unless (0 == system "tail -n 10000 $storefile-$ts > $storefile") {
        mydie "Could not rewrite $storefile";
    }
    mylog("Info: Left-truncated $storefile");
    my $json = JSON::XS->new->pretty(1)->utf8(1)->indent(1)->space_before(1)->space_after(0);
    my $status = {
        proc => $$,
        time => time,
        tell => -s $storefile,
    };
    my $sfh;
    unless (open $sfh, ">", "$tltj_statusfile.new") {
        mydie "Could not open > $tltj_statusfile.new: $!";
    }
    print $sfh $json->encode($status);
    close $sfh
        or mydie "Could not close $tltj_statusfile.new: $!";
    rename "$tltj_statusfile.new", $tltj_statusfile or mydie "Could not rename: $!";
} else {
    mydie sprintf "FATAL: tell from %s and size of %s must be equal, but: tell='%s', storefilesize '%s'",
        $tltj_statusfile, $storefile, $tell, -s $storefile;
}
my $ua = LWP::UserAgent->new;
my @old;
if (-e $storefile) {
    my $bw = File::ReadBackwards->new($storefile);
    unless ( $bw ) {
        mylog("Could not read backwards '$storefile': $!");
        die;
    }
    my $cnt = 0;
    while (defined(my $line = $bw->readline)) {
        last if ++$cnt >= 2001; # arbitrary
        push @old, $line;
    }
}
my $time_to_leave = time + 21600; # 86400; # arbitrary
while () {
    if ( self_mtime > $self_mtime ) {
        mylog(sprintf "%s has been updated, good bye", __FILE__);
        last;
    } elsif (time > $time_to_leave) {
        mylog(sprintf "after running for that long, good bye");
        last;
    }
    my $t = time;
    my $time_to_repeat = $t+15; # arbitrary
    #$ua->mirror($url, $fetchfile); # fyi: if this fails, leaves a $fetchfile-$$ behind
    # XXX srt - quickfix against varnish cache
    $ua->mirror($url."?".time(), $fetchfile); # fyi: if this fails, leaves a $fetchfile-$$ behind
    if (open my $fh, $fetchfile) {
        # [2010-10-29T21:05:10Z] [Chris Williams (BINGOS)] [pass] [RIZEN/Chat-Envolve-0.0100.tar.gz] [i386-dragonfly-64int] [perl-v5.10.0] [373b43ee-e3a0-11df-9e2d-9e9e6e8696e0] [2010-10-29T21:05:10Z]
        my @qr = (qr/\[([^\]]*)\]/) x 8;
        local $" = " ";
        my %oldseen = map {($_ => 1)} @old; # @old has all lines
                                            # (unsorted) that were in
                                            # the previous iteration
        my %seen;
        my @new;
        my $cnt = 0;
        my $overlap = 0;
    LINE: while (<$fh>) {
            my($date,$author,$result,$path,$arch,$perl,$uuid,$ts) = my @f = /@qr/ or next;
            $cnt++;
            $seen{$_}++; # %seen contains all current ones
            if (exists $oldseen{$_}) {
                $overlap = 1;
                next LINE;
            }
            push @new, $_; # @new contains only lines that we did not
                           # have in @old
        }
        unless ($overlap) {
            my $fh;
            unless ( open $fh, ">>", $storefile ) {
                mylog "Could not open >>'$storefile': $!";
            }
            print $fh "...\n";
        }
        mylog sprintf "INFO[%s]: Lines read: %d, Newlines read: %d", $$, $cnt, scalar @new;
        my $newlines_written = 0;
        if (@new) {
            my $fh;
            unless ( open $fh, ">>", $storefile ) {
                mylog "Could not open >>'$storefile': $!";
            }
            while (@new) {
                my $line = pop @new;
                print $fh $line;
                # mylog "WRITTEN: $line"; # very noisy
                $newlines_written++;
            }
            close $fh or mylog "Could not close >>'$storefile': $!";
            0 == system "$^X $ENV{HOME}/src/cpan-testers-matrix/bin/tail-log-to-json.pl -o $ENV{HOME}/var/metabase-log/log-as-json -logfile $ENV{HOME}/var/metabase-log/log-as-json.log -statusfile $tltj_statusfile $ENV{HOME}/var/metabase-log/metabase.log 2>$ENV{HOME}/var/metabase-log/log-as-json.err" or mylog("ALERT: error running tail-log-to-json");
        }
        @old = keys %seen;
        mylog sprintf "INFO[%s]: Newlines written: %d, Keeping in mem records: %d", $$, $newlines_written, scalar @old;
    }
    last if $Opt{once};
    if ( my @stat = stat $fetchfile ) {
        my $keepcalm = $stat[9] + 30; # arbitrary
        $time_to_repeat = $keepcalm if $keepcalm > $time_to_repeat;
        if ( time - $keepcalm > 30 ) {
            $time_to_repeat += 18; # they are late, do not try too often
        }
    } else {
        $time_to_repeat += 18 unless $time_to_repeat > 150; # something unusual has happened
    }
    my $sleep = $time_to_repeat - time;
    if ($sleep >= 1) {
        mylog "INFO[$$]: (sleeping $sleep)";
        sleep $sleep;
    }
}
tltj_status();
