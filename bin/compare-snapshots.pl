#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

 compare-snapshots.pl file file

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Reads two snapshot files and outputs differing modules.

For the record, snapshot files are generated within the CPAN shell by
running C<autobundle>, frequently with

  echo autobundle | cpan

=head1 SEE ALSO

configdiff.pl

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);
use List::MoreUtils qw(uniq);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

my($lfile,$rfile) = @ARGV;
pod2usage(1) unless $rfile;
my $left  = {};
my $right = {};
for my $tuple ([$left,$lfile],
               [$right,$rfile],
              ) {
    my($snap,$file) = @$tuple;
    open my $fh, $file or die "Could not open '$file': $!";
    my $in_list = 0;
  SNAPLINE: while (<$fh>) {
        if (/^=head1\sCONTENTS$/) {
            $in_list = 1;
            next SNAPLINE;
        } elsif (/^=head1\s/) {
            $in_list = 0;
        }
        next SNAPLINE unless $in_list;
        next SNAPLINE if /^\s*$/;
        my($mod,$v) = split " ", $_;
        $snap->{$mod} = $v;
    }
}
my(@miss_left,@miss_right,@diff_version);
my $c1 = 0;
for my $m (uniq keys %$left, keys %$right) {
    $c1 = length $m if length $m > $c1;
    if (! exists $right->{$m}) {
        push @miss_right, [$m, $left->{$m}];
        next;
    } elsif (! exists $left->{$m}) {
        push @miss_left, [$m, $right->{$m}];
        next;
    }
    next if $left->{$m} eq $right->{$m};
    push @diff_version, [$m, $left->{$m}, $right->{$m}];
}
my $c2 = 12;
for my $tuple (sort {$a->[0] cmp $b->[0]} @miss_right) {
    printf "%${c1}s %${c2}s =>\n", @$tuple;
}
for my $tuple (sort {$a->[0] cmp $b->[0]} @miss_left) {
    while (length $tuple->[0] > $c1) {
        (my $trim,$tuple->[0]) = unpack("a" . $c1 . "a*", $tuple->[0]);
        print "$trim\n";
    }
    printf "%${c1}s %${c2}s => %${c2}s\n", $tuple->[0], "", $tuple->[1];
}
for my $tuple (sort {$a->[0] cmp $b->[0]} @diff_version) {
    printf "%${c1}s %${c2}s => %${c2}s\n", @$tuple;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
