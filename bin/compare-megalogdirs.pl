#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

 compare-megalogdirs.pl dir-or-logfile dir-or-logfile
 compare-megalogdirs.pl glob

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--annofile=s>

Path to annofile.

=item B<--bbc!>

Only show OK => NOT OK

=item B<--debugcolorout!>

if colorout-to-dir prg is to be run, call it with -d

=item B<--help|h!>

This help

=item B<--skipanno!>

Skip things that have a line in annotated.txt

=item B<--times!>

Compare runtimes, not status. Limit yourself to OK=>OK.

=back

=head1 DESCRIPTION

Reads two megalog directories and outputs modules with differing
status. Instead of directory names also the lognames may be given, in
this case the directories will be created if missing.

-or-

if the --times option is given, outputs times only on OK=>OK pairs.

=head1 SEE ALSO

colorout-to-dir-3.pl

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use CPAN::DistnameInfo;
use Dumpvalue;
use File::Basename qw(basename dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);
use List::MoreUtils qw(uniq);
use Pod::Usage;
use YAML::Syck ();

our %Opt;
lock_keys %Opt, map { /([^\=\!\|]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(1);
}

$Opt{annofile} ||= "/home/k/sources/CPAN/andk-cpan-tools/annotate.txt";

my(%anno, %olderanno);
if ($Opt{skipanno}) {
    my $annofile = $Opt{annofile};
    open my $fh, $annofile or die "could not open '$annofile': $!";
    while (<$fh>) {
        my($dist,$anno) = /^(\S+)\s+(.*)/;
        $anno{$dist} = $anno;
        $dist =~ s/_//g;
        $anno{$dist} = $anno; # prevent DCONWAY/List-Maker-0.003_000.tar.gz to appear
        my $stem = $dist; # should we use Distnameinfo?
        $stem =~ s/-\d[\d\._]*(-TRIAL)?//;
        $olderanno{$stem} = $dist;
    }
}

my($ldir,$rdir,@rest) = @ARGV;
my($lperlref,$rperlref)=(\my $lperl,\my $rperl);
if (@rest) {
    die "Got more than two arguments: rejecting @rest";
}
if ($ldir && !$rdir && $ldir =~ /\{/) {
    my($gldir,$grdir,@grest) = glob $ldir;
    if (@grest) {
        die "Your glob '$ldir' matches more than two: $gldir $grdir; rejecting: @grest";
    }
    unless ($gldir){
        die "Your glob '$ldir' did not match anything, giving up";
    }
    unless ($grdir){
        die "Your glob '$ldir' matched only one thing: $gldir, giving up";
    }
    $ldir = $gldir;
    $rdir = $grdir;
}
unless (defined $rdir){
    warn "Usage error: Missing second argument\n";
    pod2usage(1);
}
for ($ldir,$rdir) {
    my $log = $_;
    if (s/\.(?:log)?$/.d/) { # allow both xxx.log and xxx. instead of xxx.d
        my($dir) = $_;
        if (-d $dir) {
            warn "directory '$dir' exists already, assuming I shall leave it as it is";
        } else {
            my @system = $^X;
            if ($Opt{debugcolorout}) {
                push @system, "-d";
            }
            my($name,$path,$suffix) = File::Basename::fileparse($0,".pl");
            $path .= "colorout-to-dir-3.pl";
            push @system, $path, "--html", $log;
            warn "running system[@system]";
            0==system @system or die "problem running '@system'";
        }
    }
}
my $left  = {};
my $right = {};
my $canon = {};
my $base = {};
my $superview = {};
for my $tuple (["left",$left,$ldir,$lperlref],
               ["right",$right,$rdir,$rperlref],
              ) {
    my($side,$snap,$dir,$tperlref) = @$tuple;
    $superview->{"$side-dir"} = $dir;
    opendir my $dh, $dir or die "Could not opendir '$dir': $!";
    my $in_list = 0;
    my $count = 0;
  DIRENT: for my $dirent (readdir $dh) {
        next DIRENT unless my($author,$distpath) = $dirent =~ /(.+?)!(.+)\.html$/;
        $count++;
        $distpath =~ s:!:/:g;
        my $dist = basename($distpath);
        if ($Opt{skipanno} and $anno{$dist}) {
            next DIRENT;
        }
        open my $fh, "$dir/$dirent" or die "could not open $dir/$dirent: $!";
        my $status;
        my $canon1;
        my $cputime;
    LINE: while (<$fh>) {
            if (m|<h2>Status:\s+(.+)</h2>$|){$status = $1;}
            elsif (m|<h2>Distro:\s+(.+)</h2>$|){$canon1 = $1;}
            elsif (!$$tperlref and m|<h2>Perl:\s+.+?/installed-perls/(.+)</h2>$|){$$tperlref ||= $1;}
            elsif (m|=\s+(\S+)\s+CPU\s*\)$|){$cputime = $1;}
            if ($Opt{times}) {
                # 1 wallclock secs ( 0.02 usr  0.00 sys +  0.22 cusr  0.00 csys =  0.24 CPU)
                # =  0.29 CPU)
                last LINE if $cputime;
            } else {
                last LINE if $status && $canon1;
            }
        }
        close $fh;
        $superview->{"$side-$status"}++;
        next DIRENT if $status eq "RESIDUUM";
        if ($distpath =~ m|[=']|) {
            warn "Skipping suspicious file '$distpath' in '$dir'";
            next DIRENT;
        }
        for ("$author/$distpath") {
            $base->{$_} = $dist;
            $canon->{$_} = $canon1;
            if ($Opt{times}) {
                if ($cputime) {
                    $snap->{$_} = "$status $cputime";
                }
            } else {
                $snap->{$_} = $status;
            }
        }
    }
    $superview->{"$side-perl"} = $$tperlref;
    if ($count == 0) {
        die "Alert: NO html files found in '$dir'";
    }
}
my(@miss_left,@miss_right,@diff,@bothok);
my $c1 = 0;
DISTRO: for my $m (uniq keys %$left, keys %$right) { # $m = "RWSTAUNER/File-Spec-Native-1.003"
    $c1 = length $m if length $m > $c1;
    if ($Opt{times}) {
        next DISTRO unless $left->{$m} && $left->{$m} =~ /^OK/ && $right->{$m} && $right->{$m} =~ /^OK/;
        push @bothok, [$m, $left->{$m}, $right->{$m}];
    } else {
        if (! exists $right->{$m}) {
            push @miss_right, [$m, $left->{$m}];
            next DISTRO;
        } elsif (! exists $left->{$m}) {
            push @miss_left, [$m, $right->{$m}];
            next DISTRO;
        }
        next DISTRO if $left->{$m} eq $right->{$m};
        push @diff, [$m, $left->{$m}, $right->{$m}];
    }
}
{
    # in miss_left and miss_right we now have common distributions that
    # got upgraded. We postprocess them now because they should be
    # presented differently.
    my %dist;
    for my $tutu ([left=>\@miss_left],[right=>\@miss_right]) {
        my($leftright,$miss) = @$tutu;
        for (my $i = $#$miss;$i>-1;$i--) {
            my $tuple = $miss->[$i];
            my $path = sprintf "authors/id/%s/%s/%s.tar.gz", substr($tuple->[0],0,1), substr($tuple->[0],0,2), $tuple->[0];
            my $d = CPAN::DistnameInfo->new($path);
            my $dcpanid = $d->cpanid or warn "missing id on path[$path]";
            my $ddist = $d->dist or warn "missing dist on path[$path]";
            my $dist = sprintf "%s/%s", $dcpanid, $ddist;
            $dist{$dist}{$leftright}{arr} = splice @$miss, $i, 1;
            $dist{$dist}{$leftright}{version} = $d->version;
            $dist{$dist}{was}{$leftright}{distv} = sprintf("%s/%s", $dcpanid, $d->distvname);
        }
    }
    for my $dist (keys %dist) {
        no warnings 'uninitialized';
        push @diff, [exists $dist{$dist}{was}{left} ? $dist{$dist}{was}{left}{distv} : $dist,
                     # miss right = have left:
                     "$dist{$dist}{right}{version} $dist{$dist}{right}{arr}[1]",
                     "$dist{$dist}{left}{version} $dist{$dist}{left}{arr}[1]",
                     $dist{$dist},
                    ];
    }
}
my $c2 = 15;
unless ($Opt{bbc}) {
    #warn "Missing right\n";
    for my $tuple (sort {$a->[0] cmp $b->[0]} @miss_right) {
        printf "%${c1}s %${c2}s =>\n", @$tuple;
    }
    #warn "Missing left\n";
    for my $tuple (sort {$a->[0] cmp $b->[0]} @miss_left) {
        #while (length $tuple->[0] > $c1) {
        #    (my $trim,$tuple->[0]) = unpack("a" . $c1 . "a*", $tuple->[0]);
        #    print "$trim\n";
        #}
        printf "%${c1}s %${c2}s => %-${c2}s\n", $tuple->[0], "", $tuple->[1];
    }
}
#warn "Diffing left right\n";
print YAML::Syck::Dump
    ({
      ldir => $ldir,
      lperl => $lperl,
      rdir => $rdir,
      rperl => $rperl,
     });
my $D = [];
for my $tuple (sort {lc($base->{$a->[0]}||"") cmp lc($base->{$b->[0]}||"")} @diff) {
    my $matrx = "";
    my $lpath = "";
    my $distv = $base->{$tuple->[0]};
    if ($tuple->[1] =~ /^(OK|.*[^T] OK)$/ and $tuple->[2] =~ /NOT OK$/) {
        my $bang = $tuple->[0];
        $bang =~ s{/}{!}g;
        $matrx = sprintf "http://matrix.cpantesters.org/?dist=%s", $distv;
        $lpath = sprintf "%s/%s.html", $rdir, $bang;
    }
    if ($Opt{bbc} && $matrx) {
        my $path = sprintf "authors/id/%s/%s/%s.tar.gz", substr($tuple->[0],0,1), substr($tuple->[0],0,2), $tuple->[0];
        my $d = CPAN::DistnameInfo->new($path);
        my $stem = $d->dist;
        my $audis;
        if (@$tuple >= 4) {
            $audis = sprintf "%s <= %s", $tuple->[0], $tuple->[3]{right}{arr}[0];
        } else {
            $audis = $tuple->[0];
        }
        my $dump = {
                    audis => $audis,
                    canon => $canon->{$tuple->[0]},
                    distv => $distv,
                    lpath => $lpath,
                    matrx => $matrx,
                    stem  => $stem,
                   };
        if ($Opt{skipanno}) {
            if (my $olderanno = $olderanno{$stem}) {
                $dump->{olderanno} = $olderanno;
            }
        }
        push @$D, $dump;
        print YAML::Syck::Dump($dump);
    } elsif (!$Opt{bbc}) {
        printf "%${c1}s %${c2}s => %-${c2}s%s\n", @$tuple, $matrx;
    }
}
if ($Opt{bbc}) {
    print YAML::Syck::Dump({
        count=>scalar(@$D),
        stems=>join(",",map{$D->[$_]{stem}}0..$#$D),
    });
}
for my $tuple (sort {lc($base->{$a->[0]}||"") cmp lc($base->{$b->[0]}||"")} @bothok) {
        printf "BOTHOK: %${c1}s %${c2}s => %-${c2}s\n", @$tuple;
}
for my $some_ok ("OK", "NOT OK") {
    for my $lr (qw(left right)) {
        $superview->{"$lr-either-ok-or-not-ok"} += $superview->{"$lr-$some_ok"};
    }
}
$superview->{summary} = sprintf "l/r => %.4f, r/l => %.4f",
    $superview->{"left-either-ok-or-not-ok"}/$superview->{"right-either-ok-or-not-ok"},
    $superview->{"right-either-ok-or-not-ok"}/$superview->{"left-either-ok-or-not-ok"};
print YAML::Syck::Dump($superview);
# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
