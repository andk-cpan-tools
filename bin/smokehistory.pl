#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--cleanup!>

Be prepared for interactivity without an end. Nothing will be cleaned
up without asking. See also --interactive.

=item B<--dir=s>

defaults to C</home/sand/var/ctr/done/archive/>

=item B<--filter=s>

Only show reports for this distro. Exact distroname required
(like C<Imager-Graph-0.09>). E.g.:

  smokehistory.pl --numversions=1 --verbose --filter Test-Simple-1.301001026

=item B<--help|h!>

This help

=item B<--interactive|i!>

Defaults to true. If set to false (--nointeractive or --noi), you can
remove hundreds of thousands of file without being asked a single
question.

=item B<--delete-max=i>

Maximum number of files we want to see removed

=item B<--mixedtoyaml=s>

Writes a yaml file containing distros and reports where the reports
are mixed. That's because the next step will have to parse the reports
and may need a lot more focus and attention, so it makes sense to get
that stuff separated out.

=item B<--numversions=i>

Defaults to 2 which means look at the two most recent versions for
whatever we're doing. Everything older is discarded considered
outdated.

=item B<--skipna!>

Skip both NA and unknown reports.

=item B<--verbose!>

write one line per report found

=back

=head1 DESCRIPTION

Walk through all reports we have generated and makes simple
statistics. Or remove old reports (with --cleanup) for the distros
that have got newer releases.

The initial version was only for cleanup and we called it like

  on k85:
  sudo -u sand /path/to/perl bin/smokehistory.pl --numversions=3 --cleanup --noi

because we wanted to keep 3 versions and delete the older stuff and
sudo was needed because the files belong to sand.

The second usage was for finding BBC candidates. For that we wrote
smokehistoryfocus and combined

  perl bin/smokehistory.pl --numversions=1 --mixedtoyaml=bin/smokehistory.pl.yml --skipna

with

  perl bin/smokehistoryfocus.pl --bbc --skipanno --minperl 5.015004

numversions=1 drops all distros which are not the most recent release.

skipna only takes reports that are either pass or fail.

mixedtoyaml write those to a yaml file that promise some sort of mix
in the results

bbc picks those that had 100% OK in a previous period and went to less.

skipanno ignores the stuff we have in annotate.txt

=head1 BUGS

linux hardcoded. If this shall be shared with non-linux results, reconsider.

=head1 SEE ALSO

smokehistoryfocus.pl

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use List::MoreUtils qw(uniq);
use List::Util qw(first);
use Term::Prompt qw(prompt);
use Text::Format;
use YAML::Syck;
use version;

our %Opt;
lock_keys %Opt, map { /([^=!\|]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{dir} ||= "/home/sand/var/ctr/done/archive";
$Opt{interactive} = 1 unless defined $Opt{interactive};
$Opt{numversions} ||= 2;

opendir my $dh, $Opt{dir} or die;
my %S; # for the summary at the end
my $Y; # for the yaml option
my $tf = Text::Format->new(firstIndent=>0);
my $total_deleted = 0;
$|=1;
LETTERDIR: for my $dirent (sort readdir $dh) { # A B C
    next LETTERDIR unless $dirent =~ /^[A-Za-z]$/;
    next LETTERDIR if $Opt{filter} && substr($Opt{filter},0,1) ne $dirent;
    opendir my $dh2, "$Opt{dir}/$dirent" or die;
 DISTRO: for my $dirent2 (readdir $dh2) { # FvwmPiazza CGI LWP-UserAgent-WithCache
        next DISTRO if $dirent2 =~ /^\.\.?$/;
        next DISTRO if $Opt{filter} && substr($Opt{filter},0,length($dirent2)) ne $dirent2;
        # we get A-B here even if we asked for A-B-C-42
        my $distro = $dirent2;
        my $cldir = "$Opt{dir}/$dirent/$dirent2/";
        opendir my $dh3, "$Opt{dir}/$dirent/$dirent2" or die;
        my %vdistro;
        my %result;
        my %report;
    REPORT: for my $dirent3 (readdir $dh3) { # pass.FvwmPiazza-0.2002.x86_64-linux-thread-multi-ld.2.6.32-5-xen-amd64.1300583966.24270.rpt
            next REPORT if $dirent3 =~ /^\.\.?$/;
            next REPORT unless $dirent3 =~ /\.rpt$/;
            my($result,$vdistro,$archname,$osversion,$time) =
                $dirent3 =~ m{(\w+)\.                 # pass
                              (\S+)\.                 # Dist-Zilla-4.300039
                              ([^\.]+(?:linux|freebsd)[^\.]*)\.
                                                      # i686-linux-thread-multi-64int-ld
                              (\d\S+)\.               # 3.10-3-amd64
                              (\d+\.\d+)              # 1379556685.18399
                              \.rpt$
                         }x;
            die "could not parse '$dirent3' from directory '$dirent2' into its constituents" unless $vdistro;
            # warn "vdistro[$vdistro]";
            next REPORT if $Opt{filter} && $Opt{filter} ne $vdistro;
            if ($Opt{skipna} && $result =~ /(unknown|na)/) {
                next REPORT;
            }
            if ($Opt{verbose}) {
                require POSIX;
                my($ts) = POSIX::strftime("%FT%T", localtime $time);
                my $file = "$Opt{dir}/$dirent/$dirent2/$dirent3";
                open my $fh, $file or die;
                my($perl,$host);
                while (<$fh>) {
                    my $X;
                    if (/^\s+\$\^X\s*=\s*(.+)/){
                        $X = $1;
                    } elsif (/config_args=.*-Dprefix=(\S+)/) {
                        # config_args='-Dprefix=/home/src/perl/repoperls/installed-perls/perl/v5.17.4-160-g599f1ac/a2da -Dmyhostname=k83 -Dinstallusrbinperl=n -Uversiononly -Dusedevel -des -Ui_db -Duseithreads -Duselongdouble -DDEBUGGING=-g'
                        $X = $1;
                    }
                    if ($X and $X =~ m{installed-perls/(perl|host/[^/]+)/(.+)}) {
                        $host = $1;
                        $perl = $2;
                        $perl =~ s|/bin/perl||;
                        if ($host eq "perl") {
                            $host = "";
                        } else {
                            $host =~ s|host/||;
                        }
                        last;
                    }
                }
                unless (defined $perl) {
                    $perl = "<undef>";
                    warn sprintf "Found no perl in %s\n", $file;
                }
                printf "%-6s %s %s%s\n", $result, $ts, $perl, $host ? " ($host)" : "";
            }
            my $r = $report{$vdistro} ||= [];
            push @$r, $dirent3;
            $vdistro{$vdistro} = 1;
            $result{$vdistro}{$result}++;
            $S{results}++;
        }
        my @vdistro = keys %vdistro;
        my %type;
    VDISTRO: for my $vdistro (@vdistro) {
            my $type;
            my @results = keys %{$result{$vdistro}};
            if (1 == @results) {
                if (1 == @vdistro) {
                    # only one release tested, always same result
                    $S{one_release_boring}++;
                    next DISTRO unless $Opt{verbose};
                }
                $type = $results[0];
            } else {
                $type = "mix";
            }
            $type{$vdistro} = $type;
        }
        my %version;
        for my $vdistro (keys %type) {
            my($distro,$version,$mod) = $vdistro =~ /(\S+)-(v?\d+(?:[\.\d]*[^-]*))(-(?:TRIAL|withoutworldwriteables|fix))?$/;
            unless (defined $version) {
                # Angel102
                ($distro,$version) = $vdistro =~ /(\D+)(\d+)/;
            }
            unless (defined $version){
                die "could not parse version from $vdistro";
            }
            $version=~s/[^\d\.]+$//;
            1 while $version=~s/([\d\.])[^\d\.]+.*/$1/;
            $version=~s/\.$//;
            # print "DEBUG: parse version[$version] of distro[$distro](vdistro[$vdistro])\n";
            my $numversion = eval {version->new($version)->numify};
            if (not defined $numversion) {
                die "ERROR: Could not parse version[$version] of distro[$distro](vdistro[$vdistro]): $@";
            } elsif ($@) {
                die "Panic: we have a \$\@[$@] but a numversion[$numversion] too";
            }
            #if ($numversion==1) {
            #    warn "numversion[$numversion] version[$version] vdistro[$vdistro]";
            #    $DB::single=1;
            #}
            $version{$vdistro} = $numversion;
        }
        #$DB::single = $distro eq "savelogs";
        my @vdistros_sorted_by_version = sort { $version{$b} <=> $version{$a} } keys %type;
        while (@vdistros_sorted_by_version > $Opt{numversions}) {
            if ($Opt{cleanup}) {
                my @cldirent = @{$report{$vdistros_sorted_by_version[-1]}};
                my $report = sprintf
                    ("==> Having reports from
 %d versions of
 %s in directory
 %s. The oldest of those,
 %s, has
 %d reports. We could remove:\n\n",
                     scalar @vdistros_sorted_by_version,
                     $distro,
                     $cldir,
                     $vdistros_sorted_by_version[-1],
                     scalar @cldirent,
                    );
                print $tf->format($report);
                warn map { "$_\n" } @cldirent;
                $S{could_delete_reports} += @{$report{$vdistros_sorted_by_version[-1]}};
                my $answer;
                if ($Opt{interactive}) {
                    $answer = lc prompt "x", "Shall I delete? (y/n/q)", "", "y";
                } else {
                    $answer = 'y';
                }
                if ($answer eq "q") {
                    last LETTERDIR;
                } elsif ($answer eq "n") {
                } elsif ($answer eq "y") {
                    if ($Opt{"delete-max"}) {
                        my $will_delete = @cldirent;
                        if ($total_deleted + $will_delete > $Opt{"delete-max"}) {
                            die "Delete operation would delete $will_delete files, stopping now due delete-max option. Already deleted: $total_deleted\n";
                        }
                    }
                    my $deleted = unlink map { "$cldir$_" } @cldirent or die "Could not unlink: $!";
                    $total_deleted += $deleted;
                    if ($Opt{interactive}) {
                        warn "Deleted $deleted reports. Press ENTER to continue\n";
                        <>;
                    }
                }
            }
            my $pop = pop @vdistros_sorted_by_version;
            delete $type{$pop};
            $S{outdated}++;
        }
        my @types = uniq values %type;
        if (! first { $_ eq "mix" } @types){
            $S{n_releases_boring}++;
            next DISTRO unless $Opt{verbose};
        }
        for my $vdistro (sort keys %type) {
            my $type = $type{$vdistro};
            printf "%-50s %s\n", $vdistro, $type;
            if ($Opt{mixedtoyaml}) {
                $Y->{$vdistro} = [map { "$cldir$_" } @{$report{$vdistro}}];
            }
            $S{wrote}++;
        }
    }
}
warn YAML::Syck::Dump \%S;
if (my $file = $Opt{mixedtoyaml}) {
    YAML::Syck::DumpFile $file, $Y;
    warn "Wrote yaml to '$file'\n";
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
