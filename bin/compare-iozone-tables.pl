#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

 compare-iozone-tables file1 file2 ...

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

The two or more files are compared cell by cell.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

our $S;
my $longest_table_name = 0;
for my $i (0..$#ARGV) {
    my($file) = $ARGV[$i];
    open my $fh, $file or die "Could not open '$file': $!";
    my $in_excel = 0;
    my $curtable = "";
    my @columns = ();
 LINE: while (<$fh>) {
        if (/Excel output is below/) {
            $in_excel = 1;
            next LINE;
        }
        if (/^"(.+?)"$/) { # "Writer report"
            $curtable = $1;
            $curtable =~ s/\sreport$//;
            $curtable =~ s/\s+/_/g;
            if (length $curtable > $longest_table_name) {
                $longest_table_name = length $curtable;
            }
            next LINE;
        } elsif (/^\s+(".+"$)/) { #         "4"  "8"  "16"  "32" ...
            @columns = map { /"(\d+)"/ } split " ", $1;
            next LINE;
        } elsif (/^"(.+?)"\s+([\d\s]+)$/) { # "2048"   219  358  365  347  ...
            my($curline) = $1;
            my(@curval) = split " ", $2;
            for my $j (0..$#curval) {
                $S->{$curtable}{$curline}{$columns[$j]}[$i] = $curval[$j];
            }
        }
    }
}

for my $table (sort keys %$S) {
    my $v = $S->{$table};
    my @tablesum;
    for my $line (sort {$a <=> $b} keys %$v) {
        my $v2 = $v->{$line};
        for my $col (sort {$a <=> $b} keys %$v2) {
            my $val = $v2->{$col};
            my $line = sprintf "%-*s %6d %5d", $longest_table_name, $table, $line, $col;
            for my $i (0..$#$val) {
                if ($i == 0) {
                    $line .= sprintf " %7d", $val->[$i];
                } else {
                    my $growth;
                    if ($val->[$i-1] > 0) {
                        $growth = sprintf " %4d%%", 100 * $val->[$i] / $val->[$i-1] - 100;
                    } else {
                        $growth = "N/A";
                    }
                    $line .= sprintf " %8s %7d", $growth, $val->[$i];
                }
                $tablesum[$i] += $val->[$i];
            }
            print "$line\n";
        }
    }
    my $line = sprintf "tablesum %-*s", $longest_table_name, $table;
    for my $i (0..$#tablesum) {
        if ($i == 0) {
            $line .= sprintf " %8d", $tablesum[$i];
        } else {
            my $growth;
            if ($tablesum[$i-1] > 0) {
                $growth = sprintf " %4d%%", 100 * $tablesum[$i] / $tablesum[$i-1] - 100;
            } else {
                $growth = "N/A";
            }
            $line .= sprintf " %8s %8d", $growth, $tablesum[$i];
        }
    }
    print "$line\n";
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
