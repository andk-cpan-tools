#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--skipquestions!>

Only does the conversions that work without doubt.

=back

=head1 DESCRIPTION

We have plenty of records in quickaccess/distcontext that have
greenish=3 although they are not greenish anymore because there is a
newer release.

Found a version C<.53b>.

Found a distro that contained UTF-8 in the name:

  0000000 64 69 73 74 76 5b e2 80 8b 51 75 64 6f 2d e2 80  >distv[�..Qudo-�.<
  0000020 8b 30 2e 30 31 5f 30 32 2d 5d 20 3d 3e 0a 20 64  >.0.01_02-] =>. d<

That's ZERO WIDTH SPACE, of course.

Found Spreadsheet::WriteExcel::WebPivot in
NATHANL/Spreadsheet-WriteExcel-WebPivot2.tar.gz which is
misinterpreted by all of metacpan and search and matrix as an early
version of Spreadsheet::WriteExcel. Since the module is released 2005
it's probably a good idea to keep this as the others did and let
history mull over it forever:(

Found C<makepp-1.50-cvs-070506>.

Found C<MegaDistro-0.02-5>, C<Konstrukt-0.5-beta6>, C<pfacter-1.13-1>,
C<Data-Dump-Streamer-2.05-36>, C<Data-Path-1.3.fix_missing_build_require>

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use YAML::Syck;

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

our $SIGNAL = 0;
$SIG{INT} = $SIG{TERM} = sub { my $sig = shift; warn "Caught $sig\n"; $SIGNAL=1 };

use DBI;
use Time::HiRes qw(sleep time);
use CPAN::Blame::Config::Cnntp;
my($workdir,$cpan_home,$ext_src);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
    $cpan_home = $CPAN::Blame::Config::Cnntp::Config->{cpan_home};
    $ext_src = $CPAN::Blame::Config::Cnntp::Config->{ext_src};
}
my $sldbi = DBI->connect ("dbi:SQLite:dbname=$workdir/quickaccess.db"); # return a dbi handle

my $slsth = $sldbi->prepare("SELECT count(distv) from distcontext where dist is null");
$slsth->execute();
my($slcnt) = $slsth->fetchrow_array();
warn "records without dist: $slcnt\n";

my $selsth = $sldbi->prepare("SELECT distv,yaml from distcontext where dist is null");
my $updsth = $sldbi->prepare("UPDATE distcontext set dist=? where distv=?");
$selsth->execute();
$|=1;
my $dumper = Dumpvalue->new(unctrl => "unctrl", quoteHighBit => 1);
REC: while (my($distv,$yaml) = $selsth->fetchrow_array) {
    my $y = YAML::Syck::Load $yaml;
    die "distv[$distv] has no dist???" unless $y->{dist};
    my $ans;
    my $quest = "distv[$distv] =>\n dist[$y->{dist}] (Y/n/q) ?";
    my $qquest = $dumper->stringify($quest);
    print $qquest;
    if (substr($distv,0,length($y->{dist})) eq $y->{dist}
        and substr($distv,length($y->{dist})) =~ /^-[Vv]?\d+[\d\._]*$/
        and substr($distv,-1) =~ /[0-9]/
       ) {
        $ans = "y";
        print "\n";
        sleep 0.01;
    } else {
        if ($Opt{skipquestions}) {
            next REC;
        }
        $ans = <>;
    }
    $ans = "y" if $ans =~ /^\s*$/;
    if ($ans =~ /^y/i) {
        $updsth->execute($y->{dist}, $distv) or die "Could not update";
    } elsif ($ans =~ /^q/i) {
        last;
    } else {
        warn "Skipping\n";
    }
    last REC if $SIGNAL;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
