#!/usr/bin/perl

#
# $Id: send_tr_reports.pl,v 1.10 2010/09/03 20:48:26 eserte Exp $
# Author: Slaven Rezic
#
# Copyright (C) 2008 Slaven Rezic. All rights reserved.
# This program is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.
#
# Mail: slaven@rezic.de
# WWW:  http://www.rezic.de/eserte/
#

use strict;
use Getopt::Long;
use Test::Reporter;
use Metabase::Resource::cpan::distfile;
use File::Basename;

my $use_mail;
my $opt_transport_uri;
GetOptions("mail"            => \$use_mail,
           "transport-uri=s" => \$opt_transport_uri,
    )
    or die "usage: $0 [--mail] [--transport-uri=TRANSPORT-URI]";

$opt_transport_uri //= 'https://metabase.cpantesters.org/api/v1/';
my $reportdir = shift || "$ENV{HOME}/var/ctr";

my $sync_dir = "$reportdir/sync";
my $done_dir = "$reportdir/done";
my $process_dir = "$reportdir/process";
my $quarantine_dir = "$reportdir/quarantine";

if (!-d $sync_dir) {
    warn "Create $sync_dir and move reports to this directory...";
}
for my $dir ($done_dir, $process_dir, $quarantine_dir) {
    if (!-d $dir) {
        mkdir $dir    or die "While creating $dir: $!";
    }
}

my @files = map { $_->[0] }
    sort { $b->[1] <=> $a->[1] }
    map { [ $_, -M $_ ] }
             glob("$sync_dir/pass.*.rpt"),
             glob("$sync_dir/unknown.*.rpt"),
             glob("$sync_dir/na.*.rpt"),
             glob("$sync_dir/fail.*.rpt"),
;
FILE: for my $filei (0..$#files) {
    my $file = $files[$filei];
    warn "File $file does not exist anymore?", next if !-r $file;
    warn sprintf "[%d/%d;%.4f]%s\n", 1+$filei, scalar @files, -M $file, $file;
    my $quarantine_file = $quarantine_dir . "/" . basename($file);
    if ($file =~ m|/pass\.-0\.01\.|) {
        warn "skipping to avoid a CPAN::DistnameInfo bug (# 72512)";
        rename $file, $quarantine_file or die "Could not rename...: $!";
        next FILE;
    }
    {
        open my $fh, $file or die "Could not open '$file': $!";
        local $/ = "\n";
        my $vdistro;
        while (<$fh>) {
            next unless /X-Test-Reporter-Distfile:\s*(\S+)/;
            $vdistro = $1;
            last;
        }
        if ($vdistro) {
            my $result = eval { Metabase::Resource::cpan::distfile->_validate_distfile($vdistro) };
            unless ($result) {
                warn "skipping to avoid killing the toolchain (see 2012-10-20 in 'upgradeadventures')";
                rename $file, $quarantine_file or die "Could not rename...: $!";
                next FILE;
            }
        }
    }
    my $process_file = $process_dir . "/" . basename($file);
    rename $file, $process_file
	or die "Cannot move $file to $process_file: $!";
    my @tr_args;
    if ($use_mail) {
        die "not supported";
	@tr_args = (from => "srezic\@cpan.org",
		    transport => "Net::SMTP",
		    mx => ["localhost"],
		   );
    } else {
	@tr_args = (transport => 'Metabase',
		    transport_args => [
				       uri => $opt_transport_uri,
				       id_file => '/home/sand/.metabase/id.json',
				      ],
		   );
    }
    my $r = Test::Reporter->new(@tr_args)->read($process_file);
    # XXX fix t::r bug?
    $r->{_subject} =~ s{\n}{}g;
    unless ($r->send) {
        warn sprintf "Alert: renaming '$process_file' to '$file' for further considerations";
        rename $process_file, $file or warn "Could not rename back: $!";
	die "Alert: error while running $^X $0 against $file: " . $r->errstr . ". Stop.\n";
    }
    my $done_file = $done_dir . "/" . basename($file);
    rename $process_file, $done_file
	or die "Cannot move $process_file to $done_file: $!";
}

__END__

=head1 WORKFLOW

See CPAN/CPAN::Reporter configuration below:

The good (non-fail) reports. On the windows machine

    ssh 192.168.1.253
    cd /cygdrive/c/Users/eserte/ctr
    ls sync/* && echo "sync is not empty" || mv *.rpt sync/
    rsync -v -a sync/*.rpt eserte@biokovo:var/ctr/new/ && mv sync/*.rpt done/

On the unix machine

    ctr_good_or_invalid.pl
    send_tr_reports.pl

Now review the fail reports on the windows machine. Invalid ones move
to the invalid/ subdirectory.

=head1 CPAN::REPORTER CONFIGURATION

In /cygdrive/c/Users/eserte/Documents/.cpanreporter/config.ini:

    edit_report=default:no
    email_from=srezic@cpan.org
    send_report=default:yes
    transport=File C:\Users\eserte\ctr

Basically the same configuration can be used for cygwin
~/.cpanreporter/config.ini, just use the cygwin path style for the
transport directory.

    edit_report=default:no
    email_from=srezic@cpan.org
    send_report=default:yes
    transport=File /cygdrive/c/Users/eserte/ctr

=cut

