#!/usr/bin/perl

use Sys::Hostname;
die "not yet ported to ds8143" if hostname eq "ds8143";

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION



=cut

use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

for my $arg (@ARGV) {
    0 == system $^X =>
        (
         "-I/home/k/sources/cpan-testers-parsereport/lib",
         "/home/k/sources/cpan-testers-parsereport/bin/ctgetreports",
         "--ctdb=/home/ftp/cnntp-solver-2009/workdir/cpanstats.db",
         "--pce",
         "--cachedir=/home/ftp/cpantesters",
         "--q=meta:from",
         "--q=qr:'(.*version .+? required--this is only.*)'",
         "--q=qr:'(.*is not exported.*)'",
         "--q=qr:'(.*instance running.*)'",
         "--q=qr:'(.*could.?n.t (?:open|connect).*)'",
         "--q=qr:'((?i:.*could.?n.t find.*))'",
         "--q qr:'(.*Base class package.*)'",
         "--q qr:'(Can.t call method .*)'",
         "--q qr:'(Can.t use an undefined value.*)'",
         "--q qr:'(Can.t use string .*)'",
         "--q qr:'(Can.t modify division)'",
         "--q qr:'(Connection refused)'",
         "--q qr:'(Can.t use keyword .*? as a label)'",
         "--q qr:'(Could not open .*)'",
         "--q qr:'(Address already in use)'",
         "--q qr:'(.*is deprecated (?:and will be removed)?)'",
         "--q qr:'(skipped: .*)'",
         "--q qr:'(Please rerun the make command)'",
         "--q qr:'(v-string .* non-portable at \\S+)'",
         "--q qr:'(The module .* isn.t available on CPAN)'",
         "--q qr:'(Invalid version format)'",
         "--solve",
         "--solvetop=123",
         "--dumpfile=/home/ftp/cnntp-solver-2009/workdir/solved/$arg.slvdv",
         "--transport=http_cpantesters_gzip",
         $arg,
        ) or die;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
