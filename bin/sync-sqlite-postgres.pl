#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--db=s>

DSN. Defaults to C<dbi:SQLite:dbname=$workdir/cpanstats.db> where
$workdir comes from the configuration.

=item B<--help|h!>

This help

=item B<--max=i>

Defaults to 360000. Number of records to transfer.

=back

=head1 DESCRIPTION

Just the cpanstats table. We consider already written records correct
and do not repeat them.

2013-11-16 akoenig : without throttling we seem to copy 100 records
per second. That would mean for 36M records: more than 4 days.

After a first run for 360000 records, it turned out to be slower, this
run took 4800 seconds. The next run for 2M records did t in 21400
seconds, so we were faster again.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

our $SIGNAL = 0;
$SIG{INT} = $SIG{TERM} = sub { my $sig = shift; warn "Caught $sig\n"; $SIGNAL=1 };

use DBI;
use Time::HiRes qw(sleep time);
use CPAN::Blame::Config::Cnntp;
my($workdir,$cpan_home,$ext_src);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
    $cpan_home = $CPAN::Blame::Config::Cnntp::Config->{cpan_home};
    $ext_src = $CPAN::Blame::Config::Cnntp::Config->{ext_src};
}

$Opt{max} ||= 720000;
$Opt{db}  ||= "dbi:SQLite:dbname=$workdir/cpanstats.db";

my $sldbi = DBI->connect ($Opt{db}); # return a dbi handle
my $pgdbi = DBI->connect ("dbi:Pg:dbname=analysis");

# $pgdbi->do("create table cpanstats (
#                 id          INTEGER PRIMARY KEY,
#                 guid        varchar(255),
#                 state       varchar(255),
#                 postdate    varchar(255),
#                 tester      varchar(255),
#                 dist        varchar(255),
#                 version     varchar(255),
#                 platform    varchar(255),
#                 perl        varchar(255),
#                 osname      varchar(255),
#                 osvers      varchar(255),
#                 fulldate    varchar(255),
#                 type        INTEGER)
# ");

my $slsth = $sldbi->prepare("SELECT max(id) from cpanstats");
$slsth->execute();
my($slmaxid) = $slsth->fetchrow_array();
my $pgsth = $pgdbi->prepare("SELECT max(id) from cpanstats");
$pgsth->execute();
my($pgmaxid) = $pgsth->fetchrow_array();
$pgmaxid ||= 0;
warn "slmaxid[$slmaxid] pgmaxid[$pgmaxid] opt-max[$Opt{max}]\n";
die "Nothing to copy" if $pgmaxid >= $slmaxid;

$slsth = $sldbi->prepare("SELECT id,guid,state,postdate,tester,dist,version,platform,perl,osname,osvers,fulldate,type from cpanstats where id > ? order by id limit $Opt{max}");
$slsth->execute($pgmaxid);
$pgsth = $pgdbi->prepare("INSERT INTO cpanstats
 (id,guid,state,postdate,tester,dist,version,platform,perl,osname,osvers,fulldate,type) values
 (?, ?,   ?,    ?,       ?,     ?,   ?,      ?,       ?,   ?,     ?,     ?,       ?)");

my $i = 0;
my $lastreport = my $starttime = time;
my $report_every = 1800;
ROW: while (my(@row) = $slsth->fetchrow_array) {
    unless ($pgsth->execute(@row)) {
        warn sprintf "ALERT: error inserting row/id[%s]: %s\n", $row[0], $pgdbi->errstr;
        last ROW;
    }
    ++$i;
    last ROW if $SIGNAL;
    sleep 0.01 unless $i%1000;
    if ( time - $lastreport > $report_every ) {
        warn sprintf "Running for %.1f hours, reached i[%d]\n", (time-$^T)/3600, $i;
        $lastreport = time;
    }
}
my $tooktime = time - $starttime;
warn "records transferred[$i] tooktime[$tooktime]\n";

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
