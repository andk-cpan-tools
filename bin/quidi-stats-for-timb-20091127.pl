#!/usr/bin/perl

use JSON::XS;
use YAML::Syck;
use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;

$|=1;
my $root = "$CPAN::Blame::Config::Cnntp::Config->{solver_vardir}/workdir/solved";
my $distros = 0;
my $reports = 0;
my $is_x8664ltm = 0;
my $have_uselongdouble = 0;
open my $fh, ">", "$root.all.json";
my $json = JSON::XS->new->pretty(0)->indent(0)->space_before(0)->space_after(0);
opendir my $dh, $root or die "Could not opendir '$root': $!";
for my $dirent (readdir $dh) {
    next unless $dirent =~ /\.slvdv$/;
    my $slvdv = "$root/$dirent";
    my $y = YAML::Syck::LoadFile $slvdv;
    $distros++;
    for my $rep (@{$y->{"==DATA=="}}) {
        # x86_64-linux-thread-multi?
        $reports++;
        print $fh $json->encode($rep), "\n";
        my $archname = $rep->{"conf:archname"};
        if ($archname =~ /^x86_64-linux-thread-multi(-ld)?$/) {
            $is_x8664ltm++;
            if ($rep->{"conf:uselongdouble"} eq "define"){
                $have_uselongdouble++;
            }
        }
    }
    my $jsonfilesize = -s "$root.all.json";
    print "\rdistros[$distros]reports[$reports]is_x86...[$is_x8664ltm]have_uld[$have_uselongdouble]json[$jsonfilesize]";
}
close $fh;
