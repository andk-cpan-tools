#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME

squirrelback.pl - when rsync back has not succeeded, retry

=head1 SYNOPSIS

squirrelback.pl hashdirectory

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Most famous fail during rsync back would be disk full. If we are
logged into the box that stopped due to the failing rsync back and we
know the hash directory name of the perl that should have been rsynced
back, this script can do all that's needed to bring this perl back to
the place where it belongs.

=head1 HISTORY

  (2023-10-27):
  >sand@k93jammy:/var/log/megasand% perl ~/src/andk/andk-cpan-tools/bin/squirrelback.pl /home/sand/src/perl/repoperls/installed-perls/host/k93jammy/v5.39.4/322f


=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

my($hash_dir) = @ARGV;
die "missing argument \$hash_dir" unless length $hash_dir;
die "hash_dir $hash_dir is not a directory" unless -d $hash_dir;
$hash_dir =~ s{/*$}{}; # must not have a trailing slash
my($tmp_dir) = readlink $hash_dir or die "could not readlink: $!";
die "tmp_dir $tmp_dir is not a directory" unless -d $tmp_dir;
my $perlv_dir = dirname $hash_dir;
my @hilo_dirs = glob "$perlv_dir/hilo*";
die "found no hilo directory within $perlv_dir, cannot continue" if @hilo_dirs == 0;
die "more than one hilo directory (@hilo_dirs), cannot continue" if @hilo_dirs > 1;
(my $tmp_dir_with_trailing_slash = $tmp_dir) =~ s{/*$}{/};
my @system = (rsync => '-ax', '--omit-dir-times', '--no-inc-recursive', $tmp_dir_with_trailing_slash, "$hilo_dirs[0]/");
warn "run: @system\n";
0==system @system or die "could not run: @system";
warn "unlink\n";
unlink $hash_dir or die "Could not unlink $hash_dir\: $!";
@system = (rsync => '-ax', $tmp_dir_with_trailing_slash, "$hilo_dirs[0]/");
warn "run: @system\n";
0==system @system or die "could not run: @system";
warn "rename\n";
rename $hilo_dirs[0], $hash_dir or die "Could not rename $hilo_dirs[0] to $hash_dir\: $!";
warn "rmdir\n";
rmdir "$hash_dir/.lock" or die "could not rmdir $hash_dir/.lock: $!";
@system = (rm => '-rf', $tmp_dir);
warn "run: @system\n";
0==system @system or die "could not run: @system";

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
