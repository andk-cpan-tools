#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

  ~/src/installed-perls/v5.34.0/c310/bin/perl bin/genoldestpostdate.pl

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

The ndjson implementation had a flaw in its first implementation.
Newly uploaded distros have always to be queried once without an after
parameter because some query must be the very first.

So we now introduce the table oldestpostdate and collect all oldest
postdates for every dist and every version.

=head1 SEE ALSO

refill-cpanstatsdb.pl

=cut


use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;

use Dumpvalue;
use File::Basename ();
use File::Path ();
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use List::Util qw(min);
use lib "$FindBin::Bin/../CPAN-Blame/lib";
our $HAVE_REDIS;

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

my($workdir);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
}

my($basename) = File::Basename::basename(__FILE__);

use DBI;
use Time::HiRes qw(time);
use JSON::XS ();
use List::Util qw(max);

our $jsonxs = JSON::XS->new->indent(0);
our $redis;
if ( $HAVE_REDIS ) {
    $redis = Redis->new(reconnect => 120, every => 1000);
}

my($pgdbh,$sth_ins,$sth_exists);
my %Partitions;
$pgdbh = DBI->connect("dbi:Pg:dbname=analysis") or die "Could not connect to 'analysis': $DBI::err";
my @tables = $pgdbh->tables("","public");
unless (grep { /oldestpostdate/ } @tables) {
    $pgdbh->do("CREATE TABLE oldestpostdate (dist character varying(255) not null, version character varying(255) not null, postdate character varying(6) not null)") or die "Error while trying to create table oldestpostdate";
    $pgdbh->do("CREATE UNIQUE INDEX ixoldestpostdate ON oldestpostdate (dist,version)") or die "Error while trying to create index";
}
my $sth = $pgdbh->prepare("SELECT dist,version,postdate FROM cpanstats ORDER BY id");
$sth->execute();
{
    my $sql = "INSERT INTO oldestpostdate
 (dist,version,postdate) VALUES
 (?,   ?,      ?)";
    $sth_ins = $pgdbh->prepare($sql);
}
{
    my $sql = "SELECT COUNT(*) FROM oldestpostdate WHERE dist=? AND version=?";
    $sth_exists = $pgdbh->prepare($sql);
}
my($inscount) = 0;
my($read,$wrote) = (0,0);
my $rows = $sth->rows;
ROW: while (my($dist,$version,$postdate) = $sth->fetchrow_array) {
    $read++;
    $sth_exists->execute($dist,"");
    my($cnt0) = $sth_exists->fetchrow_array;
    unless ($cnt0) {
        $sth_ins->execute($dist,"",$postdate);
        $wrote++;
    }
    $sth_exists->execute($dist,$version);
    my($cnt1) = $sth_exists->fetchrow_array;
    unless ($cnt1) {
        $sth_ins->execute($dist,$version,$postdate);
        $wrote++;
    }
    printf "\r%6s: read=%d/%d wrote=%d    ", $postdate, $read, $rows, $wrote;
}
printf "\nFIN\n";

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
