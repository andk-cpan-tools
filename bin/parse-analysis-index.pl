#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--days=f>

number of days to scan. Defaults to 1461.

=item B<--help|h!>

This help

=item B<--max=i>

Maximum number of hits to produce. Once reached, the program stops.

=item B<--redis-enqueue!>

If set, we throw each hit into the redis on localhost. Before doing so
we make a sanity check: analysis:jobqueue:q must exist and be a zset,
otherwise we die.

=back

=head1 DESCRIPTION

Find all rows on the front page that have no link in the first column
or have an 8-digit upload date or the comment column is empty despite
there is an annotation.

=head1 HISTORY

=head2 no link in first column

First run was on 2013-11-05 and revealed there were 600 rows without a
link in the first column. Nearly all of them are in the rows higher
than 2000.

Reason seems to be that some version N+1 had no fail at all and so it
remained undiscovered that N was suddenly outdated. Still
unresearched. One twist seems to be that the sample of 500 is often
too low. If we have only three fails and then sample to less than N,
we probably lose one of the three fails. We should check whether

  NUMBER_OF_FAILS * SAMPLE_SIZE / POPULATION is > SOMEVALUE

where SOMEVALUE has to be > 3 to avoid random undersampling. This has
been fixed in ctgetreports by introducing the option C<--minpass>.

Also found C<AURUM/Text-Tradition-Analysis-1.1-podfix> and
C<DYLAN/POE-Component-Runner-0.04.b>. I didn't bother to investigate
here for now.

And also

  Win32-FindFile-0.14|3
  Win32-FindFile-0.14-withoutworldwriteables|2

where we just reran a calculation with --pick and it repaired itself.
It seems no biggie, so skip this for now.

And then there is PerlIO-text-0.007 which probably was just waiting to
be calculated. It had only 6 fails in 18 months, so maybe was
discovered late and never reached by the priority queue. But maybe it
was something else.

Here is an interesting one:

  sqlite> select distv, greenish from distcontext where distv like 'Config-Model-2%';
  Config-Model-2.002|2
  Config-Model-2.005|2
  Config-Model-2.013|3
  Config-Model-2.014|2
  Config-Model-2.017|2
  Config-Model-2.015|1
  Config-Model-2.024|1
  Config-Model-2.025|2
  Config-Model-2.026_1|2
  Config-Model-2.010|
  Config-Model-2.029|2
  Config-Model-2.030_01|2
  Config-Model-2.037|2
  Config-Model-2.038|2
  Config-Model-2.039|1
  Config-Model-2.042|1

This illustrates best that one step on the way to achieve sanity can
be to delete old cruft from distcontext.

=head2 8-digit upload date

Done around the caching table distlookup Jan/Feb 2014.

=head2 annotation not yet integrated

20140310: first stab at it

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

$Opt{days} ||= 1461;
my $redis;
if ($Opt{"redis-enqueue"}) {
    require Redis;
    $redis = Redis->new;
    my($type) = $redis->type("analysis:jobqueue:q");
    die "localhost redis analysis:jobqueue:q is a '$type', not a zset" unless "zset" eq $type;
}
my $anno;
{
    my $annofile = "$FindBin::Bin/../annotate.txt";
    my $fh;
    unless (open $fh, $annofile) {
        # $DB::single=1;
        die "Could not";
    }

    $anno = {};
    local $/ = "\n";
    my $i = 0;
 ANNOLINE: while (<$fh>) {
        chomp;
        next ANNOLINE if /^\s*$/;
        my($distv,$splain) = split " ", $_, 2;
        $anno->{$distv} = $splain;
    }
}

use LWP::UserAgent;
use XML::LibXML;
my $ua = LWP::UserAgent->new();
# my $resp = $ua->get("http://217.199.168.174:3000/?author=&age=2922&SUBMIT_xxx=Submit");
my $resp = $ua->get("http://217.199.168.174:3000/?author=&age=$Opt{days}&SUBMIT_xxx=Submit");
# my $resp = $ua->get("http://217.199.168.174:3000/?author=&age=91.3&SUBMIT_xxx=Submit");
$|=1;
my $cnt = 0;
my $highscore;
if ($resp->is_success) {
    my $content = $resp->decoded_content;
    my $p = XML::LibXML->new();
    # loading as html complains about </p>
    # loading as xml complained about &nbsp not being defined, but we fixed this by changing the document
    my $doc = $p->load_xml(string => $content);
    my $root = $doc->documentElement;
    $root->setNamespace("http://www.w3.org/1999/xhtml","html",1);
    # $DB::single=1;
    my @row = $root->findnodes("//html:table[\@class='texttable']//html:tr");
 ROW: for my $row (@row) {
        my(@td) = $row->findnodes("html:td") or next;
        my $td1 = $td[1];
        my($a) = $td1->findnodes("html:a");
        my $repaircandidate = 0;
        my $distv;
        if ($a) {
            my $href = $a->getAttribute("href");
            $href =~ s/.*?distv=//;
            $distv = $href;
            $distv =~ s{.+/}{};
            # printf "[%s]", $href;
        } else {
            $repaircandidate = 1;
        }
        my $td5 = $td[5];
        my $td5string = $td5->textContent;
        if ($td5string =~ /[0-9]{8}/) {
            $repaircandidate = 1;
        }
        if ($distv && $anno->{$distv}) {
            my $td7 = $td[7];
            my $td7string = $td7->textContent;
            if ($td7string =~ /^\s*$/) {
                $repaircandidate = 1;
            }
        }
        if ($repaircandidate) {
            $cnt++;
            if ($Opt{max} && $cnt > $Opt{max}) {
                last ROW;
            }
            my $td1string = $td1->serialize;
            $td1string =~ s/.+<!-- //gs;
            $td1string =~ s/ -->.+//gs;
            $td1string =~ s/\s//g;
            $td1string =~ s|(.+)/||;
            my $author = $1;
            $author =~ s|/.+||;
            my(@td034) = @td[0,3,4];
            for my $tdi (0..$#td034) {
                my $td = $td034[$tdi];
                my $string = $td->textContent;
                $string =~ s/[^0-9]//g;
                $td034[$tdi] = $string
            }
            printf "%3d %4d %4d %4d %-9s http://matrix.cpantesters.org/?dist=%s\n", $cnt, @td034, $author, $td1string;
            if ($Opt{"redis-enqueue"}) {
                unless (defined $highscore) {
                    (undef,$highscore) = $redis->zrevrange("analysis:jobqueue:q",0,0,"withscores");
                    $highscore ||= 1;
                }
                $redis->zadd("analysis:jobqueue:q",$highscore,$td1string);
            }
        }
    }
} else {
    warn sprintf "Code: %s\n", $resp->code;
    die $resp->content;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
