#!/usr/bin/perl



=pod

Eigentlich sollte ein Einzeiler reichen, aber er ist schon ein wenig
unuebersichtlich:

diff -U 0 =(/usr/local/perl-5.8.0@17974/bin/perl -V:.\*) =(/usr/local/perl-5.8.0@17975/bin/perl -V:.\*)

See also: compare-snapshots.pl

=cut

use strict;
use warnings;
use Getopt::Long;

our %OPT;
GetOptions(\%OPT,"all!");

die "Usage: $0 perl_1 perl_2" unless @ARGV == 2;
my($l,$r) = @ARGV;
my $pipe_remove_semi = "|perl -ple s:\\;\\\\z::"; # 5.6.2 had no semicolons
my $pipe_sort = "|sort";

open my $zsh, qq{zsh -c 'diff -U 0 =($l -V:.\\*$pipe_remove_semi$pipe_sort) =($r -V:.\\*$pipe_remove_semi$pipe_sort)' |} or die;
CONFVAR: while (<$zsh>) {
  next if /^@/;
  if (!$OPT{all}) {
    next CONFVAR if /^.
                     (
                     archlib(?:exp)?|
                     bin(?:exp)?|
                     cf_time|
                     config_arg[\dc]+|
                     dynamic_ext|
                     extensions|
                     install.*|
                     libs.*|
                     man\d.*|
                     myuname|
                     perlpath|
                     prefix.*|
                     priv.*|
                     scriptdir.*|
                     sig_name.*|
                     site.*|
                     startperl|
                     zzzzzzzzzzzzzzz
                    )=/x
                        or
                            /
                             =.*\.so\b
                             /x;
  }
  print;
}
