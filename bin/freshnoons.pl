#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--top=i>

Defaults to 12. How many entries per Pair?

=back

=head1 DESCRIPTION

freshnoons are distros that have no fails in the old distro and no
passes in the new distro.

  N 0 0 N

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{top} //= 12;

use JSON::XS ();
use strict;
my %A;
my $ann = "/home/andreas/src/analysis-cpantesters-annotate/annotate.txt";
open my $fh, $ann or die "Could not open $ann: $!";
while (<$fh>){
    my($d,$r) = /(\S+)\s+(.+)/;
    $A{$1} = $2
}
my $json_file = "/home/andreas/var/beforemaintrelease/overview.json";
my $json = do { open my $fh, '<', $json_file or die "Couldn't open <$json_file: $!"; local $/; <$fh> };
my $j = JSON::XS::decode_json $json;
my $i = 0;
for my $k (sort { $j->{$b} cmp $j->{$a} } keys %$j) {
    my $bmrr = "/home/andreas/var/beforemaintrelease/result-$k.txt";
    next unless -e $bmrr;
    ++$i;
    warn "picking $bmrr";
    open $fh, $bmrr or die "Could not open $bmrr: $!";
    my @l;
    while (<$fh>){
        chomp;
        my($d,@n) = split " ", $_;
        my $v = $n[0] + $n[3] - $n[1] - $n[2];
        $v+=200 if $n[1]==0 and $n[2]==0; $v += 50 if $n[0]==0 || $n[2]==0;
        $v -= 300 if exists($A{$d});
        push @l, [ $v, sprintf "%4d %s%s\n",
            $v, $_, exists($A{$d}) ? " $A{$d}" : "" ];
    }
    @l = map { $_->[1] } sort { $b->[0] <=> $a->[0] } @l;
    pop @l while @l > $Opt{top};
    print @l;
    last if $i >= 3; # e.g. 5.36.0:5.36.1rc2, 5.36.0:5.37.11, and 5.36.0:5.37.10
}


# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
