#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME

analysis-jobqueue-rm.pl -- Remove one job from the redis jobqueue on analysis

=head1 SYNOPSIS

 $0 jobnumber [jobnumber...]

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--dry-run|n!>

Don't do anything, just tell what you would do

=back

=head1 DESCRIPTION

Remove a job by number. Does just this if argument is 639408:

 2015-08-03 21:31  redis-cli get analysis:jobqueue:jobs:639408:hex
 2015-08-03 21:31  redis-cli get analysis:jobqueue:jobs:639408:descr
 2015-08-03 21:32  redis-cli del analysis:jobqueue:jobs:40aebf9db9a664e052e4b886d0c724bbce3cf4d4:id
 2015-08-03 21:32  redis-cli del analysis:jobqueue:jobs:639408:hex
 2015-08-03 21:32  redis-cli del analysis:jobqueue:jobs:639408:descr
 2015-08-03 21:33  redis-cli srem analysis:jobqueue:runningjobs 639408

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

use Redis;
use Time::HiRes qw(sleep);
my $redis = Redis->new(reconnect => 120, every => 1000);
@ARGV or pod2usage(1);
for my $job_id (@ARGV) {
    if (my $hex = $redis->get("analysis:jobqueue:jobs:$job_id:hex")) {
        my $descr = $redis->get("analysis:jobqueue:jobs:$job_id:descr");
        warn "found job_id $job_id, hex $hex, descr $descr\n";
        next if $Opt{"dry-run"};
        for my $del_frag ("$hex\:id", "$job_id\:hex", "$job_id\:descr") {
            my $del = "analysis:jobqueue:jobs:$del_frag";
            my $ret = $redis->del($del);
            warn "del $del => ret $ret\n";
            die "Could not delete $del, giving up" unless $ret;
        }
        my $srem = "analysis:jobqueue:runningjobs";
        my $ret = $redis->srem($srem, $job_id);
        warn "srem $srem $job_id => ret $ret\n";
        die "Could not srem $srem $job_id, giving up" unless $ret;
    } else {
        die "Failing: Job_id $job_id unknown";
    }
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
