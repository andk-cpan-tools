#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

 verify-distlookup-lcpassfail.pl distv ...

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--repairnth=i>

Defaults to 18, meaning every 18th record is subject to a zero run,
i.e. a run of

  cnntp-solver.pl --cpanstats_distrofacts_zero=...

Setting this to 0 disables automatic repair.

=item B<--sleep=i>

Defaults to 2, meaning after every repair we sleep for 2 seconds.

=back

=head1 DESCRIPTION

distv arguments are optional. without such you get all
pass/pass/fail/fail data, that have deviating numbers for lastcalc
according to slvdv vs DB.

B<Probably a script of small utility>, but we keep it because it
helped us to find a missing record in the database.

Sind aber distv argumente gegeben, dann werden die Vergleiche fuer
diese angezeigt.

Die wahre Zahl der PASS und FAIL der letzten Calc findet man in der
.slvdv.gz Datei unter meta:ok in der verwegenen Struktur

   "meta:ok" : {
      "PASS" : {
         "PASS" : 1411
      },
      "FAIL" : {
         "FAIL" : 37
      },
      "NA" : {
         "NA" : 1
      }
   },

Die Datei ist in json. Prove:

  % zcat /home/andreas/data/cnntp-solver-2009/workdir/solved/MIME-tools-5.505.slvdv.gz | perl -le '
  my $j = do {local $/; <STDIN>}; 
  use JSON::XS;
  my $h = JSON::XS->new->decode($j);
  warn join ",", map { $_ => $h->{"meta:ok"}{$_}{$_} } qw(PASS FAIL NA)'
  PASS,1411,FAIL,37,NA,1 at -e line 5, <STDIN> line 1.

lc_pass und lc_fail wurde lange Zeit und wird noch immer falsch
geschrieben. Sobald das in cnntp-solver.pl gefixt ist, sollte diese
Liste automatisch kuerzer werden, aber wer weiss. Die confusion about
lastcalc, lc_*, passfail_overview, timestamp bug, etc. ist enorm, da
muss etwas reduziert werden: canonische verbindliche Adressen fuer
primaerdaten und fuer gecachetes Material.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{repairnth} //= 18;
$Opt{sleep} //= 2;
use CPAN::Blame::Config::Cnntp;
use Time::Moment;
use Algorithm::Numerical::Shuffle qw /shuffle/;
require DBI;
use JSON::XS;

my($workdir,$cpan_home,$ext_src);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
    $cpan_home = $CPAN::Blame::Config::Cnntp::Config->{cpan_home};
    $ext_src = $CPAN::Blame::Config::Cnntp::Config->{ext_src};
}

my $dbi = DBI->connect ("dbi:Pg:dbname=analysis");
my $sql = "SELECT lc_pass, lc_fail FROM distlookup WHERE distv=?";
my $sth = $dbi->prepare($sql);
my $slvdir = "$workdir/solved";
opendir my $dh, $slvdir or die;
my %argv = map {($_ => 1)} @ARGV;
my $headerwritten = 0;
my $line = 0;
my @dirent = shuffle grep {/slvdv\.gz$/} readdir $dh;
DIRENT: for my $dirent (@dirent) {
    my($distv) = $dirent =~ /(.+)\.slvdv\.gz$/ or next;
    my $wantsee;
    if (keys %argv) {
        if ($argv{$distv}) {
            $wantsee = "u"; # unconditionally
        } else {
            next DIRENT;
        }
    } else {
        $wantsee = "g"; # slv is greater
        # $wantsee = "d"; # any diff
    }
    my $abs = "$slvdir/$dirent";
    my $j = do {local $/; open my $fh, "-|", zcat => $abs; <$fh> };
    my $h = eval { JSON::XS->new->decode($j); };
    if ($@ or !defined $h) {
        warn "error while decoding $abs: $@";
        next;
    }
    my %slv = map { $_ => $h->{"meta:ok"}{$_}{$_} } qw(PASS FAIL NA);
    $sth->execute($distv);
    next unless $sth->rows >= 1;
    my(@row) = $sth->fetchrow_array();
    next unless defined $row[0] or defined $row[1];
    $row[0] ||= 0;
    $row[1] ||= 0;
    my @stat = stat $abs;
    if ($wantsee eq "d") {
        no warnings 'uninitialized';
        my $iseq = $slv{PASS}==$row[0] && $slv{FAIL}==$row[1];
        next if $iseq;
    } elsif ($wantsee eq "g") {
        no warnings 'uninitialized';
        my $isslvgt = $slv{PASS}>$row[0] || $slv{FAIL}>$row[1];
        next unless $isslvgt;
    } elsif ($wantsee eq "u") {
        # no next, we want to see everything
    } else {
        die "illegal value for wantsee: $wantsee";
    }
    #my $smalldiff = abs($slv{PASS}-$row[0]) + abs($slv{FAIL}-$row[1]) <= 4;
    #next if $smalldiff;
    unless ($headerwritten++) {
        print "|distv========|=pass-per-slv |=pass-per-db |=fail-per-slv |=fail-per-db\n";
    }
    ++$line;
    my $time = Time::Moment->from_epoch($stat[9])->at_utc->strftime("%FT%T%Z");
    $time =~ s/:[0-9][0-9]Z/z/;
    {
        no warnings 'uninitialized';
        warn sprintf "%d/%d %-31s %4d %4d %4d %4d %s\n", $line, scalar @dirent, $distv, $slv{PASS}, $row[0], $slv{FAIL}, $row[1], $time;
    }
    if ($Opt{repairnth}) {
        unless ($line % $Opt{repairnth}) {
            system "$^X bin/cnntp-solver.pl --cpanstats_distrofacts_zero=$distv";
            sleep $Opt{sleep} if $Opt{sleep};
        }
    }
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
