#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

 m-l-smokerpt-parse <filename>

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION



=head1 BUGS

This is a one-off script with shortcuts.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

my $filename = shift or pod2usage(1);

use YAML::Syck;
my $y = YAML::Syck::LoadFile($filename);
for my $report (sort { $y->{$a}{timestamp} cmp $y->{$b}{timestamp} } keys %$y) {
    my $treport = $y->{$report};
    my($avg) = $treport->{content} =~ /^    smoketime .+? \(average (.+?)\)/m;
    printf "%s %s\n", $treport->{timestamp}, $avg;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
