#!/usr/bin/perl

=head1 NAME

jpeguploads - 

=head1 SYNOPSIS

Root runs this from ~ftp/incoming directory in a loop like

  while true; do perl ~k/sources/CPAN/andk-cpan-tools/bin/jpeguploads.pl ; sleep 20 ; done

The user looks at the images witha browser under
file:///home/ftp/incoming/today/index.html

=head1 DESCRIPTION

We have a directory in our ftp server where the xxx video server is
allowed to upload images. We make thumbnails of the images and
maintain html pages that let us inspect the thumbnails quickly.

We are always in a hurry and have only that many seconds time to run.
After that we exit. So they(you) must call us repeatedly.

=cut


$ENV{LANG} = "C";

use strict;
use DateTime;
use File::Basename qw(dirname);
use Getopt::Long;
eval {require Time::Duration};
our $HAVE_TIME_DURATION = !$@;

our %Opt;
GetOptions(\%Opt, "timeout=i");

$Opt{timeout} ||= 60;
my $start = time;


# first find out what the most recently uploaded date is
my $dir0 = ".";
opendir my $dh0, $dir0 or die;
my ($m0,$d0,$y0,$iso0);
for my $dirent (readdir $dh0) {
    next unless $dirent =~ /(\d+)_(\d+)_(\d+)/;
    my($m,$d,$y) = ($1,$2,$3);
    my $iso = sprintf "%04d-%02d-%02d", $y, $m, $d;
    if (not defined $iso0 or $iso gt $iso0) {
        $iso0 = $iso;
        $y0 = $y;
        $m0 = $m;
        $d0 = $d;
    }
}

my $dir = sprintf "%d_%d_%d", $m0, $d0, $y0;
opendir my $dh, $dir or die "Could not opendir '$dir': $!";
my @base;
my @tobethumbed;
for my $dirent (readdir $dh) {
    next unless $dirent =~ /(.+)\.jpg$/;
    my $base = $1;
    next if $dirent =~ /\.thumb\.jpg$/;
    push @base, $base;
    next if -e "$dir/$base.thumb.jpg"; # already converted
    warn "base[$base]\n";
    push @tobethumbed, $base;
}
@base = sort { $b cmp $a } @base;
if (@tobethumbed or -M "$dir/index.html" > -M $0) {
    open my $fh, ">", "$dir/index.html.new" or die;
    printf $fh <<EOH, ($y0, $m0, $d0)x2;
<html><head><title>prtz%04d-%02d-%02d</title><meta http-equiv="refresh" content="1800; URL=index.html"></head><body><h3>%04d-%02d-%02d</h3>
EOH
    my $L_min;
    for my $base (@base) {
        my $min = substr($base,0,5);
        if (not defined $L_min or $min ne $L_min) {
            my $dt = DateTime->new
                (
                 year   => $y0,
                 month  => $m0,
                 day    => $d0,
                 hour   => substr($min,0,2),
                 minute => substr($min,3),
                 second => 0,
                 time_zone => 'GMT',
                );
            $dt->set_time_zone("Europe/Berlin");
            my $berlin_min = sprintf "%02d:%02d", $dt->hour, $dt->minute;
            print $fh "<h6>$berlin_min</h6>\n";
        }
        print $fh <<EOR;
<a href="$base.jpg"><img src="$base.thumb.jpg"/></a>
EOR
        $L_min = $min;
    }
    print $fh "</body></html>\n";
    rename "$dir/index.html.new", "$dir/index.html" or die;
    my $updir = dirname $dir;
    my $symlink = "$updir/today"; # wrong: we're losing the images!
    if (-l $symlink) {
        my $content = readlink $symlink;
        if ($content ne $dir) {
            unlink $symlink or die "Could not unlink: $!";
            warn "unlinked symlink '$symlink'";
        }
    }
    unless (-e $symlink) {
        symlink $dir, $symlink or die "Could not create symlink: $!"
    }
    for my $base (@tobethumbed) {
        if (0 == -s "$dir/$base.jpg") {
            warn "Found empty image '$dir/$base.jpg'\n";
            local $^T=time;
            if (-M "$dir/$base.jpg" > 1) {
                warn "Removing...\n";
                unlink "$dir/$base.jpg" or die "Could not unlink: $!";
            } else {
                warn "Skipping\n";
            }
            next;
        }
        unless ( 0 == system "convert", "-geometry", "150x150", "-quality", "75", "$dir/$base.jpg", "$dir/$base.thumb.jpg" ) {
            warn "died during convert '$dir/$base.jpg' '$dir/$base.thumb.jpg'; sleeping a bit...";
            sleep 20;
            die "Bye";
        }
        last if time - $start > $Opt{timeout};
    }
}
require Cwd;
my $abs = sprintf "%s/%s/%s.jpg", Cwd::cwd(), $dir, $base[0];
if ($HAVE_TIME_DURATION) {
    my @stat = stat $abs;
    my $mtime = $stat[9];
    warn sprintf "   %s: %s\n", Time::Duration::duration(time - $mtime), $abs;
} else {
    warn sprintf "latest: %s\n", $abs;
}
if (not -e "current.jpg" or -M "current.jpg" > -M "$dir/$base[0].jpg") {
    unlink "current.jpg";
    symlink "$dir/$base[0].jpg", "current.jpg";
}
