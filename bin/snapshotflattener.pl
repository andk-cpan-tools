#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

 bin/snapshotflattener.pl ~sand/megalog/megalog-9999-99-99T99:99:99.log

 bin/snapshotflattener.pl ~sand/megalog/megalog-9999-99-99T99:99:99.

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--dry-run|n!>

No rewrite please.

=item B<--showfirstrequirer|sfr!>

Spits out a YAML structure showing in which file we have a requirer,
the name of that one, and the names of the dependencies that are not
already listed in from of him. Implies --dry-run and an unlimited
--top.

=item B<--help|h!>

This help

=item B<--maxcand=i>

Defaults to 0. If given, we limit the number of candidates to process by
randomly kicking out candidates.

=item B<--top=i>

Defaults to 12, so we get the top 12 reported. If the value is 0, we
just get the old output which showed us whenever we had a new
highscore (something quite useless which we considered good enough for
too long).

=back

=head1 DESCRIPTION

We rank all modules that trigger a "requires" line, that is all
modules that are not yet installed when they are suddenly needed as a
dependency. This ranking is called RANKIN. We rank these modules again
according to the relative lines they occupy in the Bundle/Snapshot
files. We call this ranking RANKOUT. For the record: we rank them by
module name, not by line number. (???)

Finally we calculate the maximum diff between RANKIN and RANKOUT. If
the diff is e.g. 700, we can say: we should move this module past 700
*ranked* modules. In reality this may mean a movement of thousands of
lines up.

=head1 HISTORY

Iff all modules were in perfect linear order to satisfy all
dependencies, we could easily read the time it takes to build one
module. *And* we could easily rebalance the 5 snapshotfiles when one
of them goes over the well established max of 10 hours.

I came to this when trying to run C<megalog-overview.pl --debug=reps>
which turned out to be buggy.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{maxcand} //= 0;
$Opt{top} //= 12;
if ($Opt{showfirstrequirer}) {
    $Opt{top} = 999999999;
    $Opt{"dry-run"} = 1;
    require YAML;
}

my $megalog = shift or pod2usage(1);
$megalog =~ s/\.$/.log/;
my @snaps = glob("lib/Bundle/Snapshot_2011_10_21_0?.pm");

our(%INSTATE,
    %CANDIDATES,
    @SHIFTLINES,
    %RANKIN,
    %RANKOUT,
    %FIRSTREQUIRER,
    $REQUIRER,
    %IN_FILE,
    %OUT_FILE,
    $FILE,
    %REQTYPE,
    %BUNDLELINE,
);
$INSTATE{contents}=0;
open my $fh, $megalog or die;
while (<$fh>){
    if (/\s(\S+)\s+\[(build_requires|requires|commandline)(?:,(optional))?\]/) {
        my($namespace, $reqtype, $optional) = ($1, $2, $3);
        if (!$optional || rand(32)<1) {
            $CANDIDATES{$namespace}++;
            unless (exists $RANKIN{$namespace}) {
                $RANKIN{$namespace} = 1 + scalar keys %RANKIN;
            }
            $FIRSTREQUIRER{$namespace} ||= $REQUIRER;
            $IN_FILE{$namespace} ||= $FILE;
            $REQTYPE{$namespace} ||= $reqtype;
        }
    } elsif (/Running install for module\s+'([A-Za-z0-9\:]+)'/) {
        my $namespace = $1;
        $CANDIDATES{$namespace}++;
        $REQUIRER = $namespace;
    } elsif (/^path\|-> Bundle::\S+([0-9][0-9])\s*$/) {
        my $namespace = $1;
        $FILE = $namespace;
    }
}
if ($Opt{maxcand}) {
    while (keys %CANDIDATES > $Opt{maxcand}){
        my @c = keys %CANDIDATES;
        my $r = $c[rand $#c];
        delete $CANDIDATES{$r};
    }
}

my $bundleline = 0;
for my $snap (@snaps) {
    my($fh, $fh2);
    my $out_file = substr($snap,-5,2);
    if ($Opt{"dry-run"}) {
        open $fh, $snap or die "Could not open $snap: $!";
    } else {
        rename $snap, "$snap~" or die "Could not rename $snap: $!";
        open $fh, "$snap~" or die "Could not open $snap~: $!";
        open $fh2, ">", $snap or die "Could not open >$snap: $!";
        select $fh2;
    }
    $\ = $/ = "\n";
    while (<$fh>) {
        chop;
        if (/^=/){
            if (/^=head1 CONTENTS/){
                $INSTATE{contents}=1;
                unless ($Opt{"dry-run"}) {
                    print;
                }
                next;
            } else {
                $INSTATE{contents}=0;
                if (/^=head1 CONFIGU/){
                    unless ($Opt{"dry-run"}) {
                        print ((shift @SHIFTLINES)->[0]) while @SHIFTLINES;
                    }
                }
            }
        }
        if ($INSTATE{contents}){
            if (/^([A-Za-z]\S*)/){
                $bundleline++;
                $BUNDLELINE{$1} ||= $bundleline;
                if ($CANDIDATES{$1}){
                    $RANKOUT{$1} = 1 + scalar keys %RANKOUT;
                    $OUT_FILE{$1} = $out_file;
                    unshift @SHIFTLINES, [$_, $1];
                } else {
                    push @SHIFTLINES, [$_, $1];
                }
                if (@SHIFTLINES > 1){
                    my $line = (shift @SHIFTLINES)->[0];
                    $line =~ s/\n//g;
                    unless ($Opt{"dry-run"}) {
                        print $line;
                    }
                }
            } else {
                if (@SHIFTLINES){
                    $SHIFTLINES[-1][0] .= "\n" unless $SHIFTLINES[-1][0] =~ /\n/;
                } else {
                    push @SHIFTLINES, [""];
                }
            }
        } else {
            unless ($Opt{"dry-run"}) {
                print;
            }
        }
    }
}
my $max = { diff => -99999 };
my @reported;
my @all;
for my $k (sort keys %CANDIDATES) {
    unless ($RANKOUT{$k}){
        $RANKOUT{$k} = 1 + scalar keys %RANKOUT;
    }
    my $diff = $RANKOUT{$k} - ($RANKIN{$k} ||= 0);
    $REQTYPE{$k} ||= "?";
    $IN_FILE{$k} ||= "n/a";
    if ($Opt{top}) {
        next unless exists $FIRSTREQUIRER{$k};
        push @all, { diff=>$RANKOUT{$k}-$RANKIN{$k}, in=>$RANKIN{$k}, out=>$RANKOUT{$k}, name=>$k, reqdby=>$FIRSTREQUIRER{$k}, infile=>$IN_FILE{$k}, outfile=>$OUT_FILE{$k}||"--", reqtype=>$REQTYPE{$k}, bundleline=>($BUNDLELINE{$k} ||= $bundleline) };
    } else {
        if ($diff > $max->{diff}) {
            $max->{diff} = $diff;
            $max->{in}   = $RANKIN{$k};
            $max->{out}  = $RANKOUT{$k};
            $max->{k}    = $k;
            next if $max->{diff} < 0;
            warn "Note: so far largest distance with $max->{k}: $max->{out} - $max->{in} = $max->{diff}\n";
            push @reported, $k;
        }
    }
}
if ($Opt{top}) {
    for my $rec (@all) {
        $rec->{bundleline_reqdby} = $BUNDLELINE{$rec->{reqdby}};
        $rec->{reqtype_short} = substr($rec->{reqtype},0,1);
    }
    my $showfirstrequirer = {};
    for my $rec (sort { $a->{bundleline_reqdby} <=> $b->{bundleline_reqdby}
                            || $a->{bundleline} <=> $b->{bundleline}
                    } @all) {
        next if $BUNDLELINE{$rec->{name}} < $BUNDLELINE{$rec->{reqdby}};
        if ($Opt{showfirstrequirer}) {
            $showfirstrequirer->{bfile} ||= $rec->{infile};
            $showfirstrequirer->{name} ||= $rec->{reqdby};
            if ($rec->{reqdby} ne $showfirstrequirer->{name}) {
                print YAML::Dump($showfirstrequirer);
                last;
            }
            $showfirstrequirer->{line} = $rec->{bundleline_reqdby};
            my $a = $showfirstrequirer->{deps} ||= [];
            push @$a, $rec->{name};
            next;
        }
        printf "%5d %-58s %s %5d %5d %s %s %s\n", @{$rec}{qw/diff name reqtype_short bundleline bundleline_reqdby outfile infile reqdby/};
        push @reported, $rec->{name};
        last if @reported >= $Opt{top};
    }
}
my $show_maybe_try = 0;
if ($show_maybe_try) {
    warn sprintf "Maybe try:
  egrep -n '^(%s)(\$| )' lib/Bundle/Snapshot_2011_10_21_0*.pm
", join("|", @reported);
    warn sprintf "And/Or:
  egrep -an ' (%s) *\\[.*requi|^perl\\|' %s
", join("|", @reported), $megalog;
    warn sprintf "And/Or:
  egrep -an ' \\S+ *\\[.*requi|^perl\\||Running install for module' %s|egrep -B2 ' (%s) '
", $megalog, join("|", @reported), ;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
