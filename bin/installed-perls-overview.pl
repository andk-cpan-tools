#!/usr/bin/perl

# arguments can be package names: their versions are appended to each line, space separated

=head1 NAME

....pl - 

=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut


my $optpod = <<'=back';

=item B<--determineversion=s>

C<variable> or C<method> (or left anchored substring thereof).
Defaults to C<method> as recommended in perlobj manpage. See
https://rt.cpan.org/Ticket/Display.html?id=127301. Some want to
override, some cannot do that right.

=item B<--eval=s>

TBD: Code to eval and print the output.

=item B<--fshorts!>

Filter the list of perls for those with short describing names, like
C<v5.35.10>, ignoring everything with longer names than 8 characters.

=item B<--help|h!>

This help

=item B<--hostname=s>

when determining the config file, use this hostname.

=item B<--max=i>

stop output after that many perls

=item B<--maxperl=s>

skip perls above

=item B<--minperl=s>

skip perls below

=item B<--perlrx=s>

skip perls not matching this regexp

=item B<--show=s@>

Config attributes to be included in the table. Defaults to
C<useithreads>, C<usemultiplicity>, C<uselongdouble>, and C<config_args~EBUGGING=both>.

Config attributes are mostly reported with their own value with two
exceptions:

  define  becomes  D
  undef   becomes  u

If the expression contains a tilde, it's a primitive matching. RTFS.

=item B<--sprintfwidth|w=i>

Width reserved for the versions. Defaults to 8. If there is one
spielverderber with a version number of 20110324.1600_001, you can set
it to 17 to get the columns aligned.

=item B<--sprintfwidthperl|wp=i>

Width reserved for the perl version. Defaults to 28. If there are RCs
around like v5.32.0-RC0-45-g2652a49709/3c10, you can set it to 31 to
get the columns aligned

=item B<--verbose!>

Include the module names in the output

=back

=head1 DESCRIPTION

Looks which perls we have in
C</home/sand/src/perl/repoperls/installed-perls/host/$HOST/perl/> and
reports about the two significant directory levels in their paths,
ordered according to perl's git conventions, e.g.:

v5.17.7.0-15-ge2beedf/a2da
v5.17.7.0-15-ge2beedf/9980
v5.17.7.0-15-ge2beedf/127ecf
v5.17.7.0-15-ge2beedf/127ec
v5.17.7.0-15-ge2beedf/127e
v5.17.6-424-ge2beedf/127e
v5.17.6-423-g67e0a0c/9980
v5.17.6-422-gc2219ca/a2da
v5.17.6-421-gd1bee06/165a
v5.17.6-416-g9a90211/127e
v5.17.6-415-gbfd14e5/9980
v5.17.6-413-g2e1be9f/a2da
v5.17.6-412-g6e0a4ea/165a
v5.17.6-408-g3cf48ca/127e
v5.17.6-306-g23b7025/9980
v5.17.6-98-gcd298ce/127e
v5.17.6-97-gf5a0fd1/9980
v5.17.6-97-gf5a0fd1/127e
v5.17.6-92-g00f6437/127e


=head1 TODO


=head1 AUTHOR

=cut

use strict;
use Getopt::Long;
use Pod::Usage qw(pod2usage);

$ENV{HARNESS_ACTIVE} = 1;

my @opt = $optpod =~ /B<--(\S+)>/g;
our %Opt;
GetOptions
    (
     \%Opt,
     @opt,
    ) or pod2usage(1);

pod2usage(1) if $Opt{help};

use Sys::Hostname qw(hostname);
use Sort::Versions;
use Time::HiRes qw(sleep);

my $hostname = $Opt{hostname} || hostname();
$Opt{determineversion} //= "method";
for my $det (qw(method variable)) {
    last if $Opt{determineversion} eq $det;
    if (length $Opt{determineversion} < length($det) && $Opt{determineversion} eq substr($det,0,length $Opt{determineversion})) {
        $Opt{determineversion} = $det;
        last;
    }
}
unless ($Opt{determineversion} =~ /^(variable|method)$/) {
    die "Illegal value for --determineversion: $Opt{determineversion}. Must be either method or variable";
}
my $determineversion_is_variable = $Opt{determineversion} eq "variable" ? 1 : 0;

my $perls_config_file = "/home/sand/src/andk/andk-cpan-tools/bin/loop-over-recent.pl.otherperls.$hostname";
my @perls;
my $perlhomepath = "perl";
unless ($hostname eq "k83") {
    $perlhomepath = "host/$hostname";
}
if (open my $fh2, $perls_config_file) {
    while (<$fh2>) {
        chomp;
        s/#.*//; # remove comments
        s/\s+\z//; # remove trailing spaces
        next if /^\s*$/; # remove empty/white lines
        unless (m|/.+/|) {
            s|^|/home/sand/src/perl/repoperls/installed-perls/$perlhomepath/|;
            s|$|/bin/perl|;
        }
        next unless -x $_;
        push @perls, $_;
    }
}
if ($Opt{fshorts}) {
    @perls = grep {
        length($_) <= 8
    } @perls;
}
my %testperl = map {
    ($_ => 1)
} @perls;
my @L = glob("/home/sand/src/perl/repoperls/installed-perls/$perlhomepath/*/*/bin/perl");
if ($Opt{fshorts}) {
    @L = grep {
        my(@bn) = split m{/}, $_; # '/home/sand/src/perl/repoperls/installed-perls/host/k93msid/v5.26.3/da1c/bin/perl'
        length($bn[9]) <= 8;      # 'v5.26.3'
    } @L;
}
my %firstlettermap = ( p=>1, G=>2, v=>3 );
@L = sort {
    my $aXX = substr($a,47+length($perlhomepath));
    my $bXX = substr($b,47+length($perlhomepath));
    my $ret = $firstlettermap{substr($bXX,0,1)} <=> $firstlettermap{substr($aXX,0,1)};
    unless ($ret) {
        for ($aXX,$bXX) {
            s|/.*||;
        }
        if ($aXX =~ /(.+)-RC(\d+)/) {
            if ($bXX =~ /(.+)-RC(\d+)/) {
            } elsif (substr($bXX,0,length($1)) eq $1) {
                $ret = 1;
            }
        } elsif ($bXX =~ /(.+)-RC(\d+)/) {
            if ($aXX =~ /(.+)-RC(\d+)/) {
            } elsif (substr($aXX,0,length($1)) eq $1) {
                $ret = -1;
            }
        }
    }
    unless ($ret) {
        for ($aXX,$bXX) {
            s|^v||;
            s|^perl-||;
            s|-g.*||;
            s|-|.|g;
        }
        $ret = versioncmp($bXX,$aXX);
    }
    unless ($ret) {
        my $aXX = substr($a,47+length($perlhomepath));
        my $bXX = substr($b,47+length($perlhomepath));
        $ret = $bXX cmp $aXX;
    }
    # warn sprintf "%-12s %-12s %s\n", $aXX, $bXX, $ret;
    $ret;
    } @L;
my %globperl = map {
    ($_ => 1)
} @L;
my @testperl_not_in_globperl = grep { !$globperl{$_} } keys %testperl;
unshift @L, @testperl_not_in_globperl;
if ($Opt{max} && $Opt{max} < @L) {
    pop @L while $Opt{max} < @L;
}

my @Showconfig = qw(useithreads usemultiplicity uselongdouble config_args~EBUGGING=both);
for my $showconfig (@{$Opt{show}||[]}) {
    push @Showconfig, $showconfig;
}
$Opt{sprintfwidth} ||= 8;
$Opt{sprintfwidthperl} ||= 28;
# this string is injected into the small program that every perl has to execute:
my $sprintfshowconfig = "%1s%1s%1s%1s " . join " ", ("%-$Opt{sprintfwidth}s") x (@Showconfig-4);
# this string is injected into the small program that every perl has to execute:
my $showconfigvalues = join " ", @Showconfig;
my $modules_differs_from_args = 0;
my @modules = map {
    if (/::/) {
    } elsif (s/\.pm$//) {
        s|/|::|g;
        $modules_differs_from_args++;
    } elsif (/-/) {
        s|-|::|g;
        $modules_differs_from_args++;
    }
    $_;
} map {
    if (/,/) {
        split /,/, $_;
    } else {
        $_;
    }
}
@ARGV;
if ($modules_differs_from_args++) {
    warn "@modules\n";
}
# some day try with IPC::System::Simple for better signal handling? I
# mean for delivering ^C to the right process(es)?
my $SIGNAL = 0;
$SIG{INT} = sub {
    my($sig) = shift;
    warn "Saw signal '$sig', will stop asap";
    $SIGNAL++;
};
my $perlrx = defined $Opt{perlrx} ? qr/$Opt{perlrx}/ : undef;
PERL: for my $p (@L) {
    # my($path) = $p =~ m{/([^/\-]+(?:[\d\.]{4,}|blead)(-(?:\d+-g[[:xdigit:]]+|RC\d+))?)/};
    my $aXX = substr($p,47+length($perlhomepath));
    my($path,$sha) = $aXX =~ m|([^/]+)/([^/]+)/|;
    my $vpath = $path; # version-relevant-path
    $vpath =~ s/-g[[:xdigit:]]{7,10}//; # recently git started to use 10 digits instead of 7
    if ($Opt{minperl}){
        my $vcmp = versioncmp($vpath,$Opt{minperl});
        #warn "vcmp[$vcmp]vpath[$vpath]minperl[$Opt{minperl}]\n";
        next PERL if $vcmp<0;
    }
    if ($Opt{maxperl}){
        my $vcmp = versioncmp($vpath,$Opt{maxperl});
        #warn "vcmp[$vcmp]vpath[$vpath]maxperl[$Opt{maxperl}]\n";
        next PERL if $vcmp>0;
    }
    if (defined $perlrx){
        my $vcmp = $vpath =~ $perlrx;
        #warn "vcmp[$vcmp]vpath[$vpath]perlrx[$Opt{perlrx}]\n";
        next PERL unless $vcmp;
    }
    $testperl{$p} ||= 0;
    my $program = <<'EOP';
    no lib ".";
    my $SIGNAL = 0;
    $SIG{INT} = sub {
      my($sig) = shift;
      warn "Inner perl saw signal '$sig', will stop asap";
      $SIGNAL++;
    };
    my $testperl = shift;
    my $verbose = shift;
    my $path = shift;
    my @v;
    for my $m (@ARGV) {
                  eval "use $m ()";
                  my $v;
                  if ($@) {
                    #eval "require $m";
                    #$v = eval {$m->VERSION();};

                    eval "require Module::Info";
                    if ($@) {
                      $v="!noMI";
                    } else {
                      $v=eval "Module::Info->new_from_module(q{$m})->version";
                      $v = "_n/a_" unless defined $v;
                    }
                  } elsif (<DETERMINEVERSION_IS_VARIABLE> and defined eval "\$$m\::VERSION") {
                    $v = eval "\$$m\::VERSION";
                    $v =~ s/\s//g;
                    $v = 'v_nan' unless length $v;
                  } elsif (defined $m) {
                    local($SIG{__WARN__}) = sub {}; # silence List::Gen
                    $v = eval {$m->VERSION();};
                    if (!defined($v) or !length($v) or $@) {
                      $v = eval "\$$m\::VERSION";
                      $v =~ s/\s//g;
                      $v = 'm_nan' unless length $v;
                    }
                  } else {
                    $v = "?n-a?"
                  }
                  push @v, $verbose ? ("X$m" => $v) : $v
                 };
    if ($SIGNAL){
      warn "Exiting due signal" ;
      exit 42;
    }
    use Config;
    unless ($path) {
      $path=$^X;
      if (length $path > 21){
          for ($path){
              s{^/home/sand/src/perl/repoperls/installed-perls/<PERLHOMEPATH>/}{};
          }
      }
    }
    if (length $path > 21){
      for ($path){
        s{perl-}{p'};
      }
    }
    #no warnings "uninitialized";
    my @showconfigvalues = map {
        if (my($name,$qr)=/(.+?)~(.+)/) {
            my $cqr = eval "qr{$qr}";
            $Config{$name} =~ $qr ? 1 : 0;
        } elsif (!defined($Config{$_})) {
            "u";
        } else {
            "define" eq $Config{$_} ? "D" : $Config{$_}
        }
    } qw(<SHOWCONFIGVALUES>);
    printf "<SPRINTFWIDTHPERL> %s%-8s<SPRINTFSHOWCONFIG> %s\n", $path, $testperl?"*":" ", $Config{version}, @showconfigvalues, join(" ",map {sprintf "<SPRINTFWIDTH>", $_} @v);
EOP
    $program =~ s/<DETERMINEVERSION_IS_VARIABLE>/$determineversion_is_variable/;
    $program =~ s/<SPRINTFSHOWCONFIG>/$sprintfshowconfig/g;
    $program =~ s/<SHOWCONFIGVALUES>/$showconfigvalues/;
    $program =~ s/<SPRINTFWIDTH>/%-$Opt{sprintfwidth}s/;
    $program =~ s/<SPRINTFWIDTHPERL>/%-$Opt{sprintfwidthperl}s/;
    $program =~ s/<PERLHOMEPATH>/$perlhomepath/g;
    sleep 0.01;
    last PERL if $SIGNAL;
    my @system = ($p, '-e', $program, $testperl{$p}, $Opt{verbose}, "$path/$sha", @modules);
    # some day try with IPC::System::Simple for better signal handling?
    my $ret = system @system;
    $SIGNAL++ if $ret>>8 == 42;
    last PERL if $SIGNAL;
    sleep 0.02;
    last PERL if $SIGNAL;
}
