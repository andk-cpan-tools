#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

We are losing annotations somewhere.

I tried to find out whether a recalc enters the annotation like so:

~/src/installed-perls/v5.16.0/4e6d/bin/perl bin/annotations-in-db.pl | awk '{print $1}' | while read d; do redis-cli zincrby analysis:jobqueue:q 1 $d; done

It does, indeed.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

open my $fh, "annotate.txt" or die "Could not open 'annotate.txt': $!";
use Tie::IxHash;
my $anno;
tie %$anno, 'Tie::IxHash';
ANNOLINE: while (<$fh>) {
    chomp;
    next ANNOLINE if /^\s*$/;
    my($dist,$splain) = split " ", $_, 2;
    $anno->{$dist} = $splain;
}
use DBI ();
use YAML::Syck ();
my $dbi = DBI->connect ("dbi:Pg:dbname=analysis");
my $sth = $dbi->prepare("SELECT yaml FROM distcontext WHERE distv=?");
ANNO: for my $distv (keys %$anno) {
    $sth->execute($distv);
    if (my($yaml) = $sth->fetchrow_array) {
        my $y = YAML::Syck::Load($yaml);
        if ($y->{annotation} && $y->{annotation} eq $anno->{$distv}) {
            next ANNO;
        } else {
            substr($anno->{$distv},38) = "..." if length $anno->{$distv}>41;
            printf "%-38s %s\n", $distv, $anno->{$distv};
        }
    } else {
        warn "==> not found in db: $distv\n";
    }
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
