#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

 $0 logfile

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Reads the logfile noise written by cnntp-solver.pl. Does so backwards
because we are interested in the last run. Does so until it has
processed one complete block of anno-nnnn lines. Writes them to
stdout, adding and removing stuff.

Intended as a first step in cleaning up annotate.txt. We only extract
the distros that we believe should be eliminated. When we have
confidence that the list is OK, we pipe it in a separate step like so:

(on analysis):
2015-10-03 02:46  ~/src/installed-perls/v5.16.0/4e6d/bin/perl bin/filter-outdated-from-analysis-logfile.pl bin/cnntp-solver.sh.out | tee ~/filter-outdated-from-analysis-logfile.out

(on k83):
2015-10-03 03:51  ssh andreas@analysis cat filter-outdated-from-analysis-logfile.out | perl bin/eliminate-outdated-annotations.pl


=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

my($logfile) = @ARGV or pod2usage(1);

use File::ReadBackwards;
my $rb = File::ReadBackwards->new($logfile) or die;
my $annoblock;
# use Regexp::Debugger;
LINE: while (defined(my $line = $rb->readline)) {
    $line =~ s!\e\[3[12345](?:;\d+)?m(.*?)\e\[0m!$1!sg;
    # 1503 anno-1508 13-02-05 252: 14:  4 SHARYANTO/Org-Parser-0.30 (<0.37)
    my($cnt,$annolabel,$date,$passes,$fails,$unknowns,$author,$distv,$state) =
        $line =~ m/\s*(\d+)
              \s+(anno-\d+)
                         \s+(\d{2}-\d{2}-\d{2})
                               \s+(\d+):
                                       \s*(\d+):
                                             \s*(\d+)
                                                       \s+([\w\-]+)\/
                                                                 \s*(\S+)
                                                                       \s+\((.+?)\)
            /x;
    if ($state){
        unless (defined $annoblock) {
            $annoblock = $annolabel;
        }
        last LINE unless $annolabel eq $annoblock;
        if ($state =~ /^</) {
            printf "%-53s %s %-12s %-12s %4d\n", $distv, $annolabel, $author, $state, $cnt;
        }
    } elsif (defined $annoblock) {
        if ($line =~ /Already up-to-date/) {
            last LINE;
        }
    }
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
