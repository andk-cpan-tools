use Sys::Hostname;
die "not yet ported to ds8143" if hostname eq "ds8143";

use strict;
use warnings;

=head1 NAME

distlslt - 

=head1 SYNOPSIS

=head1 OPTIONS

=over 2

=cut

my $optpod = <<'=back';

=item --fulldate!

Display date on each line instead of age in days. Currently disables
google chart.

=item --help!

Describe usage.

=item --n=n

Number of tiles to cut from the population. In other words: if n=100
we calculate percentiles, if n=4 we calculate quartiles. Defaults to
40.

=item --distribution_based!

If this option is set then the population is not modules but
distributions on CPAN. See below for a discussion.

=item --with-distro

Accompany each reported line for a module with the distro it is in.

=item --root=s

Directory where the CPAN mirror is on disk.

=item --withcpan!

Look up if we have the module installed for every reported line.

=back

=head1 DESCRIPTION

This is the script I used to prepare the posting I<How fresh is the
CPAN> at http://use.perl.org/~LaPerla/journal/36320 .

It iterates over all modules indexed in
$CPAN/modules/02packages.details.txt (where $CPAN denotes the path to
a CPAN mirror) and measures their age as the age of the distribution
they are contained in. For that the script requires a full CPAN mirror
below the directory given in --root parameter to measure the
modification time of each distro.

The output consists of a table of 1/N-tiles (N as given in --n
parameter) and a measurement of quartiles based on months since the
release.

If Google::Chart is installed we produce a link to a google chart.
It's recommended to call it with '-n=12' because higher numbers are
not that well painted by google.

If the option --distribution_based is given then the population is
distros instead of modules. Interestingly it gives a completely
different picture. The median of the age of a distro is much higher
than the median of the age of a module (2009: 26.8 vs. 17.8). This
indicates that younger distros tend to be complexer and contain more
modules. The single-module-distro is out nowadays.

When I first calculated these statistics 2008-05 I chose modules as
the natural basis. Only later (2009-09) it occurred to me that I could
add the --distribution_based switch to see if there is a difference.
So what is better suited to represent the freshness of the CPAN--a
module based statistics or distribution based one?

I don't know. There are distros that contain many modules and are
still perceived as a single unit. Others are perceived as collection
of modules.

=head1 HISTORY

Calculated 1/40-tiles, of all modules Apr 10, 2008. This was the
output then:

 Found 53394 modules
  1 Thu Apr 10 20:15:53 2008            Test::A8N::File
  2 Sun Apr  6 15:21:12 2008            WebService::ISBNDB::Iterator
  3 Sat Mar 29 17:29:52 2008            VS::Chart::Color
  4 Sun Mar 23 00:35:16 2008            Module::ScanDeps::DataFeed
  5 Tue Mar 11 18:32:08 2008            Curses::UI::Popupmenu
  6 Sat Mar  1 00:15:57 2008            Paranoid
  7 Mon Feb 18 16:58:22 2008            Config::IniHash
  8 Mon Feb  4 15:46:18 2008            Geo::Proj::Japan
  9 Tue Jan 22 08:31:13 2008            Spreadsheet::Engine::Function::WEEKDAY
 10 Thu Jan  3 17:55:37 2008            SystemC::Vregs::Language
 11 Sat Dec 15 20:10:17 2007            Ogre::SceneManager
 12 Thu Nov 22 19:12:58 2007            Alzabo::Create
 13 Sat Oct 27 21:35:43 2007            KinoSearch::Search::HitQueue
 14 Thu Sep 27 22:42:46 2007            Business::PayPal::API::CaptureRequest
 15 Sun Aug 26 01:19:30 2007            Wx::DemoModules::wxStaticText
 16 Sat Jul 28 20:52:11 2007            CommandParser::Vcs
 17 Fri Jun  8 16:31:15 2007            HTML::FromMail::Page
 18 Thu Apr 19 03:44:27 2007            Chemistry::OpenBabel
 19 Sat Mar 17 23:17:11 2007            Java::JCR::Version::VersionHistory
 20 Sun Jan 21 01:42:01 2007            Authen::Passphrase::Clear
 21 Mon Nov 27 20:40:52 2006            CQL::PrefixNode
 22 Fri Sep 22 05:50:59 2006            Template::Magic::Zone
 23 Tue Jul  4 23:41:45 2006            Time::TCB
 24 Fri May 12 14:49:12 2006            Test::Unit::GTestRunner
 25 Tue Feb 28 00:42:21 2006            DBIx::Class::Loader::Generic
 26 Thu Dec  1 17:55:02 2005            IO::Handle::Rewind
 27 Sun Sep  4 02:39:20 2005            Games::Sudoku::OO::Set
 28 Wed Jun  1 23:31:26 2005            Parse::EBNF::Token
 29 Mon Feb 28 05:23:57 2005            Class::StrongSingleton
 30 Tue Nov 30 11:40:10 2004            CGI::Wiki::Formatter::UseMod
 31 Thu Aug 12 22:14:32 2004            DBomb::Meta::OneToMany
 32 Tue Apr 20 20:57:27 2004            Apache::AuthenNIS
 33 Tue Dec 23 10:47:30 2003            Bio::Tools::Run::PiseApplication::descseq
 34 Tue Oct  7 14:00:51 2003            Apache::Profiler
 35 Mon Jun 30 15:52:49 2003            WWW::BookBot::Test
 36 Sun Jan 19 16:04:55 2003            Introspector::MetaInheritance
 37 Fri Sep 20 14:35:14 2002            Anarres::Mud::Driver::Efun::MudOS
 38 Sun Feb 10 06:55:51 2002            Math::MVPoly::Monomial
 39 Tue Apr 17 13:41:26 2001            CGI::Test::Form::Widget::Menu::List
 40 Mon Dec 20 00:05:25 1999            Wizard::LDAP::User

Quartile 1 was about newyear 2008, quartile 2 in January 2007,
quartile 3 in November 2004.

2009-09-05 redid it with -n=12 for an re-post of the article.

 Found 68753 modules
  1    0 Parse::HTTP::UserAgent::Base::Parsers
  2   29 Wx::DocView         
  3   77 Coat::Persistent::Constraint
  4  168 PDF::API2::Basic::PDF::Page
  5  261 DJabberd::XMLElement
  6  375 eBay::API::XML::DataType::LocalMarketAutoAcceptEnabledDefinitionType
  7  533 Bit::MorseSignals::Receiver
  8  743 Data::ICal::TimeZone::Object::Europe::Helsinki
  9 1044 Email::MIME::CreateHTML::Resolver::LWP
 10 1465 Config::Setting::IniParser
 11 1947 SVG::SVG2zinc::Backend::Print
 12 2451 POE::Component::Server::PreforkTCP
 http://chart.apis.google.com/chart?chs=300x120&chd=s:97642zvqiYMAA,93ytojeZUPJEA&cht=lxy&chls=3&chxt=x,r&chxl=0:|5.6m|17.8m|48.8m|1:|0|25|50|75|100&chxp=0,93,78,40&chm=c,FF0000,0,3,10|c,FF0000,0,6,10|c,FF0000,0,9,10&chf=c,ls,90,999999,0.25,AAAAAA,0.25,CCCCCC,0.25,EEEEEE,0.25


Also 2009-09-05 I introduced the --distribution_based switch which
lead to this result (this was an hour later, so not exactly
corresponding lines):

 Found 68754 modules
  1    0     4 GPHAT/Data-Verifier-0.07.tar.gz
  2   59  9201 BBURCH/Net-Int-Stats-1.03.tar.gz
  3  142 15967 BINGOS/POE-Filter-KennySpeak-1.00.tar.gz
  4  269 23441 ANDYA/Set-IntSpan-Fast-1.15.tar.gz
  5  422 30323 ADAMK/HTML-Location-1.03.tar.gz
  6  573 35486 YUKINOBU/Math-Business-Stochastic-0.03.tar.gz
  7  803 41192 BRIANL/Statistics-Lite-3.2.tar.gz
  8 1095 46536 HDP/Proc-Daemontools-Service-0.02.tar.gz
  9 1404 50813 WONKO/CGI-Application-MailPage-1.6.tar.gz
 10 1752 55193 LGODDARD/Image-Maps-Plot-FromLatLong-0.12.tar.gz
 11 2171 60024 MADWOLF/OpenCA-DB-2.0.5.tar.gz
 12 2736 64733 JRED/JaM-1.0.10.tar.gz
 http://chart.apis.google.com/chart?chs=300x120&chd=s:9752zwqjdVMAA,93ytojeZUPJEA&cht=lxy&chls=3&chxt=x,r&chxl=0:|9.0m|26.8m|58.4m|1:|0|25|50|75|100&chxp=0,90,70,35&chm=c,FF0000,0,3,10|c,FF0000,0,6,10|c,FF0000,0,9,10&chf=c,ls,90,999999,0.25,AAAAAA,0.25,CCCCCC,0.25,EEEEEE,0.25

The posting for these results is at ...

=cut


use DateTime;
use Getopt::Long;
use Time::Progress;
use Pod::Usage qw(pod2usage);

our %Opt;
my @opt = $optpod =~ /^=item --(\S+)/mg;
GetOptions(\%Opt,
           @opt,
           ) or pod2usage(2);

if ($Opt{help}) {
    pod2usage(0);
}
if ($Opt{withcpan}) {
    require CPAN;
}
$Opt{n} ||= 40;
die "N[$Opt{n}] must be divideable by 4 to get the graphics correct" if $Opt{n} % 4;
$Opt{root} ||= "/home/ftp/pub/PAUSE";
open my $fh, "zcat $Opt{root}/modules/02packages.details.txt.gz|" or die;
my(%module_age,%distro_age,%distro_containing_module,%modules_contained_in_distro);
my $state = "header";
my $current_line = 0;
my $tp;
if (-t *STDOUT) {
    $tp = Time::Progress->new();
}
my $lines;
$| = 1;
while (<$fh>) {
    if ($state eq "header") {
        if (/^\s*$/){
            $state = "body";
            next;
        } elsif (/^Line-Count:\s*(\d+)/) {
            $lines = $1;
            if ($tp) {
                $tp->attr( min => 1, max => $lines );
            } else {
                print "Found $lines modules";
            }
        }
    } elsif ($state eq "body") {
        chomp;
        $current_line++;
        my($m,$v,$d) = split " ", $_;
        unless (-e "$Opt{root}/authors/id/$d"){
            warn "could not find '$Opt{root}/authors/id/$d' for '$m'";
            next;
        }
        my $module_age = $distro_age{$d} ||= -M _;
        $module_age{$m} = $module_age;
        $distro_containing_module{$m} = $d;
        my $a = $modules_contained_in_distro{$d} ||= [];
        push @$a, $m;
        if ($lines==$current_line || !($current_line % 100)) {
            my $formatted_current_line = sprintf "%8d", $current_line;
            print $tp->report("\r$formatted_current_line %p over: %l s, left %e s; ETA: %f", $current_line ) if $tp;
        }
    } else {
        die "illegal state $state";
    }
}
print "\n";
my @items;
if ($Opt{distribution_based}) {
    @items = sort { $distro_age{$a} <=> $distro_age{$b} } keys %distro_age;
} else {
    @items = sort { $module_age{$a} <=> $module_age{$b} } keys %module_age;
}

my $painted = 0;
if ($Opt{withcpan}) {
    CPAN::Index->reload;
}
my $now = DateTime->now;
my $value_sets = [];
my @t_index;
my $modules_covered = 0;
for my $i (0..$#items) {
    if ($Opt{distribution_based}) {
        my $modules_contained = @{$modules_contained_in_distro{$items[$i]}};
        $modules_covered += $modules_contained;
    }
    while (($painted/$Opt{n}) < ($i/@items)) {
        my $mtime;
        if ($Opt{distribution_based}) {
            $mtime = $^T-86400*$distro_age{$items[$i]};
        } else {
            my $module_age = $module_age{$items[$i]};
            $mtime = $^T-86400*$module_age;
        }
        my $dt = DateTime->from_epoch(epoch => $mtime);
        my $lt = $dt->ymd;
        my $age_days = int(($now->epoch - $dt->epoch)/86400);
        my $have = "";
        my $have_format = "%s";
        my $display_date = $age_days;
        my $date_format = "%4d";
        if ($Opt{withcpan} &&! $Opt{distribution_based}) {
            $have_format = " %-5s";
            my $mod = CPAN::Shell->expand("Module",$items[$i]);
            $have = CPAN::Shell->expand("Module",$items[$i])->inst_version if $mod;
            $have = "" unless defined $have;
        }
        if ($Opt{fulldate}) {
            $date_format = "%-10s";
            $display_date = $lt;
        }
        $painted++;
        my $fullquart = int($Opt{n}/4+.01);
        if ($painted>1 && ((($painted-1) % $fullquart) == 0)) {
            push @t_index, $painted-1;
        }
        if ($Opt{distribution_based}) {
            printf "%2d $date_format$have_format %5d %-20s\n", $painted, $display_date, $have, $modules_covered, substr($items[$i],5);
        } else {
            my $distro = "";
            if ($Opt{"with-distro"}) {
                $distro = sprintf " %s", substr($distro_containing_module{$items[$i]},5);
            }
            printf "%2d $date_format$have_format %-20s%s\n", $painted, $display_date, $have, $items[$i], $distro;
        }
        push @{$value_sets->[0]}, $Opt{fulldate} ? $display_date : -$display_date;
        push @{$value_sets->[1]}, 1-(($painted-1)/$Opt{n});
    }
}
XAXIS: push @{$value_sets->[0]}, $value_sets->[0][-1]; # must use the 0 for proper scaling
YAXIS: push @{$value_sets->[1]}, 0;
my @txlabel;
unless ($Opt{fulldate}) {
    my $display_months = 1;
    if ($display_months) {
        @txlabel = map { sprintf "%.1fm", -$_/30 } @{$value_sets->[0]}[@t_index];
    } else {
        @txlabel = map { sprintf "%dd", -$_ } @{$value_sets->[0]}[@t_index];
    }
    warn sprintf "DEBUG: t_index[%s]txlabel[%s]modules[%d]distros[%d]", join(",",@t_index), join(",",@txlabel), scalar keys %distro_containing_module, scalar keys %distro_age;
    my $HAVE_GOOGLE_CHARTS;
    my @txpos;
    unless ($Opt{fulldate}) {
        $HAVE_GOOGLE_CHARTS = eval { require Google::Chart; 1; };
        use List::Util qw(min max);
        for my $vs (@$value_sets) {
            my $min = min @$vs;
            my $max = max @$vs;
            my $range = $max - $min;
            $vs = [ map { int(100 * ($_ - $min)/$range) } @$vs ];
        }
        @txpos = @{$value_sets->[0]}[@t_index];
    }
    if ($HAVE_GOOGLE_CHARTS) {
        my $chart = Google::Chart->new(
                                       type_name => 'type_line_xy',
                                       set_size  => [ 300, 120 ],
                                       data_spec => {
                                                     encoding  => 'data_simple_encoding',
                                                     max_value => 100,
                                                     value_sets => $value_sets,
                                                    },
                                      );
        print $chart->get_url, "&chls=3&chxt=x,r&chxl=0:|$txlabel[0]|$txlabel[1]|$txlabel[2]|1:|0|25|50|75|100&chxp=0,$txpos[0],$txpos[1],$txpos[2]&chm=c,FF0000,0,$t_index[0],10|c,FF0000,0,$t_index[1],10|c,FF0000,0,$t_index[2],10&chf=c,ls,90,999999,0.25,AAAAAA,0.25,CCCCCC,0.25,EEEEEE,0.25\n";
    }
}

