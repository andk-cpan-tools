#!/home/src/perl/repoperls/installed-perls/perl/pVNtS9N/perl-5.8.0@32642/bin/perl

use Getopt::Long;

my %Opt;
GetOptions(\%Opt,
           "dir=s",
          ) or die "Usage";

$ENV{USER}="andk"; # fill in your name
$ENV{RSYNC_PASSWORD} = shift or die; # fill in your passwd

$Opt{dir} ||= "authors";
use strict;
use File::Rsync::Mirror::Recent;
my $statusfile = "/root/rmirror-status-$Opt{dir}.state";
my $rrr = File::Rsync::Mirror::Recent->new
    (
     ignore_link_stat_errors => 1,
     localroot => "/home/ftp/pub/PAUSE/$Opt{dir}",
     remote => "pause.perl.org::PAUSE/$Opt{dir}/RECENT.recent",
     max_files_per_connection => 7591,
     rsync_options =>
     {
      port => 8732, # only for PAUSE
      compress => 0,
      links => 1,
      times => 1,
      checksum => 0,
      'omit-dir-times'  => 1, # not available before rsync 3.0.3
     },
     tempdir => "/home/ftp/tmp",
     ttl => 60,
     verbose => 1,
     verboselog => "/var/log/rmirror-pause.log",
     runstatusfile => $statusfile,
     # _logfilefordone => "recent-rmirror-donelog-$tree.log",
    );
$rrr->rmirror
    (
     # "skip-deletes" => 1,
     loop => 1,
    );
