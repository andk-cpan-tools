use strict;
my $wait_before_reboot = 4*86400;
my $time_to_reboot;
while (){
  my $loggedin = `w -hs | wc -l`;
  if ($loggedin > 0){
    $time_to_reboot = time + $wait_before_reboot;
  } else {
    my $saw_accepted_publickey = `grep -c 'ccepted publickey' /var/log/auth.log`;
    if ($saw_accepted_publickey > 0){
      $time_to_reboot = time + $wait_before_reboot;
    } else {
      if ( time > $time_to_reboot ) {
        system "shutdown", "-r", "now", __FILE__;
      }
    }
  }
  sleep 60
}
