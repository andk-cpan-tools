#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--db=s>

DSN. Defaults to C<dbi:SQLite:dbname=$workdir/quickaccess.db> where
$workdir comes from the configuration.

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Just the quickaccess db and there the table distcontext: migrate from
sqlite to sqlite. We did this only to get a bug in the schema fixed.
The migration to postgres was not yet on the agenda, on the horizon
yes, but not on agenda.

The original table had greenish as a text field, we now make it an
integer field. This cannot be achieved with their "alter table"
support, so we must do it in a script.

Before we started the scrpt, we had this histogram:

  sqlite> SELECT greenish, count(*) FROM distcontext group by greenish ;
  |3432
  -1|36
  0|3617
  1|14134
  2|21649
  3|6689

We would expect that we have 7049 records with 0 afterwards and all
others equal.

On the first run on Nov 20th I was interrupting and delaying this to a
later time because it was too slow for the middle of the night. Today
I added a remaining seconds indicator and after a few minutes it
reaches a stable 10800 seconds, so I'll do the finishing at about
11:00 which looks good.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

our $SIGNAL = 0;
$SIG{INT} = $SIG{TERM} = sub { my $sig = shift; warn "Caught $sig\n"; $SIGNAL=1 };

use DBI;
use Time::HiRes qw(sleep time);
use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;
my($workdir,$cpan_home,$ext_src);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
    $cpan_home = $CPAN::Blame::Config::Cnntp::Config->{cpan_home};
    $ext_src = $CPAN::Blame::Config::Cnntp::Config->{ext_src};
}

$Opt{db}  ||= "dbi:SQLite:dbname=$workdir/quickaccess.db";

sub my_do_query {
    my($dbi,$sql,@args) = @_;
    my $success = $dbi->do($sql,undef,@args);
    unless ($success) {
        my $err = $dbi->errstr;
        warn sprintf
            (
             "Warning: error occurred while executing sql[%s]with args[%s]: %s",
             $sql,
             join(":",map { defined $_ ? "'$_'" : "<undef>"} @args),
             $err,
            );
    }
    return $success;
}

my $sldbi = DBI->connect ($Opt{db}); # return a dbi handle
my $slsth0 = $sldbi->prepare("SELECT count(*) from distcontext");
$slsth0->execute();
my($slcnt) = $slsth0->fetchrow_array();

my $slsth = $sldbi->prepare("SELECT distv,yaml,greenish,dist from distcontext order by distv");
$slsth->execute();
my $sql = "CREATE TABLE IF NOT EXISTS ngdistcontext (
distv text primary key,
yaml text,
greenish integer,
dist text)";
my_do_query($sldbi, $sql);
my $slsth2 = $sldbi->prepare("INSERT INTO ngdistcontext
 (distv,yaml,greenish,dist) values
 (?,     ?,     ?,       ?)");

my $i = 0;
my $lastreport = my $starttime = time;
$|=1;
print "\n";
ROW: while (my(@row) = $slsth->fetchrow_array) {
    $row[2] ||= 0;
    unless ($slsth2->execute(@row)) {
        warn sprintf "ALERT: error inserting row/id[%s]: %s\n", $row[0], $sldbi->errstr;
        last ROW;
    }
    ++$i;
    my $eta = int((time - $^T)*($slcnt - $i)/$i);
    printf "\r%d/%d  remaining seconds: %d    ", $i, $slcnt, $eta;
    last ROW if $SIGNAL;
}
print "\n";
my $tooktime = time - $starttime;
warn "records transferred[$i] tooktime[$tooktime]\n";

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
