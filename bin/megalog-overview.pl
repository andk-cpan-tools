#!/usr/bin/perl -l

# use 5.010;
use strict;
use warnings;

=head1 NAME

megalog-overview.pl

=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--debug=s>

String can be C<reps>, nothing else. Note: the time estimation for
individual distros is buggy, don't trust them!

=item B<--help|h!>

This help

=item B<--ml=s@>

Filename to parse. If missing, we glob and then use --tail on the
globbed filenames.

=item B<--tail=i>

Defaults to 20. Show only the last logfiles

Misnomer since the day we reversed the result of the glob.

=item B<--terse!>

Do not parse the whole files, just report the date and perl.

=back

=head1 DESCRIPTION

Get some stats on a megalog file: seconds per process, total seconds,
number of lines, number of ticks.

Usually called from the cpanpm directory with

    perl ~k/sources/CPAN/andk-cpan-tools/bin/megalog-overview.pl --tail=3

=head1 BUGS

The option --debug=reps does not reflect the stack of activities: if
Job A depends on B, then we measure ABBA: A-start, B-start, B-end,
A-end and the running time for A includes the running time for B in
the end.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);
use List::Util qw(max sum);
use Pod::Usage qw(pod2usage);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{tail} ||= 20;

my %F;
my @ml;
if ($Opt{ml} && @{$Opt{ml}}) {
    @ml = @{$Opt{ml}};
} else {
    @ml = glob("megalog-[0-9]*.log");
    if ($Opt{tail}) {
        shift @ml while @ml > $Opt{tail};
    }
}
my @reps_keys = qw/date slot t0 t1 diff name/;
my $reps_sprintf = "# %s %s %6d %6d %4d %s\n";
for my $ml (reverse @ml) {
    my $identitylinestyle = undef; # { "CPAN.pm: Building" => "cpanpm", "\e.+?\s\s" => "color" }
    open my $fh, $ml or die "Could not open '$ml': $!";
    my %seen;
    my %S;
    my %P; my $P = "00";
    # XXX initializing $process_cnt is done to avoid unini noise for
    # the case we run without monitor. All stats are broken when we do
    # that, so we should so something else instead. Try with
    # megalog-quarter-2018-12-15T17:50:22 on bionic.
    my $process_cnt = "00"; # '00','01',..,'04'
    my($date) = $ml =~ /megalog-(.+)\.log/;
    $S{adate} = $date;
    my($prev_report,$prev_0time) = ({}, 0);
    while (<$fh>) {
        $S{blines}++ unless $Opt{terse};
        my($reps); # filled exactly once per distro, as soon as we recognize it
        if (!$S{aperl} and m!^perl\|->\s+.+?/
                             (?: perl | host/([^/]+) )
                             /
                             ( [^/]+ / [^/]+ )
                             /bin/perl
                            !x) {
            if ($1) {
                $S{aperl} = "$1/$2"; # the normal case
            } else {
                $S{aperl} = "k83/$2"; # rarely true
            }
            last if $Opt{terse};
        } elsif (/=monitoring proc/) {
            # ==========monitoring proc 18571 perl v5.15.6-585-gc6fb3f6/a2da secs 30001.0000=======
            # $S{btix}++;
            if (/=monitoring proc (\d+) perl\s+\S+\s+secs (\d+\.\d+)==/) {
                my $process = $1;
                my $time = $2;
                if ($time < $prev_0time) {
                    $prev_report->{t1} = $prev_0time;
                    $prev_0time = 0;
                } else {
                    $prev_0time = $time;
                }
                $process_cnt = $P{$process} ||= $P++;
                $S{sprintf "p-%02d-0time", $process_cnt} = $time;
            }
        } elsif (/^\s+CPAN.pm: Building .\/..\/(\S+)/) { # CPAN.pm up to 2.xx
            # CPAN.pm: Building S/SR/SRI/Mojolicious-3.18.tar.gz
            my $what = $1;
            $identitylinestyle ||= "cpanpm";
            if ($identitylinestyle eq "cpanpm") {
                $S{sprintf "p-%02d-3last", $process_cnt} = $what;
                unless ( $seen{$what}++ ){
                    $S{sprintf "p-%02d-1reps", $process_cnt}++;
                    $reps = $what;
                }
            }
        } elsif (m{^\e.+?\s\s([A-Z-0-9]+/\S+?)(?:\e|$)}) { # CPAN.pm 2.03, but also before
            # <ansicolor>  TOKUHIROM/Test-Requires-0.07.tar.gz</ansicolor>
            my $what = $1;
            $identitylinestyle ||= "color";
            if ($identitylinestyle eq "color") {
                $S{sprintf "p-%02d-3last", $process_cnt} = $what;
                unless ( $seen{$what}++ ){
                    $S{sprintf "p-%02d-1reps", $process_cnt}++;
                    $reps = $what;
                }
            }
        }
        if ($Opt{debug} && $Opt{debug} eq "reps" && $reps) {
            my $this_0time = $S{"p-$process_cnt-0time"};
            if ($prev_report->{name}) {
                $prev_report->{t1} ||= $this_0time;
                $prev_report->{diff} = $prev_report->{t1} - $prev_report->{t0};
                printf $reps_sprintf, @{$prev_report}{@reps_keys};
            }
            $prev_report = { date => $date, slot => $process_cnt, t0 => $this_0time, name => $reps };
        }
    }
    if ($Opt{debug} && $Opt{debug} eq "reps") {
        $prev_report->{t1} = $S{"p-$process_cnt-0time"};
        $prev_report->{diff} = $prev_report->{t1} - $prev_report->{t0};
        printf $reps_sprintf, @{$prev_report}{@reps_keys};
    }
    unless ($Opt{terse}){
        $S{totalsecs} = sum map { $S{$_} } grep { /^p-\d+-0time$/ } keys %S;
    }
    no warnings 'uninitialized';
    print join " ",
        map { $F{$_}=max($F{$_},length($S{$_}));
              sprintf("%s=%*s", $_, $F{$_}, $S{$_}) }
            sort grep { !/^p-/ } keys %S;
    for my $kp ( "00"..$P ) {
        last unless exists $S{"p-$kp-0time"};
        $S{"p-$kp-2avg"} = sprintf "%.1f", $S{"p-$kp-1reps"} ? $S{"p-$kp-0time"}/$S{"p-$kp-1reps"} : 0;
        printf " %s:", $kp;
        for my $k (sort grep { /^p-$kp-/ } keys %S) {
            my($kl) = $k =~ /.+-\d*(\w+)/;
            my($v);
            if ($kl eq "time") {
                $v = sprintf "%d", $S{$k};
            } else {
                $v = $S{$k};
            }
            printf(" %s=%s", $kl, $v)
        }
        print "";
    }
    print "" unless $Opt{terse};
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
