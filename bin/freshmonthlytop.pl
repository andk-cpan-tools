#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--top=i>

Number of top players. Defaults to 4.

=back

=head1 DESCRIPTION



=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{top} //= 4;

use strict;

use DBI;

my $dbh = DBI->connect("dbi:Pg:dbname=analysis","andreas","");

my @tables = sort map { /^public\.cpanstats_(\d{6})$/ ? $1 : () } $dbh->tables("","public");

binmode \*STDOUT, ":utf8";

for my $pd (@tables[-3..-1]){
    my $sth = $dbh->prepare("select count(*) from cpanstats_$pd");
    $sth->execute();
    my($cnt) = $sth->fetchrow_array;
    printf "TOTAL %s %9d\n", $pd, $cnt;
}
{
    # ALL previous month
    my($sth2) = $dbh->prepare("select * from (select tester, count(*) as cnt from cpanstats where postdate = ? group by tester) x order by cnt desc limit $Opt{top}");
    $sth2->execute($tables[-2]);
    my($diff,$L,$i);
    while (my(@row) = $sth2->fetchrow_array){
        if (defined $L){
            $diff = $L - $row[1]
        }
        printf "ALLpm %4d %-110s %9d %6s\n", ++$i, @row, defined($diff) ? $diff : "";
        $L=$row[1]
    }
}
{
    # LINUX this month
    my($sth2) = $dbh->prepare("select * from (select tester, count(*) as cnt from cpanstats where postdate = ? and osname = ? group by tester) x order by cnt desc limit $Opt{top}");
    $sth2->execute($tables[-1],"linux");
    my($diff,$L,$i);
    while (my(@row) = $sth2->fetchrow_array){
        if (defined $L){ $diff = $L - $row[1] } printf "LINtm %4d %-110s %9d %6s\n", ++$i, @row, defined($diff) ? $diff : "";
        $L=$row[1]
    }
}
{
    # ALL this month
    my($sth2) = $dbh->prepare("select * from (select tester, count(*) as cnt from cpanstats where postdate = ? group by tester) x order by cnt desc limit $Opt{top}");
    $sth2->execute($tables[-1]);
    my($diff,$L,$i);
    while (my(@row) = $sth2->fetchrow_array){
        if (defined $L){
            $diff = $L - $row[1]
        }
        printf "ALLtm %4d %-110s %9d %6s\n", ++$i, @row, defined($diff) ? $diff : "";
        $L=$row[1]
    }
}
{
    my($sth2) = $dbh->prepare("select now()");
    $sth2->execute();
    while (my(@row) = $sth2->fetchrow_array){
        printf "  NOW %s\n", @row;
    }
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
