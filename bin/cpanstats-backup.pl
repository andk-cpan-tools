#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--dir=s>

Backup directory, mandatory

=back

=head1 DESCRIPTION

=head1 HISTORY

Circa 2022-06-03T18:22 was called on sidpg the command

  perl ~/src/andk-cpan-tools/bin/cpanstats-backup.pl --dir=~/var/cpanstats-backup

It was running for about 02:15 hours and produced 276 files;

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use File::Glob qw(:bsd_glob);
use File::stat;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
unless ($Opt{dir}) {
    warn "missing parameter dir";
    pod2usage(1);
}

use strict;
use DBI;
use Time::HiRes qw(gettimeofday tv_interval);
my $dbh = DBI->connect("dbi:Pg:dbname=analysis");
my @postdates = sort map { /^public\.cpanstats_(\d{6})$/ ? $1 : () } $dbh->tables("","public");
my $I = scalar @postdates;
my %postdates = map { $postdates[$_] => $_ } 0..$#postdates;
POSTDATE: for my $i (0..$#postdates){
  my $postdate = $postdates[$i];
  my $next_postdate = $i<$#postdates ? $postdates[$i+1] : "999999";
  my $t0 = [gettimeofday];
  my $out = bsd_glob("$Opt{dir}/pg_dump-cpanstats-$postdate.pg_dump.xz", GLOB_TILDE);
  if (-e $out && -s _) {
      my $st = stat $out;
      my $mtime = $st->mtime;
      my @mtime = localtime($mtime);
      $mtime[4]++;
      $mtime[5]+=1900;
      my $backup_postdate = sprintf "%04d%02d", @mtime[5,4];
      if ($backup_postdate > $next_postdate) {
          warn "$postdate has good backup\n";
          next POSTDATE;
      } else {
          warn "$postdate has backup, but maybe not old enough\n";
      }
  } else {
      warn "$postdate has no backup $out\n";
  }
  my $system = "pg_dump analysis -t cpanstats_$postdate | xz > $out";
  0 == system $system or die "something went wrong during $postdate";
  my $size = -s $out;
  my $t1 = [gettimeofday];
  warn sprintf "%*d/%d %s %6.2f\n", length($I), 1+$i, $I, $postdate, tv_interval($t0, $t1);
}
my $system = "pg_dump analysis -T 'cpanstats_*' | xz > $Opt{dir}/pg_dump-cpanstats-000000.pg_dump.xz";
0 == system $system or die "something went wrong during '$system'";


# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
