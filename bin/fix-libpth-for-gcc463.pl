#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION



=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

use Config;
use POSIX ();
my $date = POSIX::strftime "%FT%T", localtime;
if ($Config{gccversion} eq "4.4.5") {
    my $configpm = $INC{"Config.pm"} or die;
    open my $fh, $configpm or die;
    open my $fh2, ">", "$configpm.new" or die;
    while (<$fh>) {
        if (/^(\s+libpth => )'[^']+',$/) {
            print $fh2 "$1'/usr/local/lib /lib/x86_64-linux-gnu /lib/../lib /usr/lib/x86_64-linux-gnu /usr/lib/../lib /lib /usr/lib /lib64 /usr/lib64',\n";
        } else {
            print $fh2 $_;
        }
    }
    chmod 0644, $configpm or die;
    rename $configpm, "$configpm-$date" or die;
    rename "$configpm.new", $configpm or die;
    chmod 0444, $configpm or die;
} else {
    warn "Nothing to do, gccversion is '$Config{gccversion}'";
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
