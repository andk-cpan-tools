#!/home/src/perl/repoperls/installed-perls/perl/pVNtS9N/perl-5.8.0@32642/bin/perl -d

# use 5.010;
use strict;
use warnings;

=head1 NAME

cnntp-contemplate - combine datacollections and annotations to recommendations

=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--group=s>

defaults to C<perl.cpan.testers>.

=item B<--workdir=s>

defaults to something like ~/var/...

=back

=head1 BUGS

some code copy and pasted from cnntp-scrape...

=head1 DESCRIPTION

show the most recent FAILs on cpantesters. Compare to annotations.txt
which I maintain manually.

=head1 TODO

Is latest?

Release date

link to matrix

open tickets

result of -solve

my own results

=cut

use lib "$ENV{HOME}/sources/CPAN/ghub/cpanpm/lib";
use CPAN;
use DateTime;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use LWP::UserAgent;
use List::Util qw(max min reduce sum);
use Pod::Usage qw(pod2usage);
use Term::ANSIColor;
use URI ();
use XML::LibXML;
use YAML::Syck;

our %Opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

$Opt{group} ||= "perl.cpan.testers";
$Opt{baseuri} ||= "http://www.nntp.perl.org/group";

my $workdir = $Opt{workdir} ||= File::Spec->catdir(vardir(\%Opt),"workdir");
chdir $workdir
    or die("Couldn't change to $workdir: $!");

$CPAN::Frontend = "CPAN::Shell"; # alt: devnull
{
    package CPAN::Shell::devnull;
    sub myprint { return; }
    sub mywarn { return; }
    sub mydie { my($self,$why) = @_; warn "Caught error[$why]; continuing"; return; }
    sub mysleep { return; }
}
CPAN::HandleConfig->load;
CPAN::Shell::setup_output;
CPAN::Index->reload;

my $db = read_local_db_for_month(\%Opt);
my $anno = read_annotations(\%Opt);
my $max = 50;
my $i = 0;
my %seen;
my $highestreported = 0;
my $recent100000 = [];
ARTICLE: for my $k (sort {$b <=> $a} keys %{$db->{articles}}) {
    my $article = $db->{articles}{$k};
    my($ok,$dist) = ok_value_and_distro($article);
    unless ($highestreported++) {
        print "((($k))) $ok $dist\n";
    }
    next ARTICLE unless "FAIL" eq $ok;
    next ARTICLE if $seen{$dist}++;
    if (exists $anno->{$dist}) {
        my $anno = $anno->{$dist};
        substr($anno,36) = "..." if length($anno) > 39;
        printf "%s %8d skip %s (%s)\n", "-"x2, $k, $dist, $anno;
    } else {
        my @ret = cpan_lookup_dist($dist);
        my($upload_date,$author,$passfail_overview);
        if (@ret) {
            if (1 == @ret) {
                # $DB::single=1;
                unless ($upload_date = $ret[0]->{UPLOAD_DATE}) {
                    my($id) = $ret[0]->{ID};
                    $id =~ s|^./../||;
                    local($CPAN::Frontend) = "CPAN::Shell::devnull";
                    CPAN::Shell->ls($id); # find out the upload date!
                    $upload_date = $ret[0]->upload_date || "???";
                }
                $author = substr($ret[0]{ID},5);
                $author =~ s|/.*||;
                $passfail_overview = passfail_overview($dist,$db,$recent100000);
            } else {
                die "ALERT: not reached anymore";
            }
        } else {
            next ARTICLE;
            $author = "???";
            $upload_date = "....-..-..";
            $passfail_overview = "--:--:--";
        }
        ++$i;
        for ($article->{from}) {
            substr($_,56)="..." if length>58;
        }
        my $ddist = length($dist)<=52 ? $dist : (substr($dist,0,49)."...");
        if (my($y,$m,$d) = $upload_date =~ /(\d+)-(\d+)-(\d+)/) {

            my $dtu = DateTime->new(year => $y, month => $m, day => $d, time_zone => "floating");
            my $dtn = DateTime->now;
            my $dur = $dtn->subtract_datetime_absolute($dtu);
            my $delta = $dur->delta_seconds;
            my $color_on = "";
            # $DB::single++;
            if ($delta >= 3*365.25*86400) {
                $color_on = Term::ANSIColor::color("yellow");
            } elsif ($delta >= 3*86400) {
                $color_on = Term::ANSIColor::color("green");
            } else {
                $color_on = Term::ANSIColor::color("red");
            }
            my $color_off = Term::ANSIColor::color("reset");
            $upload_date = "$color_on$upload_date$color_off";
        }
        printf(
               "%2d %8d %s %s %s/%s\n",
               $i, $k,
               $upload_date,
               $passfail_overview,
               $author,
               $ddist,
              );
    }
    last if $max == $i;
}

sub ok_value_and_distro {
    my($article) = @_;
    $article->{subject} =~ /(\S+)\s+(\S+)/;
}

sub passfail_overview {
    my($dist,$db,$recent100000) = @_;
    my %summary;
    unless (@$recent100000) {
        while (my($k,$v) = each %{$db->{articles}}) {
            # in january we had ~200000 articles
            # if we limit to 100000, then we have two week
            next if $k < $db->{max_article} - 100000;
            next if $v->{subject} eq "Oops";
            my($ok,$fdist) = ok_value_and_distro($v);
            unless (defined $fdist) {
                warn "dist[$dist]fdist[$fdist]";
                $DB::single++;
            }
            push @$recent100000, {
                                  subject => $v->{subject},
                                  from    => $v->{from},
                                  ok      => $ok,
                                  dist    => $fdist,
                                 };
        }
    }
    for my $v (@$recent100000) {
        next unless $dist eq $v->{dist}; # XXX very inefficient
        $summary{$v->{ok}}++;
    }
    my $pass = delete $summary{PASS};
    my $fail = delete $summary{FAIL};
    my $other = sum values %summary;
    my $color_on = "";
    my $color_off = "";
    $pass ||= 0;
    $fail ||= 0;
    if (0 == $pass) {
        $color_on = Term::ANSIColor::color("red");
        $color_off = Term::ANSIColor::color("reset");
    } elsif ($pass>=3 && $fail>=3) {
        $color_on = Term::ANSIColor::color("green");
        $color_off = Term::ANSIColor::color("reset");
    }
    sprintf "%s%2d:%2d:%2d%s", $color_on, $pass, $fail, $other||0, $color_off;
}

sub cpan_lookup_dist {
    my($dist) = @_;
    my @ret = CPAN::Shell->expand("Distribution", "/\\/$dist/");
    my $best = reduce {
        my $la = length(mybasename($a->{ID}));
        my $lb = length(mybasename($b->{ID}));
        $la < $lb ? $a : $b
    } @ret;
    return $best ? $best : ();
}

sub mybasename {
    my($p) = @_;
    $p =~ s|.*/||;
    return $p;
}

sub dbpath {
    my($opt,$month) = @_;
    sprintf "%s/%s.yml", vardir($opt), $month;
}

sub vardir {
    my($opt) = @_;
    sprintf
        (
         "%s/var/cnntp-scrape/%s",
         $ENV{HOME},
         $opt->{group},
        );
}

sub read_local_db_for_month {
    my($opt) = @_;
    # take the max. Note: the server runs probably in some pacific
    # timezone, we really do not want to know.
    opendir my $dh, vardir($opt);
    my $current = reduce { $b gt $a ? $b : $a } grep {/^\d+-\d+\.yml$/} readdir $dh;
    my($month) = $current =~ /^(\d+-\d+)\.yml$/;
    my $path = dbpath($opt,$month);
    my $y;
    if (-e $path) {
        $y = YAML::Syck::LoadFile($path);
    } else {
        $y = +{};
    }
    if (scalar keys %{$y->{articles}} < 100000) {
        rewinddir $dh;
        my $current2 = reduce { $b gt $a ? $b : $a }
            grep {/^\d+-\d+\.yml$/ && $_ ne "$month.yml"} readdir $dh;
        my($month2) = $current2 =~ /^(\d+-\d+)\.yml$/;
        my $path2 = dbpath($opt,$month2);
        my $y2;
        if (-e $path2) {
            $y2 = YAML::Syck::LoadFile($path2);
            while (my($k,$v) = each %{$y2->{articles}}) {
                $y->{articles}{$k} = $v;
            }
        }
    }
    return $y;
}

sub read_annotations {
    my $fh;
    unless (open $fh, "../annotate.txt") {
        $DB::single=1;
        die "Could not";
    }

    my $anno = {};
    while (<$fh>) {
        chomp;
        next if /^\s*$/;
        my($dist,$splain) = split " ", $_, 2;
        $anno->{$dist} = $splain;
    }
    return $anno;
}

1;
