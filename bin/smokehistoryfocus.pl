#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--bbc!>

Only show potential BBCs; The algorithm for recognizing BBCs is that
the oldest perl must have 100% success and from there we must face a
decline. Problematic are cases where we have decline and improvements
mixed in the middle. See also skipmiddle.

=item B<--help|h!>

This help

=item B<--matrixlink!>

Add a line to the output with the link to the matrix

=item B<--minperl=f>

Disregards all perls lower than that

=item B<--skipanno!>

Skip things that have a line in annotated.txt

=item B<--skipmiddle>

Only pay attention to the minimum perl and the maxium perl. Combined
with bbc shows also bbc candidates that had some improvement between
the oldest and newest release.

=item B<--skipbefore=s>

Defaults to C<2006>. If the relase of the distro was before this date
(in iso format), then we do not display it in the list.

=back

=head1 DESCRIPTION

Reading a file written by smokehistory with contents such as

  Spoon-0.24: 
    - /home/sand/var/ctr/done/archive/S/Spoon/pass.Spoon-0.24.x86_64-linux-ld.2.6.32-5-amd64.1299821028.15026.rpt
    - /home/sand/var/ctr/done/archive/S/Spoon/pass.Spoon-0.24.x86_64-linux-thread-multi-ld.2.6.32-5-amd64.1295090374.3806.rpt
    - /home/sand/var/ctr/done/archive/S/Spoon/pass.Spoon-0.24.x86_64-linux-thread-multi.2.6.32-5-amd64.1302363043.25077.rpt
    - /home/sand/var/ctr/done/archive/S/Spoon/pass.Spoon-0.24.x86_64-linux-thread-multi-ld.2.6.32-5-amd64.1324424772.2901.rpt

which means nothing but report files per distro that already represent
this filtered set: distro is the most recent one of that distroname
(here: Spoon) and has mixed results.

Per default we just write summaries to STDOUT like this one:

  (32) JJORE/Alien-Judy-0.26.tar.gz [20110120T014407Z]:
  5.015004     5:1
  5.015005     4:0
  5.015006     4:0

Since the default run spits out >1000 reports we often use the --bbc
switch, often combined with --skipmiddle.

=head1 SEE ALSO

smokehistory.pl

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(basename dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);
use YAML::Syck;

our %Opt;
lock_keys %Opt, map { /([^=\!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

$Opt{skipbefore} ||= "2006";

my %anno;
if ($Opt{skipanno}) {
    open my $fh, "annotate.txt" or die;
    while (<$fh>) {
        my($dist,$anno) = /^(\S+)\s+(.*)/;
        $anno{$dist} = $anno;
        $dist =~ s/_//g;
        $anno{$dist} = $anno; # prevent DCONWAY/List-Maker-0.003_000.tar.gz to appear
    }
}

my $y = YAML::Syck::LoadFile "bin/smokehistory.pl.yml";
my $i = 0;
VDISTRO: for my $vdistro (sort keys %$y) { # ACME-Require-Require-v0.0.1
    if ($Opt{skipanno} and $anno{$vdistro}) {
        next VDISTRO;
    }
    my $reports = $y->{$vdistro};
    my %result;
    my $canondistro;
 REPORT: for my $r (@$reports) {
        my($result) = basename($r) =~ m{(\w+)};
        open my $fh, $r or die;
        my $v;
        while (<$fh>) {
            if (!$canondistro and /^X-Test-Reporter-Distfile:\s*(\S+)/) {
                $canondistro = $1;
            } elsif (/^X-Test-Reporter-Perl:\s*(\S+)/) {
                $v = version->new($1)->numify;
            }
            last if $canondistro && $v;
        }
        if ($Opt{minperl} && $v < $Opt{minperl}) {
            next REPORT;
        }
        $result{$v}{$result}++;
    }
    my $Lfail = 0;
    my $bbc;
    my @v = sort {$a<=>$b} keys %result;
    if ($Opt{skipmiddle}) {
        @v = @v[0,-1];
    }
    for my $vi (0..$#v) {
        my $v = $v[$vi] or next;
        my $results = $result{$v};
        for my $r (qw(pass fail)) {
            $results->{$r} ||= 0;
        }
        next unless $results->{fail} + $results->{pass} > 0;
        my $fail = $results->{fail} / ( $results->{fail} + $results->{pass} );
        if (!defined $bbc) {
            $bbc = $fail==0;
        }
        $bbc = 0 if $fail < $Lfail;
        $Lfail = $fail;
    }
    if ($Opt{bbc}){
        next VDISTRO if $Lfail < 1 || !$bbc;
    }
    my $isodate = isodate($canondistro);
    if (my $skb = $Opt{skipbefore}) {
        if ($isodate =~ /^\d/ and $skb gt $isodate) {
            next VDISTRO;
        }
    }
    $i++;
    printf "(%d) %s [%s]:\n", $i, $canondistro, $isodate;
    if ($Opt{matrixlink}) {
        printf "http://matrix.cpantesters.org/?dist=%s\n", $vdistro;
    }
    for my $v (@v) {
        my $results = $result{$v};
        printf "%-12s %s\n", $v, join(":",@{$results}{qw(pass fail)});
    }
}

sub isodate {
    my($canondistro) = @_;
    my $path = sprintf "%s/%s/%s/%s", "/home/ftp/pub/PAUSE/authors/id",
        substr($canondistro,0,1),
            substr($canondistro,0,2),
                $canondistro;
    my @stat = stat $path or return "N/A";
    my @date = gmtime $stat[9];
    $date[4]++;
    $date[5]+=1900;
    sprintf "%04d%02d%02dT%02d%02d%02dZ", @date[5,4,3,2,1,0];
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
