#!/usr/bin/perl

die "Program is dated";

# use 5.010;
use strict;
use warnings;

=head1 NAME

cnntp-refresh.pl - collect metadata of postings to nntp.perl.org

=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--sleep=i>

Will poll every N seconds.

=item B<--workdir=s>

defaults to something like ~/var/...

=back

=head1 END OF LIVE REACHED

This program is dated. Its original purpose was to update cpanstats.db
from develooper's nntp feed for the cpantesters' report mails. It's
only kept for hysterical raisins.

If you're looking for a successor, try refill-cpanstatsdb.pl.

=head1 DESCRIPTION

https://svn.develooper.com/projects/cnntp/ is the code base that
produces the pages. I found the link on
http://www.nntp.perl.org/about/

We fetched the database from
http://devel.cpantesters.org/cpanstats.db.gz and want to keep a
current/recent state of only a few fields. We are not interested much
in data from the month before.

  % time perl -le '
      use Net::NNTP; use YAML::XS qw(Dump);
      use List::Pairwise qw(mapp grepp);
      my $nntp=Net::NNTP->new("nntp.perl.org");
      my(undef,undef,$last) = $nntp->group("perl.cpan.testers");
      my %fail = grepp { $b =~ /^FAIL / } %{$nntp->xhdr("Subject", [$last-200, $last])};
      print Dump \%fail;
      '
  ---   
  '4346542': FAIL Log-Accounting-SVN-0.01 i686-linux-thread-multi-ld 2.6.28-11-generic
  '4346547': FAIL Parse-SVNDiff-0.03 i686-linux-thread-multi 2.6.28-11-generic   
  '4346552': FAIL Tie-Scalar-MarginOfError-0.03 i686-linux-thread-multi-ld 2.6.28-11-generic
  '4346556': FAIL Pointer-0.11 i686-linux-thread-multi 2.6.28-11-generic
  [...]
  perl -le 0.08s user 0.02s system 0% cpu 10.059 total

=cut


use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
BEGIN {
    push @INC, qw(    /usr/local/perl-5.10.1-uld/lib/5.10.1/x86_64-linux-ld
           /usr/local/perl-5.10.1-uld/lib/5.10.1
           /usr/local/perl-5.10.1-uld/lib/site_perl/5.10.1/x86_64-linux-ld
           /usr/local/perl-5.10.1-uld/lib/site_perl/5.10.1
     );}
use CPAN::Blame::Config::Cnntp;
use CPAN::DistnameInfo;
use CPAN::WWW::Testers::Generator::Database;
    # methods discussed in CPAN::Testers::Common::DBUtils
    # schema in CPAN::WWW::Testers::Generator

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);
#use List::Pairwise qw(mapp grepp);
use List::Util qw(max min);
use Net::NNTP;
use Pod::Usage qw(pod2usage);
#use Set::IntSpan::Fast::XS ();
#use Set::IntSpan::Fast ();
use URI ();
use YAML::XS;

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

$Opt{workdir} ||= File::Spec->catdir(vardir(),"workdir");
$Opt{sleep} ||= 311;
#chdir $workdir
#    or die("Couldn't change to $workdir: $!");

while () {
    # read db
    my $dbi = CPAN::WWW::Testers::Generator::Database->new(database=>"$Opt{workdir}/cpanstats.db");
    $dbi->{dbh}->func(1800000,'busy_timeout'); # undocumented, RTFS
    # no locking of the db needed because only we write to it

    # find out current article number
    my $sql = "select max(id) from cpanstats";
    my @rows = $dbi->get_query($sql);
    my $maxid = $rows[0][0];
    #$sql = "select * from cpanstats where id='$maxid'";
    #@rows = $dbi->get_query($sql);

=pod 

  0 | id       | 5379605                         |5430514
  1 | state    | pass                            |fail
  2 | postdate | 200909                          |
  3 | tester   | bingos@cpan.org                 |
  4 | dist     | Yahoo-Photos                    |Apache-Admin-Config
  5 | version  | 0.0.2                           |0.94
  6 | platform | i386-freebsd-thread-multi-64int |i386-freebsd
  7 | perl     | 5.10.0                          |
  8 | osname   | freebsd                         |
  9 | osvers   | 7.2-release                     |7.2-release
 10 | date     | 200909190440                    |200909190440

 From: George Greer
 Date: October 31, 2009 20:40
 Subject: PASS Log-StdLog-v0.0.3 x86_64-linux-thread-multi 2.6.28-15-generic

 DB<22> x $dbi->get_query("select * from cpanstats where id=5829958")
 0  ARRAY(0x3a32280)
     0  5829958
     1  'pass'
     2  undef
     3  undef
     4  'Log-StdLog'
     5  'v0.0.3'
     6  'x86_64-linux-thread-multi'
     7  undef
     8  undef
     9  '2.6.28-15-generic'
     10  200911010342

=cut

    # (since parsereport has the full articlespace of things having
    # failed once we need not fill in the missing data)

    # (all other data we will need are owned by "contemplate" or even
    # others)

    # merge everything (up to 200?,500?) between max of deb and current into db
    my $nntp;
    while (!$nntp) {
        unless ($nntp=Net::NNTP->new("nntp.perl.org")) {
            warn "Warning: Could not create nntp object for talking to perl.org; sleeping 15\n";
            sleep 15;
        }
    }
    my(undef,undef,$nnmaxid) = $nntp->group("perl.cpan.testers");
    my($from) = $maxid+1;
    my($upto) = min($from+3000, $nnmaxid);
    if ($from <= $upto) {
        warn sprintf "fetching subjects of articles %s..%s (nnmaxid being $nnmaxid)\n", $from, $upto;
        my $articles = $nntp->xhdr("Subject", [$from, $upto]); # hashref
        warn "inserting\n";
        my @now = gmtime;
        $now[4]++;
        $now[5]+=1900;
        my $date = sprintf "%04d%02d%02d%02d%02d", @now[reverse 1..5];
        while (my($id,$subject) = each %$articles) {
            my @split = split " ", $subject;
            if (@split <= 3) { # suspect xhdr bug discovered 2009-10-15
                my $head = $nntp->head($id);
                my $subject2 = "";
                my $in_subject = 0;
            ARTICLE_HEAD: for my $line (@$head) {
                    if ($line =~ /^Subject:\s*(.+)\n/) {
                        $subject2 = $1;
                        $in_subject = 1;
                    } elsif ($in_subject) {
                        if ($line =~ /^\s+(.+)\n/) {
                            $subject2 .= " ".$1;
                        } else {
                            last ARTICLE_HEAD;
                        }
                    }
                }
                if ($subject2 ne $subject) {
                    warn "Info: revoking subject[$subject] using subject2[$subject2]";
                    @split = split " ", $subject2;
                }
            }
            my($ok,$distro,$platform,$osvers) = @split;
            my $d = CPAN::DistnameInfo->new("FOO/$distro.tgz");
            my $sql = "INSERT INTO cpanstats (id,state,dist,version,platform,osvers,date) VALUES (?,?,?,?,?,?,?)";
            $dbi->do_query
                (
                 $sql,
                 $id,
                 lc($ok),
                 $d->dist,
                 $d->version,
                 $platform,
                 $osvers,
                 $date,
                );
            if ($ok =~ /fail/i) {
                printf "%d: %s\n", $id, $distro;
            }
        }
        warn "DEBUG: about to commit at ".scalar gmtime()."\n";
        $dbi->do_commit;
    } else {
        warn "Nothing to do, already fetched newest article $nnmaxid\n";
    }

    # release db
    undef $dbi;
    warn sprintf "sleeping %s at %s\n", $Opt{sleep}, scalar localtime;
    sleep $Opt{sleep};
}

sub vardir {
    return $CPAN::Blame::Config::Cnntp::Config->{solver_vardir};
}

1;
