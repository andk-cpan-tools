#!/home/src/perl/repoperls/installed-perls/perl/pVNtS9N/perl-5.8.0@32642/bin/perl

$ENV{USER}="andk"; # fill in your name
$ENV{RSYNC_PASSWORD} = shift; # fill in your passwd; not needed on port 873

use strict;
use File::Rsync::Mirror::Recent;
my @rrr = map {
    File::Rsync::Mirror::Recent->new
            (
             ignore_link_stat_errors => 1,
             localroot => "/home/ftp/pub/PAUSE/$_",
             remote => "pause.perl.org::PAUSE/$_/RECENT.recent",
             max_files_per_connection => 12503,
             rsync_options =>
             {
              port => 873, # was 8732 for pause2
              compress => 1,
              links => 1,
              times => 1,
              timeout => 600,
              'omit-dir-times'  => 1, # not available before rsync 3.0.3
              checksum => 0,
             },
             tempdir => "/home/ftp/tmp",
             ttl => 20,
             verbose => 1,
             # verboselog => "/var/log/rmirror-pause.log",
             runstatusfile => "/var/log/rmirror-status-$_.state",
            )} "authors", "modules";
die "directory $_ doesn't exist, giving up" for grep { ! -d $_->localroot } @rrr;
while (){
    my $ttgo = time + 20;
    for my $rrr (@rrr){
        $rrr->rmirror ( "skip-deletes" => 1 );
    }
    my $sleep = $ttgo - time;
    if ($sleep >= 1) {
        # print STDERR "sleeping $sleep ... ";
        sleep $sleep;
    }
}
