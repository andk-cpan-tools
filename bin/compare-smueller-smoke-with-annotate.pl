#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS

 $0 URL

=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--state=s>

Defaults to 'annotated'. Possible values are C<annotated>,
C<notannotated>, C<analysisized>

=back

=head1 DESCRIPTION

Shows the annotated/notannotated/analysisized distros in Steffen's list.

annotated is boring because we have done our work already

notannotated is has potential

analysisized has potential and has data at analysis

As primitive as it is we could first iterate through the analysisized
ones and write tickets for them if possible. We could then take the
notannotated ones and run some tests for them so that they end up in
analysis.

To filter the analysisized to the pass->fail ones I use

% perl bin/compare-smueller-smoke-with-annotate.pl --state=analysisized 'http://users.perl5.git.perl.org/~tsee/carp_errmsg/'|perl -nale 'print if $F[0]=~/^\d/ && $F[1]eq"N" && $F[2]eq"pf"'
2006-12-10         N pf http://analysis.cpantesters.org/solved?distv=CGI-Application-Plugin-CaptureIO-0.01
2012-02-15         N pf http://analysis.cpantesters.org/solved?distv=CPS-0.13
2008-07-17         N pf http://analysis.cpantesters.org/solved?distv=POE-Component-Server-SimpleXMLRPC-0.02
2010-09-21         N pf http://analysis.cpantesters.org/solved?distv=Thread-Pool-0.33

The "pf" stands for pass->fail while the "N" stands for notannotated.

What can be done at any time without spending too much time is to run
the notannotated ones through loop-over-recent without a glob.

I started to annotate those that I cannot grok, just so that they
disappear from the list.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
$Opt{state} ||= "annotated";
my $url = shift or pod2usage(1);
use LWP::UserAgent;
my $ua = LWP::UserAgent->new() or die;
my $resp = $ua->get($url);
my $content = $resp->content or die;
my %smoked;
my($s1,$s2);
for (split /\n/, $content) {
    #   <td><a href="http://search.cpan.org/dist/Tie-RegexpHash-0.15">Tie-RegexpHash-0.15</a></td>
    #   <td><a href="http://search.cpan.org/dist/CGI-Application-Plugin-CaptureIO-0.01">CGI-Application-Plugin-CaptureIO-0.01</a></td>
    unless ($s1) {
        if (/class="grade (pass|na|fail|unknown)"/) {
            $s1 = substr($1,0,1);
            next;
        }
    }
    unless ($s2) {
        if (/class="grade (pass|na|fail|unknown)"/) {
            $s2 = substr($1,0,1);
            next;
        }
    }
    next unless m{http://search.cpan.org/dist/([^"']+)};
    $smoked{$1} = "$s1$s2";
    $s1 = $s2 = undef;
}
open my $fh, "annotate.txt" or die;
my %anno;
while (<$fh>) {
    my($dist,$anno) = /^(\S+)\s+(.*)/;
    next unless $smoked{$dist};
    $anno{$dist} = $anno;
    if ($Opt{state} eq "annotated") {
        if ($anno) {
            printf "anno: %-34s %s\n", $dist, $anno;
        }
    } elsif ($Opt{state} eq "analysisized") {
    } elsif ($Opt{state} eq "notannotated") {
    } else {
        die "Illegal state '$Opt{state}', cannot continue";
    }
}
if ($Opt{state} eq "notannotated") {
    for my $dist (sort keys %smoked) {
        if (!$anno{$dist}) {
            printf "notanno: %s http://matrix.cpantesters.org/?dist=%s\n", reldate($dist), $dist;
        }
    }
} elsif ($Opt{state} eq "analysisized") {
    $|++;
    for my $dist (sort keys %smoked) {
        my $anaurl = sprintf "http://analysis.cpantesters.org/solved?distv=%s", $dist;
        my $resp = $ua->get($anaurl);
        my $content = $resp->content;
        my($state) = $content =~ /(No results|Release not found|Top Regressions|data were not sufficient)/;
        unless ($state) {
            warn "unknown state for dist[$dist]";
        }
        # "No results" has also "Distro-ID" and some more!
        if ($state eq "Top Regressions") {
            # <tr><td><b>Uploaded:</b></td><td>2007-12-12T01:48</td></tr>
            $state = $1 if $content =~ /Uploaded:.+?(\d\d\d\d-\d\d-\d\d)T\d\d:\d\d/;
        } else {
            $state =~ s/^(Release|data were)\s+//;
            $state =~ s/\s+/-/g;
        }
        printf "%-18s %s %s %s\n", $state, $anno{$dist}?"A":"N", $smoked{$dist}, $anaurl;
    }
}

sub reldate {
    return "2000-01-01";
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
