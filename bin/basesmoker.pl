#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--bbccheck!>

Instead of smoking we just try to list the current potential bbcs and
exit.

=item B<--dry-run|n!>

Does not run the command, just tells what it would do.

=item B<--extra>

Run this smoker without caring about other smokers running. Do not
watch other logfiles, do not suggest a compare-megalog commandline.
Just the smoke, maam. Usually we combine this with the --perl option.

=item B<--help|h!>

This help

=item B<--reporting!>

Defaults to true. By setting --noreporting we tell loop-over to turn
cpanreporting off.

=item B<--perl=s>

Path to the perl we want to smoke. Default is the most recently built
perl according to C<installed-perls-overview.pl>.

=item B<--ramdisk!>

B<MISNOMER>: it is only a ramdisk when /tmp is a ramdisk. bionic has
/tmp on ssd because a fullsmoker is not (yet?) doable in ram alone

Defaults to true.

Makes a local copy in /tmp/ (not necessarily in ramdisk!), works with
that perl, copies back to NFS after smoking. Thus avoids to run the
perl directly from NFS. The perl path stays but becomes a symlink
during the relocated phase. Possible downside: no executables allowed
on that partition

=item B<--timeout=f>

Defaults to 39600. Passed through to loop-over-recent.pl as is.

=item B<--tracedeps!>

Defaults to true. Passed through to loop-over-recent.pl with an -Ilib
switch as appropriate

=item B<--tracememory!>

Defaults to false. Passed through to loop-over-recent.pl with an -Ilib
switch as appropriate

=item B<--transient_build_dir!>

Passes this option through to loop_over_recent.

Otherwise we set
C<--config_build_dir=$ENV{HOME}/.cpan/privbuild/$hostname>.

=item B<--verbose|v!>

Verbose!

=item B<--with=s@>

If missing, defaults to --with=mydistrobundles. Otherwise each single
argument is passed on to loop-over-recent.pl as an option.

=back

=head1 DESCRIPTION



=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use File::Rsync::Mirror::Recent; # needed by loop-over-recent in this basesmoker role
use Getopt::Long;
use Hash::Util qw(lock_keys);
use List::Util qw(maxstr);
use Pod::Usage qw(pod2usage);
use Sys::Hostname ();
use Time::HiRes qw(sleep);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{extra} ||= 0;
$Opt{ramdisk} = 1 unless defined $Opt{ramdisk};
$Opt{reporting} //= 1;
$Opt{timeout} = 39600 unless defined $Opt{timeout};
$Opt{tracedeps} //= 1;
my $w = $Opt{with} //= [];
unless (@$w) {
    push @$w, "mydistrobundles";
}

our $SIGNAL = 0;

$SIG{INT} = sub { warn "\n\n****Received SIGNAL " . shift() . ", please be patient****\n\n"; $SIGNAL = 1 };

sub most_recent_megalog () {
    opendir my $dh, "." or die "Couldn't opendir .: $!";
    my $megalog = maxstr grep { /megalog-\d+-\d+-\d+T\d+:\d+:\d+.log$/ } readdir $dh or die "Did not find a single megalog file";
    return $megalog;
}

sub most_recent_megalog_but ($) {
    my($limit) = @_;
    opendir my $dh, "." or die "Couldn't opendir .: $!";
    return maxstr grep { /megalog-\d+-\d+-\d+T\d+:\d+:\d+.log$/ && $_ lt $limit } readdir $dh;
}

sub counting_sleep ($) {
    my $sleep = shift;
    my $eta = time+$sleep;
    local($|)=1;
    while () {
        my $left = $eta - time;
        last if $left < 0;
        printf "\rSleeping %d: %d    ", $sleep, $left;
        sleep 1;
        last if $SIGNAL;
    }
    print "\n";
}

sub other_smoker_running ($) {
    my $file = shift;
    my $size0 = -s $file;
    for my $i (0..9) { # arbitrary
        my $sleep = 6; # arbitrary
        sleep $sleep;
        my $size1 = -s $file;
        die "Something's wrong, most recent megalog has only 0 bytes" if $size0==0 && $size1==0;
        return 1 if $size1 > $size0;
    }
    return;
}

sub dot_d_needs_work ($) {
    my $megalogfile = shift;
    my $megalogdir = $megalogfile;
    $megalogdir =~ s/\.log/.d/;
    # look into the dir whether it is newer for all
    return 1 unless -e $megalogdir;
    opendir my $dh, $megalogdir or die "Could not open '$megalogdir': $!";
    my @stat_file = stat $megalogfile;
    for my $dirent (readdir $dh) {
        next if $dirent =~ /^\./;
        my $path = "$megalogdir/$dirent";
        my @stat_path = stat $path;
        return 1 if $stat_path[9] < $stat_file[9];
    }
    return 0;
}

sub most_recent_perl () {
    open my $fh, "-|", $^X, "$FindBin::Bin/installed-perls-overview.pl", "--max=1";
    my $absperl;
 OVLINE: while (<$fh>) {
        my($perl) = split " ", $_, 2;
        die "ALERT: Found directory of a hidden-locked smoker: '$perl'. We try smoking twice???" if $perl =~ /hilo/; # placeholder of a running smoker (hidden locked)
        my $hostname = Sys::Hostname::hostname;
        $hostname =~ s/\..*//;
        my $perl_or_host;
        if ($hostname eq "k83") {
            die "Panic: as far as I understood we do not want to run smokers on k83 anymore???";
            $perl_or_host = "perl";
        } else {
            $perl_or_host = "host/$hostname";
        }
        # Can't exec "/home/sand/src/perl/repoperls/installed-perls/perl/v5.23.7-35-g6002757/3bd2/bin/perl": No such...
        $absperl = sprintf "/home/sand/src/perl/repoperls/installed-perls/%s/%s/bin/perl", $perl_or_host, $perl;
        last OVLINE;
    }
    close $fh; # breaking a pipe?
    return $absperl;
}

sub megalog_contains_perl ($$) {
    my $file = shift;
    my $perl = shift;
    open my $fh, '<', $file or die "Could not open '$file': $!";
    while (<$fh>) {
        if (/\Q$perl\E/) {
            return 1;
        }
    }
    return 0;
}

sub suggest_cmp_megalogdir ($) {
    my($megalogfile) = @_;
    my $secondmegalogfile = most_recent_megalog_but($megalogfile);
    if (dot_d_needs_work($secondmegalogfile)) {
        warn "Note: second newest logfile '$secondmegalogfile' has no .d counterpart\n";
        # cannot make a suggestion, code below would die
        return;
    }
    my @mdir = map { s/\.log$/.d/r } $secondmegalogfile, $megalogfile;
    my $pairglob = "";
    while (    substr($mdir[0],length($pairglob),1)
               eq substr($mdir[1],length($pairglob),1) ) {
        $pairglob .= substr($mdir[0],length($pairglob),1);
    }
    my $pairgloblength = length $pairglob;
    my $i = 0;
    while () {
        my @dates = map { substr($_,$pairgloblength,$i) } @mdir;
        my $dates = join ",", @dates;
        my $glob = "$pairglob\{$dates\}*.d";
        my @g = glob $glob;
        if (@g == 2){
            $pairglob = $glob;
            last;
        } elsif ($i > length($dates[0])) {
            die sprintf "globbing on '%s' yields '%s', not trying any longer", $glob, join(",",@g);
        }
        $i++;
    }
    my $system = "perl ~/src/andk/andk-cpan-tools/bin/compare-megalogdirs.pl --annofile ~/src/andk/andk-cpan-tools/annotate.txt --skipanno --bbc $pairglob";
    warn "====> MAYBE you want to run '$system'\n";
    return $system;
}

sub make_a_dot_d ($) {
    my($megalogfile) = @_;
    my @system =
        (
         $^X,
         "$FindBin::Bin/colorout-to-dir-3.pl",
         "--html",
         $megalogfile,
        );
    die "no success running system[@system]" unless 0 == system @system;
}

sub squirrel_away ($) {
    my($perl) = @_; # /mnt/k75/homesand/src/perl/repoperls/installed-perls/perl/v5.16.0/127e/bin/perl
    my $original_torelocate_perl = dirname dirname $perl; # OP
    die "Could not make directory .lock in '$original_torelocate_perl': $!"
        unless mkdir "$original_torelocate_perl/.lock";
    my $cnt = 0;
    my $intempdir_relocated_perl =
        File::Temp::tempdir("basesmoker-reloperl-XXXX", DIR => "/tmp"); # RP
    warn sprintf "About to rsync I perl to /tmp at %s\n", scalar(localtime);
    0 == system rsync => "-ax", "$original_torelocate_perl/", "$intempdir_relocated_perl/"
        or die "error while trying to (early) rsync"; # LP => RP
    my $hidden_locked_perl = ""; # LP
    while (!$hidden_locked_perl) {
        my $trynum = int rand 10000;
        my $trydir = File::Spec->catdir(dirname($original_torelocate_perl), "hilo$trynum");
        if (rename $original_torelocate_perl, $trydir) {
            warn sprintf "WARNING[%s]: perl[%s] now hidden,unaccessible\n",
                scalar localtime, $original_torelocate_perl;
            $hidden_locked_perl = $trydir;
        } else {
            die "too many tries failed, last one was with OP[$original_torelocate_perl]trydir[$trydir]\$![$!], giving up" if ++$cnt > 100;
            sleep 0.1;
        }
    }
    warn sprintf "About to rsync II perl to /tmp at %s\n", scalar(localtime);
    0 == system rsync => "-ax", "$hidden_locked_perl/", "$intempdir_relocated_perl/"
        or die "error while trying to (final)rsync"; # LP => RP
    symlink $intempdir_relocated_perl, $original_torelocate_perl; # RP => OP
    warn sprintf "WARNING[%s]: perl[%s] now accessible again (relocated)\n",
        scalar localtime, $original_torelocate_perl;
    return
        {
         original_torelocate_perl => $original_torelocate_perl,
         hidden_locked_perl => $hidden_locked_perl,
         intempdir_relocated_perl => $intempdir_relocated_perl,
        };
}

sub squirrel_back ($) {
    my($handle) = @_;
    my($original_torelocate_perl,$hidden_locked_perl,$intempdir_relocated_perl) =
        @{$handle}{"original_torelocate_perl","hidden_locked_perl","intempdir_relocated_perl"};
    warn sprintf "About to rsync I perl back from %s/ -> %s/ at %s\n",
        $intempdir_relocated_perl, $hidden_locked_perl, scalar(localtime);
    my @system = (rsync => "-ax", "--omit-dir-times", "--no-inc-recursive", "$intempdir_relocated_perl/", "$hidden_locked_perl/");
    0 == system @system or die "error while trying to (first)rsync back with '@system'";
    unlink $original_torelocate_perl
        or die "Could not unlink '$original_torelocate_perl': $!"; # OP
    warn sprintf "WARNING[%s]: perl[%s] now hidden,unaccessible\n",
        scalar localtime, $original_torelocate_perl;
    warn sprintf "About to rsync II perl back from /tmp at %s\n", scalar(localtime);
    @system = (rsync => "-ax", "$intempdir_relocated_perl/", "$hidden_locked_perl/");
    0 == system @system or die "error while trying to (final)rsync back with '@system'";
    rename $hidden_locked_perl, $original_torelocate_perl
        or die "Could not rename back: $!"; # LP => OP
    warn sprintf "WARNING[%s]: perl[%s] now accessible (moved back)\n",
        scalar localtime, $original_torelocate_perl;
    rmdir "$original_torelocate_perl/.lock" or die "Could not rmdir: $!";
    File::Path::remove_tree($intempdir_relocated_perl, { safe => 0 });
}

# check whether this perl is completed; we do not want to
# smoke when generate_recent is still running;
sub has_test_reporter_transport_metabase {
    my($perl) = @_;
    my $mib = $perl;
    $mib =~ s|perl$|module_info|;
    if (-e $mib) {
        if (0==system($mib,"Test::Reporter::Transport::Metabase")) {
            return 1;
        }
    } elsif (! -e $perl) {
        warn "Warning: perl '$perl' not found";
        return 0;
    } elsif (0==system($perl,"-MV=Test::Reporter::Transport::Metabase")) {
        return 1;
    } else {
        return 0;
    }
}

MAIN: {
    my $megalogfile;
    if (!$Opt{extra}) {
        $megalogfile = most_recent_megalog();
    }
    if ($Opt{bbccheck}) {
        make_a_dot_d($megalogfile);
        my $system = suggest_cmp_megalogdir($megalogfile);
        warn "Running that now";
        system $system;
    } elsif (!$Opt{extra} && other_smoker_running($megalogfile)) {
        warn "Info: other smoker '$megalogfile' is running" if $Opt{verbose};
        counting_sleep 1800; # arbitrary
    } else {
        my $perl = $Opt{perl} || most_recent_perl();
        if (!$Opt{extra}) {
            if (dot_d_needs_work($megalogfile)) {
                make_a_dot_d($megalogfile);
            }
            suggest_cmp_megalogdir($megalogfile);
            if (megalog_contains_perl($megalogfile,$perl)) {
                if ($Opt{verbose}) {
                    warn "Nothing to smoke, newest perl '$perl' already smoked";
                }
                counting_sleep 1200; # arbitrary: if too short we
                                     # write the console too full; I'd
                                     # like to have a watcher for a
                                     # new perl here such that we exit
                                     # as soon as a new perl shows up
                exit;
            }
        }
        my $date = `date +%FT%T`;
        chomp $date;
        my @more;
        if (! $Opt{reporting}) {
            push @more, "--config_test_report=0";
        }
        for my $w (@{$Opt{with}}) {
            push @more, "--$w";
        }
        my $build_dir_option;
        if ($Opt{transient_build_dir}) {
            $build_dir_option = "--transient_build_dir";
        } else {
            my $hostname = Sys::Hostname::hostname;
            $hostname =~ s/\..*//;
            my $privbuilddir = "$ENV{HOME}/.cpan/privbuild/$hostname";
            mkpath $privbuilddir;
            $build_dir_option = "--config_build_dir=$privbuilddir";
        }
        my $system =
            join " ",
            map { />/ ? $_ : "\"$_\"" } # quote all arguments except redirects
            (
             $^X,
             "-I",
             "$ENV{HOME}/src/andk/rersyncrecent/lib",
             "$ENV{HOME}/src/andk/andk-cpan-tools/bin/loop-over-recent.pl",
             "--perlglob=$perl",
             "--config_build_cache=1000",
             $build_dir_option,
             # "--config_build_dir_reuse=1", # danger of serious damage through 'Argument list too long'
             "--config_ftpstats_size=0",
             $Opt{tracedeps} ? ("--tracedeps", "--inc=$ENV{HOME}/cpanpm/related/CPAN-Plugin-TraceDeps/lib") : (),
             $Opt{tracememory} ? ("--tracememory", "--inc=$ENV{HOME}/cpanpm/related/CPAN-Plugin-TraceMemory/lib") : (),
             "--inc=$ENV{HOME}/cpanpm/lib",
             "--config_build_requires_install_policy=yes",
             "--timeout=$Opt{timeout}",
             @more,
             ">",
             "megalog-$date.log",
             "2>&1",
            );
        if ($Opt{"dry-run"}) {
            warn "Would run system[$system]";
        } else {
            if ($Opt{verbose}) {
                warn "About to run '$system'\n";
            }
            while ($Opt{reporting} and !has_test_reporter_transport_metabase($perl)) {
                warn "Waiting for 'Test::Reporter::Transport::Metabase' for perl '$perl' installed\n";
                counting_sleep 180; # arbitrary
                last if $SIGNAL;
            }
            my $rphandle;
            if ($Opt{ramdisk}) {
                $rphandle = squirrel_away($perl); # relocated perl handle
            }
            my $exitcode = 0;
            unless (0 == system $system){
                warn "no success running system[$system], check content of 'megalog-$date.log'";
                $exitcode |= 1;
            }
            if ($SIGNAL) {
                if ($rphandle) {
                    warn "Trying to squirrel perl[$perl] back";
                } elsif ($Opt{ramdisk}) {
                    warn "Ouch, we probably just lost a perl[$perl]";
                } else {
                    warn "it seems we received a signal while perl is '$perl'";
                }
                $exitcode |= 2;
            }
            squirrel_back($rphandle) if $rphandle;
            if ($exitcode) {
                warn "Alert: exit code[$exitcode]";
                exit $exitcode;
            }
        }
    }
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
