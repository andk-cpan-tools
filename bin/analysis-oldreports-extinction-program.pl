#!/home/andreas/src/installed-perls/v5.16.0/4e6d/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my $optpod = <<'=back';

=item B<--focus=s> Default: tar

Valid values: C<tar> or C<dir>. RTFS.

=item B<--help|h!>

This help

=item B<--keepresults=i> Default: 4

Number of result-directories or result-tarballs to never delete.

=item B<--minage=f> Default: 690

Minimum age in days of the target directory
(measured with C<-M>).

=item B<--noninteractive|noni>

Don't ask, just apply the default answer.

=item B<--untildf=i> Default: 48000000

Run extinction until available blocks reported
by df is above this value.

=item B<--yes|y!>

The interactive question is normally answered with no as the default.
With --yes the default becomes yes.

=back

=head1 DESCRIPTION

What we look for is an old and big and outdated directory. For the
arbitrary current thresholds RTFS. When we have listed one, we exit.
We show it with age, size and neighbors of the same Distname.

=head1 BUGS

The files in C<workdir/solved/> are removed too (which is good) but
they were not removed before 2016-03-31, so we should bulk remove the
left overs separately some day; this affects files like the output of:

  ls -lt /home/andreas/data/cnntp-solver-2009/workdir/solved/Any-Moose-0.*
  ls -lt /home/andreas/data/cnntp-solver-2009/workdir/solved/package-watchdog-*

Note that package-watchdog-0.07.slv is the oldest(2009) and
package-watchdog-0.04.yml is the newest (2013). Why? Because the
package has ceased to exist. Update 2017-12-31: we have now
analysis-oldsolutions-extinction-program.pl for that.

What is the thing with suggest_alternative()? It currently says only
FIXME and dumps stuff. The idea was that when the lowest version
number is not old enough, then we could try whether the second-lowest
version number is probably old enough and then we could remove that
one instead. suggest_alternative shows what we would get as the
outcome and when I saw this, I was not convinced this to be a good
idea. Probably counterproductive. Watch out for cases where this
algorithm would remove the newest version and keep the older ones. Not
trustworthy!

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath rmtree);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use Redis 1.967;
sub myredis () {
    my $redis = Redis->new(reconnect => 120, every => 1000);
}

use LWP::UserAgent;
use JSON::XS;
use CPAN::DistnameInfo;
use List::AllUtils qw(reduce);
use CPAN::Version;
use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
use CPAN::Blame::Config::Cnntp;
use CPAN::DistnameInfo;
use Term::Prompt qw(prompt);
use YAML::XS;
use Filesys::Df qw(df);
use Time::HiRes qw(sleep);

my $ua = LWP::UserAgent->new();
$ua->default_header("Accept-Encoding", "gzip");
my $jsonxs = JSON::XS->new->indent(0);

my @opt = $optpod =~ /B<--(\S+)>/g;
our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
for my $opt (qw(focus untildf minage keepresults)) {
    next if defined $Opt{$opt};
 LINE: for my $line (split /\n/, $optpod) {
        if ($line =~ /--\Q$opt\E=.+?Default: (.+)/) {
            $Opt{$opt} = $1;
            last LINE;
        }
    }
}

sub show_df (){
    my $df = df '/';
    warn "df bavail: $df->{bavail}\n";
    $df->{bavail};
}

sub rm_other ($) {
    my($distv) = @_;
    for my $ext (qw(slv.LCK yml slvdv.gz slv slvdv)) {
        my $workdirfile = "/home/andreas/data/cnntp-solver-2009/workdir/solved/$distv.$ext";
        if (-e $workdirfile) {
            warn "Deleting $workdirfile\n";
            unlink $workdirfile or die "Could not unlink '$workdirfile': $!";
        }
    }
    for my $zs (qw(analysis:distv:calctimestamp)) {
        warn "zrem-redis: redis-cli zrem $zs $distv\n";
        my $redis = myredis;
        $redis->zrem($zs, $distv);
    }
    warn "Done.\n";
}
sub treat_dir ($$$$) {
    my($parent, $distv, $dist, $v) = @_;
    my $query = sprintf "http://fastapi.metacpan.org/v1/release/_search?q=distribution:%s&fields=name,date,status,version,author&size=400", $dist;
    warn "$query\n";
    sleep 0.3;
    my $resp = $ua->get($query);
    # print $query;
    my $jsontxt = $resp->decoded_content;
    my $j = eval { $jsonxs->decode($jsontxt); };
    if (! $j or $@){
        warn "ERROR: decode failed on query '$query'";
        return;
    }
    my $hits = $j->{hits}{hits};
    my($releasedate) = map { $_->{fields}{date} } grep { $_->{fields}{name} eq $distv } @$hits;
    unless ($releasedate) {
        warn "WARNING: Did not find own releasedate for $distv";
        return;
    }
    my $cpanversion = reduce {
        CPAN::Version->vgt($a,$b) ? $a : $b
        } map { $_->{fields}{version} } grep { $_->{fields}{date} ge $releasedate } @$hits;
    if (CPAN::Version->vgt($cpanversion, $v)) {
        my $default_answer = $Opt{yes} ? "y" : "n";
        my $answer;
        if ($Opt{noninteractive}) {
            $answer = $default_answer;
        } else {
            $answer = lc prompt "x",
                "Shall I delete $distv (cpanversion=$cpanversion) and related workdir files? (y/n)", "", 'y';
        }
        if ($answer eq "y") {
            rmtree "$parent/$distv";
            rm_other $distv;
        } else {
            warn "INFO: Skipping $distv";
        }
    }
}
sub suggest_alternative ($$$) {
    my($timeline, $l, $minage) = @_;
    my @timeline = sort { CPAN::Version->vcmp($a->{v}, $b->{v}) } @$timeline;
    warn sprintf "FIXME: investigate an alternative for %s\n", YAML::XS::Dump(\@timeline);
    for ( my $i=0; $i<=$#$timeline; $i++ ) {
        if ($timeline[$i]{M} >= $minage) {
            my($who) = map { $_->[0] } grep { $timeline[$i]{v} eq $_->[0]{v} } values %$l;
            return { i => $i, object => $timeline[$i], who => $who };
        }
    }
    return;
}

my $root = $CPAN::Blame::Config::Cnntp::Config->{ctgetreports_dir};
my $bavail = show_df;
my $outer_purged_something = 0;
LOOP: while ($bavail < $Opt{untildf}) {
    my $purged_something = 0;
    opendir my $dh, $root or die "Could not open '$root': $!";
 OUTER: for my $letter (readdir $dh) {
        next unless $letter =~ /^[A-Za-z]$/;
        opendir my $dh2, "$root/$letter" or die "Could not open '$root/$letter': $!";
        my %odist;
    INNER: for my $distdir (readdir $dh2) {
            next if $distdir eq "." or $distdir eq "..";
            my $tdir_or_tar = "$root/$letter/$distdir";
            my($distv, $isdir, $want_treat_dir);
            if (0) {
            } elsif (-d $tdir_or_tar) {
                # warn "looking at directory $tdir_or_tar\n";
                if ($Opt{focus} eq "tar") {
                    next INNER;
                } elsif ($Opt{focus} eq "dir") {
                    if (-f "$tdir_or_tar.tar") {
                        warn "skipping $tdir_or_tar because tarball also found";
                        my($most_recent_file) = qx"find $tdir_or_tar -type f | xargs ls -t";
                        chomp $most_recent_file;
                        my @triple = ($tdir_or_tar, "$tdir_or_tar.tar", $most_recent_file);
                        system "ls -ld @triple";
                        my @triple_age = sort { $a <=> $b } map { -M $_ } @triple;
                        warn "triple_age [@triple_age]";
                        # There must be a rule to say we remove the
                        # dir or not (because somebody works with it).
                        # We know that the directory timestamp can be
                        # very old because nobody updates it. So
                        # usually the youngest timestamp is from a
                        # downloaded file or the tarball. But removing
                        # the directory is dangerous, no matter how
                        # old it is
# Shall I delete /home/andreas/data/cpantesters/reports/L/Log-Agent-1.000.tar
#         (Mtime=718.514236111111111) and related workdir files? (y/n/q)
#         [default y] 
# Deleting /home/andreas/data/cpantesters/reports/L/Log-Agent-1.000.tar
# Deleting /home/andreas/data/cnntp-solver-2009/workdir/solved/Log-Agent-1.000.slv.LCK
# Deleting /home/andreas/data/cnntp-solver-2009/workdir/solved/Log-Agent-1.000.yml
# Deleting /home/andreas/data/cnntp-solver-2009/workdir/solved/Log-Agent-1.000.slvdv.gz
# Deleting /home/andreas/data/cnntp-solver-2009/workdir/solved/Log-Agent-1.000.slv
# zrem-redis: redis-cli zrem analysis:distv:calctimestamp Log-Agent-1.000
# Done.
# Done.
# df bavail: 26508336

                        # the solution is probably in the LCK file.


                        # but when in dearest need, we can say we
                        # remove the directory when the youngest is
                        # older than 5 days
                        if ($triple_age[0] > 5) {
                            warn "Ruthless deletion only directory: $tdir_or_tar\n";
                            rmtree $tdir_or_tar;
                        }
                        next INNER;
                    } else {
                        $want_treat_dir = 1;
                    }
                } else {
                    die "Illegal focus '$Opt{focus}'";
                }
                $distv = $distdir;
                $isdir = 1;
            } elsif ($distdir =~ /(.+)\.tar$/) {
                $distv = $1;
                $isdir = 0;
            } else {
                # ignore me for now
                next INNER;
            }
            my $d = CPAN::DistnameInfo->new("FOO/$distv.tgz");
            my $dist = $d->dist;
            my $v = $d->version;
            if ($want_treat_dir) {
                treat_dir("$root/$letter", $distdir, $dist, $v);
                $bavail = show_df;
                sleep 0.1;
                next INNER;
            }
            my $a = $odist{$dist}{$distv} ||= [];
            my $i = @$a;
            push @$a,
                {
                 abs => $tdir_or_tar,
                 isdir => $isdir,
                 M => -M $tdir_or_tar,
                 dist => $dist,
                 distv => $distv,
                 v => $v,
                 i => $i,
                };
        }
    ODIST: for my $d (sort {scalar keys %{$odist{$b}} <=> scalar keys %{$odist{$a}}} keys %odist) {
            my $l = $odist{$d};
            my($howold_by_M, $who_finally_deletable);
            {
                next ODIST unless scalar keys %$l >= $Opt{keepresults};

                # m_oldest ==> oldest by tarball mtime
                my $m_oldest = reduce { $a > $b ? $a : $b } map { $_->{M} } map { @{$l->{$_}} } keys %$l;
                next ODIST unless $m_oldest >= $Opt{minage};

                # v_oldest ==> oldest by version number
                my $v_oldest = reduce { CPAN::Version->vlt($a,$b) ? $a : $b } map { $_->{v} } map { @{$l->{$_}} } keys %$l;
                my($who_v_oldest) = map { $_->[0] } grep { $v_oldest eq $_->[0]{v} } values %$l;
                $howold_by_M = $who_v_oldest->{M};
                if ($howold_by_M < $Opt{minage}) {
                    warn "Oldest by version on $who_v_oldest->{dist} would be $v_oldest, but it is younger than $Opt{minage}, namely $howold_by_M, investigating $who_v_oldest->{abs}\n";
                    my @timeline = 
                        map { +{ M => $_->{M}, v => $_->{v} } } map { @{$l->{$_}} } keys %$l;
                    my $ctx = suggest_alternative(\@timeline, $l, $Opt{minage});
                    if ($ctx) {
                        warn sprintf "FIXME 2: would suggest %s\n", YAML::XS::Dump($ctx);
                    }
                    next ODIST;
                } else {
                    $who_finally_deletable = $who_v_oldest;
                }
            }

            my($abs_of_oldest);
        DISTV: for my $distv (keys %$l) {
                for my $i (0..$#{$l->{$distv}}) {
                    if ($l->{$distv}[$i]{M} == $howold_by_M) {
                        $abs_of_oldest = $l->{$distv}[$i]{abs};
                        last DISTV;
                    }
                }
            }
            open my $duh, "-|", du => "-s", $abs_of_oldest or die "Could not fork for 'du -s $abs_of_oldest': $!";
            my($duline) = <$duh>;
            my($du_of_oldest) = split " ", $duline;
            $who_finally_deletable->{du} = $du_of_oldest;

            # next unless $du_of_oldest > 7000; # arbitrary
            my $ll = []; # = [ map { @{$l->{$_}} } sort { $l->{$b}->{M} <=> $l->{$a}->{M} } keys %$l ];
            for my $distv (keys %$l) {
                for my $i (0..$#{$l->{$distv}}) {
                    push @$ll, $l->{$distv}[$i];
                }
            }
            {
                my $i = 0;
                print YAML::XS::Dump [map {$_->{'_'}=++$i;$_} sort { CPAN::Version->vcmp($b->{v},$a->{v}) } @$ll];
            }
            my $default_answer = $Opt{yes} ? "y" : "n";
            my $answer;
            if ($Opt{noninteractive}) {
                $answer = $default_answer;
            } else {
                $answer = lc prompt "x",
                    "Shall I delete $abs_of_oldest (Mtime=$howold_by_M) and related workdir files? (y/n/q)", "", $default_answer;
            }
            if ($answer eq "q") {
                warn "Leaving LOOP\n";
                last LOOP;
            } elsif ($answer eq "y") {
                warn "Deleting $abs_of_oldest\n";
                rmtree $abs_of_oldest;
                my $distv = $who_finally_deletable->{distv};
                rm_other($distv);
                warn "Done.\n";
                $purged_something = 1;
                $outer_purged_something = 1;
            } else {
                warn "Nothing deleted\n";
            }
            last OUTER;
        }
    }
    $bavail = show_df;
    unless ($purged_something) {
        my $else = $outer_purged_something ? " else" : "";
        warn "Info: Nothing$else found to delete, giving up";
        last LOOP;
    }
    sleep 0.2;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
