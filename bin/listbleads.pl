#!/usr/bin/perl

# remember  -Dnoextensions=Encode saves time

my @cand = glob("/home/src/perl/repoperls/installed-perls/*/p*/perl-*\@39999/bin/perl");

my @rich = map {
    my $path = $_;
    my($sha) = m|installed-perls/.*?/p(.*?)/perl-|;
    open my $fh, "-|", "git show --pretty=fuller --date=iso $sha" or die;
    my $t;
    while (<$fh>) {
        next unless /^CommitDate:\s+(.+)$/;
        $t = $1;
    }
    [$t, $path] } @cand;
my @sorted = sort { $b->[0] cmp $a->[0] } @rich;
for my $perl (@sorted) {
    printf "%s %s\n", @$perl;
}
