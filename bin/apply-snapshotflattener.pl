#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--maxiter|m=i>

Number of iterations, defaults to 1.

=item B<--megalog=s>

Megalogfile. Defaults to some historical file.

=back

=head1 DESCRIPTION



=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{megalog} //= "/home/sand/megalog/megalog-2018-10-05T23:09:31.log";

use YAML;

my $maxloops = $Opt{maxiter} //= 1;
my $i = 0;
while (++$i <= $maxloops) {
    my $yaml = qx{$^X bin/snapshotflattener.pl --sfr $Opt{megalog}} or last;
    my $y = YAML::Load $yaml;
    die "Human help required for yaml[\n$yaml\n]\n" if $y->{line} == 0;
    my $snap = "lib/Bundle/Snapshot_2011_10_21_$y->{bfile}.pm";
    rename $snap, "$snap~" or die "Could not rename $snap: $!";
    open my $fh, "$snap~" or die "Could not open $snap~: $!";
    open my $fh2, ">", $snap or die "Could not open >$snap: $!";
    warn "About to process $y->{name} on $snap\n";
    while (<$fh>) {
        my($m) = /^(\S*)/;
        if ($m eq $y->{name}) {
            warn "$y->{name} found\n";
            for my $dep (@{$y->{deps}}) {
                print $fh2 "$dep\n";
            }
            my $joined_deps;
            my $max_show_deps = 3;
            if (@{$y->{deps}} > $max_show_deps) {
                pop @{$y->{deps}} while @{$y->{deps}} > $max_show_deps;
                $joined_deps = join ",", @{$y->{deps}}, "...";
            } else {
                $joined_deps = join ",", @{$y->{deps}};
            }
            warn sprintf "%d lines injected near bline %s (%s)\n", scalar @{$y->{deps}}, $y->{line}, $joined_deps;
        }
        print $fh2 $_;
    }
    close $fh2 or die "Could not close $snap: $!";
    system q<perl -i~ -nle ' use strict; our(%IN,%S); BEGIN { $IN{contents}=0; } if (/^=head1 CONTENTS/){   $IN{contents}=1;   print; next; } if (/^=/){   $IN{contents}=0; } if ($IN{contents}){   /^([A-Za-z]\S*)/ and $S{$1}++ and next; } print; ' lib/Bundle/Snapshot_2011_10_21_0?.pm>;
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
