#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--dry-run|n!>

Tells us what it would do and exits.

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Work until nothing to do or we have worked for a full day. Looks into
the redis queue C<analysis:jobqueue:q> for C<distv> names and calls a
--pick job, one after the other. Concurrency is managed by
IPC::ConcurrencyLimit.

=cut


use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";
BEGIN {
    push @INC, qw(       );
}
use CPAN::Blame::Config::Cnntp;

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use Time::Moment;

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

my($workdir);
BEGIN {
    $workdir = File::Spec->catdir
        ($CPAN::Blame::Config::Cnntp::Config->{solver_vardir},
         "workdir");
}
use IPC::ConcurrencyLimit;

my($basename) = File::Basename::basename(__FILE__);
my $limit = IPC::ConcurrencyLimit->new
    (
     # Note: the next code line is parsed by catalyst for mgmt.tt, so
     # keep it matching /\bmax_procs\s*(?:=>|,)\s*(\d+)/;
     max_procs => 3,
     # 2014-01-06: 8 made the normal requests slow
     # 2014-01-29: 6 was still demanding on disk
     # 2014-08-22: 5 was very good, but it seems that 4 will be good enough too
     # 2015-06-03: 4 was wasting cycles, 5 will speed up calculating,
     #   lowered swappiness seems to help the webserver
     # 2016-02-19: setting to 4 again, would prefer better response
     #   times; if we are not calculating quickly enough, users can
     #   schedule by clicking nowadays
     # 2016-07-08: too much swapping on analysis decreasing to 3
     path      => "$workdir/IPC-ConcurrencyLimit-$basename",
    );
my $limitid = $limit->get_lock;
if (not $limitid) {
    warn "Another process appears to be still running. Exiting.";
    exit(0);
}

my $logfile = __FILE__ . ".log";
sub appendlog {
    my($what) = @_;
    $what =~ s/\s*\z//;
    my $tm = Time::Moment->now_utc;
    open my $fh, ">>", $logfile or die "Could not open '$logfile': $!";
    print $fh "$tm $what\n";
    close $fh or die "Could not close '$logfile': $!";
}

use Redis;
use Time::HiRes qw(sleep);
my $redis = Redis->new(reconnect => 120, every => 1000);
my($found_one) = 0;
setpriority(0, 0, 5); # renice
appendlog("Starting as $$");
LOOP: while () {
    my $qlen = $redis->zcard("analysis:jobqueue:q");
    last LOOP if $qlen < 1;
    my($distv) = $redis->zrevrange("analysis:jobqueue:q",0,0);
    $found_one = 1;
    if ($Opt{"dry-run"}) {
        warn "Found $qlen items in q. Would now try to remove $distv and process it. But dry-run, leaving";
        last LOOP;
    }
    if ($redis->zrem("analysis:jobqueue:q", $distv)) {
        appendlog("As $$ $distv");
        0==system qq{ionice -n 7 "$^X" "$FindBin::Bin/cnntp-solver.pl" "--pick" "$distv" >> $logfile 2>&1} or die "problem running cnntp-solver, giving up";
        sleep 3; # as a precaution that we do not throw away the whole
                 # queue too quickly in case of a bug
    } else {
        sleep 0.3;
    }
    if (time - $^T > 86400) {
        last LOOP;
    }
}
if (! $found_one) {
    if ($Opt{"dry-run"}) {
        warn "Queue seems to be empty, would just leave";
    }
}
appendlog("Finished as $$");

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
