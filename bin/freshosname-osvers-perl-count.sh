#!/bin/bash

time psql analysis -c "select osname, osvers, perl, count(*) from cpanstats where postdate >= '202401' and tester like '%andreas%' and fulldate>now() - interval '1 day' group by osname, osvers, perl order by osname, osvers, perl" | sed -e 's/ RC/-RC/' | perl -pale 'our $cum; if ($F[6] =~ /^\d+$/){ $cum += $F[6]; $_ .= " $cum" } END {printf "cum=%s, per hour=%.2f\n", $cum, $cum/24}' | sed -e 's/-RC/ RC/'; date
