#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;
use CPAN::Version;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--addopts=s@>

Options to be added to the Configure call. Do not use a leading C<->
(dash) for the content of the option as in
C<--addopts=Duserelocatableinc>

=item B<--debugging!>

Defaults to true and sets -DDEBUGGING=-g. Cannot be combined with
--debuggingoption

=item B<--debuggingoption=s>

No default. Passed through to perl with a C<-D>. Cannot be combined with
--debugging.

Example:

  --debuggingoption=DEBUGGING=both

=item B<--dotcpanhome=s>

defaults to C< $ENV{HOME}/.cpan >. The place passed to every
smoker-perl where it should pick the MyConfig from.

=item B<--help|h!>

This help

=item B<--install!>

Runs C<./installperl> on perl and exits with an error if it does not
succeed.

Defaults to true.

=item B<--inversemodule=s>

If we are running a bisect to find out when a breakage was fixed, we
need to fake the other return value than what we usually do. I.e. when
the module passes we return a fail and when it fails we return a pass.
Because git-bisect is so braindead. You get this by supplying a
--inversemodule.

=item B<--jobs|j=i>

Parameter to pass to 'make -j' on normal C<make> runs and to assign to
the TEST_JOBS environment variable on C<make test> runs. Since
20160607 we also set HARNESS_OPTIONS accordingly. Defaults to 3.

It seems perl 5.8.7 needs --jobs=1 . It doesn't succeed with x2p stuff
but it's even worse when called with parallel make.

=item B<--keepfiles!>

Do not do any cleanup after perl installation. Cleanup should be done
by the user then, e.g. with C<git clean -dfx>.

=item B<--keepfilesonerror!>

Do not do any cleanup on error, just bail out with an error.

=item B<--module=s@>

Install this (or these) module(s), die when it (the last of those)
cannot be updated to the current version. See also --inversemodule.

Misnomer. the argument can be any argument that can be passed to CPAN
shell's install command. B<But>: since we only have the uptodate
command to verify that an install has taken place, we are unable to
determine success for arguments like
MSCHWERN/Test-Simple-1.005000_005.tar.gz.

In so far, it is not such a misnomer.

Additional hook: if the argument contains no slash but a minus, we
replace it with C<::> as we are used to do it in many other contexts.

And another hook: if there is only one --module option and the
argument contains a comma, we split into an arrayref.

=item B<--patchperl!>

Whether we should try to run patchperl. If perl is 5.028003 or lower,
we default to true, otherwise to false. Reasoning being we want
unmodified sources for recent releases, patchperl should be reserved
(in our opinion) for patching old perls, others should be subject to
more scrutiny. 5.028003 actually needs patchperl due Time::Local. See
https://github.com/Perl/perl5/issues/17410

=item B<--prefix=s>

Defaults to /home/<getlogin>/src/perl/repoperls/installed-perls/perl, but only
when hostname=k83. All other hostnames get the last path element
C<perl> replaced with C<host/> concatenated with their hostname. We
started with this rule on 2012-04-22 because relocateableinc does not
work (see https://rt.perl.org:443/rt3/Ticket/Display.html?id=112448).

It gets the output of C<git describe> and a config-dependent
hex-encoded hash appended.

=item B<--report!>

Short for

  --module=Bundle::CPANxxl --module=Bundle::CPANReporter2

Defaults to false.

=item B<--suppressreadline!>

$CPAN::Suppress_readline will be set to true. Useful for testing
modules that need a readline handle, like Term::Completion

=item B<--successchecker=s>

Run a perl script that determines success or failure. Will only be
executed when state is success.

=item B<--test!>

Runs C<make test> on perl and exits with an error if it does not
succeed.

Since 2016-01-02 we default this to true.

=item B<--tmpdir=s>

Defaults to /tmp.

=item B<--ud=s>

One of C<UU>, C<UD>, C<DU>, C<DD>, C<rand>.

Defaults to UD, gets expanded to

  --addopts=Uuseithreads
  --addopts=Duselongdouble

UU gets expanded to

  --addopts=Uuseithreads
  --addopts=Uuselongdouble

etc.

Argument C<rand> changes the option itself to one of the four
available uppercase settings at random.

=item B<--usegit!>

If current directory contains a C<.git/> subdirectory, defaults to
true, otherwise to false.

=item B<--verbose!>

Add a bit extra verbosity. Underdeveloped.

=back

=head1 DESCRIPTION

Script to build perl and one or more modules and return true when the
module (or the last of several modules) can be built.

Chdir to the git repo and for a simple build run

  makeperl.pl

The default is a nonthreaded build with -Duselongdouble. To choose a
different permutation of threadedness and uselongdoubleness use the
option -ud=, eg:

  makeperl.pl -ud=DD
  makeperl.pl -ud=UU
  makeperl.pl -ud=DU

To immediately build one module with the resulting perl:

  makeperl.pl -module=YAML::Syck

With the ability to return false on fail we can start bisecting:

  git bisect start v5.14.0-658-g65374be v5.14.0-523-gc5caff6

And then C<run bisect>:

  git bisect run makeperl.pl -ud=rand --module=YAML::Syck

=cut

# stolen from loop-over-recent.pl
use BSD::Resource;
BSD::Resource::setrlimit(BSD::Resource::RLIMIT_CORE(),  40*1024*1024, 40*1024*1024);
BSD::Resource::setrlimit(BSD::Resource::RLIMIT_CPU(),   3600, 3600);
BSD::Resource::setrlimit(BSD::Resource::RLIMIT_RSS(),   3_000_000_000, 3_000_000_000);
BSD::Resource::setrlimit(BSD::Resource::RLIMIT_AS(),    4_000_000_000, 4_000_000_000);

use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use Cwd;
use File::Basename qw(dirname basename);
use File::Path qw(mkpath rmtree);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use Digest::SHA;
use Sys::Hostname qw(hostname);
our %Opt;
lock_keys %Opt, map { /([^=!\|]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

$Opt{report} ||= 0;
$Opt{install} //= 1;
$Opt{test} //= 1;
$Opt{tmpdir} ||= "/tmp";
$Opt{dotcpanhome} ||= "$ENV{HOME}/.cpan";
unless (defined $Opt{usegit}) {
    if (-d ".git") {
        $Opt{usegit} = 1;
    } else {
        $Opt{usegit} = 0;
    }
}

sub cleanup_or_die {
    # error: Untracked working tree file 'lib/Search/Dict.pm' would be overwritten by merge.
    if ($Opt{usegit}) {
        unless (0==system git => clean => "-dfx") {
            die;
        }
        my $dirty = `git status --porcelain --untracked-files=no`;
        until (!$dirty) {
            system git => reset => "--hard";
            $dirty = `git status --porcelain --untracked-files=no`;
        }
    }
}

cleanup_or_die;
my $gitdescribe;
if ($Opt{usegit}) {
    chomp($gitdescribe = `git describe`); # eg: v5.19.6-105-g448f81e
} else {
    $gitdescribe = basename(cwd());
}
my($perlversion) = $gitdescribe =~ /(?:v|perl-)(5[\d\.]+)/;
if (!defined $Opt{patchperl}) {
    require version;
    my $perlversionnumeric = version->new($perlversion)->numify;
    if ($perlversionnumeric <= "5.028003") {
        $Opt{patchperl} = 1;
    } elsif ($perlversionnumeric >= "5.030000" && $perlversionnumeric >= "5.030010") { # actually only needed on debugging
        $Opt{patchperl} = 1;
    } else {
        $Opt{patchperl} = 0;
    }
}
our $HAVE_DEVEL_PATCHPERL;
if ($Opt{patchperl}) {
    local $@;
    $HAVE_DEVEL_PATCHPERL = eval { require Devel::PatchPerl; Devel::PatchPerl->import("2.08"); 1; };
    if (!!$Opt{patchperl} && !$HAVE_DEVEL_PATCHPERL) {
        die "Option --patchperl is set but we do not have Devel::PatchPerl; please install it";
    }
    if ($Devel::PatchPerl::VERSION eq '1.54') {
        $HAVE_DEVEL_PATCHPERL = 0; # https://rt.cpan.org/Ticket/Display.html?id=128573
        warn "I'll spare you the use of Devel::PatchPerl 1.54, 1.54 is too broken, see #128573";
    }
}
if ($Opt{patchperl} && $HAVE_DEVEL_PATCHPERL) {
    if ($Opt{verbose}) {
        warn qq{About to call Devel::PatchPerl->patch_source("$perlversion", ".")};
    }
    eval { Devel::PatchPerl->patch_source($perlversion, "."); };
    if ($@) {
        warn "Alert: Devel::PatchPerl failed for perl version $perlversion with error $@!";
        use Term::Prompt qw(prompt);
        my $answer = lc prompt "x", "Shall I continue? (y/n)", "", "n";
        if ($answer eq "n") {
            print "Exiting per user request\n";
            exit;
        }
    } elsif ($Opt{verbose}) {
        warn qq{End Of Devel::PatchPerl->patch_source("$perlversion", ".")};
    }

} elsif (!$Opt{patchperl}) {
    require version;
    my $perlversionnumeric = version->new($perlversion)->numify;
    my $min = "5.022001";
    if ($perlversionnumeric < $min) {
        die "ALERT: Found perl $perlversionnumeric (<$min); I have to tell you that we really need Devel::PatchPerl";
    }
}
$Opt{jobs} = 3 unless defined $Opt{jobs};
if ($Opt{ud} eq "rand") { # one rare place where overwriting an option feels ok
    $Opt{ud} = (qw(UD UU DD DU))[int rand 4];
}
my($useithreads,$uselongdouble) = unpack "aa", $Opt{ud} ||= "UD";
my($debuggingoption, $debuggingoption2);
if (defined $Opt{debugging}) {
    if (defined $Opt{debuggingoption}) {
        die "debugging and debuggingoption not meant to be combined";
    } elsif ($Opt{debugging}) { # --debugging
        $debuggingoption = "EBUGGING=-g";
    } else { # --nodebugging
        $debuggingoption = "";
    }
} elsif (defined $Opt{debuggingoption}) {
    if ($Opt{debuggingoption} eq "rand") {
        my @o = (("")x7,("DEBUGGING=-g")x5,"DEBUGGING=both");
        $debuggingoption = $o[int rand scalar @o];
    } elsif ($Opt{debuggingoption} =~ /^EBUGGING=(?:-g|both)/) {
        $debuggingoption = $Opt{debuggingoption};
    } else {
        warn "WARNING: Got unusual debugging option '$Opt{debuggingoption}'; not sure whether I should accept that";
        $debuggingoption = $Opt{debuggingoption};
    }
} else {
    $debuggingoption = "EBUGGING=-g";
}
{
    # older perls did not understand EBUGGING=-g, only optimize=-g

    # commit eaf812ae1a347289872c15b0fb8a27a85b03dc2f
    # Author: H.Merijn Brand <h.m.brand@xs4all.nl>
    # Date:   Mon Apr 17 12:18:07 2006 +0000
    #
    #     Support for -DDEBUGGING and its alias -DEBUGGING

    require version;
    my $perlversionnumeric = version->new($perlversion)->numify;
    if ($perlversionnumeric < 5.010) {
        if (   $debuggingoption eq "EBUGGING=-g"
            || $debuggingoption eq "EBUGGING=both"
           ) {
            $debuggingoption = "optimize=-g";
            if ($debuggingoption eq "EBUGGING=both") {
                $debuggingoption2 = "EBUGGING";
            }
        }
    }
}
my $hostname = hostname;
$hostname =~ s/\..*//;
my $USER = $ENV{USER};
unless ($Opt{prefix}) {
    if (0 && $hostname eq "k83") { # obsolet 2018
        $Opt{prefix} = "/home/$USER/src/perl/repoperls/installed-perls/perl";
    } else {
        $Opt{prefix} = "/home/$USER/src/perl/repoperls/installed-perls/host/$hostname";
    }
}
warn "prefix is going to be '$Opt{prefix}'. Please interrupt if this is not your intention\n";
{
    local $|=1;
    for (1..0) {
        printf "\r%d   ", 5-$_;
        sleep 1 if $_;
    }
    print "\n";
}
my @cargs =
    (
     "-Dmyhostname=$hostname",
     "-Dinstallusrbinperl=n",
     "-Uversiononly",
     "-Dusedevel",
     "-des",
     "-Ui_db",
     # 20160102: the -lnm surprise seems to make this necessary:
     "-Dlibswanted=cl pthread socket inet nsl gdbm dbm malloc dl ld sun m crypt sec util c cposix posix ucb BSD gdbm_compat",
     "-$useithreads"."useithreads",
     "-$uselongdouble"."uselongdouble",
     $debuggingoption ? "-D$debuggingoption" : (),
     $debuggingoption2 ? "-D$debuggingoption2" : (),
     (map { "-$_" } @{$Opt{addopts}||[]})
    );
my $sha = Digest::SHA->new(1);
for my $c (@cargs) {
    $sha->add($c);
}
my $hex = $sha->hexdigest;
my $prefix;

for (my $i=4; $i<length($hex); $i++) {
    my $cargshash = substr($hex,0,$i);
    $prefix = "$Opt{prefix}/$gitdescribe/$cargshash";
    unless (-e "$prefix/bin/perl") {
        last;
    }
}
unshift @cargs, "-Dprefix=$prefix";
unless (0==system "./Configure", @cargs) {
    die;
}
my @makes = (["make"], ["make", "test"]);
$ENV{PERL_CANARY_STABILITY_NOPROMPT} = 1;
for my $make_i (0 .. ($Opt{test}?1:0)) {
    my @make = @{$makes[$make_i]};
    if ($Opt{jobs} && $Opt{jobs} > 1) {
        if ($make_i == 0) {
            push @make, "-j$Opt{jobs}";
        } elsif ($make_i == 1) {
            $ENV{TEST_JOBS} = $Opt{jobs};
            $make[1] =~ s/\Atest\z/test_harness/ or die;
        }
    }
    my $ret = system @make;
    if (0==$ret) {
    } elsif ($Opt{keepfilesonerror} || $Opt{keepfiles}) {
        die "Running make[@make] returned ret[$ret], dieing according to 'keepfiles(onerror)?'";
    } else {
        cleanup_or_die;
        warn sprintf
            (
             "Alert: %s: %s returning 125 after running make[%s] returned ret[%s]",
             scalar localtime,
             $0,
             join(" ", @make),
             $ret,
            );
        exit 125;
    }
}
if ($Opt{install}) {
    unless (0==system "./installperl") {
        cleanup_or_die;
        die;
    }
} else {
    warn "Option --install is off, not installing this perl";
}
if ($Opt{keepfiles}) {
    warn "No cleanup according to --keepfiles option. You may want to 'git clean -dfx'";
} else {
    cleanup_or_die;
}
my $inverse = 0;
my $m = $Opt{module} || [];
if (@$m == 1 and $m->[0] =~ /,/) {
    $m = [ split /,/, $m->[0] ];
}
if ($Opt{inversemodule}) {
    $inverse = 1;
    push @$m, $Opt{inversemodule};
}
if ($Opt{report} || @$m) {
    my $transient_build_dir = 1;
    my $bdir;
    if ($transient_build_dir) {
        $bdir = File::Temp::tempdir(
                                    "makeperl-XXXXXX",
                                    DIR => $Opt{tmpdir},
                                    CLEANUP => 1,
                                   ) or die $!;
    }
    require Sys::Hostname;
    my $hostname = Sys::Hostname::hostname();
    my @hostspecific;
    unless ($hostname eq "k83") {
        # -I ~/.cpan-k75 -MCPAN::MyConfig"
        @hostspecific = 
            (
             "-I",
             "$ENV{HOME}/.cpan-$hostname",
             "-MCPAN::MyConfig",
            );
    }
    my @cpanshell =
        (
         "$prefix/bin/perl",
         @hostspecific,
         "-I$Opt{dotcpanhome}",
         "-M-lib='.'",
         "-MCPAN::MyConfig",
         "-MCPAN",
         "-e","\$CPAN::Config->{test_report}=0;", # new 2019-07-16: no reports from here, avoiding premature leaks
         $bdir ? ("-e","\$CPAN::Config->{build_dir}=q{$bdir};") : (),
         "-e",
        );
    $ENV{HARNESS_OPTIONS} = "j".$Opt{jobs} if $Opt{jobs} > 1;
    if ($Opt{report}) {
        my @script;
        my $opcfv = `$prefix/bin/perl -MCPAN::FTP -e print\\\$CPAN::FTP::VERSION;`;
        if (CPAN::Version->vlt($opcfv,"5.5009")) { # very kamikaze
            my $cwd = cwd;
            eval {
                chdir "/home/$USER/cpanpm";
                mkpath "tmp-$$";
                chdir  "tmp-$$";
                0 == system git => "clone", "..", "."
                    or die "Could not clone cpanpm to tmp-$$";
                #                             vvvvv kamikaze here vvvvv (no dependency resolution)
                0 == system qq{$prefix/bin/perl Makefile.PL; $prefix/bin/perl -Ilib -MCPAN -e 'install(q(.))'}
                    or die "Could not run make install from /home/$USER/cpanpm";
                chdir "/home/$USER/cpanpm";
                rmtree "tmp-$$";
            } or warn "Error while trying to install CPAN from repo: $!";
            chdir $cwd;
        }
        my $opcv = `$prefix/bin/perl -MCPAN -e print\\\$CPAN::VERSION;`; # other perl cpan version
        my %minv;
        # $minv{version} = '2.36';
        # $minv{version} = '2.38-TRIAL';
        $minv{version} = '2.38';
        $minv{distro}  = "ANDK/CPAN-$minv{version}.tar.gz";
        if (CPAN::Version->vlt($opcv,$minv{version})) {
            push @script, qq{install("$minv{distro}");
                die unless
                    CPAN::Shell->expand(
                        Module=>"CPAN"
                    )->uptodate;
                };
        }
        push @script, qq{install(
                  "parent",
                  "version",
                  "CPAN::Meta::Requirements",
                  "JSON::PP",
                  "JSON::XS",
                  "Log::Log4perl",
                  "Log::Dispatch::File",
                  "Time::Piece",
                  "Bundle::CPANxxl",
                  "Bundle::CPANReporter2",
                  "BSD::Resource",
                  "Log::ger",
                  "Log::ger::Output::File",
                  "Log::ger::Layout::Pattern",
                  "Term::Prompt",
                  "Algorithm::Numerical::Shuffle",
                  "Parse::CPAN::Packages::Fast",
                  # "Module::Info",
                  # "Devel::PatchPerl",
                  # "Module::Versions::Report",
                  # "V",
              );
              die unless
                  CPAN::Shell->expand(
                      Module=>"Test::Reporter::Transport::Metabase"
                  )->uptodate;
             };
        for my $script (@script) {
            my(@system) =
                (@cpanshell,
                 $script,
                );
            warn "DEBUG: system[@system]";
            unless (0==system @system) {
                die "Alert: problem running system[@system]";
            }
        }
    }
    my $ret = 0;
    if ($m && @$m) {
        for (@$m) {
            s/-/::/g if /-/ and !m|/|;
        }
        my $install = join ",", map { "'$_'" } @$m;
        my $last = $m->[-1];
        my $shellcmd = "install($install); die unless CPAN::Shell->expand(Module => '$last')->uptodate;";
        if ($Opt{suppressreadline}) {
            $shellcmd = "\$CPAN::Suppress_readline=1;$shellcmd";
        }
        if (0==system @cpanshell, $shellcmd) {
            $ret = 0;
        } else {
            $ret = 1;
        }
        if ($inverse) {
            $ret ^= 1
        }
    }
    if (!$ret && $Opt{successchecker}) {
        if (0==system "$prefix/bin/perl", $Opt{successchecker}) {
            $ret = 0;
        } else {
            $ret = 1;
        }
    }
    exit $ret;
}
# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
