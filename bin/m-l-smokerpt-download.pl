#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

m-l.org/~perl/smoke/perl/... holds one month worth of smokereports.

This script fetches a collection of reports and stores them in a
single YAML file.

=head1 BUGS

This is a one-off script with shortcuts.

=head1 SEE ALSO

m-l-smokerpt-parse.pl for parsing the contents.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

our $DIR_URL = "http://m-l.org/~perl/smoke/perl/linux/blead_clang/";

use LWP::UserAgent ();
use POSIX ();
use Scalar::Util ();
use Text::Trim qw(trim);
use XML::LibXML ();
use YAML::Syck ();

my $ua = LWP::UserAgent->new;
my $p  = XML::LibXML->new;
my $S;

my $resp = $ua->get($DIR_URL);
if ($resp->is_success) {
    my $content = $resp->decoded_content;
    my $dom = XML::LibXML->load_html(string => $content);
 TR: for my $tr_ele ($dom->findnodes("/html/body/table/tr")){
        my($filename_attr) = $tr_ele->findnodes("td[2]/a/\@href") or next;
        my $filename = $filename_attr->value;
        next TR unless $filename =~ /^rpt.*\.rpt$/;
        my $timestamp = $tr_ele->findnodes("td[3]/.");
        trim $timestamp;
        my $resp = $ua->get("$DIR_URL/$filename");
        if ($resp->is_success) {
            my $content = $resp->decoded_content;
            $S->{$filename}{content} = $content;
            $S->{$filename}{timestamp} = $timestamp;
        } else {
            die
        }
    }
} else {
    die
}

my $ts = POSIX::strftime "%FT%T", localtime;
my $outfile = __FILE__ . "-$ts.out";
YAML::Syck::DumpFile($outfile, $S);

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
