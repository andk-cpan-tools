#!/usr/bin/perl

my $PERL=shift or die;
use File::Basename qw(dirname);
my $BINDIR=dirname $0;
# set -x
# set -e
while (){
    # cd $SOLVER_VARDIR/workdir
    warn "About to run $BINDIR/basesmoker.pl -v --transient_build_dir\n";
    0 == system $PERL, "$BINDIR/basesmoker.pl", "-v", "--transient_build_dir" or die;
    if ( -e "$BINDIR/basesmoker.stop" ){
        warn "Found $BINDIR/basesmoker.stop, stopping smoker\n";
        last;
    } else {
        sleep 1; # give ^C a chance
	warn "basesmoker.run.pl is starting the next instance of basesmoker.pl\n";
    }
}
