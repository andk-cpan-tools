#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION



=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(basename dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

use Imager; use YAML::XS; my @yaml = map { YAML::XS::LoadFile $_ or die "cannot load yaml file: $!"; } @ARGV;
my $xsize = 1200;  my $ysize = 800;
my $img    = Imager->new(xsize=>$xsize, ysize=>$ysize, channels=>4);
my $white  = Imager::Color->new( 255, 255, 255 );
my $gray   = Imager::Color->new( 192, 192, 192 );
my $black  = Imager::Color->new(  10,  10,  10 );
$img->flood_fill(x=>50, y=>50, color=>$white);
my $xgridlines = 30; my $ygridlines = 12;
GRIDX: for (my $xgridline = 0; $xgridline <= $xgridlines; $xgridline++) {
    my $x = 10 + ($xsize - 20) * ($xgridline/$xgridlines);
    $img->line(color=>$gray, x1=>$x, x2=>$x,      y1=>10, y2=>$ysize-10, aa=>1, endp=>1 );
}
GRIDY: for (my $ygridline = 0; $ygridline <= $ygridlines; $ygridline++) {
    my $y = 10 + ($ysize - 20) * ($ygridline/$ygridlines);
    $img->line(color=>$gray, x1=>10, x2=>$xsize-10,      y1=>$y, y2=>$y, aa=>1, endp=>1 );
}
my($max_total_secs, $max_total_lines);
YAML: for my $yaml_i (0..$#yaml){
    my $yaml = $yaml[$yaml_i];
    my @k = sort keys %{$yaml->{G}};
    $max_total_secs  //= $yaml->{G}{$k[-1]}{T};
    $max_total_lines //= $yaml->{G}{$k[-1]}{N};
    $max_total_secs  = $yaml->{G}{$k[-1]}{T} if $yaml->{G}{$k[-1]}{T} > $max_total_secs;
    $max_total_lines = $yaml->{G}{$k[-1]}{N} if $yaml->{G}{$k[-1]}{N} > $max_total_lines;
}
my $font = Imager::Font->new(file => "/usr/share/fonts/truetype/freefont/FreeSans.ttf");
$img->align_string(
    string =>int($max_total_lines),
    x      =>10,
    y      =>10,
    aa     =>1,
    halign =>'left',
    valign =>'top',
    font   =>$font,
    size   => 18,
    color  => $black,
);
$img->align_string(
    string =>int($max_total_secs),
    x      =>$xsize-10,
    y      =>$ysize-10,
    aa     =>1,
    halign =>'right',
    valign =>'baseline',
    font   =>$font,
    size   => 18,
    color  => $black,
);
DAY: for (my $i = 1;$i < $max_total_secs/86400; $i++) {
    my $x = $i*86400*($xsize-20)/$max_total_secs + 10;
    $img->line(
        color => $black,
        x1 => $x,
        x2 => $x,
        y1 => $ysize-20,
        y2 => $ysize-10,
        aa => 1,
        endp => 1,
    );
}

my @hue = map { (360/scalar(@yaml)) * $_ } 0..$#yaml;
$|=1;
for my $yaml_i (0..$#yaml){
    printf "\r%d/%d ", 1+$yaml_i, 1+$#yaml;
    my $yaml   = $yaml[$yaml_i];
    my $curcol = Imager::Color->new( hue => $hue[$yaml_i], s=>0.95, v=>0.9,  );
    my @k = sort keys %{$yaml->{G}};
    for my $t (@k){
        my $e = $yaml->{G}{$t};
        $img->circle( color => $curcol, r => 1, aa => 1, filled => 1, x => 10 + ($xsize-20)*$e->{T}/$max_total_secs, y => $ysize - 10 - ($ysize-20)*$e->{N}/$max_total_lines );
    }
    my $string = sprintf "%s (%s)", basename($ARGV[$yaml_i]), $yaml->{uniq_path_snippet} || $yaml->{hostname};
    $img->align_string(
        string =>$string,
        x      =>10,
        y      =>75+25*$yaml_i,
        aa     =>1,
        halign =>'left',
        valign =>'baseline',
        font   =>$font,
        size   => 18,
        color  => $curcol,
    );
    $img->write(file=>"t.png");
}
print "\n";

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
