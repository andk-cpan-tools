#!/usr/bin/perl

# die " Deadly, don't use yet. We must implement date verification as well, at least ";

# Have eliminated 10 lines: Dancer2-Logger-Radis-0.001(=>0.002) Yote-0.1022(=>2.02) DBD-SQLAnywhere-2.08(=>2.13) Template-Mustache-1.0.0_0(=>1.1.0) Data-Rmap-0.64(=>0.65) Pcore-PDF-v0.1.0(=>v0.4.4) IO-Socket-Socks-0.71(=>0.74) Data-Sah-Resolve-0.003(=>0.007) Git-PurePerl-0.51(=>0.53) Archive-Zip-1.57(=>1_11)


# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--debug|d!>

More output

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Visits fastapi for each command line argument. Arguments can either be
vdistro (Test-Pockito-0.02) or pretty_id style
(JWIED/HTML-EP-0.2011.tar.gz) or pretty_id without filename suffix
(FVULTO/HTML-Toc-1.12)

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Getopt::Long;
use Hash::Util qw(lock_keys);
use Pod::Usage;
use Time::HiRes qw(sleep);
use Time::Piece;

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}

use LWP::UserAgent;
use JSON::XS;
use CPAN::DistnameInfo;
use List::AllUtils qw(reduce);
use CPAN::Version;

my $ua = LWP::UserAgent->new();
$ua->default_header("Accept-Encoding", "gzip");
my $jsonxs = JSON::XS->new->indent(0);

ARG: for my $i (0..$#ARGV) {
    my($maybedistv) = $ARGV[$i];
    my $CDA;
    if ($maybedistv =~ m{/}) {
        unless ($maybedistv =~ m{/.*\.(tar\.gz|zip|tar.bz2)}) {
            $maybedistv .= ".tar.gz";
        }
        $CDA = substr($maybedistv,0,1) . '/' . substr($maybedistv,0,2) . '/' . $maybedistv;
    } else {
        $CDA = "A/AA/AAA/$maybedistv.tar.gz";
    }
    my $d = CPAN::DistnameInfo->new($CDA) or die "no d for argument $CDA";
    my $distv = $d->distvname or die "no distv for $CDA";
    my %w = ( distv => $distv ); # work
    $w{version} = $d->version;
    warn "distv => $distv\n" if $Opt{debug};
    warn "  version     => $w{version}\n" if $Opt{debug};
    my $query = sprintf "http://fastapi.metacpan.org/v1/release/_search?q=distribution:%s&fields=name,date,status,version,author&size=400&_source=tests", $d->dist;
    my $resp = $ua->get($query);
    unless ($resp->is_success) {
        warn sprintf "No success visiting '%s': %s; sleeping %.3f\n",
            $query, $resp->code;
        next ARG;
    }
    # print $query;
    my $jsontxt = $resp->decoded_content;
    my $j = eval { $jsonxs->decode($jsontxt); };
    if (!$j || $@) {
        my $err = $@ || "unknown error";
        die "Error while decoding '$jsontxt': $err";
    }
    my $hits = $j->{hits}{hits};
    my($matchingrelease) = grep { $_->{fields}{name} eq $distv } @$hits;
    unless ($matchingrelease) {
        warn "Did not find release for $distv\n";
        next ANNO;
    }
    my($releasedate) = $matchingrelease->{fields}{date};
    warn "  releasedate => $releasedate\n" if $Opt{debug};
    my($author) = $matchingrelease->{fields}{author};
    warn "  author      => $author\n" if $Opt{debug};
    $w{cpanversion} = reduce {
        CPAN::Version->vgt($a,$b) ? $a : $b
        } map { $_->{fields}{version} } grep { $_->{fields}{date} ge $releasedate } @$hits;
    if (CPAN::Version->vgt($w{cpanversion},$w{version})) {
        warn "  newercpanversion => $w{cpanversion}\n" if $Opt{debug};
        my($highest_distro) = grep { $_->{fields}{version} eq $w{cpanversion} } @$hits;
        warn "  newerreleasedate => $highest_distro->{fields}{date}\n" if $Opt{debug};
        if ($highest_distro->{fields}{author} ne $author) {
            warn "  newerauthor      => $highest_distro->{fields}{author}\n" if $Opt{debug};
        }
    }
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
