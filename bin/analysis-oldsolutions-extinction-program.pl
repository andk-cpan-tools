#!/usr/bin/perl

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=item B<--max=i>

Stop after how many

=item B<--minage=f>

Minimum age in days of the target directory
(measured with C<-M>).

=back

=head1 DESCRIPTION

We scan the C<solved> directory for families of files for one vdistro.
If there is no corresponding tarfile or directory in the reports
collection area, we remove it.

The first run on 20171230 removed about 3.5 G.

=cut


use FindBin;
use lib "$FindBin::Bin/../CPAN-Blame/lib";

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Pod::Usage;
use Hash::Util qw(lock_keys);
use CPAN::Blame::Config::Cnntp;

our %Opt;
lock_keys %Opt, map { /([^=|!]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);
if ($Opt{help}) {
    pod2usage(0);
}
$Opt{minage} //= 1440;

my $root = $CPAN::Blame::Config::Cnntp::Config->{ctgetreports_dir} || die;
my $workdir = $CPAN::Blame::Config::Cnntp::Config->{solver_vardir} || die;
$workdir =~ s"$"/workdir/solved";
opendir my $dh, $workdir or die;
my(%seen, $i);
DIRENT: for my $dirent (readdir $dh) {
    # CPAN-Mini-Indexed-0.01_01.1.slv.LCK, CPAN-Mini-Indexed-0.03_01.2.slv, CPAN-Mini-Indexed-0.03_01.2.slvdv.gz, CPAN-Mini-Indexed-0.03_01.2.yml
    my($stem) = $dirent =~ /(.+)(?:\.slv\.LCK|\.slv|\.slvdv.gz|\.yml)$/ or next;
    next if $seen{$stem}++;
    my $troot = sprintf "%s/%s/%s", $root, substr($stem,0,1), $stem;
    my @trcand = grep { -e $_ } glob("$troot\{,.tar}");
    # if (-e "$troot.tar") {
    if (@trcand) {
        my $trcand = scalar @trcand;
        # warn "  has $trcand tarballs or something: @trcand\n";
        next DIRENT;
    }
    my @cand = grep { -e $_ } glob("$workdir/$stem\{.slv.LCK,.slv,.slvdv.gz,.yml}");
    unless (@cand) {
        warn "Warning: no cand for $stem, skipping";
        next DIRENT;
    }
    if (map { /[\*\?]/ } @cand) {
        die "Illegal filename with glob character discovered: @cand";
    }
    # system "ls -l @cand";
    my @ages = sort { $a <=> $b } map { -M $_ } grep { !/\.slv\.LCK$/ } @cand;
    unless (@ages){
        @ages = sort { $a <=> $b } map { -M $_ } @cand;
    }
    if ($ages[0] < $Opt{minage}) {
        # warn "  is younger than $Opt{minage} days ($ages[0])\n";
        next DIRENT;
    }
    warn "$stem\n";
    warn "  about to unlink @cand\n";
    unlink @cand or die "Could not unlink: $!";
    system df => $root;
    last if $Opt{max} && ++$i >= $Opt{max};
}

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
