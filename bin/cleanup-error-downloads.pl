#!/usr/bin/perl

use Sys::Hostname;
die "not yet ported to ds8143" if hostname eq "ds8143";

# use 5.010;
use strict;
use warnings;

=head1 NAME



=head1 SYNOPSIS



=head1 OPTIONS

=over 8

=cut

my @opt = <<'=back' =~ /B<--(\S+)>/g;

=item B<--help|h!>

This help

=back

=head1 DESCRIPTION

Quick and dirty: we have plenty of files containing

    <h1 class="pagetitle">Error</h1>

and/or

    <title>CPAN Testers Reports: Error</title>

and/or

    <h2>Error</h2>

They all simply get removed together with their headers files.

=cut


use FindBin;
use lib "$FindBin::Bin/../lib";
BEGIN {
    push @INC, qw(       );
}

use Dumpvalue;
use File::Basename qw(dirname);
use File::Path qw(mkpath);
use File::Spec;
use File::Temp;
use Getopt::Long;
use Hash::Util qw(lock_keys);

our %Opt;
lock_keys %Opt, map { /([^=]+)/ } @opt;
GetOptions(\%Opt,
           @opt,
          ) or pod2usage(1);

use File::Find;
$|=1;
my $i = 0;
find(
     sub {
         return unless /^\d+\.gz$/;
         return if -s $File::Find::name > 3000;
         open my $fh, "-|", zcat => $File::Find::name or die "Could not open '$File::Find::name': $!";
         my $delete;
         while (<$fh>) {
             if (m{<h1 class="pagetitle">Error</h1>}) {
                 $delete++;
                 last;
             }
         }
         if ($delete) {
             unlink $File::Find::name or die "Could not unlink '$File::Find::name': $!";
             my $headers = $File::Find::name;
             $headers =~ s/gz$/headers/;
             unlink $headers or die "Could not unlink '$headers': $!";
             $i++;
             print "\r$i    ";
         }
     },
     "/home/ftp/cpantesters/reports"
);
print "\n";

# Local Variables:
# mode: cperl
# cperl-indent-level: 4
# End:
