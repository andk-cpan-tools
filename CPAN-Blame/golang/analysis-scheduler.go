package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"regexp"
)

// {"Id":"File-Locate-Iterator-21","CalcNext":1,"CalcTimestamp":0,"CalcNextETA":0,"DebugRating":500}
type StateType struct {
	Id                string
	CalcNext          uint8 // not_scheduled(0) | scheduled(1) | in_progress(2)
	CalcTimestamp     uint64
	CalcNextETA       uint64
	Pass              uint32
	Fail              uint32
	DebugRating       uint32
	DebugRunningStart uint32
	DebugRunningSetId uint32
}

// {"distv":"File-Locate-Iterator-21","start":1397696507}
type JobqueueJobsXDescr struct {
	Distv string
	Start uint32
}

// {"Id":"nonsense-0.01","VoteAccepted":true,"VoteRefusedBecause":nil,...
type VoteType struct {
	Id                string
	VoteAccepted      bool
	VoteRefusedReason string
}

// {"Id":"nonsense-0.01","LcPass":123,"LcFail":13,...
type LastCalcType struct {
	Id     string
	LcPass uint32
	LcFail uint32
}

func voteHandler(w http.ResponseWriter, r *http.Request, title string) {
	c, err := redis.Dial("tcp", ":6379")
	if err != nil {
		panic(err)
	}
	defer c.Close()

	s := new(StateType)
	s.Id = title
	set_passfail(c, s)
	set_calcnext(c, s)

	l := new(LastCalcType)
	l.Id = s.Id
	set_lcpassfail(c, l)

	v := new(VoteType)
	v.Id = s.Id

	mustenqueue := false
	if s.CalcNext == 1 { // already scheduled unsure what the
		// right thing to do is here; thinking about it hurts
		// and costs time, let's discuss this later
		v.VoteAccepted = true // but silently doing nothing
	} else if s.CalcNext == 2 {
		v.VoteAccepted = false
		v.VoteRefusedReason = "Calculation already running, voting closed"
	} else if l.LcPass == 0 && l.LcFail == 0 {
		mustenqueue = true
	} else if l.LcPass == s.Pass && l.LcFail == s.Fail {
		v.VoteAccepted = false
		v.VoteRefusedReason = "No new results available that would justify a new calc"
	} else {
		mustenqueue = true
	}
	if mustenqueue {
		_, err := redis.Int(c.Do("ZADD", "analysis:jobqueue:q", 10000, title))
		if err != nil {
			v.VoteAccepted = false
			v.VoteRefusedReason = "-ERR"
		} else {
			v.VoteAccepted = true
		}
	}

	b, err := json.Marshal(v)
	fmt.Fprintf(w, "%s\n", b)
}

func stateHandler(w http.ResponseWriter, r *http.Request, title string) {
	c, err := redis.Dial("tcp", ":6379")
	if err != nil {
		panic(err)
	}
	defer c.Close()

	s := new(StateType)
	s.Id = title

	set_passfail(c, s)
	set_calcnext(c, s)
	set_calctimestamp(c, s)
	set_eta(c, s)

	b, err := json.Marshal(s)
	fmt.Fprintf(w, "%s\n", b)
}

func set_calctimestamp(c redis.Conn, s *StateType) {
	calctimestamp := -1 // initialize to illegal value to detect nil.
	calctimestamp, err := redis.Int(c.Do("ZSCORE", "analysis:distv:calctimestamp", s.Id))
	if err != nil {
		calctimestamp = -1
	}
	if calctimestamp > -1 {
		s.CalcTimestamp = uint64(calctimestamp)
	}
}

func set_eta(c redis.Conn, s *StateType) {
	eta := -1 // initialize to illegal value to detect nil.
	eta, err := redis.Int(c.Do("ZSCORE", "analysis:distv:eta", s.Id))
	if err != nil {
		eta = -1
	}
	if eta > -1 {
		s.CalcNextETA = uint64(eta)
	}
}

func set_passfail(c redis.Conn, s *StateType) {
	fail := -1
	fail, err := redis.Int(c.Do("ZSCORE", "analysis:distv:fail", s.Id))
	if err != nil {
		fail = -1
	}
	if fail > -1 {
		s.Fail = uint32(fail)
	}
	pass := -1
	pass, err = redis.Int(c.Do("ZSCORE", "analysis:distv:pass", s.Id))
	if err != nil {
		pass = -1
	}
	if pass > -1 {
		s.Pass = uint32(pass)
	}
}

func set_lcpassfail(c redis.Conn, l *LastCalcType) {
	fail := -1
	fail, err := redis.Int(c.Do("ZSCORE", "analysis:distv:lcfail", l.Id))
	if err != nil {
		fail = 0
	}
	if fail > -1 {
		l.LcFail = uint32(fail)
	}
	pass := -1
	pass, err = redis.Int(c.Do("ZSCORE", "analysis:distv:lcpass", l.Id))
	if err != nil {
		pass = 0
	}
	if pass > -1 {
		l.LcPass = uint32(pass)
	}
}

func set_calcnext(c redis.Conn, s *StateType) {
	rating := -1 // initialize to illegal value to detect nil.
	rating, err := redis.Int(c.Do("ZSCORE", "analysis:jobqueue:q", s.Id))
	if err != nil {
		rating = -1
	}
	if rating > -1 {
		s.DebugRating = uint32(rating)
		s.CalcNext = 1
	} else {
		var tismember bool
		var tstart uint32
		var tid uint32
		v, err := redis.Values(c.Do("SMEMBERS", "analysis:jobqueue:runningjobs"))
		if err != nil {
			panic(err)
		}
		for len(v) > 0 {
			// $h{id} = $id;
			var id int
			v, err = redis.Scan(v, &id)
			if err != nil {
				panic(err)
			}
			// my($json) = $redis->get("analysis:jobqueue:jobs:$id\:descr");
			var jsonstr string
			key := fmt.Sprintf("analysis:jobqueue:jobs:%d:descr", id)
			jsonstr, err = redis.String(c.Do("GET", key))
			if err != nil {
				fmt.Println("missing key: ", key)
				panic(err)
			}
			// my($j) = $jsonxs->decode($json);
			b := []byte(jsonstr)
			var j JobqueueJobsXDescr
			err = json.Unmarshal(b, &j)
			if err != nil {
				panic(err)
			}
			// $h{distv} = $j->{distv};
			thisdistv := j.Distv
			if thisdistv == s.Id {
				tismember = true
				tstart = j.Start
				tid = uint32(id)
				// last ???
			}
		}
		if tismember {
			s.CalcNext = 2
			s.DebugRunningStart = tstart
			s.DebugRunningSetId = tid
		}
	}
}

var validPath = regexp.MustCompile("^/scheduler/(state|vote)/([a-zA-Z0-9_.-]+)$")

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

var (
	addr = flag.Bool("addr", false, "find open address and print to final-port.txt")
)

func main() {
	flag.Parse()
	http.HandleFunc("/scheduler/state/", makeHandler(stateHandler))
	http.HandleFunc("/scheduler/vote/", makeHandler(voteHandler))

	if *addr {
		l, err := net.Listen("tcp", "127.0.0.1:0")
		if err != nil {
			log.Fatal(err)
		}
		err = ioutil.WriteFile("final-port.txt", []byte(l.Addr().String()), 0644)
		if err != nil {
			log.Fatal(err)
		}
		s := &http.Server{}
		s.Serve(l)
		return
	}

	http.ListenAndServe(":3001", nil)
}
