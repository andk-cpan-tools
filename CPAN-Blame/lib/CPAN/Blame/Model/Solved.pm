{
    package CPAN::Blame::Model::Solved::sortable_perl;
    sub as_html {
        my($self) = @_;
        sprintf "<!--%s-->%s", @{$self}{"sortable","native"};
    }
}

package CPAN::Blame::Model::Solved;

use strict;
use warnings;
use parent 'Catalyst::Model';

use YAML::Syck;
use CPAN::Blame::Config::Cnntp;
use lib "/home/src/perl/CPAN-WWW-Testers-Generator-0.30-MB9KFm/blib/lib";
# ds8143 has it in: "/home/andreas/src/installed-perls/v5.16.0/4e6d/lib/site_perl/5.16.0/CPAN/WWW/Testers/Generator/Database.pm"
# k83 has it in: ../...../fromds8143
use lib "/home/k/src/andk-cpan-tools/../fromds8143";
use List::MoreUtils qw(uniq);
use YAML::Syck;
use Encode ();
use CPAN::Blame::Util;

my $Slvdir = "$CPAN::Blame::Config::Cnntp::Config->{solver_vardir}/workdir/solved";

sub db_quickaccess_handle {
    return CPAN::Blame::Config::Cnntp::common_quickaccess_handle();
}

sub my_get_query { # note: copy and paste from cnntp-solver
    my($dbi,$sql,@args) = @_;
    my $sth = $dbi->prepare($sql);
    my $rv = eval { $sth->execute(@args); };
    unless ($rv) {
        my $err = $sth->errstr;
        warn "Warning: error occurred while executing '$sql': $err";
    }
    my @rows;
    while (my(@trow) = $sth->fetchrow_array()) {
        push @rows, \@trow;
    }
    \@rows;
}

sub ndjson {
    my($self, $p) = @_;
    my $dbi = db_quickaccess_handle();
    my $after_id = 0;
    my $post_date = '000000';
    if (my $guid = delete $p->{after}) {
        my $query = "select id,postdate,dist,version from cpanstats where guid=?";
        my $sth = $dbi->prepare($query);
        my $rv = $sth->execute($guid);
        die "the guid '$guid' was not found in cpanstats" unless $rv > 0;
        my($guidid,$guidpostdate,$guiddist,$guidversion) = $sth->fetchrow_array;
        if ($p->{dist}) {
            if ($p->{dist} ne $guiddist) {
                die "the dist in params '$p->{dist}' does not match the dist in after-record '$guiddist'";
            }
        } else {
            $p->{dist} = $guiddist;
        }
        if ($p->{version}) {
            if ( $p->{version} ne $guidversion) {
                die "the version in params '$p->{version}' does not match the version in after-record '$guidversion'";
            }
        } else {
            # NO DEFAULT!
        }
        $after_id = $guidid;
        $post_date = $guidpostdate;
    } else {
        if ($p->{dist}) {
            my $query = "select postdate from oldestpostdate where dist=? and version=?";
            my $sth = $dbi->prepare($query);
            my $rv = $sth->execute($p->{dist},defined $p->{version} ? $p->{version} : "");
            if ($rv > 0) {
                ($post_date) = $sth->fetchrow_array;
            } else {
                # could be a race condition and then there is a chance
                # we find something in the max postdate but not in
                # earlier postdates
                my $query = "select max(postdate) from oldestpostdate";
                my $sth = $dbi->prepare($query);
                $sth->execute();
                ($post_date) = $sth->fetchrow_array;
            }
        } else {
            die "missing parameter: neither parameter `after` nor `dist` given, cannot continue";
        }
    }
    die "missing parameter dist" unless exists $p->{dist};
    my(@where, @bind);
    for my $k (sort keys %$p) {
        push @where, "$k=?";
        push @bind, $p->{$k};
    }
    use feature 'state';
    state @fields = qw(id guid state tester dist version platform perl osname osvers fulldate);
    state $fields = join ",", @fields;
    state %rev_fields = map { $fields[$_] => $_ } 0..$#fields;
    state $nth_fulldate = $rev_fields{fulldate};
    my $query = "select $fields from cpanstats where " .
        join(" and ", @where, 'id>?', 'postdate>=?') . " order by id";
    push @bind, $after_id, $post_date;
    my $sth = $dbi->prepare($query);
    my $rv = $sth->execute(@bind);
    warn "query=$query;bind=@bind";
    my @rows;
    while (my(@trow) = $sth->fetchrow_array()) {
        $trow[$nth_fulldate] =~ s{^(....)-(..)-(..) (..):(..).*}{$1$2$3$4$5};
        # not using quotemeta() because we believe, it is not the right thing for json
        my $eliminate_control_characters = 0;
        if ($eliminate_control_characters) {
            push @rows, sprintf qq({"id":"%s","guid":"%s","state":"%s","tester":"%s","dist":"%s","version":"%s","platform":"%s","perl":"%s","osname":"%s","osvers":"%s","fulldate":"%s"}\n), map {
                my $x = Encode::decode("UTF-8",$_);
                $x =~ s/\p{Control}//g;
                $x =~ s/([\\"])/\\$1/g;
                Encode::encode("UTF-8",$x);
            } @trow;
        } else {
            push @rows, sprintf qq({"id":"%s","guid":"%s","state":"%s","tester":"%s","dist":"%s","version":"%s","platform":"%s","perl":"%s","osname":"%s","osvers":"%s","fulldate":"%s"}\n), map { s/([\\"])/\\$1/gr } @trow;
        }
    }
    # join "", "#$query |@bind|$rv\n", @rows;
    join "", @rows;
}

sub all {
    my($self,$style,$filter) = @_;
    $style ||= "readdir";
    my @solved;
    if ($style =~ /^db(\+?)$/) {
        my $plus_slv_stat = $1;
        my $dbi = db_quickaccess_handle();
        my $sql = "SELECT distv, yaml FROM distcontext
 WHERE yaml IS NOT NULL AND (greenish IS NULL OR greenish >= '3')";
        my $rows = my_get_query($dbi,$sql);
        my $date_threshold;
        @solved = map {
            my($distv, $yaml)=@$_;
            my $y = YAML::Syck::Load($yaml);
            my $selected;
            if ($filter->{author}) {
                $selected = $filter->{author} eq $y->{author};
            } else {
                $selected = 1;
            }
            if ($selected) {
                $filter->{age}||=91.3;
                $date_threshold ||= do {
                    my @gmtime = gmtime(time-$filter->{age}*86400);
                    $gmtime[4]++;
                    $gmtime[5]+=1900;
                    sprintf "%04d-%02d-%02dT%02d:%02dz", @gmtime[5,4,3,2,1];
                };
                unless ($y->{upload_date}) {
                    warn "Warning: no upload_date for '$distv'/'$y->{distv}'";
                    $y->{upload_date} = "";
                }
                $selected = $y->{upload_date} gt $date_threshold;
                if ($selected && $plus_slv_stat) {
                    $y->{slvfile} = -e "$Slvdir/$y->{distv}.slv";
                }
            }
            $selected ? $y : ();
        } @$rows;
    } elsif ($style eq "readdir") {
        opendir my $dh, $Slvdir or die "Could not opendir '$Slvdir': $!";
        @solved = map {+{distv => $_}} grep {s/\.slv$//} readdir $dh;
        closedir $dh;
    DIRENT: for my $solved (@solved) {
            my $distv = $solved->{distv};
            my $yml = "$Slvdir/$distv.yml";
            if (-e $yml) {
                my $y = eval { YAML::Syck::LoadFile($yml); };
                next DIRENT unless $y;
                while (my($k,$v) = each %$y) {
                    $solved->{$k} = $v;
                }
            }
        }
    } else {
        die "illegal style $style";
    }
    @solved = grep {
        #my $mostrecent = exists $_->{allversions} ? $_->{allversions}[-1]{version} : "";
        #$_->{distv} =~ /\Q$mostrecent\E$/;
        ($_->{greenish}||0) >= 3;
    } @solved;
    no warnings 'uninitialized';
    for (@solved) {
        $self->decode_annotation($_);
        my $want_zwsp = 0;
        if (exists $_->{allversions}
           && @{$_->{allversions}}) {
            my $released = $_->{allversions}[-1]{date};
            $released =~ s/(....)(..)(..)(..)(..)/$1-$2-$3 $4:$5z/;
            # warn "debug: $released";
            $_->{released} = $released;
            $want_zwsp=1;
        } elsif ($_->{upload_date}) { # picked?
            my $released = $_->{upload_date};
            my $success = $released =~ s/(....)-?(..)-?(..)T(..):?(..)/$1-$2-$3 $4:$5z/;
            $success = $released =~ s/(\d\d\d\d-\d\d-\d\d)\(\?\)/$1/ unless $success;
            # warn "debug: $released";
            $_->{released} = $released;
            $want_zwsp=1;
        }
        if ($want_zwsp) {
            $_->{distv_with_zwsp} = $_->{distv};
            $_->{distv_with_zwsp} =~ s/-/-&#x200b;/g;
            $_->{distv_dashesc} = $_->{distv};
            $_->{distv_dashesc} =~ s/-/- /g;
        }
    }
    my $i = 0;
    @solved = map {
        $_->[0]{i} = ++$i;
        $_->[0]
    } sort {
        $b->[1] cmp $a->[1] || $a->[2] cmp $b->[2]
    } map {
        [$_,
         $_->{released},
         $_->{distv},
        ]
    } @solved;
    return \@solved;
}

sub decode_annotation {
    my($self, $e) = @_;
    CPAN::Blame::Util::decode_annotation $e;
}

sub adist {
    my($self,$distv) = @_;
    my $file = "$Slvdir/$distv.slv";
    my ($content, $calctime, $lastcalc);
    my($uploadtime, $author,   $pass,     $fail,
       $dist,       $version,  $distroid, $highest_version,
       $annotation, $rtticket, $thirdpassfail
      );
    if (open my $fh, $file) {
        local $/;
        $content = <$fh>;
        my @stat = stat ($file . "dv.gz");
        # during the calc of a new one the current one is uncompressed
        # but not yet usable, so we take the slv
        @stat = stat ($file) unless @stat;
        $lastcalc = $stat[9] || 0;
        $calctime = do {
            my @gmtime = gmtime $lastcalc;
            $gmtime[4]++;
            $gmtime[5]+=1900;
            sprintf "%04d-%02d-%02dT%02d:%02dz", @gmtime[5,4,3,2,1];
        };
    } else {
        ($dist) = $distv =~ /(.+)-v?[\d\.]+$/;
    }
    my($y,$rows);
    my $dbi = db_quickaccess_handle();
    my $sql = "SELECT yaml FROM distcontext
 WHERE distv=?";
    $rows = my_get_query($dbi,$sql,$distv);
    if ($rows->[0] && defined $rows->[0][0]) {
        $y = eval { YAML::Syck::Load($rows->[0][0]) };
    } else {
        $file =~ s/\.slv$/.yml/;
        $y = eval {  YAML::Syck::LoadFile($file); };
    }
    my $need_lookup = 0;
    if ($y) {
        $self->decode_annotation($y);
        if ($y->{allversions} && @{$y->{allversions}}) {
            $highest_version = $y->{allversions}[-1]{version};
            if ($highest_version eq $y->{version}) {
                my $date = $y->{allversions}[-1]{date};
                $date =~ s/(....)(..)(..)(..)(..)/$1-$2-$3T$4:$5z/;
                $uploadtime = $date;
            } else {
                $need_lookup=1;
            }
        }
        $author = $y->{author};
        $distroid = $y->{distroid};
        $dist = $y->{dist};
        $version = $y->{version};
        $pass = $y->{passfail_overview}{pass};
        $fail = $y->{passfail_overview}{fail};
        $thirdpassfail = $pass>=3 && $fail>=3;
        $annotation = $y->{annotation};
        $rtticket = $y->{rtticket};
    } else {
        $need_lookup=1;
    }
    if ($need_lookup) {
        my $sql2 = "SELECT author, upload_date, distroid, release_date_metacpan,
        cuti_pass, cuti_fail, cuti_ts, thirdpassid,
        thirdfailid
  FROM distlookup
 WHERE distv=?";
        my $rows2 = my_get_query($dbi,$sql2,$distv);
        if ($rows2 && @$rows2) {
            my @r = @{$rows2->[0]};
            $author ||= $r[0];
            $uploadtime = $r[3] || $r[0];
            $distroid ||= $r[2];
            $pass = $r[4];
            $fail = $r[5];
            $thirdpassfail ||= ($r[7] && $r[8]);
        }
    }
    my $calc;
    if ($content) {
        my $rtables = $self->regression_tables($content);
        # warn "DEBUG rtables: ".YAML::Syck::Dump($rtables);
        my $rsq1_regressions = join ", ", map { $_->{name} } grep { $_->{rsq}==1 } @$rtables;
        my @lines = split /\n/, $content;
        pop @lines while @lines && $lines[-1] !~ /^ /;
        shift @lines while @lines && $lines[0] !~ /^ /;
        my(@xlines);
        my($calctimepass,$calctimefail) = (0,0);
        for my $line (@lines) {
            if ($line =~ s/ ([A-Z]+)\s+(\d+)//) {
                my($state,$id) = ($1,$2);
                $calctimepass++ if $state eq "PASS";
                $calctimefail++ if $state eq "FAIL";
                push @xlines, { state=>$state, id=>$id, line=>$line };
            }
        }
        $calc =
            {
             lastcalc         => $lastcalc,
             calctime         => $calctime,
             calctimefail     => $calctimefail,
             calctimepass     => $calctimepass,
             lines            => \@xlines, # unused?
             rsq1_regressions => $rsq1_regressions,
             stables          => $rtables,
            };
    } elsif ($thirdpassfail) {
        $calc =
            {
             lastcalc => 0,
             calctimepass => 0,
             calctimefail => 0,
            };
    }
    return
        {
         author           => $author,
         calc             => $calc,
         dist             => $dist,
         version          => $version,
         distroid         => $distroid,
         fail             => $fail,
         pass             => $pass,
         uploadtime       => $uploadtime,
         highest_version  => $highest_version,
         annotation       => $annotation,
         rtticket         => $rtticket,
         thirdpassfail    => $thirdpassfail,
        };
}

sub dumped_variables {
    my($self,$distv,$fields,$order) = @_; # fields,order are tainted
    my $file = "$Slvdir/$distv.slvdv";
    my($fh,$success);
    if (-e "$file.gz") {
        $success = open $fh, '-|', zcat => $file;
    } else {
        $success = open $fh, '<', $file;
    }
    unless ($success) {
        return [{id=>"N/A", ok=>"none", result=>"Currently no data available, please retry later"}];
    }
    my $y = do {local $/; YAML::Syck::Load(<$fh>)};
    close $fh;
    my %cols;
    my $obfuscate_from = grep { $_ eq "meta:from" } @$fields;
    my $sortable_perl = grep { $_ eq "meta:perl" } @$fields;
    warn "DEBUG: obfuscate_from[$obfuscate_from]";
    my @rows = map {
        for my $k (keys %$_) {
            $cols{$k} = undef;
        }
        if ($obfuscate_from and $_->{"meta:from"}){
            for ($_->{"meta:from"}) {
                s/.+\(([^\)]+)\).*/$1/;
                last if s/.*\(\"([^"]+)\"\)/$1/;
                last if s/.+\(([^\)]+)\)/$1/;
                last if s/\@([^.]+)\..+/ at $1/;
                last if s/\@/.../;
            }
            for ($_->{"meta:from"}) {
                s/<.*//; # found <briang
                s/\s+$//;
                s/^\s+//;
                s/^\"([^"]+)\"$/$1/;
                s/\\u([[:xdigit:]]{4})/Encode::encode_utf8(chr(hex($1)))/eg;
            }
        }
        if ($sortable_perl and $_->{"meta:perl"}) {
            use version;
            my $v1 = $_->{"meta:perl"};
            $v1 =~ s/[^0-9\.].*//;
            my $v2 = eval {version->new($v1)};
            my $v3 = $v2 ? $v2->numify : "";
            $_->{"meta:perl"} = bless { sortable => sprintf("%09.6f", $v3), native => $_->{"meta:perl"}}, "CPAN::Blame::Model::Solved::sortable_perl";
        }
        # id and state are always #0 and #1, rest comes as arrayref
        +{ id => $_->{id},
           state => $_->{"meta:ok"},
           results => [@{$_}{@$fields}],
         }
    } grep {
        $_->{"meta:ok"} =~ /^(PASS|FAIL|NA)$/;
    } @{$y->{"==DATA=="}};
    if (@rows && $order) {
        my @order = split /,/, $order; # 0nd,25ta: first column descending numerically, 26th col text asc
    ORDERCRIT: while (my $thisorder = pop @order) {
            my($col,$numortext,$direction) = $thisorder =~ /^(\d+)([nt])([da])$/ or next;
            my $crit;
            if ($col > scalar @$fields + 1) {
                next ORDERCRIT; # skip, no guessing, tainted data
            } elsif ($col == 0) {
                $crit = sub { shift->{id} };
            } elsif ($col == 1) {
                $crit = sub { shift->{state} };
            } else {
                $crit = sub { shift->{results}[$col-2] }
            }
            if ($numortext eq "n") {
                # <=>
                if ($direction eq "a") {
                    # $a$b
                    @rows = sort { $crit->($a) <=> $crit->($b) } @rows;
                } else {
                    # $b$a
                    @rows = sort { $crit->($b) <=> $crit->($a) } @rows;
                }
            } else {
                # cmp
                if ($direction eq "a") {
                    # $a$b
                    @rows = sort { $crit->($a) cmp $crit->($b) } @rows;
                } else {
                    # $b$a
                    @rows = sort { $crit->($b) cmp $crit->($a) } @rows;
                }
            }
        }
    }
    return { rows => \@rows, cols => [sort keys %cols] };
}

sub regression_tables {
    my($self,$content) = @_;
    return [] unless $content;
    my @tables = $content =~ m/(\(\d+\)\n\*+\nReg.*\n\*+\n(?:..+\n)+\nR\^2.+\n\*+\n)/g;
    require URI::Escape;
    my @stables = map {
        my($head,$middle,$foot) = m/\(\d+\)\n\*+\n(Reg.*)\n\*+\n((?:..+\n)+)\n(R\^2.+)\n\*+\n/;
        my @mlines = split /\n/, $middle;
        for my $line (@mlines) {
            my @td = $line =~ m/(.+\])\s+(\S+)\s+(\S+)\s+(\S+)/;
            $line = \@td;
        }
        my($name) = $head =~ /Regression '(.+)'/;
        # R^2= 1.000,
        my($rsq) = $foot =~ /^R\^2=\s*-?([\d\.]+),/;
        my($searchstring, $modstring);
        if ($name eq "qr:(Can't locate \\S+pm)") {
            my @grep;
            for my $line (@mlines) {
                next unless $line->[0];
                next unless $line->[0] =~ /eq_Can't locate (\S+)\.pm/;
                my $inc = $1;
                $inc =~ s|/|::|g;
                push @grep, $inc;
            }
            # http://search.cpan.org/grep?cpanid=BWARDEN&release=WebService-Sprint-0.01&string=DateTime|Digest%3A%3AMD5&n=1&C=0
            $searchstring = join "|", @grep;
        } elsif ($name eq "qr:(.*Base class package.*)") {
            my @grep;
            for my $line (@mlines) {
                next unless $line->[0];
                next unless $line->[0] =~ /eq_.*Base class package "(\S+?)" is empty/;
                my $inc = $1;
                push @grep, $inc;
            }
            $searchstring = join "|", uniq @grep;
        } elsif ($name =~ /^mod:(.+)/) {
            $modstring = $1;
        }
        +{
          name => $name,
          escaped_name => URI::Escape::uri_escape($name),
          rsq => $rsq,
          head => $head,
          regression => \@mlines,
          foot => $foot,
          searchstring => $searchstring,
          modstring => $modstring,
         };
    } @tables;
    \@stables;
}

sub modified {
    my($self) = @_;
    my $ret = {};
    for my $exte (qw(slv yml)) {
        my $file = sprintf "%s/lastchange_%s.ts", $Slvdir, $exte;
        if (my @stat = stat $file) {
            my @gmtime = gmtime $stat[9];
            $gmtime[4]++;
            $gmtime[5]+=1900;
            $ret->{$exte}{date} = sprintf "%04d-%02d-%02dT%02d:%02dz", @gmtime[5,4,3,2,1];
            if (open my $fh, $file) {
                local $/;
                my($content) = <$fh>;
                my $hash = eval { YAML::Syck::Load($content); };
                if ($hash && !$@ && ref $hash) {
                    $ret->{$exte}{name} = $hash->{name};
                } else {
                    chomp $content;
                    $content =~ s|.+/||;
                    $content =~ s|\.slv$||;
                    $ret->{$exte}{name} = $content;
                }
            }
        } else {
            warn "could not stat: $!";
        }
    }
    return $ret;
}

=head1 NAME

CPAN::Blame::Model::Solved - Catalyst Model

=head1 DESCRIPTION

Catalyst Model.

=head1 AUTHOR

Andreas J. Koenig,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
