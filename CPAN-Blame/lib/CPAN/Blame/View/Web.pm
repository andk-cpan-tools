package CPAN::Blame::View::Web;

use strict;
use warnings;

use base 'Catalyst::View::TT';

__PACKAGE__->config(TEMPLATE_EXTENSION => '.tt');
__PACKAGE__->config->{EVAL_PERL} = 1;
__PACKAGE__->config->{render_die} = 1;

=head1 NAME

CPAN::Blame::View::Web - TT View for CPAN::Blame

=head1 DESCRIPTION

TT View for CPAN::Blame.

=head1 SEE ALSO

L<CPAN::Blame>

=head1 AUTHOR

Andreas J. Koenig,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
