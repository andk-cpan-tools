#!/usr/bin/perl

package CPAN::Blame::Config::Cnntp;


use Sys::Hostname;
use strict;

my $hostname = Sys::Hostname::hostname;
my $x;
if (0) {
} elsif ($hostname eq "ds8143") {
    $x =
        +{
          solver_vardir => "/home/andreas/data/cnntp-solver-2009",
          ctgetreports_dir => "/home/andreas/data/cpantesters/reports",
          cpan_home => "/opt/projects/CPAN",
          ext_src => "/home/andreas/src",
         };
} elsif ($hostname eq "k81") {
    $x =
        +{
          solver_vardir => "/home/k/var/cnntp-refresh",
          ctgetreports_dir => "/home/k/var/cpantesters",
          cpan_home => "/home/ftp/pub/PAUSE",
          ext_src => "/dev/null",
        };
} elsif ($hostname eq "k83") {
    $x =
        +{
          solver_vardir => "/dev/null",
          ctgetreports_dir => "/dev/null",
          cpan_home => "/home/ftp/pub/PAUSE",
          ext_src => "/home/k/src",
        };
} elsif ($hostname =~ /^(pause|pause2-231)$/) {
    $x =
        +{
          solver_vardir => "/home/ftp/cnntp-solver-2009",
          ctgetreports_dir => "/home/ftp/cpantesters/reports",
          cpan_home => "/home/ftp/pub/PAUSE",
          ext_src => "/home/k/sources",
         };
} elsif ($hostname =~ /^(k75)$/) {
    $x =
        +{
          solver_vardir => "/home/cpantesters/cnntp-solver-2009",
          ctgetreports_dir => "/home/cpantesters/reports",
          cpan_home => "/home/ftp/pub/PAUSE",
          ext_src => "/home/k/sources",
         };
} elsif ($hostname =~ /^(k93x64sid|sidpg|ssdnodes-629b09d13097a)$/) {
    $x =
        +{
          solver_vardir => "$ENV{HOME}/var/solver_vardir", # ???
          ctgetreports_dir => "$ENV{HOME}/var/ctgetreports_dir", # ???
          cpan_home => "/home/ftp/pub/PAUSE",
          ext_src => "$ENV{HOME}/src", # ???
         };
} else {
    warn "Warning: Cnntp.pm not preconfigured for hostname[$hostname]";
    # do not die, we want that perl -c can finish
    $x =
        +{
          solver_vardir => "/dev/null",
          ctgetreports_dir => "/dev/null",
          cpan_home => "/dev/null",
          ext_src => "/dev/null",
         }
}
$x->{maxyears} = 16;
# my $db = "sqlite";
my $db = "pg";

sub common_quickaccess_handle {
    require DBI;
    if ($db eq "sqlite") {
        # now SQLite; Note: both cnntp-solver and Solved.pm get it from here
        my $workdir = "$x->{solver_vardir}/workdir";
        my $qctdb = "$workdir/quickaccess.db";
        my $dbi = DBI->connect ("dbi:SQLite:dbname=$qctdb");
        # not any more: return CPAN::WWW::Testers::Generator::Database->new(database=>$qctdb);
    } else {
        my $dbi = DBI->connect ("dbi:Pg:dbname=analysis");
    }
}

no warnings 'once';
$CPAN::Blame::Config::Cnntp::Config = $x;

unless (caller()) {
    my($arg) = shift @ARGV;
    print $x->{$arg}, "\n";
}

1;
