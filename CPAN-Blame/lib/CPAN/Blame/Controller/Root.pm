package CPAN::Blame::Controller::Root;

use strict;
use warnings;
use parent 'Catalyst::Controller';
use Catalyst::Utils ();
use List::Util qw(max);
use List::MoreUtils qw(uniq);
use Scalar::Util qw(reftype);
use JSON::XS;
use Time::Moment;
use YAML::Syck;

use CPAN::Blame::Config::Cnntp;

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#

my $Slvdir = "$CPAN::Blame::Config::Cnntp::Config->{solver_vardir}/workdir/solved";

__PACKAGE__->config->{namespace} = '';

=head1 NAME

CPAN::Blame::Controller::Root - Root Controller for CPAN::Blame

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=cut

=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
    my $author = uc($c->req->params->{author}||"");
    my $age = $c->req->params->{age};
    my %filter =
        (
         author => $author,
         age => $age,
        );
    $c->stash
        (
         tpage => ".",
         age_options =>
         [
          ["91.3",  "1/4 year"],
          ["182.6", "1/2 year"],
          ["365.3", "1 year"],
          ["730.5", "2 years"],
          ["1461",  "4 years"],
          ["2922", "8 years"],
          $CPAN::Blame::Config::Cnntp::Config->{maxyears} >= 16 ? ["5844", "16 years"] : (),
         ],
         dists => $c->model('Solved')->all("db+",\%filter),
         modified => $c->model('Solved')->modified,
         %filter,
        );
}

sub solved :Local {
    my ( $self, $c ) = @_;
    my $distv = $c->req->params->{distv};
    $c->stash
        (
         tpage => "solved",
         distv => $distv,
         rtables => $c->model('Solved')->adist($distv),
        );
}

sub parsed_slv :Local {
    my ( $self, $c ) = @_;
    my $distv = $c->req->params->{distv};
    $c->stash
        (
         tpage => "reports_by_field",
         distv => $distv,
         rtables => $c->model('Solved')->adist($distv),
        );
}

sub reports_by_field :Local {
    my ( $self, $c ) = @_;
    my $p = $c->req->params;
    my $distv = $p->{distv};
    my $fields = $p->{field}; # strange rename
    my $order = $p->{order};
    if ($fields) {
        my $reftype = reftype $fields;
        if ($reftype && $reftype eq "ARRAY"){
            $fields = [ uniq @$fields ];
        } else {
            $fields = [$fields];
        }
    } else {
        $fields = [qw( meta:date meta:perl conf:archname conf:osvers meta:from conf:useithreads conf:nvsize )];
    }
    $c->stash
        (
         tpage => "reports_by_field",
         distv => $distv,
         fields => $fields,
         dumped_variables => $c->model('Solved')->dumped_variables($distv,$fields,$order),
         rtables => $c->model('Solved')->adist($distv),
        );
}

sub about :Local {
    my ( $self, $c ) = @_;
    $c->stash
        (
         tpage => "about",
        );
}

sub news :Local {
    my ( $self, $c ) = @_;
    my $home = Catalyst::Utils::home(__PACKAGE__);
    my $chlog = File::Spec->catfile($home, "root", "news.changelog");
    my $content = do { open my $fh, $chlog or die; local $/; <$fh> };
    $content =~ s/\cl.*//s;
    my @days = split /(?=^\d)/m, $content;
    for my $day (@days) {
        my($label,$daybody) = $day =~ /(.+)\n[ \t]*\n((?s:.+))/;
        my @li = split /(?=^\t\*)/m, $daybody;
        warn "DEBUG: li[@li]";
        for my $li (@li) {
            $li =~ s/^\t\*\s+//;
            my @p = split /\n[ \t]*\n/, $li;
            for (@p) {
                s|\b(http://\S+)|<a href="$1">$1</a>|g;
            }
            $li = \@p;
        }
        $day = { label => $label, body => \@li, rawbody => $daybody };
    }
    $c->stash
        (
         tpage => "news",
         content => \@days,
        );
}

# estimate_total_time
sub ett {
    my($self,$h,$recent_timing,$distv,$freeslots) = @_;
    if ($recent_timing->{$distv}) {
        $h->{ett} = $recent_timing->{$distv};
        $h->{et} = "previous calculation"; # estimation type prevcalcduration
    } else {
        my $slvfile = "$Slvdir/$distv.slv";
        if (-e $slvfile && -s _) {
            $h->{ett} = int(433/2354809 * (-s _) + .5);
            $h->{et} = "size of metadata"; # estimation type bytes-in-slvfile
        } else {
            $h->{ett} = 440; # last 10000 on 2018-07-17
            $h->{et} = "default"; # estimation type default
        }
    }
    unless (defined $h->{myslot}) {
        my($mini,$minstart);
        for my $i (0..$#$freeslots) {
            $freeslots->[$i] ||= time;
            if (!defined $minstart || $freeslots->[$i]<$minstart) {
                $minstart = $freeslots->[$i];
                $mini = $i;
            }
        }
        $h->{myslot} = $mini;
        $h->{startepoch} = $minstart;
    }
    if ($h->{startepoch}+$h->{ett} < time) {
        $h->{etaepoch} = time;
        $h->{overtime} = 1;
    } else {
        $h->{etaepoch} = $h->{startepoch}+$h->{ett};
    }
    $freeslots->[$h->{myslot}] = $h->{etaepoch};
    $h->{ets} = Time::Moment->from_epoch($h->{startepoch})->to_string;
    $h->{eta} = Time::Moment->from_epoch($h->{etaepoch})->to_string;
    my $redis = Redis->new;
    $redis->zadd("analysis:distv:eta",$h->{etaepoch},$distv);
}

sub mgmt :Local {
    my ( $self, $c ) = @_;
    my(%mgmt,%recent_timing,%cache_descr);
    use Redis;
    my $redis = Redis->new;
    my $jsonxs = JSON::XS->new->indent(0);
    {
        #
        # Recent Jobs
        #
        my $n = $c->req->params->{nrecent} || 10;
        my(@recent) = $redis->lrange("analysis:jobqueue:recentjobs",-10000,-1);
        $mgmt{recentcnt} = $n;
        $mgmt{recenttotal} = scalar @recent;
        $mgmt{recent} = [];
        for my $id (reverse @recent) {
            my %h;
            $h{id} = $id;
            my $j;
            unless ($j = $cache_descr{$id}) {
                my($json) = $redis->get("analysis:jobqueue:jobs:$id\:descr");
                $j = eval { $jsonxs->decode($json) };
                if ($j && !$@) {
                    $cache_descr{$id} = $j;
                } else {
                    $cache_descr{$id} = "N/A ($id)";
                }
            }
            my $distv = $h{distv} = $j->{distv};
            $recent_timing{$distv} ||= $j->{took};
            if (@{$mgmt{recent}} < $n) {
                $h{start} = Time::Moment->from_epoch($j->{start})->to_string;
                $h{finished} = Time::Moment->from_epoch($j->{finished})->to_string;
                $h{took} = $j->{took};
                push @{$mgmt{recent}}, \%h;
            }
        }
    }
    my @freeslots;
    {
        #
        # Running Jobs
        #
        my(@running) = $redis->smembers("analysis:jobqueue:runningjobs");
        $mgmt{runningcnt} = scalar @running;
        my @a;
        for my $id (@running) {
            my %h;
            $h{id} = $id;
            my $j;
            unless ($j = $cache_descr{$id}) {
                my($json) = $redis->get("analysis:jobqueue:jobs:$id\:descr");
                $j = $jsonxs->decode($json);
                $cache_descr{$id} = $j;
            }
            my $distv = $h{distv} = $j->{distv};
            $h{startepoch} = $j->{start};
            $h{myslot} = scalar @a;
            $freeslots[$h{myslot}] = $h{startepoch};
            $h{start} = Time::Moment->from_epoch($j->{start})->to_string;
            $h{href} = 1;
            $self->ett(\%h,\%recent_timing,$distv,\@freeslots);
            push @a, \%h;
        }
        $mgmt{running} = \@a;
        my $cronjob = "$CPAN::Blame::Config::Cnntp::Config->{ext_src}/andk-cpan-tools/bin/analysis-worker.pl";
        open my $cronfh, "<", $cronjob
            or die "Could not open '$cronjob': $!";
    LINE: while (<$cronfh>) {
            next unless /\bmax_procs\s*(?:=>|,)\s*(\d+)/;
            $mgmt{concurrencylimit} = $1;
            $#freeslots = $mgmt{concurrencylimit}-1;
            last LINE;
        }
        while (@{$mgmt{running}} < $mgmt{concurrencylimit}) {
            push @{$mgmt{running}}, { id=>"-", distv=>"(EMPTY SLOT)", start=>"-", href=>0 };
        }
    }
    {
        #
        # Upcoming Jobs
        #
        my $n = $c->req->params->{nupcom} || 10;
        my($zcard) = $redis->zcard("analysis:jobqueue:q");
        $n = $zcard if $n > $zcard;
        my(@upcoming) = $redis->zrevrange("analysis:jobqueue:q",0,$zcard-1,"withscores");
        $mgmt{upcomingcnt} = $n;
        $mgmt{upcomingtotal} = $zcard;
        $mgmt{upcoming} = [];
        while (@upcoming) {
            my($distv,$score) = splice @upcoming, 0, 2;
            my %h;
            $h{distv} = $distv;
            $h{score} = $score;
            $self->ett(\%h,\%recent_timing,$distv,\@freeslots);
            push @{$mgmt{upcoming}}, \%h unless @{$mgmt{upcoming}} >= $n;
        }
    }
    {
        no warnings 'uninitialized'; # freeslots may be full with undefs
        my $max = max @freeslots;
        for my $i (0..$#{$mgmt{running}}) {
            $mgmt{running}[$i]{slot_free_at_epoch} = $freeslots[$i];
            $mgmt{running}[$i]{slot_free_at} = $freeslots[$i] ?
                Time::Moment->from_epoch($freeslots[$i])->to_string : "";
            if ($freeslots[$i] == $max) {
                $mgmt{running}[$i]{slot_free_at_max} = 1;
            }
        }
    }
    $c->stash
        (
         tpage => "mgmt",
         mgmt => \%mgmt,
        );
}

sub matrixndjson :Path('matrixndjson') :Args(0) {
    my ( $self, $c ) = @_;
    my %params;
    for my $param (qw(after dist version)) {
        my $p = $c->req->params->{$param};
        $params{$param} = $p if length($p);
    }
    my $body = Encode::encode("UTF-8", $c->model('Solved')->ndjson(\%params));
    warn "body=$body";
    $c->response->headers->header("Content-Type" => "application/x-ndjson; charset=utf-8");
    $c->response->body($body);
}

sub beforemaintrelease :Local {
    my ( $self, $c ) = @_;
    my(%meta,%data);
    my $dir = "/home/andreas/var/beforemaintrelease/";
    use File::Spec ();
    my $overviewfile =
        File::Spec->catfile($dir, "overview.json");
    my $O;
    use JSON::XS;
    my $jsonxs = JSON::XS->new->indent(0);
    until ($O) {
        my($slurp) = do { open my($lfh), $overviewfile; local $/; <$lfh> };
        if ($slurp) {
            $O = eval { $jsonxs->decode($slurp) };
        }
        unless ($O) {
            use Time::HiRes ();
            Time::HiRes::sleep 0.2;
        }
    }
    my @pairs;
    for my $k (sort {$O->{$b} cmp $O->{$a}} keys %$O) {
        my $v = $O->{$k};
        my @p = split /:/, $k;
        push @pairs, { ts => $v, p1 => $p[0], p2 => $p[1] };
    }
    if (my $pair = $c->req->params->{pair}) {
        if (my $result_ts = $O->{$pair}) {
            $meta{ts} = $result_ts;
            my $file = File::Spec->catfile($dir,"result-$pair.json");
            if (-f $file) {
                $meta{file} = $file;
            }
        }
    }
    unless ($meta{file}) {
        my $pair = $pairs[0];
        $meta{file} = File::Spec->catfile($dir,"result-$pair->{p1}:$pair->{p2}.json");;
        $meta{ts}   = $pair->{ts};
    }
    open my $fh, $meta{file} or die "could not open <'$meta{file}': $!";
    my($old_highscore);
    my $S = eval { local $/; decode_json <$fh>; };
    my $T;
    if (!$S || $@) {
        die "Error while parsing $meta{file}: $@";
    } else {
        $T = $S->{"!ALL"};
        $S = $S->{"!CAND"};
    }
    {
        my @k = keys %$S; # Test-Given
        my @k2 = keys %{$S->{$k[0]}}; # 0.3.1
        @meta{qw(oldperl rcperl)} = sort keys %{$S->{$k[0]}{$k2[0]}}; # 5.18...
        $meta{totalrcreports}     = $T->{$meta{rcperl}} || "N/A";
        $meta{totalrcdists}       = $T->{totalrcdists}  || "N/A";
    }
    use DBI;
    my $pgdbh = DBI->connect("dbi:Pg:dbname=analysis") or die "Could not connect to 'analysis': $DBI::err";
    my $pgsth1 = $pgdbh->prepare("select author, release_date_metacpan, upload_date from distlookup where distv=?");
    my $pgsth2 = $pgdbh->prepare("SELECT yaml FROM distcontext WHERE distv=?");
    my $redis = eval { Redis->new };
    if (1) {
        for my $k (sort {lc $a cmp lc $b} keys %$S) {
            for my $k2 (sort keys %{$S->{$k}}){
                my $v = $S->{$k}{$k2};
                next unless $meta{oldperl} and $meta{rcperl} and exists $v->{$meta{oldperl}} and exists $v->{$meta{rcperl}};
                my %r;
                $r{distv} = "$k-$k2";
                $r{oldpass} = $v->{$meta{oldperl}}{pass}||0;
                $r{oldfail} = $v->{$meta{oldperl}}{fail}||0;
                $r{rcpass} = $v->{$meta{rcperl}}{pass}||0;
                $r{rcfail} = $v->{$meta{rcperl}}{fail}||0;
                $r{has_solved} = -e "$Slvdir/$r{distv}.slv";
                my $allow_influencing_jobqueue = 0;
                if ($redis && $allow_influencing_jobqueue && !$r{has_solved}) {
                    unless (defined $old_highscore) {
                        my($topdistv,$highscore) = $redis->zrevrange("analysis:jobqueue:q",0,0,"withscores");
                        $old_highscore = $highscore;
                        $old_highscore //= 0;
                    }
                    my $score = $old_highscore;
                    $redis->zadd("analysis:jobqueue:q", $score, $r{distv});
                }
                $pgsth1->execute($r{distv});
                @r{qw(author release_date_metacpan upload_date)} = $pgsth1->fetchrow_array;
                if ($r{release_date_metacpan}){
                    $r{release_date_metacpan} =~ s/\s/T/;
                    $r{upload_date} =
                        Time::Moment->from_string
                            ($r{release_date_metacpan})->at_utc->strftime("%FT%T%Z");
                    $r{upload_date} =~ s/Z/z/;
                }
                my $a = $data{results} ||= [];
                push @$a, \%r;
                $pgsth2->execute($r{distv});
                my($ystr) = $pgsth2->fetchrow_array;
                if (my $y = eval { YAML::Syck::Load($ystr) }) {
                    $c->model('Solved')->decode_annotation($y);
                    $r{annotation} = $y->{annotation};
                    $r{rtticket} = $y->{rtticket};
                    if (
                        $y->{allversions} &&
                        @{$y->{allversions}} &&
                        $y->{version} ne $y->{allversions}[-1]{version}) {
                        if ($r{annotation}) {
                            $r{annotation} = "=> newer $y->{allversions}[-1]{version},$r{annotation}";
                        } else {
                            $r{annotation} = "=> newer $y->{allversions}[-1]{version}";
                        }
                    }
                }
                if ($redis && ! $r{annotation}) {
                    my $redis_lcfail = $redis->zscore("analysis:distv:lcfail",$r{distv});
                    if (defined $redis_lcfail and $redis_lcfail == 0) {
                        $r{annotation} = "NOTE: last regression calculation concludes there are no fails. Maybe these fails had to be filtered out.";
                    }
                }
            }
        }
    }
    $meta{count} = scalar @{$data{results}||[]};
    $c->stash
        (
         tpage => "beforemaintrelease",
         meta => \%meta,
         data => \%data,
         pairs => \@pairs,
        );
}

sub default :Path {
    my ( $self, $c ) = @_;
    $c->response->body( 'Page not found' );
    $c->response->status(404);
}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : ActionClass('RenderView') {
    my ( $self, $c ) = @_;
    my $error = $c->error;
    my $errors = @$error;
    if ($errors) {
        warn @$error;
        $c->res->status(500);
        $c->res->body('internal server error');
        $c->clear_errors;
    } else {
        $c->stash
            (menu => 
             [
              { label => "home", path => "." },
              { label => "about", path => "about" },
              { label => "news", path => "news" },
              { label => "mgmt", path => "mgmt" },
             ]
            );
    }
}

=head1 AUTHOR

Andreas J. Koenig,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
