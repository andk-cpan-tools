package CPAN::Blame::Util;

sub decode_annotation ($) {
    my($e) = @_;
    if ($e->{annotation} && $e->{annotation} =~ m!,|\d{5,6}|https?://!) {
        my @tickets_raw = split /\s*,\s*/, $e->{annotation};
        my @tickets;
        for my $t (@tickets_raw) {
            my(%t);
            $t{label} = $t;
            if ($t =~ m!(\bhttps?://\S+)!) {
                $t{url} = $1;
            } elsif ($t =~ /(\d{5,6})/) { # warning: limiting RT tickets to 999999
                $t{id} = $1; # backwards compatibility
                $t{url} = "https://rt.cpan.org/Ticket/Display.html?id=$1";
            }
            push @tickets, \%t;
        }
        $e->{rtticket} = \@tickets; # [map {+{ id => $_ }} split /[,\s]+/, ];
    }
}

1;
