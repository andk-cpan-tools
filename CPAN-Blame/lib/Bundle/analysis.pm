package Bundle::analysis;

$VERSION = '0.01';

1;

__END__

=head1 NAME

Bundle::analysis

=head1 SYNOPSIS

perl -MCPAN -e 'install Bundle::analysis'

=head1 CONTENTS

Algorithm::Numerical::Shuffle
BSD::Resource
CPAN::Testers::ParseReport
CPAN::Testers::WWW::Reports::Query::Reports
Catalyst::Model
File::Rsync::Mirror::Recent
File::chdir
IPC::ConcurrencyLimit
Linux::Inotify2
Redis
Sort::Versions
Time::Duration
Time::Moment
XML::LibXML
Catalyst::Plugin::AccessLog
Catalyst::Plugin::Compress::Gzip
Catalyst::View::TT
Template::Plugin::CGI

=head1 CONFIGURATION

=head1 AUTHOR

