#
# Example of a user's .screenrc file
#

# This is how one can set a reattach password:
#password ODSJQf.4IJN7E    # "1234"

# no annoying audible bell, please
vbell on

# detach on hangup
autodetach on

# don't display the copyright page
startup_message off

# Extend the vt100 desciption with some sequences.
termcap  vt100* ms:AL=\E[%dL:DL=\E[%dM:UP=\E[%dA:DO=\E[%dB:LE=\E[%dD:RI=\E[%dC
terminfo vt100* ms:AL=\E[%p1%dL:DL=\E[%p1%dM:UP=\E[%p1%dA:DO=\E[%p1%dB:LE=\E[%p1%dD:RI=\E[%p1%dC

#xterm understands both im/ic and doesn't have a status line.
#Note: Do not specify im and ic in the real termcap/info file as
#some programs (e.g. vi) will not work anymore.
termcap  xterm hs@:cs=\E[%i%d;%dr:im=\E[4h:ei=\E[4l
terminfo xterm hs@:cs=\E[%i%p1%d;%p2%dr:im=\E[4h:ei=\E[4l

#80/132 column switching must be enabled for ^AW to work
#change init sequence to not switch width
termcap  xterm Z0=\E[?3h:Z1=\E[?3l:is=\E[r\E[m\E[2J\E[H\E[?7h\E[?1;4;6l
terminfo xterm Z0=\E[?3h:Z1=\E[?3l:is=\E[r\E[m\E[2J\E[H\E[?7h\E[?1;4;6l

#make hp700 termcap/info better
termcap  hp700 'Z0=\E[?3h:Z1=\E[?3l:hs:ts=\E[62"p\E[0$~\E[2$~\E[1$}:fs=\E[0}\E[61"p:ds=\E[62"p\E[1$~\E[61"p:ic@'
terminfo hp700 'Z0=\E[?3h:Z1=\E[?3l:hs:ts=\E[62"p\E[0$~\E[2$~\E[1$}:fs=\E[0}\E[61"p:ds=\E[62"p\E[1$~\E[61"p:ic@'

#wyse-75-42 must have flow control (xo = "terminal uses xon/xoff")
#essential to have it here, as this is a slow terminal.
termcap wy75-42 xo
terminfo wy75-42 xo

# New termcap sequences for cursor application mode.
termcap wy* CS=\E[?1h:CE=\E[?1l:vi=\E[?25l:ve=\E[?25h:VR=\E[?5h:VN=\E[?5l:cb=\E[1K:CD=\E[1J
terminfo wy* CS=\E[?1h:CE=\E[?1l:vi=\E[?25l:ve=\E[?25h:VR=\E[?5h:VN=\E[?5l:cb=\E[1K:CD=\E[1J

# Make the output buffer large for (fast) xterms.
termcap xterm* OL=10000
terminfo xterm* OL=10000

#remove some stupid / dangerous key bindings
bind k
bind ^k
bind .
bind ^\
bind \\
bind ^h
bind h
#make them better
# NOOOO: bind '\\' quit
bind 'K' kill
bind 'I' login on
bind 'O' login off
bind '}' history

pow_detach_msg "Screen session of \$LOGNAME \$:cr:\$:nl:ended."

# Yet another hack:
# Prepend/append register [/] to the paste if ^a^] is pressed.
# This lets me have autoindent mode in vi.
register [ "\033:se noai\015a"
register ] "\033:se ai\015a"
bind ^] paste [.]

# tell screen that xterm can switch to dark background and has function
# keys.
terminfo xterm 'VR=\E[?5h:VN=\E[?5l:k1=\E[11~:k2=\E[12~:k3=\E[13~:k4=\E[14~'
termcap  xterm 'VR=\E[?5h:VN=\E[?5l:k1=\E[11~:k2=\E[12~:k3=\E[13~:k4=\E[14~'
termcap  xterm 'kh=\E[1~:kI=\E[2~:kD=\E[3~:kH=\E[4~:kP=\E[5~:kN=\E[6~'
terminfo xterm 'kh=\E[1~:kI=\E[2~:kD=\E[3~:kH=\E[4~:kP=\E[5~:kN=\E[6~'

# special xterm hardstatus: use the window title.
termcap  xterm 'hs:ts=\E]2;:fs=\007:ds=\E]0;Screen\007'
terminfo xterm 'hs:ts=\E]2;:fs=\007:ds=\E]0;Screen\007'

# advertise hardstatus support to $TERMCAP
termcap  * '' 'hs:ts=\E_:fs=\E\\:ds=\E_\E\\'
terminfo * '' 'hs:ts=\E_:fs=\E\\:ds=\E_\E\\'

# make the shell in every window a login shell
#shell -$SHELL

# shellaka '> |tcsh'
# shellaka '$ |sh'

terminfo wy75-42 'G0:S0=\E(K:E0=\E(B:C0=\104\133\126\134\134\135\144\173\166\174\174\175\137\176'

terminfo wy75-42 'hs@'

# our xterm has colors! (rxvt, too)
termcap  xterm 'AF=\E[3%dm:AB=\E[4%dm'
terminfo xterm 'AF=\E[3%p1%dm:AB=\E[4%p1%dm'

# set every new windows hardstatus line to somenthing descriptive
defhstatus "Screen: window \5 (\5t)"

defscrollback 15000

#terminfo xterm 'vb=\E[?5h$<200/>\E[?5l'
#termcap  xterm 'vi=\E[?25l:ve=\E[34h\E[?25h:vs=\E[34l'

# emulate part of the 'K' charset
termcap   xterm 'XC=K%,%\E(B,[\304,\\\\\326,]\334,{\344,|\366,}\374,~\337'
terminfo  xterm 'XC=K%,%\E(B,[\304,\\\\\326,]\334,{\344,|\366,}\374,~\337'

escape ^Oo

defutf8 on

msgminwait 4

# not possible with the standard version:
# maxwin 199

# bindkey -k k8 windowlist -b
bind ^i windowlist -b -m

#                   screen -fn -t foobar -L 2 telnet foobar

screen 0
screen -t "____root____"
stuff "ssh root@localhost -X -Y"

screen -t "TI I perl-clone secondsmoke"
stuff 'cd /tmp && git clone /home/sand/src/perl/repoperls/perl5.git.perl.org/perl --no-single-branch perl-clone-$$ && cd perl-clone-$$/ && git config user.email root@dubravka.in-berlin.de && git config user.name "Andreas Koenig"'

screen -t "TI III run cpan"
stuff "/home/sand/src/perl/repoperls/installed-perls/host/\$HOST/v5.[TAB]"

screen -t "TI V basesmoker (msid)"
stuff "cd ~/megalog && perl ~/src/andk/andk-cpan-tools/bin/basesmoker.run.pl /home/sand/src/perl/repoperls/installed-perls/host/k93msid/v5.34.1/1e0c/bin/perl --sleep=170000"

screen -t "TI V basesmoker --extra (usually not msid)"
stuff "cd /var/log/megasand && /home/sand/src/perl/repoperls/installed-perls/host/\$HOST/v5.34.1/1e0c/bin/perl ~/src/andk/andk-cpan-tools/bin/basesmoker.pl --extra --perl=/home/sand/src/perl/repoperls/installed-perls/host/\$HOST/v5.[TAB]"

screen -t "TI V basesmoker --with=use-02-packages (usually bionic)"
stuff "cd /var/log/megasand && /home/sand/src/perl/repoperls/installed-perls/host/\$HOST/v5.36.0/6567/bin/perl ~/src/andk/andk-cpan-tools/bin/basesmoker.pl --extra --perl=/home/sand/src/perl/repoperls/installed-perls/host/\$HOST/v5.37.x/xxxx/bin/perl --with=use-02-packages --timeout=2200000 --with=log --transient_build_dir &"

screen -t "TI V loop --makeperl (msid)"
stuff "/home/sand/src/perl/repoperls/installed-perls/host/k93msid/v5.38.2/1e0c/bin/perl  ~/src/andk/andk-cpan-tools/bin/loop-over-recent.pl --transient_build_dir --distro=Data::VString --makeperl"

screen -t "TI VIII antihang (not msid)"
stuff "cd /var/log/megasand && perl ~/src/andk/andk-cpan-tools/bin/basesmoker-antihang.pl"

screen -t "TI VIII antihang (msid)"
stuff "cd ~/megalog && perl ~/src/andk/andk-cpan-tools/bin/basesmoker-antihang.pl"

screen -t "TI VII perl git (msid)"
stuff "cd ~/src/perl/repoperls/perl5.git.perl.org/perl/ && git config user.email root@dubravka.in-berlin.de && git config user.name Andreas\ Koenig && /home/sand/src/perl/repoperls/installed-perls/host/k93msid/v5.34.1/1e0c/bin/perl ~/src/andk/andk-cpan-tools/bin/generate-recent.pl --jobs=2 --sleep 58000"

screen -t "TI C I replika (msid)"
stuff "while true; do nice /home/sand/src/perl/repoperls/installed-perls/host/k93msid/v5.34.1/1e0c/bin/perl -I ~/src/andk/rersyncrecent/lib -I ~/cpanpm/lib ~/src/andk/andk-cpan-tools/bin/loop-over-recent.pl --randomskip=0.86 --sleep=0.2 --parallel=1 --transient_build_dir --log ; date ; echo 'Sleeping 1800'; sleep 1800; date; done"

screen -t "TI C I replika (stretch)"
stuff "/home/sand/src/perl/repoperls/installed-perls/host/k93stretch/v5.34.1/50aa/bin/perl -I ~/src/andk/rersyncrecent/lib -I ~/cpanpm/lib ~/src/andk/andk-cpan-tools/bin/loop-over-recent.pl  --parallel=1 --transient_build_dir --statefile=/home/sand/.cpan/loop-over-recent-k93stretch.state --test --perlglob='/home/sand/src/perl/repoperls/installed-perls/host/k93stretch/v5.3[246].[01]/????/bin/perl' --log"

screen -t "TI C I replika (buster)"
stuff "/home/sand/src/perl/repoperls/installed-perls/host/k93buster/v5.30.0/2497/bin/perl -I ~/src/andk/rersyncrecent/lib -I ~/cpanpm/lib ~/src/andk/andk-cpan-tools/bin/loop-over-recent.pl  --parallel=1 --transient_build_dir --initial-backlog=600 --statefile=/home/sand/.cpan/loop-over-recent-k93buster.state --test --perlglob='/home/sand/src/perl/repoperls/installed-perls/host/k93buster/v5.{26.3/ba24,30.2/2497,31.10/9de7}/bin/perl' --log"

screen -t "TI C VI replika cleanout ctr loop (msid)"
stuff "/home/sand/src/perl/repoperls/installed-perls/host/k93msid/v5.34.1/1e0c/bin/perl ~/src/andk/andk-cpan-tools/bin/cleanout-ctr-loop.pl"

screen -t "~~~~Xvfb :121~~~~ (all)"
stuff "while true; do date ; if ! ps auxww | grep -v grep | grep -q Xvfb ; then Xvfb :121 & fi; echo -n 'sleeping 60 '; sleep 60; done"

screen -t "TI IX cleanup TEMP (all)"
stuff "cd ~/src/andk/andk-cpan-tools && while true; do perl bin/quick-tmp-cleanup.pl; echo sleeping; sleep 10800; done"

screen -t "vmstat...tee (all)"
stuff "vmstat -t 1 | tee vmstat-\$HOST-`date +%FT%T`.log"
