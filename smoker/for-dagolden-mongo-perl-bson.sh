#!/bin/sh

# call as:
# sh smoker/for-dagolden-mongo-perl-bson.sh |& tee smoker/for-dagolden-mongo-perl-bson.sh.out

# remember: JSON::XS requires 5.8.3

# remember: this would be a case for install-prerequisites as cpanm
# has it, so that one never has to do the install "." command, just
# the install-prereqs once

set -x
cd /home/sand/src/mongo-perl-bson
git pull
git describe
for p in /home/sand/src/perl/repoperls/installed-perls/perl/*5.8.[3-9]/????/bin/perl ; do
    $p Makefile.PL
    : $p -MCPAN -e 'install("Devel::PPPort", "JSON::XS", ".")'
    $p -MCPAN -e 'test "."'
done
set +x
